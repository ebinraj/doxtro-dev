importScripts('https://www.gstatic.com/firebasejs/4.3.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.3.0/firebase-messaging.js');

firebase.initializeApp({
    // get this from Firebase console, Cloud messaging section
    'messagingSenderId': '107438437489'
});

const messaging = firebase.messaging();