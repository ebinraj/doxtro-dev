// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   host : "https://apps.doxtro.com/",
//   apiversion  :"api/v1/",
//   firebase: {
//     apiKey: 'AIzaSyDz9jKDxnifednTl5-6dTUhxV_FNjWSHlo',
//     authDomain: 'doxtrov2.firebaseapp.com',
//     databaseURL: 'https://doxtrov2.firebaseio.com',
//     projectId: 'doxtrov2',
//     storageBucket: 'doxtrov2.appspot.com',
//     messagingSenderId: '1034170587910'
//   },
//   consultationkey : "Consultations"
// };

export const environment = {
  production: false,
   host: "https://apps.doxtro.com/",
  // host : "http://localhost:3000/",
  //host : "http://doxtro2c.above-inc.com/",
  apiversion: "api/v1/",
  firebase: {
    apiKey: "AIzaSyAVhQi08PId7yGBm2cKfV2adjjgSW-YHlo",
    authDomain: "doxtrodevelopment.firebaseapp.com",
    databaseURL: "https://doxtrodevelopment.firebaseio.com",
    projectId: "doxtrodevelopment",
    storageBucket: "doxtrodevelopment.appspot.com",
    messagingSenderId: "107438437489"
  },
  consultationkey: "Consultations"
};
