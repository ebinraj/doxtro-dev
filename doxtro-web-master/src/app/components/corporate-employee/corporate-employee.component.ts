import { Router } from '@angular/router';
import { headerwithtoken } from './../../common/headers';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewRef, ViewChild, ElementRef } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { AppService } from 'app/services/app.service';
const URL = 'api/v1/employees/upload';


@Component({
  selector: 'app-corporate-employee',
  templateUrl: './corporate-employee.component.html',
  styleUrls: ['./corporate-employee.component.css']
})
export class CorporateEmployeeComponent implements OnInit {

  employeeForm: FormGroup;
  companyList: any[] = [];
  companyGroupList: any[] = [];
  companyObj = { groups: [] };
  bulkuploadresult = {
    success: [],
    failed: []
  };


  uploader: FileUploader = new FileUploader({
    url: URL,
    authToken: localStorage.getItem("token")
  });

  constructor(public _fb: FormBuilder, public _appService: AppService, public router: Router) { }

  ngOnInit() {
    debugger;
    this.employeeForm = this._fb.group({
      companyName: ["", Validators.required],
      group: [""],
      employeeName: ["", Validators.required],
      employeeEmail: ["", [Validators.required, Validators.email]],
      employeePhone: ["", Validators.required],
    });

    this._appService.getcompanyNameList().toPromise().then(result => {
      this.companyList = result.data;
    });

    this.uploader.response.subscribe(res => {
      debugger;

      if (JSON.parse(res).data) {

        console.log("res", res);
        this.bulkuploadresult = JSON.parse(res).data;
        console.log(this.bulkuploadresult);
      }
      else {
        alert("Error " + JSON.parse(res).message);
      }
    }, error => {
      console.log("error uploader subscribe", error);
    });

  }

  handleUpload(){
    console.log("uploaderlength", this.uploader.queue);
    if(this.uploader.queue.length > 0){
      this.uploader.uploadAll();
    }
  }

  deleteFileFromQueue(item){
    this.uploader.removeFromQueue(item);
  }

  doEmployeeCreate({ value, valid }) {
    if (valid) {
      value.companyId = this.companyObj["_id"];
      this._appService.createEmployee(value).toPromise().then(result => {
        alert("Employee Created");
        this.router.navigate(["app/employee-list/"+value.companyName]);
      }).catch(err => {
        console.log(err);
        alert(JSON.parse(err._body).message);
      });
    }
    else {
      alert("Please fill the form or check the email address");
    }
  }

  companyChange(e) {
    this._appService.getcompanyByName(e.currentTarget.value).toPromise().then(result => {
      this.companyObj = result.data;
      if (this.companyObj.groups) {
        this.companyGroupList = this.companyObj.groups;
      }
    }).catch(err => {
      console.log(err);
    })
  }



}
