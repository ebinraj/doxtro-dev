import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultationChargesComponent } from './consultation-charges.component';

describe('ConsultationChargesComponent', () => {
  let component: ConsultationChargesComponent;
  let fixture: ComponentFixture<ConsultationChargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultationChargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultationChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
