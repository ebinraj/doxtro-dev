import { ToastrService } from 'ngx-toastr';
import { AppService } from '../../services/app.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consultation-charges',
  templateUrl: './consultation-charges.component.html',
  styleUrls: ['./consultation-charges.component.css']
})
export class ConsultationChargesComponent implements OnInit {
  ccharges: any = { category: "charges" };
  cchargesList: any[] = [];
  specializationList: any[] = [];
  consulationTypeList = ["Chat", "Audio", "Planned-Chat", "Planned-Audio"];

  constructor(public _appService: AppService, public toastr: ToastrService) {
    this.ccharges.charges = { consultationFee: 0, serviceCharge: 0, doctorFee: 0 };
  }

  ngOnInit() {
    this._appService.getAllSpecialization().toPromise().then(result => {
      this.specializationList = result.data;
      this.ccharges.subCategory = this.specializationList[0]._id;
    }, reject => {
      console.log(reject);
    });
    this.getListOfCharges();
    this.ccharges.consultationType = this.consulationTypeList[0];
  }

  getListOfCharges() {
    this._appService.getAllSettings("charges").toPromise().then(result => {
      this.cchargesList = result.data.sort((a,b) =>{
        return (a.subCategory > b.subCategory) ? 1 : ((b.subCategory > a.subCategory) ? -1 : 0);
      });
    }, reject => {
      console.log(reject);
    });
  }

  createccharge() {
    debugger;
    let self = this;
    let countlength = this.cchargesList.filter(function(item) {
      debugger;
      return (item.subCategory == self.ccharges.subCategory && item.consultationType == self.ccharges.consultationType);
    }).length;

    if (!this.ccharges.charges.consultationFee)
      this.toastr.error("Error", "Please enter something in Consultation fee");
    else if (!this.ccharges.charges.serviceCharge)
      this.toastr.error("Error", "Please enter something in Service Charge");
    else if (!this.ccharges.charges.doctorFee)
      this.toastr.error("Error", "Please enter something in Doctor Fee");
    else if (countlength > 0 && !this.ccharges._id)
      this.toastr.error("Error", "Record exists for this Specialization.");
    else {

      if (this.ccharges._id) {
        this.updateCallSplCharges(this.ccharges);
      }
      else {
        this._appService.createChargesSetting(this.ccharges).toPromise().then(result => {
          this.cchargesList = result.data;
          this.toastr.success("Success", "Created Successfully");
          this.ccharges.subCategory = this.specializationList[0]._id;
          this.ccharges.charges = { consultationFee: 0, serviceCharge: 0, doctorFee: 0 };

          this.getListOfCharges();
        }, reject => {
          this.toastr.error("Error", "Oops something bad happend.");
          console.log(reject);
        });
      }
    }
  }

  updateSplCharges(item) {
    this.ccharges._id = item._id;
    this.ccharges.subCategory = item.subCategory;
    this.ccharges.consultationType = item.consultationType;
    this.ccharges.charges = item.charges;
  }

  deleteSplCharges(id) {

  }

  updateCallSplCharges(item) {
    this._appService.updateSplCharges(item).toPromise().then(result => {
      this.toastr.success("Success", "Updated Successfully");
      this.getListOfCharges();
      this.ccharges.charges = { consultationFee: 0, serviceCharge: 0, doctorFee: 0 };
    }, reject => {
      this.toastr.error("Error", "Oops something bad happend.");
      console.log(reject);
    });
  }

}
