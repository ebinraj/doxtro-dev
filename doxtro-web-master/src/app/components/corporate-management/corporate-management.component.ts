import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { AppService } from 'app/services/app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';

@Component({
  selector: 'app-corporate-management',
  changeDetection: ChangeDetectionStrategy.Default,
  templateUrl: './corporate-management.component.html',
  styleUrls: ['./corporate-management.component.css']
})
export class CorporateManagementComponent implements OnInit {

  companyForm: FormGroup;
  paCoupon: FormGroup;
  subscriptionFrm: FormGroup;
  companyList=["Partner","Corporates"];
  industryList = ["IT & Software", "Manufacturing", "Financial Service"];
  companyObj;
  preappliedCouponList = [];
  companyId;
  groups: FormArray;
  groupItems: FormArray;
  subscriptionList = [];
  bsValuestart: Date = new Date();
  bsValueend: Date = new Date();
  mindate: Date;
  minstartdate: Date = new Date("2018/01/01");

  groupbasicobj = {
    groupName: "",
    feePerEmployee: 0,
    consultationPerEmployee: 1,
    allowedFor: []
  };

  constructor(public _fb: FormBuilder, public _appService: AppService, public activatedRoute: ActivatedRoute, public router: Router, public cref : ChangeDetectorRef) {
    this.setSubscriptionForm();
    this.setCompanyForm();
    this.setPaCouponReset();

  }

  setSubscriptionForm() {
    this.subscriptionFrm = this._fb.group({
      _id: '',
      subscriptionId: '',
      subscriptionTag: [''],
      userType: 'corporate',
      subscriptionType: [''],
      price: [''],
      originalPrice : [0],
      startDate: [''],
      endDate: [''],
      description : [''],
      paymentCycle: [''],
      freeGPConsultations: [''],
      totalConsultations: [''],
      groups: this._fb.array([this.createGroup(this.groupbasicobj)]),
      feePerEmployee: 0,
      consultationPerEmployee: 1,
      allowedFor: [],
    });
  }

  setCompanyForm() {
    this.companyForm = this._fb.group({
      _id: [''],
      companyName: ['', Validators.required],
      employeeStrength: [0, Validators.required],
      companyDescription: [""],
      industryType: [this.industryList[0], Validators.required],
      corporatePayType: ["Non-Paying", Validators.required],
      planType: ["Coupon", Validators.required],
      companyType: [this.companyList[0], Validators.required],
      partnerTag:[""],
      groupItems: this._fb.array([this.createGroupItems()])
    });
  }

  setPaCouponReset() {
    this.paCoupon = this._fb.group({
      _id: "",
      couponTag: ["", Validators.required],
      countFrom: [1, Validators.required],
      countTo: [2, Validators.required],
      amount: [0, Validators.required],
      amountType: ["flat", Validators.required],
      discountType: ["discount", Validators.required],
      companyName: [""],
      isCorporate: [true]
    });
  }

  getGroupItemsControlls() {
    return this.companyForm.controls['groupItems'] as FormGroup;
  }

  getGroupControlls() {
    return this.subscriptionFrm.controls['groups'] as FormGroup;
  }

  setMinDate(e: Date = new Date()) {
    if (e != null && e.getDate()) {
      this.mindate = new Date();
      this.mindate.setDate(e.getDate() + 1);
      this.mindate.setMonth(e.getMonth());
      this.mindate.setFullYear(e.getFullYear());
    }
  }


  createGroupItems(value = "Default", disable = false): FormGroup {
    return this._fb.group({
      group: [{ value: value, disabled: disable }, Validators.required]
    });
  }

  createGroup(obj): FormGroup {
    return this._fb.group({
      groupName: [obj.groupName, []],
      feePerEmployee: [obj.feePerEmployee, []],
      consultationPerEmployee: [obj.consultationPerEmployee, []],
      allowedFor: [obj.allowedFor, []]
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params["id"] != 0) {
        this.companyId = params["id"];
        this.getCompanyInfo();
      }
    });

  }

  addGroup(obj = this.groupbasicobj) {
    this.groups = this.subscriptionFrm.get('groups') as FormArray;
    this.groups.push(this.createGroup(obj));
  }

  btnAddGroupItem(value = "Default", disable = false) {
    this.groupItems = this.companyForm.get('groupItems') as FormArray;
    this.groupItems.push(this.createGroupItems(value, disable));
  }

  removeGroupItem(i, item = {}) {
    if (this.groupItems.length && this.groupItems.length > 1) {
      if (Object.keys(item).length == 0) {
        this.groupItems.removeAt(i);
      }
      else {
        if (item["_value"].group == "Default" && (item["_status"] == "DISABLED")) {
          alert("can't delete the default group");
        }
        else {

          if (this.companyId) {

            let body = {
              companyName: this.companyObj.companyName,
              group: item["_value"].group,
              _id: this.companyObj._id
            }

            //send api and check if api says delete then delete 
            this._appService.deleteGroup(body).toPromise().then(result => {
              this.groupItems.removeAt(i);
            }).catch(err => {
              console.log(err);
            });
          }
          else {
            this.groupItems.removeAt(i);
          }
        }
      }
    }
    else {
      alert("You can't delete the last group");
    }
  }
  removeGroupSubscrption(i) {
    if (this.groups.length && this.groups.length > 1)
      this.groups.removeAt(i);
  }

  doCompanyCreateUpdate({ value, valid }) {
    if(value.companyType =='Partner'){
      value.corporatePayType = 'Non-Paying';
      value.planType = 'Coupon';
    }
    else{
      if((value.companyName) || (value.employeeStrength) && (!value.partnerTag)){
      valid = true;
      }
    }
    if (valid) {
      debugger;

      if (value.groupItems) {
        value.groups = value.groupItems.map((item) => {
          return item.group;
        });

        if (this.companyObj) //in case of update
          value.groups = [...value.groups, ...this.companyObj.groups];

        value.groups = value.groups.reduce(function (a, b) { if (a.indexOf(b) < 0) a.push(b); return a; }, []);

      }


      if (!value._id) {
        delete value._id;
        this._appService.createCompanyinfo(value).toPromise().then(result => {
          // this.companyForm.patchValue(result.data);
          // this.companyObj = result.data;

          this.setCompanyForm();

          alert("Company Created Successfully");

          this.router.navigate(["/app/corpomanage/" + result.data._id]);

          // this.companyId == this.companyObj._id;

          //attach subscription plan on page load
          //this.subscriptionFrm.patchValue({ subscriptionType: this.companyObj.planType });

        }).catch(err => {
          alert("some error occcurred : " + JSON.parse(err._body).message);
          console.log(err);
        });
      }
      else {
        this._appService.updateCompanyinfo(value).toPromise().then(result => {
          alert("successfully updated");
          this.setSubscriptionForm();
          this.getCompanyInfo();
        }).catch(err => {
          alert("some error occcurred : " + JSON.parse(err._body).message);
          console.log(err);
        });
      }
    }
    else {
      alert("please fill the correct information");
    }
  }

  //coupon segment
  doUpdateCreateCoupon({ value, valid }) {

    if (valid) {
      //check if the company is already registered or not
      if (this.companyForm.value._id) {

        //if id is not there create coupon
        if (!this.paCoupon.value._id) {
          value.companyName = this.companyObj.companyName;
          delete value._id;
          this._appService.createPreAppliedCoupon(value).toPromise().then(result => {
            this.preappliedCouponList.push(result.data);
            this.setPaCouponReset();
          }).catch(err => {
            console.log(err);
          });
        }
        else {
          this._appService.updatePreAppliedCoupon(value).toPromise().then(result => {
            alert("successfully updated");
            this.getPreAppliedCouponList();
            this.setPaCouponReset();
          }).catch(err => {
            console.log(err);
          });
        }

      }
      else {
        alert("Please create the company first");
      }

    }
    else {
      alert("please fill all the info");
    }

  }

  getPreAppliedCouponList() {
    this._appService.getListPreAppliedCoupon(true, this.companyObj.companyName).toPromise().then(result => {
      this.preappliedCouponList = result.data;
    }).catch(err => {
      console.log(err);
    });
  }

  updateCouponbtn(pa) {
    this.paCoupon.patchValue(pa);
  }

  deleteCoupon(id) {
    this._appService.deletePreAppliedCoupon(id).toPromise().then(result => {
      this.getPreAppliedCouponList();
    }).catch(err => {
      console.log("some err occured");
    });
  }

  //subscription 

  doUpdateCreateSubscription({ value, valid }) {

    if (value.subscriptionType == "Bulk") {
      delete value.groups;
      delete value.feePerEmployee;
      delete value.consultationPerEmployee;
      delete value.allowedFor;
      delete value.paymentCycle;
    }
    else if ((value.subscriptionType == "Subscription" || value.subscriptionType == "Package") && this.companyObj.corporatePayType == 'Non-Paying') {
      delete value.groups;
      delete value.startDate;
      delete value.endDate;

      if (!value.allowedFor)
        valid = false;

    }
    else if (value.subscriptionType == "Subscription" && this.companyObj.corporatePayType == 'Paying') {
      value.groups.forEach(item => {
        if (item.allowedFor.length == 0)
          valid = false;
      });

    }

    if ((value.subscriptionType == "Subscription" || value.subscriptionType == "Bulk") && this.companyObj.corporatePayType == 'Paying') {
      if (value.startDate > value.endDate)
        valid = false;
    }




    if (valid) {
      if (!this.subscriptionFrm.value.subscriptionId) {

        this.subscriptionFrm.value.companyName = this.companyObj.companyName;
        this.subscriptionFrm.value.companyId = this.companyObj._id;

        delete value._id;

        debugger;
        console.log(this.subscriptionFrm.value);

        this._appService.subscriptionCreate(value).toPromise().then(result => {
          //this.subscriptionList.push(result.data);
          //this.getCompanyInfo();
          alert("Subscription Created Successfully");
          // this.setSubscriptionForm();
          location.reload();

        }).catch(err => {
          console.log(err);
          alert("Some error occured");
        });

      }
      else {
        //update subscription

        this._appService.updateSubscription(value).toPromise().then(result => {
          alert("Subscription Updated Successfully");
          // this.setSubscriptionForm();
          // this.getCompanyInfo();

          location.reload();

        }).catch(err => {
          console.log(err);
          alert("Some error occured");
        });


      }
    }
    else {
      alert("Please fill the correct value in the form. Make sure to select allowed for in case of subscription/package");
    }


  }

  getSubscriptionList() {
    this._appService.getSubscriptionList().toPromise().then(result => {
      this.subscriptionList = result.data;
    }).catch(err => {
      console.log(err);
    });
  }

  updateSubScription(ss) {
    this.subscriptionFrm.patchValue(ss);

    if (ss.subscriptionType != "Bulk" && this.companyObj.corporatePayType != 'Non-Paying') {
      ss.groups.forEach(element => {
        this.addGroup(element);
      });
      this.groups.removeAt(0);
    }

  }

  deleteSubscription(id) {
    var r = confirm("Are you sure you want to delete this subscription?");
    if (r == true) {
      this._appService.deleteSubscription(id).toPromise().then(result => {
        this.getCompanyInfo();
      }).catch(err => {
        alert(err);
      });
    }
  }

  //on page load get company info
  getCompanyInfo() {
    this._appService.getCompanyInfo(this.companyId).toPromise().then(result => {
      this.companyObj = result.data;
      this.companyForm.patchValue(this.companyObj);
      debugger;

      this.companyObj.groups.forEach(element => {
        this.btnAddGroupItem(element, true);
      });

      this.removeGroupItem(0);
      this.companyForm.updateValueAndValidity();

      debugger;
      if (this.companyObj.planType == "Coupon")
        this.getPreAppliedCouponList();
      else if (this.companyObj.currentSubscriptions.length > 0 && this.companyObj.corporatePayType == "Paying") {
        this.subscriptionFrm.patchValue(this.companyObj.currentSubscriptions[0]);

        // let sdate = this.companyObj.currentSubscriptions[0].startDate;
        // let edate = this.companyObj.currentSubscriptions[0].endDate;

        // this.subscriptionFrm.patchValue({ startDate: sdate });
        // this.subscriptionFrm.patchValue({ endDate: edate });


        if (this.companyObj.planType != "Bulk") {
          console.log(this.companyObj.currentSubscriptions[0].groups)

          this.companyObj.currentSubscriptions[0].groups.forEach(element => {
            this.addGroup(element);
          });

          this.removeGroupSubscrption(0);
        }

        this.subscriptionFrm.updateValueAndValidity();

      }
      else {
        //attach subscription plan on page load
        this.subscriptionFrm.patchValue({ subscriptionType: this.companyObj.planType });
      }

      //for non paying fill the list

      this.subscriptionList = [];

      if (this.companyObj.corporatePayType == "Non-Paying" && (this.companyObj.planType == "Subscription" || this.companyObj.planType == "Package")) {
        for (let i = 0; i < this.companyObj.currentSubscriptions.length; i++) {
          this.subscriptionList.push(this.companyObj.currentSubscriptions[i]);
        }
      }

      this.cref.detectChanges();

      console.log(this.companyForm.value);
    }).catch(err => {
      console.log(err);
    });
  }


  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select Relatives',
    allSelected: 'All selected',
  };

  relativesList: IMultiSelectOption[] = [
    { id: 'Myself', name: 'Myself' },
    { id: 'Dad', name: 'Dad' },
    { id: 'Mom', name: 'Mom' },
    { id: 'Spouse', name: 'Spouse' },
    { id: 'Son', name: 'Son' },
    { id: 'Daughter', name: 'Daughter' },
    { id: 'Sister', name: 'Sister' },
    { id: 'Brother', name: 'Brother' },
    { id: 'Others', name: 'Others' },
  ];


}
