import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AccountService } from '../../services/account.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  signIn: FormGroup;
  ifFPVisible: boolean = false;
  emailId: string = "";

  constructor(private fb: FormBuilder, private _accountServices: AccountService, private router: Router) { }

  ngOnInit() {
    this.signIn = this.fb.group({
      emailid: ["", Validators.required],
      password: ["", Validators.required]
    });

    localStorage.removeItem("token");
  }

  doSignIn(event) {
    this._accountServices.doSignIn(this.signIn.value).subscribe(result => {
      debugger;
      if (result.data.token) {
        localStorage.setItem('token', result.data.token);
        localStorage.setItem('name', result.data.firstName);
        localStorage.setItem('emailid', result.data.emailid);
        this.router.navigate(['/app/dashboard']);
      }
      else {
        alert("unable to login");
      }
    }, error => {
      debugger;
      alert("unable to login");
    });

  }

  openforgotpassword() {
    this.ifFPVisible = true;
  }

  submitForgotPassword() {

    if (this.emailId) {
      this._accountServices.forgotPassword(this.emailId).toPromise().then((result) => {
        alert("We have sent you the link over email. Kindly check that")
      }, reject => {
        alert("Oops something bad happed");
      });
    }
    else {
      alert("Please enter valid email id");
    }
  }

}
