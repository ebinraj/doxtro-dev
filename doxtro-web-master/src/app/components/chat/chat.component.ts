import { environment } from './../../../environments/environment';
import { AppService } from './../../services/app.service';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { ActivatedRoute } from '@angular/router';
import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import * as _ from 'lodash';
import * as firebase from 'firebase';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  public consultationId: String;
  public messagesList = [];
  public consultation;
  adminMsg: String;
  patientId;
  adminComment;
  suggestionList: any[] = [];
  @ViewChild('content') content: ElementRef;
  @ViewChildren('messages') messages: QueryList<any>;
  @ViewChild('contentSuggestion') contentSuggestion: ElementRef;
  @ViewChildren('messagesSuggestion') messagesSuggestion: QueryList<any>;

  constructor(public route: ActivatedRoute, public db: AngularFireDatabase, public _appService: AppService) { }
  public privateMode = true;

  ngOnInit() {
    this.route.params.subscribe(param => {
      debugger;
      this.consultationId = param["id"];
      this.getMessageList(this.consultationId);
      this.getComments();
    });
    this.consultation = this._appService.getSelectedConsultaion();
    console.log(this.consultation);
  }

  getMessageList(id) {
    this.db.list(environment.consultationkey + '/' + this.consultationId).subscribe(result => {
      this.messagesList = _.sortBy(result, "timeStamp");

      this.messagesList = this.messagesList.filter((item) => {
        return ['alert', 'text', 'userText', 'image', 'docImage', 'prescription', 'userIntro'].indexOf(item.msgType) != -1;
      });

      this.patientId = this.messagesList.filter((item) => {
        return item.from == 'Patient';
      })[0]._id;

    });
    //this.getreadcount();

  }

  getreadcount() {

    //   // let considarr = ["59d7290391eaff48a0a8b965", this.consultationId]

    //   // let msgref = firebase.database().ref('Consultations/');
    //   // msgref..orderByChild("status").equalTo("unread").on("value", function(ysnapshot) {
    //   //   var unread = ysnapshot.numChildren();
    //   //   console.log("count",unread);
    // });

  }

  scrollToBottom = (): void => {

    try {
      this.content.nativeElement.scrollTop = this.content.nativeElement.scrollHeight;
    } catch (err) { console.log(err); }
  }

  scrollToBottom2 = (): void => {

    try {
      this.contentSuggestion.nativeElement.scrollTop = this.contentSuggestion.nativeElement.scrollHeight;
    } catch (err) { console.log(err); }
  }

  onPrivateModeChange(input) {
    debugger;
    this.privateMode = input.target.checked;
  }

  onRightClick() {
    return false;
  }

  ngAfterViewInit() {
    this.messages.changes.subscribe(this.scrollToBottom);
    this.messagesSuggestion.changes.subscribe(this.scrollToBottom2)
    setTimeout(() => {
      this.scrollToBottom();
    }, 2000);
  }

  sendMessage() {

    if (this.adminMsg) {
      let msg = {
        _id: localStorage.getItem("id"),
        from: "Admin",
        sender: "Doxtro Admin",
        receiver: this.patientId,
        status: "Sent",
        msgType: "alert",
        data: this.adminMsg
      }

      let self = this;
      let chatObj = this.db.database.ref(environment.consultationkey + '/' + this.consultationId + "/Admin_" + new Date().getTime());
      chatObj.once('value').then(function (snapshot) {
        msg["timeStamp"] = firebase.database.ServerValue.TIMESTAMP;
        chatObj.update(msg).then(result => {
          self.adminMsg = "";
        }, reject => {
        }).catch(errr => {
          console.log(errr);
        });
      }).catch(err => {
        console.log(err);
      });
    }
    else {
      alert("Please write something");
    }

  }

  // comment section

  getComments() {
    this._appService.getConsultationComments(this.consultationId).subscribe(result => {
      this.suggestionList = result.data;
    });
  }

  createConsultationComments() {

    let username  = localStorage.getItem("name")

    if (this.adminComment) {
      this.suggestionList.push({ consultationId: this.consultationId, commentText: this.adminComment, commentBy: username });
      this._appService.createConsultationComments({ consultationId: this.consultationId, commentText: this.adminComment, commentBy:username }).toPromise().then(result => {
        this.adminComment = "";
      });
    }
    else {
      alert("Please write something in textbox");
    }
  }

}
