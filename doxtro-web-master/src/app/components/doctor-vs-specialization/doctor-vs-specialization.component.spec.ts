import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorVsSpecializationComponent } from './doctor-vs-specialization.component';

describe('DoctorVsSpecializationComponent', () => {
  let component: DoctorVsSpecializationComponent;
  let fixture: ComponentFixture<DoctorVsSpecializationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorVsSpecializationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorVsSpecializationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
