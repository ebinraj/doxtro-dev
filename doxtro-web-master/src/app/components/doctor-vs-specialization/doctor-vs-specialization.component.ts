import { AppService } from 'app/services/app.service';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';

declare let d3: any;

@Component({
  selector: 'app-doctor-vs-specialization',
  templateUrl: './doctor-vs-specialization.component.html',
  styleUrls: ['./doctor-vs-specialization.component.css', '../../../../node_modules/nvd3/build/nv.d3.css'],
  encapsulation: ViewEncapsulation.None
})
export class DoctorVsSpecializationComponent implements OnInit {

  options;
  data;
  chartType;
  totalDoctors = 0;

  @ViewChild('docvsspecs') docvsspecs;

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

  testmethod(){
    debugger;

    this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
        margin: {
          top: 20,
          right: 20,
          bottom: 165,
          left: 65
        },
        useInteractiveGuideline: true,
        duration: 500,
        xAxis: {
          axisLabel: 'Specialization',
          axisLabelDistance: 20,
          rotateLabels: -35,
          tickFormat: function (d) {
            return d;
          }
        },
        yAxis: {
          axisLabel: 'Doctor Count',
          axisLabelDistance: 0,
          tickFormat: function (d) {
            return d.toFixed(0);
          }
        }
      }
    }

    this.data = [{
      values: [{ x: "General Physician", y: 0 }],
      key: 'Doctor vs Specialization',
      color: '#ff7f0e'
    }];

    this._appService.getDocVsSpecs().toPromise().then((result) => {

      this.totalDoctors =0;
      if (result.data) {
        let graphdata = [];
        result.data.forEach(element => {
          this.totalDoctors +=element.count;
          graphdata.push({x:element.specialization, y:element.count});
        });

        this.data = [{
          values: graphdata,
          key: 'Doctor vs Specialization',
          color: '#ff7f0e'
        }];

        this.docvsspecs.chart.update();
      }
      else {
        alert("No data found");
      }

    }, reject => {
      this.data = [{
        values: [{ x: "General Physician", y: 3 }, { x: "Dermatologist", y: 5 }, { x: "Pediatrician", y: 2 }, { x: "Sexologist", y: 10 },],
        key: 'Doctor vs Specialization',
        color: '#ff7f0e'
      }];
      this.docvsspecs.chart.update();

      console.log(reject);
    });
    
  }

}
