import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';
import { AppService } from '../../services/app.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { PatientProfileComponent } from '../patient-profile/patient-profile.component';
import { PatientEditProfileComponent } from '../patient-edit-profile/patient-edit-profile.component';


@Component({
  selector: 'app-patients-list',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients-list.component.css']
})
export class PatientsListComponent implements OnInit {
  public modalRef: BsModalRef;
  pageCount: any = 10;
  currentPage: any = 1;
  searchTerm: String = "";
  sortField: String = "accountActivationTime-desc";
  paginationshowcount: any[] = [];
  dataList: any[];
  totalpatients : Number;
  skip = 0;

  constructor(private _appService: AppService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getData("", this.pageCount, 0, this.sortField);
  }

  doSearch(st) {
    this.getData(st, this.pageCount, 0, this.sortField);
  }

  onPageCountChange() {
    let pscs = Math.ceil(this.dataList.length / (Number)(this.pageCount));
    if (pscs < this.currentPage)
      this.currentPage = 1;

      this.skip  =this.currentPage === 1 ? 0 : (Number(this.currentPage) * Number(this.pageCount));

    this.getData(this.searchTerm, this.pageCount,this.skip , this.sortField);
  }

  getData(search, pagecount, skip, sortField) {
    this._appService.getPatientsList(search, pagecount, skip, sortField).subscribe(result => {
      this.dataList = result.data;
      this.totalpatients = result.totalCount;
      let pscs = Math.ceil(result.totalCount / (Number)(this.pageCount));
      this.paginationshowcount = Array.from(Array(pscs), (x, i) => i + 1);
    }, error => {
      console.log(error)
    });
  }

  doPrevious() {
    if (this.currentPage != 1) {
      this.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 2) * Number(this.pageCount));
      this.getData(this.searchTerm, this.pageCount,this.skip , this.sortField);
      this.currentPage = (Number)(this.currentPage) - 1;
    }
  }

  doNext() {
    if (this.currentPage != this.paginationshowcount[this.paginationshowcount.length - 1]) {
      this.skip = (Number(this.currentPage) * Number(this.pageCount));
      this.getData(this.searchTerm, this.pageCount,this.skip , this.sortField);
      this.currentPage = (Number)(this.currentPage) + 1;
    }
  }

  doChangePage(pnum) {
    this.skip = ((pnum - 1) * Number(this.pageCount));
    this.getData(this.searchTerm, this.pageCount,this.skip , this.sortField);
    this.currentPage = pnum;
  }

  doChangeSort(sortfield) {
    let sf = this.sortField.split('-');
    if (sortfield == sf[0]) {
      if (sf[1] == "asc") {
        this.sortField = sortfield + "-desc";
      }
      else {
        this.sortField = sortfield + "-asc";
      }
    }
    else {
      this.sortField = sortfield + "-asc";
    }

    this.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 1) * Number(this.pageCount));

    this.getData(this.searchTerm, this.pageCount,this.skip , this.sortField);

  }

  getDetails(id) {
    this._appService.setSelectedDocId(id);
    this.modalService.config.class = "modal-lg";
    this.modalRef = this.modalService.show(PatientProfileComponent);

  }

  editProfile(id) {
    this._appService.getPatientDetails(id).subscribe(result => {
      if (result.data) {
        this._appService.setSelectedDetails(result.data);
        this.modalService.config.class = "modal-lg";
        this.modalRef = this.modalService.show(PatientEditProfileComponent);
      } else { console.log("no data found") }
    })
  }

   block(i, id){
    this._appService.changeAccountStatus({_id : id, tag : 'patient', accountStatus : 'blocked'}).subscribe(result => {
      this.dataList[i].accountStatus = 'blocked';
      alert("Blocked successfully");
    });
  }

   unblock(i, id){
    this._appService.changeAccountStatus({_id : id, tag : 'patient', accountStatus : 'inactive'}).subscribe(result => {
      this.dataList[i].accountStatus = 'inactive';
      alert("Unblocked successfully");
    });
  }

  remove(index, id) {
    this._appService.remove({_id : id, tag : 'patient'}).subscribe(result => {
      this.dataList.splice(index, 1);
      alert(result.message);
    });
  }

  getColor(status){
    if(status=="active")
      return "badge-success";
    else if(status=="inactive")
      return "badge-warning";
    else if(status=="deactivated")
      return "badge-danger";
    else if(status=="blocked")
      return "badge-info";


  }

}
