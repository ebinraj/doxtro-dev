import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthFeedContainerComponent } from './health-feed-container.component';

describe('HealthFeedContainerComponent', () => {
  let component: HealthFeedContainerComponent;
  let fixture: ComponentFixture<HealthFeedContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthFeedContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthFeedContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
