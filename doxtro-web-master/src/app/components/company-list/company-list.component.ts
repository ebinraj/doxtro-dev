import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {

  pageCount: any = 10;
  currentPage: any = 1;
  searchTerm: string = "";
  sortField: String = "createdAt-desc";
  paginationshowcount: any[] = [];
  dataList: any[];
  totalComap: Number;

  public companyObj = {
    pageCount: this.pageCount,
    sortField: this.sortField,
    skip: 0,
    searchTerm: ""
  };

  constructor(private _appService: AppService, public route: ActivatedRoute, public router : Router) { }

  ngOnInit() {

    let localobj = localStorage.getItem("companyObj");
    if (localobj) {
      this.companyObj = JSON.parse(localobj);
      this.searchTerm = this.companyObj.searchTerm;
      this.pageCount = this.companyObj.pageCount;
      this.getData();
    }
    else {
      this.getData();
    }

  }

  doSearch(st) {

    this.companyObj = {
      pageCount: this.pageCount,
      sortField: this.sortField,
      skip: 0,
      searchTerm: st
    };

    this.getData();
    this.updateCompanyObj(this.companyObj);
  }

  onPageCountChange() {
    let pscs = Math.ceil(this.dataList.length / (Number)(this.pageCount));
    if (pscs < this.currentPage)
      this.currentPage = 1;

    this.companyObj.searchTerm = this.searchTerm;
    this.companyObj.pageCount = this.pageCount;
    this.companyObj.skip = this.currentPage === 1 ? 0 : (Number(this.currentPage) * Number(this.pageCount));
    this.companyObj.sortField = this.sortField;
    this.getData();
    this.updateCompanyObj(this.companyObj);
  }

  getData() {
    debugger;
    let docObj = this.companyObj;

    this._appService.getcompanyList(docObj.searchTerm, docObj.pageCount, docObj.skip, docObj.sortField).subscribe(result => {
      this.dataList = result.data;
      this.totalComap = result.totalCount;
      console.log(this.dataList);
      let pscs = Math.ceil(result.totalCount / (Number)(this.pageCount));
      this.paginationshowcount = Array.from(Array(pscs), (x, i) => i + 1);
    });
  }

  doPrevious() {
    if (this.currentPage != 1) {

      this.companyObj.searchTerm = this.searchTerm;
      this.companyObj.pageCount = this.pageCount;
      this.companyObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 2) * Number(this.pageCount));
      this.companyObj.sortField = this.sortField;

      this.currentPage = (Number)(this.currentPage) - 1;
      this.getData();
      this.updateCompanyObj(this.companyObj);
    }
  }

  doNext() {
    if (this.currentPage != this.paginationshowcount[this.paginationshowcount.length - 1]) {

      this.companyObj.searchTerm = this.searchTerm;
      this.companyObj.pageCount = this.pageCount;
      this.companyObj.skip = (Number(this.currentPage) * Number(this.pageCount));
      this.companyObj.sortField = this.sortField;

      this.currentPage = (Number)(this.currentPage) + 1;
      this.getData();
      this.updateCompanyObj(this.companyObj);
    }
  }

  doChangePage(pnum) {

    this.companyObj.searchTerm = this.searchTerm;
    this.companyObj.pageCount = this.pageCount;
    this.companyObj.skip = ((pnum - 1) * Number(this.pageCount));
    this.companyObj.sortField = this.sortField;

    this.currentPage = pnum;
    this.getData();
    this.updateCompanyObj(this.companyObj);
  }

  doChangeSort(sortfield) {
    let sf = this.sortField.split('-');
    if (sortfield == sf[0]) {
      if (sf[1] == "asc") {
        this.sortField = sortfield + "-desc";
      }
      else {
        this.sortField = sortfield + "-asc";
      }
    }
    else {
      this.sortField = sortfield + "-asc";
    }

    this.companyObj.searchTerm = this.searchTerm;
    this.companyObj.pageCount = this.pageCount;
    this.companyObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 1) * Number(this.pageCount));
    this.companyObj.sortField = this.sortField;

    this.getData();
    this.updateCompanyObj(this.companyObj);

  }

  updateCompanyObj(obj) {
    localStorage.setItem("companyObj", JSON.stringify(obj));
  }

  deletecompany(id) {
    var r = confirm("Are you sure you want to delete this company?");
    if (r == true) {
      this._appService.deletecompany(id).toPromise().then(result => {
        this.getData();
        alert("company deleted");
      }).catch(err => {
        console.log(err);
      });
    }
  }

  getEmployeeList(cname){
    this.router.navigate(["/app/employee-list/" + cname]);
  }

}
