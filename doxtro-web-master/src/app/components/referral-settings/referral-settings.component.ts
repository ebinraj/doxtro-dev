import { AppService } from 'app/services/app.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-referral-settings',
  templateUrl: './referral-settings.component.html',
  styleUrls: ['./referral-settings.component.css']
})
export class ReferralSettingsComponent implements OnInit {

  ref : any = {
    "category": "referrals",
  }

  constructor(public _appService: AppService) { }

  ngOnInit() {
    this.doGetReferralSettings();
  }

  doCreateUpdate() {
    debugger;
    this._appService.updateSplCharges(this.ref).toPromise().then(result =>{
      alert("Successfully updated");
    }).catch(err => {
      alert("Some error occured");
      console.log(err);
    });
  }

  doGetReferralSettings() {
    this._appService.getAllSettings(this.ref.category).toPromise().then(result => {
      console.log(result.data[0]);
      this.ref = result.data[0];
    }).catch(err => {
      console.log(err);
    });
  }

}
