import { ToastrService } from 'ngx-toastr';
import { AppService } from './../../services/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-coupon',
  templateUrl: './create-coupon.component.html',
  styleUrls: ['./create-coupon.component.css']
})
export class CreateCouponComponent implements OnInit {
  couponform: FormGroup;
  couponId: string = "";
  couponList: any[];
  couponTypeList = ["cashback", "flat", "percentage"];
  packageAvailable : any[] = [];
  
  constructor(public route: ActivatedRoute, public _fb: FormBuilder, public _appService: AppService, public toastr: ToastrService) {
    this.couponform = this._fb.group({
      _id: [],
      couponTag: ["", Validators.required],
      amount: [0, Validators.required],
      validFrom: [""],
      validTo: [""],
      isForFirstConsultation: [false, Validators.required],
      iterationAllowed: [1, Validators.required],
      range: [, Validators.required],
      type: ["", Validators.required],
      applicableType: ["consultation", Validators.required],
      packageId:[""],
      couponGenderSpecific: ["All", Validators.required]
    });

  }

  ngOnInit() {
    this.getCouponList();
    this.getPackageAvailable();
    this.route.params.subscribe(param => {
      this.couponId = param["id"];
    });
    this.couponform.patchValue({ type: this.couponTypeList[1] });
  }

  doCreateUpdate({ value, valid }) {
    debugger;
    console.log(value);
    if (valid) {

      value.validFrom = value.range[0];
      value.validTo = value.range[1];
      if(value.applicableType == "consultation"){
        delete value.packageId;
      }
      else{
        value.packageName = value.packageId.subscriptionTag;
        if(value.packageName == null || value.packageName == undefined) {
          alert("please select the package");
        }
        value.packageId = value.packageId._id;
      }
      if (!value._id) {
        this._appService.createCoupon(value).subscribe(result => {
          this.toastr.success("Success", "Coupon created successfully");
          this.getCouponList();
        }, err => {
          this.toastr.error("Error", "Oops something bad happend.")
        });
      }
      else {
        this._appService.updateCoupon(value, value._id).subscribe(result => {
          this.toastr.success("Success", "Coupon updated successfully");
          this.getCouponList();
        }, err => {
          this.toastr.error("Error", "Oops something bad happend.")
        });
      }
    }
    else {
      alert("please fill the complete form");
    }
  }

  getCouponList() {
    this._appService.getCouponList().toPromise().then(res => {
      this.couponList = res.data;
    }, rej => {
      this.toastr.error("Error", "Unable to fetch the coupon list");
    });
  }

  updateCouponbtn(data) {
    data.range = [new Date(data.validFrom), new Date(data.validTo)];
    console.log(data);
    if(data.applicableType =="package"){
        let index = this.packageAvailable.findIndex(x => x._id == data.packageId);
        data.packageId = this.packageAvailable[index];
      }
    this.couponform.patchValue(data);
   }

  deleteCoupon(id) {
    var r = confirm("Are you sure you want to delete this coupon?");
    if (r == true) {

      this._appService.deleteCoupon(id).toPromise().then(res => {
        this.toastr.success("Success", "Coupon deleted successfully");

        this.couponList = this.couponList.filter(function (item) {
          return item._id != id;
        });

      }, rej => {
        this.toastr.error("Error", "Unable to delete the coupon");
      });
    }
  }

  getPackageAvailable(){
    this._appService.getSubscriptionList("?userType=retail").toPromise().then(result =>{
      this.packageAvailable = result.data;
      this.packageAvailable = this.packageAvailable.filter(item=>item.subscriptionType =='Package');
    }).catch(error =>{
      console.log(error);
    });
  }
}
