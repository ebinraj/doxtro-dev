import { AppService } from '../../services/app.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

declare var c3: any;
declare var Maplace: any;
declare var Noty: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './dashboard.component.css',
    '../../../assets/fonts/weather/css/weather-icons.min.css',
    '../../../assets/libs/c3js/c3.min.css',
    '../../../assets/libs/noty/noty.css',
    '../../../assets/styles/widgets/payment.min.css',
    '../../../assets/styles/widgets/panels.min.css',
    '../../../assets/styles/dashboard/tabbed-sidebar.min.css'
  ]
})
export class DashboardComponent implements OnInit, OnDestroy {

  public newRequest = 0;
  public unassignedConsultation = 0;
  public inActive = 0;
  public ongoingConsultation = 0;
  public activityLogList = [];
  public onlineDoctors = [];

  public consultationCountSubscriber : Subscription;
  public activityLogSubscriber : Subscription;
  public onlineDoctorsSubscriber : Subscription;

  constructor(public _appService: AppService) { }

  ngOnInit() {

    this.consultationCountSubscriber = this._appService.getConsultationCountforDashboard().subscribe(result => {
      debugger;
      this.initCounts();

      this.newRequest = result.data["new"];
      this.unassignedConsultation = result.data["unassigned"];
      this.ongoingConsultation = result.data["ongoing"];
      this.inActive = result.data["inactive"];

    });

    this.activityLogSubscriber = this._appService.getActivityLog().subscribe(result => {
      this.activityLogList = result.data;
    });

    this.onlineDoctorsSubscriber =  this._appService.getOnlineDoctorList().subscribe(result => {
      this.onlineDoctors = result.data;
    })

  }

  initCounts() {
    this.inActive = 0;
    this.newRequest = 0;
    this.ongoingConsultation = 0;
    this.unassignedConsultation = 0;
  }

  ngOnDestroy(): void {
    this.activityLogSubscriber.unsubscribe();
    this.consultationCountSubscriber.unsubscribe();
    this.onlineDoctorsSubscriber.unsubscribe();
  }
}
