import { AppService } from 'app/services/app.service';
import { Component, OnInit } from '@angular/core';
declare let FB: any;

@Component({
  selector: 'app-news-feed-list',
  templateUrl: './news-feed-list.component.html',
  styleUrls: ['./news-feed-list.component.css']
})
export class NewsFeedListComponent implements OnInit {
  healthFeedList : any[] = [];

  pageCount: any = 10;
  currentPage: any = 1;
  search: string = "";
  paginationshowcount: any[] = [];
  dataList: any[];
  totalArticles = 0;

  public articleObj = {
    pageCount: this.pageCount,
    skip: 0,
    search: ""
  }


  constructor(public _appService : AppService) { }

  ngOnInit() {

    let localobj = localStorage.getItem("doctorObj");
    if (localobj) {
      this.articleObj = JSON.parse(localobj);
      this.search = this.articleObj.search;
      this.pageCount = this.articleObj.pageCount;
      this.getData();
    }
    else {
      this.getData();
    }

    this._appService.getAllHealthFeed().toPromise().then(result =>{
      console.log(result);
      this.healthFeedList = result.data;

      setTimeout(() => { FB.XFBML.parse(); });
    }).catch(err =>{
      console.log(err);
    });
  }

  doSearch(st) {

    this.articleObj = {
      pageCount: this.pageCount,
      skip: 0,
      search: st
    };

    this.getData();
    this.updateObj(this.articleObj);
  }

  onPageCountChange() {
    let pscs = Math.ceil(this.dataList.length / (Number)(this.pageCount));
    if (pscs < this.currentPage)
      this.currentPage = 1;

    this.articleObj.search = this.search;
    this.articleObj.pageCount = this.pageCount;
    this.articleObj.skip = this.currentPage === 1 ? 0 : (Number(this.currentPage) * Number(this.pageCount));
    this.getData();
    this.updateObj(this.articleObj);
  }

  getData() {

    let artObj = this.articleObj;

    this._appService.getAllHealthFeed(artObj.search, artObj.pageCount, artObj.skip).subscribe(result => {
      this.dataList = result.data;
      this.totalArticles = result.totalCount;
      
      let pscs = Math.ceil(result.totalCount / (Number)(this.pageCount));
      this.paginationshowcount = Array.from(Array(pscs), (x, i) => i + 1);
    });
  }

  doPrevious() {
    if (this.currentPage != 1) {

      this.articleObj.search = this.search;
      this.articleObj.pageCount = this.pageCount;
      this.articleObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 2) * Number(this.pageCount));

      this.currentPage = (Number)(this.currentPage) - 1;
      this.getData();
      this.updateObj(this.articleObj);
    }
  }

  doNext() {
    if (this.currentPage != this.paginationshowcount[this.paginationshowcount.length - 1]) {

      this.articleObj.search = this.search;
      this.articleObj.pageCount = this.pageCount;
      this.articleObj.skip = (Number(this.currentPage) * Number(this.pageCount));

      this.currentPage = (Number)(this.currentPage) + 1;
      this.getData();
      this.updateObj(this.articleObj);
    }
  }

  doChangePage(pnum) {

    this.articleObj.search = this.search;
    this.articleObj.pageCount = this.pageCount;
    this.articleObj.skip = ((pnum - 1) * Number(this.pageCount));

    this.currentPage = pnum;
    this.getData();
    this.updateObj(this.articleObj);
  }

  updateObj(obj){
    localStorage.setItem("articleObj",JSON.stringify(obj));
  }

  getVideoUrl(item) {
    return "https://www.facebook.com/facebook/videos/" + item;
  }

}
