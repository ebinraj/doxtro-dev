import { Component, OnInit, ElementRef } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-document-verification',
  templateUrl: './document-verification.component.html',
  styleUrls: ['./document-verification.component.css']
})
export class DocumentVerificationComponent implements OnInit {

  doctorId : String;
  documents : any[];

  constructor(public modalRef: BsModalRef, private _appService: AppService, public element: ElementRef) { }

  ngOnInit() {
    this.doctorId = this._appService.getSelectedDocId();
    this.documents = this._appService.getSelectedDocDocuments();
  }

  doVerifyDoc(docId){
    debugger;
    let body = {};
    body["_id"] = this.doctorId;
    body["status"] = this.element.nativeElement.getElementsByClassName(docId+'-status')[0].value;
    body["reason"] = this.element.nativeElement.getElementsByClassName(docId + '-reason')[0].value;
    body["docId"] = docId;

    let self = this;
    this._appService.verifyDocuments(body).subscribe(result =>{
      self.updateDocument(result.data);
    });
  }

  updateDocument(obj){
    let newobj = this.documents;
    for(let i=0; i<this.documents.length;i++){
      if(this.documents[i]._id == obj.docId){
        newobj[i].status = obj.status;
        newobj[i].reason = obj.reason;
        newobj[i]._id = obj.docId;
      }
    }

    this.documents = newobj;
  }
}
