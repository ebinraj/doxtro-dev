import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreappliedCouponComponent } from './preapplied-coupon.component';

describe('PreappliedCouponComponent', () => {
  let component: PreappliedCouponComponent;
  let fixture: ComponentFixture<PreappliedCouponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreappliedCouponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreappliedCouponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
