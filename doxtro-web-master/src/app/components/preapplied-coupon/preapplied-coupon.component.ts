import { AppService } from '../../services/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preapplied-coupon',
  templateUrl: './preapplied-coupon.component.html',
  styleUrls: ['./preapplied-coupon.component.css']
})
export class PreappliedCouponComponent implements OnInit {
  paCoupon: FormGroup
  couponList: any[] = [];

  constructor(public fb: FormBuilder, public _appService: AppService) {
    this.paCoupon = this.fb.group({
      _id: "",
      couponTag: ["", Validators.required],
      countFrom: [1, Validators.required],
      countTo: [2, Validators.required],
      amount: [0, Validators.required],
      amountType: ["flat", Validators.required],
      discountType: ["discount", Validators.required]
    });
  }

  ngOnInit() {
    this.getCouponList();
  }

  doUpdate({ value, valid }) {

    if (valid) {
      let te = this.couponList.filter((item) => {
        if (item.couponTag != value.couponTag) {
          let fr = (item.countFrom <= value.countFrom && item.countTo >= value.countFrom);
          let sr = (item.countFrom <= value.countTo && item.countTo >= value.countTo);
          return fr || sr;
        }
      });

      if (te.length > 0)
        alert("The counsultation count range is not valid");
      else {

        let method = "";

        if (!value._id){
          method = "createPreAppliedCoupon";
          delete value._id;
        }
        else
          method = "updatePreAppliedCoupon"

        this._appService[method](value).subscribe(result => {

          if (method == "createPreAppliedCoupon")
            this.couponList.push(result.data);
          else
            this.getCouponList();

          this.paCoupon.reset();
        });

      }
    }
    else {
      alert("form is invalid");
    }
  }

  deleteCoupon(id) {
    this._appService.deletePreAppliedCoupon(id).toPromise().then(result => {
      this.getCouponList();
    });
  }

  getCouponList() {
    this._appService.getListPreAppliedCoupon().toPromise().then(result => {
      this.couponList = result.data;
    });
  }

  updateCouponbtn(cp) {
    this.paCoupon.patchValue(cp);
  }

}
