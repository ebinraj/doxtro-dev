import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultationsVsTimeComponent } from './consultations-vs-time.component';

describe('ConsultationsVsTimeComponent', () => {
  let component: ConsultationsVsTimeComponent;
  let fixture: ComponentFixture<ConsultationsVsTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultationsVsTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultationsVsTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
