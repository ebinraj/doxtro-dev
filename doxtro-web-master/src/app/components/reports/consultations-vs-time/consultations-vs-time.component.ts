import { AppService } from './../../../services/app.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';

declare let d3: any;

@Component({
  selector: 'app-consultations-vs-time',
  templateUrl: './consultations-vs-time.component.html',
  styleUrls: ['./consultations-vs-time.component.css']
})
export class ConsultationsVsTimeComponent implements OnInit {

  options;
  data;
  chartType;
  specializationList = [];
  startdate;
  endDate;
  selectedSpecializations = [];
  completedata = [];
  totalConsultations = 0;

  @ViewChild('consvstime') consvstime;

  constructor(public _appService: AppService) { }

  ngOnInit() {
    this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
        margin: {
          top: 20,
          right: 20,
          bottom: 165,
          left: 65
        },
        useInteractiveGuideline: true,
        duration: 500,
        xAxis: {
          axisLabel: 'Time Slot',
          axisLabelDistance: 20,
          rotateLabels: -35,
          tickFormat: function (d) {
            return d + ":00";
          }
        },
        yAxis: {
          axisLabel: 'Consultations Count',
          axisLabelDistance: 0,
          tickFormat: function (d) {
            if ( (d * 10) % 10 === 0 ) {
              return d3.format('.0f')(d);
            }
            else {
              return '';
            }
          },
        }
      }
    
    }

   
    this.getSpeclizationList();

    this.data = [{
      values: [{ x: "00.00", y: 0 }],
      key: 'Consultations vs Time',
      color: '#ff7f0e'
    }];
  }

  getReport(e) {
    debugger;
    if (e) {
      this.startdate = e[0];
      this.endDate = e[1];
      if(this.selectedSpecializations.length ===0)
        this.selectedSpecializations = this.completedata;

      this._appService.getCosultationVsTime({ startDate: e[0], endDate : e[1], 'specializationCategory': this.selectedSpecializations }).toPromise().then(result => {
        console.log(result);

        if (result.data) {
          this.displayGraphData(result);
        }


      }).catch(err => {
        console.log(err);
      });
    }
  }

  getSpeclizationList() {
    this._appService.getAllSpecialization().toPromise().then(result => {
      console.log(result);


      let optspecs = [];

      result.data.forEach(element => {
        let obj = { id: element._id, name: element._id };
        optspecs.push(obj);
        this.completedata.push(element._id);
      });


      this.specializationList = optspecs;
    }, reject => {
      console.log(reject);
    });
  }

  onSpecsChange(e) {

    if(this.selectedSpecializations.length ===0)
        this.selectedSpecializations = this.completedata;

    this._appService.getCosultationVsTime({ startDate: this.startdate, endDate : this.endDate, 'specializationCategory': this.selectedSpecializations }).toPromise().then(result => {
      console.log(result);

      if (result.data) {
        this.displayGraphData(result);
      }


    }).catch(err => {
      console.log(err);
    });
  }

  displayGraphData(result) {
    let graphdata = [];

    const ordered = {};
    let keys = Object.keys(result.data).map(Number);

    console.log(keys);

    this.totalConsultations = 0;
    for (let i = 0; i < 24; i++) {
      if (keys.indexOf(i) > -1) {
        this.totalConsultations += parseInt(result.data[i > 9 ? i : "0" + i]);
        graphdata.push({ x: i.toString(), y: parseInt(result.data[i > 9 ? i : "0" + i]) });
      }
      else {
        graphdata.push({ x: i.toString(), y: 0 });
      }
    }

    console.log(graphdata);



    this.data = [{
      values: graphdata,
      key: 'Consultations vs Time',
      color: '#ff7f0e'
    }];

    this.consvstime.chart.update();
  }

  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    autoUnselect: true,
    showCheckAll : true,
    showUncheckAll : true
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };

}
