import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AppService } from 'app/services/app.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';

@Component({
  selector: 'app-package-vs-coupon',
  templateUrl: './package-vs-coupon.component.html',
  styleUrls: ['./package-vs-coupon.component.css']
})
export class PackageVsCouponComponent implements OnInit {

  constructor(public _appService: AppService, public _fb: FormBuilder) { }

  packageCouponReportForm = {
    packageId : [],
    range : {}
  };
  packageList: any[] = [];
  reportrows: any[] = [];
  reportCount: any;
  selectedPackageId : string = "";
  additionalDetails:any;
  additionalDetailsKeys: any = [];

  ngOnInit() {
    this.getPackageList();
  }

  changeInRange(e){
    this.reportrows = [];
    this.reportCount = 0;
  }

  changeInPackage(e){
    this.reportrows = [];
    this.reportCount = 0;
    this.selectedPackageId = e[0];
  }

  getPackageList(){
    this._appService.getSubscriptionList("?userType=retail").toPromise().then(result =>{
      let subscriptions = result.data;
      subscriptions = subscriptions.filter(item=>item.subscriptionType =='Package');
      for(let i = 0; i < subscriptions.length; i++){
        this.packageList.push({
          "id" : subscriptions[i]._id,
          "name" : subscriptions[i].subscriptionTag
        })
      }
    }).catch(error =>{
      console.log(error);
    });
  }

  getReport(){
    if(!this.selectedPackageId){
      alert("Package not selected!");
      return;
    }
    if(!this.packageCouponReportForm.range[0] || !this.packageCouponReportForm.range[1]){
      alert("Date range not selected!");
      return;
    }
    if(this.selectedPackageId && this.packageCouponReportForm.range[0] && this.packageCouponReportForm.range[1]){
      let body = {
        startDate : this.packageCouponReportForm.range[0],
        endDate : this.packageCouponReportForm.range[1],
        packageId : this.selectedPackageId
      }
      this._appService.getPackageCouponReport(body).toPromise().then((result) => {
        if (result.data.length > 0) {
          this.reportrows = result.data;
          this.reportCount = result.count;
          this.additionalDetails = result.additionalDetails;
          this.additionalDetailsKeys = Object.keys(this.additionalDetails);
        }else{
          alert("No records found!");
        }
      }, reject => {
        console.log(reject);
      });
    }
  }

  downloadReport() {
    let value = this.packageCouponReportForm;
    let body = {
      startDate : value.range[0],
      endDate : value.range[1],
      packageId : this.selectedPackageId,
      type: "csv",
      report: "package-coupon"
    }

    this._appService.downloadReports(body).toPromise().then((result) => {
      window.open(result.data.url, '_blank');
    }, reject => {
      console.log(reject);
    });
  }

  constructDate(date){
    let date1 = new Date(date);
    return date1.getDate() + "/" + (date1.getMonth()+1) + "/" + date1.getFullYear() + "  " + date1.getHours() + ":" + date1.getMinutes();
  }

  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    selectionLimit: 1,
    autoUnselect: true
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };

}
