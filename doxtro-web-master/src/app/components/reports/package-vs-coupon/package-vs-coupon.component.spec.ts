import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageVsCouponComponent } from './package-vs-coupon.component';

describe('PackageVsCouponComponent', () => {
  let component: PackageVsCouponComponent;
  let fixture: ComponentFixture<PackageVsCouponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageVsCouponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageVsCouponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
