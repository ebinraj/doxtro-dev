import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersVsCorporatesComponent } from './partners-vs-corporates.component';

describe('PartnersVsCorporatesComponent', () => {
  let component: PartnersVsCorporatesComponent;
  let fixture: ComponentFixture<PartnersVsCorporatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnersVsCorporatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersVsCorporatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
