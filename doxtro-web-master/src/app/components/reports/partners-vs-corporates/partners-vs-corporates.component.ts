import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AppService } from 'app/services/app.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';

@Component({
  selector: 'app-partners-vs-corporates',
  templateUrl: './partners-vs-corporates.component.html',
  styleUrls: ['./partners-vs-corporates.component.css']
})
export class PartnersVsCorporatesComponent implements OnInit {

  constructor(public _appService: AppService, public _fb: FormBuilder) { }

  registrationReportForm = {
    companyType : [],
    range : {},
    companyName : []
  };
  companyTypeList = [{
    "id" : "Partner",
    "name" : "Partner"
  },
  {
    "id" : "Corporates",
    "name" : "Corporates"
  }];
  companyList: any[] = [];
  reportrows: any[] = [];
  reportCount: any;
  selectedCompanyType : string = "";
  selectedCompanyName : string = "";

  ngOnInit() {
  }


  changeInRange(e){
    this.companyList = [];
    this.reportrows = [];
    this.reportCount = 0;
    this.getCompanyList();
    this.selectedCompanyName = "";
  }

  changeInCompanyType(e){
    this.companyList = [];
    this.reportrows = [];
    this.reportCount = 0;
    this.selectedCompanyType = e[0];
    this.selectedCompanyName = "";
    this.getCompanyList();
  }

  changeInCompanyName(e){
    this.selectedCompanyName = e[0];
    this.reportrows = [];
    this.reportCount = 0;
  }

  getCompanyList(){
    if(!this.selectedCompanyType || !this.registrationReportForm.range[0] || !this.registrationReportForm.range[1]){
      return;
    }
    let body = {
      startDate : this.registrationReportForm.range[0],
      endDate : this.registrationReportForm.range[1],
      companyType : this.selectedCompanyType
    }
    this._appService.getCompanyListPartnerCorporates(body).toPromise().then((result) => {
      if (result.data.length > 0) {
        for(let i = 0; i < result.data.length; i++){
          this.companyList.push({
            "id" : result.data[i],
            "name" : result.data[i]
          })
        }
      }
    }, reject => {
      console.log(reject);
    });
  }

  getReport(){
    if(!this.selectedCompanyType){
      alert("Company Type not provided!");
      return;
    }
    if(!this.registrationReportForm.range[0] || !this.registrationReportForm.range[1]){
      alert("Date range not selected!");
      return;
    }
    if(this.selectedCompanyType && this.registrationReportForm.range[0] && this.registrationReportForm.range[1]){
      let body = {
        startDate : this.registrationReportForm.range[0],
        endDate : this.registrationReportForm.range[1],
        companyType : this.selectedCompanyType
      }
      if(this.selectedCompanyName){
        body["companyName"] = this.selectedCompanyName;
      }
      this._appService.getRegistrationsByPartnerCorporates(body).toPromise().then((result) => {
        if (result.data.length > 0) {
          this.reportrows = result.data;
          this.reportCount = result.count;
        }else{
          alert("No registration records found!");
        }
      }, reject => {
        console.log(reject);
      });
    }
  }

  downloadReport() {
    let value = this.registrationReportForm;
    let body = {
      startDate : value.range[0],
      endDate : value.range[1],
      companyType : this.selectedCompanyType,
      type: "csv",
      report: "partner-corporates-registration"
    }
    if(this.selectedCompanyName){
      body["companyName"] = this.selectedCompanyName;
    }
    this._appService.downloadReports(body).toPromise().then((result) => {
      window.open(result.data.url, '_blank');
    }, reject => {
      console.log(reject);
    });
  }

  constructDate(date){
    let date1 = new Date(date);
    return date1.getDate() + "/" + (date1.getMonth()+1) + "/" + date1.getFullYear() + "  " + date1.getHours() + ":" + date1.getMinutes();
  }

  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    selectionLimit: 1,
    autoUnselect: true
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };

}
