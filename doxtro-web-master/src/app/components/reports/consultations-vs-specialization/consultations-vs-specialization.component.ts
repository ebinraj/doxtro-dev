import { AppService } from './../../../services/app.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';

declare let d3: any;

@Component({
  selector: 'app-consultations-vs-specialization',
  templateUrl: './consultations-vs-specialization.component.html',
  styleUrls: ['./consultations-vs-specialization.component.css']
})
export class ConsultationsVsSpecializationComponent implements OnInit {

  options;
  data;
  chartType;
  specializationList = [];
  specsarray = [];
  selectedSpecializations;
  completedata = [];
  totalConsultations = 0;

  @ViewChild('docvsspecs') consvsspecs;

  constructor(public _appService: AppService) { }

  ngOnInit() {

    this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
        margin: {
          top: 20,
          right: 20,
          bottom: 165,
          left: 65
        },
        useInteractiveGuideline: true,
        duration: 500,
        xAxis: {
          axisLabel: 'Specialization',
          axisLabelDistance: 20,
          rotateLabels: -35,
          tickFormat: function (d) {
            return d;
          }
        },
        yAxis: {
          axisLabel: 'Consultations Count',
          axisLabelDistance: 0,
          tickFormat: function (d) {
            return d.toFixed(0);
          }
        }
      }
    }

    this.data = [{
      values: [{ x: "General Physician", y: 0 }],
      key: 'Doctor vs Specialization',
      color: '#ff7f0e'
    }];

  }

  getReport(e) {
    this._appService.consultationsvsspecs({ startDate: e[0], endDate: e[1] }).toPromise().then(result => {

      this.totalConsultations = 0;

      if (result.data) {
        let graphdata = [];
        let optspecs = [];
        result.data.forEach(element => {
          this.totalConsultations += element.count;
          graphdata.push({ x: element.specializationCategory, y: element.count });
          let obj = { id: element.specializationCategory, name: element.specializationCategory };
          optspecs.push(obj);
          this.specsarray.push(element.specializationCategory);
        });

        this.data = [{
          values: graphdata,
          key: 'Consultations vs Specialization',
          color: '#ff7f0e'
        }];

        this.completedata = graphdata;

        this.specializationList = optspecs;
        this.consvsspecs.chart.update();
      }
      else {
        alert("No data found");
      }

    }).catch(err => {
      console.log(err);
    });
  }

  onSpecsChange(e) {
    debugger;

    if (e.length == 0)
      e = this.specsarray;

    this.totalConsultations = 0;

    let filtereddata = this.completedata.filter((item) => {
      return e.indexOf(item.x) != -1 ? true : false;
    });
    
    filtereddata.forEach(item => {
      this.totalConsultations += item.y;
    });

    this.data = [{
      values: filtereddata,
      key: 'Consultations vs Specialization',
      color: '#ff7f0e'
    }];
  }

  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    autoUnselect: true
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };


}
