import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultationsVsSpecializationComponent } from './consultations-vs-specialization.component';

describe('ConsultationsVsSpecializationComponent', () => {
  let component: ConsultationsVsSpecializationComponent;
  let fixture: ComponentFixture<ConsultationsVsSpecializationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultationsVsSpecializationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultationsVsSpecializationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
