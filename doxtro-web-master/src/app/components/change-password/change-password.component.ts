import { AccountService } from '../../services/account.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  newpassword  :FormGroup;
  token : string = "";

  constructor(public _fb : FormBuilder, public _accountService : AccountService, public route: ActivatedRoute) { }

  ngOnInit() {
    this.newpassword = this._fb.group({
      password : ""
    });

    this.route.params.subscribe(param => {
      this.token = param["token"];
    });
  }

  doChangePassword({valid, value}){
    this._accountService.resetPassword(value, this.token).toPromise().then((result) =>{
      alert("Password changed successfully");
    }, reject =>{
      alert("Oops .. Something bad happened");
    });
  }

}
