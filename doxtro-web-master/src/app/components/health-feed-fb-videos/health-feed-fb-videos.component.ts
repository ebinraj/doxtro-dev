import { Component, OnInit, Input, ViewChild, AfterViewInit, ElementRef, Output, EventEmitter } from '@angular/core';
import { FacebookService, InitParams, FBVideoComponent, LoginResponse } from 'ngx-facebook';
import { DomSanitizer } from '@angular/platform-browser';
import { EmbedVideoService } from 'ngx-embed-video';
declare let FB : any;

@Component({
  selector: 'app-health-feed-fb-videos',
  templateUrl: './health-feed-fb-videos.component.html',
  styleUrls: ['./health-feed-fb-videos.component.css']
})
export class HealthFeedFbVideosComponent implements OnInit, AfterViewInit {
  
  videos : any[] = [];
  @Output() videoSelected = new EventEmitter();
  pagination : any = {};
  
  constructor(private fb: FacebookService, public sanitizer : DomSanitizer, public embedService: EmbedVideoService) { }

  ngOnInit() {
    const params: InitParams = {
      appId: '1744202892564890',
      version: 'v2.9',
      xfbml: true
    };

    // this.videoSelected.emit("");

    this.fb.init(params);
    this.getVideoList();
  }

  getVideoList(){
    this.fb.login()
      .then((response: LoginResponse) => {
        console.log(response)
        debugger;

        this.fb.api('/323831871341300/videos?limit=4').then(result => {
          console.log(result);
          this.videos = result.data;
          this.pagination = result.paging;

          setTimeout(() =>{
            FB.XFBML.parse();
          })

        }).catch(err => {
          console.log(err);
        });

      }).catch((error: any) => console.error(error));
  }

  getPrevNext(link){
    this.fb.api(link).then(result => {
      console.log(result);
      this.videos = result.data;
      this.pagination = result.paging;

      setTimeout(() =>{
        FB.XFBML.parse();
      })

    }).catch(err => {
      console.log(err);
    });

  }

  ngAfterViewInit(): void {
    FB.XFBML.parse();
  }

  getVideoUrl(item){
    return "https://www.facebook.com/facebook/videos/"+item.id;
  }

  selectedVideo(item){
    this.videoSelected.emit(item.id);
  }
  

}
