import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthFeedFbVideosComponent } from './health-feed-fb-videos.component';

describe('HealthFeedFbVideosComponent', () => {
  let component: HealthFeedFbVideosComponent;
  let fixture: ComponentFixture<HealthFeedFbVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthFeedFbVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthFeedFbVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
