import { stat } from 'fs';
import { element } from 'protractor';
import { debug } from 'util';
import { UploadService } from './../../services/upload.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';
import * as firebase from 'firebase';
const uuid4 = require("uuid/v4");

@Component({
  selector: 'app-doctor-update',
  templateUrl: './doctor-update.component.html',
  styleUrls: ['./doctor-update.component.css']
})
export class DoctorUpdateComponent implements OnInit {
  public doctorId: String;
  public docProfile: any;
  public docavailability;
  public doctorform: FormGroup;
  public languageList: String[];
  public specializationlist;
  public imgnotfound: String = "http://www.stallerdental.com/wp-content/uploads/2016/12/user-icon.png";
  public profilePic: any;
  public certificates: any[];
  public idProofs: any[];
  public documentList: any[];
  public categorySpecsList: any[];
  public loadingindicator: boolean = false;
  public worksforlist: any[];
  public bankAccountList: any[];
  public educationlist: any[];

  public wf: any = {};
  public ugpg: any = {};
  public pgedu: any = {};
  public bank: any = {};
  public degreelist = [];
  public ugpgtypelist = ["UG", "PG"];

  public BankList = ['Allahabad Bank',
    'Andhra Bank',
    'Axis Bank',
    'Bank of Bahrain and Kuwait',
    'Bank of Baroda - Corporate Banking',
    'Bank of Baroda - Retail Banking',
    'Bank of India',
    'Bank of Maharashtra',
    'Canara Bank',
    'Central Bank of India',
    'City Union Bank',
    'Corporation Bank',
    'Deutsche Bank',
    'Development Credit Bank',
    'Dhanlaxmi Bank',
    'Federal Bank',
    'ICICI Bank',
    'IDBI Bank',
    'Indian Bank',
    'Indian Overseas Bank',
    'IndusInd Bank',
    'ING Vysya Bank',
    'Jammu and Kashmir Bank',
    'Karnataka Bank Ltd',
    'Karur Vysya Bank',
    'Kotak Bank',
    'Laxmi Vilas Bank',
    'Oriental Bank of Commerce',
    'Punjab National Bank - Corporate Banking',
    'Punjab National Bank - Retail Banking',
    'Punjab & Sind Bank',
    'Shamrao Vitthal Co-operative Bank',
    'South Indian Bank',
    'State Bank of Bikaner & Jaipur',
    'State Bank of Hyderabad',
    'State Bank of India',
    'State Bank of Mysore',
    'State Bank of Patiala',
    'State Bank of Travancore',
    'Syndicate Bank',
    'Tamilnad Mercantile Bank Ltd.',
    'UCO Bank',
    'Union Bank of India',
    'United Bank of India',
    'Vijaya Bank',
    'Yes Bank Ltd'];

  constructor(private activatedRoute: ActivatedRoute, private _appService: AppService, public _fb: FormBuilder, private upSvc: UploadService) {
    this.doctorform = this._fb.group({
      _id: "",
      firstName: ["",],
      emailid: ["",],
      experience: ["",],
      duns: ["",],
      gender: ["Male",],
      enabledForAudio : [true, ],
      enabledForPlanned : [true,],
      age: [""],
      language: [],
      mobile: ["",],
      specialization: [],
      category: [],
      bio: [""],
      profilePic: [""],
      wforganization: [""],
      wfspecialization: [""],
      wfexperience: [""],
      pgedudegree: [""],
      ugedudegree: [""],
      pgedeadditionaldata: [""],
      pgeducollege: [""],
      ugeducollege: [""],
      ugedeadditionaldata: [""],
      bkaccnumber: "",
      bkbank: "",
      bkifsc: "",
      bkname: "",
      ugpgtypedd: ""
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.doctorId = params["id"];
      this.getDoctorProfile(this.doctorId);
      this.getLanguageList();
      this.getAllSpecialization();
    });

    this.getDegreeList();

    this.ugpg.type = this.ugpgtypelist[0];
    this.ugpg.degree = this.degreelist[0];
    this.bank.bank = this.BankList[0];
  }

  getDoctorProfile(id) {
    this._appService.getDoctorDetails(id).subscribe(doc => {
      console.log(doc);
      this.docProfile = doc.data;
      this.docavailability = doc.data.availability;
      this.doctorform.patchValue(this.docProfile);
      this.doctorform.updateValueAndValidity();
      this.documentList = this.docProfile.documents;
      this.worksforlist = this.docProfile.WorksFor;
      this.bankAccountList = this.docProfile.bankDetails;

      this.educationlist = this.docProfile.qualification;

      // if (ugeduefromdb)
      //   this.ugedu = ugeduefromdb;
      // if (pgeduefromdb)
      //   this.pgedu = pgeduefromdb;


    });
  }

  getLanguageList() {
    this._appService.getLanguages().subscribe(result => {
      this.languageList = result.data.map(function (value, i) {
        let obj = { id: value, name: value };
        return obj;
      });
    });
  }

  deleteDoc(url: String) {
    this.documentList = this.documentList.filter(function (item) {
      return item.url != url;
    });
    console.log(this.documentList);
  }

  doUpdate({ value, valid }) {
    if (valid) {
      this.loadingindicator = true;
      let self = this;

      this.updateProfilePicture().then(ppres => {
        debugger;
        if (ppres["status"] == 200)
          value.profilePic = ppres["imgUrl"];

        //upload documents
        self.updateDocuments().then(docres => {
          debugger;
          self.updateProfile(value);

        });


      });


      debugger;
    }
  }

  updateProfile(value) {
    value.documents = this.documentList;
    value.category = this.categorySpecsList.filter(function (item) {
      return item.specializations.some(r => value.specialization.includes(r));
    }).map(function (value) {
      return value._id;
    });

    value.WorksFor = this.worksforlist;
    value.bankDetails = this.bankAccountList;
    value.qualification = this.educationlist;

    // value.qualification = [this.ugedu, this.pgedu];

    this._appService.updateDoctorProfile(value).subscribe(result => {
      this.docProfile = result.data;
      this.doctorform.patchValue(this.docProfile);
      this.doctorform.updateValueAndValidity();
      this.documentList = this.docProfile.documents;
      this.worksforlist = this.docProfile.WorksFor;
      this.bankAccountList = this.docProfile.bankDetails;

      this.loadingindicator = false;
    }, reject => {
      this.loadingindicator = false;
      console.log(reject);
    });
  }

  updateProfilePicture() {
    let self = this;
    return new Promise(function (resolve, reject) {
      if (!self.profilePic) {
        resolve({ status: 404 });
      }
      else {

        self.upSvc.pushUpload(self.profilePic).then((result) => {
          resolve({ status: 200, imgUrl: result.downloadURL });
        }, (rej) => {
          reject({ status: 400, msg: rej });
        }).catch(err => {
          reject({ status: 500, msg: err });
        });

      }
    });
  }

  updateDocuments() {
    let self = this;
    return new Promise(function (resolve, reject) {
      if (!self.certificates && !self.idProofs) {
        resolve({ status: 404 });
      }
      else {
        let docarr = [];
        let promarr = [];
        if (self.certificates) {
          for (let i = 0; i < self.certificates.length; i++) {
            docarr.push({ file: self.certificates[i], storagename: uuid4(), type: "Certificates" });
          }
        }
        if (self.idProofs) {
          for (let i = 0; i < self.idProofs.length; i++) {
            docarr.push({ file: self.idProofs[i], storagename: uuid4(), type: "idProof" });
          }
        }

        docarr.forEach(item => {
          promarr.push(self.upSvc.pushDocuments(item.file, item.storagename));
        });

        Promise.all(promarr).then(result => {

          for (let i = 0; i < result.length; i++) {
            let data = result[i];

            let imgdata = docarr.filter(function (item) {
              let name = decodeURI(data.downloadURL).split('?')[0].split('/');
              return item.storagename = name[name.length - 1];
            })

            self.documentList.push({ tag: imgdata[0]["type"], url: data.downloadURL, status: "pending" });
          }

          resolve({ status: 200 });

        }, reject => {
          reject({ status: 400, msg: reject });
        }).catch(err => {
          reject({ status: 500, msg: err });
        });
      }
    });
  }

  ProfilePicChange(event) {
    this.profilePic = event.srcElement.files[0];
  }

  CertificateChange(event) {
    this.certificates = event.srcElement.files;
  }

  idProofChange(event) {
    this.idProofs = event.srcElement.files;
  }


  getAllSpecialization() {
    this._appService.getAllSpecialization().subscribe(result => {
      this.categorySpecsList = result.data;
      this.wf.specialization = this.categorySpecsList[0]._id;
      // this.doctorform.patchValue({wfspecialization : this.categorySpecsList[0]._id});
      // this.doctorform.updateValueAndValidity();

      let optspecs = [];
      for (let i = 0; i < result.data.length; i++) {
        for (let j = 0; j < result.data[i].specializations.length; j++) {
          let obj = { id: result.data[i].specializations[j], name: result.data[i].specializations[j], parent: result.data[i]._id };
          optspecs.push(obj);
        }
      }

      this.specializationlist = optspecs;
    });
  }

  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    selectionLimit: 3
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };

  // Labels / Parents 
  myOptions: IMultiSelectOption[] = [
    { id: 2, name: 'Volvo', parentId: 1 },
    { id: 3, name: 'Honda', parentId: 1 },
    { id: 4, name: 'BMW', parentId: 1 },
    { id: 6, name: 'Blue', parentId: 5 },
    { id: 7, name: 'Red', parentId: 5 },
    { id: 8, name: 'White', parentId: 5 }
  ];

  // Works for section

  addWorksFor() {

    if (!this.wf.organization)
      alert("Please enter Organization in Work Information");
    else if (!this.wf.experience)
      alert("Please enter Experience in Work Information");
    else {

      this.worksforlist.push(this.wf);

      this.wf = {};
      this.wf.specialization = this.categorySpecsList[0]._id;
    }

  }

  deleteWorksFor(wf) {
    this.worksforlist.splice(this.worksforlist.indexOf(wf), 1);
  }

  updateworksfor(wf){
    this.deleteWorksFor(wf);
    this.wf=wf;
  }

  // Bank Account Information

  addBanktotheList() {

    if (!this.bank.accountNumber)
      alert("Please enter Account Number in Bank Details");
    else if (!this.bank.IFSC)
      alert("Please enter IFSC Code in Bank Details");
    else if (!this.bank.accountHolderName)
      alert("Please enter Account Holder Name in Bank Details");
    else {
      this.bankAccountList.push(this.bank);

      this.bank = {};
      this.bank.bank = this.BankList[0];
    }
  }

  deleteBankAccount(bk) {
    this.bankAccountList.splice(this.bankAccountList.indexOf(bk), 1);
  }

  updateBankAccount(bk){
    this.deleteBankAccount(bk);
    this.bank = bk;
  }

  // Educational Details

  deleteEducationalDetails(ed) {
    this.educationlist.splice(this.educationlist.indexOf(ed), 1);
  }

  addEducationalDetails() {

    if (!this.ugpg.college)
      alert("Please enter the college name");
    else if (!this.ugpg.yearFrom)
      alert("Please enter the starting year");
    else if (!this.ugpg.yearTo)
      alert("Please enter the ending year");
    else if (this.ugpg.degree == 'Others' && !this.ugpg.additionalData)
      alert("Please enter the Other degree");
    else {
      this.educationlist.push(this.ugpg);
      this.ugpg = {};

      this.ugpg.type = this.ugpgtypelist[0];
      this.ugpg.degree = this.degreelist[0];
    }


  }

  getDegreeList(){
    this._appService.getDegreeList().toPromise().then(result =>{
      this.degreelist = result.data;
    }, reject =>{
      console.log(reject);
    });
  }

  updateEducationalDetails(ed){
    this.deleteEducationalDetails(ed);
    this.ugpg = ed;
  }


}
