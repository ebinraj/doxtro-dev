import { ImageResize } from 'quill-image-resize-module';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, ViewEncapsulation, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FacebookService, InitParams, AuthResponse, LoginResponse, FBVideoComponent } from 'ngx-facebook';
import { AppService } from '../../services/app.service';
import { IMultiSelectSettings, IMultiSelectTexts, IMultiSelectOption } from 'angular-4-dropdown-multiselect';
import { UploadService } from '../../services/upload.service';
import { ActivatedRoute, Router } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ModalDirective } from 'ngx-bootstrap';

declare let FB: any;


import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
import Quill from 'quill';
import { ComponentCanDeactivate } from '../../common/auth.guard';
import { Observable } from 'rxjs/Observable';

// Quill.register('modules/imageResize', ImageResize);


@Component({
  selector: 'app-news-feed-create',
  templateUrl: './news-feed-create.component.html',
  styleUrls: ['./news-feed-create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewsFeedCreateComponent implements OnInit, ComponentCanDeactivate {

  frmHealthFeed: FormGroup
  specializationlist: any[] = [];
  coverPic;
  doctorList: any[] = [];
  doctorListResponse = [];
  newsFeedId = "";
  newsFeed = {};
  uploadedCoverPic = "";
  videoList = [];
  modalRef: BsModalRef;
  selectedVideo = "";
  videoThumbnails = [];
  isRequestInProgress = false;
  healfeedImage;
  contentEditor: "";
  categories = "Skin & Hair Care, Mental Well being, Depression, Addiction, Stress, Asthma, Thyroid, PCOS, Chronic Conditions, Pain Management, Oral Health, Pregnancy, Fertility, Cancer, General Health, Nutrition, Fitness, Sexual Health, Obesity, Weight Management, Diet".split(',');

  @ViewChild(FBVideoComponent) video: FBVideoComponent;
  @ViewChild('template') template: ModalDirective;
  @ViewChild('editor') editor: ElementRef;

  constructor(public _fb: FormBuilder, private fb: FacebookService, public _appService: AppService, public upSvc: UploadService, public activatedRoute: ActivatedRoute, public router: Router, private modalService: BsModalService) {
    this.initHealthFeedForm();
  }

  initHealthFeedForm() {
    this.frmHealthFeed = this._fb.group({
      _id: [],
      title: ['', Validators.required],
      content: ['', [Validators.required, Validators.minLength(200)]],
      categories: [[], Validators.required],
      coverPic: [],
      mediaType: ["Image", Validators.required],
      doctorId: [[], Validators.required]
    });
  }

  ngOnInit() {

    const params: InitParams = {
      appId: '1744202892564890',
      version: 'v2.9',
      xfbml: true
    };

    this.fb.init(params);

    // this.getAllSpecialization();
    this.getAllDoctors();

    let optspecs = [];
    for (let i = 0; i < this.categories.length; i++) {

      let obj = { id: this.categories[i], name: this.categories[i], parent: this.categories[i] };
      optspecs.push(obj);

    }

    this.specializationlist = optspecs;

    this.activatedRoute.params.subscribe(params => {
      if (params["id"] != 0) {
        this.newsFeedId = params["id"];
        this.getNewsFeed();
      }
      else {

        this.frmHealthFeed.reset();
        this.frmHealthFeed.patchValue({
          _id: "",
          title: '',
          content: '',
          categories: [],
          coverPic: '',
          mediaType: "Image",
          doctorId: []
        });
        this.selectedVideo = "";
        this.videoThumbnails = [];
        this.coverPic;
        this.newsFeedId = "";
        this.newsFeed = {};
        this.uploadedCoverPic = "";
        this.videoList = [];

      }
    });
  }

  openPreview() {
    window.open("/healthfeed/view/" + this.frmHealthFeed.value._id, '_blank');
  }

  doCreateUpdate({ value, valid }) {

    if (valid) {

      this.isRequestInProgress = true;

      let doctorIndex = this.doctorListResponse.findIndex(x => x._id === value.doctorId[0]);
      value.doctor = this.doctorListResponse[doctorIndex];

      if (value.mediaType === 'Image') {

        //if the image is present
        if (this.coverPic) {

          this.upSvc.pushHealthFeed(this.coverPic, value.title).then((result) => {

            value.coverPic = result.downloadURL;
            this._appService.createOrUpdateHealthFeed(value).toPromise().then((result) => {
              this.frmHealthFeed.patchValue(result.data);
              this.newsFeedId = result.data._id;
              alert("Healthfeed created");
              this.isRequestInProgress = false;
            });

          }).catch(err => {
            this.isRequestInProgress = false;
            alert("some error occured : " + err);
          });
        }
        else { //if the image is not present
          if (value._id && value.coverPic) {
            this._appService.createOrUpdateHealthFeed(value).toPromise().then((result) => {
              console.log(result);
              alert("Healthfeed updated");
              this.isRequestInProgress = false;
            }).catch(err => {
              this.isRequestInProgress = false;
              alert("some error occured : " + err);
            });
          }
          else {
            alert("The form is invalid. Please fill all the required fields");
            this.isRequestInProgress = false;
          }

        }

      }
      else {
        value.videoId = this.selectedVideo;
        value.thumbnails = this.videoThumbnails;

        if (value.videoId && value.thumbnails) {
          this._appService.createOrUpdateHealthFeed(value).toPromise().then((result) => {
            console.log(result);
            alert("Healthfeed Created / Updated");
            this.frmHealthFeed.patchValue(result.data);
            this.isRequestInProgress = false;
          }).catch(err => {
            this.isRequestInProgress = false;
            alert("some error occured : " + err);
          });
        }
        else {
          alert("The form is invalid. Please fill all the required fields");
          this.isRequestInProgress = false;
        }

      }

    }
    else {
      alert("The form is invalid. Please fill all the required fields.");
    }
  }

  CoverPicChange(event) {
    this.coverPic = event.srcElement.files[0];
  }

  getAllSpecialization() {
    this._appService.getAllSpecialization().subscribe(result => {

      let optspecs = [];
      for (let i = 0; i < result.data.length; i++) {
        for (let j = 0; j < result.data[i].specializations.length; j++) {
          let obj = { id: result.data[i].specializations[j], name: result.data[i].specializations[j], parent: result.data[i]._id };
          optspecs.push(obj);
        }
      }

      this.specializationlist = optspecs;
    });
  }

  getAllDoctors() {
    this._appService.getAllDoctors().toPromise().then(result => {
      this.doctorListResponse = result.data;

      let optspecs = [];
      optspecs = result.data.map((item, index) => {
        return { id: item._id, name: item.firstName, parent: item._id };
      });

      this.doctorList = optspecs;


    }).catch(err => {
      console.log(err);
    });
  }

  getNewsFeed(id = this.newsFeedId) {
    this._appService.getNewsFeedById(id).toPromise().then((result) => {
      this.newsFeed = result.data;
      this.frmHealthFeed.patchValue(result.data);
      this.frmHealthFeed.patchValue({ doctorId: result.data.doctor._id });

      if (result.data.mediaType == "Video") {
        this.selectedVideo = result.data.videoId;
        this.videoThumbnails = result.data.thumbnails;
      }
      else
        this.uploadedCoverPic = result.data.coverPic;


    }).catch(err => {
      console.log(err);
    });
  }

  /************* Videos ****************/

  getVideoList() {

    this.modalService.config.class = "modal-lg";
    this.modalRef = this.modalService.show(this.template);

  }

  getVideoUrl(item = this.selectedVideo) {
    return "https://www.facebook.com/facebook/videos/" + item;
  }

  videoSelectHandler(item) {
    console.log(item);
    this.selectedVideo = item;
    this.modalRef.hide();

    this.fb.api('/' + item + "/thumbnails").then((result) => {
      this.videoThumbnails = result.data;

    }).catch(err => {
      console.log(err);
    });

    setTimeout(() => { FB.XFBML.parse(); });
  }


  /************* Helpers ****************/


  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };

  docSettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    selectionLimit: 1
  };

  imageHandler() {
    const Imageinput = document.createElement('input');
    Imageinput.setAttribute('type', 'file');
    Imageinput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon');
    Imageinput.classList.add('ql-image');

    Imageinput.addEventListener('change', () => {
      this.isRequestInProgress = true;
      this.healfeedImage = Imageinput.files[0];
      if (Imageinput.files != null && Imageinput.files[0] != null) {
        this.upSvc.pushHealthFeed(this.healfeedImage, "").then(res => {
          //this._returnedURL = res;
          this.pushImageToEditor(res);
        });
      }
    });

    Imageinput.click();
  }

  EditorCreated(quill) {
    const toolbar = quill.getModule('toolbar');
    toolbar.addHandler('image', this.imageHandler.bind(this));
    //this.editor = quill;
  }

  pushImageToEditor(resurl) {
    console.log(resurl);
    debugger;
    const range = this.editor["quillEditor"].getSelection(true);
    const index = range.index + range.length;
    this.editor["quillEditor"].insertEmbed(range.index, 'image', resurl.downloadURL);

    this.isRequestInProgress = false;
  }

  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> | boolean {
    return false;
  }

}
