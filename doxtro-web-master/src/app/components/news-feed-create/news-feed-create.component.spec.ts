import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsFeedCreateComponent } from './news-feed-create.component';

describe('NewsFeedCreateComponent', () => {
  let component: NewsFeedCreateComponent;
  let fixture: ComponentFixture<NewsFeedCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsFeedCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsFeedCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
