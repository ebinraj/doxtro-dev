import { ActivatedRoute, Router } from '@angular/router';
import { MessagingService } from '../../services/messaging.service';
import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-app-container',
  templateUrl: './app-container.component.html',
  styleUrls: ['./app-container.component.css']
})
export class AppContainerComponent implements OnInit {
  message;

  public options = {
    position: ["bottom", "left"],
    timeOut: 5000,
    lastOnBottom: true
}

  constructor(public msgService : MessagingService, public toastr : ToastrService, public route : Router) { }

  ngOnInit() {

    this.msgService.getPermission();
    this.msgService.receiveMessage();
    this.message = this.msgService.currentMessage.subscribe((msg) => {

      if (msg) {
        this.toastr.info(msg.notification.title, msg.notification.body).onTap.subscribe(resu =>{
          this.route.navigate(['/app/consultations']);
        });
      }

    });

    
  }

}
