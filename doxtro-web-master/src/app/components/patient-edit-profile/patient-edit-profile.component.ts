import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { AppService } from '../../services/app.service';
@Component({
  selector: 'app-patient-edit-profile',
  templateUrl: './patient-edit-profile.component.html',
  styleUrls: ['./patient-edit-profile.component.css']
})
export class PatientEditProfileComponent implements OnInit {
  patientDetails: any;
  editProfile: FormGroup;
  height :any;
  num : any;
  heightFt : Number;
  heightInch : Number;
  public feetList = [1, 2, 3, 4, 5, 6, 7, 8];
  public inchList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

  constructor(private fb: FormBuilder, public modalRef: BsModalRef, private _appService: AppService) { }



  ngOnInit() {
         this.patientDetails = this._appService.getSelectedDetails();
         if(this.patientDetails.height){
           this.num = this.patientDetails.height.replace(/[^0-9]/g, '');
           this.height = Array.from(this.num.toString()).map(Number);
           this.heightFt = this.height[0];
           this.heightInch = this.height[1];
         }
         this.editProfile = this.fb.group({
           userId: [this.patientDetails.userId],
           firstName: [this.patientDetails.firstName, Validators.required],
           emailid: [this.patientDetails.emailid, Validators.required],
           mobile: [this.patientDetails.mobile],
           gender: [this.patientDetails.gender, Validators.required],
           age: [this.patientDetails.age, Validators.required],
           weight: [this.patientDetails.weight, Validators.required],
           heightFt : [],
           heightInch : []
         });
  }

editPatient(doc) {
    if (this.editProfile.dirty) {
      this.editProfile.value._id = this.patientDetails._id;
      if(this.editProfile.value.heightFt != undefined && this.editProfile.value.heightInch != undefined)
        this.editProfile.value.height = this.editProfile.value.heightFt + "'" + this.editProfile.value.heightInch + "''";
      this._appService.editPatient(this.editProfile.value).subscribe(result => {
        this.patientDetails = result.data;
        alert("profile updated successfully")
        this.modalRef.hide();
      }, error => { console.log(error) });
    } else {
      alert("No data to update")
    }

  }

}
