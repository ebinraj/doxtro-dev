import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { AppService } from '../../services/app.service';
import { Subscription } from 'rxjs/Subscription';
import { csv } from 'd3';

@Component({
  selector: 'app-consultations',
  templateUrl: './consultations.component.html',
  styleUrls: ['./consultations.component.css']
})
export class ConsultationsComponent implements OnInit, OnDestroy {

  pageCount: any = 10;
  currentPage = 1;
  searchTerm: String = "";
  filterStatus: string = "";
  filterCompany: string = "";
  companyName: string = "";
  sortField: String = "paymentDate-desc";
  paginationshowcount: any[] = [];
  dataList: any[] = [];
  doctorList: any[] = [];
  companyList: any[] = [];
  selectedConsultation: any;
  seelctedConsultationObj: any;
  getConsultationListSubscriber: Subscription;
  public totalCount: Number;

  public modalRef: BsModalRef;

  public consultationObj = {
    pageCount: this.pageCount,
    sortField: this.sortField,
    skip: 0,
    searchTerm: "",
    status: "",
    companyName: ""
  };
  public companyObj = {
    pageCount: null,
    sortField: this.sortField,
    skip: 0,
    searchTerm: ""
  };
  public reportObj = {
    type: "csv",
    report: "consultations",
    companyName: ""
  }

  constructor(private _appService: AppService, private modalService: BsModalService, public toastr: ToastrService, public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    debugger;
    let localobj = localStorage.getItem("consultationObj");
    if (localobj) {
      this.consultationObj = JSON.parse(localobj);
      this.consultationObj.companyName = "";
      this.pageCount = this.consultationObj.pageCount;
      this.searchTerm = this.consultationObj.searchTerm;
      this.filterStatus = this.consultationObj.status;
      this.currentPage = this.consultationObj.skip != 0 ? (Number(this.consultationObj.skip) / Number(this.consultationObj.pageCount))+1 : 1;
      this.getData();
      this.getCompanyList();
    }
    else {
      this.getData();
      this.getCompanyList();
    }
  }

  doSearch(st) {
    this.consultationObj.searchTerm = st;
    this.consultationObj.skip = 0;
    this.currentPage = 1;
    this.getData();
    this.updateConsultationObj(this.consultationObj);
  }

  onPageCountChange() {
    let pscs = Math.ceil(this.dataList.length / (Number)(this.pageCount));
    if (pscs < this.currentPage)
      this.currentPage = 1;

    this.consultationObj.pageCount = this.pageCount;
    this.consultationObj.skip = this.currentPage === 1 ? 0 : (Number(this.currentPage) * Number(this.pageCount));

    this.getData();
    this.updateConsultationObj(this.consultationObj);
  }

  filterStatusChange() {
    this.consultationObj.status = this.filterStatus;
    this.consultationObj.skip = 0;
    this.pageCount = 1;
    this.getData();
    this.updateConsultationObj(this.consultationObj);
  }

  filterCompanyChange() {
    this.consultationObj.companyName = this.filterCompany;
    this.consultationObj.status = this.filterStatus;
    this.consultationObj.skip = 0;
    this.pageCount = 1;
    this.getData();
    this.updateConsultationObj(this.consultationObj);
  }

  getData() {

    if (this.getConsultationListSubscriber)
      this.getConsultationListSubscriber.unsubscribe();

    this.getConsultationListSubscriber = this._appService.getConsultationList(this.consultationObj.searchTerm, this.consultationObj.pageCount, this.consultationObj.skip, this.consultationObj.sortField, this.consultationObj.status, this.consultationObj.companyName).subscribe(result => {
      this.dataList = result.data;
      this.totalCount = result.totalCount;

      let pscs = Math.ceil(result.totalCount / (Number)(this.pageCount));
      this.paginationshowcount = Array.from(Array(pscs), (x, i) => i + 1);
    }, error => {
      console.log(error)
    });
  }

  doPrevious() {
    if (this.currentPage != 1) {
      this.consultationObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 2) * Number(this.pageCount));
      this.currentPage = (Number)(this.currentPage) - 1;
      this.getData();
      this.updateConsultationObj(this.consultationObj);
    }
  }

  doNext() {
    if (this.currentPage != this.paginationshowcount[this.paginationshowcount.length - 1]) {
      this.consultationObj.skip = (Number(this.currentPage) * Number(this.pageCount));
      this.getData();
      this.currentPage = (Number)(this.currentPage) + 1;
      this.updateConsultationObj(this.consultationObj);
    }
  }

  doChangePage(pnum) {
    this.consultationObj.skip = ((pnum - 1) * Number(this.pageCount));
    this.getData();
    this.currentPage = pnum;
    this.updateConsultationObj(this.consultationObj);
  }

  doChangeSort(sortfield) {
    let sf = this.sortField.split('-');
    if (sortfield == sf[0]) {
      if (sf[1] == "asc") {
        this.sortField = sortfield + "-desc";
      }
      else {
        this.sortField = sortfield + "-asc";
      }
    }
    else {
      this.sortField = sortfield + "-asc";
    }

    this.consultationObj.sortField = this.sortField;
    this.getData();
    this.updateConsultationObj(this.consultationObj);

  }

  getDoctors(specialization, language) {
    alert("hi")
    this._appService.getDoctors(specialization, language).subscribe(result => {
      console.log("result", result);
      this.doctorList = result.data;
    }, error => {
      console.log(error)
    });
  }


  // Open Model of the Doctor List

  public openModal(mdlSelectDoctor: TemplateRef<any>, data) {
    this.selectedConsultation = "";
    this.seelctedConsultationObj = {};

    this._appService.getDoctors(data.specializationCategory, data.consultationLanguage).subscribe(result => {
      this.doctorList = result.data;
      this.selectedConsultation = data._id;
      this.seelctedConsultationObj = data;
      this.modalRef = this.modalService.show(mdlSelectDoctor);
    });

  }

  assignDoctor(docId) {
    debugger;
    let body = {
      doctorId: docId,
      consultationId: this.selectedConsultation
    }
    this._appService.assignDoctor(body).subscribe(result => {
      this.toastr.success("Request sent to the doctor");
      this.modalRef.hide();
    }, reject => {
      debugger;
      if (reject.status == 409)
        this.toastr.warning("Please ask doctor to accept/reject.");
      else
        this.toastr.error("Request to the doctor failed", reject);

      this.modalRef.hide();
    });
  }

  openChat(cns) {
    this._appService.setSelectedConsultation(cns);
    this.router.navigate(["/app/chat/" + cns._id]);
  }

  openTimeline(cns) {
    this._appService.setSelectedConsultation(cns);
    this.router.navigate(["/app/consultation-timeline/" + cns._id]);
  }

  getClassName(rejby) {
    if (rejby.length == 1)
      return "orange";
    else if (rejby.length == 2)
      return "red";
    else
      return "";
  }

  getdatediff(datea: any) {
    let today: any = new Date();
    let datenew: any = new Date(datea);
    let diff: any = Math.abs(today - datenew);
    let minutes = Math.floor((diff / 1000) / 60);

    if (minutes > 3)
      return false;
    else
      return true;

  }

  getCompanyList() {
    debugger;
    let docObj = this.companyObj;
    let totalCount;
    this._appService.getcompanyList(docObj.searchTerm, docObj.pageCount, docObj.skip, docObj.sortField).subscribe(result => {
      this.companyList = result.data;
      totalCount = result.totalCount;
      console.log(this.dataList);
      let pscs = Math.ceil(result.totalCount / (Number)(this.pageCount));
      this.paginationshowcount = Array.from(Array(pscs), (x, i) => i + 1);
    });
  }

  downloadCsv(){
    debugger;
    this.reportObj.companyName = this.consultationObj.companyName;
    this._appService.downloadReports(this.reportObj).subscribe(result => {
      console.log(result);
      if(!result.data.url){
        alert("No records found.");
      }
      else{
      window.open(result.data.url, '_blank');
      }
    }, error => {
      console.log(error);
      alert("No records found.");
    });
  }

  ngOnDestroy(): void {
    this.getConsultationListSubscriber.unsubscribe();
  }

  updateConsultationObj(obj) {
    localStorage.setItem("consultationObj", JSON.stringify(obj));
  }

}
