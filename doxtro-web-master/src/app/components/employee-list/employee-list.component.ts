import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../services/app.service';
import { IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  pageCount: any = 10;
  currentPage: any = 1;
  searchTerm: string = "";
  sortField: String = "createdAt-desc";
  paginationshowcount: any[] = [];
  dataList: any[];
  totalComap: Number;
  companyList = [];
  selectedCompany = [];

  public empObj = {
    pageCount: this.pageCount,
    sortField: this.sortField,
    skip: 0,
    searchTerm: ""
  };

  constructor(private _appService: AppService, public route: ActivatedRoute) { }

  ngOnInit() {
    debugger;
    this.route.params.subscribe(params => {
      if (params["company"] != 0)
        this.selectedCompany.push(params["company"]);
    });


    let localobj = localStorage.getItem("empObj");
    if (localobj) {
      this.empObj = JSON.parse(localobj);
      this.searchTerm = this.empObj.searchTerm;
      this.pageCount = this.empObj.pageCount;
      this.getData();
    }
    else {
      debugger;
      this.getData();
    }

    this.getCompanyList();

  }

  doSearch(st) {

    this.empObj = {
      pageCount: this.pageCount,
      sortField: this.sortField,
      skip: 0,
      searchTerm: st
    };

    this.getData();
    this.updateempObj(this.empObj);
  }

  onPageCountChange() {
    let pscs = Math.ceil(this.dataList.length / (Number)(this.pageCount));
    if (pscs < this.currentPage)
      this.currentPage = 1;

    this.empObj.searchTerm = this.searchTerm;
    this.empObj.pageCount = this.pageCount;
    this.empObj.skip = this.currentPage === 1 ? 0 : (Number(this.currentPage) * Number(this.pageCount));
    this.empObj.sortField = this.sortField;
    this.getData();
    this.updateempObj(this.empObj);
  }

  getData() {
    debugger;
    let docObj = this.empObj;

    this._appService.getEmployeeList(docObj.searchTerm, docObj.pageCount, docObj.skip, docObj.sortField, this.selectedCompany).subscribe(result => {
      this.dataList = result.data;
      this.totalComap = result.totalCount;
      console.log(this.dataList);
      let pscs = Math.ceil(result.totalCount / (Number)(this.pageCount));
      this.paginationshowcount = Array.from(Array(pscs), (x, i) => i + 1);
    });
  }

  doPrevious() {
    if (this.currentPage != 1) {

      this.empObj.searchTerm = this.searchTerm;
      this.empObj.pageCount = this.pageCount;
      this.empObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 2) * Number(this.pageCount));
      this.empObj.sortField = this.sortField;

      this.currentPage = (Number)(this.currentPage) - 1;
      this.getData();
      this.updateempObj(this.empObj);
    }
  }

  doNext() {
    if (this.currentPage != this.paginationshowcount[this.paginationshowcount.length - 1]) {

      this.empObj.searchTerm = this.searchTerm;
      this.empObj.pageCount = this.pageCount;
      this.empObj.skip = (Number(this.currentPage) * Number(this.pageCount));
      this.empObj.sortField = this.sortField;

      this.currentPage = (Number)(this.currentPage) + 1;
      this.getData();
      this.updateempObj(this.empObj);
    }
  }

  doChangePage(pnum) {

    this.empObj.searchTerm = this.searchTerm;
    this.empObj.pageCount = this.pageCount;
    this.empObj.skip = ((pnum - 1) * Number(this.pageCount));
    this.empObj.sortField = this.sortField;

    this.currentPage = pnum;
    this.getData();
    this.updateempObj(this.empObj);
  }

  doChangeSort(sortfield) {
    let sf = this.sortField.split('-');
    if (sortfield == sf[0]) {
      if (sf[1] == "asc") {
        this.sortField = sortfield + "-desc";
      }
      else {
        this.sortField = sortfield + "-asc";
      }
    }
    else {
      this.sortField = sortfield + "-asc";
    }

    this.empObj.searchTerm = this.searchTerm;
    this.empObj.pageCount = this.pageCount;
    this.empObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 1) * Number(this.pageCount));
    this.empObj.sortField = this.sortField;

    this.getData();
    this.updateempObj(this.empObj);

  }

  updateempObj(obj) {
    localStorage.setItem("empObj", JSON.stringify(obj));
  }

  deleteEmployee(id) {
    var r = confirm("Are you sure you want to delete this employee?");
    if (r == true) {
      this._appService.deleteEmployee(id).toPromise().then(result => {
        this.getData();
        alert("Employee deleted");
      }).catch(err => {
        console.log(err);
      });
    }
  }

  getCompanyList() {
    this._appService.getcompanyNameList().toPromise().then(result => {
      debugger;
      let optsrc = [];
      result.data.forEach(item => {
        let obj = { id: item, name: item, parent: item };
        optsrc.push(obj);
      });

      this.companyList = optsrc;

    }).catch(err => {
      console.log(err);
    });
  }

  onChangeCompany(e) {
    this.getData();
  }

  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    selectionLimit: 1,
    autoUnselect: true,
    showUncheckAll : true
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };

}
