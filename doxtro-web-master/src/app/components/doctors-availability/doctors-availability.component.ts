import { AppService } from '../../services/app.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctors-availability',
  templateUrl: './doctors-availability.component.html',
  styleUrls: ['./doctors-availability.component.css']
})
export class DoctorsAvailabilityComponent implements OnInit {
  docavailability: any;
  docProfile: any;
  doctorId: any;
  public timeSlots = ["00.00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30", "23:59"];
  avabform: FormGroup;
  public dayList = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  constructor(private activatedRoute: ActivatedRoute, private _appService: AppService, public _fb: FormBuilder) {
    this.avabform = _fb.group({
      dayOfWeek: "",
      startTime: "",
      endTime: ""
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.doctorId = params["id"];
      this.getDoctorProfile(this.doctorId);
    });

    this.avabform.patchValue({ dayOfWeek: this.dayList[0], startTime: this.timeSlots[0], endTime: this.timeSlots[1] });
  }

  getDoctorProfile(id) {
    this._appService.getDoctorDetails(id).subscribe(doc => {
      console.log(doc);
      this.docProfile = doc.data;
      this.docavailability = doc.data.availability;
    });
  }

  doUpdate({ value, valid }) {

    if (this.timeSlots.indexOf(value.startTime) > this.timeSlots.indexOf(value.endTime))
      alert("starttime should be lesser than end time");
    else {
      value.startTime = value.startTime.replace(':', '');
      value.endTime = value.endTime.replace(':', '');
      this.docavailability.push(value);
      this._appService.updateDoctorAvailability({ _id: this.doctorId, operationalHours: this.docavailability }).subscribe(result => {
        console.log(result);
      });
    }
  }

  deleteavailability(av) {
    this.docavailability.splice(this.docavailability.indexOf(av), 1);
    this._appService.updateDoctorAvailability({ _id: this.doctorId, operationalHours: this.docavailability }).subscribe(result => {
      console.log(result);
    });
  }

  copyToAll({ value, valid }) {
    if (this.timeSlots.indexOf(value.startTime) > this.timeSlots.indexOf(value.endTime))
      alert("starttime should be lesser than end time");
    else {
      value.startTime = value.startTime.replace(':', '');
      value.endTime = value.endTime.replace(':', '');

      for (let i = 0; i < this.dayList.length; i++) {
        let fd = {dayOfWeek : "", startTime : "", endTime :""};
        fd["dayOfWeek"] = this.dayList[i];
        fd.startTime = value["startTime"];
        fd.endTime = value.endTime;
        
        this.docavailability.push(fd);
      }

      this._appService.updateDoctorAvailability({ _id: this.doctorId, operationalHours: this.docavailability }).subscribe(result => {
        console.log(result);
      });
    }
  }


}
