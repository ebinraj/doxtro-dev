import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AppService } from 'app/services/app.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';

@Component({
  selector: 'app-doctor-payment-report',
  templateUrl: './doctor-payment-report.component.html',
  styleUrls: ['./doctor-payment-report.component.css']
})
export class DoctorPaymentReportComponent implements OnInit {

  dprfrm = {
    doctorId : [],
    range : {},
    specializationId : []
  };
  public doctorList: any[];
  public reportrows: any[] = [];
  specializationList = [];
  totalAmount = 0;
  isDownloadEnabled = false;
  doctormodel = [];
  specializationsmodel = [];

  constructor(public _appService: AppService, public _fb: FormBuilder) {
    
  }

  ngOnInit() {
    
  }

  onChangeDoctor(e){
    this.dprfrm.specializationId = [];
  }
  onChangeSpecs(e){
    this.dprfrm.doctorId = [];
  }

  dogetreport() {

    let value = this.dprfrm;

    if ((value.doctorId && value.doctorId.length > 0) || (value.specializationId && value.specializationId.length > 0)) {

      debugger;
      let body = {
        doctorId: (value.doctorId && value.doctorId.length) ? value.doctorId : null,
        startDate: value.range[0],
        endDate: value.range[1],
        specializationCategory: value.specializationId
      }

      this._appService.getdoctorpaymentreport(body).toPromise().then((result) => {
        if (result.data.length > 0) {
          this.reportrows = result.data;

          if (body.doctorId && body.doctorId.length)
            this.isDownloadEnabled = true;
          else
            this.isDownloadEnabled = false;

        }
        else {
          alert("no record found");
        }
      }, reject => {
        alert("Some error occured");
        console.log(reject);
      });
    } else {
      alert("please select the doctor or Specialization Category");
    }

  }

  getSum(field) {
    let sum = 0;
    for (let i = 0; i < this.reportrows.length; i++) {
      sum += this.reportrows[i][field];
    }
    return sum;
  }

  downloadPDF() {
    let value = this.dprfrm;
    let body = {
      doctorId: value.doctorId,
      startDate: value.range[0],
      endDate: value.range[1],
      type: "pdf"
    }

    this._appService.downloaddoctorpaymentreport(body).toPromise().then((result) => {
      window.open(result.data.url, '_blank');
    }, reject => {
      console.log(reject);
    });
  }

  getDoctorsandSpecs(e) {
    this.getdoctors(e);
    this.getSpecialization(e);
  }

  getdoctors(e) {

    debugger;

    if (e) {

      this._appService.getDoctorListForReport(e[0], e[1]).toPromise().then((result) => {
        let optspecs = [];
        for (let i = 0; i < result.data.length; i++) {
          let obj = { id: result.data[i]._id, name: result.data[i].firstName };
          optspecs.push(obj);
        }

        this.doctorList = optspecs;
        //this.dprfrm.patchValue({ "doctorId": result.data[0]._id });
      }, reject => {
        console.log(reject);
      }).catch(err => {
        console.log(err);
      });

    }

  }

  getSpecialization(e) {
    if (e) {
      this._appService.getSpecializationForReport(e[0], e[1]).toPromise().then((result) => {
        let optspecs = [];
        for (let i = 0; i < result.data.length; i++) {
          let obj = { id: result.data[i], name: result.data[i] };
          optspecs.push(obj);
        }

        this.specializationList = optspecs;
        //this.dprfrm.patchValue({ "doctorId": result.data[0]._id });
      }, reject => {
        console.log(reject);
      }).catch(err => {
        console.log(err);
      });
    }
  }

  // Settings configuration 
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true,
    selectionLimit: 1,
    autoUnselect: true
  };

  // Text configuration 
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select from the List',
    allSelected: 'All selected',
  };


}
