import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorPaymentReportComponent } from './doctor-payment-report.component';

describe('DoctorPaymentReportComponent', () => {
  let component: DoctorPaymentReportComponent;
  let fixture: ComponentFixture<DoctorPaymentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorPaymentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorPaymentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
