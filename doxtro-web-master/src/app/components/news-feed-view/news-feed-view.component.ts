import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../services/app.service';
import { FBVideoComponent, InitParams, FacebookService } from 'ngx-facebook';

declare let FB: any;

@Component({
  selector: 'app-news-feed-view',
  templateUrl: './news-feed-view.component.html',
  styleUrls: ['./news-feed-view.component.css']
})
export class NewsFeedViewComponent implements OnInit, AfterViewInit {

  newsFeedId = "";
  newsFeed: any = {};

  @ViewChild(FBVideoComponent) video: FBVideoComponent;
  @ViewChild("healthfeedcontent") healthfeedcontent;

  constructor(public activatedRoute: ActivatedRoute, public _appService: AppService, public fb: FacebookService, private elementRef: ElementRef, public sanitizer  :DomSanitizer) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.newsFeedId = params["id"];
      this.getNewsFeed();
    });

    const params: InitParams = {
      appId: '470817833008568',
      version: 'v2.9',
      xfbml: true
    };

    this.fb.init(params);
  }

  ngAfterViewInit(): void {
    debugger;
    setTimeout(() => {
      let imgs = this.elementRef.nativeElement.getElementsByTagName('img');
      for (let item of imgs) {
        item.classList.add('img-fluid');
        item.classList.add('rounded');
      }
    },1000);

  }

  getNewsFeed(id = this.newsFeedId) {

    this._appService.getNewsFeedById(id).toPromise().then((result) => {
      
      this.newsFeed = result.data;

      setTimeout(() => { FB.XFBML.parse(); });

    }).catch(err => {
      console.log(err);
    });
  }

  getNewsFeedContent(){
    return this.sanitizer.bypassSecurityTrustHtml(this.newsFeed.content);
  }

  getVideoUrl(item) {
    return "https://www.facebook.com/facebook/videos/" + item;
  }

}
