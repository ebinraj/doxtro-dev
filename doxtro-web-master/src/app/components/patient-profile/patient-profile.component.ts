import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { AppService } from '../../services/app.service';
import * as firebase from 'firebase';
import { DecodeURIPipe } from '../../pipes/decode-uri.pipe';

@Component({
  selector: 'app-patient-profile',
  templateUrl: './patient-profile.component.html',
  styleUrls: ['./patient-profile.component.css']
})
export class PatientProfileComponent implements OnInit {
  patientDetails: any;
  constructor(public modalRef: BsModalRef, private _appService: AppService) { }

  ngOnInit() {
    this._appService.getPatientDetails(this._appService.getSelectedDocId()).subscribe(result => {
      if (result.data) {
        this.patientDetails = result.data;
        this._appService.setSelectedDetails(result.data);
      } else {
        console.log("No data found");
      }
    });
  }

 }
