import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, TemplateRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AppService } from '../../services/app.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { DoctorProfileComponent } from '../doctor-profile/doctor-profile.component';

@Component({
  selector: 'app-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.css']
})
export class DoctorsListComponent implements OnInit, OnDestroy {

  public modalRef: BsModalRef;
  pageCount: any = 10;
  currentPage: any = 1;
  searchTerm: string = "";
  sortField: String = "accountActivationTime-desc";
  paginationshowcount: any[] = [];
  dataList: any[];
  doctorDetail: any;
  totalDocs: Number;

  public doctorObj = {
    pageCount: this.pageCount,
    sortField: this.sortField,
    skip: 0,
    searchTerm: ""
  };

  constructor(private _appService: AppService, private modalService: BsModalService, public route: ActivatedRoute) { }

  ngOnInit() {
    debugger;
    let localobj = localStorage.getItem("doctorObj");
    if (localobj) {
      this.doctorObj = JSON.parse(localobj);
      this.searchTerm = this.doctorObj.searchTerm;
      this.pageCount = this.doctorObj.pageCount;
      this.getData();
    }
    else {
      this.getData();
    }

    this.route.params.subscribe(param => {
      if (param["id"])
        this.getDetails(param["id"]);
    });
  }

  doSearch(st) {

    this.doctorObj = {
      pageCount: this.pageCount,
      sortField: this.sortField,
      skip: 0,
      searchTerm: st
    };

    this.getData();
    this.updateDoctorObj(this.doctorObj);
  }

  onPageCountChange() {
    let pscs = Math.ceil(this.dataList.length / (Number)(this.pageCount));
    if (pscs < this.currentPage)
      this.currentPage = 1;

    this.doctorObj.searchTerm = this.searchTerm;
    this.doctorObj.pageCount = this.pageCount;
    this.doctorObj.skip = this.currentPage === 1 ? 0 : (Number(this.currentPage) * Number(this.pageCount));
    this.doctorObj.sortField = this.sortField;
    this.getData();
    this.updateDoctorObj(this.doctorObj);
  }

  getData() {

    let docObj = this.doctorObj;
    debugger;
    this._appService.getDoctorsList(docObj.searchTerm, docObj.pageCount, docObj.skip, docObj.sortField).subscribe(result => {
      this.dataList = result.data;
      this.totalDocs = result.totalCount;

      let pscs = Math.ceil(result.totalCount / (Number)(this.pageCount));
      this.paginationshowcount = Array.from(Array(pscs), (x, i) => i + 1);
    });
  }

  doPrevious() {
    if (this.currentPage != 1) {

      this.doctorObj.searchTerm = this.searchTerm;
      this.doctorObj.pageCount = this.pageCount;
      this.doctorObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 2) * Number(this.pageCount));
      this.doctorObj.sortField = this.sortField;

      this.currentPage = (Number)(this.currentPage) - 1;
      this.getData();
      this.updateDoctorObj(this.doctorObj);
    }
  }

  doNext() {
    if (this.currentPage != this.paginationshowcount[this.paginationshowcount.length - 1]) {

      this.doctorObj.searchTerm = this.searchTerm;
      this.doctorObj.pageCount = this.pageCount;
      this.doctorObj.skip = (Number(this.currentPage) * Number(this.pageCount));
      this.doctorObj.sortField = this.sortField;

      this.currentPage = (Number)(this.currentPage) + 1;
      this.getData();
      this.updateDoctorObj(this.doctorObj);
    }
  }

  doChangePage(pnum) {

    this.doctorObj.searchTerm = this.searchTerm;
    this.doctorObj.pageCount = this.pageCount;
    this.doctorObj.skip = ((pnum - 1) * Number(this.pageCount));
    this.doctorObj.sortField = this.sortField;

    this.currentPage = pnum;
    this.getData();
    this.updateDoctorObj(this.doctorObj);
  }

  doChangeSort(sortfield) {
    let sf = this.sortField.split('-');
    if (sortfield == sf[0]) {
      if (sf[1] == "asc") {
        this.sortField = sortfield + "-desc";
      }
      else {
        this.sortField = sortfield + "-asc";
      }
    }
    else {
      this.sortField = sortfield + "-asc";
    }

    this.doctorObj.searchTerm = this.searchTerm;
    this.doctorObj.pageCount = this.pageCount;
    this.doctorObj.skip = this.currentPage === 1 ? 0 : (Number((Number)(this.currentPage) - 1) * Number(this.pageCount));
    this.doctorObj.sortField = this.sortField;

    this.getData();
    this.updateDoctorObj(this.doctorObj);

  }

  getDetails(id) {
    this._appService.setSelectedDocId(id);
    this.modalService.config.class = "modal-lg";
    this.modalRef = this.modalService.show(DoctorProfileComponent);

  }

  updateDoctorObj(obj) {
    localStorage.setItem("doctorObj", JSON.stringify(obj));
  }

  ngOnDestroy(): void {
    if (this.modalService.getModalsCount() > 0)
      this.modalRef.hide();
  }

}
