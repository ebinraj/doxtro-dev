import { Component, OnInit, Input, Inject } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { AppService } from '../../services/app.service';
import * as firebase from 'firebase';
import { DecodeURIPipe } from '../../pipes/decode-uri.pipe';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DocumentVerificationComponent } from '../document-verification/document-verification.component';

@Component({
  selector: 'app-doctor-profile',
  templateUrl: './doctor-profile.component.html',
  styleUrls: ['./doctor-profile.component.css']
})
export class DoctorProfileComponent implements OnInit {

  public imgnotfound: String = "http://www.stallerdental.com/wp-content/uploads/2016/12/user-icon.png";
  listOfDays: String[] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", 'Saturday'];
  today: String = this.listOfDays[new Date().getDay()];
  listOfAvailability: any[];
  selectedDaysAvailability: String;
  isStatusFormVisible = false;
  doctorDetail: any;
  consultationCounts = {};
  statusForm: any = {
    ratingByAdmin :1
  };

  constructor(public modalRef: BsModalRef, private _appService: AppService, private modalService: BsModalService) { }

  ngOnInit() {
    this._appService.getDoctorDetails(this._appService.getSelectedDocId()).subscribe(result => {
      debugger;
      this.doctorDetail = result.data;
      this.listOfAvailability = result.data.availability;
      this.getAvailabilityByDay();
      console.log(result);
    });

    this._appService.getDoctorConsultationCount(this._appService.getSelectedDocId()).toPromise().then(result => {
      this.consultationCounts = result.data;
    }).catch(error => {
    });

    // let storageRef =  firebase.storage().ref();
    // storageRef = storageRef.child('doctor documents');

  }

  getAvailabilityByDay() {
    let self = this;
    if (this.listOfAvailability.length > 0) {
      let avaarry = this.listOfAvailability.filter(function (e) {
        return e.dayOfWeek == self.today;

      });

      if (avaarry.length > 0)
        this.selectedDaysAvailability = avaarry.map(function (e) {
          return e.startTime + " - " + e.endTime;
        }).join(" , ");
      else
        this.selectedDaysAvailability = "NA";
    }
    else {
      this.selectedDaysAvailability = "NA";
    }

  }

  openVerificationWindow() {
    this.modalRef.hide();

    this._appService.setSelectedDocDocuments(this.doctorDetail.documents);

    this.modalService.config.class = "modal-lg";
    this.modalRef = this.modalService.show(DocumentVerificationComponent);
  }

  // Status Action Area

  public statusList = ["approved", "rejected"];
  
  showStatusForm() {

    debugger;

    this.isStatusFormVisible = true;
    this.statusForm._id = this._appService.getSelectedDocId();
    this.statusForm.status = this.doctorDetail.status;
    this.statusForm.doctorFee = this.doctorDetail.doctorFee ? this.doctorDetail.doctorFee : 0;
    this.statusForm.doctorAudioFee = this.doctorDetail.doctorAudioFee ? this.doctorDetail.doctorAudioFee : 0;
    this.statusForm.plannedChatFee = this.doctorDetail.plannedChatFee ? this.doctorDetail.plannedChatFee : 0;
    this.statusForm.plannedAudioFee = this.doctorDetail.plannedAudioFee ? this.doctorDetail.plannedAudioFee : 0;
    this.statusForm.reason = this.doctorDetail.reason;
    this.statusForm.ratingByAdmin = this.doctorDetail.ratingByAdmin;
  }

  updateStatus() {
    debugger;
    console.log(this.statusForm);
    this._appService.updateDoctorsStatus(this.statusForm).subscribe(result => {
      this.doctorDetail.status = result.data.status;
      this.isStatusFormVisible = false;
    }, reject => {
      if (reject.status == 400)
        alert(JSON.parse(reject._body).message);
    });
  }

  handleCancel() {
    this.isStatusFormVisible = false;
  }

  getArrayofNumber(num, type) {
    if (type == "positive")
      return Array(num).fill(0).map((x, i) => i)
    else
      return Array(5 - num).fill(0).map((x, i) => i)
  }

}
