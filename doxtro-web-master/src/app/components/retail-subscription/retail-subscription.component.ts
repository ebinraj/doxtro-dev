import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-retail-subscription',
  templateUrl: './retail-subscription.component.html',
  styleUrls: ['./retail-subscription.component.css']
})
export class RetailSubscriptionComponent implements OnInit {

  subscriptionFrm: FormGroup;
  subscriptionList : any[] = [];

  constructor(public _fb: FormBuilder,  public _appService: AppService) { 
    this.subscriptionFrm = this._fb.group({
      _id: '',
      subscriptionId: '',
      subscriptionTag: ['', Validators.required],
      userType: 'retail',
      subscriptionType: ['Subscription', Validators.required],
      price: ['', Validators.required],
      originalPrice: ['', Validators.required],
      description: [''],
      paymentCycle: ['', Validators.required],
      freeGPConsultations : [0, Validators.required],
      feePerEmployee: 0,
      consultationPerEmployee: 1,
      allowedFor: [[], Validators.required]
    });
  }

  ngOnInit() {
    this.getSubscriptionList();
  }

  doUpdateCreateSubscription({ value, valid }) {

    if (valid) {
      if (!this.subscriptionFrm.value._id) {

        debugger;
        console.log(this.subscriptionFrm.value);
        delete value._id;

        this._appService.retailSubscriptionCreate(value).toPromise().then(result => {
  
          alert("Subscription Created Successfully");
          this.subscriptionFrm.reset();
          this.getSubscriptionList();

          this.subscriptionFrm.patchValue({subscriptionType : 'Subscription'});

        }).catch(err => {
          console.log(err);
          alert("Some error occured");
        });

      }
      else{
        //update subscription

        this._appService.updateSubscription(value).toPromise().then(result =>{
          alert("Subscription Updated Successfully");
          this.subscriptionFrm.reset();
          this.getSubscriptionList();
          this.subscriptionFrm.patchValue({subscriptionType : 'Subscription'});

        }).catch(err =>{
          console.log(err);
          alert("Some error occured");
        });


      }
    }
    else{
      alert("Please select or fill the complete form");
    }


  }

  getSubscriptionList(){
    this._appService.getSubscriptionList("?userType=retail").toPromise().then(result =>{
      this.subscriptionList = result.data;
      console.log(result);
    }).catch(error =>{
      console.log(error);
    });
  }

  updateSubScription(ss) {
    debugger;
    this.subscriptionFrm.patchValue(ss);
  }

  deleteSubscription(id){
    this._appService.deleteSubscription(id).toPromise().then(result =>{
      this.getSubscriptionList();
    }).catch(err =>{
      alert(err);
    });
  }


    // Settings configuration 
    mySettings: IMultiSelectSettings = {
      enableSearch: true,
      checkedStyle: 'fontawesome',
      buttonClasses: 'btn btn-default btn-block',
      dynamicTitleMaxItems: 2,
      displayAllSelectedText: true
    };
  
    // Text configuration 
    myTexts: IMultiSelectTexts = {
      checkAll: 'Select all',
      uncheckAll: 'Unselect all',
      checked: 'item selected',
      checkedPlural: 'items selected',
      searchPlaceholder: 'Find',
      defaultTitle: 'Select Relatives',
      allSelected: 'All selected',
    };
  
    relativesList: IMultiSelectOption[] = [
      { id: 'Myself', name: 'Myself' },
      { id: 'Dad', name: 'Dad' },
      { id: 'Mom', name: 'Mom' },
      { id: 'Spouse', name: 'Spouse' },
      { id: 'Son', name: 'Son' },
      { id: 'Daughter', name: 'Daughter' },
      { id: 'Sister', name: 'Sister' },
      { id: 'Brother', name: 'Brother' },
      { id: 'Others', name: 'Others' },
      
    ];
  

}
