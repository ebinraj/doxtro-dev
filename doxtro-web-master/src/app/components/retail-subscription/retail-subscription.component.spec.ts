import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailSubscriptionComponent } from './retail-subscription.component';

describe('RetailSubscriptionComponent', () => {
  let component: RetailSubscriptionComponent;
  let fixture: ComponentFixture<RetailSubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailSubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
