import { AngularFireDatabase } from 'angularfire2/database';
import { ToastrService } from 'ngx-toastr';
import { UploadService } from './../../services/upload.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-slider-image',
  templateUrl: './slider-image.component.html',
  styleUrls: ['./slider-image.component.css']
})
export class SliderImageComponent implements OnInit {

  sliderPic;
  sliderPicList;
  sliderURL;

  constructor(public upSvc: UploadService, public db: AngularFireDatabase, public toastr: ToastrService) { }

  ngOnInit() {
    this.db.list("/BannerImages/").subscribe(result => {
      this.sliderPicList = result;
    });
  }

  SliderPicChange(event) {
    this.sliderPic = event.srcElement.files[0];
  }

  deleteSliderPic(key) {
    this.db.object("/BannerImages/" + key).remove().then(result => {
      this.toastr.success("Image deleted succssfully", "Success");
    }, reject => {
      this.toastr.error("Image deletion failed " + reject, "Error");
    }).catch(ex => {
      this.toastr.error("Image deletion failed " + ex, "Error");
    });
  }

  saveSliderData() {

    this.upSvc.pushBanner(this.sliderPic).then((result) => {

      firebase.database().ref("/BannerImages/" + new Date().getTime()).set({imgUrl : result.downloadURL, link : this.sliderURL}).then(suc => {
        this.sliderURL = "";
        this.toastr.success("Image Uploaded succssfully", "Success");
      }, rej => {
        this.toastr.error("Image Uploaded failed" + rej, "Error");
      });

    }, (rej) => {
      this.toastr.error("Image Uploaded failed", "Error");
    }).catch(err => {
      this.toastr.error("Image Uploaded failed " + err, "Error");
    });

  }

}
