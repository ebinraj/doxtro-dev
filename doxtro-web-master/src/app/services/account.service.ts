import { headerwithtoken } from './../common/headers';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { serviceUrls } from '../common/serviceUrls';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

@Injectable()
export class AccountService {

  constructor(private http: Http, public router: Router) { }

  doSignIn(body) {
    return this.http.post(serviceUrls.adminSignIn, body).map(res => res.json());
  }

  doLogout() {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    this.router.navigate(['/account/signin']);

  }

  forgotPassword(email) {
    return this.http.post(serviceUrls.forgotPassword, { 'emailid': email }).map(res => res.json());
  }

  resetPassword(body, token) {
    return this.http.post(serviceUrls.resetPassword, body, { headers: headerwithtoken(token) }).map(res => res.json());
  }



}
