import { serviceUrls } from './../common/serviceUrls';
import { AuthHttp } from 'angular2-jwt/angular2-jwt.js';
import { Injectable }          from '@angular/core';

import * as firebase from 'firebase';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
@Injectable()
export class MessagingService {
  messaging = firebase.messaging()
  currentMessage = new BehaviorSubject(null)
  constructor(public authhttp:AuthHttp) { }

  updateToken(token) {
    this.authhttp.post(serviceUrls.updateAdminDeviceId,{"deviceId" : token}).toPromise();
  }

  
  getPermission() {
      this.messaging.requestPermission()
      .then(() => {
        console.log('Notification permission granted.');
        return this.messaging.getToken()
      })
      .then(token => {
        console.log(token)
        this.updateToken(token);
      })
      .catch((err) => {
        console.log('Unable to get permission to notify.', err);
      });
    }
    receiveMessage() {
       this.messaging.onMessage((payload) => {
        console.log("Message received. ", payload);
        this.currentMessage.next(payload)
      });
    }
}