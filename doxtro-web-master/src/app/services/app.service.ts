import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { serviceUrls } from '../common/serviceUrls';
import { AuthHttp } from 'angular2-jwt/angular2-jwt.js';
import 'rxjs/add/operator/map';
import { contentHeaders } from '../common/headers';
import 'rxjs/Rx';

@Injectable()
export class AppService {

  public selectedDocId: String;
  public selectedDocDocuments: any[];
  public selectedDetails: any[];
  public counsultationQueryObject: any = {};

  public selectedConsultation: any;

  constructor(private authhttp: AuthHttp, public http: Http) {

  }

  getSelectedDocId() {
    return this.selectedDocId;
  }
  setSelectedDocId(id: String) {
    this.selectedDocId = id;
    return this.selectedDocId;
  }

  getSelectedConsultaion() {
    return JSON.parse(localStorage.getItem("selectedConsultation"));
  }
  setSelectedConsultation(data) {
    localStorage.setItem("selectedConsultation", JSON.stringify(data));
    return JSON.parse(localStorage.getItem("selectedConsultation"));
  }

  setSelectedDetails(doc) {
    this.selectedDetails = doc
  }

  getSelectedDetails() {
    return this.selectedDetails;
  }

  getSelectedDocDocuments() {
    return this.selectedDocDocuments;
  }
  setSelectedDocDocuments(list) {
    this.selectedDocDocuments = list;
    return this.selectedDocId;
  }

  getDoctorsList(search = "", pagecount: Number = 10, skip: Number = 0, sortfield) {
    let sf = sortfield.split('-');

    return this.authhttp.get(serviceUrls.doctorList + "&searchField=" + search + "&limit=" + pagecount + "&skip=" + skip + "&sortField=" + sf[0] + "&sortOrder=" + (sf[1] == 'asc' ? "1" : "-1"), { headers: contentHeaders }).map(res => res.json());
  }

  getDoctorDetails(id) {
    return this.authhttp.post(serviceUrls.doctorDetails, { "_id": id }, { headers: contentHeaders }).map(res => res.json());
  }

  getPatientsList(search = "", pagecount: Number = 10, skip: Number = 0, sortfield) {
    let sf = sortfield.split('-');
    return this.authhttp.get(serviceUrls.patientList + "&searchField=" + search + "&limit=" + pagecount + "&skip=" + skip + "&sortField=" + sf[0] + "&sortOrder=" + (sf[1] == 'asc' ? "1" : "-1"), { headers: contentHeaders }).map(res => res.json());
  }

  getTeamList(search = "", pagecount: Number = 10, skip: Number = 0, sortfield) {
    let sf = sortfield.split('-');
    return this.authhttp.get(serviceUrls.teamList + "&searchField=" + search + "&limit=" + pagecount + "&skip=" + skip + "&sortField=" + sf[0] + "&sortOrder=" + (sf[1] == 'asc' ? "1" : "-1"), { headers: contentHeaders }).map(res => res.json());
  }

  getConsultationList(search = "", pagecount: Number = 10, skip: Number = 0, sortfield, filterStatus, filterCompany) {
    let sf = sortfield.split('-')

    this.counsultationQueryObject["searchField"] = search;
    this.counsultationQueryObject["limit"] = pagecount;
    this.counsultationQueryObject["skip"] = skip;
    this.counsultationQueryObject["sortField"] = sf[0];
    this.counsultationQueryObject["sortOrder"] = (sf[1] == 'asc' ? "1" : "-1");

    let filterquery = "";
    if (filterStatus)
      filterquery = "&status=" + filterStatus;
    if (filterCompany)
        filterquery = "&companyName="+filterCompany;

    if (this.counsultationQueryObject["searchField"] != "") {
      return Observable.timer(0, 50000).flatMap(() => {
        return this.authhttp.get(serviceUrls.searchConsultation + "?searchField=" + this.counsultationQueryObject["searchField"] + "&limit=" + this.counsultationQueryObject["limit"] + "&skip=" + this.counsultationQueryObject["skip"] + "&sortField=" + this.counsultationQueryObject["sortField"] + "&sortOrder=" + this.counsultationQueryObject["sortOrder"] + filterquery, { headers: contentHeaders }).map(res => res.json());
      });

    } else {
      return Observable.timer(0, 50000).flatMap(() => {
        return this.authhttp.get(serviceUrls.consultationList + "?limit=" + this.counsultationQueryObject["limit"] + "&skip=" + this.counsultationQueryObject["skip"] + "&sortField=" + this.counsultationQueryObject["sortField"] + "&sortOrder=" + this.counsultationQueryObject["sortOrder"] + filterquery, { headers: contentHeaders }).map(res => res.json());
      });
    }
  }

  getPatientDetails(id) {
    return this.authhttp.post(serviceUrls.patientDetails, { "_id": id }, { headers: contentHeaders }).map(res => res.json());
  }

  verifyDocuments(body) {
    return this.authhttp.post(serviceUrls.verifyDocuments, body).map(res => res.json());
  }

  verifyDoctor(body) {
    return this.authhttp.post(serviceUrls.verifyDoctor, body).map(res => res.json());
  }

  remove(body) {
    return this.authhttp.post(serviceUrls.remove, body).map(res => res.json());
  }

  changeAccountStatus(body) {
    return this.authhttp.post(serviceUrls.block, body).map(res => res.json());
  }

  editPatient(body) {
    return this.authhttp.post(serviceUrls.editPatient, body).map(res => res.json());
  }

  editDoctor(body) {
    return this.authhttp.put(serviceUrls.editDoctor, body).map(res => res.json());
  }

  getLanguages() {
    return this.authhttp.get(serviceUrls.getLanguageList).map(res => res.json());
  }

  getAllSpecialization() {
    return this.authhttp.get(serviceUrls.getAllSpecialization).map(res => res.json());
  }

  updateDoctorProfile(value: any) {
    return this.authhttp.put(serviceUrls.updateDoctorProfile, value).map(res => res.json());
  }

  updateDoctorAvailability(value) {
    return this.authhttp.post(serviceUrls.updateDoctorAvailability, value).map(res => res.json());
  }

  updateDoctorsStatus(value) {
    return this.authhttp.post(serviceUrls.updateDoctorsStatus, value).map(res => res.json());
  }

  getDoctors(specializationCategory, language) {
    return this.authhttp.get(serviceUrls.getDoctors + "?specializationCategory=" + specializationCategory + "&language=" + language).map(res => res.json());
  }

  assignDoctor(body) {
    return this.authhttp.post(serviceUrls.assignDoctor, body).map(res => res.json());
  }

  createCoupon(body) {
    return this.authhttp.post(serviceUrls.createCoupon, body).map(res => res.json());
  }

  getCouponList() {
    return this.authhttp.get(serviceUrls.couponList).map(res => res.json());
  }

  updateCoupon(data, id) {
    return this.authhttp.put(serviceUrls.updateCoupon(id), data).map(res => res.json());
  }

  deleteCoupon(id) {
    return this.authhttp.delete(serviceUrls.deleteCoupon + "/" + id).map(res => res.json());
  }

  createPreAppliedCoupon(body) {
    return this.authhttp.post(serviceUrls.createPreAppliedCoupon, body).map(res => res.json());
  }

  updatePreAppliedCoupon(body) {
    return this.authhttp.post(serviceUrls.updatePreAppliedCoupon, body).map(res => res.json());
  }

  deletePreAppliedCoupon(id) {
    return this.authhttp.delete(serviceUrls.deletePreAppliedCoupon + "/" + id).map(res => res.json());
  }

  getListPreAppliedCoupon(isCorporate = false, companyName = null) {
    return this.authhttp.get(serviceUrls.getListPreAppliedCoupon + "?isCorporate=" + isCorporate + "&companyName=" + companyName).map(res => res.json());
  }

  getDegreeList() {
    return this.authhttp.get(serviceUrls.getDegreeList).map(res => res.json());
  }

  getAllSettings(item) {
    return this.authhttp.get(serviceUrls.getAllSettings + item).map(res => res.json());
  }

  createChargesSetting(body) {
    return this.authhttp.post(serviceUrls.createChargesSetting, body).map(res => res.json());
  }

  updateSplCharges(body) {
    return this.authhttp.post(serviceUrls.updateSplChargeSettings, body).map(res => res.json());
  }

  getConsultationComments(id) {
    return this.authhttp.get(serviceUrls.getConsultationComments + id).map(res => res.json());
  }

  createConsultationComments(body) {
    return this.authhttp.post(serviceUrls.createConsultationComments, body).map(res => res.json());
  }

  getOnlineDoctorList() {
    return Observable.timer(0, 30000).flatMap(() => {
      return this.authhttp.get(serviceUrls.getOnlineDoctorsList).map(res => res.json());
    });
  }

  getActivityLog() {
    return Observable.timer(0, 35000).flatMap(() => {
      return this.authhttp.get(serviceUrls.getActivityLog).map(res => res.json());
    });
  }

  getDoctorConsultationCount(id) {
    return this.authhttp.get(serviceUrls.getDoctorConsultationCount + "?doctorId=" + id).map(res => res.json());
  }

  getConsultationCountforDashboard() {

    return Observable.timer(0, 25000).flatMap(() => {
      return this.authhttp.get(serviceUrls.getConsultationCountforDashboard, { headers: contentHeaders }).map(res => res.json());
    });

  }

  getdoctorpaymentreport(body) {
    if (body.doctorId) {
      return this.authhttp.post(serviceUrls.getdoctorpaymentreport, body).map(res => res.json());
    }
    else {
      return this.authhttp.post(serviceUrls.getConsultationVsCategories, body).map(res => res.json());
    }
  }

  downloaddoctorpaymentreport(body) {
    return this.authhttp.post(serviceUrls.downloaddoctorpaymentreport, body).map(res => res.json());
  }

  getDoctorListForReport(startDate, endDate) {
    return this.authhttp.get(serviceUrls.getDoctorListForReport + "?startDate=" + startDate + "&endDate=" + endDate).map(res => res.json());
  }

  getSpecializationForReport(startDate, endDate) {
    let body = { "startDate": startDate, "endDate": endDate };
    return this.authhttp.post(serviceUrls.getSpecializationForReport, body).map(res => res.json());
  }

  getDocVsSpecs() {
    return this.authhttp.get(serviceUrls.getDocVsSpecs).map(res => res.json());
  }

  createCompanyinfo(body) {
    return this.authhttp.post(serviceUrls.createCompanyinfo, body).map(res => res.json());
  }
  updateCompanyinfo(body) {
    return this.authhttp.post(serviceUrls.updateCompanyinfo, body).map(res => res.json());
  }
  getCompanyInfo(id) {
    return this.authhttp.get(serviceUrls.getCompanyInfo + "?id=" + id).map(res => res.json());
  }
  getcompanyList(search = "", pagecount: Number = 10, skip: Number = 0, sortfield) {
    let sf = sortfield.split('-');

    return this.authhttp.get(serviceUrls.getcompanylist + "?searchField=" + search + "&limit=" + pagecount + "&skip=" + skip + "&sortField=" + sf[0] + "&sortOrder=" + (sf[1] == 'asc' ? "1" : "-1"), { headers: contentHeaders }).map(res => res.json());
  }
  getEmployeeList(search = "", pagecount: Number = 10, skip: Number = 0, sortfield, selectedCompany) {
    let sf = sortfield.split('-');
    if (selectedCompany.length == 0 || selectedCompany[0] == 0)
      return this.authhttp.get(serviceUrls.employeelist + "?searchField=" + search + "&limit=" + pagecount + "&skip=" + skip + "&sortField=" + sf[0] + "&sortOrder=" + (sf[1] == 'asc' ? "1" : "-1"), { headers: contentHeaders }).map(res => res.json());
    else
      return this.authhttp.get(serviceUrls.employeelist + "?searchField=" + search + "&companyName=" + selectedCompany[0] + "&limit=" + pagecount + "&skip=" + skip + "&sortField=" + sf[0] + "&sortOrder=" + (sf[1] == 'asc' ? "1" : "-1"), { headers: contentHeaders }).map(res => res.json());

  }
  deletecompany(id) {
    return this.authhttp.get(serviceUrls.deletecompany + "?_id=" + id).map(res => res.json());
  }
  deleteEmployee(id) {
    return this.authhttp.get(serviceUrls.deleteEmployee + "?_id=" + id).map(res => res.json());
  }

  createEmployee(body) {
    return this.authhttp.post(serviceUrls.createEmployee, body).map(res => res.json());
  }

  getcompanyNameList() {
    return this.authhttp.get(serviceUrls.getCompanyNameList).map(res => res.json());
  }

  getcompanyByName(name) {
    return this.authhttp.get(serviceUrls.getCompanyByName + "?company=" + name).map(res => res.json());
  }

  deleteGroup(body) {
    return this.authhttp.post(serviceUrls.deleteGroup, body).map(res => res.json());
  }

  subscriptionCreate(body) {
    return this.authhttp.post(serviceUrls.subscriptionCreate, body).map(res => res.json());
  }

  updateSubscription(body) {
    return this.authhttp.post(serviceUrls.subscriptionUpdate, body).map(res => res.json());
  }

  getSubscriptionList(query = "") {
    return this.authhttp.get(serviceUrls.getSubscriptionList + query).map(res => res.json());
  }

  deleteSubscription(id) {
    return this.authhttp.delete(serviceUrls.deleteSubscription + id).map(res => res.json());
  }

  retailSubscriptionCreate(body) {
    return this.authhttp.post(serviceUrls.retailSubscriptionCreate, body).map(res => res.json());
  }

  createOrUpdateHealthFeed(body) {
    return this.authhttp.post(serviceUrls.createOrUpdateHealthFeed, body).map(res => res.json());
  }

  getAllDoctors() {
    return this.authhttp.get(serviceUrls.getAllDoctors).map(res => res.json());
  }

  getNewsFeedById(id) {
    return this.http.get(serviceUrls.getNewsFeedById + id).map(res => res.json());
  }

  getAllHealthFeed(search = "", pagecount: Number = 10, skip: Number = 0) {
    return this.authhttp.get(serviceUrls.getAllHealthFeeds + "&search=" + search + "&limit=" + pagecount + "&skip=" + skip).map(res => res.json());
  }

  consultationsvsspecs(body) {
    return this.authhttp.post(serviceUrls.consultationsvsspecs, body).map(res => res.json());
  }

  getCosultationVsTime(body) {
    return this.authhttp.post(serviceUrls.getCosultationVsTime, body).map(res => res.json());
  }

  getRegistrationsByPartnerCorporates(body) {
    return this.authhttp.post(serviceUrls.getRegistrationsByDateRangeForCompany, body).map(res => res.json());
  }

  getCompanyListPartnerCorporates(body){
    return this.authhttp.post(serviceUrls.getCompanyListForPartnerCorporates, body).map(res => res.json());
  }

  getPackageCouponReport(body){
    return this.authhttp.post(serviceUrls.getPackageCouponReport, body).map(res => res.json());
  }

  downloadReports(body) {
    return this.authhttp.post(serviceUrls.downloadReports, body).map(res => res.json());
  }
}
