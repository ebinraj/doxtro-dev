import { Upload } from './../common/upload';
import { FirebaseListObservable } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable()
export class UploadService {

  constructor() { }

  private profilePic:String = '/Profile_Images';
  private documents : String = "/documents";
  private Banners : String = "/Banners"
  private HealthFeed  :String = "/HealthFeed"
  uploads: FirebaseListObservable<Upload[]>;
  
  pushUpload(upload: Upload) {
    let storageRef = firebase.storage().ref();
    return storageRef.child(`${this.profilePic}/${upload.name}`).put(upload);
  }

  pushDocuments(upload:Upload, name : String){
    let storageRef = firebase.storage().ref();
    return storageRef.child(`${this.documents}/${name}`).put(upload);
  }

  pushBanner(upload:Upload){
    let storageRef = firebase.storage().ref();
    return storageRef.child(`${this.Banners}/${upload.name}`).put(upload);
  }

  pushHealthFeed(upload:Upload, title:String = ""){
    let storageRef = firebase.storage().ref();
    return storageRef.child(`${this.HealthFeed}/${title}`).put(upload);
  }

}
