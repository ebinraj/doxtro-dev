import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decodeURI'
})
export class DecodeURIPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let dc = decodeURIComponent(value).split('/');
    return dc[dc.length-1].split('?')[0];
  }

}

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let v = value.toString().split('');
    if(v.length==4)
      return v[0] + v[1] + ":" + v[2]+v[3];
    else if(v.length==5)
      return value;
    else if(v.length==2)
      return "00:"+value;
    else if(v.length==3)
      return "0"+v[0] + ":" + v[1]+v[2];
    else
      return "00:00";
  }

}
