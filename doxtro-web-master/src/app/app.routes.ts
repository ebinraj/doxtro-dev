import { NewsFeedListComponent } from './components/news-feed-list/news-feed-list.component';
import { HealthFeedContainerComponent } from './components/health-feed-container/health-feed-container.component';
import { NewsFeedViewComponent } from './components/news-feed-view/news-feed-view.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { CompanyListComponent } from './components/company-list/company-list.component';
import { CorporateEmployeeComponent } from './components/corporate-employee/corporate-employee.component';
import { CorporateManagementComponent } from './components/corporate-management/corporate-management.component';
import { ReportsComponent } from './components/reports/reports.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ChatComponent } from './components/chat/chat.component';
import { CreateCouponComponent } from './components/create-coupon/create-coupon.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, PagePreventGuard } from './common/auth.guard';

import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { AppContainerComponent } from './components/app-container/app-container.component';
import { SigninComponent } from './components/signin/signin.component';
import { ConsultationsComponent } from './components/consultations/consultations.component';
import { DoctorsListComponent } from './components/doctors-list/doctors-list.component';
import { PatientsListComponent } from './components/patients-list/patients-list.component';
import { TeamListComponent } from './components/team-list/team-list.component';
import { SharedModule } from "./components/shared/shared.module";
import { DoctorUpdateComponent } from "./components/doctor-update/doctor-update.component";
import { DoctorsAvailabilityComponent } from './components/doctors-availability/doctors-availability.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ConsultationTimelineComponent } from './components/consultation-timeline/consultation-timeline.component';
import { NewsFeedCreateComponent } from './components/news-feed-create/news-feed-create.component';

const routes: Routes = [
  {
    path: 'account',
    children: [
      { path: '', redirectTo: 'signin', pathMatch: 'full' },
      { path: 'signin', component: SigninComponent },
      { path: 'changepassword/:token', component: ChangePasswordComponent },
    ]
  },
  {
    path: 'app',
    component: AppContainerComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
      { path: 'consultations', component: ConsultationsComponent, canActivate: [AuthGuard] },
      { path: 'doctorslist', component: DoctorsListComponent, canActivate: [AuthGuard] },
      { path: 'doctorslist/:id', component: DoctorsListComponent, canActivate: [AuthGuard] },
      { path: 'patientslist', component: PatientsListComponent, canActivate: [AuthGuard] },
      { path: 'teamlist', component: TeamListComponent, canActivate: [AuthGuard] },
      { path: 'doctorupdate/:id', component: DoctorUpdateComponent, canActivate: [AuthGuard] },
      { path: 'doctoravailabilityupdate/:id', component: DoctorsAvailabilityComponent, canActivate: [AuthGuard] },
      { path: 'createcoupon/:id', component: CreateCouponComponent, canActivate: [AuthGuard] },
      { path: 'chat/:id', component: ChatComponent, canActivate: [AuthGuard] },
      { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
      { path: 'consultation-timeline/:id', component: ConsultationTimelineComponent, canActivate: [AuthGuard] },
      { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard] },
      { path: 'corpomanage/:id', component: CorporateManagementComponent, canActivate: [AuthGuard] },
      { path: 'corpoemployee', component: CorporateEmployeeComponent, canActivate: [AuthGuard] },
      { path: 'company-list', component: CompanyListComponent, canActivate: [AuthGuard] },
      { path: 'employee-list/:company', component: EmployeeListComponent, canActivate: [AuthGuard] },
      { path: 'health-feed/create/:id', component: NewsFeedCreateComponent, canActivate: [AuthGuard], canDeactivate : [PagePreventGuard] },
      { path: 'health-feed/list', component: NewsFeedListComponent, canActivate: [AuthGuard] }
    ]
  },
  {
    path: 'healthfeed',
    component : HealthFeedContainerComponent,
    children: [
      { path: 'view/:id', component: NewsFeedViewComponent },
    ]
  },
  { path: '', redirectTo: 'app', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: false
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }