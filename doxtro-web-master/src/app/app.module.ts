import { AngularFireDatabase } from 'angularfire2/database';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { AppRoutingModule } from './app.routes';
import { AuthGuard, PagePreventGuard } from './common/auth.guard';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS, provideAuth } from 'angular2-jwt/angular2-jwt.js';
import { MomentModule } from 'angular2-moment';
import { ModalModule, BsDatepickerModule, TabsModule } from 'ngx-bootstrap';

import { AngularFireModule } from 'angularfire2';
import * as firebase from 'firebase';
import { environment } from '../environments/environment';
import { MultiselectDropdownModule } from 'angular-4-dropdown-multiselect';
import {SharedFileUploadModule}  from './modules/app.fileupload.module';

import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from "./components/shared/shared.module";
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { SigninComponent } from './components/signin/signin.component';
import { AppContainerComponent } from './components/app-container/app-container.component';

import { AccountService } from './services/account.service';
import { AppService } from './services/app.service';
import { UploadService } from './services/upload.service';
import { MessagingService } from './services/messaging.service';

import { ConsultationsComponent } from './components/consultations/consultations.component';
import { DoctorsListComponent } from './components/doctors-list/doctors-list.component';
import { DoctorProfileComponent } from './components/doctor-profile/doctor-profile.component';
import { DecodeURIPipe, TimeFormatPipe } from './pipes/decode-uri.pipe';
import { PatientsListComponent } from './components/patients-list/patients-list.component';
import { TeamListComponent } from './components/team-list/team-list.component';
import { PatientProfileComponent } from './components/patient-profile/patient-profile.component';
import { DocumentVerificationComponent } from './components/document-verification/document-verification.component';
import { TeamProfileComponent } from './components/team-profile/team-profile.component';
import { PatientEditProfileComponent } from './components/patient-edit-profile/patient-edit-profile.component';
import { DoctorUpdateComponent } from './components/doctor-update/doctor-update.component';
import { DoctorsAvailabilityComponent } from './components/doctors-availability/doctors-availability.component';

import { ToastrModule } from 'ngx-toastr';
import { CreateCouponComponent } from './components/create-coupon/create-coupon.component';
import { ChatComponent } from './components/chat/chat.component';
import { SettingsComponent } from './components/settings/settings.component';
import { PreappliedCouponComponent } from './components/preapplied-coupon/preapplied-coupon.component';
import { ConsultationChargesComponent } from './components/consultation-charges/consultation-charges.component';
import { SliderImageComponent } from './components/slider-image/slider-image.component';
import { ConsultationTimelineComponent } from './components/consultation-timeline/consultation-timeline.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { DoctorPaymentReportComponent } from './components/doctor-payment-report/doctor-payment-report.component';
import { ReportsComponent } from './components/reports/reports.component';
import { DoctorVsSpecializationComponent } from './components/doctor-vs-specialization/doctor-vs-specialization.component';

import { NvD3Module } from 'ng2-nvd3';

// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';
import { CorporateManagementComponent } from './components/corporate-management/corporate-management.component';
import { CorporateEmployeeComponent } from './components/corporate-employee/corporate-employee.component';
import { CompanyListComponent } from './components/company-list/company-list.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { CreateSubscriptionComponent } from './components/create-subscription/create-subscription.component';
import { RetailSubscriptionComponent } from './components/retail-subscription/retail-subscription.component';
import { NewsFeedCreateComponent } from './components/news-feed-create/news-feed-create.component';
import { NewsFeedListComponent } from './components/news-feed-list/news-feed-list.component';
import { NewsFeedViewComponent } from './components/news-feed-view/news-feed-view.component';

import { QuillModule } from 'ngx-quill';
import { FacebookModule } from 'ngx-facebook';
import { HealthFeedContainerComponent } from './components/health-feed-container/health-feed-container.component';
import { HealthFeedFbVideosComponent } from './components/health-feed-fb-videos/health-feed-fb-videos.component';
import { EmbedVideo } from 'ngx-embed-video';
import { ReferralSettingsComponent } from './components/referral-settings/referral-settings.component';
import { ConsultationsVsSpecializationComponent } from './components/reports/consultations-vs-specialization/consultations-vs-specialization.component';
import { ConsultationsVsTimeComponent } from './components/reports/consultations-vs-time/consultations-vs-time.component';
import { PartnersVsCorporatesComponent } from './components/reports/partners-vs-corporates/partners-vs-corporates.component';
import { PackageVsCouponComponent } from './components/reports/package-vs-coupon/package-vs-coupon.component';

firebase.initializeApp(environment.firebase);

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenName: 'token',
    headerName:'Authorization',
    headerPrefix :'',
    noTokenScheme:true,
		tokenGetter: (() => localStorage.getItem('token') ? localStorage.getItem('token') : "NoTokenFound"),
		globalHeaders: [{'Content-Type':'application/json'}],
	}), http, options);
}


@NgModule({
  declarations: [
    AppContainerComponent,
    AppComponent,
    SidebarComponent,
    NavbarComponent,
    DashboardComponent,
    SigninComponent,
    PageNotFoundComponent,
    ConsultationsComponent,
    DoctorsListComponent,
    DoctorProfileComponent,
    DecodeURIPipe,
    TimeFormatPipe,
    PatientsListComponent,
    TeamListComponent,
    PatientProfileComponent,
    DocumentVerificationComponent,
    TeamProfileComponent,
    PatientEditProfileComponent,
    DoctorUpdateComponent,
    DoctorsAvailabilityComponent,
    CreateCouponComponent,
    ChatComponent,
    SettingsComponent,
    PreappliedCouponComponent,
    ConsultationChargesComponent,
    SliderImageComponent,
    ConsultationTimelineComponent,
    ChangePasswordComponent,
    DoctorPaymentReportComponent,
    ReportsComponent,
    DoctorVsSpecializationComponent,
    CorporateManagementComponent,
    CorporateEmployeeComponent,
    CompanyListComponent,
    EmployeeListComponent,
    CreateSubscriptionComponent,
    RetailSubscriptionComponent,
    NewsFeedCreateComponent,
    NewsFeedListComponent,
    NewsFeedViewComponent,
    HealthFeedContainerComponent,
    HealthFeedFbVideosComponent,
    ReferralSettingsComponent,
    ConsultationsVsSpecializationComponent,
    ConsultationsVsTimeComponent,
    PartnersVsCorporatesComponent,
    PackageVsCouponComponent
  ],
  entryComponents: [
    DoctorProfileComponent,
    PatientProfileComponent,
    DocumentVerificationComponent,
    PatientEditProfileComponent,
    HealthFeedFbVideosComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    SharedModule,
    AppRoutingModule,
    MomentModule,
    ModalModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    MultiselectDropdownModule,
    SharedFileUploadModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    NvD3Module,
    QuillModule,
    FacebookModule.forRoot(),
    EmbedVideo.forRoot(),
  ],
  providers: [AccountService, AppService,UploadService, MessagingService, AuthGuard, PagePreventGuard, AngularFireDatabase, AuthHttp, {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
