import { Headers } from '@angular/http';

export const contentHeaders = new Headers();
contentHeaders.append('Accept', 'application/json');
contentHeaders.append('Content-Type', 'application/json');

export let headerwithtoken = function(token){
    let ct = new Headers();
    ct.append('Authorization',token);

    return ct;
}