import { Injectable } from '@angular/core';
import { Router, CanActivate, CanDeactivate } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt/angular2-jwt.js';
import { Observable } from 'rxjs/Observable';

export interface ComponentCanDeactivate {
  canDeactivate: () => boolean | Observable<boolean>;
}

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate() {
    if (tokenNotExpired())
      return true;  

    this.router.navigate(['/account/signin']);
    return false;
  }
}

@Injectable()
export class PagePreventGuard implements CanDeactivate<ComponentCanDeactivate>{
  constructor(){}

  canDeactivate(component: ComponentCanDeactivate): boolean | Observable<boolean> {
    // if there are no pending changes, just allow deactivation; else confirm first
    debugger;
    return component.canDeactivate() ?
      true :
      
      confirm('WARNING: Are you sure, you want to leave this page ?');
  }
}