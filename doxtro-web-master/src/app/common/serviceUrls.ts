import { environment } from '../../environments/environment';
export const serviceUrls = {
    adminSignIn: environment.host + environment.apiversion + "admin/signIn",
    forgotPassword : environment.host + environment.apiversion + "admin/forgotPassword",
    resetPassword : environment.host + environment.apiversion + "admin/resetPassword",
    doctorList : environment.host + environment.apiversion + "admin/getUsers?tag=doctor",
    doctorDetails : environment.host + environment.apiversion + "doctor/getProfile",
    patientList : environment.host + environment.apiversion + "admin/getUsers?tag=patient",
    teamList : environment.host + environment.apiversion + "admin/getUsers?tag=admin",
    consultationList : environment.host + environment.apiversion + "consultation/getConsultations",
    patientDetails : environment.host + environment.apiversion + "patient/getProfile",
    verifyDocuments : environment.host + environment.apiversion + "doctor/updateDocumentStatus",
    verifyDoctor : environment.host + environment.apiversion + "doctor/updateStatus",
    remove :environment.host + environment.apiversion + "admin/remove",
    block :environment.host + environment.apiversion + "admin/changeAccountStatus",
    editPatient : environment.host + environment.apiversion + "patient/profile",
    editDoctor : environment.host + environment.apiversion + "doctor/profile",
    getLanguageList : environment.host + environment.apiversion + "settings/getLanguages",
    getAllSpecialization : environment.host + environment.apiversion + "specialization/getAllSpecialization",
    updateDoctorProfile : environment.host + environment.apiversion + "doctor/profile",
    updateDoctorAvailability : environment.host + environment.apiversion + "availability/create",
    updateDoctorsStatus : environment.host + environment.apiversion + "doctor/updateStatus",
    searchConsultation : environment.host + environment.apiversion + "consultation/search",
    getDoctors : environment.host + environment.apiversion + "admin/getDoctors",
    assignDoctor : environment.host + environment.apiversion + "consultation/sendRequest",
    updateAdminDeviceId : environment.host + environment.apiversion + "admin/updateDeviceId",
    createCoupon : environment.host + environment.apiversion + "coupon/create",
    couponList : environment.host + environment.apiversion + "coupon/getCouponList",
    updateCoupon : function(id) {return environment.host + environment.apiversion + "coupon/"+ id +"/update"},
    deleteCoupon : environment.host + environment.apiversion + "coupon/deletecoupon",
    createPreAppliedCoupon : environment.host + environment.apiversion + "preappliedcoupon/save",
    updatePreAppliedCoupon : environment.host + environment.apiversion + "preappliedcoupon/update",
    deletePreAppliedCoupon : environment.host + environment.apiversion + "preappliedcoupon/delete",
    getListPreAppliedCoupon : environment.host + environment.apiversion + "preappliedcoupon/list",
    getDegreeList : environment.host + environment.apiversion + "settings/getDegree",
    getAllSettings : environment.host + environment.apiversion + "settings/find?category=",
    createChargesSetting : environment.host + environment.apiversion + "settings/create",
    updateSplChargeSettings : environment.host + environment.apiversion + "settings/update",
    getConsultationComments : environment.host + environment.apiversion + "comments/find?consultationId=",
    createConsultationComments : environment.host + environment.apiversion + "comments/create",
    getOnlineDoctorsList : environment.host + environment.apiversion + "doctor/getOnlineDoctors",
    getActivityLog : environment.host + environment.apiversion + "activityLog/list?limit=20&visible=true",
    getDoctorConsultationCount : environment.host + environment.apiversion + "consultation/getConsultationCounts",
    getConsultationCountforDashboard : environment.host + environment.apiversion + "consultation/getCounts",
    getdoctorpaymentreport : environment.host + environment.apiversion + "admin/getDoctorReport",
    downloaddoctorpaymentreport : environment.host + environment.apiversion + "admin/generateReport",
    getDoctorListForReport : environment.host + environment.apiversion + "admin/getDoctorList",
    getSpecializationForReport : environment.host + environment.apiversion + "reports/getspecializationList",
    getDocVsSpecs : environment.host + environment.apiversion + "reports/getDocvsSpecs",
    createCompanyinfo  : environment.host + environment.apiversion + "companyInfo/create",
    updateCompanyinfo  : environment.host + environment.apiversion + "companyInfo/updateCompany",
    getCompanyInfo : environment.host + environment.apiversion + "companyInfo/getCompanyById",
    getcompanylist : environment.host + environment.apiversion + "companyInfo/getCompanyList",
    employeelist : environment.host + environment.apiversion + "employees/list",
    deletecompany: environment.host + environment.apiversion + "companyInfo/delete",
    deleteEmployee : environment.host + environment.apiversion + "employees/delete",
    createEmployee :  environment.host + environment.apiversion + "employees/create",
    getCompanyNameList :  environment.host + environment.apiversion + "companyInfo/getCompanyNames",
    getCompanyByName :  environment.host + environment.apiversion + "companyInfo/getCompanyByName",
    deleteGroup :  environment.host + environment.apiversion + "companyInfo/removeGroup",
    subscriptionCreate :  environment.host + environment.apiversion + "subscription/create",
    getSubscriptionList : environment.host + environment.apiversion + "subscription/find",
    subscriptionUpdate :  environment.host + environment.apiversion + "subscription/update",
    deleteSubscription : environment.host + environment.apiversion + "subscription/delete?_id=",
    retailSubscriptionCreate : environment.host + environment.apiversion + "subscription/createRetail",
    createOrUpdateHealthFeed : environment.host + environment.apiversion + "healthFeed/createOrUpdate",
    getAllDoctors : environment.host + environment.apiversion + "doctor/getAllDoctors",
    getNewsFeedById : environment.host + environment.apiversion + "healthFeed/getById/",
    getAllHealthFeeds : environment.host + environment.apiversion + "healthFeed/all?listType=recent",
    getConsultationVsCategories : environment.host + environment.apiversion + "reports/getConsultationVsCategories",
    consultationsvsspecs : environment.host + environment.apiversion + "reports/getConsultationVsSpecs",
    getCosultationVsTime : environment.host + environment.apiversion + "reports/getCosultationVsTime",
    getRegistrationsByDateRangeForCompany : environment.host + environment.apiversion + "reports/getRegisteredPartnerCorporateEmployees",
    getCompanyListForPartnerCorporates : environment.host + environment.apiversion + "reports/getPartnerCorporatesList",
    getPackageCouponReport : environment.host + environment.apiversion + "reports/getPackageCouponReport",
    downloadReports : environment.host + environment.apiversion + "reports/downloadReport",
}