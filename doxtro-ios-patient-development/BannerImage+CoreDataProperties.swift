//
//  BannerImage+CoreDataProperties.swift
//  
//
//  Created by Ayush Yadav on 15/01/18.
//
//

import Foundation
import CoreData


extension BannerImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BannerImage> {
        return NSFetchRequest<BannerImage>(entityName: "BannerImage")
    }

    @NSManaged public var bannerImage: NSData?

}
