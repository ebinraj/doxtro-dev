//
//  ConsultationList+CoreDataProperties.swift
//  
//
//  Created by Ayush Yadav on 14/01/18.
//
//

import Foundation
import CoreData


extension ConsultationList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ConsultationList> {
        return NSFetchRequest<ConsultationList>(entityName: "ConsultationList")
    }

    @NSManaged public var consultationArray: NSData?

}
