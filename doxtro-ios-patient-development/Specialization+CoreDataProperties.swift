//
//  Specialization+CoreDataProperties.swift
//  
//
//  Created by Ayush Yadav on 16/01/18.
//
//

import Foundation
import CoreData


extension Specialization {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Specialization> {
        return NSFetchRequest<Specialization>(entityName: "Specialization")
    }

    @NSManaged public var specializationDescription: String?
    @NSManaged public var specializationImage: NSData?
    @NSManaged public var specializationName: String?
    @NSManaged public var specilizationImageUrl: String?

}
