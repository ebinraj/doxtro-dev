//
//  DPPlannedDoctorList.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 12/02/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class DPDoctorList {
    var ratingByAdmin : Int = Int()
     var _id : String = String()
     var ratingUserCount : Int = Int()
     var firstName : String = String()
     var averageRating : Int = Int()
    var qualification : [Qualification] = []
    var category : [String] = []
    var profilePic : String = String()
    var experience : String = String()
    var consultingFee : Int = Int()
    init(dict : JSON){
        self.ratingByAdmin = dict["ratingByAdmin"].int ?? Int()
        self._id = dict["_id"].string ?? String()
        self.ratingUserCount = dict["ratingUserCount"].int ?? Int()
        self.firstName = dict["firstName"].string ?? String()
        self.averageRating = dict["averageRating"].int ?? Int()
        self.profilePic = dict["profilePic"].string ?? String()
        self.experience = dict["experience"].string ?? String()
        self.consultingFee = dict["consultingFee"].int ?? Int()
        if let qualificationArray = dict["qualification"].array{
            for value in qualificationArray{
                qualification.append(Qualification(dict: JSON(value.dictionaryValue)))
            }
        }
        if let workForArray = dict["category"].array{
            for value in workForArray{
                category.append(value.stringValue)
            }
        }
    }
}
/*
 {
 "ratingByAdmin" : 10,
 "_id" : "58be82f0ff37ce5b76e8a883",
 "ratingUserCount" : 151,
 "firstName" : "Dr. Sumit Kumar",
 "category" : [
 "General Physician"
 ],
 "averageRating" : 3,
 "qualification" : [
 {
 "_id" : "5a27b6e135575872e8a8b169",
 "type" : "UG",
 "college" : "",
 "degree" : "Others",
 "additionalData" : ""
 },
 {
 "_id" : "5a27b6e135575872e8a8b168",
 "type" : "PG",
 "college" : "",
 "degree" : "Others",
 "additionalData" : ""
 }
 ]
 },
 */
