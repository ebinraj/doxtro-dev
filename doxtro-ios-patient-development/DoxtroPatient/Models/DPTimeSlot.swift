//
//  DPTimeSlot.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 16/02/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
class TimeSlot {
    var availabilityId : String = ""
    var endTime : Int = 0
    var startTime : Int = 0
    var duration : String = ""
    init(dict : JSON) {
       self.availabilityId = dict["availabilityId"].string ?? ""
        self.duration = dict["duration"].string ?? ""
        self.startTime = dict["startTime"].int ?? 0
        self.endTime = dict["endTime"].int ?? 0
    }
}
//"availabilityId" : "5a869c4db4303f78a870adae",
//"endTime" : 2359,
//"startTime" : 0,
//"duration" : "5pm-6pm"

