//
//  DPUser.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 25/07/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
let USER_INFO_KEY = "user_info"
class DPUser: NSObject,NSCoding {
    
    private let ID_key = "_id"
    private let accountActivationTime_key = "accountActivationTime"
    private let accountStatus_key   = "accountStatus"
    private let tag_key = "tag"
    //let status_key  = "status"
    private let deviceId_key    = "deviceId"
    private let token_key       = "token"
    private let mobilNume_key = "mobile"
    private let firstName_key = "firstName"
    private let ageKey = "age"
    private let genderKey = "gender"
    private let heightKey = "height"
    private let weightKey = "weight"
    private let isCorporate_key = "isCorporate"
    private let companyData_key = "companyData"
    private let support_key = "SupportNumber"
    var name: String?
    var emailId: String?
    var mobileNum: String?
    var userId: String?
    var accountActivationTime: String?
    var accountStatus: String?
    var tag: String?
    var age : String?
    var gender : String?
    var height : String?
    var weight : String?
    var deviceId: String?
    var token: String?
    var profileUrl : String?
    var ayush : String?
    var referralCode : String?
    var isCorporate: Bool?
    var supportNumber: String?
    //var companyDetails: CompanyData?
    init(data : [String : JSON]) {
        super.init()
        userId                = data[ID_key]?.stringValue ?? ""
        referralCode = data["referralCode"]?.stringValue ?? ""
        accountActivationTime = data[accountActivationTime_key]?.stringValue ?? ""
        accountStatus   = data[accountStatus_key]?.stringValue ?? ""
        tag             = data[tag_key]?.stringValue ?? ""
        // status          = data[status_key]?.string ?? ""
        deviceId        = data[deviceId_key]?.stringValue ?? ""
        token           = data[token_key]?.stringValue ?? DPWebServicesManager.sharedManager.user?.token ?? ""
        name            = data[firstName_key]?.stringValue ?? ""
        mobileNum       = data[mobilNume_key]?.stringValue ?? ""
        age             = data[ageKey]?.stringValue ?? ""
        emailId         = data[emailId_key]?.stringValue ?? ""
        gender         = data[genderKey]?.stringValue ?? ""
        height         = data[heightKey]?.stringValue ?? ""
        weight         = data[weightKey]?.stringValue ?? ""
        profileUrl     = data["profilePic"]?.stringValue ?? ""
        isCorporate    = data[isCorporate_key]?.boolValue
        if let companyDataDict = data[companyData_key]?.dictionaryValue {
            supportNumber = companyDataDict[support_key]?.string ?? ""
            //companyDetails = CompanyData(companyDataDict)
        }
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userId, forKey: ID_key)
        aCoder.encode(referralCode, forKey: "referralCode")
        aCoder.encode(accountActivationTime,    forKey: accountActivationTime_key)
        aCoder.encode(accountStatus,    forKey: accountStatus_key)
        aCoder.encode(tag,    forKey: tag_key)
        //  aCoder.encode(status,    forKey: status_key)
        aCoder.encode(deviceId,    forKey: deviceId_key)
        aCoder.encode(token,    forKey: token_key)
        aCoder.encode(mobileNum, forKey: mobilNume_key)
        aCoder.encode(name, forKey: firstName_key)
        aCoder.encode(age, forKey: ageKey)
        aCoder.encode(emailId, forKey: emailId_key)
        aCoder.encode(gender, forKey: genderKey)
        aCoder.encode(height, forKey: heightKey)
        aCoder.encode(weight, forKey: weightKey)
        aCoder.encode(profileUrl,forKey : "profilePic")
        aCoder.encode(isCorporate,forKey : isCorporate_key)
        aCoder.encode(supportNumber,forKey : support_key)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.userId =  aDecoder.decodeObject(forKey: ID_key) as? String
        self.referralCode = aDecoder.decodeObject(forKey:"referralCode") as? String
        self.accountActivationTime = aDecoder.decodeObject(forKey: accountActivationTime_key) as? String
        self.accountStatus = aDecoder.decodeObject(forKey: accountStatus_key) as? String
        self.tag = aDecoder.decodeObject(forKey: tag_key) as? String
        //aDecoder.decodeObject(forKey: status_key)
        self.deviceId = aDecoder.decodeObject(forKey: deviceId_key) as? String
        self.token = aDecoder.decodeObject(forKey: token_key) as? String
        self.mobileNum = aDecoder.decodeObject(forKey: mobilNume_key) as? String
        self.name = aDecoder.decodeObject(forKey: firstName_key) as? String
        self.age = aDecoder.decodeObject(forKey: ageKey) as? String
        self.emailId = aDecoder.decodeObject(forKey: emailId_key) as? String
        self.gender = aDecoder.decodeObject(forKey: genderKey) as? String
        self.height = aDecoder.decodeObject(forKey: heightKey) as? String
        self.weight = aDecoder.decodeObject(forKey: weightKey) as? String
        self.profileUrl = aDecoder.decodeObject(forKey : "profilePic") as? String
        self.isCorporate = aDecoder.decodeObject(forKey: isCorporate_key) as? Bool
        self.supportNumber = aDecoder.decodeObject(forKey: support_key) as? String
//        if let companyData = aDecoder.decodeObject(forKey: companyData_key) as? CompanyData {
//            self.companyDetails = companyData
//        }
    }
    func save(){
        let userData = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(userData, forKey: USER_INFO_KEY)
        UserDefaults.standard.synchronize()
    }
    class func getUserInfoFromUserDefaults()->DPUser?{
        if let userData = UserDefaults.standard.object(forKey: USER_INFO_KEY) as? Data{
            return NSKeyedUnarchiver.unarchiveObject(with: userData) as? DPUser
        }else{
            return nil
        }
    }
    class func cleanUser(){
        UserDefaults.standard.removeObject(forKey: USER_INFO_KEY)
        UserDefaults.standard.synchronize()
    }
    class func saveReminderStatus(status : Bool){
        UserDefaults.standard.set(status, forKey: REMINDER_DEFAULT_KEY)
        UserDefaults.standard.synchronize()
    }
    class func getReminderStatus()->Bool?{
        if let status = UserDefaults.standard.value(forKey: REMINDER_DEFAULT_KEY) as? Bool{
            return status
        }
        return nil
    }
}
class DPSpecializationList{
    let categoryKey = "category"
    let descriptionKey = "description"
    
    var specializationCategory : String?
    var specializationCategoryDescription : String?
    var categoryImageUrl : URL?
    init(dict : [String : JSON]) {
        self.specializationCategory = dict[categoryKey]?.string ?? ""
        self.specializationCategoryDescription = dict[descriptionKey]?.string ?? ""
        self.categoryImageUrl = URL(string: (dict["imageUrl"]?.stringValue) ?? "")
    }
    init(dict:[String : String]) {
        self.specializationCategory = dict[categoryKey]
    }
}
struct CompanyData {
    var _id: String
    var companyName: String
    var planType: String
    var createdAt: String
    var corporatePayType: String
    var updatedAt: String
    var companyDescription: String
    var supportNumber: String
    init(_ dict: [String : JSON]) {
        self._id = dict[ID_key]?.string ?? ""
        self.companyName = dict["companyName"]?.string ?? ""
        self.planType = dict["planType"]?.string ?? ""
        self.createdAt = dict["createdAt"]?.string ?? ""
        self.corporatePayType = dict["corporatePayType"]?.string ?? ""
        self.companyDescription = dict["companyDescription"]?.string ?? ""
        self.updatedAt = dict["updatedAt"]?.string ?? ""
        self.supportNumber = dict["supportNumber"]?.string ?? ""
    }
}
