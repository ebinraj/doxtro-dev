//
//  DPRequestManager.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 25/07/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import Alamofire

struct LoginRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
    self.endPoint = kSignIn
    self.parameters = params
    self.requestType = .post
    self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

struct SignupRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
        self.endPoint = kSignUp
        self.parameters = params
        self.requestType = .post
    }
}

struct VerifyOtpRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
        self.endPoint = kVerification
        self.parameters = params
        self.requestType = .post
    }
}
struct UpdateProfileRequest : DPWebRequestProtocol{
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.endPoint = KProfileUpdate
        self.parameters = params
        self.requestType = .put
    }
}

struct ResendOtpRequest : DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String :AnyObject]) {
        self.parameters = params
        self.endPoint = kSignIn
        self.requestType = .post
         self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

struct SpecializationRequest : DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init() {
        self.endPoint = KGetSpecializationList
    }
}
struct GetRelativeListRequest : DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.endPoint = KGetRelativeList
        self.parameters = params
        self.requestType = .get
    }
}
struct createMemberRequest : DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.endPoint = KCreateMembers
        self.parameters = params
        self.requestType = .post
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct GetPreferredLanguageListRequest : DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init() {
     self.endPoint = KGetPreferredLanguageList
    }
}

struct SubmitDetailsForConsultationRequest :DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.endPoint = KIntiateConsultation
        self.parameters = params
        self.requestType = .post
         self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct MakePaymentRequest : DPWebRequestProtocol {
     var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "consultation/makePayment"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

struct GetRecentConsultationrequest : DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.endPoint = "consultation/getConsultations"
        self.requestType = .get
    }
}
struct DoctorProfileRequest :DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(prams : [String:AnyObject]) {
        self.parameters = prams
        self.endPoint = "consultation/getDoctorProfile"
    }
}
struct ApplyCouponRequest :  DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "coupon/applyCoupon"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct GetRsaRequest : DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "payment/getRSA"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct GetPaymentOptions : DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        //command=%@&currency=%@&amount=%@&access_code=%@&customer_identifier=%@
        self.parameters = params
        self.requestType = .post
        self.endPoint = CCAvenue.Credential.PAYMENT_OPTION_PATH
        self.baseUrl = URL(string: CCAvenue.URLS.LIVE)!
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded","User-Agent" : "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Referer":CCAvenue.URLS.LIVE]
    }
}
struct PayementCaptureRequest : DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "payment/capture"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        
    }
}
struct UpdateCurrentUserDeviceIdRequest : DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "patient/updateDeviceId"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        
    }
}
struct GetConsultationInfo : DPWebRequestProtocol{
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "consultation/getConsultationDetails"
    }
}
struct GetFeedbackOfConsulatation : DPWebRequestProtocol{
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "consultation/getfeedbackConsultations"
    }
}
struct SubmitFeedback : DPWebRequestProtocol{
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "consultation/submitFeedback"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        
    }
}
struct GetTransactionHistory : DPWebRequestProtocol{
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "payment/transactionHistory"
    }
}
struct GetChecksumPaaytmRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "payment/generateChecksum"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct PaytmPaymentCaptureRequest : DPWebRequestProtocol  {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "payment/paytmCapture"
        self.parameterEncoding = URLEncoding.default
        self.headers = ["Accept" : "application/json"]
    }
}
struct CreateHelathRecordRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "healthRecords/create"
        self.parameterEncoding = URLEncoding.httpBody
        self.headers = ["Content-Type" : "application/json"]
    }
}
struct FetchHealthRecordRequest : DPWebRequestProtocol{
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "healthRecords/getAllRecords"
    }
}
struct FetchHealthRecordSeprateRequest : DPWebRequestProtocol{
        var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
        init(params : [String : AnyObject]) {
            self.parameters = params
            self.requestType = .get
            self.endPoint = "healthRecords/find"
        }
}
struct DeleteHealthRecordRequest : DPWebRequestProtocol{
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "healthRecords/delete"
    }
}
struct FollowUpConsultationRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "consultation/startFollowup"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct UpdateRelativeMemberInfoRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .put
        self.endPoint = "relatives/update"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct GetAllSpecialization : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "patient/getAllSpecialization"
        //self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct UserLogoutRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "patient/logout"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct StartFreeConsultationRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "payment/startFreeConsultation"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct ConsultationDeleteRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "consultation/delete"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}

struct UpdateProfielRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "patient/getProfile"
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct GetPackageRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "subscription/findUserSubscription"
        //self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct PackageListRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "subscription/listUserSubscriptions"
        //self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct OneTimePackageRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = kOneTimePackage
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct SubscriptionsRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = kSubscriptions
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct CancelSubscriptionsRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = kCancelSubscriptions
        self.headers = ["Content-Type" : "application/x-www-form-urlencoded"]
    }
}
struct PlannedConsultationDoctorList : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "doctor/find"
    }
}
///doctor/getAvailability?doctorId=5a66eb9f71c2a60e122747cf&date=2018-02-19
struct GetDoctorSlotRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .get
        self.endPoint = "doctor/getAvailability"
    }
}
struct PlannedConsultationRequest : DPWebRequestProtocol {
    var baseRequest  : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.requestType = .post
        self.endPoint = "plannedConsultation/start"
    }
}
struct DeactiveUserAccountRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.endPoint = "patient/deactivateAccount"
        self.requestType = .post
    }
}
struct GetDoxtroCashRequest : DPWebRequestProtocol{
var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.endPoint = "patient/getDoxtroCash"
        self.requestType = .get
    }
}
struct AddDoxtroCashRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.parameters = params
        self.endPoint = "patient/addMoney"
        self.requestType = .post
    }
}
struct GetAlHealthFeedRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject],endPoint : String) {
        self.endPoint = endPoint
        self.parameters = params
        self.requestType = .get
    }
}
struct RefreshConsultationPaymentCardRequest :  DPWebRequestProtocol{
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
        self.endPoint = "consultation/getConsultationCharges"
        self.parameters = params
        self.requestType = .get
    }
}
struct RemoveCouponRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
        self.endPoint = "coupon/removeCoupon"
        self.parameters = params
        self.requestType = .post
    }
}
struct GetReferlCodeRequest :  DPWebRequestProtocol{
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
        self.endPoint = "patient/getReferralCode"
        self.parameters = params
        self.requestType = .get
    }
}
struct GetSubscriptionChargesRequest : DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.endPoint = "subscription/getSubscriptionCharges"
        self.parameters = params
        self.requestType = .post
    }
}
struct GetDoctorProfileRequest: DPWebRequestProtocol {
    var baseRequest: DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String : AnyObject]) {
        self.endPoint = "doctor/getProfile"
        self.parameters = params
        self.requestType = .post
    }
}
struct GetCompanyListRequest : DPWebRequestProtocol {
    var baseRequest : DPWebServiceRequest = DPWebServiceRequest()
    init(params : [String:AnyObject]) {
        self.endPoint = "companyInfo/getCompanyList"
        self.parameters = params
        self.requestType = .get
    }
}
