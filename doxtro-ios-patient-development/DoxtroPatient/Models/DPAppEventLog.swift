//
//  DPAppEventLog.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 25/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//
import AppsFlyerLib
import CleverTapSDK
class DPAppEventLog: NSObject {
    let APPSFLYER_DEVKEY = "rQXTs7AqLD4mPLR6EsYK43"
    let APPSFLYER_APPID = "1161686991"
    static let shared : DPAppEventLog = {
        let instance = DPAppEventLog()
        return instance
    }()
    func initializeAppFlyerComponents() {
        AppsFlyerTracker.shared().appsFlyerDevKey = APPSFLYER_DEVKEY
        AppsFlyerTracker.shared().appleAppID = APPSFLYER_APPID
    }
    func trackAppLauch() {
        // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
        AppsFlyerTracker.shared().trackAppLaunch()
    }
    func logEventName(_ eventName: String, values: [AnyHashable : AnyObject]) {
        AppsFlyerTracker.shared().trackEvent(eventName, withValues: values)
        CleverTap.sharedInstance()?.recordEvent(eventName, withProps: values)
    }
}
