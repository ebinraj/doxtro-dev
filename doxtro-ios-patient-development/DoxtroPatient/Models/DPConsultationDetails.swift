//
//  DPConsultationDetails.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 28/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//


import Foundation
import SwiftyJSON
enum PaymentStatus : String{
    case pending
    case free
    case paid
    case Anone
    init(paymentstatus : String) {
        switch paymentstatus {
        case "pending":
            self = .pending
            break
        case "free":
            self = .free
            break
        case "paid":
            self = .paid
            break
        default:
            self = .pending
            break
        }
    }
}
enum InitateConsultationType : String{
    case Chat
    case Audio
    init(type : String){
        switch type {
        case "Chat","Planned-Chat":
            self = .Chat
            break
        case "Audio","Planned-Audio":
            self = .Audio
            break
        default:
            self = .Chat
        }
    }
}

enum InitatePlannedConsultationType : String{
    case PlannedChat = "Planned-Chat"
    case PlannedAudio = "Planned-Audio"
    init(type : String){
        switch type {
        case "Planned-Chat":
            self = .PlannedChat
            break
        case "Planned-Audio":
            self = .PlannedAudio
            break
        default:
            self = .PlannedChat
        }
    }
}
enum ConsultationStatus : String{
    case Waiting
    case New = "Payment pending"
    case Ongoing = "Ongoing consultation"
    case Completed
    case Closed
    case Anone
    case Planned
    init(consulatationStatus : String) {
        switch consulatationStatus {
        case "Waiting":
            self = .Waiting
            break
        case "New":
            self = .New
            break
        case "Ongoing":
            self = .Ongoing
            break
        case "Completed":
            self = .Completed
            break
        case "Closed":
            self = .Closed
            break
        case "Planned":
            self = .Planned
            break
        default:
            self = .Anone
            break
        }
    }
}
class DpConsultationDetails : NSObject , NSCoding{
    var assignRequestStatus : String?
    var status : ConsultationStatus?
    var specializationCategory : String?
    var consultationType : InitateConsultationType = .Chat
    var planConsultationType : InitatePlannedConsultationType = .PlannedChat
    var deleted : Bool?
    var consultationId : String?
    var consultationLanguage : String?
    var logicalDelete : Bool?
    var consultingFor : String?
    var patientId : String?
    var isFollowUp : String?
    var userName : String?
    var relativesId : String?
    var patientFee : Int?
    var paymentStatus : PaymentStatus?
    var relativeObj : RelativeData?
    var patientObj : PatientData?
    var note : String?
    var doctorObj : DoctorData?
    var createdDate : String?
    var consultingFee : Int?
    init(dict : JSON) {
        self.consultingFee = dict["consultingFee"].intValue
        self.assignRequestStatus = dict["assignRequestStatus"].stringValue
        self.status =  ConsultationStatus(consulatationStatus: dict["status"].stringValue)
        self.specializationCategory = dict["specializationCategory"].stringValue
        self.consultationType = InitateConsultationType(type : dict["consultationType"].stringValue)
        if let value = InitatePlannedConsultationType(rawValue: dict["consultationType"].stringValue){
            self.planConsultationType = value
        }
        self.deleted = dict["deleted"].boolValue
        self.consultationId = dict["_id"].stringValue
        self.consultationLanguage = dict["consultationLanguage"].stringValue
        self.logicalDelete = dict["logicalDelete"].boolValue
        self.consultingFor = dict["consultingFor"].stringValue
        if let patientId = dict["patientId"].string{
            self.patientId = patientId
        }else{
            self.patientId = dict["patientId"]["_id"].stringValue
            self.patientObj = PatientData(dict: dict["patientId"])
        }
        self.isFollowUp = dict["isFollowUp"].stringValue
        self.userName = dict["userName"].stringValue
        self.note = dict["note"].stringValue
        if let relativeId = dict["relativesId"].string{
         self.relativesId = relativeId
        }else{
            self.relativesId = dict["relativesId"]["_id"].stringValue
            self.relativeObj = RelativeData(dict: dict["relativesId"])
        }
        self.patientFee = dict["patientFee"].intValue
        self.paymentStatus =  PaymentStatus(paymentstatus: dict["paymentStatus"].stringValue)
        if let doctor = dict["doctorId"].dictionary{
            self.doctorObj = DoctorData(dict: JSON(doctor))
        }
        self.createdDate = dict["createdDate"].stringValue
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(consultingFee, forKey: "consultingFee")
        aCoder.encode(assignRequestStatus,    forKey: "assignRequestStatus")
        aCoder.encode(status?.rawValue,    forKey: "status")
        aCoder.encode(specializationCategory,    forKey: "specializationCategory")
        aCoder.encode(consultationType.rawValue,    forKey: "consultationType")
        aCoder.encode(deleted,    forKey: "deleted")
        aCoder.encode(consultationId,    forKey: "consultationId")
        aCoder.encode(consultationLanguage, forKey: "consultationLanguage")
        aCoder.encode(logicalDelete, forKey: "logicalDelete")
        aCoder.encode(consultingFor, forKey: "consultingFor")
        aCoder.encode(patientId, forKey: "patientId")
        aCoder.encode(patientObj, forKey: "patientObj")
        aCoder.encode(isFollowUp, forKey: "isFollowUp")
        aCoder.encode(userName, forKey: "userName")
        aCoder.encode(note,forKey : "note")
        aCoder.encode(relativesId,forKey : "relativesId")
        aCoder.encode(relativeObj,forKey : "relativeObj")
        aCoder.encode(patientFee,forKey : "patientFee")
        aCoder.encode(paymentStatus?.rawValue,forKey : "paymentStatus")
        aCoder.encode(doctorObj,forKey : "doctorObj")
        aCoder.encode(createdDate,forKey : "createdDate")
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.consultingFee =  aDecoder.decodeObject(forKey: "consultingFee") as? Int
        self.assignRequestStatus = aDecoder.decodeObject(forKey: "assignRequestStatus") as? String
        self.status = ConsultationStatus(rawValue: aDecoder.decodeObject(forKey: "status") as! String)!
        self.specializationCategory = aDecoder.decodeObject(forKey: "specializationCategory") as? String
        self.consultationType = InitateConsultationType(rawValue:aDecoder.decodeObject(forKey:"consultationType") as! String)!
        self.deleted = aDecoder.decodeObject(forKey: "deleted") as? Bool
        self.consultationId = aDecoder.decodeObject(forKey: "consultationId") as? String
        self.consultationLanguage = aDecoder.decodeObject(forKey: "consultationLanguage") as? String
        self.logicalDelete = aDecoder.decodeObject(forKey: "logicalDelete") as? Bool
        self.consultingFor = aDecoder.decodeObject(forKey: "consultingFor") as? String
        self.patientId = aDecoder.decodeObject(forKey: "patientId") as? String
        self.patientObj = aDecoder.decodeObject(forKey: "patientObj") as? PatientData
        self.isFollowUp = aDecoder.decodeObject(forKey: "isFollowUp") as? String
        self.userName = aDecoder.decodeObject(forKey : "userName") as? String
         self.note = aDecoder.decodeObject(forKey : "note") as? String
         self.relativesId = aDecoder.decodeObject(forKey : "relativesId") as? String
         self.relativeObj = aDecoder.decodeObject(forKey : "relativeObj") as? RelativeData
         self.patientFee = aDecoder.decodeObject(forKey : "patientFee") as? Int
         self.paymentStatus = PaymentStatus( rawValue: aDecoder.decodeObject(forKey : "paymentStatus") as! String)!
         self.doctorObj = aDecoder.decodeObject(forKey : "doctorObj") as? DoctorData
         self.createdDate = aDecoder.decodeObject(forKey : "createdDate") as? String
    }
    
}
class RelativeData : NSObject,NSCoding{
    var firstName : String?
    var age : String?
    var gender : String?
    var id : String?
    init(dict : JSON) {
        firstName = dict["name"].stringValue
        age = dict["age"].stringValue
        gender = dict["gender"].stringValue
        id = dict["_id"].stringValue
        
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(age,    forKey: "age")
        aCoder.encode(gender,    forKey: "gender")
        aCoder.encode(id,    forKey: "id")
    }
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.firstName =  aDecoder.decodeObject(forKey: "firstName") as? String
        self.age = aDecoder.decodeObject(forKey: "age") as? String
        self.gender = (aDecoder.decodeObject(forKey: "gender")) as? String
        self.id = aDecoder.decodeObject(forKey: "id") as? String
}
}
class PatientData : NSObject , NSCoding{
    var firstName : String?
    var age : String?
    var gender : String?
    var id : String?
    init(dict : JSON) {
        firstName = dict["firstName"].stringValue
        age = dict["age"].stringValue
        gender = dict["gender"].stringValue
        id = dict["_id"].stringValue
        
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(age,    forKey: "age")
        aCoder.encode(gender,    forKey: "gender")
        aCoder.encode(id,    forKey: "id")
    }
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.firstName =  aDecoder.decodeObject(forKey: "firstName") as? String
        self.age = aDecoder.decodeObject(forKey: "age") as? String
        self.gender = (aDecoder.decodeObject(forKey: "gender")) as? String
        self.id = aDecoder.decodeObject(forKey: "id") as? String
    }

}
class DoctorData : NSObject , NSCoding{
    var gender : String?
    var firstName : String?
    var doctorId : String?
    var categoryArray : [String] = []
    init(dict : JSON) {
        self.gender = dict["gender"].string ?? ""
        self.firstName = dict["firstName"].string ?? ""
        self.doctorId  = dict["_id"].string ?? ""
        if let categoryArray = dict["category"].array{
            self.categoryArray.removeAll()
            for value in categoryArray{
                self.categoryArray.append(value.stringValue)
            }
        }
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(categoryArray,    forKey: "categoryArray")
        aCoder.encode(gender,    forKey: "gender")
        aCoder.encode(doctorId,    forKey: "doctorId")
    }
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.firstName =  aDecoder.decodeObject(forKey: "firstName") as? String
        self.categoryArray = (aDecoder.decodeObject(forKey: "categoryArray") as? [String])!
        self.gender = (aDecoder.decodeObject(forKey: "gender")) as? String
        self.doctorId = aDecoder.decodeObject(forKey: "doctorId") as? String
    }
    
}


