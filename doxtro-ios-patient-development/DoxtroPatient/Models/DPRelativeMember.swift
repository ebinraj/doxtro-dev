//
//  DPRelativeMember.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 21/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class DPRelativeMember {
    var gender : String?
    var id : String?
    var isDeleted : Bool?
    var age : Int?
    var updatedAt : String?
    var createdAt : String?
    var relation : String?
    var name: String?
    var patientId : String?
    init(dict : JSON) {
        self.gender = dict["gender"].stringValue
        self.id = dict["_id"].stringValue
        self.isDeleted = dict["deleted"].boolValue
        self.age = dict["age"].intValue
        self.updatedAt = dict["updatedAt"].stringValue
        self.createdAt = dict["createdAt"].stringValue
        self.relation = dict["relation"].stringValue
        self.name = dict["name"].stringValue
        self.patientId = dict["patientId"].stringValue
    }
}
