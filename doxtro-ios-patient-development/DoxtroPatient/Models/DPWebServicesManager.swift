//
//  DPWebServicesManager.swift
//  DoxtroPatient
//
//  Created by vinaykumar on 7/18/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import Firebase
import FirebaseDatabase
import Reachability
import TSMessages
let getRelatedArticleApi                       = "healthFeed/relatedArticles"
class DPWebServicesManager {
    private let alamofireManager : Alamofire.SessionManager
    static var sharedManager : DPWebServicesManager = {
        let manager = DPWebServicesManager()
        manager.restoreData()
        manager.reachability = Reachability()
        NotificationCenter.default.addObserver(manager, selector: #selector(DPWebServicesManager.reachabilityChanged(note:)),name: Notification.Name.reachabilityChanged,object: manager.reachability)
        return manager
    }()
     var reachability: Reachability?
    var relativeMemberArray : [DPRelativeMember] = []
    var badgeArray  = [String]()
    lazy var channelRef: DatabaseReference = Database.database().reference().child(FIREBASE_CONSULTATION_DBKEY)
    var channelRefHandle: DatabaseHandle?
    var recentConsulatationArrray : [DpConsultationDetails] = []
    var user : DPUser?
    var consultationInfo : DpConsultationDetails?
    init() {
        alamofireManager = Alamofire.SessionManager.default
    }
    func restoreData(){
        user = DPUser.getUserInfoFromUserDefaults()
    }
    
    //MARK: - get relate articles
    func getRelatedArticles(details: [String: AnyObject], completionHandler:@escaping (_ response: JSON?, _ error: Error?)->()) {
        if let articleId = details["id"] as? String {
            if let categories = details["categories"] as? String {
                let appendUrl = "?id=\(articleId)&categories=\(categories)"
                let url = kWebServiceBase + "/" + getRelatedArticleApi + appendUrl
               DPWebServicesManager.alamofireRequest(url: url, httpMethod: HTTPMethod.get.rawValue, details: [:]) { (json : JSON?, error) in
                    completionHandler(json, error)
                }
            }
        }
    }
    func getPackageValues(request : DPWebRequestProtocol,complitionHandler : @escaping(_ paymentData : PaymentData?,_ error : Error?)-> Void){
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let response = response {
                if let data = response["data"].dictionary {
                    let paymentdata = PaymentData(dict: JSON(data))
                    complitionHandler(paymentdata,nil)
                }
            }else{
                    complitionHandler(nil,error)
            }
        }
    }
    func createRequest(with request : DPWebRequestProtocol)->Alamofire.DataRequest?{
        
        guard let isReachable = reachability?.isReachable, isReachable == true  else {
            ABProgressIndicator.shared.hideAnimator()
            showNoNetworMessage()
            return nil
        }
        guard  let endPoint = request.endPoint else {
            return nil
        }
        return alamofireManager.request(request.baseUrl.appendingPathComponent(endPoint), method: request.requestType, parameters: request.parameters, encoding: request.parameterEncoding, headers: request.headers)
    }
    @objc func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            
        } else {
            DispatchQueue.main.async {
               self.showNoNetworMessage()
            }
        }
    }
    func showNoNetworMessage(){
         TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Network Error", subtitle:  "Your internet connection disappeared!", type: .error)
    }
    class func getResponse(with request : DPWebRequestProtocol,completionHandler : @escaping(JSON?,Error?)->Void){
        if let alamofireRequest = sharedManager.createRequest(with: request){
            alamofireRequest.responseJSON { (response) in
               // debugPrint(response.result)
                switch(response.result){
                case .success(let data):
                   // debugPrint(JSON(data))
                    completionHandler(JSON(data),nil)
                    break
                case .failure(let error):
                    completionHandler(nil,error)
                   
                    break
                }
            }
        }
    }
    class func userLogout(){
        UserDefaults.standard.removeObject(forKey: JPDeviceToken)
        UserDefaults.standard.synchronize()
        DPUser.cleanUser()
    }
     func saveUser(with userInfo : DPUser){
        user = userInfo
        user?.save()
    }
    func saveConsultationInfo(with info : DpConsultationDetails){
        consultationInfo = info
    }
     func UpdateUserDeviceId(){
        if let user = DPWebServicesManager.sharedManager.user{
             if let deviceToken = UserDefaults.standard.object(forKey: JPDeviceToken) as? String {
                let params = ["_id":user.userId,"deviceId":deviceToken,"typeOfApp" : "patient","typeOfPhone":"iphone"]
                let request = UpdateCurrentUserDeviceIdRequest(params: params as [String : AnyObject])
                DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                   // debugPrint(response)
                })
            }
        }
    }
    class func DeactiveUserAccount(){
        if let userId = sharedManager.user?.userId{
        UIAlertController.showAlertWithMessage("STR_DEACTIVE_MSG".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
        }, actionButtonHandler: { (_ , _) in
           Deactive(userId)
        })
        }
    }
    class func Deactive(_ userID : String){
            let params = ["_id" : userID]
            let request = DeactiveUserAccountRequest(params: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
            getResponse(with: request, completionHandler: { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let _ = response{
                    DPWebServicesManager.userLogout()
                    UIApplication.appDelegate().setUpViewController(With: "Main", and: "signIn")
                    TSMessage.showNotification(in: UIApplication.getRootViewController(), title: "Success", subtitle: "Account deactiavted successfully", type: .success)
                }else{
                 TSMessage.showNotification(in: UIApplication.getRootViewController(), title: "Error", subtitle: "You requst can't be completed at this moment,try after some time", type: .error)
                }
            })
    }
    func getRelativeList(completionHandler : @escaping(_ error : Error?,_ memberArray : [DPRelativeMember]?)->Void){
        if let user =  DPWebServicesManager.sharedManager.user{
            ABProgressIndicator.shared.showAnimator()
            let getRelativesList = GetRelativeListRequest(params: ["patientId": user.userId as AnyObject])
            DPWebServicesManager.getResponse(with: getRelativesList) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let response = response?["data"].array{
                    self.relativeMemberArray.removeAll()
                    for memberObj in response{
                        self.relativeMemberArray.append(DPRelativeMember(dict: memberObj))
                    }
                    completionHandler(nil,self.relativeMemberArray)
                }else{
                    completionHandler(error,nil)
                }
            }
        }
    }
    class func getConsulatationInfo(consultationId : String,completationHandler :@escaping(_ resposnse : DpConsultationDetails?,_ error : Error?)->Void){
        let params  = ["consultationId" : consultationId]
        let request = GetConsultationInfo(params: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if error == nil,let response = response{
                if let array = response["data"].array{
                    guard array.count > 0 else{
                        completationHandler(nil,nil)
                        return
                    }
                }
                if let data = response["data"].array?.first?.dictionary{
                    let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                    completationHandler(consulattionInfoObj,nil)
                }
            }
        }
    }
}
extension DPWebServicesManager{
    func getRecentConsultations(isShow : Bool = true,completationHandler : @escaping(_ error : Error?,_ consultationArray : [DpConsultationDetails]?)->Void){
        if let isRechable = DPWebServicesManager.sharedManager.reachability?.isReachable,isRechable{
        if let user = DPWebServicesManager.sharedManager.user{
            let params = ["patientId" : user.userId,"skip":0,"limit":20] as [String : Any]
            let request = GetRecentConsultationrequest(params: params as [String : AnyObject])
            if isShow{
                ABProgressIndicator.shared.showAnimator()
            }
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if isShow{
                    ABProgressIndicator.shared.hideAnimator()
                }
                if error == nil,let response = response{
                    if let data = response["data"].array{
                        self.recentConsulatationArrray.removeAll()
                        for value in data{
                            let consulattionInfoObj = DpConsultationDetails(dict: value)
                            self.recentConsulatationArrray.append(consulattionInfoObj)
                        }
                        DatabaseController.deleteConsultationCoredataList()
                        DatabaseController.insertConsultationListInCoreData(obj: self.recentConsulatationArrray)
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "RefreshAllConsultationList")))
                        completationHandler(nil,self.recentConsulatationArrray)
                        //self.updateBadgeCount()
                        //consulattionInfoObj.patientFee = response["patientFee"].intValue
                    }
                }else{
                     completationHandler(error,nil)
                }
            })
        }
        }else{
            if let list =   DatabaseController.fetchConsulatationList().0{
            self.recentConsulatationArrray = list
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "RefreshAllConsultationList")))
                completationHandler(nil,self.recentConsulatationArrray)
            }
        }
    }
    
//    func updateBadgeCount(){
//        badgeArray.removeAll()
//        for obj in self.recentConsulatationArrray{
//            if let consulatationId = obj.consultationId{
//         let messageQuery = channelRef.child(consulatationId)
//         messageQuery.observeSingleEvent(of: .value, with: { (snapshot) in
//            if let channelData = snapshot.value as? Dictionary<String, AnyObject>{ // 2
//            debugPrint(channelData)
//            for (_,obj) in channelData{
//                if let obj = JSON(obj).dictionary{
//                    if let msgType = obj["msgType"]?.string,msgType == "text"{
//                        if let from = obj["from"]?.string{
//                            if from == "Doctor" {
//                                if let status = obj["status"]?.string{
//                                    if status == "sent" || status == "Sent"{
//                                        if !self.badgeArray.contains(consulatationId){
//                                            self.badgeArray.append(consulatationId)
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                }
//            }
//            if self.badgeArray.count > 0{
//                if let item = UIApplication.appDelegate().getTabbarController()?.tabBar.items![1]{
//                    item.badgeValue = String(describing: self.badgeArray.count)
//                }
//            }else{
//                if let item = UIApplication.appDelegate().getTabbarController()?.tabBar.items![1]{
//                    item.badgeValue = nil
//                }
//            }
//        }) { (error) in
//            print(error.localizedDescription)
//        }
//        }
//        }
//    }
    class func getallSpecializaion(){
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
            let prams = ["patienId" :userID ]
            let request = GetAllSpecialization(params: prams as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if let resposne  = response{
                    if let specializationArray = resposne["data"].array{
                        if let user =  DPWebServicesManager.sharedManager.user{
                            var values = [String : AnyObject]()
                            values["Name"] = user.name as AnyObject
                            values["Email"] = user.emailId as AnyObject
                            values["Phone"] = user.mobileNum as AnyObject
                            values["Age"] = user.age as AnyObject
                            values["Gender"] = user.gender as AnyObject
                            values["specialization"] = specializationArray as AnyObject
                            DPAppEventLog.shared.logEventName(EVENT_COMPLETED_SIGNUP, values: values)
                            CleverTap.sharedInstance()?.onUserLogin(values)
                        }
                    }
                }
                
                
            })
        }
    }
    class func alamofireRequest(url: String!, httpMethod: String, details: [String : AnyObject], completionHandler: @escaping (_ status: JSON?, _ error: NSError?) -> ()) {
        if let urlString = url as NSString? {
            if let urlStr : NSString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) as? NSString {
        if let URL = URL(string: urlStr as String) {
            let request = NSMutableURLRequest(url: URL)
            request.httpMethod = httpMethod
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            if let userToekn = DPWebServicesManager.sharedManager.user?.token{
                request.setValue(userToekn, forHTTPHeaderField: "Authorization")
            }
            if details.count > 0 {
                request.httpBody = try! JSONSerialization.data(withJSONObject: details, options: [])
            }
            Alamofire.request(request as URLRequest)
                .responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        DispatchQueue.global().async {
                            DispatchQueue.main.async {
                                // print("error is \(error)")
                                completionHandler(nil,error as NSError)
                            }
                        }
                    case .success(let responseObject):
                        DispatchQueue.global().async {
                            let json : JSON =  JSON(responseObject)
                            DispatchQueue.main.async {
                                print("success is \(responseObject)")
                                completionHandler(json, nil)
                            }
                        }
                    }
            }
        }
            }
        }
    }
    func updateUserProfile(){
        if let userId = self.user?.userId{
        let params : [String : AnyObject] = ["_id":userId as AnyObject]
        let request = UpdateProfielRequest(params: params)
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if let response = response {
                    if let data = response[DATA_KEY].dictionary{
                        let userInfo : DPUser = DPUser(data :data)
                        DPWebServicesManager.sharedManager.saveUser(with: userInfo)
                    }
                }
            })
        }
    }
    func getAllHealthFeedList(details: [String : JSON], completionHandler:@escaping (_ response: JSON?, _ error: Error?)->()) {
        let listType = details["listType"]?.string ?? ""
        let limit = details["limit"]?.int ?? 0
        let skip = details["skip"]?.int ?? 0
        let search = details["search"]?.string ?? ""
        var appendUrl = ""//"?listType=\(listType)&limit=\(limit)&skip=\(skip)"
        if search == "" {
            appendUrl = "?listType=\(listType)&limit=\(limit)&skip=\(skip)"
        } else {
            appendUrl = "?listType=\(listType)&limit=\(limit)&skip=\(skip)&search=\(search)"
        }
        
        let url =  getUrl(urlString: "/healthFeed/all") + appendUrl
        DPWebServicesManager.alamofireRequest(url: url, httpMethod: HTTPMethod.get.rawValue, details: [:]) { (json : JSON?, error) in
            completionHandler(json, error)
        }
    }
    private func getUrl(urlString: String)-> String {
        let url = kWebServiceBase + urlString
        return url
    }
    //MARK: - SendResponse to consultation request
    func updateHealthFeedLike(details: [String : AnyObject], completionHandler:@escaping (_ response: JSON?, _ error: Error?)->()) {
        let url = getUrl(urlString: healthFeedLike)
         DPWebServicesManager.alamofireRequest(url: url, httpMethod: HTTPMethod.post.rawValue, details: details) { (json : JSON?, error) in
            completionHandler(json, error)
        }
    }
    // healthFeedDetail
    //MARK: - find Availability slots for planned Consultation
    func getHealthFeedDetail(articleId: String, completionHandler:@escaping (_ response: JSON?, _ error: Error?)->()) {
        let url = getUrl(urlString: healthFeedDetail) + articleId
        DPWebServicesManager.alamofireRequest(url: url, httpMethod: HTTPMethod.get.rawValue, details: [:]) { (json : JSON?, error) in
            completionHandler(json, error)
        }
    }
    func getRefreshConsultationPaymentCard(consultationId :  String,completionHandler : @escaping(_ response : JSON?,_ error : Error?)->Void){
        let params = ["consultationId" : consultationId] as [String : AnyObject]
        let request = RefreshConsultationPaymentCardRequest(params: params)
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            if let response = response,error == nil{
                completionHandler(response,nil)
            }else{
                completionHandler(nil,error)
            }
        }
        
    }
    func getDoctorProfile(_ doctorId: String, completionHandler: @escaping(_ response: JSON?, _ error: Error?)->Void) {
        let params = ["_id" : doctorId] as [String : AnyObject]
        let request = GetDoctorProfileRequest(params: params)
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            if let response = response,error == nil{
                completionHandler(response,nil)
            }else{
                completionHandler(nil,error)
            }
        }
    }
    func getCompanyList(completionHandler: @escaping(_ response: JSON?, _ error: Error?)->Void) {
        let request = GetCompanyListRequest(params: [:])
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            if let response = response,error == nil{
                completionHandler(response,nil)
            }else{
                completionHandler(nil,error)
            }
        }
    }
}
let healthFeedLike  = "/healthFeed/like"
let healthFeedDetail = "/healthFeed/getById/"
let companyList = "companyInfo/getCompanyList"
