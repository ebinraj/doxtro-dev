//
//  DPPaymentOptions.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 16/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
enum RazorPayPaymentStatus{
    case created
    case authorized
    case captured
    case refunded
    case failed
    init(status : String){
        switch status {
        case "created":
            self = .created
            break
        case "authorized":
            self = .authorized
            break
        case "captured","TXN_SUCCESS","free":
            self = .captured
            break
        case "refunded":
            self = .refunded
            break
        case "failed":
            self = .failed
            break
        default:
            self = .failed
            break;
        }
    }
}
class PaymentInfo{
    public var id : String?
    public var entity : String?
    public var amount : Int?
    public var currency : String?
    public var status : RazorPayPaymentStatus?
    public var order_id : String?
    public var invoice_id : String?
    public var international : String?
    public var method : String?
    public var amount_refunded : Int?
    public var refund_status : String?
    public var captured : String?
    public var description : String?
    public var card_id : String?
    public var bank : String?
    public var wallet : String?
    public var vpa : String?
    public var email : String?
    public var contact : Int?
    public var notes : Array<String>?
    public var fee : Int?
    public var service_tax : Int?
    public var error_code : String?
    public var error_description : String?
    public var created_at : Int?
    public var tax : Int?
    init(dictionary : JSON) {
        id = dictionary["id"].stringValue
        entity = dictionary["entity"].stringValue
        amount = dictionary["amount"].intValue
        currency = dictionary["currency"].stringValue
        if let status  = dictionary["status"].string{
         self.status = RazorPayPaymentStatus(status: status)
        }
        order_id = dictionary["order_id"].stringValue
        invoice_id = dictionary["invoice_id"].stringValue
        international = dictionary["international"].stringValue
        method = dictionary["method"].stringValue
        amount_refunded = dictionary["amount_refunded"].intValue
        refund_status = dictionary["refund_status"].stringValue
        captured = dictionary["captured"].stringValue
        description = dictionary["description"].stringValue
        card_id = dictionary["card_id"].stringValue
        bank = dictionary["bank"].stringValue
        wallet = dictionary["wallet"].stringValue
        vpa = dictionary["vpa"].stringValue
        email = dictionary["email"].stringValue
        contact = dictionary["contact"].intValue
        //if (dictionary["notes"] != nil) { notes = notes.di(dictionary["notes"] as! NSArray) }
        fee = dictionary["fee"].intValue
        service_tax = dictionary["service_tax"].intValue
        error_code = dictionary["error_code"].stringValue
        error_description = dictionary["error_description"].stringValue
        created_at = dictionary["created_at"].intValue
        tax = dictionary["tax"].intValue
        
    }
}
