//
//  DPPackageListModel.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
class PackageListDetail{
    var subscriptionPackageDetail : [Subscription] = []
    var oneTimePackageDetail : [Package] = []
    init(data : JSON) {
        if let subsCriptionPackageDetail = data["Subscription"].array{
            for obj in subsCriptionPackageDetail{
                self.subscriptionPackageDetail.append(Subscription(data: JSON(obj)))
            }
        }
        if let oneTimePackageDetail = data["Package"].array
        {
            for obj in oneTimePackageDetail{
                self.oneTimePackageDetail.append(Package(data: JSON(obj)))
            }
        }
    }
}
class Subscription{
    var subscriptionTag : String = ""
    var freeGPConsultations : Int = 0
    var _id : String = ""
    var subscriptionType : String = ""
    var price : Int = 0
    var consultationPerEmployee : Int = 0
    var planId: String = ""
    var text: String = ""
    var paymentCycle: String = ""
    var duration: String = ""
    var description:String = ""
    var originalPrice: Int = 0
    init(data : JSON) {
        self.subscriptionTag = data["subscriptionTag"].string ?? ""
        self.freeGPConsultations = data["freeGPConsultations"].int ?? 0
        self._id = data["_id"].string ?? ""
        self.subscriptionType = data["subscriptionType"].string ?? ""
        self.price = data["price"].int ?? 0
        self.consultationPerEmployee = data["consultationPerEmployee"].int ?? 0
        self.planId = data["planId"].string ?? ""
        self.text = data["text"].string ?? ""
        self.paymentCycle = data["paymentCycle"].string ?? ""
        self.duration = data["duration"].string ?? ""
        self.description = data["description"].string ?? ""
        self.originalPrice = data["originalPrice"].int ?? 0
    }
    
}
class Package {
    var subscriptionTag : String = ""
    var freeGPConsultations : Int = 0
    var _id : String = ""
    var subscriptionType : String = ""
    var price : Int = 0
    var duration: String = ""
    var consultationPerEmployee : Int = 0
    var description:String = ""
    var originalPrice: Int = 0
    init(data : JSON) {
        self.subscriptionTag = data["subscriptionTag"].string ?? ""
        self.freeGPConsultations = data["freeGPConsultations"].int ?? 0
        self._id = data["_id"].string ?? ""
        self.subscriptionType = data["subscriptionType"].string ?? ""
        self.price = data["price"].int ?? 0
        self.consultationPerEmployee = data["consultationPerEmployee"].int ?? 0
        self.duration = data["duration"].string ?? ""
        self.description = data["description"].string ?? ""
        self.originalPrice = data["originalPrice"].int ?? 0
    }
}

/*
 "package" : [
 {
 "subscriptionTag" : "mypackage",
 "freeGPConsultations" : 10,
 "_id" : "5a6f231429c4d7abb3abf29f",
 "subscriptionType" : "package",
 "price" : 2000,
 "consultationPerEmployee" : 20
 }
 ]
 */
