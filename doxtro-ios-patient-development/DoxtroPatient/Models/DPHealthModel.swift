//
//  DPHealthModel.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 31/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class HealthRecord{
    var diagnostic : [HealthModel] = []
    var medication : [HealthModel] = []
    var intolerance : [HealthModel] = []
    var medicalHisory : [HealthModel] = []
    init(dicts : [JSON]) {
        for dict in dicts{
        if let tag = dict["tag"].string{
            let tagtype = HealthRecordType(value: tag)
            switch tagtype{
            case .history:
                if let data = dict["data"].array{
                    self.medicalHisory.removeAll()
                    for value in data{
                        self.medicalHisory.append(HealthModel(dictionary: JSON(value.dictionary)))
                    }
                }
                break
            case .report:
                if let data = dict["data"].array{
                    self.diagnostic.removeAll()
                    for value in data{
                    self.diagnostic.append(HealthModel(dictionary: JSON(value.dictionary)))
                    }
                }
                break
            case .medication:
                if let data = dict["data"].array{
                    self.medication.removeAll()
                    for value in data{
                        self.medication.append(HealthModel(dictionary: JSON(value.dictionary)))
                    }
                }
                break
            case .intolerance:
                if let data = dict["data"].array{
                    self.intolerance.removeAll()
                    for value in data{
                        self.intolerance.append(HealthModel(dictionary: JSON(value.dictionary)))
                    }
                }
                break
            case .none:
                break
            }
        }
    }
    }
}
class Diagnostic{
    public var _id : String?
    public var content : String?
    var contentUrl : URL?
    public var deleted : String?
    public var __v : Int?
    public var relativesId : String?
    public var tag : String?
    public var updatedAt : String?
    public var createdAt : String?
    public var date : String?
    public var patientId : String?
    public var url : Array<String>?
    init(dictionary : JSON) {
        _id = dictionary["_id"].string ?? ""
        content = dictionary["content"].string ?? ""
        if let content = content{
         contentUrl = URL(string: content)
        }
        deleted = dictionary["deleted"].string ?? ""
        __v = dictionary["__v"].intValue
        relativesId = dictionary["relativesId"].string ?? ""
        tag = dictionary["tag"].string ?? ""
        updatedAt = dictionary["updatedAt"].string ?? ""
        createdAt = dictionary["createdAt"].string ?? ""
        date = dictionary["date"].string ?? ""
        patientId = dictionary["patientId"].string ?? ""
//        if (dictionary["url"] != nil) { url = URL.modelsFromDictionaryArray(dictionary["url"] as! NSArray) }
    }
    
}
class UrlUtility{
    var url : URL?
    var urlType : String?
    var documentName : String?
    init(dict : [String : JSON]) {
        self.documentName = dict["documentName"]?.string ?? ""
        self.urlType = dict["urlType"]?.string ?? ""
        if let urlString = dict["url"]?.string{
            if let url  = URL(string: urlString){
                self.url = url
            }
        }
    }
}
class HealthModel{
    public var _id : String?
    public var content : String?
    var contentUrl : URL?
    public var deleted : String?
    public var __v : Int?
    public var relativesId : String?
    public var tag : String?
    public var updatedAt : String?
    public var createdAt : String?
    public var date : String?
    public var patientId : String?
    public var url : [UrlUtility] = []
    var note : String?
    init(dictionary : JSON) {
        _id = dictionary["_id"].string ?? ""
        content = dictionary["content"].string ?? ""
        if let content = content{
            contentUrl = URL(string: content)
        }
        deleted = dictionary["deleted"].string ?? ""
        __v = dictionary["__v"].intValue
        relativesId = dictionary["relativesId"].string ?? ""
        tag = dictionary["tag"].string ?? ""
        updatedAt = dictionary["updatedAt"].string ?? ""
        createdAt = dictionary["createdAt"].string ?? ""
        date = dictionary["date"].string ?? ""
        patientId = dictionary["patientId"].string ?? ""
        note = dictionary["note"].string ?? ""
        if let urls = dictionary["url"].array{
            self.url.removeAll()
            for url in urls{
                if let urlDictionary = url.dictionary{
                   self.url.append(UrlUtility(dict: urlDictionary))
                }
                
            }
        }
        //        if (dictionary["url"] != nil) { url = URL.modelsFromDictionaryArray(dictionary["url"] as! NSArray) }
    }
    
}
/*
 [
 {
 "_id" : "59fbe9ee44ab6349c4d49b3c",
 "content" : "Ayush ",
 "note" : "Next check",
 "url" : [
 "https:\/\/firebasestorage.googleapis.com\/v0\/b\/doxtrodevelopment.appspot.com\/o\/chat_documents%2F598985e230548821d4dd5be12017-11-03T03%3A59%3A58.713Z?alt=media&token=2b01cfbf-1788-4489-824d-ded9b569bffa",
 "https:\/\/firebasestorage.googleapis.com\/v0\/b\/doxtrodevelopment.appspot.com\/o\/chat_documents%2F598985e230548821d4dd5be12017-11-03T04%3A00%3A06.024Z?alt=media&token=832cc991-ed69-47e3-a8e6-8da2fd4c18d4",
 "https:\/\/firebasestorage.googleapis.com\/v0\/b\/doxtrodevelopment.appspot.com\/o\/chat_documents%2F598985e230548821d4dd5be12017-11-03T04%3A00%3A13.882Z?alt=media&token=e45d8295-bb94-4ac8-adf7-ec9dd7b76900",
 "https:\/\/firebasestorage.googleapis.com\/v0\/b\/doxtrodevelopment.appspot.com\/o\/chat_documents%2F598985e230548821d4dd5be12017-11-03T04%3A00%3A20.501Z?alt=media&token=6d0bd2d8-77d2-4f26-8fa1-d751cf6986b2",
 "https:\/\/firebasestorage.googleapis.com\/v0\/b\/doxtrodevelopment.appspot.com\/o\/chat_documents%2F598985e230548821d4dd5be12017-11-03T04%3A00%3A39.405Z?alt=media&token=0e8fdd08-f6ba-4fcb-a898-77a0b1159406"
 ],
 "date" : "2017-11-03T04:00:46.117Z"
 },
 {
 "_id" : "59fb5b3444ab6349c4d49b28",
 "content" : "Medicine Nmae",
 "note" : "Yadav",
 "url" : [
 
 ],
 "date" : "2017-11-02T17:51:48.506Z"
 },
 {
 "_id" : "59f8693dc4416a79311a0d01",
 "content" : "Ayush",
 "note" : "Bshah",
 "url" : [
 "https:\/\/firebasestorage.googleapis.com\/v0\/b\/doxtrodevelopment.appspot.com\/o\/chat_documents%2F598985e230548821d4dd5be12017-10-31T12%3A14%3A41.564Z?alt=media&token=d63248e9-70cf-40f6-b6bb-f2f7097d4dca"
 ],
 "date" : "2017-10-31T12:14:53.104Z"
 }
 ]
 }
 ],
 */
