//
//  DPPackageHomeDetailMOdel.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
class PackageDeatail{
    var boughtPackageDetail : BoughtPackageDeatail?
    var packageAdditionalDetail : PackageAdditionalDetail?
    init(data : JSON) {
        if let packageDetail = data["data"].dictionary{
             self.boughtPackageDetail = BoughtPackageDeatail(data: JSON(packageDetail))
        }
        if let packageDetail = data["additionalDetails"].dictionary{
          self.packageAdditionalDetail = PackageAdditionalDetail(data:JSON(packageDetail))
        }
    }
}
class BoughtPackageDeatail{
    var subscriptionTag : String = ""
    var endDate : String = ""
    var freeGPConsultations : Float = 0
    var remainingGPConsultations : Float = 0
    var consultationPerEmployee : Float = 0
    var remainingConsultations : Float = 0
    var availGPConsultation : Float = 0
    var availNormalConsultation : Float = 0
    var progressBarColor : UIColor?
    var totalAvailConsultation : Float = 0
    var totalConsultations : Float = 0
    var totalRemainingConsultation : Float = 0
    var subscriptionType: String = ""
    var corporatePayType: String = ""
    init(data : JSON) {
        self.subscriptionTag = data["subscriptionTag"].string ?? ""
         self.freeGPConsultations = data["freeGPConsultations"].float ?? 0
         self.remainingGPConsultations = data["remainingGPConsultations"].float ?? 0
         self.consultationPerEmployee = data["consultationPerEmployee"].float ?? 0
         self.remainingConsultations = data["remainingConsultations"].float ?? 0
         self.endDate = data["endDate"].string ?? ""
        self.subscriptionType = data["subscriptionType"].string ?? ""
        self.corporatePayType = data["corporatePayType"].string ?? ""
         self.availGPConsultation = self.freeGPConsultations - self.remainingGPConsultations
         self.availNormalConsultation = self.consultationPerEmployee - self.remainingConsultations
        
        self.totalAvailConsultation = self.availNormalConsultation + self.availGPConsultation
        self.totalConsultations = self.freeGPConsultations + self.consultationPerEmployee
        self.totalRemainingConsultation = self.remainingGPConsultations + self.remainingConsultations
        if  self.totalRemainingConsultation <= 0{
            self.progressBarColor = UIColor.redPackageProgressColor()
        }else{
            self.progressBarColor = UIColor.greenPackageProgressBarColor()
        }
    }
}
class PackageAdditionalDetail{
    var description : String = ""
    var title : String = ""
    var imageUrl : URL?
    init(data : JSON) {
        self.description = data["description"].string ?? ""
        self.title = data["title"].string ?? ""
        self.imageUrl = data["imageUrl"].url
    }
}
extension UIColor{
   class func greenPackageProgressBarColor()->UIColor{
        return UIColor(red: 202, green: 215, blue: 103)
    }
   class  func redPackageProgressColor()->UIColor{
        return UIColor(red: 215, green: 83, blue: 41)
    }
}
/*
 "data" : {
 "subscriptionTag" : "mysubscription",
 "freeGPConsultations" : 2,
 "remainingGPConsultations" : 1,
 "remainingConsultations" : 0,
 "consultationPerEmployee" : 1,
 "endDate" : "2019-01-17T18:29:59.999Z"
 },
 "message" : "Subscription found"
 
 "additionalDetails" : {
 "description" : "this is dummy text",
 "title" : "Health Package"
 },
 "data" : {
 
 },
*/
