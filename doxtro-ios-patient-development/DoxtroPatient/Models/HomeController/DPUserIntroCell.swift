//
//  DPUserIntroCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class UserIntro: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    @IBOutlet weak var nameAgeGenderLabel: UILabel!
    
    @IBOutlet weak var helathConcernLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2)
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 1).cgColor
        mainView.clipsToBounds = true
    }
}
