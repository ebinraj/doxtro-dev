//
//  DPFeeConsultationCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol DPConsultationFeeDelegate {
    func makePayment()
}
class ayush : UICollectionViewCell{
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
}
class ConsultationFeeCell: UICollectionViewCell {
    var delegate : DPConsultationFeeDelegate?
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var dicountValue: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    
    @IBAction func makePayment(_ sender: Any) {
        self.delegate?.makePayment()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 1).cgColor
        mainView.layer.cornerRadius = 4
        mainView.clipsToBounds = true
    }
}
