//
//  DPSelectedSpecializationController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 19/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
import SwiftyJSON
class DPSelectedSpecilizationController: UIViewController {
    var selectedSpecializationObj :DPSpecializationList?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectedSpecilizationLabel: UILabel!
    var defaultText : String = "Enter your health concern here "
    var informationObj : DPSubmitInformation = DPSubmitInformation()
    var submitButtonCell : DPSubmitButtonCell?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.selectedSpecilizationLabel.text = selectedSpecializationObj?.specializationCategory
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func SubmitDetails(){
        var values = [String : AnyObject]()
        values["Consultation requested"] = true as AnyObject
        DPAppEventLog.shared.logEventName("Consultation requested", values: values)
        var params  = [String : String]()
        if let user = DPWebServicesManager.sharedManager.user,let relativeMemberObj = self.informationObj.selectedRelativeMember{
            if let patientId = user.userId,let relativesId = relativeMemberObj.id,let consultingFor = relativeMemberObj.relation,let consultationLanguage = self.informationObj.selectedlanguage,let specializationCategory = self.selectedSpecializationObj?.specializationCategory,let helathConcern = self.informationObj.helathConcernText{
                params = ["patientId" :patientId,
                          "relativesId" : relativesId,
                          "consultingFor" : consultingFor,
                          "consultationType" : "Chat",
                          "consultationLanguage" : consultationLanguage,
                          "specializationCategory" : specializationCategory,
                          "note" : helathConcern
                ]
                let request = SubmitDetailsForConsultationRequest(params: params as [String : AnyObject])
                ABProgressIndicator.shared.showAnimator()
                DPWebServicesManager.getResponse(with: request) { (response, error) in
                    ABProgressIndicator.shared.hideAnimator()
                    if error == nil,let response = response{
                        if let data = response["data"].dictionary{
                            let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                            consulattionInfoObj.patientFee = response["patientFee"].intValue
                            DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                            //self.updateCosultationListAndRecentOCnsultation()
                            self.performSegue(withIdentifier: "showConsultViewController", sender: consulattionInfoObj)
                        }
                    }
                }
            }else{
               
                if let language = self.informationObj.selectedlanguage {
                
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please select language to start consultation", type: .error)
                }
                if let helathconvcen = self.informationObj.helathConcernText{
                    
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please fill health concern to start consultation", type: .error)
                }
                
            }
        }else{
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please select Member for start consultation", type: .error)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showConsultViewController"{
            if  let destinatioVC = segue.destination as? ConsultationViewController{
                destinatioVC.consulatationInfo = sender as? DpConsultationDetails
            }
        }else if segue.identifier == "editProfile"{
            let destinationVC  = segue.destination as! DPEditProfileController
        }
    }
//    func updateCosultationListAndRecentOCnsultation(){
//       // NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "RefreshConsultationList")))
//    }
    
}

extension DPSelectedSpecilizationController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "memberTableViewCell", for: indexPath) as? DPMemberTableViewCell
            cell?.delegate = self
            return cell!
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "consultationTypeCell", for: indexPath) as? DPConsultationTypeTableViewCell
            cell?.delegate = self
            return cell!
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelthConcernCell", for: indexPath) as? DPHelthConcernCell
            cell?.delegate = self
            return cell!
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell", for: indexPath) as? DPSubmitButtonCell
            self.submitButtonCell = cell
            cell?.delegate = self
            return cell!
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 139
        case 1:
            return 180
        case 2:
            return 150
        case 3:
            return 100
        default:
            return 200
        }
    }
}
extension DPSelectedSpecilizationController : DPSubmitDetailsCellDelegate{
    func submitDeatails() {
        SubmitDetails()
        //self.performSegue(withIdentifier: "showConsultViewController", sender: nil)
    }
}
extension DPSelectedSpecilizationController : DPSelectedRealtiveMemberDelegate{
    func setSelectedRelativeMember(relativeMemberObj: DPRelativeMember) {
        var values = [String : AnyObject]()
        values["Consulting for whom"] = relativeMemberObj.relation as AnyObject
        DPAppEventLog.shared.logEventName("Consultation requested", values: values)
        self.informationObj.selectedRelativeMember = relativeMemberObj
    }
    func openEditProfilePage() {
        self.performSegue(withIdentifier: "editProfile", sender: nil)
    }
}
extension DPSelectedSpecilizationController : DPSelectedConsultationTypeDelegate{
    func setSelectedlanguage(language: String) {
        self.informationObj.selectedlanguage = language
    }
    
}
extension DPSelectedSpecilizationController : DpHealthCncernDelegateDelegate{
    func setHelathCocern(helathConcernText: String) {
        self.informationObj.helathConcernText = helathConcernText
    }
    func forChangeSubmitButtonStatus(text : UITextView){
        if let cell = self.submitButtonCell{
            if text.text == defaultText || text.text == ""{
            cell.submitButton.backgroundColor = UIColor(red: 88/255, green: 89/255, blue: 91/255, alpha: 1)
            }else{
               cell.submitButton.backgroundColor = UIColor().greenDoxtroCgColor()
            }
            
        }
    }
}
