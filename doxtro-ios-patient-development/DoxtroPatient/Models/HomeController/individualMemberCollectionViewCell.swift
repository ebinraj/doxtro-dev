//
//  individualMemberCollectionViewCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 21/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol  ShowLongPressView {
    func longPressView(row : Int)
}
class DPMemberCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var memberImageView: UIImageView?
    @IBOutlet weak var memberNameLabel: UILabel?
    var indexPath : IndexPath?
    var delegate : ShowLongPressView?
    override func awakeFromNib() {
        super.awakeFromNib()
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(sender:)))
        self.addGestureRecognizer(longPressRecognizer)
    }
    func longPressed(sender: UILongPressGestureRecognizer)
    {
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
           if let index = indexPath{
                self.delegate?.longPressView(row: index.row)
            }
        }
    }
    func configureCellSelection(selectedIndexPath : IndexPath?,currentIndexPath : IndexPath){
        if selectedIndexPath != nil,let index = selectedIndexPath{
            if index == currentIndexPath{
                self.memberImageView?.makeCircleandBorder(with: (UIColor().greenDoxtroCgColor().cgColor),width: 2.5)
                self.memberNameLabel?.textColor = UIColor().greenDoxtroCgColor()
            }else{
                self.memberImageView?.makeCircleandBorder(with: (UIColor().grayDoxtroCgColor()?.cgColor)!)
                self.memberNameLabel?.textColor = UIColor.black
            }
        }else{
            self.memberImageView?.makeCircleandBorder(with: (UIColor().grayDoxtroCgColor()?.cgColor)!)
            self.memberNameLabel?.textColor = UIColor.black
        }

    }
}
