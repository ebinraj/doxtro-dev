//
//  DPChatReceverImageCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 08/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class ChatReceiverImageCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var timeLabel: UILabel!
    
    
    @IBAction func showFullScreen(_ sender: Any) {
        self.showFullScreen!(chatImageView)
    }
    
    let recognizer = UITapGestureRecognizer()
    override func awakeFromNib() {
        super.awakeFromNib()
//        chatMainView.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
//        chatMainView.layer.borderWidth = 1
//        chatMainView.layer.cornerRadius = 8
//        chatMainView.clipsToBounds = true
        chatImageView.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
        chatImageView.layer.borderWidth = 1
        chatImageView.layer.cornerRadius = 4
        chatImageView.clipsToBounds = true
        
        //sets the user interaction to true, so we can actually track when the image has been tapped
        chatImageView.isUserInteractionEnabled = true
        //this is where we add the target, since our method to track the taps is in this class
        //we can just type "self", and then put our method name in quotes for the action parameter
        recognizer.addTarget(self, action: "profileImageHasBeenTapped")
        //finally, this is where we add the gesture recognizer, so it actually functions correctly
        chatImageView.addGestureRecognizer(recognizer)

    }
    func profileImageHasBeenTapped(){
        self.showFullScreen!(chatImageView)
    }
    @IBOutlet weak var chatMainView: UIView!
    @IBOutlet weak var chatImageView: UIImageView!
    var showFullScreen : ((_ image : UIImageView) -> Void)?
}
