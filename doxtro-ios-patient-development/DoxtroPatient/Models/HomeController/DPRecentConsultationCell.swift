//
//  DPRecentConsultationCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 24/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol DPRecentConsultationDelegate  {
    func consultDoctor()
}
class DPRecentConsultationCell: UITableViewCell {
    var delegate : DPRecentConsultationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.consultDoctorButton.layer.cornerRadius = 20
        self.consultDoctorButton.layer.borderWidth = 2
        self.consultDoctorButton.layer.borderColor = UIColor().greenDoxtroCgColor().cgColor
        self.consultDoctorButton.clipsToBounds = true

    }
    @IBAction func consultDoctor(_ sender: Any) {
         self.delegate?.consultDoctor()
    }
    @IBOutlet weak var consultDoctorButton: UIButton!
    
}
