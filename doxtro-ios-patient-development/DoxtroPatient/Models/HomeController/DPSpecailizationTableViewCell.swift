//
//  DPSpecailizationTableViewCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 11/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class DPSpecailizationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var specializationImageView: UIImageView!
    @IBOutlet weak var specializationTitleLabel: UILabel!
    @IBOutlet weak var specializationDescriptionLabel: UILabel!
}
