//
//  DPConsultationViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 28/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase
import SwiftyJSON
import TSMessages
import IQKeyboardManagerSwift
import UserNotifications
let showChatCountAray : [String] = ["alert","docImage","docDocuments","prescription","diagnostic","text","healthRecords"]
enum BotConsultationType : String{
    case alert
    case suggestion
    case payment
    case userIntro
    case normal
    case profile
    case waiting
    case image
    case documents
    case prescription
    case closedAlert
    case diagnostic
    case healthRecords
    init?(string : String){
        switch string{
        case "alert":
            self = .alert
            break
        case "suggestion":
            self = .suggestion
            break
        case "payment":
            self = .payment
            break
        case "userIntro":
            self = .userIntro
            break
        case "profile" :
            self = .profile
            break
        case "waiting" :
            self = .waiting
            break
        case "image","docImage" :
            self = .image
            break
        case "documents" ,"docDocuments":
            self = .documents
            break
        case "prescription" :
            self = .prescription
            break
        case "diagnostic" :
            self = .diagnostic
            break
        case "text","userText" :
            self = .normal
            break
        case "closedAlert":
            self = .closedAlert
            break
        case "healthRecords" :
            self = .healthRecords
            break
        default:
            self = .normal
            break
            
        }
    }
}
enum ChatAttachmentType : Int{
    case image = 1
    case camera
    case document
    case healthrecord
    case none
    init(value : Int){
        switch value {
        case 1:
            self = .image
            break
        case 2:
            self = .camera
            break
        case 3:
            self = .document
            break
        case 4:
            self = .healthrecord
            break
        default:
            self = .none
            break
        }
    }
}
enum MedicineTime : String{
    case Morning
    case Night
    case Afternoon
}
let MorningMedicineNotificationKey = "MorningMedicineAlert"
let NightMedicineNotificationKey = "NightMorningMedicineAlert"
let AfternoonMedicineNotificationKey = "AfternoonMedicineAlert"
class ConsultationViewController: JSQMessagesViewController {
    let textCellId = "textCellId"
    var consulatationInfo : DpConsultationDetails?
    var messages = [JSQMessage]()
    var documentInteractionController : UIDocumentInteractionController?
    var chatMessageArray : [ConsultationChatMessage] = []
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    lazy var channelRef: DatabaseReference = Database.database().reference().child(FIREBASE_CONSULTATION_DBKEY)
    var channelRefHandle: DatabaseHandle?
    var receiverID : String?
    var prescriptionRecord : PrescriptionRecord?
    var diagnosticRecord : DiagnosticRecord?
    var prescriptionTime : String?
    var paymentData : PaymentData?
    @IBOutlet weak var happyPatientLabel: UILabel!
    @IBOutlet weak var superView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        addHelpButton()
        ConsultationViewController.changeUserStatus(with: "online")
        self.inputToolbar.clipsToBounds = true
        registerRequiredCollectionViewCell()
        makePaymentButton.translatesAutoresizingMaskIntoConstraints = true
        happyPatientLabel.translatesAutoresizingMaskIntoConstraints = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        if let user = DPWebServicesManager.sharedManager.user{
            self.senderId = user.userId
            self.senderDisplayName = user.name ?? ""
        }
        addPaymentButton()
        NotificationCenter.default.addObserver(self, selector: #selector(self.ayush), name: NSNotification.Name(rawValue: "updateOnlineUser"), object: nil)
    }
    func setBackButton(){
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: UIBarButtonItemStyle.bordered, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    func ayush(){
         ConsultationViewController.changeUserStatus(with: "online")
    }
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        UIViewController.switchToTab(.Consulatation, pop: true, animated: true)
    }
    func textCellTopLabel(indexPath: IndexPath) -> String? {
        let message = self.chatMessageArray[indexPath.row]
        if let timeString = message.timeStampString as? String {
            let messageDate = DXUtility.convertADate(dateStr: timeString)//DXUtility."dd MMM,yyyy"(dateStr: timeString)
            if indexPath.row - 1 > 0 {
                let attributedString = JSQMessagesTimestampFormatter.shared().relativeDate(for: messageDate as Date?)
                return attributedString
            }
        }
        return nil
    }
    func returnChatRequiredTime(timeString : String)->String{
            let messageDate = DXUtility.convertADate(dateStr: timeString)//DXUtility."dd MMM,yyyy"(dateStr: timeString)
        if let attributedString = JSQMessagesTimestampFormatter.shared().time(for: messageDate as Date?){
                return attributedString
        }
        return ""
        
    }
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let indexPath = collectionView.indexPathForItem(at: visiblePoint) {
            self.chatMessagesTimeLabel.text = self.textCellTopLabel(indexPath: indexPath)
            if let timeLabel = self.chatMessagesTimeLabel.text {
                if (timeLabel.contains("Today")) {
                    self.chatMessagesTimeLabel.text = "Today"
                } else if timeLabel.contains("Yesterday") {
                    self.chatMessagesTimeLabel.text = "Yesterday"
                }else{
                    
                }
            }
        }
        DispatchQueue.main.async {
            self.timeView.isHidden = false
            self.perform(#selector(self.timeLabelHideAfterDisplay), with: nil, afterDelay: 2.0)
        }
    }
    func timeLabelHideAfterDisplay() {
        timeView.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        observeChannels()
        observeMessagesWhenChanged()
        self.tabBarController?.tabBar.isHidden = true
        ConsultationViewController.changeUserStatus(with: "online")
        addPaymentButton()
        self.collectionView.reloadData()
    }
    
    func addPaymentButton(){
        if let consultationINfo = DPWebServicesManager.sharedManager.consultationInfo{
            if let paymentStatus = consultationINfo.paymentStatus{
                switch paymentStatus {
                case .pending:
                    var isAdded : Bool = false
                    for views in self.view.subviews{
                        if views.tag == 100 {
                            isAdded = true
                        }
                    }
                    if !isAdded{
                    paymentView.addSubview(makePaymentButton)
                    paymentView.addSubview(happyPatientLabel)
                    self.view.addSubview(paymentView)
                    self.inputToolbar.isHidden = true
                    }
                    break
                case .free,.paid:
                    if let consultationStatus = consultationINfo.status{
                        switch consultationStatus {
                        case .New:
                            for view in self.view.subviews{
                                if view.tag == 100{
                                    view.removeFromSuperview()
                                }
                            }
                            self.inputToolbar.isHidden = true
                            break
                        case .Ongoing,.Waiting,.Completed:
                            for view in self.view.subviews{
                                if view.tag == 100{
                                    view.removeFromSuperview()
                                }
                            }
                            self.inputToolbar.isHidden = false
                            break
                        case .Closed:
                            var isAdded : Bool = false
                            for views in self.view.subviews{
                                if views.tag == 100 {
                                    isAdded = true
                                }
                            }
                            if !isAdded{
                                makePaymentButton.setTitle("PAY AND RESTART CONSULTATION", for: .normal)
                                paymentView.addSubview(makePaymentButton)
                                paymentView.addSubview(happyPatientLabel)
                                self.view.addSubview(paymentView)
                                self.inputToolbar.isHidden = true
                            }
                            break
                        default:
                            break
                        }
                    }
                    break
                default:
                    break
                }
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews{
            if view.tag == 100{
                paymentView.frame = CGRect(x: 0, y: self.view.frame.height-paymentView.frame.height, width: self.view.frame.width, height: paymentView.frame.height)
                makePaymentButton.frame = CGRect(x: paymentView.frame.width * 0.1, y:8, width: paymentView.frame.width * 0.8 , height:paymentView.frame.height * 0.5)
                happyPatientLabel.frame = CGRect(x: 0, y: makePaymentButton.frame.height + 8, width: paymentView.frame.width, height: paymentView.frame.height * 0.3)
            }
        }
        
    }
    @IBAction func makePaymentClicked(_ sender: Any) {
        if let consultationINfo = DPWebServicesManager.sharedManager.consultationInfo{
            if let status = consultationINfo.status{
            if status == .Closed{
               intialteFollowUpCOnsultation()
            }else{
                DPAppEventLog.shared.logEventName("Proceeded to Pay", values: ["Proceed to pay": true as AnyObject])
                self.performSegue(withIdentifier: "showApplyCouponcontroller", sender: nil)
            }
        }
        }
       
        
        // makePayment()
    }
    func intialteFollowUpCOnsultation(){
        if let parentId = self.consulatationInfo?.consultationId{
             let params = ["parentId":parentId]
            let request = FollowUpConsultationRequest(params: params as [String :AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if error == nil,let resposne = response{
                    if let data = resposne["data"].dictionary{
                 let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ConsultationViewController
                        controller?.consulatationInfo = consulattionInfoObj
                        DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                        self.navigationController?.pushViewController(controller!, animated: true)
                    }
                }
            })
        }
       
        //let request =
    }
    var animator: ZoomToFullscreen?
    
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var makePaymentButton: UIButton!
    
    func registerRequiredCollectionViewCell(){
        collectionView.register(UINib(nibName: "DPUserIntro", bundle: nil), forCellWithReuseIdentifier: "userIntroCell")
        collectionView.register(UINib(nibName: "DPAlertUserCell", bundle: nil), forCellWithReuseIdentifier: "alertUserCell")
        collectionView.register(UINib(nibName: "DPDoctorListCell", bundle: nil), forCellWithReuseIdentifier: "doctorList")
        collectionView.register(UINib(nibName: "DPFeeConculatationCell", bundle: nil), forCellWithReuseIdentifier: "feeConsultationCell")
        collectionView.register(UINib(nibName: "DPsuccessfullyPaymentCell", bundle: nil), forCellWithReuseIdentifier: "paymentSuccess")
        collectionView.register(UINib(nibName: "DPDoctorProfileCell", bundle: nil), forCellWithReuseIdentifier: "doctorProfile")
        collectionView.register(UINib(nibName: "DDChatUserTextCVCell", bundle: nil), forCellWithReuseIdentifier: "userTextTypeCellId")
        collectionView.register(UINib(nibName: "DPWaitingConsultCell", bundle: nil),forCellWithReuseIdentifier: "waitingConsultCell")
        collectionView.register(UINib(nibName: "DPChatImageCell", bundle: nil), forCellWithReuseIdentifier: "chatImageViewCell")
        collectionView.register(UINib(nibName: "DDChatTextTypeCVCell", bundle: nil), forCellWithReuseIdentifier: textCellId)
        collectionView.register(UINib(nibName: "DPChatReceverImageCell", bundle: nil), forCellWithReuseIdentifier: "chatReceiverImageViewCell")
        collectionView.register(UINib(nibName: "DPFileMessageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "fileMessageCell")
        collectionView.register(UINib(nibName: "DPReceiverFileMessageCell", bundle: nil), forCellWithReuseIdentifier: "receiverfileMessageCell")
        collectionView.register(UINib(nibName: "DPDoctorPreceptionViewCell", bundle: nil), forCellWithReuseIdentifier: "preceptionCell")
         collectionView.register(UINib(nibName: "DPClosedAlert", bundle: nil), forCellWithReuseIdentifier: "closedAlert")
        collectionView.register(UINib(nibName: "DPHealthRecordShare", bundle: nil), forCellWithReuseIdentifier: "healthRecordShareCell")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "doctorProfile"{
            let destinationVC = segue.destination as! DoctorProfileViewController
            destinationVC.doctorId = sender as! String
        }
        if segue.identifier == "pdfReader"{
            if let destinationVC  = segue.destination as? PDFWebViewController{
                if let dict = sender as? [String : String]{
                    destinationVC.url = dict["url"] ?? ""
                    destinationVC.navTitile = dict["fileName"] ?? ""
                }
                
            }
        }
        if segue.identifier == "showApplyCouponcontroller"{
            if let destinationVC = segue.destination as? CouponViewController{
                destinationVC.consultationID = self.consulatationInfo?.consultationId
                if let consultationInfo = self.consulatationInfo{
                    if let paymentdata = self.paymentData{
                      destinationVC.couponPaymentData = paymentdata
                    }
                    destinationVC.createdConsultationInfo = consultationInfo
                }
            }
        }
        if segue.identifier == "showPreception"{
            if let destinationVC = segue.destination as? DoctorPreceptionViewController{
                if let prescriptionRecord = sender as? PrescriptionRecord{
                    destinationVC.prescriptionRecord = prescriptionRecord
                    destinationVC.time = self.prescriptionTime
                }
            }
        }
        if segue.identifier == "showDiagnostic"{
            if let destinationVC = segue.destination as? DoctorPreceptionViewController{
                if let diagnosticRecord = sender as? DiagnosticRecord{
                    destinationVC.diagnosticRecord = diagnosticRecord
                    destinationVC.type = "diagnostic"
                    destinationVC.time = self.prescriptionTime
                }
            }
        }
}
    // MARK: Firebase related methods
    private func observeChannels() {
        ABProgressIndicator.shared.showAnimator()
        channelRefHandle = channelRef.child((self.consulatationInfo?.consultationId)!).observe(DataEventType.value, with: { (snapshot) -> Void in // 1
            debugPrint(snapshot)
            ABProgressIndicator.shared.hideAnimator()
            if let channelData = snapshot.value as? Dictionary<String, AnyObject>{ // 2
                let id = snapshot.key
                self.chatMessageArray.removeAll()
                for (key,obj) in channelData{
                    if let objs = JSON(obj).dictionary{
                        if let from = objs["from"]?.string{
                            if from == "Doctor"{
                                if let currentUserID = DPWebServicesManager.sharedManager.user?.userId{
                                if let id = objs["_id"]?.string,id != currentUserID{
                                    if let status = objs[status_key]?.string {
                                        if status == "Sent" || status == "sent" {
                                            self.channelRef.child((self.consulatationInfo?.consultationId!)!).child(key).updateChildValues(["status" : "read"])
                                        }
                                    }
                                }
                            }
                            }
                        }
                    }
                    self.chatMessageArray.append(ConsultationChatMessage(dict : JSON(obj),isMediaMessage : false))
                }
            }
            self.chatMessageArray.sort{DXUtility.convertADate(dateStr: $0.sortingTimeStamp).compare((DXUtility.convertADate(dateStr: ($1.sortingTimeStamp)) as Date) as Date) == ComparisonResult.orderedAscending}
            // self.chatMessageArray.append(ConsultationChatMessage(dict : JSON(channelData["Bot_1504020098007"]),isMediaMessage : false))
            self.finishReceivingMessage()
        })
    }
    private func observeMessagesWhenChanged() {
//        let messageQuery = channelRef.queryLimited(toLast:20)
//        // 2. We can use the observe method to listen for new
//        // messages being written to the Firebase DB
//        channelRefHandle = messageQuery.observe(.childChanged, with: { (snapshot) -> Void in
//            if let messageData = snapshot.value as? Dictionary<String, AnyObject> {
//                debugPrint(messageData)
//                if let status = messageData[status_key] as? String {
//                    if status == "read" {
//                        for(index, element) in self.chatMessageArray.enumerated() {
//                            var message = element
//                            if let text = messageData["data"] as? String {
//                                if text == message.data?.text {
//                                    message.status = "read"
//                                }
//                                if message.status == "sent" || message.status == "Sent" {
//                                    message.status = "read"
//                                }
//                                self.chatMessageArray[index] = message
//                            }
//                        }
//                    }
//                }
//                self.finishReceivingMessage()
//            }
//        })
    }
//    private func observeMessagesWhenChanged() {
//        let messageQuery = channelRef.queryLimited(toLast:10000)
//        // 2. We can use the observe method to listen for new
//        // messages being written to the Firebase DB
//        channelRefHandle = messageQuery.observe(.childChanged, with: { (snapshot) -> Void in
//            if let messageData = snapshot.value as? Dictionary<String, AnyObject> {
//                if let status = messageData[status_key] as? String {
//                  //  if status == "read" {
//                        if let message = self.chatMessageArray.last {
//                            var lasMessage = message
//                            lasMessage = ConsultationChatMessage(dict: JSON(messageData), isMediaMessage: false)
//                            self.chatMessageArray[self.chatMessageArray.count - 1] = lasMessage
//                        }
//                    }
//               // }
//                DispatchQueue.main.async {
//                    self.collectionView.reloadData()
//                }
//            }
//        })
//    }
    func updateCosultationListAndRecentOCnsultation(){
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "RefreshConsultationList")))
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateCosultationListAndRecentOCnsultation()
        if let channelReference = channelRefHandle{
            self.channelRef.child((self.consulatationInfo?.consultationId)!).removeObserver(withHandle: channelReference)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateOnlineUser"), object: nil)
        ConsultationViewController.changeUserStatus(with: "offline")
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }
    deinit {
        
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatMessageArray.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData {
        return chatMessageArray[indexPath.row]
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
        
    }
    func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = chatMessageArray[indexPath.item] // 1
        if message.id == self.senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? DoctorPreceptionCell{
            if let reportType = cell.reportType{
                switch reportType {
                case .diagnostic:
                    if let diagnosticRecord = self.diagnosticRecord{
                        self.performSegue(withIdentifier: "showDiagnostic", sender: diagnosticRecord)
                    }
                    break
                case .prescription:
                    if let precriptionRecord = self.prescriptionRecord{
                        self.performSegue(withIdentifier: "showPreception", sender: precriptionRecord)
                    }
                    break
                }
            }
        }
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let chatMessageObj = chatMessageArray[indexPath.row]
        if let msgType = chatMessageObj.msgType{
            if let cell = cellFromMsgType(msgType: msgType, chatMessageObj: chatMessageObj, indexPath: indexPath){
                return cell
            }
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"userTextTypeCellId", for: indexPath) as! DDChatUserTextCVCell
        let text = chatMessageObj.text
        let descriptionText = chatMessageObj.from! + "\n" + text
        cell.descriptionLabel.text = descriptionText
        cell.timeLabel.text = DXUtility.convertDateAndTime(dateStr: chatMessageObj.timeStampString)
        //       let cells = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        //            let message = chatMessageArray[indexPath.item]
        //            cells.textView?.textColor = UIColor.black
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let chatMessageObj = chatMessageArray[indexPath.row]
        if let msgType = chatMessageObj.msgType{
            switch msgType{
            case .profile :
                return CGSize(width: UIScreen.main.bounds.width, height: 170)
            case .alert:
                 return CGSize(width: UIScreen.main.bounds.width , height:140)
            case .suggestion:
                return CGSize(width: UIScreen.main.bounds.width, height:200)
            case .payment:
                if DPWebServicesManager.sharedManager.consultationInfo?.paymentStatus != .pending{
                    return CGSize(width: UIScreen.main.bounds.width * 0.5, height:25
                    )
                }
                return CGSize(width: UIScreen.main.bounds.width * 0.97, height:162)
            case .userIntro:
                var aText = ""
                let font = UIFont.systemFont(ofSize: 15.0)
                    if let note = chatMessageObj.data?.textData?.note as? String {
                        aText =  note
                    }
            var height =  45 + aText.height(withConstrainedWidth: screenWidth - (2*20), font: font)
                height = height + 70 + aText.height(withConstrainedWidth: screenWidth - (2*20), font: font)
                return CGSize(width:screenWidth - 20, height: height)
            case .normal:
                if chatMessageObj.id == DPWebServicesManager.sharedManager.user?.userId{
                    var aText = ""
                    let font = UIFont.systemFont(ofSize: 15.0)
                    if let text = chatMessageObj.text as? String {
                        aText = text
                    }
                    var height =  60 + aText.height(withConstrainedWidth: screenWidth - (2*20), font: font)
                    
                    //height = height + (chatMessageObj.from?.height(withConstrainedWidth: screenWidth - (2*20), font: font))!
                    return CGSize(width:screenWidth - 20, height: height)
                }else{
                    var aText = ""
                    let font = UIFont.systemFont(ofSize: 15.0)
                    if let text = chatMessageObj.text as? String {
                        aText = text
                    }
                    var height =  80 + aText.height(withConstrainedWidth: screenWidth - (2*20), font: font)
                    
                    height = height + (chatMessageObj.senderName?.height(withConstrainedWidth: screenWidth - (2*20), font: font))!
                    return CGSize(width:screenWidth - 20, height: height)
                }
            case .waiting:
                for  value in chatMessageArray{
                    if  value.msgType == .profile{
                        return CGSize(width: 0 , height:0)
                    }
                }
                return CGSize(width: UIScreen.main.bounds.width , height:140)
            case .image:
                 return CGSize(width: UIScreen.main.bounds.width , height: 200)
            case .documents:
                 return CGSize(width: UIScreen.main.bounds.width, height: 120)
            case .prescription :
                if let status = DPUser.getReminderStatus(){
                    if status{
                        return CGSize(width: UIScreen.main.bounds.width , height: 440)
                    }else{
                        return CGSize(width: UIScreen.main.bounds.width , height: 350)
                    }
                }
                return CGSize(width: UIScreen.main.bounds.width , height: 350)
            case .closedAlert:
                return CGSize(width: UIScreen.main.bounds.width, height: 100)
            case .healthRecords:
                return CGSize(width: UIScreen.main.bounds.width, height: 100)
                
                case .diagnostic:
                return CGSize(width: UIScreen.main.bounds.width , height: 300)
            }
        }
        return CGSize(width: 200, height: 150)
    }
}
//MARK:CHATACTION
extension ConsultationViewController{
    override func didPressAccessoryButton(_ sender: UIButton!) {
        let openType = ChatAttachmentType(value: sender.tag)
        switch openType{
        case .image:
            openRespectiveAttachmentType(with: .photoLibrary)
            break
        case .camera:
            openRespectiveAttachmentType(with: .camera)
            break
        case .document:
            openRespectiveAttachmentType(documents: true)
            break
        case .healthrecord:
    UIAlertController.showAlertWithMessage("Are you sure you want to share health records with doctor for this ongoing consultation", title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
                
            }, actionButtonHandler: { (_ , _) in
                if let relativesID = self.consulatationInfo?.relativesId{
                    self.send(with: relativesID, msgType: "healthRecords")
                }
            })
            break
        case .none:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "UnAuthorizedType", type: .error)
            break
        default:
            break
        }
    }
    //    override func didPressRightButton(_ sender: Any!) {
    //        send(with: bottomTextView.text,msgType : "userText")
    //    }
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        send(with: text,msgType : "userText")
    }
    func send(with text : String,msgType : String,documentName : String = ""){
        let itemRef = channelRef.child((self.consulatationInfo?.consultationId)!).childByAutoId() // 1
        if let user =  DPWebServicesManager.sharedManager.user{
            var messageItem = [ // 2
                "_id": user.userId,
                "sender" : user.name,
                "from": "Patient",
                "status" : "sent",
                "msgType" : msgType,
                "timeStamp" :  ServerValue.timestamp(),//DXUtility.getCurrentDate(),
                "data":  text,
                ] as [String : Any]// 1// 3
            if let receiverId = self.receiverID{
                messageItem["receiver"] = receiverId
            }
            if documentName != ""{
                messageItem["data"] = ["documentName":documentName,"documentUrl":text]
            }
            itemRef.setValue(messageItem)
            JSQSystemSoundPlayer.jsq_playMessageSentSound() // 4
            finishSendingMessage() // 5
        }
        /*  {
         _id: “_id of sender”,
         From : “Patient/Doctor/Bot”,
         Status : “Sent/Read/Delivered”,
         msgType :   “userIntro/alert/suggestion/payment/userText/text/...”,
         Data : {
         text : “required_message or data(json object)”// This object may change according to message type
         }
         }*/
        
        
        //        self.addMessage(withId: DPAppPreferences.getUserId() , name: "Doctor" , messgeType: "text", data: [text_key : text! as AnyObject] , status: "Sent" )
        //
        //        // 5
        //        self.finishReceivingMessage()
    }
    
}
extension ConsultationViewController : DPConsultationFeeDelegate{
    func makePayment() {
        if let user = DPWebServicesManager.sharedManager.user{
            let params = ["consultationId":self.consulatationInfo?.consultationId,"firstName" : user.name]
            let request = MakePaymentRequest(params: params as [String : AnyObject] )
            ABProgressIndicator.shared.showAnimator()
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if error == nil,let response = response{
                    if let data = response["data"].dictionary{
                        let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                        consulattionInfoObj.patientFee = response["patientFee"].intValue
                        DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                    }
                    self.collectionView.reloadData()
                }
            })
        }
    }
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
extension ConsultationViewController : DoctorProfileDelegate{
    func showDoctorProfile(wirh doctorId: String) {
        self.performSegue(withIdentifier: "doctorProfile", sender: doctorId)
    }
}
extension ConsultationViewController{
    func cellFromMsgType(msgType : BotConsultationType,chatMessageObj:ConsultationChatMessage,indexPath:IndexPath)->UICollectionViewCell?{
        switch msgType{
        case .alert:
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "alertUserCell", for: indexPath) as? UserAlertCell
            cell?.timeLabel.text = returnChatRequiredTime(timeString:chatMessageObj.timeStampString)
            if let introInfo = chatMessageObj.data{
                cell?.alerUserTextLabel.text = introInfo.text
            }
            return cell!
        case .userIntro:
            let cell =  super.collectionView.dequeueReusableCell(withReuseIdentifier: "userIntroCell", for: indexPath) as? UserIntro
            cell?.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
            if let introInfo = chatMessageObj.data?.textData{
                cell?.headerTitleLabel.text = (introInfo.firstName)! + "is consulting for " + (introInfo.relation)!
                cell?.nameAgeGenderLabel.text = introInfo.relativesName! + ", " + introInfo.relativesGender! + ", " + introInfo.relativesAge!
                cell?.helathConcernLabel.text = introInfo.note

            }
            return cell!
        case .suggestion :
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "doctorList", for: indexPath) as? DoctorListCell
            cell?.doctorList = (chatMessageObj.data?.doctorList)!
            cell?.delegate = self
            return cell!
        case .payment :
            if let consultationINfo = DPWebServicesManager.sharedManager.consultationInfo{
                if consultationINfo.paymentStatus == .pending{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "feeConsultationCell", for: indexPath) as? ConsultationFeeCell
                    if let paymentData = chatMessageObj.paymentData{
                         self.paymentData = paymentData
                    }
                    if let isPreapplied = chatMessageObj.paymentData?.isPreApplied{
                        if isPreapplied{
                            if let fee = chatMessageObj.paymentData,let patientFee = fee.patientFee,let discountValue = fee.consultingFee{
                                cell?.feesLabel.text = "₹" + "\(String(describing: patientFee))"
                                self.makePaymentButton.setTitle("PROCEED TO PAY ₹" + "\(patientFee)", for: .normal)
                                cell?.dicountValue.text = "₹\(String(describing:discountValue ))"
                            }
                        }else{
                            if let fee = chatMessageObj.paymentData,let patientFee = fee.patientFee{
                             cell?.dicountValue.text = ""
                             cell?.feesLabel.text = "\(String(describing: patientFee))"
                             self.makePaymentButton.setTitle("PROCEED TO PAY ₹" + "\(patientFee)", for: .normal)
                            }
                        }
                    }
                    cell?.delegate = self
                    return cell!
                }else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentSuccess", for: indexPath)
                    return cell
                }
            }
        case .profile :
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "doctorProfile", for: indexPath) as? DoctorProfileCollectionViewCell
            cell?.doctorNameLAbel.text = chatMessageObj.data?.textData?.firstName
            cell?.doctorId = (chatMessageObj.data?.textData?.Rid)!
            var specializationQualification : String = ""
            for (index,degree) in (chatMessageObj.data?.textData?.qualifucationArray.enumerated())!{
                specializationQualification = specializationQualification  + degree.degree!
                if ((chatMessageObj.data?.textData?.qualifucationArray.count)! - 1) != index {
                    specializationQualification = specializationQualification + ","
                }
            }
            cell?.experienceLabel.text = "Experience : " + (chatMessageObj.data?.textData?.experience)! + "yrs"
             cell?.specializationInfoLabel.text = specializationQualification
            if DPWebServicesManager.sharedManager.consultationInfo?.status != .Completed && DPWebServicesManager.sharedManager.consultationInfo?.status != .Closed{
                if DPWebServicesManager.sharedManager.consultationInfo?.status != .Completed && DPWebServicesManager.sharedManager.consultationInfo?.status != .Closed{
                    DPWebServicesManager.sharedManager.consultationInfo?.status = .Ongoing
                    self.consulatationInfo?.status = .Ongoing
                    addPaymentButton()
                }
            }
            if let receiverId = chatMessageObj.data?.textData?.Rid{
               self.receiverID = receiverId
            }
            cell?.delegate = self
           if self.consulatationInfo?.status == .Ongoing{
            
            }
            return cell!
        case .waiting:
            for  value in chatMessageArray{
                if  value.msgType == .profile{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "waitingConsultCell", for: indexPath) as? ConsultingWaitingCell
                    cell?.frame = CGRect(x: 0, y: 0, width: 0, height: 0 )
                    return cell!
                }
            }
            if DPWebServicesManager.sharedManager.consultationInfo?.status == .New{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "waitingConsultCell", for: indexPath) as? ConsultingWaitingCell
                return cell!
            }else if DPWebServicesManager.sharedManager.consultationInfo?.status == .Waiting{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "waitingConsultCell", for: indexPath) as? ConsultingWaitingCell
                return cell!
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "waitingConsultCell", for: indexPath) as? ConsultingWaitingCell
                cell?.frame = CGRect(x: 0, y: 0, width: 0, height: 0 )
                return cell!
            }
        case .image :
            if chatMessageObj.id == DPWebServicesManager.sharedManager.user?.userId{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatImageViewCell", for: indexPath) as? ChatImageColelctionViewCell
                if let imageUrl = URL(string: chatMessageObj.text){
                    cell?.chatImageView.af_setImage(withURL: imageUrl, placeholderImage: UIImage(named: "AppIcon"))
                }
                cell?.showFullScreen = {[weak self](imageView : UIImageView) in
                    if let weakSelf  = self{
                        let viewController = self?.storyboard?.instantiateViewController(withIdentifier: "FullViewImage") as!  JPFullScreenImageController
                        viewController.image = cell?.chatImageView.image
                        let animator = ZoomToFullscreen.animator()
                        self?.animator = animator
                        animator.presentController(viewController, fromController: weakSelf, fromImageView: imageView)
                    }
                }
                if let status = chatMessageObj.status{
                    if let image = getStatusImageFromStatusText(status) {
                        cell?.statusImageView.image = image
                    }
                }
                cell?.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                return cell!
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatReceiverImageViewCell", for: indexPath) as? ChatReceiverImageCollectionViewCell
                cell?.chatImageView.af_setImage(withURL: URL(string: chatMessageObj.text ?? "")!, placeholderImage: UIImage(named: "AppIcon"))
                cell?.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                cell?.showFullScreen = {[weak self](imageView : UIImageView) in
                    if let weakSelf  = self{
                        let viewController = self?.storyboard?.instantiateViewController(withIdentifier: "FullViewImage") as!  JPFullScreenImageController
                        viewController.image = cell?.chatImageView.image
                        let animator = ZoomToFullscreen.animator()
                        self?.animator = animator
                        animator.presentController(viewController, fromController: weakSelf, fromImageView: imageView)
                    }
                }
                return cell!
            }
        case .documents :
            if chatMessageObj.id == DPWebServicesManager.sharedManager.user?.userId{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fileMessageCell", for: indexPath) as? FileMessageCollectionViewCell
                if let urlString = chatMessageObj.documentsData?.documentUrl as? String{
                    cell?.PDfUrl =  urlString
                }
                if let documentName = chatMessageObj.documentsData?.documentName {
                    cell?.fileName.text = documentName
                    cell?.fileNames = documentName
                }
                
                cell?.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                if let status = chatMessageObj.status{
                    if let image = getStatusImageFromStatusText(status) {
                        cell?.statusImageView.image = image
                    }
                }
                cell?.delegate = self
                return cell!
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "receiverfileMessageCell", for: indexPath) as? ReceiverFileMessageCollectionViewCell
                if let urlString = chatMessageObj.documentsData?.documentUrl as? String{
                    cell?.PDfUrl =  urlString
                }
                if let documentName = chatMessageObj.documentsData?.documentName {
                    cell?.fileName.text = documentName
                    cell?.fileNames = documentName
                }
                
                cell?.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                cell?.delegate = self
                return cell!
            }
        case .prescription:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "preceptionCell", for: indexPath) as? DoctorPreceptionCell
            setCellUsingReminderState(cell: cell!)
            cell?.delegate = self
            cell?.reportType = .prescription
            cell?.timeLabel.isHidden = false;
            cell?.pillsImage.isHidden = false;
            if let prescriptionrecord = chatMessageObj.prescriptionRecord,let doctorProfile = prescriptionrecord.doctorProfile{
                cell?.dunsLabel.text = "Reg. Number : " + doctorProfile.duns!
                self.prescriptionRecord = prescriptionrecord
                cell?.DoctorName.text = doctorProfile.firstName
                cell?.qualificationLabel.text = doctorProfile.qualificationString
                cell?.timeLabel.text = "Date: " + DXUtility.convertDateAndTime(dateStr: chatMessageObj.timeStampString)
                 cell?.mainTimeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                self.prescriptionTime = chatMessageObj.timeStampString
                if let relativeInfo = prescriptionrecord.relativeInfo{
                    if let name = relativeInfo.name,let age = relativeInfo.age,let gender = relativeInfo.gender{
                        var mainString = "Name: "
                        mainString = mainString + name + " "
                        mainString = mainString  + String(age) + ","
                        mainString = mainString + gender
                        cell?.patientInfoLabel.text = mainString
                    }
                }
                if prescriptionrecord.medicationInfo.count > 0 {
                    if let medicineObject = prescriptionrecord.medicationInfo.first{
                        cell?.medicineName.text = medicineObject.name
                        cell?.doseTime.text = medicineObject.doseTimeString
                        if let duration = medicineObject.duration,let durationUnit = medicineObject.durationUnit{
                        cell?.maedicineDuration.text = "Duration : " + "\(duration) " + durationUnit
                        }
                        cell?.timeTakenLabel.text = "Time : " + medicineObject.foodWarning!
                    }
                }
            }
            cell?.prescriptionTextLabel.text = "Doctor Sent you the prescription"
            return cell!
            
        case .diagnostic:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "preceptionCell", for: indexPath) as? DoctorPreceptionCell
             cell?.reportType = .diagnostic
            cell?.delegate = self
            cell?.reminderView.isHidden = true
            cell?.serReminder.isHidden = true
            if let diagnosticRecordData = chatMessageObj.diagnosticRecord,let doctorProfile = diagnosticRecordData.doctorProfile{
                cell?.dunsLabel.text = "Reg. Number : " + doctorProfile.duns!
                 cell?.mainTimeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                self.diagnosticRecord = diagnosticRecordData
                cell?.DoctorName.text = doctorProfile.firstName
                cell?.qualificationLabel.text = doctorProfile.qualificationString
                cell?.timeLabel.text = "Date: " + DXUtility.convertDateAndTime(dateStr: chatMessageObj.timeStampString)
                cell?.pillsImage.isHidden = true;
                self.prescriptionTime = chatMessageObj.timeStampString
                if let relativeInfo = diagnosticRecordData.relativeInfo{
                    if let name = relativeInfo.name,let age = relativeInfo.age,let gender = relativeInfo.gender{
                        var mainString = "Name: "
                        mainString = mainString + name + " "
                        mainString = mainString  + String(age) + ","
                        mainString = mainString + gender
                        cell?.patientInfoLabel.text = mainString
                    }
                }
                if diagnosticRecordData.instructionInfo.count > 0 {
                    if let medicineObject = diagnosticRecordData.instructionInfo.first{
                        cell?.medicineName.text = medicineObject.name
                        cell?.doseTime.text = "";
                        cell?.maedicineDuration.text = "";//Duration : " + "\(duration) " + durationUnit
                        cell?.timeTakenLabel.text = medicineObject.instruction
                    }
                }
                cell?.prescriptionTextLabel.text = "Doctor sent diagnostic test for you";
            }
            return cell!
        case .closedAlert :
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "closedAlert", for: indexPath) as? ClosedAlert
            cell?.sessionEndMessageLabel.text = chatMessageObj.text
            return cell!
        case .healthRecords :
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "healthRecordShareCell", for: indexPath) as? HealthRecordShareCell
            cell?.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
            if let status = chatMessageObj.status{
                if let image = getStatusImageFromStatusText(status) {
                    cell?.statusImageView.image = image
                }
            }
            return cell!
        default:
            if chatMessageObj.id == DPWebServicesManager.sharedManager.user?.userId{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: textCellId, for: indexPath) as! DDChatTextTypeCVCell
                let text = chatMessageObj.text
                let descriptionText =  text
                cell.textLabel.text = descriptionText
                // cell.timeLabel.text = DXUtility.convertDateAndTime(dateStr: message.timeStamp)
                cell.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                if let status = chatMessageObj.status{
                    if let image = getStatusImageFromStatusText(status) {
                        cell.statusImageView.image = image
                    }
                }
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"userTextTypeCellId", for: indexPath) as! DDChatUserTextCVCell
                let text = chatMessageObj.text
                let descriptionText = text
                cell.descriptionLabel.text = descriptionText
                cell.senderNameLabel.text = chatMessageObj.senderName
                cell.timeLabel.text = returnChatRequiredTime(timeString: chatMessageObj.timeStampString)
                return cell
            }
        }
        return nil
    }
    func getStatusImageFromStatusText(_ status: String) -> UIImage? {
        if status == "read" {
            return UIImage(named: "Doubletick")
        } else if status == "sent" || status == "Sent" {
            return UIImage(named: "singletick")
        } else {
            return UIImage()
        }
    }
    func setCellUsingReminderState(cell : DoctorPreceptionCell){
        if let status = DPUser.getReminderStatus(){
            if status{
                cell.reminderView.isHidden = false
                cell.serReminder.isHidden = true
            }else{
                cell.reminderView.isHidden = true
                 cell.serReminder.isHidden = false
            }
        }else{
            cell.reminderView.isHidden = true
            cell.serReminder.isHidden = false
        }
        
    }
    
}
extension ConsultationViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            uploadPhoto(image: editedImage)
        }
        else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadPhoto(image: image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func uploadPhoto(image: UIImage) {
        let storageRef: StorageReference = Storage.storage().reference().child("chat_documents/").child((DPWebServicesManager.sharedManager.user?.userId)! + "ayushImage.png" + "\(Date())")
        let resizeImage = DXUtility.shared.resizeImage(image: image, newWidth: 400)
        if let uploadData = UIImagePNGRepresentation(resizeImage) {
            ABProgressIndicator.shared.showAnimator()
            storageRef.putData(uploadData, metadata: nil, completion: { (metaData, error) in
                if error == nil {
                    if let user = DPWebServicesManager.sharedManager.user{
                        self.send(with: (metaData?.downloadURL()?.absoluteString)!,msgType: "image")
                        //self.updateUserProfile(with: ["profilePic": metaData?.downloadURL()?.absoluteString as AnyObject,"_id": user.userId as AnyObject])
                        ABProgressIndicator.shared.hideAnimator()
                    }else{
                        ABProgressIndicator.shared.hideAnimator()
                    }
                }else{
                    ABProgressIndicator.shared.hideAnimator()
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: error?.localizedDescription, type: .error)
                }
            })
        }
    }
}

extension ConsultationViewController : UIDocumentMenuDelegate,UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        
        let cico = url as URL
        uploadDocument(fileUrl: cico,fileName: cico.lastPathComponent)
        
        //optional, case PDF -> render
        //displayPDFweb.loadRequest(NSURLRequest(url: cico) as URLRequest)
        
        
        
        
    }
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        
        
        dismiss(animated: true, completion: nil)
        
        
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    func opennPDf()
    {
        self.documentInteractionController = UIDocumentInteractionController(url: URL(string: "file:///private/var/mobile/Containers/Data/Application/75585354-9BA3-4C73-9A15-25DE4BBECEDE/tmp/com.doxtro.app-Inbox/iOSTechOverview.pdf")!)
        self.documentInteractionController?.delegate = self
        self.documentInteractionController?.presentPreview(animated: true)
    }
    func  uploadDocument(fileUrl : URL,fileName : String){
        ABProgressIndicator.shared.showAnimator()
        // Create a reference to the file you want to upload
        let docRef = Storage.storage().reference().child("chat_documents/").child((DPWebServicesManager.sharedManager.user?.userId)! + "ayushImage.png" + "\(Date())")
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = docRef.putFile(from: fileUrl, metadata: nil) { metadata, error in
            ABProgressIndicator.shared.hideAnimator()
            if let error = error {
                
            } else{
                // Metadata contains file metadata such as size, content-type, and download URL.
                if let downloadURL = metadata!.downloadURL()?.absoluteString{
                    self.send(with:downloadURL , msgType: "documents",documentName: fileName)
                }
            }
        }
        
    }
}
extension ConsultationViewController : FileMessageCollectionViewCellDelegate{
    func openPDf(With url: String, fileName: String) {
        self.performSegue(withIdentifier: "pdfReader", sender: ["fileName" : fileName,"url" : url])
    }
}
extension ConsultationViewController{
    func openRespectiveAttachmentType(with sourceType : UIImagePickerControllerSourceType = .photoLibrary,documents : Bool = false){
        if !documents{
            let imagePickerController = UIImagePickerController()
            imagePickerController.navigationBar.tintColor = UIColor().greenDoxtroCgColor()
            imagePickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePickerController.allowsEditing = true
            if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                imagePickerController.sourceType = sourceType
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }else{
            let importMenu = UIDocumentMenuViewController(documentTypes: ["public.composite-content"], in: .import)
            importMenu.delegate = self as? UIDocumentMenuDelegate
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
        }
    }
}
extension ConsultationViewController{
class func changeUserStatus(with status : String){
         let currentUserRef = Database.database().reference().child("Users")
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
           let userRef = currentUserRef.child(userID)
           let _idRef = userRef.child("_id")
           let statusRef = userRef.child("status")
           let timeStampRef = userRef.child("timeStamp")
            _idRef.setValue(userID)
            statusRef.setValue(status)
            timeStampRef.setValue(ServerValue.timestamp())
}
}
}

//MARK: Prescription and diagnostic delegate
extension ConsultationViewController : ShowFullPrecriptionDelegate{
    func showFullPrescription(reportType : ReportType) {
        //diagnosticRecord
        switch reportType {
        case .diagnostic:
            if let diagnosticRecord = self.diagnosticRecord{
                self.performSegue(withIdentifier: "showDiagnostic", sender: diagnosticRecord)
            }
        break
        case .prescription:
            if let precriptionRecord = self.prescriptionRecord{
                self.performSegue(withIdentifier: "showPreception", sender: precriptionRecord)
            }
        break
        }
    }
    func deleteRminder() {
        DPUser.saveReminderStatus(status: false)
        self.collectionView.reloadData()
    }
    
    func setReminder() {
        DPUser.saveReminderStatus(status: true)
        self.collectionView.reloadData()
        var  neededReminderTimeArray : [String : Bool] = [:]
        var morning : [Int] = []
        var afternoon : [Int] = []
        var night : [Int] = []
        var morningDays : Int?
        var afternoonDays : Int?
        var nightDays : Int?
        if let medicineArray = self.prescriptionRecord?.medicationInfo{
            for obj in medicineArray{
                for (value) in obj.doseTimeArrays{
                    switch value.stringValue{
                    case MedicineTime.Morning.rawValue :
                        neededReminderTimeArray[MedicineTime.Morning.rawValue] = true
                        if let duration = obj.duration{
                        morning.append(duration)
                        }
                        morningDays = morning.max()
                        break
                    case MedicineTime.Afternoon.rawValue :
                        neededReminderTimeArray[MedicineTime.Afternoon.rawValue] = true
                        if let duration = obj.duration{
                          afternoon.append(duration)
                            
                        }
                        afternoonDays = afternoon.max()
                        break
                    case MedicineTime.Night.rawValue :
                        neededReminderTimeArray[MedicineTime.Night.rawValue] = true
                        if let duration = obj.duration{
                          night.append(duration)
                        }
                        nightDays = night.max()
                        break
                    default :
                        break
                    }
                }
            }
        }
        for value in neededReminderTimeArray{
            switch value.key{
            case MedicineTime.Morning.rawValue :
                if value.value{
                    showNotifiCation(hour: 08, minute: 00, notificationKey: MorningMedicineNotificationKey)
                }
                break
            case MedicineTime.Afternoon.rawValue :
                if value.value{
                    showNotifiCation(hour: 13, minute: 00, notificationKey: AfternoonMedicineNotificationKey)
                }
                break
            case MedicineTime.Night.rawValue :
                if value.value{
                    showNotifiCation(hour: 20, minute: 00, notificationKey: NightMedicineNotificationKey)
                }
                break
            default :
                break
            }
        }
        
    }
    func showNotifiCation(hour : Int,minute : Int,days : Int = 12,notificationKey : String){
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            let content = UNMutableNotificationContent()
            content.title = "Medicine Time"
            content.body = "Time For medicine"
            content.categoryIdentifier = "alarm"
            content.sound = UNNotificationSound.default()
            var date = DateComponents()
           // date.day = days
            date.hour = hour
            date.minute = minute
            date.timeZone = NSTimeZone.local
            let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: true)
            let request = UNNotificationRequest(identifier: notificationKey, content: content, trigger: trigger)
            center.add(request)
            }
        }
    }
}
enum ReportType : Int{
    case diagnostic
    case prescription
}
//doxtroSupportView
extension ConsultationViewController{
    func showDoxtroSupportView(){
        let view =  Bundle.main.loadNibNamed("DPHelpDoxtroView", owner: nil, options: nil)
        if let mainView = view?[0] as? HelpDoxtroView{
            //  mainView.delegate = self
            mainView.frame = (UIApplication.getTopViewController()?.view.bounds)!
            UIApplication.getTopViewController()?.view.addSubview(mainView)
        }
        
    }
    func addHelpButton(){
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "Helpicon"), style: .done, target: self, action: #selector(self.showDoxtroSupportAction))
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    func showDoxtroSupportAction(){
    showDoxtroSupportView()
    }
}
