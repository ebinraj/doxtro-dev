//
//  DPAddNewMemberView.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 21/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
let relationImageArray =               ["Spouse","Dad","Mom","Son","Daughter","Brother","Sister","Others"]
let  relationGenderArray = ["Male","Male","Female","Male","Female","Male","Female","Male"]
class DpAddMemberView: UIView {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    var delegate : DPAddNewMemberDelegate?
    var sortedSegmentViews : [UIView]?
    var selectedgenderType : DPgenderType = .Male
    var selectedRelation : String = "Spouse"
    var selectedIndex : IndexPath?
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName: "DPMemberRelaitionView", bundle: nil), forCellWithReuseIdentifier: "relativeCelltype")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        setUpNewsTypeSegmentController()
        self.ageTextField.addBootomBorderLine(width: 1)
        self.userNameTextField.addBootomBorderLine(width: 1)
        self.sortedSegmentViews = genderSegmentControl.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
        changeToSelectedSegmentColor(selectedIndex: 0)
    }
    @IBAction func genderSelection(_ sender: Any) {
        if sender is UISegmentedControl{
            if let segmentControl = sender as? UISegmentedControl{
                changeToSelectedSegmentColor(selectedIndex: segmentControl.selectedSegmentIndex)
            }
        }
        
    }
    func changeToSelectedSegmentColor(selectedIndex : Int){
        switch selectedIndex {
        case 0:
            self.selectedgenderType = .Male
            if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[1]{
                view.backgroundColor = UIColor.white
            }
            break
        case 1:
            self.selectedgenderType = .Female
            if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[0]{
                view.backgroundColor = UIColor.white
            }
            break
        default:
            break
        }
    }
    
    func setUpNewsTypeSegmentController(){
        genderSegmentControl.layer.masksToBounds = true
        self.genderSegmentControl.tintColor = UIColor.clear
        genderSegmentControl.backgroundColor = UIColor.white
        genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .selected)
        genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .normal)
        //NSBackgroundColorAttributeName:UIColor.magenta
    }
    
    @IBAction func saveUserInfo(_ sender: Any) {
        
        if let user = DPWebServicesManager.sharedManager.user{
            if self.userNameTextField.text == ""{
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please Enter Member Name", type: .error)
            }else if self.ageTextField.text == ""{
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please enter member age", type: .error)
            }else {
            let params  : [String : AnyObject] = ["patientId":user.userId as AnyObject,
                                                  "relation": self.selectedRelation as AnyObject,
                                                  "name": self.userNameTextField.text as AnyObject,
                                                  "age": ageTextField.text as AnyObject,
                                                  "gender": self.selectedgenderType.rawValue as AnyObject]
                var values = [String : AnyObject]()
                values["Add a Member"] = ["Name" : self.userNameTextField.text as AnyObject, "Age" : self.ageTextField.text as AnyObject, "Gender" : self.selectedgenderType as AnyObject] as AnyObject
                DPAppEventLog.shared.logEventName("Consultation requested", values: values)
            self.delegate?.saveAddNewMemberInfo(params: params)
            }
        }
    }
    @IBAction func removeUserInfo(_ sender: Any) {
        self.delegate?.removeAddNewMemberView()
    }
}
extension DpAddMemberView : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "relativeCelltype", for: indexPath) as? RelativeTypeCell
     cell?.memberNameLabel.text = relationImageArray[indexPath.row]
        cell?.memberImageView.image = relationImageArray[indexPath.row].imageForRelation(with:relationGenderArray[indexPath.row])
        if self.selectedIndex == nil{
            if indexPath.row == 0{
                self.selectedIndex = indexPath
            }
        }
        if let cell = cell{
        cell.setCircleAndTextColor(with: cell.memberImageView, index: indexPath.row , selectedIndex: selectedIndex?.row)
        }
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relationImageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        self.selectedRelation = relationImageArray[indexPath.row]
        collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/5, height: collectionView.bounds.height)
    }
}
class RelativeTypeCell : UICollectionViewCell{
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var memberImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        memberImageView?.makeCircleandBorder(with: (UIColor().grayDoxtroCgColor()?.cgColor)!)
        memberNameLabel.textColor = UIColor.darkGray
    }
    func setCircleAndTextColor(with viewType : UIView,index : Int,selectedIndex : Int?=nil){
        switch viewType {
        case is UIImageView:
            let imageView = viewType as? UIImageView
            if let selectedIndex = selectedIndex{
                if selectedIndex == index{
                    imageView?.makeCircleandBorder(with: (UIColor().greenDoxtroCgColor().cgColor),width: 2.5)
                    break
                }
            }
            imageView?.makeCircleandBorder(with: (UIColor().grayDoxtroCgColor()?.cgColor)!)
            break
        case is UILabel :
            let label = viewType as? UILabel
            if let selectedIndex = selectedIndex{
                if selectedIndex == index{
                    label?.textColor = UIColor().greenDoxtroCgColor()
                    return
                }
            }
            label?.textColor = UIColor.black
            label?.text = relationImageArray[index]
            break
        default:
            break
        }
    }
}
/*
 import Foundation
 import UIKit
 import TSMessages
 let relationImageArray = ["Spouse","Parents","Child","Sibilings"]
 class DpAddMemberView: UIView {
 @IBOutlet weak var allRelationStackView: UIStackView!
 @IBOutlet weak var ageTextField: UITextField!
 @IBOutlet weak var userNameTextField: UITextField!
 var delegate : DPAddNewMemberDelegate?
 var sortedSegmentViews : [UIView]?
 var selectedgenderType : DPgenderType = .Male
 var selectedRelation : String = "Spouse"
 @IBOutlet weak var genderSegmentControl: UISegmentedControl!
 
 override func awakeFromNib() {
 super.awakeFromNib()
 setUpNewsTypeSegmentController()
 self.ageTextField.addBootomBorderLine(width: 1)
 self.userNameTextField.addBootomBorderLine(width: 1)
 self.sortedSegmentViews = genderSegmentControl.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
 for (index,relationView) in self.allRelationStackView.subviews.enumerated(){
 let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
 relationView.addGestureRecognizer(tap)
 relationView.tag = index
 self.setCircleAndTextColor(with: relationView, index: index)
 }
 changeToSelectedSegmentColor(selectedIndex: 0)
 setCircleAndTextColor(with: self.allRelationStackView.subviews.first!, index: 0, selectedIndex: 0)
 }
 func handleTap(sender: UITapGestureRecognizer) {
 if let selectedIndex = sender.view?.tag{
 self.selectedRelation = relationImageArray[selectedIndex]
 for (index,relationView) in self.allRelationStackView.subviews.enumerated(){
 self.setCircleAndTextColor(with: relationView,index : index,selectedIndex : selectedIndex)
 }
 }
 
 }
 func setCircleAndTextColor(with relationView : UIView,index : Int,selectedIndex : Int?=nil){
 for viewType in relationView.subviews{
 switch viewType {
 case is UIImageView:
 let imageView = viewType as? UIImageView
 if let selectedIndex = selectedIndex{
 if selectedIndex == index{
 imageView?.makeCircleandBorder(with: (UIColor().greenDoxtroCgColor().cgColor),width: 2.5)
 break
 }
 }
 imageView?.makeCircleandBorder(with: (UIColor().grayDoxtroCgColor()?.cgColor)!)
 break
 case is UILabel :
 let label = viewType as? UILabel
 if let selectedIndex = selectedIndex{
 if selectedIndex == index{
 label?.textColor = UIColor().greenDoxtroCgColor()
 return
 }
 }
 label?.textColor = UIColor.black
 label?.text = relationImageArray[index]
 break
 default:
 break
 }
 }
 }
 @IBAction func genderSelection(_ sender: Any) {
 if sender is UISegmentedControl{
 if let segmentControl = sender as? UISegmentedControl{
 changeToSelectedSegmentColor(selectedIndex: segmentControl.selectedSegmentIndex)
 }
 }
 
 }
 func changeToSelectedSegmentColor(selectedIndex : Int){
 switch selectedIndex {
 case 0:
 self.selectedgenderType = .Male
 if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
 view.backgroundColor = UIColor().greenDoxtroCgColor()
 view.layer.cornerRadius = 4
 }
 if let view : UIView = self.sortedSegmentViews?[1]{
 view.backgroundColor = UIColor.white
 }
 break
 case 1:
 self.selectedgenderType = .Female
 if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
 view.backgroundColor = UIColor().greenDoxtroCgColor()
 view.layer.cornerRadius = 4
 }
 if let view : UIView = self.sortedSegmentViews?[0]{
 view.backgroundColor = UIColor.white
 }
 break
 default:
 break
 }
 }
 
 func setUpNewsTypeSegmentController(){
 genderSegmentControl.layer.masksToBounds = true
 self.genderSegmentControl.tintColor = UIColor.clear
 genderSegmentControl.backgroundColor = UIColor.white
 genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .selected)
 genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .normal)
 //NSBackgroundColorAttributeName:UIColor.magenta
 }
 
 @IBAction func saveUserInfo(_ sender: Any) {
 
 if let user = DPWebServicesManager.sharedManager.user{
 if self.userNameTextField.text == ""{
 TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please Enter Member Name", type: .error)
 }else if self.ageTextField.text == ""{
 TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please enter member age", type: .error)
 }else {
 let params  : [String : AnyObject] = ["patientId":user.userId as AnyObject,
 "relation": self.selectedRelation as AnyObject,
 "name": self.userNameTextField.text as AnyObject,
 "age": ageTextField.text as AnyObject,
 "gender": self.selectedgenderType.rawValue as AnyObject]
 var values = [String : AnyObject]()
 values["Add a Member"] = ["Name" : self.userNameTextField.text as AnyObject, "Age" : self.ageTextField.text as AnyObject, "Gender" : self.selectedgenderType as AnyObject] as AnyObject
 DPAppEventLog.shared.logEventName("Consultation requested", values: values)
 self.delegate?.saveAddNewMemberInfo(params: params)
 }
 }
 }
 @IBAction func removeUserInfo(_ sender: Any) {
 self.delegate?.removeAddNewMemberView()
 }
 }

 */
