//
//  DPDoctorProfile.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class DoctorProfileCollectionViewCell
 : UICollectionViewCell  {
    @IBOutlet weak var doctorNameLAbel: UILabel!
    @IBOutlet weak var doctorprofileImage: UIImageView!
    @IBOutlet weak var specializationInfoLabel: UILabel!
    var doctorId : String = ""
    @IBOutlet weak var experienceLabel: UILabel!
    var delegate : DoctorProfileDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        doctorprofileImage.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2)
    }
    @IBAction func viewFullprofileDoctor(_ sender: Any) {
          DPAppEventLog.shared.logEventName("Proceeded to Pay", values: ["Proceeded to Pay": "" as AnyObject])
        self.delegate?.showDoctorProfile(wirh: doctorId)
    }
    @IBOutlet weak var viewFullProfile: UIButton!
}
