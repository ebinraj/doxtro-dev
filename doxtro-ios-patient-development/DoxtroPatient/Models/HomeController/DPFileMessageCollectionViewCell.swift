//
//  DPFileMessageCollectionViewCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 08/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol  FileMessageCollectionViewCellDelegate{
    func openPDf(With url : String,fileName : String)
}
class FileMessageCollectionViewCell : UICollectionViewCell{
    
    @IBOutlet weak var statusImageView: UIImageView!
    var PDfUrl : String = ""
    var fileNames : String = ""
    var delegate : FileMessageCollectionViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
                mainView.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
                mainView.layer.borderWidth = 0.5
                mainView.layer.cornerRadius = 8
                mainView.clipsToBounds = true
    }
    @IBAction func viewFile(_ sender: Any) {
        self.delegate?.openPDf(With: PDfUrl, fileName: fileNames)
    }
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var mainView: UIView!
}
