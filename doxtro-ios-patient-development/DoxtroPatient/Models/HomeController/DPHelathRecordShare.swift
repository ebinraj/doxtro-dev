//
//  DPHelathRecordShare.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 08/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class HealthRecordShareCell : UICollectionViewCell{
@IBOutlet weak var timeLabel: UILabel!
     @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        makeBorder(borderView: mainView)
    }
    @IBOutlet weak var statusImageView: UIImageView!
    func makeBorder(borderView : UIView){
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor().greenDoxtroCgColor().cgColor
    }
}
