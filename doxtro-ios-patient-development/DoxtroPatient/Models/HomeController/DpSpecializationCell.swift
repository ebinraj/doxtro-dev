//
//  DpSpecializationCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 12/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol DPSpecializationDelegate {
    func showFullListSpecialization()
    func selectedSpecialization(with index : Int)
}
class DpSpecializationCell: UITableViewCell{
    var viewArray : [UIView] = []
    var delegate : DPSpecializationDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewArray.removeAll()
        for views in firstFourtackView.subviews{
            viewArray.append(views)
        }
        for views in lastFourStackView.subviews{
            viewArray.append(views)
        }
    }
    @IBOutlet weak var firstFourtackView: UIStackView!
    @IBOutlet weak var lastFourStackView: UIStackView!
    @IBAction func showSpecializationFullList(_ sender: Any) {
        self.delegate?.showFullListSpecialization()
    }
    func configureCell(specializationListArray:[DPSpecializationList]){
        if specializationListArray.count > 0 {
            for (index,mainView) in viewArray.enumerated(){
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
                mainView.tag = index
                mainView.addGestureRecognizer(tap)
                let specializationObj = specializationListArray[index]
                for innerView in mainView.subviews{
                    switch innerView {
                    case is UIImageView:
                        let imageView = innerView as? UIImageView
                        if let categoryImageUrl = specializationObj.categoryImageUrl{
                            imageView?.af_setImage(withURL: categoryImageUrl, placeholderImage: UIImage(named: "AppIcon"), imageTransition: UIImageView.ImageTransition.crossDissolve(0.1))
                        }
                        break
                    case is UILabel:
                        let titleLabel = innerView as? UILabel
                        titleLabel?.text = specializationObj.specializationCategory
                        break
                    default:
                        break
                    }
                }
            }
        }
    }
    func handleTap(sender: UITapGestureRecognizer) {
        self.delegate?.selectedSpecialization(with: (sender.view?.tag)!)
    }
}
