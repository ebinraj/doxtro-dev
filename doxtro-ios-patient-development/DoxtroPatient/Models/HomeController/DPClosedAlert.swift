//
//  DPClosedAlert.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 02/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class ClosedAlert : UICollectionViewCell{
    @IBOutlet weak var sessionEndMessageLabel: UILabel!
}
