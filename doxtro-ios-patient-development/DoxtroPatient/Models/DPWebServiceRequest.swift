//
//  DPWebServiceRequest.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 24/07/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import Alamofire
let mobile_key  = "mobile"
let emailId_key     = "emailid"
struct DPWebServiceRequest{
    var baseURL : URL
    var endPoint: String?
    var requestType: Alamofire.HTTPMethod
    var parameterEncoding : ParameterEncoding
    var parameters: [String: AnyObject]?
    var headers : [String : String]?
    init() {
        self.baseURL = URL(string: kWebServiceBase)!
        self.requestType = .get
        self.parameterEncoding = URLEncoding.default
        self.parameters = [ : ]
        if let user = DPWebServicesManager.sharedManager.user{
            self.headers = ["Authorization": user.token ?? ""]
        }else{
            self.headers = [ : ]
        }
    }
}
