//
//  DPTransactionHistory.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 16/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class TransactionHistory{
    var id : String?
    var amount : Int?
    var method : String?
    var consultationId : String?
    var date : String?
    var doctorName : String?
    init(dict : JSON) {
        self.amount = dict["amount"].intValue
        self.id = dict["_id"].string ?? ""
        self.method = dict["method"].string ?? ""
        self.consultationId = dict["consultationId"].string ?? ""
        self.date = dict["date"].string ?? ""
        self.doctorName = dict["doctorName"].string ?? ""
    }
}
