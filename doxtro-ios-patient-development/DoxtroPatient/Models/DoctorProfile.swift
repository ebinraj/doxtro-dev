//
//  DPDoctorProfile.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 05/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
class DoctorProfile {
    var consultationCount : Int?
    var fee : Int?
    var profile : Profile?
    var feedBack : [FeedBack] = []
    init(dict : JSON) {
        if let dict = dict["data"].dictionary{
            self.consultationCount = dict["consultationCount"]?.intValue
            self.fee = dict["fee"]?.intValue
            self.profile = Profile(dict:JSON(dict["profile"]?.dictionary))
            if let feedbackArray = dict["feedback"]?.array{
                for value in feedbackArray{
                    feedBack.append(FeedBack(dict: JSON(value.dictionaryValue)))
                }
            }
        }
        
    }
}
class Profile {
    var id : String?
    var profilePic : URL?
    var experience : String?
    var specialization : [String] = []
    var qualification : [Qualification] = []
    var firstName : String?
    var workFor : [WorksFor] = []
    var bio : String?
    var category : [String] = []
    init(dict : JSON) {
        self.id = dict["_id"].string ?? ""
        self.profilePic = URL(string: (dict["profilePic"].string) ?? "")
        self.experience = dict["experience"].string ?? ""
        self.firstName = dict["firstName"].string ?? ""
        self.bio = dict["bio"].string ?? ""
        if let specializationArray  = dict["specialization"].array{
            for value in specializationArray{
                self.specialization.append(value.stringValue)
            }
        }
        if let qualificationArray = dict["qualification"].array{
            for value in qualificationArray{
                qualification.append(Qualification(dict: JSON(value.dictionaryValue)))
            }
        }
        if let workForArray = dict["WorksFor"].array{
            for value in workForArray{
                workFor.append(WorksFor(dict: JSON(value.dictionaryValue)))
            }
        }
        if let workForArray = dict["category"].array{
            for value in workForArray{
                category.append(value.stringValue)
            }
        }
    }
}
class Qualification{
    var type : String?
    var degree : String?
    var college : String?
    var yearTo : String?
    var  yearFrom : String?
    var additionalData : String = ""
    init(dict : JSON) {
        self.type = dict["type"].string ?? ""
        self.degree = dict["degree"].string ?? ""
        self.college = dict["college"].string ?? ""
        self.yearTo = dict["yearTo"].string ?? ""
        self.yearFrom = dict["yearFrom"].string ?? ""
        self.additionalData = dict["additionalData"].string ?? ""
    }
}
class WorksFor{
    var organization : String?
    var experience : String = ""
    var specialization : String?
    init(dict : JSON) {
        self.organization = dict["organization"].string ?? ""
        self.experience = dict["experience"].string ?? ""
        self.specialization = dict["specialization"].string ?? ""
    }
    
}
class FeedBack{
    var assignedAt : String?
    var userName : String?
    var text : String?
    var behaviour : String?
    var rating : Int?
    var profilePic : URL?
    init(dict : JSON) {
        if let ProfilePicString = dict["profilePic"].string {
            if let url = URL(string: ProfilePicString){
                self.profilePic = url
            }
        }
        self.assignedAt = dict["assignedAt"].string ?? ""
        self.userName = dict["userName"].string ?? ""
        self.text = dict["feedback"]["text"].string ?? ""
        self.behaviour = dict["feedback"]["behaviour"].string ?? ""
        self.rating = dict["feedback"]["rating"].int ?? 0
    }
}
