//
//  DPConsultationChat.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 28/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
//enum DoseTimeType : String{
//    case morning
//    case afternoon
//    case night
//    init(type : String){
//        switch type {
//        case "Morning":
//        self = .morning
//            break
//        case "Afternoon":
//            self = .afternoon
//            break
//        case "Night":
//            self = .night
//            break
//        default:
//            break
//        }
//    }
//}
class ConsultationChatMessage : NSObject, JSQMessageData {
    var id : String = ""
    var data : ConsultationData?
    var from : String = ""
    var msgString : String = ""
    var msgType : BotConsultationType?
    var status : String?
    var timeStamp : Date?
    var timeStampString : String = ""
    var senderDisplayName_: String = ""
    var senderId_: String = ""
    var date_: Date
    var isMediaMessage_ : Bool
    var messageHash_ : UInt
    var senderName : String = ""
    var documentName : String = ""
    var sortingTimeStamp : String = ""
    var text_ : String = ""
    var receiverDoctorId : String = ""
    var prescriptionRecord : PrescriptionRecord?
    var diagnosticRecord : DiagnosticRecord?
    var paymentData : PaymentData?
    var documentsData : DocumentsData?
    init(dict : JSON,isMediaMessage : Bool = false) {
        self.documentName = dict["documentName"].stringValue
        self.senderDisplayName_ = "Ayush"
        self.senderId_ = dict["_id"].stringValue
        self.date_ = Date()
        self.isMediaMessage_ = isMediaMessage
        self.messageHash_ = UInt(3)
        self.text_ = dict["data"].stringValue
        if let timeStampInt = dict["timeStamp"].int{
            let date = Date(timeIntervalSince1970: Double(timeStampInt))
            let dateFormat = PacoDate.formatterForLocal()
            dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = dateFormat?.string(from: date){
                sortingTimeStamp = date
            }
        }
        if let timeStampInt = dict["timeStamp"].int{
            let date = Date(timeIntervalSince1970: Double(timeStampInt/1000))
            let dateFormat = PacoDate.formatterForLocal()
            dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = dateFormat?.string(from: date){
            timeStampString = date
            }
        }
        self.senderName = dict["sender"].stringValue
        self.id = dict["_id"].stringValue
        self.from = dict["from"].stringValue
        self.msgString = dict["msgType"].stringValue
        self.msgType =  BotConsultationType(string : msgString)
        if self.msgType == .profile{
            self.receiverDoctorId = dict["_id"].stringValue
        }
        if self.msgType == .payment{
            self.paymentData = PaymentData(dict: JSON(dict["data"].dictionary))
        }
        self.status = dict["status"].stringValue
        if let timeString = dict["timeStamp"].string{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.timeZone = NSTimeZone.local//Your date format
            //dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
            if let date = dateFormatter.date(from: timeString){//according to date format your date
                self.date_ = date
            }
        }
        if self.msgType == .prescription {
            self.prescriptionRecord = PrescriptionRecord(dict: JSON(dict["data"].dictionaryValue))
        } else if self.msgType == .diagnostic {
            self.diagnosticRecord = DiagnosticRecord(dict: JSON(dict["data"].dictionaryValue))
        } else {
            self.data = ConsultationData(dict : dict["data"])
        }
        if msgType == .documents{
            self.documentsData = DocumentsData(dict: JSON(dict["data"].dictionaryValue))
        }
    }
    func isMediaMessage() -> Bool {
        return isMediaMessage_
    }
    
    func date() -> Date! {
        return date_
    }
    
    func senderId() -> String! {
        return senderId_
    }
    
    func senderDisplayName() -> String! {
        return senderDisplayName_
    }
    func messageHash() -> UInt{
        return messageHash_
    }
    func text() -> String! {
        return text_
    }
}
class DocumentsData {
    var documentName : String = ""
    var documentUrl : String = ""
    init(dict : JSON) {
        self.documentName = dict["documentName"].stringValue ?? ""
        self.documentUrl = dict["documentUrl"].stringValue ?? ""
    }
}
class ConsultationData{
    var message : String = ""
    var textData : TextData?
    var doctorList : [TextData] = []
    var text : String = ""
    init(dict : JSON) {
        self.message = dict["message"].stringValue
        if let text = dict.string{
            self.text = text
        }else if let dictionary = dict.dictionary{
            self.textData = TextData(dict: JSON(dictionary))
        }else{
            let array = JSON(dict.arrayValue)
            doctorList.removeAll()
            for (_,value) in array.enumerated(){
                doctorList.append(TextData(dict: JSON(value.1.dictionaryValue)))
            }
        }
    }
    
}
class TextData {
    var Rid : String?
    var availabilityId : String = ""
    var bio : String = ""
    var category : [String] = []
    var experience : String = ""
    var firstName : String = ""
    var language : String = ""
    var specialization : [String] = []
    var totalConsultations : String = ""
    var note : String = ""
    var relation : String = ""
    var relativesAge : String = ""
    var relativesGender : String = ""
    var relativesName : String = ""
    var gender : String = ""
    var profilePic : URL?
    var qualification : String = ""
    var qualifucationArray : [Qualification] = []
    init(dict : JSON){
        self.Rid = dict["_id"].stringValue
        self.availabilityId = dict["availabilityId"].stringValue
        self.bio = dict["bio"].stringValue
        if let categoryArray = dict["category"].array{
            for value in categoryArray{
                self.category.append(value.stringValue)
            }
        }
        self.experience = dict["experience"].string ?? "0"
        self.firstName = dict["firstName"].stringValue
        self.language = dict["language"].stringValue
        if let specializationArray  = dict["specialization"].array{
            for value in specializationArray{
                self.specialization.append(value.stringValue)
            }
        }
        self.totalConsultations = dict["totalConsultations"].stringValue
        self.note = dict["note"].stringValue
        self.relation = dict["relation"].stringValue
        self.relativesAge = dict["relativesAge"].stringValue
        self.relativesGender = dict["relativesGender"].stringValue
        self.relativesName = dict["relativesName"].stringValue
        self.gender = dict["gender"].stringValue
        
        self.profilePic = URL(string: (dict["profilePic"].string) ?? "")
        self.qualification = dict["qualification"].stringValue
        if let qualificationArray = dict["qualification"].array{
            for value in qualificationArray{
                qualifucationArray.append(Qualification(dict: JSON(value.dictionaryValue)))
            }
        }
    }
}
class PrescriptionRecord{
    var doctorProfile : PrescriptionDoctorProfile?
    var medicationInfo : [PrescriptionMedicationInfo] = []
    var relativeInfo : DPRelativeMember?
    var prescriptionId : String = ""
    var timeForMedicine : String = ""
    init(dict : JSON) {
        prescriptionId = dict["prescriptionId"].stringValue
        relativeInfo = DPRelativeMember(dict: JSON(dict["additionalDetails"].dictionaryValue))
        doctorProfile = PrescriptionDoctorProfile(dict:JSON(dict["doctorProfile"].dictionaryValue))
        if let medicationArray = dict["medication"].array{
            for value in medicationArray{
               medicationInfo.append(PrescriptionMedicationInfo(dict: JSON(value.dictionary)))
            }
        }
        
    }
}
class PrescriptionDoctorProfile{
    var duns : String = ""
    var firstName : String = ""
    var qualificationString : String = ""
     var category : [String] = []
    init(dict : JSON) {
        duns = dict["duns"].stringValue
        firstName = dict["firstName"].stringValue
        if let qualificationArray  = dict["qualification"].array{
            for (index,value) in qualificationArray.enumerated(){
               qualificationString = qualificationString + value.stringValue
                if qualificationArray.count - 1 > index {
                    qualificationString = qualificationString + ","
                }
            }
        }
        if let workForArray = dict["category"].array{
            for value in workForArray{
                category.append(value.stringValue)
            }
        }
    }
}
class PrescriptionMedicationInfo{
    var doseTimeString : String = ""
    var duration : Int?
    var foodWarning : String = ""
    var instruction : String = ""
    var name : String = ""
    var durationUnit : String?
    var doseTimeArrays : [JSON] = []
    var doseTimeRfinedArray : [String] = ["","",""]
    init(dict : JSON) {
        if let doseTimeArray = dict["doseTime"].array{
             self.doseTimeArrays = doseTimeArray
             self.durationUnit = dict["durationUnit"].stringValue
             duration = dict["duration"].intValue
             foodWarning = dict["foodWarning"].stringValue
             instruction = dict["instruction"].stringValue
             name = dict["name"].stringValue
            for (value) in doseTimeArray{
                switch value.stringValue{
                case "Morning","morning" :
                    doseTimeRfinedArray[0] = "Morning"
                    break
                case "Night","night" :
                    doseTimeRfinedArray[2] = "Night"
                    break
                case "Afternoon","afternoon":
                    doseTimeRfinedArray[1] = "Afternoon"
                    break
                default :
                    break
                }
            }
            for (index,value) in doseTimeRfinedArray.enumerated(){
                if value == ""{
                    doseTimeString = doseTimeString + ""
                }else{
                    doseTimeString = doseTimeString + value
                        if index == 1{
                            if doseTimeRfinedArray[doseTimeRfinedArray.count-1] != ""{
                                doseTimeString = doseTimeString + "-"
                            }
                        }else if index == 0{
                                if doseTimeRfinedArray[doseTimeRfinedArray.count-2] != ""{
                                    doseTimeString = doseTimeString + "-"
                                }else{
                                    if doseTimeRfinedArray[doseTimeRfinedArray.count-1] != ""{
                                        doseTimeString = doseTimeString + "-"
                                    }
                            }
                        }else{
                         if doseTimeRfinedArray.count - 1  > index{
                          doseTimeString = doseTimeString + "-"
                         }
                    }
            }
        }
    }
}
}

class DiagnosticRecord{
    var doctorProfile : PrescriptionDoctorProfile?
    var instructionInfo : [DiagnosticInstructionInfo] = []
    var relativeInfo : DPRelativeMember?
    var diagnosticId : String = ""
    var timeForMedicine : String = ""
    init(dict : JSON) {
        diagnosticId = dict["diagnosticId"].stringValue
        relativeInfo = DPRelativeMember(dict: JSON(dict["additionalDetails"].dictionaryValue))
        doctorProfile = PrescriptionDoctorProfile(dict:JSON(dict["doctorProfile"].dictionaryValue))
        if let medicationArray = dict["diagnostic"].array{
            for value in medicationArray{
                instructionInfo.append(DiagnosticInstructionInfo(dict: JSON(value.dictionary)))
            }
        }
    }
}

class DiagnosticInstructionInfo{
    var instruction : String = ""
    var name : String = ""
    init(dict : JSON) {
        instruction = dict["instruction"].stringValue
        name = dict["name"].stringValue
    }
}
class PaymentData {
    var consultingFee : Int = 0
    var couponDiscount : Int = 0
    var couponUsed : String?
    var discountValue: String?
    var isFree : Bool = false
    var patientFee : Int = 0
    var preAppliedCouponId : String?
    var isPreApplied : Bool?
    var doxtroCash : Int = 0
    var usedDoxtroCash :Int = 0
    var remainingDoxtroCash : Int = 0
    var amount : Int = 0
    var price : Int = 0
    var feeExcludingDoxtroCash : Int = 0
    init(dict : JSON) {
         self.doxtroCash = dict["doxtroCash"].int ?? 0
         self.usedDoxtroCash = dict["usedDoxtroCash"].int ?? 0
         isPreApplied = dict["isPreApplied"].boolValue
         consultingFee = dict["consultingFee"].int ?? 0
         couponDiscount = dict["couponDiscount"].int ?? 0
         couponUsed = dict["couponUsed"].string ?? ""
         discountValue = dict["discountValue"].string ?? ""
         isFree = dict["isFree"].boolValue
         patientFee = dict["totalPayable"].int ?? 0
         self.remainingDoxtroCash = dict["remainingDoxtroCash"].int ?? 0
         preAppliedCouponId = dict["preAppliedCouponId"].string ?? ""
        price = dict["price"].int ?? 0
        amount = dict["amount"].int ?? 0
        feeExcludingDoxtroCash = dict["feeExcludingDoxtroCash"].int ?? 0
    }
}
