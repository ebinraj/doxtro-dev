//
//  DPReferelViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 06/03/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import FirebaseInvites
import FirebaseDynamicLinks
import TSMessages
let REFERAL_MESSAGE_KEY = "REFERALMESSAGEKEY"
let REFERAL_INVITE_KEY = "REFERALINVITEKEY"
class ReferelViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        getRefrelValue()
        self.referalDashView.addDashedBOrderLine(color:UIColor(red: 219, green: 219, blue: 219).cgColor)
    }
    var referalCode : String?
    var referalMessage : String = ""
    
    @IBAction func dismissController(_ sender: Any) {
        self.dismissController()
    }
    func openActionShareSheet(url : String){
        let shareVC:JYSystemShare = JYSystemShare()
        if let code = self.referalCode{
        if let shareActivityVC:UIActivityViewController = shareVC.shareActionViewControllerWithItems(
            nil,
            shareContent: (self.referalMessage + " Use referral code - " + code),
            shareURL: url,
            shareUUID: nil) {
            //(nil, shareText: shareObject.shareText, shareURLString: shareObject.shareURL, shareUUID: nil) {
            self.present(shareActivityVC, animated: true, completion: { () -> Void in
                // debugPrint("share completed..")
            })
        }
    }
    }
    
    @IBOutlet weak var referalDashView: UIView!
    @IBOutlet weak var referelCodeLabel: UILabel!
     @IBOutlet weak var referAndEarnDescriptionLabel: UILabel!
    @IBAction func invitesFriendsTapped(_ sender: Any) {
        guard let rechable = DPWebServicesManager.sharedManager.reachability?.isReachable,rechable else{
            TSMessage.showNotification(in: self, title: "Network Error", subtitle:  "Your internet connection disappeared!", type: .error)
            return
        }
//
//        if let invite = Invites.inviteDialog() {
//            invite.setInviteDelegate(self)
//            let targetApplication = InvitesTargetApplication.init()
//            targetApplication.androidClientID = "1:107438437489:android:c0bba85280604d88"
//            invite.setOtherPlatformsTargetApplication(targetApplication)
//            // NOTE: You must have the App Store ID set in your developer console project
//            // in order for invitations to successfully be sent.
//
//            // A message hint for the dialog. Note this manifests differently depending on the
//            // received invitation type. For example, in an email invite this appears as the subject.
//            invite.setMessage(self.referAndEarnDescriptionLabel.text!)
//            // Title for the dialog, this is what the user sees before sending the invites.
//            invite.setTitle("Refer & Earn")
//            invite.setDeepLink("https://b7e39.app.goo.gl/ArticlesPatient")
//            invite.setCallToActionText("Install!")
//            invite.setCustomImage("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png")
//            invite.open()
//        }
        //guard let uid = Auth.auth().currentUser?.uid else { return }
        if let referralCode = DPWebServicesManager.sharedManager.user?.referralCode{
        let link = URL(string: "https://doxtro.com/?invitedby=\(referralCode)")
        let referralLink = DynamicLinkComponents(link: link!, domain: "ut6d4.app.goo.gl")
        referralLink.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.doxtro.app")
        //referralLink.iOSParameters?.minimumAppVersion = "1.0.1"
        referralLink.iOSParameters?.appStoreID = "1161686991"
        referralLink.iOSParameters?.customScheme = "com.doxtro.app"
        referralLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.doxtro.app")
        referralLink.androidParameters?.minimumVersion = 19
        ABProgressIndicator.shared.showAnimator()
        referralLink.shorten { (shortURL, warnings, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if let url = shortURL{
                self.openReferalCodeUrlWithshare(url: url.absoluteString)
            }else{
             UIAlertController.showAlertWithMessage("Failed to Refer")
            }
            }
        }
    }
    func openReferalCodeUrlWithshare(url : String){
        self.openActionShareSheet(url: url)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let rechability = DPWebServicesManager.sharedManager.reachability?.isReachable,rechability{
        if let referelCode = self.referalCode{
         self.referelCodeLabel.text = referelCode
        }
        }else{
           NoInterNetSetUP()
        }
    }
    func NoInterNetSetUP(){
        if let referelCode = DPWebServicesManager.sharedManager.user?.referralCode{
            self.referelCodeLabel.text = referelCode
        }
        if let referalMessage = self.getFromNsuserDefault(key: REFERAL_MESSAGE_KEY){
              self.referAndEarnDescriptionLabel.text = referalMessage
        }
        if let referalInviteMessage = self.getFromNsuserDefault(key: REFERAL_INVITE_KEY){
            self.referalMessage = referalInviteMessage
        }
    }
    func getRefrelValue(){
     if   let userID = DPWebServicesManager.sharedManager.user?.userId{
        let params = ["_id":userID]
        let request = GetReferlCodeRequest(params: params as [String:AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let response = response{
                if let data = response["data"].dictionary{
                    if let description = data["description"]?.string{
                     self.referAndEarnDescriptionLabel.text = description
                        self.saveINNsuserDefault(key: REFERAL_MESSAGE_KEY, value: description)
                    }
                    if let referalCode = data["referralCode"]?.string{
                        self.referalCode = referalCode
                        self.referelCodeLabel.text = referalCode
                    }
                    if let message = data["message"]?.string{
                        self.referalMessage = message
                        self.saveINNsuserDefault(key: REFERAL_INVITE_KEY, value: message)
                    }
                   
                }
            }
        }
    }
 }
    func saveINNsuserDefault (key : String, value : String)  {
        UserDefaults.standard.setValue(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    func getFromNsuserDefault (key : String) -> String?  {
        return UserDefaults.standard.value(forKey: key) as? String
    }
}
extension ReferelViewController : InviteDelegate{
    func inviteFinished(withInvitations invitationIds: [String], error: Error?) {
        
    }
}
