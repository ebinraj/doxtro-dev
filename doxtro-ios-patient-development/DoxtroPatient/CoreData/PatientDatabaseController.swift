 //
//  PatientDatabaseController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 09/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import CoreData
 let SECIALIZATION_ENTITY = String(describing: Specialization.self)
 let CONSULTATION_LIST_ENTITY = String(describing: ConsultationList.self)
 let BANNER_IMAGE_ENTITY = String(describing: BannerImage.self)
 class DatabaseController{
    // MARK: - Core Data stack
    private init(){
         
    }
    class func getContext() -> NSManagedObjectContext{
        return persistentContainer.viewContext
    }
    @available(iOS 10.0, *)
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "DoxtroPatient")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    @available(iOS 10.0, *)
    class func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
   class func fetchSpecializationListFromCoreData(predicate : NSPredicate? = nil) -> [Specialization]?{
        let specializationFetchRequest : NSFetchRequest<Specialization> = Specialization.fetchRequest()
        if let predicate = predicate{
            specializationFetchRequest.predicate = predicate
        }
        do{
            return try DatabaseController.getContext().fetch(specializationFetchRequest)
        }catch{
            return nil
        }
    }
     //MARK : Consultation List
    class func insertConsultationListInCoreData(obj : [DpConsultationDetails]){
        if let consultationList = NSEntityDescription.insertNewObject(forEntityName: CONSULTATION_LIST_ENTITY, into: DatabaseController.getContext()) as? ConsultationList {
            consultationList.consultationArray = NSKeyedArchiver.archivedData(withRootObject: obj) as NSData
            DatabaseController.saveContext()
        }
    }
    class func fetchConsulatationList()->([DpConsultationDetails]?,[ConsultationList]?){
        let consultationFetchRequest : NSFetchRequest<ConsultationList> = ConsultationList.fetchRequest()
        do{
            let list =  try DatabaseController.getContext().fetch(consultationFetchRequest)
            if let first = list.first{
                return (NSKeyedUnarchiver.unarchiveObject(with: first.consultationArray as! Data ) as? [DpConsultationDetails],list)
            }else{
                return (nil,nil)
            }
        }catch{
            return (nil,nil)
        }
    }
    class func deleteConsultationCoredataList(){
        if let specializationList = fetchConsulatationList().1{
            for specialiaztioObj in specializationList{
                DatabaseController.getContext().delete(specialiaztioObj)
            }
        }
    }
    //MARK :Banner Image
    class func insertBannerImageInCoreData(obj : NSData){
        if let bannerList = NSEntityDescription.insertNewObject(forEntityName:BANNER_IMAGE_ENTITY, into: DatabaseController.getContext()) as? BannerImage {
            bannerList.bannerImage = obj
            DatabaseController.saveContext()
        }
    }
    class func fetchBannerImage()->([BannerImage]?){
        let bannerFetchRequest : NSFetchRequest<BannerImage> = BannerImage.fetchRequest()
        do{
           return try DatabaseController.getContext().fetch(bannerFetchRequest)
        }catch{
            return nil
        }
    }
    class func deleteBanerFromCoredataList(){
        if let bannerImageList = fetchBannerImage(){
            for bannerObj in bannerImageList{
                DatabaseController.getContext().delete(bannerObj)
            }
        }
    }
//   class  func fetchSpecializationListFromCoreData() -> [Any]?{
//        let specializationFetchRequest : NSFetchRequest<Specialization> = Specialization.fetchRequest()
//        do{
//            return try DatabaseController.getContext().fetch(specializationFetchRequest)
//        }catch{
//            return nil
//        }
//    }
 }
