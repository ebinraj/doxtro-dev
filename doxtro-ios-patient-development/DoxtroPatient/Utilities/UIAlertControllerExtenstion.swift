//
//  UIAlertControllerExtenstion.swift
//  Pinksamaritan
//
//  Created by Ayush Yadav on 04/04/17.
//  Copyright © 2017 Ayush Yadav. All rights reserved.
//
import UIKit
/**
 Add more flexibility to show alert easily.
 */
extension UIAlertController {
    
    typealias ActionHandler = (UIAlertController, UIAlertAction) -> Void
    
    //MARK: Alert show messages
    
    /**
     Present alert controller with message and "Ok" button to dismiss alert.
     
     - Parameter message: Message to display in alert
     */
    class func showAlertWithMessage(_ message: String) {
        showAlertWithMessage(message, title: nil)
    }
    
    /**
     Present alert controller with message, title and "Ok" button to dismiss alert.
     
     - Parameters:
     - message: Message to display in alert.
     - title  : Title for alert, it is optional.
     */
    class func showAlertWithMessage(_ message: String, title: String?) {
        showAlertWithMessage(message, title: title, cancelButtonTitle: "OK",handler: nil)
    }
    
    /**
     Present alert controller with message, title and one action button.
     
     - Parameters:
     - message           : Message to display in alert.
     - title             : Title for alert, it is optional.
     - cancelButtonTitle : Action button title, cannot be nil.
     - handler           : Handler for action button, passing nil provide default action. i.e dismiss alert.
     */
    class func showAlertWithMessage(_ message: String, title: String?, cancelButtonTitle: String, handler: ActionHandler?) {
        let alertController = alertControllerWithMessage(message, title: title)
        let _ = alertController.addActionWithTitle(cancelButtonTitle, style: .default, handler: handler)
        alertController.show()
    }
    
    /**
     Present alert controller with one action button and cancel button.
     */
    class func showAlertWithMessage(_ message: String, title: String?, actionButtonTitle: String, cancelButtonTitle: String, actionButtonHandler: ActionHandler?, cancelButtonHandler: ActionHandler?) {
        let alertController = alertControllerWithMessage(message, title: title)
        let _ = alertController.addActionWithTitle(actionButtonTitle, style: .default, handler: actionButtonHandler)
        let _ = alertController.addActionWithTitle(cancelButtonTitle, style: .default, handler: cancelButtonHandler)
        alertController.show()
    }
    
    /**
     Present alert controller with one action button and destructive button.
     */
    class func showAlertWithMessage(_ message: String, title: String?, destructiveButtonTitle: String, actionButtonTitle: String, destructiveButtonHandler: ActionHandler?, actionButtonHandler: ActionHandler?) {
        let alertController = alertControllerWithMessage(message, title: title)
        let _ = alertController.addActionWithTitle(destructiveButtonTitle, style: .destructive, handler: destructiveButtonHandler)
        let _ = alertController.addActionWithTitle(actionButtonTitle, style: .default, handler: actionButtonHandler)
        alertController.show()
    }
    class func showAlertWithMessageForEditProfile(_ message: String, title: String?, destructiveButtonTitle: String, actionButtonTitle: String, destructiveButtonHandler: ActionHandler?, actionButtonHandler: ActionHandler?)-> UIAlertController{
        let alertController = alertControllerWithMessage(message, title: title)
        let _ = alertController.addActionWithTitle(destructiveButtonTitle, style: .destructive, handler: destructiveButtonHandler)
        let _ = alertController.addActionWithTitle(actionButtonTitle, style: .default, handler: actionButtonHandler)
        return alertController
    }
    
    //MARK: Creates Alert controller
    /**
     Creates and return alert controller with message.
     
     - Returns: created alert controller.
     */
    class func alertControllerWithMessage(_ message: String) -> UIAlertController {
        return alertControllerWithMessage(message, title: nil)
    }
    
    /**
     Creates and return alert controller with message and title.
     
     - Returns: created alert controller.
     */
    class func alertControllerWithMessage(_ message: String, title: String?) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: .alert)
    }
    
    //MARK: Add actions methods
    /**
     Add button to alert view controller.
     
     - Returns: Created alert action.
     */
    func addActionWithTitle(_ title: String, style: UIAlertActionStyle, handler: ActionHandler? ) -> UIAlertAction {
        
        //create new handler, which support by built in method.
        var actionHandler: ((UIAlertAction) -> Void)? = nil
        if let handler = handler {
            actionHandler = { [weak self] (sender: UIAlertAction) -> Void in
                if let weakSelf = self {
                    handler(weakSelf, sender)
                }
            }
        }
        
        let action: UIAlertAction = UIAlertAction(title: title, style: style, handler: actionHandler)
        self.addAction(action)
        return action
    }
    
    //MARK: Show alert
    
    /**
     Present alert view controller, if controller view is not in view hierarchy.
     */
    func show() {
        
        //Present only if it is not in hierarchy
        if self.view.window == nil {
            let presenter = getRootViewController()
            presenter?.present(self, animated: true, completion: nil)
        }
    }
}

private extension UIAlertController {
    
    /**
     Returns root view controller of application
     */
    func getRootViewController() -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
}
