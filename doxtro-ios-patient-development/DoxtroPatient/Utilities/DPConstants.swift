//
//  DPConstants.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/22/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
let staging = "Consultations_staging"
let dev = "Consultations"
let FIREBASE_CONSULTATION_DBKEY = dev
let screenWidth = UIScreen.main.bounds.width
let screenHeight   = UIScreen.main.bounds.height
let ACCEPTABLE_CHARACTERS  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_., '?/"
let ACCEPTABLE_NUMBERS  = "0123456789 "


let OTP_Segue  = "otpSegue"


//Color code
let lightGray_hex = "f2f2f2"
let darkGray_hex = "d4d5d7"
let greenColor = ColorMethod.hexStringToUIColor(hex: "27A39C")
var TWO_C_DEV = "http://doxtro2c.above-inc.com"
var TWO2_C_DEV = "http://2c.above-inc.net"
var LIVE_BASE_URL = "https://apps.doxtro.com"
var STAGING_URL  = "https://doxtrostaging.above-inc.net"
var DEV_BASE_URL = "http://doxtrodev.above-inc.net" //Pre production //"http://app.doxtro.com"
var QA_URL = "http://doxtroqa.above-inc.net"
var kWebServiceBase =  LIVE_BASE_URL + "/api/v1"
//var imagePath = BASE_URL + "/uploads/"
//var chatServiceBase = BASE_URL + ":1337"

//MARK: - ------------Api constants-------------

// phase 1 Api's.....these constants will be removed Later.
let kUserCreate                            =   "usermanagement/create"
let kDoctorUpdate                           =   "doctor/updateDoctor"
let kUserConfirmation                       =   "user/accountConfirmation"
let kConsultationList                       =   "doctor/consultationList"
let kDoctorDetails                          =   "doctor/doctorDetails"
let kBankDetailsCreate                      =   "BankDetails/create"
let kCreateResponseNotification             =   "doctor/createResponseNotification"
let kImageUpload                            =   "documentupload/imageUpload"
let kDocUpload                              =   "documentupload/create"
let kPrescriptionUpload                     =   "prescription/create"
let kPrescriptionDelete                     =   "prescription/delete"
let kGetPrescriptionDetails                 =   "prescription/find"
let kGetAvailabilityDetails                 =   "CalendarEvent/find"
let kCreateAvailability                     =   "CalendarEvent/createAvailability"
let INTERNET_ERRO_MSG                       =   "The Internet connection appears to be offline."
let GROUP_ID                                =   "groupName"
let kCreateFollowUp                         =   "patientmeta/createFollowUp"
let kCloseConsultation                      =   "patientmeta/closeConsultation"
let kDocEarnings                            =   "Doctor/earnings"
let kNotificationFind                       =   "notification/find"
let kFindBankDetails                        =   "BankDetails/findOne"
let kUpdateDeviceToken                      =   "usermanagement/updateDeviceToken"
let kFindMedia                              =   "Doctor/findMedia"
let kUpdateStatus                           =   "doctor/updateStatus"

//New Apis phase 2
let kSignUp                                 =   "patient/signUp"
let kSignIn                                 =   "patient/signIn"
let kVerification                           =   "patient/verify"
let KProfileUpdate                          =   "patient/profile"
let KGetSpecializationList                  =   "specialization/getCategories"
let KGetRelativeList                        =   "relatives/getRelatives"
let KCreateMembers                          =   "relatives/create"
let KGetPreferredLanguageList               =   "settings/getLanguages"
let kGetAllSpecializations                  =   "specialization/getAllSpecializations"
let KIntiateConsultation                    = "consultation/start"
let kOneTimePackage                  = "subscription/subscribe"
let kSubscriptions                       = "subscription/subscribeToRazorPay"
let kCancelSubscriptions                       = "subscription/cancelSubscription"
var userDefaults                            =   UserDefaults.standard


//MARK: - Api parameters

let appTag_key  = "tag"
let typeOfPhone_key = "typeOfPhone"
let deviceId_key    = "deviceId"
let givenName_key   = "givenName"
let lastName_key    = "lastName"
let otp_key         = "otp"
let ID_key         = "_id"
let token_key       = "token"
let authorization_key       = "Authorization"
let loggedIn_key    = "isLoggedIn"
//MARK: -

//MARK : - Error messages
let nameErrorMessage = "Enter name"
let emailIdErrorMessage = "Enter emailId"
let mobileNumErrorMessage = "Enter "
// MARK: - Configuration

let CONFIGURATION_PLIST = "Configration"

//MARK: - Segue Names
let OTP_SEGUE   = "otpSegue"
//MARK: - Color code
let lightGray = "d2d3d5"
let gray1     = "949599"
let grayColor = ColorMethod.hexStringToUIColor(hex: "949599")
let yellowColor = ColorMethod.hexStringToUIColor(hex: "FFC608")

// Days

let MONDAY = "Monday"
let TUESDAY = "Tuesday"
let WEDNESDAY = "Wednesday"
let THURSDAY = "Thursday"
let FRIDAY = "Friday"
let SATURDAY = "Saturday"
let SUNDAY = "Sunday"

// Availability table
let AVAILABILITY_TABLE = "DXAvailability"
let AVAILABILITY_STARTTIME = "startTime"
let AVAILABILITY_ENDTIME = "endTime"
let AVAILABILITY_DAY = "dayOfWeek"
let AVAILABILITY_ID = "id"
let AAVAILABLILITY_STATUS = "status"
let AVAILABLILITY_USERID = "userId"
let AVAILABLILITY_VALIDFROM = "validFrom"
let AVAILABLILITY_COUNT = "count"
let AVAILABLILITY_OPERATIONALHOURS = "operationalHours"
let AVAILABLILITY_DATA = "data"
let AVAILABILITY_MESSAGE = "message"
let WEBSERVICE_CODE = "code"
let WEBSERVICE_SUCCESS_CODE = 2008
let WEBSERVICE_SUCCESSCODECREATEAVAILABILITY = 200
let AVAILABILITY_DATANOTFOUNDCODE = 1302
let DATA_KEY = "data"
let MESSAGE_KEY = "message"
let accountActivationTime_key = "accountActivationTime"
let accountStatus_key   = "accountStatus"
let tag_key = "tag"
let status_key  = "status"
let userModel_key   = "UserModel"
let token = "token"
let POST = "POST"

//Clevertap & Appsflyer Event names
let EVENT_CONSULTATION_REQUESTED = "Consultation requested"
let EVENT_COMPLETED_SIGNUP    = "Completed Signup"
let EVENT_SPECIALIZATION_SELECTED =  "Specialization selected"
let EVENT_OTP_VERIFIED = "OTP Verified"
let EVENT_COMPLETED_PERSONAL_DETAILS = "Completed Personal Details"
let EVENT_PROCEED_TO_PAY = "Proceeded to Pay"
let EVENT_CHAT_VISITORS = "Chat visitors"
let EVENT_PROCEED_TO_RAZORPAY = "Proceeded to Razorpay"
let EVENT_PAID_FEES = "Paid Fees"
let EVENT_SETTINGS = "Settings"
let EVENT_HEALTH_RECORD = "Health Record"
let EVENT_RECENT_CONSULTATIONS = "Recent Consultations"
let EVENT_BANNER_VISITORS = "Banner Visitors"




