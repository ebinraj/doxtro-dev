//
//  ProgressIndicator.swift
//  Pinksamaritan
//
//  Created by Ayush Yadav on 22/04/17.
//  Copyright © 2017 Ayush Yadav. All rights reserved.
//

import Foundation
import UIKit
class ABProgressIndicator {
    
    static let shared = ABProgressIndicator()
    
    lazy var containerView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black
        view.alpha = 0.3
        view.isUserInteractionEnabled = true
        return view
    }()
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    func showAnimator() {
        
        //if container view is not in view hierarchy, then add it to window.
        if containerView.window == nil {
            UIApplication.shared.keyWindow?.addSubview(containerView)
            UIApplication.shared.keyWindow?.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
            UIApplication.shared.keyWindow?.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
            UIApplication.shared.keyWindow?.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
            UIApplication.shared.keyWindow?.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
            
            if activityIndicator.superview == nil {
                self.containerView.addSubview(activityIndicator)
                activityIndicator.centerXAnchor.constraint(equalTo: self.containerView.centerXAnchor).isActive = true
                activityIndicator.centerYAnchor.constraint(equalTo: self.containerView.centerYAnchor).isActive = true
            }
            
        }
        
        //start activity indicator
        activityIndicator.startAnimating()
        
    }
    
    func hideAnimator() {
        
        activityIndicator.stopAnimating()
        
        //remove container view from view hierarchy
        containerView.removeFromSuperview()
    }
}
