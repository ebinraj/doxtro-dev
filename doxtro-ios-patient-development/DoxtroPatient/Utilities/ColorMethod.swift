//
//  ColorMethod.swift
//  MasskTax
//
//  Created by Vinay Kumar Podili on 25/02/16.
//  Copyright © 2016 Vinay Podili. All rights reserved.
//

import UIKit

class ColorMethod: UIColor {
   class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
            
        if (cString.hasPrefix("#")) {
            //cString = cString.substringFromIndex(cString.startIndex)
            cString = (cString as NSString).substring(from: 1)
            //cString = cString.substringFromIndex(advance(cString.startIndex, 1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
