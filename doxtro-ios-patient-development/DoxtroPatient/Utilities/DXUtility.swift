//
//  DXUtility.swift
//  Doxtro
//
//  Created by Satish Kumar A on 07/07/16.
//  Copyright © 2016 Above Solutions. All rights reserved.
//

import UIKit
import SwiftyJSON
import Photos
import TSMessages
class DXUtility: NSObject {
    var shadowColor: UIColor? = UIColor.gray
    var shadowOffsetWidth: Int = 0
    var shadowOffsetHeight: Int = 2
    var shadowOpacity: Float = 0.75
    var activityIndicatorView:UIActivityIndicatorView!
    static let shared: DXUtility = {
        let instance = DXUtility()
        return instance
    }()
    static func getStringDefaults(key:String) -> String {
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: key)
        return value == nil ? "" : value!
    }
    
    class func returnChatRequiredTime(timeString : String)->String{
        let messageDate = DXUtility.convertADate(dateStr: timeString)//DXUtility."dd MMM,yyyy"(dateStr: timeString)
        if let attributedString = JSQMessagesTimestampFormatter.shared().relativeDate(for: messageDate as Date?){
            return attributedString
        }
        return ""
    }
    private  func showAlertWithTitle(title: String?, alertMessage message: String?, dismissButtonsTitle dismissTitle: String?, inController controller: UIViewController?, andActions actions: [UIAlertAction]?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let alertActions = actions {
            for action in alertActions {
                alertController.addAction(action)
            }
        }else {
            let okayAction = UIAlertAction(title: dismissTitle, style: .default, handler: nil)
            alertController.addAction(okayAction)
        }
        
        if let cntrl = controller {
            cntrl.present(alertController, animated: true, completion: nil)
        } else {
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    private func showAlertWithTitle(title: String?, alertMessage message: String?, dismissButtonsTitle dismissTitle: String?, cancelButtonTitle:String?, inController controller: UIViewController?, andActions actions: [UIAlertAction]?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let alertActions = actions {
            for action in alertActions {
                alertController.addAction(action)
            }
        }else {
            let okayAction = UIAlertAction(title: dismissTitle, style: .default, handler: nil)
            alertController.addAction(okayAction)
        }
        
        if let cntrl = controller {
            cntrl.present(alertController, animated: true, completion: nil)
        }else {
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        if let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx) as NSPredicate? {
            return emailTest.evaluate(with: testStr)
        }
        return false
    }
    
    class func convertDate(dateStr:String)->NSDate? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from: dateStr)
        return date as! NSDate
    }
    
    class  func getOnlyDateFromDateUTC(aDate: NSDate)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "dd MMM"
        let theDate = dateFormat?.string(from: aDate as Date)
        return theDate!
    }
    class func getOnlyTimeFromDate(aDate: NSDate)->String {
        let dateFormat = PacoDate.formatterForLocal()
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let theTime = dateFormat?.string(from: aDate as Date)
        return theTime!
    }
    class func convertDateAndTime(dateStr:String!)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr {
            if  let date  = dateFormat?.date(from: datSt) {
                dateFormat?.dateFormat = "dd MMM, h:mm a"
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }
    class func convertDateAndTimeforPresription(dateStr:String!)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr {
            if  let date  = dateFormat?.date(from: datSt) {
                dateFormat?.dateFormat = "dd MMM,YYYY h:mm a"
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }
    class func convertDateAndTimes(dateStr:String!)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr {
            if  let date  = dateFormat?.date(from: datSt) {
                dateFormat?.dateFormat = "h:mm"
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }
    class func getCurrentDate()->String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let date = dateFormatter.string(from: NSDate() as Date)
        return date
    }
    class func convertDateIntoRequiredFormat(dateStr:String!)->String {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = NSTimeZone.local
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let userCalendar = Calendar.current
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        let startTime = Date()
        let endTime = dateFormat.date(from: dateStr)//dateFormatDateFormatter?.date(from: "25/12/16 00:00:00")
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: endTime!, to:startTime )
        if let day = timeDifference.day{
            if day == 0{
                dateFormat.dateFormat = "HH:mm"
                return dateFormat.string(from: endTime!)
            }else if day >= 0 {
                dateFormat.dateFormat = "dd/MM/yy"
                return dateFormat.string(from: endTime!)
            }
        }
        return String(describing: timeDifference.day!)
    }
    class func convertADate(dateStr:String)->NSDate {
        // let dateFormatter = NSDateFormatter()
        let dateFormatter = PacoDate.formatterForLocal()
        dateFormatter?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let timeZone = NSTimeZone(name: "UTC") as TimeZone? {
            dateFormatter?.timeZone = timeZone
        }
        
        let date = dateFormatter?.date(from: dateStr)
        if let tempDate = date {
            return tempDate as NSDate
        }
        return NSDate()
    }
    class func convertToDMY(dateStr:String!)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr {
            if  let date  = dateFormat?.date(from: datSt) {
                dateFormat?.dateFormat = "dd/MM/YY"
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }

    class func convertToTime(dateStr:String!)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr {
            if  let date  = dateFormat?.date(from: datSt) {
                dateFormat?.dateFormat = "h:mm a"
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }
//        }
        //return ""
    //}
    
    class func stringSize(string: String, withSizeConstraint constraint: CGSize, andFont font: UIFont) -> (CGSize) {
        let keyLabelText = NSString(string: string)
        let sizeOfString = keyLabelText.boundingRect(with: constraint, options: .usesLineFragmentOrigin,
                                                             attributes: [NSFontAttributeName: font],
                                                             context: nil).size
        
        return sizeOfString
    }
    class func isValidURL(urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = URL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    class func isValidPhoneNumber(phoneNumber: String) -> Bool {
        if (phoneNumber.characters.count <= 0) {
            return false
        }
        let numberRegEx = "[0-9]{13}"
        let phoneNumberPredicate = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        
        return phoneNumberPredicate.evaluate(with: phoneNumber)
    }
    func roundCornersWithShadowWithColor (view:AnyObject!, color:UIColor){
        view.layer.cornerRadius = 2.0
        view.layer.borderWidth  = 0.1
        view.layer.borderColor = color.cgColor
        view.layer.masksToBounds = false
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        view.layer.shadowOpacity = shadowOpacity
    }
      class  func startActivityAnimation(activity:UIActivityIndicatorView) {
        activity.superview?.bringSubview(toFront: activity)
        activity.startAnimating()
        activity.isHidden = false
    }
    class func stopActivityAnimation(activity:UIActivityIndicatorView) {
        activity.stopAnimating()
        activity.isHidden = true
    }
    func createCustomActivityIndicatorView() {
        let window = UIApplication.shared.keyWindow
        self.activityIndicatorView = UIActivityIndicatorView.init(frame: CGRect(x:(window?.center.x)! - 20, y:(window?.center.y)! - 20, width:40, height:40))
        self.activityIndicatorView.activityIndicatorViewStyle = .white
        self.activityIndicatorView.color = UIColor.darkGray
        self.activityIndicatorView.startAnimating()
        self.activityIndicatorView.isHidden = false
        window?.addSubview(self.activityIndicatorView)
    }
    class func convertDateWithYear(dateStr:String)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr as? String {
            if  let date  = dateFormat?.date(from: datSt) {
                //                dateFormat.dateFormat = "dd MMM YYYY"
                dateFormat?.dateFormat = "dd MMM YYYY"
                
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        if let newImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return newImage
        }
        return UIImage()
    }

    func removeActivityIndicatorView() -> Void {
        if self.activityIndicatorView != nil {
            self.activityIndicatorView.isHidden = true
            self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.removeFromSuperview()
        }
    }
    func trimmingWhiteSpacesInText(text:String)->String {
        let str = text
        let trimmed = str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmed
    }
    func checkEmptyTextField(text:String!)->Bool {
        let trimmedStr = self.trimmingWhiteSpacesInText(text: text)
        if trimmedStr.characters.count == 0 {
            return true
        }
        return false
    }
//    func convertStringToObject(text: String) -> JSON{
//        if let dataFromString = text.data(using: String.Encoding.utf8, allowLossyConversion: false) {
//            let json = JSON(data: dataFromString)
//           // print("Object \(json)")
//            return json
//        }
//        /*
//        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
//            do {
//                let json = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
//                return json
//            } catch {
//                print("Something went wrong")
//            }
//        }*/
//        return nil
//    }
   
//    func trimmingLastWhiteSpace(string:String)->String {
//        let truncated = string.substring(to: string.endIndex.pr)//string.substringToIndex(string.endIndex.predecessor())
//        return truncated
//    }
    func convertObjectToString(object:JSON)->String {
        if let string = object.rawString() {
            //Do something you want
            return string
        }
        return ""

        /*
        var tempJson : NSString = ""
        do {
            let arrJson = try NSJSONSerialization.dataWithJSONObject(object, options: NSJSONWritingOptions.PrettyPrinted)
            let string = NSString(data: arrJson, encoding: NSUTF8StringEncoding)
            tempJson = string! as NSString
        }catch let error as NSError{
            print(error.description)
        }
        return tempJson as String
 */
    }
    static func replaceCharecterInString(input:String, subStringToBeReplaced:String , replaceWit:String) ->String {
       let outputString = input.replacingOccurrences(of: subStringToBeReplaced, with: replaceWit)
        return outputString
    }
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)//UIImage(CGImage: image.CGImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x:posX, y:posY, width:cgwidth, height:cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)//UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    func getAssetThumbnail(asset: PHAsset, size: CGSize) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: size.width, height: size.height), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            if let reslt = result {
                thumbnail = reslt
            }else {
                thumbnail = UIImage()
            }
        })
        return thumbnail
    }
    func beginBackgroundTask() -> UIBackgroundTaskIdentifier {
        return UIApplication.shared.beginBackgroundTask(expirationHandler: {})
    }
    
    func endBackgroundTask(taskID: UIBackgroundTaskIdentifier) {
        UIApplication.shared.endBackgroundTask(taskID)
    }

    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    class func showNoNetworMessage(){
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Network Error", subtitle:  "Your internet connection disappeared!", type: .error)
    }
    class func animateViewSize(animateView : UIView){
        UIView.animate(withDuration: 0.4, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            animateView.transform = CGAffineTransform(scaleX: 3,y: 3)
        }) { (status) in
            
        }
    }
    static func setStringDefaults(key:String , value:String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
extension String {
    func insert(string:String,ind:Int) -> String {
        return  String(self.characters.prefix(ind)) + string + String(self.characters.suffix(self.characters.count-ind))
    }
    var length: Int {
        return characters.count
    }
}

extension NSDate {
    func hoursFrom(date: NSDate) -> Int {
        return NSCalendar.current.component(.hour, from: date as Date)
        //return NSCalendar.current.components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func convertToDMY(dateStr:String!)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr {
            if  let date  = dateFormat?.date(from: datSt) {
                dateFormat?.dateFormat = "dd MMM YYYY"
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }
    func convertADate(dateStr:String)->NSDate {
        // let dateFormatter = NSDateFormatter()
        let dateFormatter = PacoDate.formatterForLocal()
        dateFormatter?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let timeZone = NSTimeZone(name: "UTC") as TimeZone? {
            dateFormatter?.timeZone = timeZone
        }
        
        let date = dateFormatter?.date(from: dateStr)
        if let tempDate = date {
            return tempDate as NSDate
        }
        return NSDate()
    }
}
extension UIApplication{
    class func getTopViewController() -> UIViewController? {
        var presentedVC = UIApplication.shared.keyWindow?.rootViewController
        while let pVC = presentedVC?.presentedViewController
        {
            presentedVC = pVC
        }
        return presentedVC
    }
}
extension UIView{
    func makeCircleandBorder(with Color : CGColor,width : CGFloat = 1.0){
        self.layer.borderWidth = width
        self.layer.masksToBounds = false
        self.layer.borderColor = Color
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
}

