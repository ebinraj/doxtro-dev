/* Copyright 2013 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "PacoDate.h"

//#import "PacoModel.h"
//#import "PacoExperimentSchedule.h"
//#import "PacoExperiment.h"

@implementation PacoDate

/*
 * 2013/07/25 12:33:22-0700
 */
+ (NSDateFormatter*)dateFormatter {
    static NSDateFormatter* dateFormatter = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    });
    
    return dateFormatter;
}

+ (NSString *)pacoStringForDate:(NSDate *)date {
    return [[PacoDate dateFormatter] stringFromDate:date];
}

+ (NSDate *)pacoDateForString:(NSString *)dateStr {
    return [[PacoDate dateFormatter] dateFromString:dateStr];
}

+ (NSDateFormatter*)debugDateFormatter:(NSString *)format {
    static NSDateFormatter* debugDateFormatter = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        debugDateFormatter = [[NSDateFormatter alloc] init];
        [debugDateFormatter setDateFormat:format];//@"HH:mm:ssZZZ, MMM dd, YYYY"];
        [debugDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    });
    
    return debugDateFormatter;
}

+ (NSString*)debugStringForDate:(NSDate*)date toFormat:(NSString *)format{
    return [[PacoDate debugDateFormatter:format] stringFromDate:date];
}


+ (int)dayIndexOfDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger day = [calendar ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitYear forDate:date];
    assert(day > 0);
    return (int)day - 1;
}

+ (int)weekdayIndexOfDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:date];
    NSInteger weekday = [components weekday];
    return (int)weekday - 1;
}

+ (int)weekOfYearIndexOfDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekOfYear fromDate:date];
    NSInteger week = [components weekOfYear];
    return (int)week - 1;
}

+ (int)monthOfYearIndexOfDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitMonth fromDate:date];
    NSInteger month = [components month];
    return (int)month - 1;
}

+ (NSDate *)midnightThisDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit units = NSCalendarUnitYear |
    NSCalendarUnitMonth |
    NSCalendarUnitDay |
    NSCalendarUnitHour |
    NSCalendarUnitMinute |
    NSCalendarUnitSecond |
    NSCalendarUnitWeekday |
    NSCalendarUnitWeekOfYear;
    
    NSDateComponents *components = [calendar components:units fromDate:date];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    return [calendar dateFromComponents:components];
}

+ (NSDate *)firstDayOfMonth:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit units = NSCalendarUnitYear |
    NSCalendarUnitMonth |
    NSCalendarUnitDay |
    NSCalendarUnitHour |
    NSCalendarUnitMinute |
    NSCalendarUnitSecond |
    NSCalendarUnitWeekday |
    NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:units fromDate:date];
    components.day = 1;
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    return [calendar dateFromComponents:components];
}

+ (NSDate *)timeOfDayThisDate:(NSDate *)date
                        hrs24:(NSInteger)hrs24
                      minutes:(NSInteger)minutes {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit units = NSCalendarUnitYear |
    NSCalendarUnitMonth |
    NSCalendarUnitDay |
    NSCalendarUnitHour |
    NSCalendarUnitMinute |
    NSCalendarUnitSecond |
    NSCalendarUnitWeekday |
    NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:units fromDate:date];
    components.hour = hrs24;
    components.minute = minutes;
    components.second = 0;
    return [calendar dateFromComponents:components];
}

+ (NSDate *)nextTimeFromScheduledDates:(NSArray *)scheduledDates
                           onDayOfDate:(NSDate *)dayOfDate {
    //NSDate *now = [self midnightThisDate:dayOfDate];
    for (NSDate *date in scheduledDates) {
        if ([dayOfDate compare:date] == NSOrderedAscending) {
            NSLog(@"LHS=%@ RHS=%@",
                  [PacoDate pacoStringForDate:dayOfDate], [PacoDate pacoStringForDate:date]);
            return date;
        } else {
            NSLog(@"SKIPPING %@ vs. %@",
                  [PacoDate pacoStringForDate:dayOfDate], [PacoDate pacoStringForDate:date]);
        }
    }
    // Time for a new list of scheduled dates.
    return nil;
}

+ (NSDate *)nextTimeFromScheduledTimes:(NSArray *)scheduledTimes
                           onDayOfDate:(NSDate *)dayOfDate {
    NSDate *now = [self midnightThisDate:dayOfDate];
    for (NSNumber *longSeconds in scheduledTimes) {
        long milliseconds = [longSeconds longValue];
        long seconds = milliseconds / 1000;
        long minutes = seconds / 60;
        long hrs = minutes / 60;
        hrs %= 24;
        minutes %= 60;
        NSDate *dateScheduled = [self timeOfDayThisDate:now hrs24:hrs minutes:minutes];
        if (dateScheduled.timeIntervalSince1970 > dayOfDate.timeIntervalSince1970) {
            return dateScheduled;
        }
    }
    // Must be the next day.
    return nil;
}

+ (NSDate *)date:(NSDate *)date thisManyDaysFrom:(int)daysFrom {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *oneDay = [[NSDateComponents alloc] init];
    oneDay.day = daysFrom;
    return [calendar dateByAddingComponents:oneDay toDate:date options:0];
}

+ (NSDate *)date:(NSDate *)date thisManyWeeksFrom:(int)weeksFrom {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *weeks = [[NSDateComponents alloc] init];
    weeks.day = weeksFrom * 7;
    return [calendar dateByAddingComponents:weeks toDate:date options:0];
}

+ (NSDate *)date:(NSDate *)date thisManyMonthsFrom:(int)monthsFrom {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *months = [[NSDateComponents alloc] init];
    months.month = monthsFrom;
    return [calendar dateByAddingComponents:months toDate:date options:0];
}

+ (NSDate *)dateSameWeekAs:(NSDate *)sameWeekAs
                  dayIndex:(int)dayIndex
                      hr24:(int)hr24
                       min:(int)min {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    int weekdayIndex = [self weekdayIndexOfDate:sameWeekAs];
    int diff = dayIndex - weekdayIndex;
    NSDate *day = [self date:sameWeekAs thisManyDaysFrom:diff];
    day = [self midnightThisDate:day];
    NSDateComponents *time = [[NSDateComponents alloc] init];
    time.hour = hr24;
    time.minute = min;
    return [calendar dateByAddingComponents:time toDate:day options:0];
}

+ (NSDate *)dateSameMonthAs:(NSDate *)sameMonthAs
                   dayIndex:(int)dayIndex {
    NSDate *monthStart = [self firstDayOfMonth:sameMonthAs];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *day = [[NSDateComponents alloc] init];
    day.day = dayIndex;
    return [calendar dateByAddingComponents:day toDate:monthStart options:0];
}

+ (NSDate *)dateOnNthOfMonth:(NSDate *)sameMonthAs
                         nth:(int)nth
                    dayFlags:(unsigned int)dayFlags {
    NSDate *startMonth = [self dateSameMonthAs:sameMonthAs dayIndex:0];
    NSDateComponents *day = [[NSDateComponents alloc] init];
    day.weekdayOrdinal = nth;
    day.weekday = 0;
    for (int i = 0; i < 7; ++i) {
        if (dayFlags & (1 << i)) {
            day.weekday = i;
            break;
        }
    }
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:day toDate:startMonth options:0];
    
}

+ (NSDate *)nextScheduledDay:(NSUInteger)dayFlags fromDate:(NSDate *)date {
    int weekday = [self weekdayIndexOfDate:date];
    int startIndex = ((weekday + 1) % 7);
    int count = 1;
    for (int i = startIndex; i < 7; ++i) {
        NSUInteger flag = 1 << i;
        if (dayFlags & flag) {
            return [self date:date thisManyDaysFrom:count];
        }
        count++;
    }
    return nil;
}

+ (NSString *)stringFromDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [self formatterForLocal];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:date];
}

+ (NSUInteger)randomUnsignedIntegerBetweenMin:(NSUInteger)min andMax:(NSUInteger)max {
    NSAssert(max >= min, @"max should be larger than or equal to min!");
    int temp = arc4random_uniform(max - min + 1);  //[0, max-min]
    return temp + min; //[min, max]
}

+ (NSString *)timestampFromDate:(NSDate *)date {
    NSTimeInterval value = [date timeIntervalSince1970];
    return [NSString stringWithFormat:@"%d", (int)value];
}

+ (NSDate *)dateFromTimestamp:(NSString *)string {
    NSTimeInterval value = [string doubleValue];
    return [NSDate dateWithTimeIntervalSince1970:value];
}

+ (NSString *)getCurrentLocalTime{
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    return [NSString stringWithFormat:@"%@",destinationDate];
}

+ (NSString *)convertTOGMT:(NSDate *)date {
    NSDateFormatter *dateFormatter = [self formatterForGMT];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *gmtStr = [dateFormatter stringFromDate:date];
    return gmtStr;
}


+ (NSString *)convertToTimeStamp:(NSString *)date {
    NSString *serverTime = date;
    NSDateFormatter *dateFormatter = [self formatterForGMT];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* serveDate = [dateFormatter dateFromString:serverTime];
    double interval  = [[NSDate date] timeIntervalSinceDate:serveDate];
    return  [PacoDate getHumanDuration:interval withDate:serveDate];
}

+ (NSDateFormatter *)descriptiveDate {
    NSDateFormatter *formatter = [self formatterForLocal];
    [formatter setDateFormat:@"MMM dd, yyyy hh:mma"];
    //  [formatter setDateStyle:NSDateFormatterMediumStyle];
    //    [formatter setTimeStyle:NSDateFormatterShortStyle];
    return formatter;
}

+ (NSString *)date:(NSDate *)date andFormatter:(NSDateFormatter *)formatter {
    NSString *dateStr = [formatter stringFromDate:date];
    return dateStr;
}

+ (long)differenceBetweenDatesinHours:(NSDate *)date {
    NSTimeInterval interval = [date timeIntervalSinceNow];
    return  interval / 3600;
}

+ (NSString*) getHumanDuration:(double) interval withDate:(NSDate*) serveDate {
    long seconds = interval;
    long minutes = seconds / 60;
    long hours = minutes / 60;
    long days = hours / 24;
    NSString* dateStr;
    if(seconds<=1) {
        dateStr = [NSString stringWithFormat:@"1 second ago"];
    }
    else if (seconds<60) {
        dateStr = [NSString stringWithFormat:@"%ld seconds ago",seconds];
    }
    else if (minutes==1) {
        dateStr = [NSString stringWithFormat:@"%ld minute ago",minutes];
    }
    else if (minutes<60) {
        dateStr = [NSString stringWithFormat:@"%ld minutes ago",minutes];
    }
    else if (hours==1) {
        dateStr = [NSString stringWithFormat:@"%ld hour ago",hours];
    }
    else if (hours<24) {
        dateStr = [NSString stringWithFormat:@"%ld hours ago",hours];
    }
    else if (days==1) {
        dateStr = [NSString stringWithFormat:@"%ld day ago",days];
    }
    else if (days<7) {
        dateStr = [NSString stringWithFormat:@"%ld days ago",days];
    }
    else if (days>=7) {
        //format to a readable style
        dateStr = [self date:serveDate andFormatter:[self descriptiveDate]];
    }
    return dateStr;
}

+ (NSString *)convertToTimeStampFromDate:(NSDate *)date{
    NSDate* serveDate = date;
    double interval  = [[NSDate date] timeIntervalSinceDate:serveDate];
    return [PacoDate  getHumanDuration:interval withDate:serveDate];
}

+(NSString *)timeWithDateFromDate:(NSDate *)date{
    NSDateFormatter *dateToString = [self formatterForLocal];
    [dateToString setDateFormat:@"dd MMM, hh:mm a"];
    return [dateToString stringFromDate:date];
}

+ (NSString *)getOnlyDateFromDate:(NSDate *)d {
    NSDateFormatter *dateFormat = [self formatterForLocal];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *theDate = [dateFormat stringFromDate:d];
    return theDate;
}

+ (NSString *)getOnlyDescriptiveDate:(NSDate *)d {
    NSDateFormatter *dateFormat = [self formatterForLocal];
    [dateFormat setDateFormat:@"MMM dd, yyyy"];
    NSString *theDate = [dateFormat stringFromDate:d];
    return theDate;
}

+ (NSString *)getOnlyDateFromDateUTC:(NSDate *)d {
    NSDateFormatter *dateFormat = [self formatterForGMT];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *theDate = [dateFormat stringFromDate:d];
    return theDate;
}

+(NSString *)getOnlyTimeFromDate:(NSDate *)d {
    NSDateFormatter *timeFormat = [self formatterForLocal];
    [timeFormat setDateFormat:@"HH:mm:ss a"];
    NSString *theTime = [timeFormat stringFromDate:d];
    return theTime;
}

+ (NSString *)getCompeleteTimeFromDate:(NSDate *)d {
    NSDateFormatter *timeFormat = [self formatterForLocal];
    [timeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *theTime = [timeFormat stringFromDate:d];
    return theTime;
}

+ (NSDate *)getDateWithMonth:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second{
    
    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    [dateComponents setHour:hour];
    [dateComponents setMinute:minute];
    [dateComponents setSecond:second];
    
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
}

+ (NSDate *)dateFromStringUTC:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [self formatterForGMT];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter dateFromString:dateString];
}

+ (NSDate *)dateFromString:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [self formatterForLocal];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter dateFromString:dateString];
}

+ (NSString *)convertGMTToLocalString:(NSDate *)date {
    NSDateFormatter *dateFormatter = [self formatterForLocal];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *gmtStr = [dateFormatter stringFromDate:date];
    return gmtStr;
}

+ (NSString *)dateInAMPM:(NSString *)convertDate {
    NSDateFormatter *dateFormatter = [self formatterForLocal];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    NSDate *date = [self dateFromString:convertDate];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    return  formattedDate;
}



+ (NSDateFormatter *)formatterForGMT {
    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSDateFormatter *inputDateFormatter = [NSDateFormatter new];
    [inputDateFormatter setTimeZone:inputTimeZone];
    return inputDateFormatter;
}

+ (NSDateFormatter *)formatterForLocal {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    NSTimeZone *localTZ = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:localTZ];
    return dateFormatter;
}

+ (NSDate *)dateOnlyFromString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [self formatterForLocal];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter dateFromString:dateString];
}

@end
