//
//  AppDelegate.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/22/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseDatabase
import MediaPlayer
import UserNotifications
import TSMessages
import AppsFlyerLib
import FirebaseInvites
let JPDeviceToken = "device_token"
let INVITE_REFERAL_CODE_KEY = "referalCode"
let INVITE_REFERAL_OBSERVER_KEY = "referalCodeObserver"
var isFromDeepLink: Bool = false
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, AppsFlyerTrackerDelegate{
    var window: UIWindow?
    var deviceToken : String?
    var isUniversalLinkClick = false
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //configure firebase
        //Fresh Chat methods
        FreshChatController.sharedManager.initFreshchatSDK()
        FreshChatController.sharedManager.setConfugurartionofFreshChat()
        //firbase consguration
        FirebaseApp.configure()
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "com.doxtro.app"
        Database.database().isPersistenceEnabled = true
        IQKeyboardManager.sharedManager().enable = true
        //intialize app event analyser
        DPAppEventLog.shared.initializeAppFlyerComponents()
        AppsFlyerTracker.shared().delegate = self
        UINavigationBar.appearance().tintColor = UIColor.white
        setUpIntailViewController()
        if let launchOptions = launchOptions,(launchOptions[UIApplicationLaunchOptionsKey.userActivityDictionary] != nil){
                isUniversalLinkClick = true
        }
        if let launchOptions = launchOptions, let apnsInfo = launchOptions[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
           
            if let apnsinfo =  apnsInfo as? [String : Any]{
                if #available(iOS 10.0, *) {
                    notificationHandlerss(userInfo:apnsinfo)
                } else {
                   
                    notificationHandlerss(userInfo:apnsinfo)
                    // Fallback on earlier versions
                }
            }else{
               showSplashVideo()
            }
        }else{
            if !isUniversalLinkClick{
            showSplashVideo()
            }
        }
        if Freshchat.sharedInstance().isFreshchatNotification(launchOptions) {
            Freshchat.sharedInstance().handleRemoteNotification(launchOptions, andAppstate: application.applicationState)
        }
        return true
    }
    /*
     //launch option is because of push notification
     if let launchOptions = launchOptions, let apnsInfo = launchOptions[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
     if let apnsinfo =  apnsInfo as? [String : Any]{
     if #available(iOS 10.0, *) {
     notificationHandler(userInfo:apnsinfo)
     } else {
     
     // Fallback on earlier versions
     }
     }
     }
 */
    func notificationHandlerss(userInfo : [String : Any]){
        if let userInfo = userInfo as? [String : Any]{
            // response.request.content.userInfo as? [String : Any]{
            if let type = userInfo["type"] as? String{
                if type == "chat" || type == "doctorAssign"{
                    if let consultationId = userInfo["consultationId"] as? String{
                        getConsulatationInfo(consultationId: consultationId)
                    }
                }
            } else{
                if let clevertapDeepLinkString = userInfo["wzrk_dl"] as? String {
                    if let deepUrl = URL(string: clevertapDeepLinkString) {
                        if let af_dp = deepUrl.valueOf(queryParamaterName: "af_dp") {// Output i.e "blah" {
                            if let afDpUrl = URL(string: af_dp) {
                                //if (afDpUrl.pathComponents.count) >= 1 {
                                let lastPathComponent:String = (afDpUrl).lastPathComponent
                                if let hostName = afDpUrl.host {
                                    self.loadViewController(pId: (hostName), campaignName: lastPathComponent)
                                }
                                // }
                            }
                        }
                    }
                }
                
               /* if let clevertapDeepLinkString = userInfo["wzrk_dl"] as? String {
                    if let deepUrl = URL(string: clevertapDeepLinkString) {
                        let queryItems = URLComponents(url: deepUrl, resolvingAgainstBaseURL: true)?.queryItems
                        if let articleId =  queryItems?.filter({(item) in item.name == "articleIdBy"}).first?.value{
                            // UIAlertController.showAlertWithMessage("inviteBy Fail")
                            loadArticleByArticleId(articleId: articleId)
                        }
                    }
                }*/
            }
            
        }
    }
    func showSplashVideo(){
        var movieUrl : URL?
        var moviePlayer :MPMoviePlayerViewController?
        if  let moviewPath =  Bundle.main.path(forResource: "Logo_Reveal_v4", ofType: "m4v", inDirectory: "Video"){
                movieUrl = URL(fileURLWithPath: moviewPath)
        }
        moviePlayer = MPMoviePlayerViewController(contentURL: movieUrl)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setIntialControllers), name: NSNotification.Name.MPMoviePlayerPlaybackDidFinish, object: moviePlayer?.moviePlayer)
        moviePlayer?.moviePlayer.controlStyle = .none
        moviePlayer?.moviePlayer.scalingMode = .aspectFit
        moviePlayer?.moviePlayer.setFullscreen(true, animated: false)
        moviePlayer?.moviePlayer.play()
        let uiView = UIView()
        uiView.frame = UIScreen.main.bounds
        moviePlayer?.moviePlayer.view.backgroundColor = UIColor.white
        moviePlayer?.view.backgroundColor = UIColor.white
        moviePlayer?.moviePlayer.backgroundView.backgroundColor = UIColor.white
        self.window?.rootViewController = moviePlayer
    }
    //after finishing video show main view constroller
    func setIntialControllers(){
    setUpIntailViewController()
    }
       func setUpIntailViewController(){
        if DPWebServicesManager.sharedManager.user != nil{
            registerForPushNotification()
            setUpViewController(With: "DPTabbar", and: "TabStoryboardId")
            self.getTabbarController()?.delegate = self as? UITabBarControllerDelegate
        }else{
            setUpViewController(With: "Main", and: "signIn")
        }
    }
    func setUpViewController(With storyBoardId : String ,and viewController : String){
            let storyBoard : UIStoryboard = UIStoryboard(name: storyBoardId , bundle:nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: viewController)
            self.window?.rootViewController = viewController
            self.window?.makeKeyAndVisible()
    }
    func getTabbarController()-> UITabBarController?{
        let tabBarController = self.window?.rootViewController as? UITabBarController
        return tabBarController!
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        ConsultationViewController.changeUserStatus(with:"offline")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "updateOnlineUser")))
        registerForAPNS()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        DPAppEventLog.shared.trackAppLauch()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        ConsultationViewController.changeUserStatus(with:"offline")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        if #available(iOS 10.0, *) {
            DatabaseController.saveContext()
        } else {
            // Fallback on earlier versions
        }
    }
    func registerForPushNotification(){
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {status, _ in
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }

   

}
extension AppDelegate : UITabBarControllerDelegate{
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return viewController != tabBarController.selectedViewController;
}
}
extension AppDelegate :  UNUserNotificationCenterDelegate,MessagingDelegate{
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FreshChatController.sharedManager.registerPushNotification(deviceToken: deviceToken)
        //update your Device Token
        if let fcmToken = Messaging.messaging().fcmToken{
            self.deviceToken = fcmToken
            checkForUpdateToken(newToken: fcmToken)
        }else if let fcmToken = InstanceID.instanceID().token(){
            self.deviceToken = fcmToken
            checkForUpdateToken(newToken: fcmToken)
        }
        CleverTap.sharedInstance()?.setPushToken(deviceToken)
    }
    func checkForUpdateToken(newToken : String){
        if let oldToken = UserDefaults.standard.value(forKey: JPDeviceToken) as? String, oldToken.length > 0 && oldToken == newToken {
            return
        }
        UserDefaults.standard.set(newToken, forKey: JPDeviceToken)
        UserDefaults.standard.synchronize()
        DPWebServicesManager.sharedManager.UpdateUserDeviceId()
    }
    // for ios 9
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        registerForAPNS()
    }
    func registerForAPNS() {
        UIApplication.shared.registerForRemoteNotifications()
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //DPWebServicesManager.sharedManager.updateBadgeCount()
        completionHandler([.sound,.badge,.alert])
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let userInfo = response.notification.request.content.userInfo as? [String : Any]{
            FreshChatController.sharedManager.handleFreshChatPushNotification(userInfo: userInfo, application: UIApplication.shared)
            notificationHandler(userInfo: userInfo)
        }
        // if you wish CleverTap to record the notification open and fire any deep links contained in the payload
        CleverTap.sharedInstance().handleNotification(withData: response.notification.request.content.userInfo)
        completionHandler()
    }
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        self.deviceToken = fcmToken
       checkForUpdateToken(newToken: fcmToken)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
         FreshChatController.sharedManager.handleFreshChatPushNotification(userInfo: userInfo, application: application)
        CleverTap.sharedInstance()?.handleNotification(withData: userInfo)
        completionHandler(.noData)

    }
    func application(application: UIApplication, didReceiveRemoteNotification
        userInfo: [NSObject : AnyObject]) {
        if Freshchat.sharedInstance().isFreshchatNotification(userInfo) {
            Freshchat.sharedInstance().handleRemoteNotification(userInfo, andAppstate: application.applicationState)
        }
        CleverTap.sharedInstance()?.handleNotification(withData: userInfo)
    }
    func application(application: UIApplication, didReceiveLocalNotification
        notification: UILocalNotification) {
        CleverTap.sharedInstance()?.handleNotification(withData: notification)
    }
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?,
                     forRemoteNotification userInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        CleverTap.sharedInstance()?.handleNotification(withData: userInfo)
        completionHandler()
    }
    func application(application: UIApplication, openURL url: NSURL,
                     sourceApplication: String?, annotation: AnyObject) -> Bool {
        CleverTap.sharedInstance()?.handleOpen(url as URL! as URL!, sourceApplication: sourceApplication)
        return (Invites.handle(url as URL, sourceApplication: sourceApplication, annotation: annotation) != nil)
//        return Invites.handleUniversalLink(url) { invite, error in
//            // ...
//        }
    }
    
    // Swift 3
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        CleverTap.sharedInstance()?.handleOpen(url, sourceApplication: nil)
        AppsFlyerTracker.shared().handleOpen(url, options: options)
        if let isDynamicLink = DynamicLinks.dynamicLinks()?.shouldHandleDynamicLink(fromCustomSchemeURL: url),
            isDynamicLink {
            let dynamicLink = DynamicLinks.dynamicLinks()?.dynamicLink(fromCustomSchemeURL: url)
            return handleDynamicLink(dynamicLink)
        }
        
        return false
    }
    
    func open(_ url: URL, options: [String : Any] = [:],
              completionHandler completion: ((Bool) -> Swift.Void)? = nil) {
        CleverTap.sharedInstance()?.handleOpen(url, sourceApplication: nil)
        completion?(false)
    }
    // As of iOS 8 and above
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?,
                     forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        CleverTap.sharedInstance()?.handleNotification(withData: notification)
        completionHandler()
    }
    @available(iOS 10.0, *)
    func notificationHandler(userInfo : [String : Any]) {
        
        if let userInfo = userInfo as? [String : Any]{
       // response.request.content.userInfo as? [String : Any]{
            if let type = userInfo["type"] as? String{
                if type == "chat" || type == "doctorAssign"{
                    if let consultationId = userInfo["consultationId"] as? String{
                        getConsulatationInfo(consultationId: consultationId)
                    }
                }
            }
        }
    }
    func getConsulatationInfo(consultationId : String){
        DPWebServicesManager.getConsulatationInfo(consultationId: consultationId,completationHandler: { (consulatationObj, error) in
            if let consulatationObj = consulatationObj{
                DPWebServicesManager.sharedManager.saveConsultationInfo(with:consulatationObj)
                let storyBoard : UIStoryboard = UIStoryboard(name: "DPTabbar" , bundle:nil)
                if let viewController = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as? ConsultationViewController{
                 viewController.consulatationInfo = consulatationObj
                    DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulatationObj)
                    if let tabCpntroller = UIViewController.tabViewController(){
                        if let Controller =  UIViewController.navigationControllerForTab(.home){
                            Controller.pushViewController(viewController, animated: true)
                        }
                    }
                }
            }else{
             TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "ConsulatationNotFound", type: .error)
            }
        })
    }
}
extension NSData {
    func stringFromData() -> String {
        let characterSet: CharacterSet = CharacterSet(charactersIn: "<>")
        let deviceTokenString: String = (self.description as NSString)
            .trimmingCharacters(in: characterSet)
            .replacingOccurrences( of: " ", with: "") as String
        return deviceTokenString
    }
}
extension AppDelegate {
    func loadChatScreenWithConsultationId(_ consultationId: String) {
        DPWebServicesManager.getConsulatationInfo(consultationId: consultationId,completationHandler: { (consulatationObj, error) in
            if let consulatationObj = consulatationObj{
               
                    let storyBoard = UIStoryboard(name: "DPTabbar", bundle: nil)
                    let viewController = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ConsultationViewController
                    viewController.isChatNavigate = true
                    viewController.senderDisplayName = ""
                    viewController.consulatationInfo = consulatationObj
                 DPWebServicesManager.sharedManager.saveConsultationInfo(with:consulatationObj)
                    // UserDefaults.standard.setStringDefaults(key: consultationId_key, value: consultationId)
                if let tabCpntroller = UIViewController.tabViewController(){
                    if let Controller =  UIViewController.navigationControllerForTab(.home){
                        Controller.pushViewController(viewController, animated: true)
                    }
                }

            }else{
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "ConsulatationNotFound", type: .error)
            }
        })
       
    }
    func loadHealthRecords() {
        if let controller = UIViewController.controllerWithSwitchTab(.health, pop: true, animated: false) as? ConsultationTabViewController {
        }
    }
    func loadSettings() {
         if let controller = UIViewController.controllerWithSwitchTab(.settings, pop: true, animated: false) as? ConsultationTabViewController {
        }
    }
    func loadPreselectSpecialization(specializatioName : String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "DPTabbar" , bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "selectedSpecializationlist") as! DPSelectedSpecilizationController
        viewController.isFromDeepLink = true
        let listobj = DPSpecializationList(dict: ["category":specializatioName])
        let predicate = NSPredicate(format: "specializationName == %@", specializatioName)
        if let obj = DatabaseController.fetchSpecializationListFromCoreData(predicate: predicate),let specializationObj = obj.first{
            viewController.selectedSpecializationObj = specializationObj
        if let tabCpntroller = UIViewController.tabViewController(){
        if let Controller =  UIViewController.navigationControllerForTab(.home){
        Controller.pushViewController(viewController, animated: true)
        }
            }
        }
    }
}
extension AppDelegate {
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        AppsFlyerTracker.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
        return (Invites.handle(url , sourceApplication: sourceApplication, annotation: annotation) != nil)
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let af_dp = userActivity.webpageURL?.valueOf(queryParamaterName: "af_dp") {// Output i.e "blah" {
            if let afDpUrl = URL(string: af_dp) {
                if (afDpUrl.pathComponents.count) >= 1 {
                    let lastPathComponent:String = (afDpUrl).lastPathComponent
                    if let hostName = afDpUrl.host {
                        self.loadViewController(pId: (hostName), campaignName: lastPathComponent)
                    }
                }
            }
        }
        guard let dynamicLinks = DynamicLinks.dynamicLinks() else { return false }
        let handled = dynamicLinks.handleUniversalLink(userActivity.webpageURL!) { (dynamicLink, error) in
            if (dynamicLink != nil) && !(error != nil) {
                self.handleDynamicLink(dynamicLink)
            }
        }
        if !handled {
            // Handle incoming URL with other methods as necessary
            // ...
        }
        
        AppsFlyerTracker.shared().continue(userActivity, restorationHandler: restorationHandler)
        return handled
    }
    /**
     Deep link part
     */
    func loadViewController(pId: String, campaignName: String) {
        if pId == "chat" {
            var consultationid = campaignName
            if consultationid == "" {
                consultationid = "sample"
            }
        loadChatScreenWithConsultationId(consultationid)
            //Load chat screen
            
        } else if pId == "specialization" {
          //  loadSpecialization()
        } else if pId == "consultation_offer" {
            if campaignName == "" {
                //Navigate to page when campaign is empty
            } else {
                loadPreselectSpecialization(specializatioName : campaignName)
                //Navigate to page which has preselected specialization
            }
            // loadAvailability()
        } else if pId == "health_records" {
            // loadBankDetails()
            loadHealthRecords()
        } else if pId == "settings" {
            loadSettings()
        } else if pId == "article" {
            loadArticleByArticleId(articleId: campaignName)
        }else if pId == "subscription"{
            loadPage(tabIndex: .home, tabType: "subscription")
        }else if pId == "oneTimePackage"{
            loadPage(tabIndex: .home, tabType: "oneTimePackage")
        }else if pId == "referral"{
            loadPage(tabIndex: .settings, tabType: "referral")
        }
    }
}
//mark: firebase dynmic link
extension AppDelegate{
    func handleDynamicLink(_ dynamicLink: DynamicLink?) -> Bool {
        guard let dynamicLink = dynamicLink else { return false }
        guard let deepLink = dynamicLink.url else { return false }
        isFromDeepLink = true
        let queryItems = URLComponents(url: deepLink, resolvingAgainstBaseURL: true)?.queryItems
        if let invitedBy = queryItems?.filter({(item) in item.name == "invitedby"}).first?.value{
            //UIAlertController.showAlertWithMessage("inviteBy \(invitedBy)")
            UserDefaults.standard.setValue(invitedBy, forKey: INVITE_REFERAL_CODE_KEY)
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: Notification.Name(INVITE_REFERAL_OBSERVER_KEY), object: nil)
        }else if let articleId =  queryItems?.filter({(item) in item.name == "articleIdBy"}).first?.value{
           // UIAlertController.showAlertWithMessage("inviteBy Fail")
            loadArticleByArticleId(articleId: articleId)
        }
    
        //let user = Auth.auth().currentUser
        // If the user isn't signed in and the app was opened via an invitation
        // link, sign in the user anonymously and record the referrer UID in the
        // user's RTDB record.
//        if user == nil && invitedBy != nil {
//            Auth.auth().signInAnonymously() { (user, error) in
//                if let user = user {
//                    let userRecord = Database.database().reference().child("users").child(user.uid)
//                    userRecord.child("referred_by").setValue(invitedBy)
//                    if dynamicLink.matchConfidence == .weak {
//                        // If the Dynamic Link has a weak match confidence, it is possible
//                        // that the current device isn't the same device on which the invitation
//                        // link was originally opened. The way you handle this situation
//                        // depends on your app, but in general, you should avoid exposing
//                        // personal information, such as the referrer's email address, to
//                        // the user.
//                    }
//                }
//            }
//        }
        return true
    }
//    func loadArticleByArticleId(articleId: String) {
//        //DDAppController.shared.initilizeWebServiceComponents()
//        let viewController = UIViewController.tabViewController()
//        viewController?.selectedIndex = 2
//        window?.rootViewController = viewController
//        window?.makeKeyAndVisible()
//        if self.window?.rootViewController as? UITabBarController != nil {
//            if let tababarController = self.window?.rootViewController as? UITabBarController {
//                DXUtility.setStringDefaults(key: "articleIdFromDeepLink", value: articleId)
//                tababarController.selectedIndex = 2
//            }
//        }
//    }
    func loadArticleByArticleId(articleId: String) {
        let loginStatus = DPWebServicesManager.sharedManager.user != nil ? true : false
        if loginStatus == true {
           // DDAppController.shared.initilizeWebServiceComponents()
            
            let viewController = UIViewController.tabViewController()
          viewController?.selectedIndex = 3
            window?.rootViewController = viewController
            window?.makeKeyAndVisible()
            if self.window?.rootViewController as? UITabBarController != nil {
                if let tababarController = self.window?.rootViewController as? UITabBarController {
                    DXUtility.setStringDefaults(key: "articleIdFromDeepLink", value: articleId)
                    NotificationCenter.default.post(name: Notification.Name(LOAD_HEALTH_FEED_KEY), object: nil)
                    tababarController.selectedIndex = 3
                }
            }
        } else {
            isFromDeepLink = false
        }
    }
    
    func loadPage(tabIndex : DPTab,tabType : String)  {
        let loginStatus = DPWebServicesManager.sharedManager.user != nil ? true : false
        if loginStatus == true {
            // DDAppController.shared.initilizeWebServiceComponents()
            
            let viewController = UIViewController.tabViewController()
            viewController?.selectedIndex = tabIndex.rawValue
            window?.rootViewController = viewController
            window?.makeKeyAndVisible()
            if self.window?.rootViewController as? UITabBarController != nil {
                if let tababarController = self.window?.rootViewController as? UITabBarController {
                    tababarController.selectedIndex = tabIndex.rawValue
                    if let tabCpntroller = UIViewController.tabViewController(){
                        if let Controller =  UIViewController.navigationControllerForTab(tabIndex){
                            if tabType == "subscription" || tabType == "oneTimePackage"{
                                let storyBoards = UIStoryboard(name: "DPTabbar", bundle: nil)
                                if let viewController = storyBoards.instantiateViewController(withIdentifier: "packageController") as? PackageViewController{
                                    Controller.pushViewController(viewController, animated: true)
                                }
                            }else if tabType == "referral" {
                                let storyBoards = UIStoryboard(name: "Main", bundle: nil)
                                if let viewController = storyBoards.instantiateViewController(withIdentifier: "referelCodeController") as? ReferelViewController{
                                    Controller.pushViewController(viewController, animated: true)
                                }
                            }
                        }
                    }
                }
            }
        } else {
            isFromDeepLink = false
        }
    }
}
