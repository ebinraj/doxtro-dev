//
//  DDHealthFeedDetailViewController.swift
//  DrDoxtro
//
//  Created by vinaykumar on 12/14/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import Alamofire
import WebKit
import SwiftyJSON
import Firebase
import FirebaseDatabase
class DDHealthFeedDetailViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var likedButton: UIButton!
    @IBOutlet weak var likesCountLabel: UILabel!
    var healthModel: DDHealthFeedModel!
    var animator: ZoomToFullscreen?
    var healthFeedDetailObj = [String: JSON]()
    lazy var articleRef: DatabaseReference = Database.database().reference().child("HealthFeed")
    var articleRefHandle: DatabaseHandle?
    var likesArray = [String]()
    var webContentHeight: CGFloat = 0
    var contentHeights : [CGFloat] = [0.0, 0.0]
    var videoContentHeights : [CGFloat] = [0.0, 0.0]
    var articleId: String = ""
    var isLiked: Bool = false
    var likesCount: Int = 0
    var relatedArticles = [JSON]()
    var heightOfWebview: CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        articleRef = Database.database().reference().child("HealthFeed")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 250
        self.tableView.tableFooterView = UIView()
       // if healthModel != nil {
           // if let articleId = healthModel._id {
                
            ABProgressIndicator.shared.showAnimator()
                DPWebServicesManager.sharedManager.getHealthFeedDetail(articleId: articleId, completionHandler: { (json, error) in
                    ABProgressIndicator.shared.hideAnimator()
                    if let response = json {
                        if let data = response[data_key].dictionary {
                            self.healthFeedDetailObj = data
                            self.healthModel = DDHealthFeedModel(dict: data)
                            self.tableView.reloadData()
                            self.observeMessages()
                        }//https://www.facebook.com/iotgoin/videos/402626340151428/
                    }
                })
    }
    func fetchRelatedArticles() {
        let categoryString = (healthModel?.categories?.map{String(describing: $0)})?.joined(separator: ",")
        let details = ["id" : healthModel._id, "categories" : categoryString]
        DPWebServicesManager.sharedManager.getRelatedArticles(details: details as [String : AnyObject]) { (response, error) in
            if let res = response {
                if let data = res[data_key].array {
                   self.relatedArticles = data
                }
                self.tableView.reloadData()
            }
        }
    }
    private func observeMessages() {
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
        articleRefHandle = articleRef.observe(.value, with: { (snapshot : DataSnapshot) in
            if let data = snapshot.value as? Dictionary<String, AnyObject> {
                if let value = data[userID] as? [String] {
                    self.likesArray = value
                }else {
                    self.likesArray.removeAll()
                }
                self.updateLikeStatus()
            }
        })
    }
}
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let refHandle = articleRefHandle {
            articleRef.removeObserver(withHandle: refHandle)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        if healthModel != nil {
            observeMessages()
        }
    }
    /**
     Update like for specific article to Firebase & API
     */
    func updateLikeStatus() {
       // if healthModel != nil {
            //let isLiked = self.isLiked
        
        if likesArray.contains(articleId) {
           healthModel.isLiked = true
        } else {
            healthModel.isLiked = false
        }
        
        self.likedButton.setTitle(healthModel.isLiked == true ? "Liked" : "Like", for: .normal)
        self.likedButton.setImage(healthModel.isLiked == true ? #imageLiteral(resourceName: "heart_green") : #imageLiteral(resourceName: "heart"), for: .normal)
        
        self.likesCountLabel.text =  self.healthModel.isLiked == true ?  String(self.likesCount + 1) : String(self.likesCount)
        if self.likesCountLabel.text == "0" {
            self.likesCountLabel.isHidden = true
        } else {
            self.likesCountLabel.isHidden = false
        }
        self.navigationItem.title = healthModel?.title
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func likeAction(_ sender: UIButton) {
        if let userId = DPWebServicesManager.sharedManager.user?.userId{
            if healthModel != nil {
                let isLike = healthModel.isLiked
                let details = ["articleId" : healthModel._id, "userId" : userId, "type" : (isLike == true ? "unlike" : "like")]
                ABProgressIndicator.shared.showAnimator()
                DPWebServicesManager.sharedManager.updateHealthFeedLike(details: details as [String : AnyObject]) { (json, error) in
                    ABProgressIndicator.shared.hideAnimator()
                    if let response = json {
                        if let _ = response[data_key].dictionary {
                            if !isLike {
                                self.likesArray.append(self.healthModel?._id ?? "")
                            } else{
                                // Remove/filter item with value '_id'
                                self.likesArray = self.likesArray.filter { $0 != self.healthModel?._id }
                            }
                            let itemRef = self.articleRef.child(userId) // 1
                            let likesItem = self.likesArray as [Any]
                            itemRef.setValue(likesItem)
                            self.healthModel.isLiked = !self.healthModel.isLiked
                            sender.setTitle(self.healthModel?.isLiked == true ? "Liked" : "Like", for: .normal)
                            sender.setImage(self.healthModel.isLiked == true ? #imageLiteral(resourceName: "heart_green") : #imageLiteral(resourceName: "heart"), for: .normal)
                            self.likesCountLabel.text = self.healthModel.isLiked == true ?  String(self.likesCount + 1) : String(self.likesCount)
                            if self.likesCountLabel.text == "0" {
                                self.likesCountLabel.isHidden = true
                            } else {
                                self.likesCountLabel.isHidden = false
                            }

                        } else {
                            if let er = error {
                                UIAlertController.showAlertWithMessage( er.localizedDescription, title: "Error")
                            }
                        }
                    }
                }
            }
        }
    }
    @IBAction func shareAction(_ sender: UIButton) {
        if healthModel != nil {
            
            if let id = healthModel._id {
                if let linkUrl = URL(string: "https://doxtro.com/?articleIdBy=\(id)") {
                    let components = DynamicLinkComponents(link: linkUrl, domain: "ut6d4.app.goo.gl")
                    let iOSParams = DynamicLinkIOSParameters(bundleID: "com.doxtro.app")
                    iOSParams.customScheme = "com.doxtro.app"
                    iOSParams.appStoreID = "1161686991"
                    //iOSParams.fallbackURL = URL(string:"http://doxtro.com")
                    components.iOSParameters = iOSParams
                    
                    let androidParams = DynamicLinkAndroidParameters(packageName: "com.doxtro.app")
                    //androidParams.fallbackURL = URL(string: "http://doxtro.com")

                    components.androidParameters = androidParams
                    
                    // social tag params
                    let socialParams = DynamicLinkSocialMetaTagParameters()
                    socialParams.title = healthModel.title
                    socialParams.descriptionText = healthModel.summaryText?.convertHtml().string ?? ""
                    if healthModel.mediaType == "Image" {
                        if let coverPic = healthModel.coverPic {
                            socialParams.imageURL = URL(string: coverPic)
                        }
                    } else {
                        let thumbNails = healthModel.thumbnails
                        if thumbNails.count > 0 {
                            if let healthFeedVideoThumbnail = thumbNails.first {
                                if let uri = healthFeedVideoThumbnail.uri {
                                    if let url = URL(string: uri) {
                                        socialParams.imageURL = url
                                    }
                                }
                            }
                        }
                    }
                    components.socialMetaTagParameters = socialParams

                    
                    let options = DynamicLinkComponentsOptions()
                    options.pathLength = .unguessable
                    components.options = options
                    ABProgressIndicator.shared.showAnimator()

                    components.shorten { (shortURL, warnings, error) in
                        // Handle shortURL.
                        ABProgressIndicator.shared.hideAnimator()

                        if let error = error {
                            print(error.localizedDescription)
                            return
                        }
                        let shortLink = shortURL
                        print(shortLink?.absoluteString ?? "")
                        let activityController = UIActivityViewController(activityItems: [shortLink?.absoluteString ?? ""], applicationActivities: nil)
                        DispatchQueue.main.async {
                            self.present(activityController, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "doctorProfileId" {
            let destinationController = segue.destination as! DoctorProfileViewController
            destinationController.doctorId = healthModel.doctor?.id
            destinationController.doctorFee = "***"
            destinationController.isArticle = true
        }
    }
}
extension DDHealthFeedDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
//        if relatedArticles.count > 0 {
//            return 5
//        }
        return 5//3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4 {
            return relatedArticles.count
        } else if section == 3 {
            if relatedArticles.count > 0 {
                return 1
            }
            return 0
        } else {
            return 1
        }
//        if relatedArticles.count > 0 {
//            if section == 4 {
//                return relatedArticles.count
//            } else if section == 3 {
//                return 1
//            }
//        }
//        return 1
    }
    fileprivate func docDetailsConfigureCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! DDHealthFeedDetailHeaderTVCell
        if healthModel != nil {
            cell.articleTitleLabel.text = healthModel?.title
            if let firstName = healthModel.doctor?.firstName {
                if firstName.count > 3 {
                    if firstName.prefix(3) != "Dr." {
                        if let firstName = healthModel.doctor?.firstName {
                            cell.docNameLabel.text = "Dr. " + firstName
                        }
                    } else {
                        cell.docNameLabel.text = healthModel?.doctor?.firstName
                    }
                } else {
                    cell.docNameLabel.text = healthModel?.doctor?.firstName
                }
            }
            if let createdAt = healthModel?.createdAt {
                cell.postedDateLabel.text = "Posted on " + createdAt.convertDateAndYear(dateStr: createdAt)
            }
            if let profilePic = healthModel.doctor?.profilePic {
                if let url = URL(string: profilePic) {
                    DispatchQueue.main.async {
                        cell.profilePicImageView.af_setImage(for: .normal, url: url)
                        //cell.profilePicImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "DefaultImage"))
                    }
                } else {
                    //  cell.bannerImagePreview.image =  #imageLiteral(resourceName: "DefaultImage")
                }
            }
            if let speicilaizationString = healthModel.doctor?.category {
                let categoryString = (speicilaizationString.map{String(describing: $0)}).joined(separator: ", ")
                cell.docSpecilaizationLabel.text = categoryString
            }
            let categoryString = (healthModel?.categories?.map{String(describing: $0)})?.joined(separator: ", ")
            if categoryString == "" {
                cell.categoryView.isHidden = true
            } else {
                cell.categoryView.isHidden = false
            }
            cell.categoriesLabel.text = categoryString
            cell.delegate = self
            cell.selectionStyle = .none
        }
        return cell
    }
    
    fileprivate func headerConfigureCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "videoContentCell", for: indexPath) as! DDHealthFeedDetailWebContentTVCell
        if healthModel != nil {
            var videoFrameString = ""
            if healthModel?.mediaType != "Image" {
                cell.coverImageView.isHidden = true
                videoFrameString = "<iframe src=https://www.facebook.com/plugins/video.php?href=https://www.facebook.com/iotgoin/videos/\(self.healthModel?.videoId ?? "")?width=500 height=\"215\"/></iframe>"
                let finalString = videoFrameString.appending("<style>p {font-size:17px}</style>").appending("<style>img{display: inline;height: auto;max-width: 100%;}</style>").appending("<style>p {font-family:'Asap-Regular'}</style>").appending("<style>iframe{display: inline; width:100%; max-width: 100%;}</style>")
                //HelveticaNeue-Light
                let htmlHeight = videoContentHeights[indexPath.row]
                cell.webView.tag = indexPath.row + 100
                let htmlString = finalString
                if videoContentHeights[0] == 0 {
                    cell.webView.loadHTMLString(htmlString, baseURL: nil)
                    cell.webView.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width - 20, height: htmlHeight)
                }
            } else {
                cell.coverImageView.isHidden = false
                cell.webView.isHidden = true
                if let coverPicUrl = healthModel.coverPic {
                    if let url = URL(string: coverPicUrl) {
                        DispatchQueue.main.async {
                            cell.coverImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "DefaultImage"))
                        }
                    } else {
                        //  cell.bannerImagePreview.image =  #imageLiteral(resourceName: "DefaultImage")
                    }
                }
                cell.showFullScreen = {[weak self](imageView : UIImageView) in
                    if let weakSelf  = self{
                        let storyBoard = UIStoryboard(name: "DPTabbar", bundle: nil)
                        let viewController = storyBoard.instantiateViewController(withIdentifier: "FullViewImage") as!  JPFullScreenImageController
                        viewController.image = cell.coverImageView.image
                        let animator = ZoomToFullscreen.animator()
                        self?.animator = animator
                        animator.presentController(viewController, fromController: weakSelf, fromImageView: imageView)
                    }
                }
                
            }
            cell.delegate = self
            cell.webView.scrollView.isScrollEnabled = false
            cell.selectionStyle = .none
            
        }
        return cell
    }
    
    fileprivate func contentConfigureCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "contentCell", for: indexPath) as! DDHealthFeedDetailWebContentTVCell
        if healthModel != nil {
            if let content = healthFeedDetailObj["content"]?.string {
                let finalString = content.appending("<style>p {font-size:20px}</style>").appending("<style>img{display: inline;height: auto;max-width: 100%;}</style>").appending("<style>p {font-family:'HelveticaNeue-Light'}</style>").appending("<style>iframe{display: inline; width:100%; max-width: 100%;}</style>").appending("<style>p{line-height: 1.6;}</style>")
                let videoFrameString = ""
                if healthModel?.mediaType != "Image" {
                    //                        videoFrameString = "<iframe src=https://www.facebook.com/plugins/video.php?href=https://www.facebook.com/iotgoin/videos/\(self.healthModel?.videoId ?? "")?width=500 height=\"215\"/></iframe>"
                }
                cell.delegate = self
                cell.webView.scrollView.isScrollEnabled = false
                cell.selectionStyle = .none
                
                let htmlHeight = contentHeights[indexPath.row]
                cell.webView.tag = indexPath.row + 200
                let htmlString = videoFrameString + finalString
                if contentHeights[0] == 0 {

                cell.webView.loadHTMLString(htmlString, baseURL: nil)
                cell.webView.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width - 20, height: htmlHeight)
                }
            }
        }
        return cell
    }
    
    fileprivate func relatedArticlesConfigureCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "relatedArticle", for: indexPath) as! DDHealthRelatedArticleTVCell
        if relatedArticles.count > indexPath.row {
            if let dict = relatedArticles[indexPath.row].dictionary {
                cell.articleTitleLabel.text = dict["title"]?.string ?? ""
                cell.articleSubTitleLabel.text = (dict["summaryText"]?.string ?? "").convertHtml().string
                cell.coverPicImageView.layer.cornerRadius = 5
                if dict["mediaType"]?.string == "Image" {
                    if let coverPicUrl = dict["coverPic"]?.string {
                        if let url = URL(string: coverPicUrl) {
                            DispatchQueue.main.async {
                                cell.coverPicImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "DefaultImage"))
                            }
                        } else {
                            //  cell.bannerImagePreview.image =  #imageLiteral(resourceName: "DefaultImage")
                        }
                    }
                } else {
                    cell.playerThumbnailView.isHidden = false
                    if let thumbNails = dict["thumbnails"]?.array {
                        if thumbNails.count > 0 {
                            if let healthFeedVideoThumbnailDict = thumbNails.first {
                                if let uri = healthFeedVideoThumbnailDict["uri"].string {
                                    if let url = URL(string: uri) {
                                        DispatchQueue.main.async {
                                            cell.coverPicImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "DefaultImage"))
                                        }
                                    } else {
                                        // cell.bannerImagePreview.image =  #imageLiteral(resourceName: "DefaultImage")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return headerConfigureCell(indexPath)

        case 1:
            return docDetailsConfigureCell(indexPath)

        case 2:
            return contentConfigureCell(indexPath)

        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath) as! DDhealthRelatedSectionTVCell
            cell.selectionStyle = .none
            cell.sectionTitle.text = "Related articles"
            return cell

        case 4:
            return relatedArticlesConfigureCell(indexPath)

        default:
            return relatedArticlesConfigureCell(indexPath)
//UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 4 {
            let relatedArticle = relatedArticles[indexPath.row]
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "feedDetailViewId") as! DDHealthFeedDetailViewController
            viewController.articleId = relatedArticle["_id"].string ?? ""
            viewController.likesCount = relatedArticle["likes"].int ?? 0
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            return UITableViewAutomaticDimension//contentHeights[indexPath.row]
        } else if indexPath.section == 3 {
          return 55
        }
        else if indexPath.section == 2 {
            return contentHeights[indexPath.row]
//55
        } else if indexPath.section == 4 {
            return UITableViewAutomaticDimension
        } else {
            if healthModel != nil {
                if healthModel.mediaType == "Video" {
//                  return videoContentHeights[indexPath.row] + UITableViewAutomaticDimensionr
//                } else {
                   // return UITableViewAutomaticDimension
                    return videoContentHeights[indexPath.row]
                    
                }
            }
        }
        return UITableViewAutomaticDimension
    }

}
extension DDHealthFeedDetailViewController: DDHealthHeaderDelegate {
    func tappedOnDoctorName(cell: DDHealthFeedDetailHeaderTVCell) {
        self.performSegue(withIdentifier: "doctorProfileId", sender: nil)
    }
    func videoFrameFinished(_ webView: UIWebView) {
//        if webView.tag == 100 {
//            if (videoContentHeights[0] != 0.0 &&  videoContentHeights[0] >= webView.scrollView.contentSize.height)
//                //        if (cell.videoWebviewHeightConstraint.constant != 190 &&  cell.videoWebviewHeightConstraint.constant >= cell.videoWebview.scrollView.contentSize.height)
//
//            {
//                ABProgressIndicator.shared.hideAnimator()
//                // we already know height, no need to reload cell
//                return
//            }
//            // cell.videoWebviewHeightConstraint.constant = cell.videoWebview.scrollView.contentSize.height
//
//            videoContentHeights[0] = webView.scrollView.contentSize.height
//            self.tableView.reloadData(completion: { (isComplete : Bool) in
//            })
//        }
    }
}
extension DDHealthFeedDetailViewController: HealthFeedDetailDelegate {
    func afterFinishWebLoadingFinished(_ webView: UIWebView) {
        if webView.tag == 200 {
            if (contentHeights[0] != 0.0 &&  contentHeights[0] >= webView.scrollView.contentSize.height)
            {
                ABProgressIndicator.shared.hideAnimator()
                // we already know height, no need to reload cell
                return
            }
            contentHeights[0] = webView.scrollView.contentSize.height
            self.tableView.reloadData(completion: { (isComplete : Bool) in
                if isComplete {
                    self.fetchRelatedArticles()
                }
            })
        } else if webView.tag == 100 {
            if (videoContentHeights[0] != 0.0 &&  videoContentHeights[0] >= webView.scrollView.contentSize.height)
            {
                ABProgressIndicator.shared.hideAnimator()
                // we already know height, no need to reload cell
                return
            }
            videoContentHeights[0] = webView.scrollView.contentSize.height
            self.tableView.reloadData()
           // self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)


           // self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
        }
    }
}
extension UITableView {
    func reloadData(completion: @escaping (_ isComplete: Bool)->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .automatic)

           // self.reloadSections(IndexSet(integer: 2), with: .none)
            //self.reloadRows(at: [IndexPath(row: 0, section: 2)], with: .automatic)
        }) { (isComplete) in
          completion(isComplete)
        }
    }
}
extension String{
    func convertDateAndYear(dateStr:String!)->String {
        let dateFormat = PacoDate.formatterForLocal()
        //        dateFormat.dateFormat = "dd MMM yyyy"
        dateFormat?.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let datSt = dateStr {
            if  let date  = dateFormat?.date(from: datSt) {
                dateFormat?.dateFormat = "dd MMM YYYY, h:mm a"
                let theDate = dateFormat?.string(from: date)
                return theDate!
            }
        }
        return ""
    }
}
