
//
//  DDPlayerView.swift
//  DrDoxtro
//
//  Created by vinaykumar on 12/15/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class DDPlayerView: UIView {
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
           playerLayer.player = newValue
        }
    }
}
