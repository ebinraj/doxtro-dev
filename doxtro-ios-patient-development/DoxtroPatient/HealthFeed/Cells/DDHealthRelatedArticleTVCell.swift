//
//  DDHealthRelatedArticleTVCell.swift
//  Dr Doxtro
//
//  Created by vinaykumar on 3/15/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import UIKit

class DDHealthRelatedArticleTVCell: UITableViewCell {

    @IBOutlet weak var coverPicImageView: UIImageView!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleSubTitleLabel: UILabel!
    @IBOutlet weak var playerThumbnailView: DDCustomView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
