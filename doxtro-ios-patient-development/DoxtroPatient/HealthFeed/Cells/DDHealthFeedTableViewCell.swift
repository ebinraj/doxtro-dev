//
//  DDHealthFeedTableViewCell.swift
//  DrDoxtro
//
//  Created by vinaykumar on 12/14/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit

class DDHealthFeedTableViewCell: UITableViewCell {
    @IBOutlet weak var bannerImagePreview: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleDescription: UILabel!
    @IBOutlet weak var healthButton: DDHighlightedButton!
    @IBOutlet weak var likedButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var playerView: DDPlayerView!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var playerButton: UIButton!
    @IBOutlet weak var playerThumbnailOverlayView: DDCustomView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
