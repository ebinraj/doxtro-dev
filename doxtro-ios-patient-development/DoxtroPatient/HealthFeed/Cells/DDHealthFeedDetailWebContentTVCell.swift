//
//  DDHealthFeedDetailWebContentTVCell.swift
//  Dr Doxtro
//
//  Created by vinaykumar on 3/2/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import UIKit
protocol HealthFeedDetailDelegate {
    func afterFinishWebLoadingFinished(_ webview: UIWebView)
}
class DDHealthFeedDetailWebContentTVCell: UITableViewCell, UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var coverImageView: UIImageView!
    let recognizer = UITapGestureRecognizer()
     var showFullScreen : ((_ image : UIImageView) -> Void)?
    var delegate: HealthFeedDetailDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        if coverImageView != nil{
        //sets the user interaction to true, so we can actually track when the image has been tapped
        coverImageView.isUserInteractionEnabled = true
        //this is where we add the target, since our method to track the taps is in this class
        //we can just type "self", and then put our method name in quotes for the action parameter
        recognizer.addTarget(self, action: "profileImageHasBeenTapped")
        //finally, this is where we add the gesture recognizer, so it actually functions correctly
        coverImageView.addGestureRecognizer(recognizer)
        }
    
        // Initialization code
    }
    func profileImageHasBeenTapped(){
        self.showFullScreen!(coverImageView)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DXUtility.shared.removeActivityIndicatorView()
        self.delegate?.afterFinishWebLoadingFinished(webView)
    }
}
