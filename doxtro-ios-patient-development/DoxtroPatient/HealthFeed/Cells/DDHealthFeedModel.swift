//
//  DDHealthFeedModel.swift
//  DrDoxtro
//
//  Created by vinaykumar on 12/14/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON

//Constants
let bannerImageUrl_key = "bannerImageUrl"
let title_key = "title"
let description_key = "description"
let count_key = "count"
let liked_key = "liked"
let like_key = "like"
let article_key = "article"
let categories_key = "categories"
let coverPic_key = "coverPic"
struct DDHealthFeedModel {
    var specializationName: String?
    var article: DDHealthFeedArticle?
    //var like: DDHealthFeedLike?
    var isLiked: Bool = false
    var _id: String?
    var categories: [JSON]?
    var coverPic: String?
    var createdAt: String?
    var doctor: DDHealthDoctor?
    var likes: Int = 0
    var mediaType: String?
    var thumbnails = [DDHealthThumbnail]()
    var title: String?
    var summaryText: String?
    var updatedAt: String?
    var videoId: String = ""
    init(dict: [String : JSON]) {
        self.specializationName = dict[specialization_key]?.string ?? ""
        if let articleDict = dict[article_key]?.dictionaryValue {
            self.article = DDHealthFeedArticle(dict: articleDict)
        }
        if let likeDic = dict[like_key]?.dictionaryValue {
            //self.like = DDHealthFeedLike(dict: likeDic)
        }
        self.isLiked = dict[like_key]?.bool ?? false
        self._id = dict[ID_key]?.string ?? ""
        if let categories = dict[categories_key]?.array {
           self.categories = categories
        }
        self.coverPic = dict["coverPic"]?.string ?? ""
        if let healthDoctor = dict["doctor"]?.dictionaryValue {
          self.doctor = DDHealthDoctor(dict: healthDoctor)
        }
        if let likesCount = dict["likes"]?.int  {
            self.likes = likesCount
        }
        if let mediaType = dict["mediaType"]?.string {
            self.mediaType = mediaType
        }
        if let thumbNailArray = dict["thumbnails"]?.array {
            for thumbnail in thumbNailArray {
                self.thumbnails.append(DDHealthThumbnail(dict: thumbnail.dictionaryValue))
            }
        }
        self.createdAt = dict["createdAt"]?.string ?? ""
        self.title = dict["title"]?.string ?? ""
        self.summaryText = dict["summaryText"]?.string ?? ""
        self.updatedAt = dict["updatedAt"]?.string ?? ""
        self.videoId = dict["videoId"]?.string ?? ""
    }
}
struct DDHealthFeedArticle {
    var type: String?
    var bannerImageUrl : String?
    var url : String?
    var title : String?
    var descriptionText : String?
    init(dict: [String : JSON]) {
      self.type = dict[type_key]?.string ?? ""
        self.bannerImageUrl = dict[bannerImageUrl_key]?.string ?? ""
        self.url = dict[url_key]?.string ?? ""
        self.title = dict[title_key]?.string ?? ""
        self.descriptionText = dict[description_key]?.string ?? ""
    }
}
let url_key = "url"
let type_key = "type"
struct DDHealthFeedLike {
    var count: Int?
    var isLiked: Bool?
    init(dict: [String : JSON]) {
       self.count = dict[count_key]?.int ?? 0
        self.isLiked = dict[liked_key]?.bool ?? false
    }
}
struct DDHealthDoctor {
    var id: String?
    var category: [JSON]?
    var firstName: String?
    var profilePic: String?
    init(dict: [String : JSON]) {
        self.id = dict[ID_key]?.string ?? ""
        self.firstName = dict[firstName_key]?.string ?? ""
        self.profilePic = dict[profilePic_key]?.string ?? ""
        if let categories = dict["category"]?.array {
            self.category = categories
        }
    }
}
struct DDHealthThumbnail {
    var height: Int?
    var id : String?
    var isPrefered: Int?
    var scale: Int?
    var uri: String?
    var width: Int?
    init(dict: [String : JSON]) {
        self.height = dict["height"]?.int ?? 0
        self.id = dict[ID_key]?.string ?? ""
        self.isPrefered = dict["is_preferred"]?.int ?? 0
        self.scale = dict["scale"]?.int ?? 0
        self.uri = dict["uri"]?.string ?? ""
        self.width = dict["width"]?.int ?? 0
    }
}
let specialization_key  = "specialization"
let profilePic_key  = "profilePic"
let category_key = "category"

