//
//  DDHealthFeedDetailHeaderTVCell.swift
//  Dr Doxtro
//
//  Created by vinaykumar on 3/2/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import UIKit
protocol DDHealthHeaderDelegate {
    func tappedOnDoctorName(cell: DDHealthFeedDetailHeaderTVCell)
}


class DDHealthFeedDetailHeaderTVCell: UITableViewCell {

    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var categoryView: DDCustomView!
    @IBOutlet weak var byLabel: UILabel!
    @IBOutlet weak var docNameLabel: UILabel!
    @IBOutlet weak var postedDateLabel: UILabel!
     @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var docSpecilaizationLabel: UILabel!
    var delegate: DDHealthHeaderDelegate?
     @IBOutlet weak var profilePicImageView: DDHighlightedButton!
    
    func docNameTapRecognizer(_ sender: UITapGestureRecognizer) {
        
       self.delegate?.tappedOnDoctorName(cell: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DDHealthFeedDetailHeaderTVCell.docNameTapRecognizer(_:)))
            
        docNameLabel.isUserInteractionEnabled = true
        
        docNameLabel.addGestureRecognizer(tapGesture)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
