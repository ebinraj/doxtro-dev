//
//  DDHealthFeedViewController.swift
//  DrDoxtro
//
//  Created by vinaykumar on 12/14/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import FirebaseDatabase
import NPSegmentedControl
import TSMessages
let LOAD_HEALTH_FEED_KEY = "loadHelathFeed"
class DDHealthFeedViewController: UIViewController {
    @IBOutlet weak var healthFeedTableView: UITableView!
    //noSearchResultFoundlabel
    @IBOutlet weak var noSearchResultFoundlabel: UILabel!
    @IBOutlet weak var segmentControl: NPSegmentedControl!
    var feedsArray = [DDHealthFeedModel]()
    var limit: Int = 10
    var loadMore: Bool = false
    var recentSkip: Int = 0
    var popularSkip: Int = 0
    var spinner:UIActivityIndicatorView?
    var recentList = [DDHealthFeedModel]()
    var popularList = [DDHealthFeedModel]()
    lazy var articlesRef: DatabaseReference = Database.database().reference().child("HealthFeed")
    var articlesRefHandle: DatabaseHandle?
    @IBOutlet weak var searchBar: UISearchBar!
    var likesArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.healthFeedTableView.rowHeight = UITableViewAutomaticDimension
        self.healthFeedTableView.estimatedRowHeight = 250
        //parseFeedJson()
         loadHealthFeed()
        self.healthFeedTableView.tableFooterView = UIView()
        setUpCustomSegmentControl()
        searchBar.layer.borderWidth = 0
        searchBar.layer.borderColor = UIColor.clear.cgColor
        searchBar.backgroundImage = UIImage()
        searchBar.layer.cornerRadius = 20
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadHealthFeed), name: Notification.Name(LOAD_HEALTH_FEED_KEY), object: nil)
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
        articlesRef = Database.database().reference().child("HealthFeed")
        self.observeMessages()
    }
    private func observeMessages() {
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
            ABProgressIndicator.shared.showAnimator()
        articlesRefHandle = articlesRef.observe(.value, with: { (snapshot : DataSnapshot) in
            ABProgressIndicator.shared.hideAnimator()
            if let data = snapshot.value as? Dictionary<String, AnyObject> {
                if let value = data[userID] as? [String] {
                    self.likesArray = value
                } else {
                    self.likesArray.removeAll()
                }
                //self.likesArray = data
                self.healthFeedTableView.reloadData()
            }
        })
        //DXUtility.appLog("**")
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let refHandle = articlesRefHandle {
            articlesRef.removeObserver(withHandle: refHandle)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    func refreshListWhenSegmentSelected() {
        let details:[String : JSON]!
        self.recentSkip = 0
        self.popularSkip = 0
        if segmentControl.selectedIndex == 0 {
            details = ["listType" : "recent" , "limit" : JSON(self.limit), "skip" : JSON(self.recentSkip)]
            self.feedsArray = self.recentList
        } else {
            details = ["listType" : "popular", "limit" : JSON(self.limit), "skip" : JSON(self.popularSkip)]
            self.feedsArray = self.popularList
        }
       // ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.sharedManager.getAllHealthFeedList(details: details) { (json, error) in
          //  ABProgressIndicator.shared.hideAnimator()
            if let response = json {
                if let data = response["data"].array {
                    if data.count > self.feedsArray.count {
                        var tempRecentArray = [DDHealthFeedModel]()
                        var tempPopularArray = [DDHealthFeedModel]()
                        self.feedsArray.removeAll()
                        for (_, element) in data.enumerated() {
                            let dict = element.dictionaryValue
                            if self.segmentControl.selectedIndex == 0 {
                                tempRecentArray.append(DDHealthFeedModel(dict: dict))
                            } else {
                                tempPopularArray.append(DDHealthFeedModel(dict: dict))
                            }
                            if self.segmentControl.selectedIndex == 0 {
                                self.feedsArray = tempRecentArray
                                self.recentList = tempRecentArray
                            } else {
                                self.feedsArray = tempPopularArray
                                self.popularList = tempPopularArray
                            }
                        }
                        if data.count == 0 {
                            self.loadMore = false
                            self.removeSpinner()
                        } else {
                            self.loadMore = true
                        }
                    }
                    self.healthFeedTableView.reloadData()
                    if isFromDeepLink {
                        isFromDeepLink = false
                        let articleId = DXUtility.getStringDefaults(key: "articleIdFromDeepLink")
                        let healthModel = self.feedsArray.filter{ $0._id == articleId }.first
                        self.performSegue(withIdentifier: "feedDetailSegue", sender: healthModel)
                        //self.performSegue(withIdentifier: "feedDetailSegue", sender: healthModel?._id)
                        
                        // isFromDeepLink = false
                    }
                }
            }
            self.showNoSearchresultFoundWithMessage(message: "No Feeds found")
        }
    }
    func loadHealthFeed() {
            let details:[String : JSON]!
            self.recentSkip = 0
            self.popularSkip = 0
            if segmentControl.selectedIndex == 0 {
                details = ["listType" : "recent" , "limit" : JSON(self.limit), "skip" : JSON(self.recentSkip)]
                self.feedsArray = self.recentList
            } else {
                details = ["listType" : "popular", "limit" : JSON(self.limit), "skip" : JSON(self.popularSkip)]
                self.feedsArray = self.popularList
            }
        ABProgressIndicator.shared.showAnimator()
            DPWebServicesManager.sharedManager.getAllHealthFeedList(details: details) { (json, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let response = json {
                    if let data = response["data"].array {
                       // if data.count > self.feedsArray.count {
                            var tempRecentArray = [DDHealthFeedModel]()
                            var tempPopularArray = [DDHealthFeedModel]()
                            self.feedsArray.removeAll()
                            for (_, element) in data.enumerated() {
                                let dict = element.dictionaryValue
                                if self.segmentControl.selectedIndex == 0 {
                                    tempRecentArray.append(DDHealthFeedModel(dict: dict))
                                } else {
                                    tempPopularArray.append(DDHealthFeedModel(dict: dict))
                                }
                                if self.segmentControl.selectedIndex == 0 {
                                   self.feedsArray = tempRecentArray
                                    self.recentList = tempRecentArray
                                } else {
                                    self.feedsArray = tempPopularArray
                                    self.popularList = tempPopularArray
                                }
                            }
                            if data.count == 0 {
                                self.loadMore = false
                                self.removeSpinner()
                            } else {
                                self.loadMore = true
                            }
                     //   }
                        self.healthFeedTableView.reloadData()
                        if isFromDeepLink {
                            isFromDeepLink = false
                            let articleId = DXUtility.getStringDefaults(key: "articleIdFromDeepLink")
                            let healthModel = self.feedsArray.filter{ $0._id == articleId }.first
                            self.performSegue(withIdentifier: "feedDetailSegue", sender: healthModel)
                            //self.performSegue(withIdentifier: "feedDetailSegue", sender: healthModel?._id)
                            
                            // isFromDeepLink = false
                        }
                    }
                }
                self.showNoSearchresultFoundWithMessage(message: "No Feeds found")
            }
    }
    /*
     // MARK: - Navigation
     */
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "feedDetailSegue" {
            let destinationController = segue.destination as! DDHealthFeedDetailViewController
//            if let healthFeedModel = sender as? DDHealthFeedModel {
//                destinationController.healthModel = healthFeedModel
//            }
            if let healthModel = sender as? DDHealthFeedModel {
                if let id  = healthModel._id{
                destinationController.articleId = id
                }
                destinationController.likesCount = healthModel.likes
               // destinationController.isLiked = healthModel.isLiked
            }
        }
     }
    @IBAction func segmentTapped(_ sender: NPSegmentedControl) {
        if let searchText = self.searchBar.text {
            if searchText == "" {
                refreshListWhenSegmentSelected()
            } else {
                self.searchHealthFeed(searchText: searchText)
            }
        }
    }
    func likeAction(_ sender: UIButton) {
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
        var healthModel = feedsArray[sender.tag]
         // 3
//        healthModel.isLiked = !healthModel.isLiked
//        sender.setTitle(healthModel.isLiked == true ? "Liked" : "Like", for: .normal)
//        sender.setImage(healthModel.isLiked == true ? #imageLiteral(resourceName: "heart_green") : #imageLiteral(resourceName: "heart"), for: .normal)
//        feedsArray[sender.tag] = healthModel
        let isLike = healthModel.isLiked
        let details = ["articleId" : healthModel._id, "userId" : userID, "type" : (isLike == true ? "unlike" : "like")]
            ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.sharedManager.updateHealthFeedLike(details: details as [String : AnyObject]) { (json, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let response = json {
                if let _ = response[data_key].dictionary {
                    if !isLike {
                        self.likesArray.append(healthModel._id ?? "")
                    } else{
                        // Remove/filter item with value '_id'
                        self.likesArray = self.likesArray.filter { $0 != healthModel._id }
                    }
                    let itemRef = self.articlesRef.child(userID) // 1
                    let likesItem = self.likesArray as [Any]
                    itemRef.setValue(likesItem)
                    healthModel.isLiked = !healthModel.isLiked
                    sender.setTitle(healthModel.isLiked == true ? "Liked" : "Like", for: .normal)
                    sender.setImage(healthModel.isLiked == true ? #imageLiteral(resourceName: "heart_green") : #imageLiteral(resourceName: "heart"), for: .normal)
                    self.feedsArray[sender.tag] = healthModel
                } else {
                    if let er = error {
                        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: er.localizedDescription, type: .error)
                    }
                }
            }
        }
        }
       
    }
    func shareAction(_ sender: UIButton) {
        let healthModel = feedsArray[sender.tag]
        if let id = healthModel._id {
            ////           let url = "https://b7e39.app.goo.gl/?link=https://drdoxtro.com/articles/\(id)&isi=12345678&ibi=com.doxtro.doctor&ius=com.doxtro.doctor"
            //           let dynamicUrl = "https://doxtro.onelink.me"
            //          //  let url = "https://doxtro.onelink.me/AJle/af_dp=doxtro://articles/\(id)"
            //            let url = "http://doxtro.onelink.me/AJle?af_dp=com.doxtro.doctor://articles/\(id)"
            //
            //
            //
            //          //  http://doxtrodev.above-inc.net/
            //
            if let linkUrl = URL(string: "https://doxtro.com/?articleIdBy=\(id)") {
                let components = DynamicLinkComponents(link: linkUrl, domain: "ut6d4.app.goo.gl")
                let iOSParams = DynamicLinkIOSParameters(bundleID: "com.doxtro.app")
                iOSParams.customScheme = "com.doxtro.app"
                iOSParams.appStoreID = "1161686991"
                // iOSParams.fallbackURL = URL(string:"http://doxtro.com")
                components.iOSParameters = iOSParams
                
                let androidParams = DynamicLinkAndroidParameters(packageName: "com.doxtro.app")
                //  DynamicLinkAndroidParameters(packageName: "com.doxtro.*")
                // androidParams.fallbackURL = URL(string: "http://doxtro.com")
                components.androidParameters = androidParams
                
                
                // social tag params
                let socialParams = DynamicLinkSocialMetaTagParameters()
                socialParams.title = healthModel.title
                socialParams.descriptionText = healthModel.summaryText?.convertHtml().string ?? ""
                if healthModel.mediaType == "Image" {
                    if let coverPic = healthModel.coverPic {
                        socialParams.imageURL = URL(string: coverPic)
                    }
                } else {
                    let thumbNails = healthModel.thumbnails
                    if thumbNails.count > 0 {
                        if let healthFeedVideoThumbnail = thumbNails.first {
                            if let uri = healthFeedVideoThumbnail.uri {
                                if let url = URL(string: uri) {
                                    socialParams.imageURL = url
                                }
                            }
                        }
                    }
                }
                components.socialMetaTagParameters = socialParams
                
                let options = DynamicLinkComponentsOptions()
                options.pathLength = .unguessable
                components.options = options
                ABProgressIndicator.shared.showAnimator()
                components.shorten { (shortURL, warnings, error) in
                    // Handle shortURL.
                    ABProgressIndicator.shared.hideAnimator()
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                    let shortLink = shortURL
                    print(shortLink?.absoluteString ?? "")
                    let activityController = UIActivityViewController(activityItems: [shortLink?.absoluteString ?? ""], applicationActivities: nil)
                    DispatchQueue.main.async {
                        self.present(activityController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    func playVideo(_ sender: UIButton) {
        let healthModel = feedsArray[sender.tag]
        let cell = self.healthFeedTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! DDHealthFeedTableViewCell
        if let type = healthModel.article?.type {
            if type == "video" {
                cell.playerView.isHidden = false
                //  if let url = URL(string: "http://uapi-f1.picovico.com/v2.1/v/nMirP/ArwenUndomiel.mp4") {
                if  let videoPath =  Bundle.main.path(forResource: "Logo_Reveal_v4", ofType: "m4v", inDirectory: "Video") {
                    let avPlayer = AVPlayer(url: URL(fileURLWithPath: videoPath))
                    // let avPlayer = AVPlayer(url: url)
                    cell.playerView.playerLayer.backgroundColor = UIColor.green.cgColor
                    cell.playerView.playerLayer.player = avPlayer
                    cell.playerView.playerLayer.player?.play()
                    cell.playerView.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
                }
            }
        }
    }
}
extension DDHealthFeedViewController {
    fileprivate func parseFeedJson() {
        let feedJson = loadJson(filename: "HealthFeed")
        if let dict = feedJson?.dictionaryValue {
            if let data = dict[data_key]?.array {
                for (_, element) in data.enumerated() {
                    if let dict = element.dictionary {
                        let healthFeedModel = DDHealthFeedModel(dict: dict)
                        self.feedsArray.append(healthFeedModel)
                    }
                }
                self.healthFeedTableView.reloadData()
            }
        }
    }
    /**
     Load json file from bundle for temproray..
     */
    //TODO://Later this will be removed
    func loadJson(filename fileName: String) -> JSON? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dictionary = object as? [String: AnyObject] {
                    return JSON(dictionary)
                }
            } catch {
                print("Error!! Unable to parse  \(fileName).json")
            }
        }
        return nil
    }
    
    func setUpCustomSegmentControl() {
        let segmentElements = ["Recent", "Popular"]
        segmentControl.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
        //segmentControl.cursor = UIImageView(image: UIImage(named: "tabindicator.png"))
        segmentControl.unselectedFont = UIFont(name: "Asap-Regular", size: 16)
        segmentControl.selectedFont = UIFont(name: "Asap-Medium", size: 16)
        segmentControl.unselectedTextColor = UIColor.gray//UIColor(black: 1, alpha: 0.8)
        segmentControl.unselectedColor = UIColor.white
        segmentControl.selectedTextColor = greenColor//UIColor(red: 10/255, green: 137/255, blue: 169/255, alpha: 1)
        segmentControl.selectedColor = UIColor.clear
        let view = UIView(frame: CGRect(x: 0, y: (segmentControl.viewController()?.view.frame.height)!, width: ((segmentControl.viewController()?.view.frame.width)!/2 ), height: (4)))
        view.backgroundColor = greenColor//UIColor(red: 10/255, green: 137/255, blue: 169/255, alpha: 1)
        segmentControl.useUnderline(underlineColor: lightGrayColor)
        segmentControl.cursor = view
        segmentControl.cursorPosition = CursorPosition.Bottom
        segmentControl.setItems(items: segmentElements)
        segmentControl.setSelectedIndex(index: 0)
        segmentControl.backgroundColor = UIColor.clear
    }
    
}
extension DDHealthFeedViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedsArray.count
    }
   
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = self.healthFeedTableView.dequeueReusableCell(withIdentifier: "healthFeedCellId", for: indexPath) as! DDHealthFeedTableViewCell
        cell.bannerImagePreview.image = nil
        
    }
    fileprivate func configureCell(_ indexPath: IndexPath, _ cell: DDHealthFeedTableViewCell) {
        
        var healthFeedModel = feedsArray[indexPath.row]
        cell.articleTitle.text = healthFeedModel.title
        cell.articleDescription.text = healthFeedModel.summaryText?.convertHtml().string ?? ""
        let categoryString = (healthFeedModel.categories?.map{String(describing: $0)})?.joined(separator: ", ")
        
        cell.healthButton.setTitle(categoryString, for: .normal)
        cell.bannerImagePreview.image = nil
        let likesstring = String(describing: healthFeedModel.likes)
        cell.likesCountLabel.text = likesstring
        if healthFeedModel.likes <= 0{
         cell.likesCountLabel.isHidden = true
        }else{
         cell.likesCountLabel.isHidden = false
        }
        cell.likesCountLabel.text = String(describing: healthFeedModel.likes)
        if healthFeedModel.mediaType == "Image" {
            cell.playerThumbnailOverlayView.isHidden = true
            cell.bannerImagePreview.isHidden = false
            if let coverPicUrl = healthFeedModel.coverPic {
                if let url = URL(string: coverPicUrl) {
                    DispatchQueue.main.async {
                        cell.bannerImagePreview.af_setImage(withURL: url, placeholderImage: UIImage(named: "DefaultImage"))
                    }
                } else {
                    cell.bannerImagePreview.image =  #imageLiteral(resourceName: "DefaultImage")
                }
            }
        } else {
            cell.playerThumbnailOverlayView.isHidden = false
            let thumbNails = healthFeedModel.thumbnails
            if thumbNails.count > 0 {
                if let healthFeedVideoThumbnail = thumbNails.first {
                    if let uri = healthFeedVideoThumbnail.uri {
                        if let url = URL(string: uri) {
                            DispatchQueue.main.async {
                                cell.bannerImagePreview.af_setImage(withURL: url, placeholderImage: UIImage(named: "DefaultImage"))
                            }
                        }
                    }
                }
            }
        }
        if let articleId = healthFeedModel._id {
            if self.likesArray.contains(articleId) {
                healthFeedModel.isLiked = true
                cell.likesCountLabel.text = String(describing: healthFeedModel.likes + 1)
            }
        }
        let isLiked = healthFeedModel.isLiked
        cell.likedButton.setTitle(isLiked == true ? "Liked" : "Like", for: .normal)
        cell.likedButton.tag = indexPath.row
        cell.likedButton.setImage(isLiked == true ? #imageLiteral(resourceName: "heart_green") : #imageLiteral(resourceName: "heart"), for: .normal)
        //        cell.bannerImagePreview.setImage(healthFeedModel.article?.bannerImageUrl == "Feed1" ? #imageLiteral(resourceName: "Feed1") : #imageLiteral(resourceName: "Feed2"), for: .normal)
        cell.likedButton.addTarget(self, action: #selector(likeAction(_:)), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(shareAction(_:)), for: .touchUpInside)
        cell.bannerImagePreview.tag = indexPath.row
        if feedsArray.count > indexPath.row {
            feedsArray[indexPath.row] = healthFeedModel
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "healthFeedCellId", for: indexPath) as! DDHealthFeedTableViewCell
        cell.selectionStyle = .none
          if self.feedsArray.count > indexPath.row {
        configureCell(indexPath, cell)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let healthModel = feedsArray[indexPath.row]
        self.performSegue(withIdentifier: "feedDetailSegue", sender: healthModel)
//        self.performSegue(withIdentifier: "feedDetailSegue", sender: healthModel._id)

    }
}
extension DDHealthFeedViewController {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                  willDecelerate decelerate: Bool) {
        if !decelerate {
            checkHasScrolledToBottom(scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        checkHasScrolledToBottom(scrollView)
    }
    
    func checkHasScrolledToBottom(_ scrollView: UIScrollView) {
        let _ = scrollView.contentOffset.y + scrollView.frame.size.height
        if (scrollView.contentOffset.y == 0){
            //reach top
            // getRecentConsultation()
        }
            
        else if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            //if offsetY > contentHeight - scrollView.frame.size.height {//if bottomEdge >= scrollView.contentSize.height {
            // we are at the end
            if let rechable = DPWebServicesManager.sharedManager.reachability?.isReachable,rechable {
                if self.loadMore {
                    self.setupSpinner()
                    if self.segmentControl.selectedIndex == 0 {
                        self.recentSkip = self.recentSkip + self.limit
                    }else {
                        self.popularSkip = self.popularSkip + self.limit
                    }
                    loadMoreArticles()
                }else {
                    self.removeSpinner()
                }
            } else {
               // getRecentConsultationsFromModel()
            }
        }
    }
    func loadMoreArticles() {
        if let rechable = DPWebServicesManager.sharedManager.reachability?.isReachable,rechable {
            var listType = ""
            var skip = 0
            if segmentControl.selectedIndex == 0 {
                listType = "recent"
                skip = self.recentSkip
            } else {
                listType = "popular"
                skip = self.popularSkip
            }
            var details: [String: JSON]!
            if self.searchBar.text == "" {
              details = ["listType" : JSON(listType), "limit" : JSON(self.limit), "skip" : JSON(skip)]
            } else {
                details = ["listType" : JSON(listType), "limit" : JSON(self.limit), "skip" : JSON(skip), "search" : JSON(self.searchBar.text ?? "")]
            }
            DPWebServicesManager.sharedManager.getAllHealthFeedList(details: details) { (json, error) in
                if let response = json {
                    if let data = response[data_key].array {
                        for (_, element) in data.enumerated() {
                            let dict = element.dictionaryValue
                            if self.segmentControl.selectedIndex == 0 {
                                self.recentList.append(DDHealthFeedModel(dict: dict))
                            }else {
                                self.popularList.append(DDHealthFeedModel(dict: dict))
                            }
                            // self.feedsArray.append(DDHealthFeedModel(dict: dict))
                        }
                        if self.segmentControl.selectedIndex == 0 {
                            self.feedsArray = self.recentList
                        } else {
                            self.feedsArray = self.popularList
                        }

                        if data.count == 0 {
                            self.loadMore = false
                            self.removeSpinner()
                        } else {
                            self.loadMore = true
                            self.healthFeedTableView.reloadData()
                        }
                    }
                }
                self.showNoSearchresultFoundWithMessage(message: "No Feeds found")
            }
        } else {
            self.showNoSearchresultFoundWithMessage(message: "No Feeds found")
        }
    }
    func searchHealthFeed(searchText: String) {
            var listType = ""
            var skip = 0
            if segmentControl.selectedIndex == 0 {
                listType = "recent"
                skip = self.recentSkip
            } else {
                listType = "popular"
                skip = self.popularSkip
            }
            let details:[String : JSON] = ["listType" : JSON(listType), "limit" : JSON(self.limit), "skip" : JSON(skip), "search" : JSON(searchText)]
        
            DPWebServicesManager.sharedManager.getAllHealthFeedList(details: details) { (json, error) in
                
                if let response = json {
                    if let data = response[data_key].array {
                        self.feedsArray.removeAll()
                        for (_, element) in data.enumerated() {
                            let dict = element.dictionaryValue
                            self.feedsArray.append(DDHealthFeedModel(dict: dict))
                        }
                        if self.segmentControl.selectedIndex == 0 {
                            self.recentList = self.feedsArray
                        } else {
                            self.popularList = self.feedsArray
                        }
                    }
                }
                self.showNoSearchresultFoundWithMessage(message: "No search result found")
                self.healthFeedTableView.reloadData()
            }
    }
    func showNoSearchresultFoundWithMessage(message :String = ""){
        if feedsArray.count <= 0{
            self.noSearchResultFoundlabel.text = message
        }else{
            self.noSearchResultFoundlabel.text = ""
        }
    }
}
extension DDHealthFeedViewController {
    func setupSpinner(){
        self.spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: screenWidth - 40, width: 40, height:40))
        if let spinner = self.spinner {
            spinner.color = UIColor.darkGray
            //  spinner.center = CGPoint(x:(self.consultationTableView.tableFooterView?.bounds.size.width)! / 2, y:(self.consultationTableView.tableFooterView?.bounds.size.height)!/2)
            spinner.startAnimating()
            self.healthFeedTableView.tableFooterView = spinner
            self.healthFeedTableView.tableFooterView?.isHidden = false
            spinner.superview?.bringSubview(toFront: spinner)
            //spinner.hidesWhenStopped = true
        }
    }
    func removeSpinner() {
        self.spinner?.removeFromSuperview()
    }
}
extension DDHealthFeedViewController {
    //MARK UISearchBar Delegate Methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= 3 {
            if let rechable = DPWebServicesManager.sharedManager.reachability?.isReachable,rechable{
                self.searchHealthFeed(searchText: searchText)
            } else {
                self.feedsArray.removeAll()
                if searchText.count == 0 {
                    if self.segmentControl.selectedIndex == 0{
                        self.feedsArray = self.recentList
                    } else {
                        self.feedsArray = self.popularList
                    }
                } else {
                    var something = [DDHealthFeedModel]()
                    if self.segmentControl.selectedIndex == 0{
                        self.feedsArray = self.recentList
                    } else {
                        self.feedsArray = self.popularList
                    }
                    something = feedsArray
                    var temArr = [DDHealthFeedModel]()
                    
                    for (_, parentElement) in something.enumerated() {
                        
                        let healthFeed = (parentElement)
                        if let strArry = healthFeed.title {
                            let range = (strArry as NSString).range(of: searchText, options: .caseInsensitive)
                            if range.location != NSNotFound {
                                temArr.append(healthFeed)
                            }
                            if temArr.count > 0 {
                                self.feedsArray = temArr
                            }
                        }
                    }
                }
                self.healthFeedTableView.reloadData()
            }
        }else if searchText.count == 0 {
            loadHealthFeed()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
let data_key = "data"
let lightGrayColor = ColorMethod.hexStringToUIColor(hex: "d2d3d5")
extension String {
    
    var htmlToAttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch{
            return NSAttributedString()
        }
    }
}
