//
//  ViewController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/22/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import NPSegmentedControl
import TSMessages
import DropDown
let firstName_key   = "firstName"
protocol AuthenticateTVCellDelegate: NSObjectProtocol {
    func swipeAction(direction: UISwipeGestureRecognizerDirection)
}
protocol DDValidationErrorDelegate {
    func validateError(errorStr: String)
}
protocol DPOtpVerificationDelegate{
    func verifyOTP(with otp : String)
}
protocol DDAuthenticationDelegate {
    func submitSignIn(details: [String : AnyObject])
    func submitSignUp(details: [String : AnyObject])
}
import SwiftyJSON
class AuthenticateTVCell:UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var segmentControl: NPSegmentedControl!
    @IBOutlet weak var signUpPlaceHolderview: UIView!
    @IBOutlet weak var signInPlaceHolderview: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var signUpMobileNumTextField: UITextField!
    @IBOutlet weak var invitationCodeTextFeild: UITextField!
    @IBOutlet weak var signInMobileNumTextField: UITextField!
    @IBOutlet weak var signUpSubmitButton: UIButton!
    @IBOutlet weak var signInSubmitButton: UIButton!
    @IBOutlet weak var corporatePlanSwitch: UISwitch!
    @IBOutlet weak var employeeIdTxtFld: UITextField!
    @IBOutlet weak var employerNameTxtFld: UITextField!
    var companyId: String = ""
    var delegate:DDAuthenticationDelegate? = nil
    var validateDelegate: DDValidationErrorDelegate? = nil
    var swipeDelegate : AuthenticateTVCellDelegate? = nil
    
    let employeeIDKey = "employeeId"
    let companyNameKey = "companyName"
    let companyIDKey = "companyId"
    let dropDown = DropDown()
    
    fileprivate func setupDropDownList() {
        dropDown.anchorView = employerNameTxtFld
        dropDown.direction = .any
        DropDown.startListeningToKeyboard()
        dropDown.backgroundColor = .white
        if let viewHeight = dropDown.anchorView?.plainView.bounds.height {
            dropDown.topOffset = CGPoint(x: 0, y:-viewHeight)
        }
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            debugPrint("Selected item: \(item) at index: \(index)")
            self.employerNameTxtFld.text = item
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nameTextField.addBootomBorderLine(width: 1)
        self.emailTextField.addBootomBorderLine(width: 1)
        self.invitationCodeTextFeild.addBootomBorderLine(width: 1)
        self.signUpMobileNumTextField.addBootomBorderLine(width: 1)
        self.signInMobileNumTextField.addBootomBorderLine(width: 1)
        self.employeeIdTxtFld.addBootomBorderLine(width: 1)
        self.employerNameTxtFld.addBootomBorderLine(width: 1)
        self.signUpSubmitButton.layer.cornerRadius = 5.0
        self.signInSubmitButton.layer.cornerRadius = 5.0
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.addGestureRecognizer(swipeLeft)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setRefrealCodeValueForTextfield), name: Notification.Name(rawValue: INVITE_REFERAL_OBSERVER_KEY), object: nil)
        //DPWebServicesManager.shared.test()
        employerNameTxtFld.isHidden = !corporatePlanSwitch.isOn
        employeeIdTxtFld.isHidden = !corporatePlanSwitch.isOn
        employerNameTxtFld.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        setupDropDownList()
        fetchCompaniesList()
    }
    fileprivate func tapRecognizerOnEmployerName() {
        let employerNameTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(respondToTapGestureOnEmpolyerName(_:)))
        employerNameTapRecognizer.numberOfTapsRequired = 1
        employerNameTxtFld.addGestureRecognizer(employerNameTapRecognizer)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @objc func respondToTapGestureOnEmpolyerName(_ sender : UITapGestureRecognizer) {
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Tapped on Employer", type: .error)
    }
    func respondToSwipeGesture(gesture : UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            self.swipeDelegate?.swipeAction(direction: swipeGesture.direction)
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                segmentControl.setSelectedIndex(index: 0)
            case UISwipeGestureRecognizerDirection.left:
                segmentControl.setSelectedIndex(index: 1)
                
            default:
                break
            }
        }
    }
    func setRefrealCodeValueForTextfield(){
       // if DPWebServicesManager.sharedManager.user == nil{
        if let value = UserDefaults.standard.value(forKey: INVITE_REFERAL_CODE_KEY) as? String{
            self.invitationCodeTextFeild.text = value
        }
        UserDefaults.standard.removeObject(forKey: INVITE_REFERAL_CODE_KEY)
        UserDefaults.standard.synchronize()
        
      //  }
    }
    /*
     Customize segment control
     */
    func setUpCustomSegmentControl() {
        let segmentElements = ["Sign in", "Sign up"]
        segmentControl.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
        //segmentControl.cursor = UIImageView(image: UIImage(named: "tabindicator.png"))
        segmentControl.unselectedFont = UIFont(name: "Asap-Regular", size: 16)
        segmentControl.selectedFont = UIFont(name: "Asap-Medium", size: 16)
        segmentControl.unselectedTextColor = UIColor.gray//UIColor(black: 1, alpha: 0.8)
        segmentControl.unselectedColor = UIColor.white
        segmentControl.selectedTextColor = UIColor().greenDoxtroCgColor()
        segmentControl.selectedColor = UIColor.clear
       let view = UIView(frame: CGRect(x: 0, y: (segmentControl.viewController()?.view.frame.height)!, width: ((segmentControl.viewController()?.view.frame.width)!/2 ), height: (4)))
        view.backgroundColor = UIColor().greenDoxtroCgColor()
        segmentControl.addBootomBorderLine(width: 1)
        segmentControl.cursor = view
        segmentControl.cursorPosition = CursorPosition.Bottom
        segmentControl.setItems(items: segmentElements)
        segmentControl.backgroundColor = UIColor.clear
        segmentControl.setSelectedIndex(index: 1)
        signInPlaceHolderview.isHidden = true
        signUpPlaceHolderview.isHidden = false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    @IBAction func submitSignBtnActions(_ sender: Any) {
        //performSegue(withIdentifier: OTP_SEGUE, sender: nil)
        if self.signInMobileNumTextField.text?.count == 0 {
            //TODO: -TODO show Alert for empty mobile number
           TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter Mobile Number", type: .error)
        } else {
            ABProgressIndicator.shared.showAnimator()
            let mobileDetails = [mobile_key : self.signInMobileNumTextField.text!] as [String : AnyObject]
            
            self.delegate?.submitSignIn(details: mobileDetails)
            self.signInMobileNumTextField.text = ""
        }
    }
    @IBAction func submitSignUpBtnActions(_ sender: Any) {
        if self.nameTextField.text?.count == 0 {
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter your name Number", type: .error)
        } else if self.emailTextField.text?.count == 0 {
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter your email", type: .error)
        } else if signUpMobileNumTextField.text?.count == 0 {
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter your mobile number", type: .error)
        }
        else if employeeIdTxtFld.text?.count == 0 && corporatePlanSwitch.isOn {
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter Employee ID", type: .error)
        }else if employerNameTxtFld.text?.count == 0 && corporatePlanSwitch.isOn {
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter Employer name", type: .error)
        }
        else {
            if DXUtility.shared.isValidEmail(testStr: self.emailTextField.text!) {
                var details = [String : Any]()
                if corporatePlanSwitch.isOn == true {
                    let filterArray = filterCompanyName((self.employerNameTxtFld.text!))
                    if filterArray.count == 0 {
                        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Select valid company name", type: .error)
                        return
                    } else {//get one item
                       let companyInfo = filterArray[0]
                        self.companyId = companyInfo.companyId
                    }
                    details = [mobile_key: signUpMobileNumTextField.text!, firstName_key : nameTextField.text!, emailId_key : emailTextField.text!, companyIDKey : self.companyId, companyNameKey : self.employerNameTxtFld.text!, employeeIDKey : self.employeeIdTxtFld.text!, "isCorporate": true]
                } else {
                    details = [mobile_key: signUpMobileNumTextField.text!, firstName_key : nameTextField.text!, emailId_key : emailTextField.text!, "isCorporate" : false]
                }
                
                if invitationCodeTextFeild.text?.count != 0,let text = invitationCodeTextFeild.text{
                    details["referralCode"] = text
                }
                self.delegate?.submitSignUp(details: details as [String : AnyObject] )
            } else {
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter valid email address", type: .error)
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case signUpMobileNumTextField,signInMobileNumTextField:
            return isValidUPtolength(withTextField: textField, length: 10, range: range, string: string)
        case nameTextField:
            return isValidUPtolength(withTextField: textField, length: 30, range: range, string: string)
        default:
            return true
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == employerNameTxtFld {
          /*
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let companyListController = storyBoard.instantiateViewController(withIdentifier: "companyListid")as! DPCompanyListViewController
            companyListController.delegate = self
            
            UIApplication.shared.keyWindow?.rootViewController?.present(companyListController, animated: true, completion: nil)
            */
          //  return false
        }
        return true
    }
    func isValidUPtolength(withTextField : UITextField,length : Int,range: NSRange,string : String)->Bool{
        let currentString: NSString = NSString(string: withTextField.text!)
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= length
    }
    
    @IBAction func didSwitchToCorporatePlan(_ sender: UISwitch) {
        employerNameTxtFld.isHidden = !sender.isOn
        employeeIdTxtFld.isHidden = !sender.isOn
    }
    @IBAction func didTapOnHelpOfCorporatePlan(_ sender: Any) {
        let message = "Organizations and Institutions sign up with Doxtro to cover online doctor access for their employees. Please check with your HR or Admin if you are eligible for the subscription plan."
        UIAlertController.showAlertWithMessage("", title: message)
    }
    var companyList: [CompanyInformation]?
    func fetchCompaniesList() {
        DPWebServicesManager.sharedManager.getCompanyList { (json, error) in
            if error == nil {
                if let response = json {
                    if let data = response["data"].array {
                        self.companyList = data.map({ return CompanyInformation($0.dictionaryValue)})
                        self.companyList = self.companyList?.filter {
                            $0.allowSignup == "true"
                        }
                    }
                }
            }else {
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: error?.localizedDescription, type: .error)
            }
        }
    }

    func filterCompanyName(_ string: String)->[CompanyInformation] {
        var filterArray =  [CompanyInformation]()
        guard let companyList = self.companyList else{
          return [CompanyInformation]()
        }
        filterArray = companyList.filter({
            $0.companyName.lowercased().contains(string.lowercased())
        })
        return filterArray
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        //your code
        if let textFieldText = textField.text {
            if textFieldText.count >= 3 {
               debugPrint(filterCompanyName(textFieldText).count)
                let companyInfo = filterCompanyName(textFieldText)
                dropDown.dataSource = companyInfo.map({return ($0.companyName)})
                if companyInfo.count == 0 {
                    dropDown.hide()
                }else {
                    dropDown.show()
                }
            } else {
              dropDown.hide()
            }
        }
    }
}
class ViewController: UIViewController, AuthenticateTVCellDelegate {
    @IBOutlet weak var tableView: UITableView!
    var globalCell:AuthenticateTVCell!
    var mobileNumber : String?
   // var isCheckBoxCheck:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func checkBoxTap(_ sender: Any) {
       /* let button = sender as! UIButton
        if !isCheckBoxCheck {
            button.setImage(UIImage(named: "checkedcheckbox"), for: .normal)
            isCheckBoxCheck = true
        }else {
            button.setImage(UIImage(named: "uncheckedcheckbox"), for: .normal)
            isCheckBoxCheck = false
        }*/
    }
      @IBAction func termsAndConditionTap(_ sender: Any) {
        let button = sender as! UIButton
        switch button.tag {
        case 0:
            if let url = NSURL(string: "http://doxtro.com/termsandprivacy") {
               loadtermsViewController(url: "http://doxtro.com/termsandprivacy")
            }
            break
        case 1:
            if let url = NSURL(string: "http://doxtro.com/termsandprivacy") {
                loadtermsViewController(url: "http://doxtro.com/termsandprivacy")
            }
            break
        default:
            break
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func swipeAction(direction: UISwipeGestureRecognizerDirection) {
        switch direction {
        case UISwipeGestureRecognizerDirection.right:
            print("Swiped right")
            self.globalCell.signInPlaceHolderview.isHidden = false
            self.globalCell.signUpPlaceHolderview.isHidden = true
            
        case UISwipeGestureRecognizerDirection.left:
            self.globalCell.signInPlaceHolderview.isHidden = true
            self.globalCell.signUpPlaceHolderview.isHidden = false
            
        default:
            break
        }
    }
    /*
     Selected segment control
     */
    func segmentValueChanged(_ sender: Any) {
        let segment = sender as! NPSegmentedControl
        //debugPrint(segment.selectedIndex)
        if segment.tag == segment.selectedIndex {
            self.globalCell.signInPlaceHolderview.isHidden = false
            self.globalCell.signUpPlaceHolderview.isHidden = true
        } else {
            self.globalCell.signInPlaceHolderview.isHidden = true
            self.globalCell.signUpPlaceHolderview.isHidden = false
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == OTP_Segue {
            if let destinationController = segue.destination as? DPOtpController,let userId = sender as? String{
                destinationController.userID = userId
                destinationController.mobileNumber = self.mobileNumber
            }
        }
    }
}
extension ViewController : UITableViewDataSource, UITableViewDelegate{
    //MARK: - UITableView datasource & delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AuthenticateTVCell
        cell.setUpCustomSegmentControl()
        cell.delegate = self
        cell.nameTextField.addLeftImageInTextFields("insaan")
        cell.emailTextField.addLeftImageInTextFields("email")
        cell.invitationCodeTextFeild.addLeftImageInTextFields("Refer and earn")
        cell.signInMobileNumTextField.addLeftImageInTextFields("mobile")
        cell.signUpMobileNumTextField.addLeftImageInTextFields("mobile")
        self.globalCell = cell
        cell.segmentControl.addTarget(self, action: #selector(ViewController.segmentValueChanged(_:)), for: .valueChanged)
        cell.selectionStyle = .none
        return cell
    }
}
extension ViewController : DDAuthenticationDelegate{
    func getallSpecializaion(details : [String : AnyObject]){
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
        let prams = ["patienId" :userID ]
            let request = GetAllSpecialization(params: prams as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
               // debugPrint(response)
                if let resposne  = response{
                    if let specializationArray = resposne["data"].array{
                        var values = [String : AnyObject]()
                        values["Name"] = details[firstName_key] as AnyObject
                        values["Email"] = details[emailId_key] as AnyObject
                        values["Phone"] = details[mobile_key] as AnyObject
                        values["specialization"] = specializationArray as AnyObject
                        DPAppEventLog.shared.logEventName(EVENT_COMPLETED_SIGNUP, values: values)
                        CleverTap.sharedInstance()?.onUserLogin(values)
                    }
                }
               
                
            })
        }
    }
    func submitSignUp(details: [String : AnyObject]) {
        /*if !isCheckBoxCheck {
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please accept terms and conditions & privacy policy", type: .error)
            return
        }*/
        ABProgressIndicator.shared.showAnimator()
        let signUpRequest = SignupRequest(params: details)
        self.mobileNumber = details[mobile_key] as? String
        DPWebServicesManager.getResponse(with: signUpRequest) { (response, error) in
            self.getallSpecializaion(details: details)
            self.setUPOTPController(with: response,error: error)
        }
    }
    func submitSignIn(details: [String : AnyObject]) {
        let loginRequest = LoginRequest(params: details)
        self.mobileNumber = details[mobile_key] as? String
        DPWebServicesManager.getResponse(with: loginRequest) { (response, error) in
            self.setUPOTPController(with: response,error: error)
        }
    }
    func setUPOTPController(with response : JSON?,error : Error?){
        DispatchQueue.main.async {
            ABProgressIndicator.shared.hideAnimator()
        }
        if error == nil,let data = response?["data"].dictionary,let userId = data["_id"]?.stringValue{
            self.performSegue(withIdentifier: OTP_SEGUE, sender: userId)
        }else{
            if let message = response?[MESSAGE_KEY].string{
               TSMessage.showNotification(in: UIApplication.shared.keyWindow?.rootViewController, title: "Login Error", subtitle: message, type: .error)
            }
        }
    }
}
extension AuthenticateTVCell: DPCompanyListProtocol {
    func didSelectCompanyName(companyInfo: CompanyInformation) {
        self.employerNameTxtFld.text = companyInfo.companyName
        self.companyId = companyInfo.companyId
    }
}
