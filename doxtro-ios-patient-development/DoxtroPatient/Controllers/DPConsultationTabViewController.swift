//
//  DPConsultationViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Firebase
import FirebaseDatabase
class ConsultationTabViewController: UIViewController {
    var recentConsulatationobj : DpConsultationDetails?
    var recentConsulatationArrray : [DpConsultationDetails] = []
    lazy var channelRef: DatabaseReference = Database.database().reference().child(FIREBASE_CONSULTATION_DBKEY)
    var channelRefHandle: DatabaseHandle?
    var isNewDataLoading : Bool = false
    //var consultationIdCheckArray : [String] = []
    @IBOutlet weak var noRecordFoundLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       // getRecentConsultation(isShow: true)
        loadConsultations()
         self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        self.title = "Consultations"
        tableView.register(UINib(nibName: "DPAllConsultationTabCell", bundle: nil), forCellReuseIdentifier: "allConsultationCell")
        tableView.separatorStyle = .none
        self.automaticallyAdjustsScrollViewInsets = false
        NotificationCenter.default.addObserver(self, selector: #selector(ConsultationTabViewController.refreshConsultationList), name: NSNotification.Name(rawValue: "RefreshAllConsultationList"), object: nil)
    }
    func refreshConsultationList(){
        loadConsultations()
        //getRecentConsultation(isShow: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        if self.recentConsulatationArrray.count > 0{
            self.noRecordFoundLabel.isHidden = true
        }else{
            self.noRecordFoundLabel.isHidden = false
        }
    }
    func getRecentConsultation(isShow : Bool){
        DPWebServicesManager.sharedManager.getRecentConsultations(isShow: isShow, completationHandler: { (error, response) in
            if error == nil,let consultationArray = response{
                guard consultationArray.count > 0 else{
                    self.noRecordFoundLabel.isHidden = false
                    return
                }
                 self.noRecordFoundLabel.isHidden = true
                self.recentConsulatationArrray = consultationArray
                self.tableView.reloadData()
            }
        })
    }
    @IBOutlet weak var tableView: UITableView!
    func loadConsultations(){
          self.recentConsulatationArrray = DPWebServicesManager.sharedManager.recentConsulatationArrray
        if self.recentConsulatationArrray.count > 0{
            self.noRecordFoundLabel.isHidden = true
        }else{
            self.noRecordFoundLabel.isHidden = false
        }
          self.tableView.reloadData()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showConsultViewController"{
            if  let destinatioVC = segue.destination as? ConsultationViewController{
                if self.recentConsulatationobj?.status == .Ongoing{
                   
                }else{
                    
                }
                destinatioVC.consulatationInfo = self.recentConsulatationobj
                DPWebServicesManager.sharedManager.saveConsultationInfo(with: self.recentConsulatationobj!)
            }
            
        }
    }
}
extension ConsultationTabViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.recentConsulatationobj = self.recentConsulatationArrray[indexPath.row]
        if let recentConsultation = self.recentConsulatationobj?.relativeObj{
            self.tableView.allowsSelection = true
            let cell = tableView.dequeueReusableCell(withIdentifier: "allConsultationCell") as? AllConsultCell
            cell?.nameLabel.text = self.recentConsulatationobj?.specializationCategory
            cell?.relationAgeLabel.text = (self.recentConsulatationobj?.consultingFor)! + ", " + recentConsultation.age!
            cell?.concernLabel.text = self.recentConsulatationobj?.note
            cell?.unreadMsgCountView.isHidden = true
            cell?.unreadMsgCountView.backgroundColor = UIColor.clear
            if let type = self.recentConsulatationobj?.consultationType{
              cell?.imageConsultationType.image = type.rawValue.imageForRelation(with: "")
            }
            if let status = self.recentConsulatationobj?.status{
                switch status {
                case .Waiting:
                    setConsultationLabelViewWithColor(cell: cell, color:UIColor().yellowDoxtroCgColor() , With:ConsultationStatus.Waiting.rawValue )
                    cell?.unreadMsgCountView.isHidden = true
                    break
                case .New:
                    setConsultationLabelViewWithColor(cell: cell, color:UIColor.red , With:ConsultationStatus.New.rawValue)
                     cell?.unreadMsgCountView.isHidden = true
                    break
                case .Ongoing:
                    setConsultationLabelViewWithColor(cell: cell, color:UIColor(red: 110, green: 160, blue: 56) , With:ConsultationStatus.Ongoing.rawValue)
                    if let recentConsulataion =  self.recentConsulatationobj?.consultationId{
                        UpdateChatUnreadCount(consulatationId:  recentConsulataion, cell: cell!)
                       // observeMessagesWhenChanged(cosultationId : recentConsulataion, cell: cell!)
                    }
                    cell?.nameLabel.text = self.recentConsulatationobj?.doctorObj?.firstName
                    cell?.unreadMsgCountView.backgroundColor = UIColor(red: 110, green: 160, blue: 56)
                    break
                case .Planned:
                    setConsultationLabelViewWithColor(cell: cell, color:UIColor().grayDoxtroCgColor() , With:ConsultationStatus.Planned.rawValue)
                    cell?.unreadMsgCountView.isHidden = true
                    cell?.nameLabel.text = self.recentConsulatationobj?.doctorObj?.firstName

                    break
                case .Completed:
                    setConsultationLabelViewWithColor(cell: cell, color:UIColor().greenDoxtroCgColor() , With:ConsultationStatus.Completed.rawValue)
                     cell?.unreadMsgCountView.isHidden = true
                     cell?.nameLabel.text = self.recentConsulatationobj?.doctorObj?.firstName
                    break
                case .Closed:
                    setConsultationLabelViewWithColor(cell: cell, color: UIColor.red , With:ConsultationStatus.Closed.rawValue)
                     cell?.unreadMsgCountView.isHidden = true
                     cell?.nameLabel.text = self.recentConsulatationobj?.doctorObj?.firstName
                    break
                default:
                    cell?.consulataionSatausView.isHidden = true
                    break
                }
            }
            if let gender = recentConsultation.gender{
                cell?.profileImageView.image = self.recentConsulatationobj?.consultingFor?.imageForRelation(with: gender)
            }
            
            cell?.timeLabel.text = self.textCellTopLabel(indexPath: indexPath)?.string
            if let timeLabel = cell?.timeLabel.text {
                if (timeLabel.contains("Today")) {
                    cell?.timeLabel.text = DXUtility.convertToTime(dateStr: self.recentConsulatationobj?.createdDate)//"Today"
                } else if timeLabel.contains("Yesterday") {
                    cell?.timeLabel.text = "Yesterday"
                }else {
                    cell?.timeLabel.text = DXUtility.convertToDMY(dateStr: self.recentConsulatationobj?.createdDate)//"Today"
                }
            }
           // cell?.timeLabel.text = DXUtility.convertDateIntoRequiredFormat(dateStr: self.recentConsulatationobj?.createdDate)
            return cell!
        }
        return UITableViewCell()
    }
    func setConsultationLabelViewWithColor(cell: AllConsultCell?,color : UIColor?,With text : String){
        cell?.consulataionSatausView.backgroundColor = color
        cell?.cconsulatationStatusLabel.text = text
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (recentConsulatationArrray.count)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.recentConsulatationobj = self.recentConsulatationArrray[indexPath.row]
        self.performSegue(withIdentifier: "showConsultViewController", sender: nil)
        DPAppEventLog.shared.logEventName(EVENT_CHAT_VISITORS, values: ["People going to chat from consultation page": true as AnyObject])
    }
    func UpdateChatUnreadCount(consulatationId : String,cell : AllConsultCell){
        let messageQuery = channelRef.child(consulatationId)
        // 2. We can use the observe method to listen for new
        // messages being written to the Firebase DB
        channelRefHandle = messageQuery.observe(.value, with: { (snapshot) -> Void in
            if let channelData = snapshot.value as? Dictionary<String, AnyObject>{ // 2
            var i = 0
            for (_,obj) in channelData{
                if let obj = JSON(obj).dictionary{
                    if let msgType = obj["msgType"]?.string,showChatCountAray.contains(msgType){
                        if let from = obj["from"]?.string{
                            if from == "Doctor" {
                                if let status = obj["status"]?.string{
                                    if status == "sent" || status == "Sent"{
                                        i = i+1
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if i > 0{
                cell.unreadMessageCountLabel.text = String(describing: i)
                cell.unreadMsgCountView.isHidden = false
            }else{
                cell.unreadMsgCountView.isHidden = true
            }
        }
            })
    }
    private func observeMessagesWhenChanged(cosultationId : String,cell : AllConsultCell) {
        let messageQuery = channelRef.child(cosultationId).queryLimited(toLast:20)
        // 2. We can use the observe method to listen for new
        // messages being written to the Firebase DB
        channelRefHandle = messageQuery.observe(.childChanged, with: { (snapshot) -> Void in
            let channelData = snapshot.value as! Dictionary<String, AnyObject> // 2
            var i = 0
                if let obj = JSON(channelData).dictionary{
                    if let msgType = obj["msgType"]?.string,msgType == "text"{
                        if let from = obj["from"]?.string{
                            if from == "Doctor" {
                                if let status = obj["status"]?.string{
                                    if status == "sent" || status == "Sent"{
                                        i = i+1
                                    }
                                }
                            }
                        }
                    }
                }
            if i > 0{
                cell.unreadMessageCountLabel.text = String(describing: i)
                cell.unreadMsgCountView.isHidden = false
            }else{
                cell.unreadMsgCountView.isHidden = true
            }
        })
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
extension ConsultationTabViewController
{
    func textCellTopLabel(indexPath: IndexPath) -> NSAttributedString? {
        self.recentConsulatationobj = self.recentConsulatationArrray[indexPath.row]
        if let createdDate = self.recentConsulatationobj?.createdDate {
            let messageDate = DXUtility.convertADate(dateStr: createdDate)
            let attributedString = JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: messageDate as Date?)
            return attributedString
        }
        return nil
    }
}
extension ConsultationTabViewController{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isNewDataLoading = false
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == tableView{
            if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height)
            {
                if !isNewDataLoading{
                    isNewDataLoading = true
                    loadRecentConsultationWithLimit(isShow: false, skip: self.recentConsulatationArrray.count, limit: 20)
                }
            }
        }
    }
    func loadRecentConsultationWithLimit(isShow : Bool,skip : Int,limit : Int){
        if let user = DPWebServicesManager.sharedManager.user{
            let params = ["patientId" : user.userId,"skip":skip,"limit":limit] as [String : Any]
            let request = GetRecentConsultationrequest(params: params as [String : AnyObject])
            if isShow{
                ABProgressIndicator.shared.showAnimator()
            }
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if isShow{
                    ABProgressIndicator.shared.hideAnimator()
                }
                if error == nil,let response = response{
                    if let data = response["data"].array{
                        for value in data{
                            let consulattionInfoObj = DpConsultationDetails(dict: value)
                            self.recentConsulatationArrray.append(consulattionInfoObj)
                        }
                        self.tableView.reloadData()
                    }
                }else{
                    
                }
            })
    }
    }
}
