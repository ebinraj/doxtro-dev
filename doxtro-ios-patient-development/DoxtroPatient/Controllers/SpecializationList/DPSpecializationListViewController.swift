//
//  DPSpecializationListViewController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/24/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
class DPSpecializationListTVCell:UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
class DPSpecializationListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var specializationTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.specializationTableView.estimatedRowHeight = 86
        self.specializationTableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func submitAction(_ sender: Any) {
     self.performSegue(withIdentifier: "basicDetailSegue", sender: sender)
    }
    //MARK: - UITableView datasource & delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.specializationTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DPSpecializationListTVCell
        if indexPath.row == 3 {
            cell.descriptionLabel.text = "Health concern - Loerm ipsm mei euis mei euismod percipitur liberavisse. Health concern - Loerm ipsm mei euis mei euismod percipitur liberavisse. Health concern - Loerm ipsm mei euis mei euismod percipitur liberavisse. Health concern - Loerm ipsm mei euis mei euismod percipitur liberavisse"
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
