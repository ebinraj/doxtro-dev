//
//  DPTermsAndConditionsViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 22/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController,UIWebViewDelegate {
    
    var navTitile = String()
    var url = String()
   
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet var navigationTitle: UILabel!
    
    @IBOutlet var termsWV: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationView()
        navigationTitle.text = navTitile
        ABProgressIndicator.shared.showAnimator()
        let requestURL = URL(string:url)
        let request = URLRequest(url: requestURL!)
        termsWV.loadRequest(request)
        termsWV.delegate = self
        
        // Do any additional setup after loading the view.
    }
    @IBAction func closeController(_ sender: Any) {
        self.dismissController()
    }
    @IBOutlet weak var closeController: UIButton!
    func setUpNavigationView(){
        navigationView.layer.shadowColor = UIColor.gray.cgColor
        navigationView.layer.shadowOffset = CGSize(width:0, height:0)
        navigationView.layer.shadowRadius = 2.0
        navigationView.layer.shadowOpacity = 0.8
        navigationView.layer.masksToBounds = false
    }
    //    @IBAction func backAction(_ sender: AnyObject) {
    //
    //        self.dismiss(animated: true, completion: nil)
    //
    //    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ABProgressIndicator.shared.hideAnimator()
        termsWV.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily =\"HelveticaNeue\"");
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        ABProgressIndicator.shared.hideAnimator()
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        ABProgressIndicator.shared.hideAnimator()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.backItem?.title = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let srt = URL(string: self.url)
        let url = URLRequest(url:srt!)
        termsWV.loadRequest(url)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension UIViewController{
    func loadtermsViewController(url : String,navTitle : String = "Terms and Conditions"){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main" , bundle:nil)
        //termsController
        let vc = storyBoard.instantiateViewController(withIdentifier: "termsController") as! TermsViewController
        vc.navTitile = navTitle
        vc.url = url
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //self.navigationController?.pushViewController(vc, animated: true)
         self.present(vc, animated: true , completion: nil)
    }
}
