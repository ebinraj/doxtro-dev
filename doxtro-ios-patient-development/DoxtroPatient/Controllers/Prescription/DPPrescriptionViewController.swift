//
//  DPPrescriptionViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 06/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import  PDFGenerator
class DoctorPreceptionViewController: UIViewController {
    var prescriptionRecord : PrescriptionRecord?
    var diagnosticRecord : DiagnosticRecord?
    var type = "prescription"
    var time : String?
    var doctorId: String!
    fileprivate var signPicUrl: String = ""
    @IBOutlet weak var orderMedicineButton: DDHighlightedButton!
    var docController : UIDocumentInteractionController!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        self.automaticallyAdjustsScrollViewInsets = false
        if type == "prescription"{
         addRightBarButtonItem()
        } else {
            self.orderMedicineButton.isHidden = true
        }
        getDoctorProfile()
    }
    fileprivate func getDoctorProfile() {
        guard let doctorId = self.doctorId else {return}
        DPWebServicesManager.sharedManager.getDoctorProfile(doctorId) { (responseJson, error) in
            if responseJson != nil {
               debugPrint(responseJson)
                if let dic = responseJson?.dictionary {
                    if let data = dic["data"]?.dictionary {
                        if let signPic = data["signPic"]?.string {
                            self.signPicUrl = signPic
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    func addRightBarButtonItem(){
        //  let button  = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(DoctorPreceptionViewController.downloadPresciption(_:)))
//        let button : UIBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DoctorPreceptionViewController.downloadPresciption(_:)))
        let downloadButton : UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "download"), style: .plain, target: self, action: #selector(downloadPresciption(_:)))
      //  let orderPrescriptionButton : UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "freshchatsupport"), style: .plain, target: self, action: #selector(orderPrescriptionOnChat(_:)))

        self.navigationItem.setRightBarButtonItems([downloadButton], animated: true)
    }
    func downloadPresciption(_ button:UIButton){
        if var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last{
            do {
                docURL = docURL.appendingPathComponent("Doxtro_\(type)_\(Date()).pdf")
                try PDFGenerator.generate(self.tableView, to: docURL)
                openPDFViewer(pdfPath: docURL)
            } catch let error {
                print(error)
            }
        }
    }
   
    func orderPrescriptionOnChat(_ sender: Any) {
        FreshChatController.sharedManager.setUserInfoToFreshChat()
        var message = ""
       // if !isPrescriptionSent {
            if let medicineObjects = prescriptionRecord?.medicationInfo {
                for (index, medicineObject) in medicineObjects.enumerated() {
                    let medicineName = medicineObject.name
                    var tableCount = 0
                    if let duration = medicineObject.duration {
                        tableCount = duration * medicineObject.doseTimeArrays.count
                    }
                    message =  message + "\(index + 1). \(medicineName) - \(tableCount) \n"
                }
            }
            message = "PRESCRIPTION: \n" + message
       // }
      //  let isPrescriptionSent = UserDefaults.standard.bool(forKey: "prescriptionSent")
        let alertController = UIAlertController(title: "SUPPORT", message: "Do you want to send this prescription?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "YES", style: .default) { (alertAction) in
            FreshChatController.sharedManager.sendMessage("prescription", message, tag: "order_doxtro_medicine")
        }
        let noAction = UIAlertAction(title: "NO", style: .default) { (alertAction) in
            FreshChatController.sharedManager.sendMessage("prescription", "", tag: "order_doxtro_medicine")
        }
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func orderMedicine(_ sender: Any) {
      orderPrescriptionOnChat(sender)
    }
    @IBAction func downloadPdfPrescription(_ sender: Any) {
        
        //        let priorBounds = tableView.bounds
        //        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height))
        //        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        //        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:self.view.frame.height)
        //        let pdfData = NSMutableData()
        //        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        //        var pageOriginY: CGFloat = 0
        //        while pageOriginY < fittedSize.height {
        //            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        //            UIGraphicsGetCurrentContext()!.saveGState()
        //            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        //            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
        //            UIGraphicsGetCurrentContext()!.restoreGState()
        //            pageOriginY += pdfPageBounds.size.height
        //        }
        //        UIGraphicsEndPDFContext()
        //        tableView.bounds = priorBounds
        //        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        //        docURL = docURL.appendingPathComponent("Test_1.pdf")
        //        pdfData.write(to: docURL as URL, atomically: true)
        //        var pdfData = NSMutableData()
        //        UIGraphicsBeginPDFContextToData(pdfData, CGRect(x: tableView.bounds.minX, y: tableView.bounds.minY, width: tableView.bounds.width, height: 1000), nil);
        //        UIGraphicsBeginPDFPage()
        //       let pdfContext = UIGraphicsGetCurrentContext()
        //        tableView.layer.render(in: pdfContext!)
        //        UIGraphicsEndPDFContext();
        //        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
        //            let documentsFileName = NSHomeDirectory() + "/" + "ayushTableView.pdf"
        //            debugPrint(documentsFileName)
        //            pdfData.write(toFile: documentsFileName, atomically: true)
    }
    func openPDFViewer(pdfPath : URL){
        self.docController = UIDocumentInteractionController(url: pdfPath)
        docController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
    }
}

extension DoctorPreceptionViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == "prescription" {
            return ((prescriptionRecord?.medicationInfo.count)! + 2)
        } else {
            return ((diagnosticRecord?.instructionInfo.count)! + 2)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(type == "prescription"){
            return getCellForPrescription(indexPath, tableView)
        } else{
            return getCellForDiagnostic(indexPath, tableView)
        }
    }
    fileprivate func getCellForPrescription(_ indexPath: IndexPath, _ tableView: UITableView) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as? PrescriptionHeaderCell
            if let prescriptionrecord = prescriptionRecord,let doctorProfile = prescriptionrecord.doctorProfile,let time  = self.time{
                cell?.DoctorName.text = doctorProfile.firstName
                cell?.qualificationLabel.text = doctorProfile.qualificationString
                cell?.timeLabel.text = "Date: " + DXUtility.convertDateAndTimeforPresription(dateStr: time)
                cell?.dunsLabel.text = "Reg. Number : " + doctorProfile.duns
            }
            if let relativeInfo = prescriptionRecord?.relativeInfo{
                if let name = relativeInfo.name,let age = relativeInfo.age,let gender = relativeInfo.gender{
                    var mainString = "Name: "
                    mainString = mainString + name + " "
                    mainString = mainString  + String(age) + ","
                    mainString = mainString + gender
                    cell?.patientInfoLabel.text = mainString
                }
            }
            return cell!
        }
        if indexPath.row == ((prescriptionRecord?.medicationInfo.count)! + 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footerCell", for: indexPath) as? PrescriptionFooterCell
            if let prescriptionrecord = prescriptionRecord,let doctorProfile = prescriptionrecord.doctorProfile,let _  = self.time{
                cell?.DoctorName.text = doctorProfile.firstName
                cell?.qualificationLabel.text = doctorProfile.qualificationString
                cell?.dunsLabel.text = "Reg. Number : " + doctorProfile.duns
                if let imageUrl = URL(string: self.signPicUrl){
                    cell?.signatureImageView.af_setImage(withURL: imageUrl, placeholderImage: UIImage(named: "imagePlaceholder"))
                }
                var specializationQualification : String = ""
                for (index,degree) in doctorProfile.category.enumerated(){
                    if index == 0{
                        specializationQualification = degree
                    }else{
                        specializationQualification = specializationQualification + " , " + degree
                    }
                }
                cell?.specializationDegree.text = specializationQualification
            }
            return cell!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "medicineCell", for: indexPath) as? PrescriptionMedicinCell
        if (prescriptionRecord?.medicationInfo.count)! > 0 {
            if let medicineObject = prescriptionRecord?.medicationInfo[indexPath.row - 1]{
                cell?.medicineName.text = medicineObject.name
                cell?.doseTime.text = medicineObject.doseTimeString
                if let duration = medicineObject.duration,let durationUnit = medicineObject.durationUnit{
                    cell?.maedicineDuration.text = "Duration : " + "\(duration) " + durationUnit
                }
                cell?.timeTakenLabel.text = "Time : " + medicineObject.foodWarning
                cell?.countLabel.text = ("\(indexPath.row).")
                cell?.instructionCell.text = medicineObject.instruction
            }
        }
        return cell!
    }
    
    fileprivate func getCellForDiagnostic(_ indexPath: IndexPath, _ tableView: UITableView) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as? PrescriptionHeaderCell
            if let diagnosticRecord = diagnosticRecord,let doctorProfile = diagnosticRecord.doctorProfile,let time  = self.time{
                cell?.DoctorName.text = doctorProfile.firstName
                cell?.qualificationLabel.text = doctorProfile.qualificationString
                cell?.timeLabel.text = "Date: " + DXUtility.convertDateAndTimeforPresription(dateStr: time)
                cell?.dunsLabel.text = "Reg. Number : " + doctorProfile.duns
            }
            cell?.timeLabel.isHidden = false;
            if let relativeInfo = diagnosticRecord?.relativeInfo{
                if let name = relativeInfo.name,let age = relativeInfo.age,let gender = relativeInfo.gender{
                    var mainString = "Name: "
                    mainString = mainString + name + " "
                    mainString = mainString  + String(age) + ","
                    mainString = mainString + gender
                    cell?.patientInfoLabel.text = mainString
                }
            }
            return cell!
        }
        if indexPath.row == ((diagnosticRecord?.instructionInfo.count)! + 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footerCell", for: indexPath) as? PrescriptionFooterCell
            if let diagnosticrecord = diagnosticRecord,let doctorProfile = diagnosticrecord.doctorProfile,let _  = self.time{
                cell?.DoctorName.text = doctorProfile.firstName
                cell?.qualificationLabel.text = doctorProfile.qualificationString
                cell?.dunsLabel.text = "Reg. Number : " + doctorProfile.duns
                if let imageUrl = URL(string: self.signPicUrl){
                     cell?.signatureImageView.af_setImage(withURL: imageUrl, placeholderImage: UIImage(named: "imagePlaceholder"))
                }
                var specializationQualification : String = ""
                for (index,degree) in doctorProfile.category.enumerated(){
                    if index == 0{
                        specializationQualification = degree
                    }else{
                        specializationQualification = specializationQualification + " , " + degree
                    }
                    
                }
                cell?.specializationDegree.text = specializationQualification
            }
            return cell!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "medicineCell", for: indexPath) as? PrescriptionMedicinCell
        if (diagnosticRecord?.instructionInfo.count)! > 0 {
            if let instructionObject = diagnosticRecord?.instructionInfo[indexPath.row - 1]{
                cell?.medicineName.text = instructionObject.name
                cell?.doseTime.text = "";//instructionObject.instruction
                cell?.maedicineDuration.text = "";//Duration : " + "\(duration) " + durationUnit
                cell?.timeTakenLabel.text = "";//Time : " + medicineObject.foodWarning!
                cell?.countLabel.text = ("\(indexPath.row).")
                cell?.instructionCell.text = instructionObject.instruction
                cell?.pillsImage.isHidden = true;
            }
        }
        return cell!
    }
}
