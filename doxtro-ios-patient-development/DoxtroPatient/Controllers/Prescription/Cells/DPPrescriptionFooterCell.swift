//
//  DPPrescriptionFooterCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 06/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class PrescriptionFooterCell : UITableViewCell{
    @IBOutlet weak var DoctorName: UILabel!
    @IBOutlet weak var qualificationLabel: UILabel!
    @IBOutlet weak var dunsLabel: UILabel!
    @IBOutlet weak var specializationDegree: UILabel!
    @IBOutlet weak var signatureImageView: UIImageView!
}
