//
//  DPPrescriptionHeaderCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 06/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class PrescriptionHeaderCell : UITableViewCell{
    @IBOutlet weak var DoctorName: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var patientInfoLabel: UILabel!
    @IBOutlet weak var qualificationLabel: UILabel!
    @IBOutlet weak var dunsLabel: UILabel!
}
