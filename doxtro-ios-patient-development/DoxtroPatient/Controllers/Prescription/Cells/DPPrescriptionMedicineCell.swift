//
//  DPPrescriptionMedicineCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 06/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class PrescriptionMedicinCell : UITableViewCell{
    @IBOutlet weak var medicineName: UILabel!
    @IBOutlet weak var maedicineDuration: UILabel!
    @IBOutlet weak var doseTime: UILabel!
    @IBOutlet weak var instructionCell: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var timeTakenLabel: UILabel!
    @IBOutlet weak var pillsImage: UIImageView!
}
