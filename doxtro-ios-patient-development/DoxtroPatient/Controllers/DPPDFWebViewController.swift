//
//  DPPDFWebViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 09/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class PDFWebViewController: UIViewController,UIWebViewDelegate {
    
    @IBAction func closeWebView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    var navTitile = String()
    var url = String()
    @IBOutlet var navigationTitle: UILabel!
    @IBOutlet var termsWV: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitle.text = navTitile
        self.termsWV.scalesPageToFit = true
      //  ABProgressIndicator.shared.showAnimator()
        let requestURL = URL(string:url)
        if let url = requestURL{
            let request = URLRequest(url:url)
            termsWV.loadRequest(request)
        }
        
        
        // Do any additional setup after loading the view.
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        ABProgressIndicator.shared.hideAnimator()
        //termsWV.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily =\"HelveticaNeue\"");
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        ABProgressIndicator.shared.hideAnimator()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.backItem?.title = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let srt = URL(string: self.url)
        if let srt = srt{
            let url = URLRequest(url:srt)
            termsWV.loadRequest(url)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
