//
//  DPFreshChatController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 03/03/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
class FreshChatController : NSObject{
    private override init() {
    //for singleton
    }
    static var sharedManager : FreshChatController = {
      let manager = FreshChatController()
        manager.freschatConfig = FreshchatConfig()
        return manager
    }()
    var freschatConfig : FreshchatConfig?
    func initFreshchatSDK() {
        let fchatConfig = FreshchatConfig.init(appID: FreshChatCredential.APP_ID, andAppKey: FreshChatCredential.APP_KEY)
        Freshchat.sharedInstance().initWith(fchatConfig)
    }
    //if we have to show  badge for unread count //call on did become active
    func fetchAndShowUnreadCount(){
        var freahchatUnreadCount = Int()
        Freshchat.sharedInstance().unreadCount { (unreadCount) in
            freahchatUnreadCount = unreadCount
        }
        UIApplication.shared.applicationIconBadgeNumber = freahchatUnreadCount;
    }
    func setConfugurartionofFreshChat(){
        freschatConfig?.gallerySelectionEnabled = true; // set NO to disable picture selection for messaging via gallery
        freschatConfig?.cameraCaptureEnabled = true; // set NO to disable picture selection for messaging via camera
        freschatConfig?.teamMemberInfoVisible = true; // set to NO to turn off showing an team member avatar. To customize the avatar shown, use the theme file
        freschatConfig?.showNotificationBanner = true; // set to NO if you don't want to show the in-app notification banner upon receiving a new message while the app is open
    }
    
}
//for conversation controller configuration
extension FreshChatController {
    func presentConversationController() {
        Freshchat.sharedInstance().showConversations(UIApplication.getTopViewController())
    }
    func presentConversationControllerWithFilter(){
        let options = ConversationOptions.init()
        let tags: [String] = ["all","paidUser"]
        options.filter(byTags: tags, withTitle: "Filter_View_Title")
        Freshchat.sharedInstance().showConversations(UIApplication.getTopViewController(), with: options)
    }
    func presentConversationControllerWithFilters(_ tags: [String], _ title: String) {
        let options = ConversationOptions.init()
        let tags: [String] = tags
        options.filter(byTags: tags, withTitle: title)
        Freshchat.sharedInstance().showConversations(UIApplication.getTopViewController(), with: options)
    }
    func sendMessage(_ messageType: String, _ message: String, tag: String) {
        if messageType == "prescription" {
            let freshChatMessagge = FreshchatMessage(message: message, andTag: tag)
            if !message.isEmpty {
                Freshchat.sharedInstance().send(freshChatMessagge)
            }
            FreshChatController.sharedManager.presentConversationControllerWithFilters([tag], "Order Doxtro Medicines")
            UserDefaults.standard.set(true, forKey: "prescriptionSent")
        }
    }
}
//for FAQ controller configuration
extension FreshChatController {
    func presentFAQ(sender: Any) {
        Freshchat.sharedInstance().showFAQs(UIApplication.getTopViewController())
    }
    func presentFAQWithFilter(){
        let options = FAQOptions.init()
        let tags: [String] = ["article_tag"]
        options.filter(byTags: tags, withTitle:"Filter_screen_title", andType :ARTICLE)
        Freshchat.sharedInstance().showFAQs(UIApplication.getTopViewController(), with: options)
        //some more properties to cutomize
        /*
         let options = FAQOptions.init()
         options.showFaqCategoriesAsGrid = true; // Set false to show faq in list view
         options.showContactUsOnAppBar = true; // Set false to hide contact us from app bar
         options.showContactUsOnFaqScreens = true; // Set false to hide contact us bottom banner view
         options.filterContactUs(byTags: ["feedback1"], withTitle: "Filtered_contactus_title")
 */
    }

}
extension FreshChatController{
    func setUserInfoToFreshChat(){
        if let userInfo = DPWebServicesManager.sharedManager.user{
            if let userId = userInfo.userId{
                //for fectching restoreID
                setExternalIDAndRestoreId(externalId: userId, RestoreId: nil)
            }
           // let freshChatUser = freshChatUser
            let user = FreshchatUser.sharedInstance()
            user?.firstName = userInfo.name
            user?.email = userInfo.emailId
            user?.phoneCountryCode = "91"
            user?.phoneNumber = userInfo.mobileNum
            Freshchat.sharedInstance().setUser(user)
            if let gender = userInfo.gender{
                 Freshchat.sharedInstance().setUserPropertyforKey("gender", withValue: userInfo.gender)
            }
        }
    }
    func setExternalIDAndRestoreId(externalId : String,RestoreId : String?){
     Freshchat.sharedInstance().identifyUser(withExternalID: externalId, restoreID: nil)
    }
}
//push notification configuration
extension FreshChatController{
    func registerPushNotification(deviceToken : Data){
        Freshchat.sharedInstance().setPushRegistrationToken(deviceToken)
    }
    func handleFreshChatPushNotification(userInfo :[AnyHashable : Any],application : UIApplication ){
        if Freshchat.sharedInstance().isFreshchatNotification(userInfo) {
            Freshchat.sharedInstance().handleRemoteNotification(userInfo, andAppstate: application.applicationState)
        }
    }
}
struct FreshChatCredential {
    //Production
    static let APP_ID = "ffa6dbfe-33f7-4595-bbcb-7b17a36d96f3"
    static let APP_KEY = "10a46ae4-80c0-491f-a71c-5e2efeae288f"
    
    //Development
    
//    static let APP_ID = "1702c5e1-d848-4730-bcca-324bdd336a37"
//    static let APP_KEY = "46593161-305e-49c7-9589-e2577f592b46"
    
}
//Production

