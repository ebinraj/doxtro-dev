//
//  DPCompanyListViewController.swift
//  DoxtroPatient
//
//  Created by Vinaykumar on 9/7/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import UIKit
import SwiftyJSON
import TSMessages
struct CompanyInformation {
    var companyId: String
    var createdAt: String
    var companyName: String
    var employeeStrength: Int
    var companyDescription: String
    var industryType: String
    var planType: String
    var dynamicLink: String
    var corporateType: String
    var groups: [Any]
    var allowSignup: String
    init(_ dict: [String : JSON]) {
        self.companyId = dict[ID_key]?.string ?? ""
        self.createdAt = dict["createdAt"]?.string ?? ""
        self.companyName = dict["companyName"]?.string ?? ""
        self.employeeStrength = dict["employeeStrength"]?.int ?? 0
        self.companyDescription = dict["companyDescription"]?.string ?? ""
        self.industryType = dict["industryType"]?.string ?? ""
        self.corporateType = dict["corporatePayType"]?.string ?? ""
        self.planType = dict["planType"]?.string ?? ""
        self.dynamicLink = dict["dynamicLink"]?.string ?? ""
        self.groups  = dict["groups"]?.arrayObject ?? []
        self.allowSignup = dict["allowSignup"]?.string ?? ""
    }
}
protocol DPCompanyListProtocol {
    func didSelectCompanyName(companyInfo: CompanyInformation)
}
class DPCompanyListViewController: UIViewController {
    var companyList = [CompanyInformation]()
    let cellId = "cellId"
    var delegate: DPCompanyListProtocol?
    @IBOutlet weak var companyListView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //companyList =
        self.companyListView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        self.fetchCompaniesList()
    }
    @IBAction func dismissController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func fetchCompaniesList() {
        DPWebServicesManager.sharedManager.getCompanyList { (json, error) in
            if error == nil {
                if let response = json {
                    if let data = response["data"].array {
                        self.companyList = data.map({ return CompanyInformation($0.dictionaryValue)})
                        DispatchQueue.main.async {
                            self.companyListView.reloadData()
                        }
                    }
                }
            }else {
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: error?.localizedDescription, type: .error)
            }
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DPCompanyListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companyList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = companyList[indexPath.row].companyName
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectCompanyName(companyInfo: companyList[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
}
