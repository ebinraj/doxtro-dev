//
//  DPOtpViewController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/23/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
class DPOtpTVCell:UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var ageTextFld: UITextField!
    @IBOutlet weak var genderMaleButton: UIButton!
    @IBOutlet weak var genderFemaleButton: UIButton!
    
    @IBOutlet weak var saveProfileButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.saveProfileButton.layer.cornerRadius = 10.0
        self.genderMaleButton.layer.borderColor = ColorMethod.hexStringToUIColor(hex: "86c3bf").cgColor
        self.genderMaleButton.layer.borderWidth = 1.0
        self.genderMaleButton.layer.cornerRadius = 15.0
        self.genderFemaleButton.layer.cornerRadius = 0.0
        self.ageTextFld.useUnderline(underlineColor: UIColor.gray)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
class DPOtpViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var globalCell:DPOtpTVCell!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     Select gender male Buttons
     */
    func didTapOnMaleButton(_ sender: Any) {
        self.globalCell.genderFemaleButton.layer.borderColor = UIColor.clear.cgColor
        self.globalCell.genderFemaleButton.layer.borderWidth = 0.0
        self.globalCell.genderFemaleButton.layer.cornerRadius = 0.0
        self.globalCell.genderMaleButton.layer.borderColor = ColorMethod.hexStringToUIColor(hex: "86c3bf").cgColor
        self.globalCell.genderMaleButton.layer.borderWidth = 1.0
        self.globalCell.genderMaleButton.layer.cornerRadius = 15.0

       /* UIView.animate(withDuration:0.25,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions.curveEaseOut,
                                   animations: {
        },
                                   completion: { finished in
        })*/
    }
    /*
     Select gender Female Buttons
     */
    func didTapOnFeMaleButton(_ sender: Any) {
        self.globalCell.genderMaleButton.layer.borderColor = UIColor.clear.cgColor
        self.globalCell.genderMaleButton.layer.borderWidth = 0.0
        self.globalCell.genderMaleButton.layer.cornerRadius = 0.0
        self.globalCell.genderFemaleButton.layer.borderColor = ColorMethod.hexStringToUIColor(hex: "86c3bf").cgColor
        self.globalCell.genderFemaleButton.layer.borderWidth = 1.0
        self.globalCell.genderFemaleButton.layer.cornerRadius = 15.0

       /* UIView.animate(withDuration:0.25,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: {
        },
                       completion: { finished in
        })
        */
      }
    /*
     Select Save profile Buttons
     */
    func saveProfile(_ sender:Any) {
      self.performSegue(withIdentifier: "tabBarSegue", sender: sender)
//        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "tabStoryboardId")
//        self.navigationController?.pushViewController(viewController!, animated: true)
    }


    //MARK: - UITableView datasource & delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DPOtpTVCell
        self.globalCell = cell
        cell.genderMaleButton.addTarget(self, action: #selector(didTapOnMaleButton(_:)), for: .touchUpInside)
        cell.genderFemaleButton.addTarget(self, action: #selector(didTapOnFeMaleButton(_:)), for: .touchUpInside)
        cell.saveProfileButton.addTarget(self, action: #selector(saveProfile(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "tabSegue" {
            
        }
    }
    

}
