//
//  DPOtpController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 27/07/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//


import UIKit
import TSMessages
import SwiftyJSON
class DDOtpTableViewCell: UITableViewCell {
    @IBOutlet weak var firstOtpTextField: UITextField!
    
    @IBOutlet weak var secondOtpTextField: UITextField!
    
    @IBOutlet weak var thirdOtpTextField: UITextField!
    
    @IBOutlet weak var fourthOtpTextField: UITextField!
    
    @IBOutlet weak var fifthOtpTextField: UITextField!
    
    @IBOutlet weak var sixthOtpTextField: UITextField!
    
    @IBOutlet weak var resendOtpTimerLabel: UILabel!
     var startCountDown : ((_ image : UIImageView) -> Void)?
    var count = 30
    var timer : Timer?
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    var otpDelegate: DPOtpVerificationDelegate? = nil
    var validationErrorDelegate: DDValidationErrorDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        startResendOtpCountdown()
        setUpOtpTextFields()
        addtextFieldObserver()
    }
    func setUpOtpTextFields(){
        firstOtpTextField.addBootomBorderLine(width:2)
        firstOtpTextField.delegate = self
        secondOtpTextField.addBootomBorderLine(width:2)
        secondOtpTextField.delegate = self
        thirdOtpTextField.addBootomBorderLine(width:2)
        thirdOtpTextField.delegate = self
        fourthOtpTextField.addBootomBorderLine(width:2)
        fourthOtpTextField.delegate = self
        fifthOtpTextField.addBootomBorderLine(width:2)
        fifthOtpTextField.delegate = self
        sixthOtpTextField.addBootomBorderLine(width:2)
        sixthOtpTextField.delegate = self
    }
    func addtextFieldObserver(){
        firstOtpTextField.addTarget(self, action: #selector(DDOtpTableViewCell.textFieldDidChange), for: .editingChanged)
        secondOtpTextField.addTarget(self, action: #selector(DDOtpTableViewCell.textFieldDidChange), for: .editingChanged)
        thirdOtpTextField.addTarget(self, action: #selector(DDOtpTableViewCell.textFieldDidChange), for: .editingChanged)
        fourthOtpTextField.addTarget(self, action: #selector(DDOtpTableViewCell.textFieldDidChange), for: .editingChanged)
        fifthOtpTextField.addTarget(self, action: #selector(DDOtpTableViewCell.textFieldDidChange), for: .editingChanged)
        sixthOtpTextField.addTarget(self, action: #selector(DDOtpTableViewCell.textFieldDidChange), for: .editingChanged)
    }
    @IBAction func submitOtpAction(_ sender: Any) {
        guard let firstTF = firstOtpTextField.text else {return}
        guard let secondTF = secondOtpTextField.text else {return}
        guard let thirdTF = thirdOtpTextField.text else {return}
        guard let fourthTF = fourthOtpTextField.text else {return}
        guard let fifthTF = fifthOtpTextField.text else {return}
        guard let sixthTF = sixthOtpTextField.text else {return}
        let string = "\(firstTF)\(secondTF)\(thirdTF)\(fourthTF)\(fifthTF)\(sixthTF)"
//        var string : String = firstOtpTextField.text! + secondOtpTextField.text! + thirdOtpTextField.text! + fourthOtpTextField.text! + fifthOtpTextField.text! + sixthOtpTextField.text!
        if string.count == 0 || string.count < 6 {
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter valid otp", type: .error)
        }else{
            self.otpDelegate?.verifyOTP(with: string)
        }
    }
}
extension DDOtpTableViewCell : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
               let currentString: NSString = NSString(string: textField.text!)
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= 1
    }
    func textFieldDidChange(sender : UITextField){
        switch sender {
        case firstOtpTextField:
            if firstOtpTextField.text != ""{
                secondOtpTextField.becomeFirstResponder()
            }
            break
        case secondOtpTextField:
            if secondOtpTextField.text != ""{
                thirdOtpTextField.becomeFirstResponder()
            }
            break
        case thirdOtpTextField:
            if thirdOtpTextField.text != ""{
                fourthOtpTextField.becomeFirstResponder()
            }
            break
        case fourthOtpTextField:
            if fourthOtpTextField.text != ""{
                fifthOtpTextField.becomeFirstResponder()
            }
            break
        case fifthOtpTextField:
            if fifthOtpTextField.text != ""{
                sixthOtpTextField.becomeFirstResponder()
            }
            break
        case sixthOtpTextField:
            if sixthOtpTextField.text != ""{
                sixthOtpTextField.resignFirstResponder()
            }
            break
        default:
            break
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func startResendOtpCountdown(){
        self.resendButton.isEnabled = false
         self.resendOtpTimerLabel.isHidden = false
        self.resendButton.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        timer?.fire()
    }
    func updateCounter() {
        if(count > 0) {
            self.resendOtpTimerLabel.text = String(count)
            count = count - 1
        }else{
            self.resendButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
            self.resendButton.isEnabled = true
            self.resendOtpTimerLabel.isHidden = true
            timer?.invalidate()
            count = 30
        }
    }
}
class DPOtpController: UIViewController, UITableViewDelegate, UITableViewDataSource,DPOtpVerificationDelegate {
    @IBOutlet weak var otpTableView: UITableView!
    private let otpCellId = "otpCell"
    private let profileSegue = "profileConfigSegue"
    
    
    @IBAction func dismissOtpController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    var OtpCell : DDOtpTableViewCell?
    var userID : String?
    var mobileNumber : String?
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - IBAction methods
    
    @IBAction func leftBarButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    func resendOTPAction(_ sender: Any) {
        let params = [mobile_key : mobileNumber, appTag_key : "Patient", typeOfPhone_key : "iphone", deviceId_key: "Sample"]
        ABProgressIndicator.shared.showAnimator()
        let resendOtprequest : DPWebRequestProtocol = ResendOtpRequest(params:params as [String : AnyObject])
        DPWebServicesManager.getResponse(with: resendOtprequest, completionHandler: {(response ,error) in
            self.setUPOTPController(with : response,error : error)
        })
    }
    func setUPOTPController(with response : JSON?,error : Error?){
        DispatchQueue.main.async {
            ABProgressIndicator.shared.hideAnimator()
        }
        if error == nil,let data = response?["data"].dictionary,let userId = data["_id"]?.stringValue{
            if let message = response?[MESSAGE_KEY].string{
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title:"Success", subtitle: message, type: .success)
            }else {
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title:"Success", subtitle: "Resend Otp Successfully", type: .success)
            }
            if let otpCell = self.OtpCell{
                otpCell.startResendOtpCountdown()
            }
        }else{
            if let message = response?[MESSAGE_KEY].string{
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title:"Error", subtitle: message, type: .error)
            }
        }
    }
    // MARK: - UITableview Delegate & Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.otpTableView.dequeueReusableCell(withIdentifier: otpCellId, for: indexPath) as! DDOtpTableViewCell
        cell.otpDelegate = self
        cell.resendButton.addTarget(self, action: #selector(DPOtpController.resendOTPAction(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        OtpCell = cell
        return cell
    }
    func verifyOTP(with otp: String) {
        guard let userID = self.userID else {
            return
        }
        let details : [String : AnyObject] = [ID_key : userID as AnyObject, otp_key : otp as AnyObject, typeOfPhone_key : "iphone" as AnyObject]
        ABProgressIndicator.shared.showAnimator()
        let OtpRequest = VerifyOtpRequest(params: details)
        DPWebServicesManager.getResponse(with: OtpRequest, completionHandler: {(response,error) in
            DispatchQueue.main.async {
                ABProgressIndicator.shared.hideAnimator()
            }
            if let response = response {
                if let data = response[DATA_KEY].dictionary{
                    let userInfo : DPUser = DPUser(data :data)
                    DPWebServicesManager.sharedManager.saveUser(with: userInfo)
                    self.appDelegate?.setUpIntailViewController()
                    var values = [String : AnyObject]()
                    values["OTP"] = "" as AnyObject
                    DPAppEventLog.shared.logEventName(EVENT_OTP_VERIFIED, values: values)
                } else {
                    TSMessage.showNotification(in: self, title: "Error", subtitle:  response[MESSAGE_KEY].string ?? "", type: .error)
                }
            } else {
                if let errorDiscription = error?.localizedDescription {
                    TSMessage.showNotification(in: self, title: "Error", subtitle: errorDiscription, type: .error)
                }
            }
        })
    }
}
