//
//  DPHomeSpecializationCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class HomeSpecializationCell : UICollectionViewCell{
    @IBOutlet weak var specializationLabel: UILabel!
    @IBOutlet weak var specializationImageView: UIImageView!
    @IBOutlet weak var labelView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if labelView != nil{
        labelView.layer.cornerRadius = 8
        labelView.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
        labelView.layer.borderWidth = 0.5
        }
    }
    }
