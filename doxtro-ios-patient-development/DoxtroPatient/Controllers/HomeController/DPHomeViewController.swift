//
//  DPHomeViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 09/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import FirebaseDatabase
import Firebase
import CoreData
import TSMessages
let DOXTRO_SHARE_URL = "https://itunes.apple.com/in/app/doxtro-consult-doctor-online/id1161686991?mt=8"
class DPHomeViewController : UIViewController {
    var specializationList : [Specialization] = []
    var packageDetail : PackageDeatail?
    
    @IBAction func shareAppAction(_ sender: Any) {
        openActionShareSheet()
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pageControlView: UIView!
    var recentConsulatationobj : DpConsultationDetails?
    var recentConsulatationArray : [DpConsultationDetails] = []
    var feedbackConsulatationArrray : [DpConsultationDetails] = []
    let animator: ZoomToFullscreen = ZoomToFullscreen.animator()
    lazy var channelRef: DatabaseReference = Database.database().reference().child(FIREBASE_CONSULTATION_DBKEY)
    var channelRefHandle: DatabaseHandle?
    let user =  DPUser.getUserInfoFromUserDefaults()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "DPMoreButtonCell", bundle: nil), forCellReuseIdentifier: "moreCellId")
        getRecentConsultation(isShow: true)
       // checkAndCreateMySelf()
        getFeedbackForConsultation()
        getSpecializationList()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.title = "Home"
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "navigationlogo"), highlightedImage: nil)
        self.automaticallyAdjustsScrollViewInsets = false
        //self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "DPAllConsultationTabCell", bundle: nil), forCellReuseIdentifier: "allConsultationCell")
        NotificationCenter.default.addObserver(self, selector: #selector(DPHomeViewController.refreshConsultationList), name: NSNotification.Name(rawValue: "RefreshConsultationList"), object: nil)
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
//        getPackage()
    }
//    func checkAndCreateMySelf(){
//        DPWebServicesManager.sharedManager.getRelativeList(completionHandler: { (error, response) in
//                if let response = response{
//                   debugPrint(response.count)
//                }else{
//                    self.createMySelf()
//                }
//            })
//    }
    func openActionShareSheet(){
        let shareVC:JYSystemShare = JYSystemShare()
        if let shareActivityVC:UIActivityViewController = shareVC.shareActionViewControllerWithItems(
            nil,
            shareContent: nil,
            shareURL: DOXTRO_SHARE_URL,
            shareUUID: nil) {
            //(nil, shareText: shareObject.shareText, shareURLString: shareObject.shareURL, shareUUID: nil) {
            self.present(shareActivityVC, animated: true, completion: { () -> Void in
                // debugPrint("share completed..")
            })
        }
    }
//    func createMySelf(){
//        if let user = DPWebServicesManager.sharedManager.user{
//            var params  = ["relation": "Myself"] as [String : AnyObject]
//            if let userId = user.userId,let name = user.name{
//                params["patientId"] = userId as AnyObject
//                params["name"] = name as AnyObject
//            }
//            if let age = user.age,let gender = user.gender,age != "" && gender != ""{
//                params["age"] = age as AnyObject
//                params["gender"] = gender as AnyObject
//            }
//        let createMemberrequest  = createMemberRequest(params: params )
//        DPWebServicesManager.getResponse(with: createMemberrequest, completionHandler: { (response, error) in
//            if let _ = response?["data"]{
//             debugPrint(response)
//            }
//        })
//    }
//    }
    func getFeedbackForConsultation(){
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
            let params = ["patientId" : userID]
            let  request = GetFeedbackOfConsulatation(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if let response = response{
                    if let data = response["data"].array{
                        self.feedbackConsulatationArrray.removeAll()
                        for value in data{
                            let consulattionInfoObj = DpConsultationDetails(dict: value)
                         self.feedbackConsulatationArrray.append(consulattionInfoObj)
                        }
                        if self.feedbackConsulatationArrray.count > 0{
                         self.showFeedackView(with: self.feedbackConsulatationArrray)
                        }
                    }
                }
                
            })
        }
    }
    func showFeedackView(with feedBackArray :  [DpConsultationDetails]){
        let viewController = UIStoryboard(name: "DPTabbar", bundle: nil).instantiateViewController(withIdentifier: "RatingController") as! DPRatingViewController
        viewController.feedbackConsulatationArrray = feedBackArray
        self.animator.isPresenting = true
        self.animator.presentController(viewController, fromController: UIApplication.getTopViewController()!)
    }
    func refreshConsultationList(){
        getRecentConsultation(isShow: false)
    }
 override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.tabBarController?.tabBar.isHidden = false
        getPackage()
    }
    func getSpecializationList(){
        if (DPWebServicesManager.sharedManager.reachability?.isReachable)!{
            ABProgressIndicator.shared.showAnimator()
            let getspecializationRequest = SpecializationRequest()
            DPWebServicesManager.getResponse(with: getspecializationRequest) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let data = response?["data"].array{
                    self.deleteSpecializationCoredataList()
                    for obj in data{
                        if let dictObj = obj.dictionary{
                            let specialzationObj = DPSpecializationList(dict: dictObj)
                            self.insertSpecializationIntoCoreData(specializationObj: specialzationObj)
                        }
                    }
                }
            }
        } else {
            self.loadSpecializationFromCoreData()
        }
    }
    //old way of show 
//    func ayush(){
//        if let user = DPWebServicesManager.sharedManager.user{
//        let params = ["patientId" : user.userId,"skip":0,"limit":1] as [String : Any]
//        let request = GetRecentConsultationrequest(params: params as [String : AnyObject])
//            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
//                debugPrint(response)
//                if error == nil,let response = response{
//                    if let array = response["data"].array{
//                        guard array.count > 0 else{
//                            return
//                        }
//                    }
//                        if let data = response["data"].array?.first?.dictionary{
//                            let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
//                            self.recentConsulatationobj = consulattionInfoObj
//                            //consulattionInfoObj.patientFee = response["patientFee"].intValue
//                        }
//                    self.tableView.reloadData()
//                }
//            })
//        }
//    }
    func getRecentConsultation(isShow : Bool){
        DPWebServicesManager.sharedManager.getRecentConsultations(isShow: isShow, completationHandler: { (error, response) in
                if error == nil,let consultationArray = response{
                        guard consultationArray.count > 0 else{
                            return
                        }
                    self.recentConsulatationArray = consultationArray
                    self.tableView.reloadData()
                }
            })
        }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "specializationListView"{
        let destinationController = segue.destination as? DPSpecializationListController
            destinationController?.specializationList = self.specializationList
        }else if segue.identifier == "showSelectedSpecializationPage"{
            let destinationController = segue.destination as? DPSelectedSpecilizationController
            destinationController?.selectedSpecializationObj = sender as? Specialization
        }else if segue.identifier == "showConsultViewController"{
            if  let destinatioVC = segue.destination as? ConsultationViewController{
                destinatioVC.navigationController?.navigationBar
                destinatioVC.consulatationInfo = self.recentConsulatationobj
            }
        }
    }
    /**
     Call Action for Medical Assistance
     **/
    func callActionForMedicalAssistance(_ sender : Any) {
       //Perform Call
        guard let supportNumber = user?.supportNumber,let number = URL(string: "tel://" + supportNumber) else {
            return }
        UIApplication.shared.open(number)
    }
}
extension DPHomeViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
           /* let cell = tableView.dequeueReusableCell(withIdentifier: "homeSpecilaizationCell") as? DpSpecializationCell
            cell?.configureCell(specializationListArray: self.specializationList)
            cell?.selectionStyle = .none
            cell?.delegate = self
            return cell!
            */
            let cell = tableView.dequeueReusableCell(withIdentifier: "startConsCell") as! DPHomeStartConsultationCell
            cell.startConsultationButton.addTarget(self, action: #selector(showFullListSpecialization), for: .touchUpInside)
            return cell
        } else if indexPath.section == 3 {
            guard let _ = user?.supportNumber else {return UITableViewCell()}
            if let isCorporate = user?.isCorporate {
                if isCorporate {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: "medicalAssid", for: indexPath) as? DPMedicalAssistanceTVCell {
                        cell.callButton.addTarget(self, action: #selector(callActionForMedicalAssistance(_:)), for: .touchUpInside)
                        return cell
                    }
                }else {
                    return UITableViewCell()
                }
            } else {
                return UITableViewCell()
            }
        }
        else if indexPath.section == 2 {
            if let packageDetail = self.packageDetail{
                if let boughtPackageDetail = packageDetail.boughtPackageDetail{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "packageProgressCell", for: indexPath) as! PackageProgressCell
                    cell.packageTypeLabel.text = boughtPackageDetail.subscriptionTag
                    cell.renewDateLabel.text = "Renews on \(NSDate().convertToDMY(dateStr:boughtPackageDetail.endDate))"
                    cell.remainingConsultationLablel.text = "\(Int(boughtPackageDetail.totalRemainingConsultation)) of \(Int(boughtPackageDetail.totalConsultations)) consultations remaining"
                    cell.configureProgressBar(packageDetail: boughtPackageDetail)
                    
                    if boughtPackageDetail.corporatePayType == "Non-Paying" {
                        if boughtPackageDetail.subscriptionType == "Subscription" {
                            //For subscription we will show both cancel and renew button. cancel is always be displayed. But renew button will be displayed when current today's date is exceeded than end date of subscription.
                            if Date().compare(NSDate().convertADate(dateStr: boughtPackageDetail.endDate) as Date) == .orderedDescending {
                                cell.renewButton.isHidden = false
                            } else {
                                cell.renewButton.isHidden = true
                            }
                            cell.cancelButton.isHidden = false
                        } else {
                            if boughtPackageDetail.totalRemainingConsultation == 0 || Date().compare(NSDate().convertADate(dateStr: boughtPackageDetail.endDate) as Date) == .orderedDescending {
                                //For packages we will not display the cancel button. So for layout constraints adjusting, change cancel button to renew button functionality. So cancel is nothing but renew here. We will hide renew button.
                                cell.cancelButton.isHidden = false
                                cell.cancelButton.setTitle("Renew", for: .normal)
                            } else {
                                
                                cell.cancelButton.isHidden = true
                            }
                            cell.renewButton.isHidden = true
                        }
                    } else {
                        cell.cancelButton.isHidden = true
                        cell.renewButton.isHidden = true
                    }
                    cell.renewButton.addTarget(self, action: #selector(renewPackageAction(_:)), for: .touchUpInside)
                    cell.cancelButton.addTarget(self, action: #selector(cancelPackageButtonAction(_:)), for: .touchUpInside)
                    return cell
                }else if let packageAddtionalDetail = packageDetail.packageAdditionalDetail{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "packageInfoCell", for: indexPath) as! HomePackageCell
                    cell.delegate = self
                    cell.titleLabel.text = packageAddtionalDetail.title
                    cell.subTitleLabel.text = packageAddtionalDetail.description
//                    cell.subTitleLabel.text = "packageAddtionalDetail.description, packageAddtionalDetail.description, packageAddtionalDetail.description, packageAddtionalDetail.description,packageAddtionalDetail.description"

                    if let imageUrl = packageAddtionalDetail.imageUrl{
                        cell.packageImageView.af_setImage(withURL: imageUrl, imageTransition: .crossDissolve(0.1), runImageTransitionIfCached: true)
                    }
                    return cell
                }
            }else{
               return UITableViewCell()
            }
        } else if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "freshChatId", for: indexPath) as? DPMedicalFreshChatTVCell {
                cell.collectionView.reloadData()
                return cell
            }
        }
        else if indexPath.section == 5 {
            if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "moreCellId" , for: indexPath) as! DDMoreTVCell
                cell.moreButton.addTarget(self, action: #selector(moreButtonAction(_:)), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
            }
            if self.recentConsulatationArray.count-1  >= indexPath.row {
                let recentConsulatationobj = self.recentConsulatationArray[indexPath.row]
                if let recentConsultation = self.recentConsulatationArray[indexPath.row].relativeObj{
                    self.tableView.allowsSelection = true
                    let cell = tableView.dequeueReusableCell(withIdentifier: "allConsultationCell") as? AllConsultCell
                    cell?.nameLabel.text = recentConsulatationobj.specializationCategory
                    cell?.relationAgeLabel.text = (recentConsulatationobj.consultingFor)! + ", " + recentConsultation.age!
                    cell?.concernLabel.text = recentConsulatationobj.note
                    cell?.unreadMsgCountView.isHidden = true
                    cell?.imageConsultationType.image = recentConsulatationobj.consultationType.rawValue.imageForRelation(with: "")
                    if let status = recentConsulatationobj.status{
                        switch status {
                        case .Waiting:
                            setConsultationLabelViewWithColor(cell: cell, color:UIColor().yellowDoxtroCgColor() , With:ConsultationStatus.Waiting.rawValue )
                            cell?.unreadMsgCountView.isHidden = true
                            break
                        case .New:
                            setConsultationLabelViewWithColor(cell: cell, color:UIColor.red , With:ConsultationStatus.New.rawValue)
                            cell?.unreadMsgCountView.isHidden = true
                            break
                        case .Ongoing:
                            setConsultationLabelViewWithColor(cell: cell, color:UIColor(red: 110, green: 160, blue: 56) , With:ConsultationStatus.Ongoing.rawValue)
                            if let recentConsulataion =  recentConsulatationobj.consultationId{
                                UpdateChatUnreadCount(consulatationId:  recentConsulataion, cell: cell!)
                                cell?.nameLabel.text = recentConsulatationobj.doctorObj?.firstName
                                // observeMessagesWhenChanged(cosultationId : recentConsulataion, cell: cell!)
                            }
                            break
                        case .Completed:
                            setConsultationLabelViewWithColor(cell: cell, color:UIColor().greenDoxtroCgColor() , With:ConsultationStatus.Completed.rawValue)
                            cell?.unreadMsgCountView.isHidden = true
                            cell?.nameLabel.text = recentConsulatationobj.doctorObj?.firstName
                            break
                        case .Closed:
                            setConsultationLabelViewWithColor(cell: cell, color: UIColor.red , With:ConsultationStatus.Closed.rawValue)
                            cell?.unreadMsgCountView.isHidden = true
                            cell?.nameLabel.text = recentConsulatationobj.doctorObj?.firstName
                            break
                        case .Planned:
                            setConsultationLabelViewWithColor(cell: cell, color:UIColor().greenDoxtroCgColor() , With:ConsultationStatus.Planned.rawValue)
                            cell?.unreadMsgCountView.isHidden = true
                            cell?.nameLabel.text = recentConsulatationobj.doctorObj?.firstName
                            break
                        default:
                            cell?.consulataionSatausView.isHidden = true
                            break
                        }
                        
                        if let gender = recentConsultation.gender{
                            cell?.profileImageView.image = recentConsulatationobj.consultingFor?.imageForRelation(with: gender)
                        }
                        cell?.timeLabel.text = self.textCellTopLabel(indexPath: indexPath)?.string
                        
                        if let timeLabel = cell?.timeLabel.text {
                            if (timeLabel.contains("Today")) {
                                cell?.timeLabel.text = DXUtility.convertToTime(dateStr: self.recentConsulatationobj?.createdDate)//"Today"
                            } else if timeLabel.contains("Yesterday") {
                                cell?.timeLabel.text = "Yesterday"
                            }else {
                                cell?.timeLabel.text = DXUtility.convertToDMY(dateStr: self.recentConsulatationobj?.createdDate)//"Today"
                            }
                        }
                    }
                    
                    return cell!
                }
            }
            else{
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "recentConsultationCell") as? DPRecentConsultationCell
                    self.tableView.allowsSelection = false
                    cell?.delegate = self
                    return cell!
                }
                else if indexPath.row == 1 || indexPath.row == 2{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "moreCellId" , for: indexPath) as! DDMoreTVCell
                    cell.moreButton.addTarget(self, action: #selector(moreButtonAction(_:)), for: .touchUpInside)
                    cell.selectionStyle = .none
                    return cell
                }
            }
        }else if indexPath.section == 4 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "referEarnId", for: indexPath) as! DPReferEarnCell
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 5{
            if self.recentConsulatationArray.count > 1{
                return (3)
            }else{
                return (2)
            }
        }
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        /*if let isCorporate = user?.isCorporate {
            if isCorporate {
                return 6
            }
        }*/
        return 6
    }
    /*func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60
        }
        return UITableViewAutomaticDimension
    }*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 85
        }else if indexPath.section == 1 {
            return 140//screenWidth/3
        }else if indexPath.section == 2 || indexPath.section == 5 {
            return UITableViewAutomaticDimension
        }else if indexPath.section == 3 {
            if let supportNumber = user?.supportNumber {
                if supportNumber == "" {
                    return 0
                }
            }
            if let isCorporate = self.user?.isCorporate {
                if isCorporate {
                    return 100//UITableViewAutomaticDimension
                } else {
                    return 0
                }
            } else {
                return 0
            }
        }else if indexPath.section == 4 {
           return 122
        }
        return 256
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell  =  tableView.cellForRow(at: indexPath) as? DDMoreTVCell{
            return
        }
        switch indexPath.section {
        case 4:
            let destinationVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "referelCodeController") as! ReferelViewController
            self.present(destinationVC, animated: true, completion: nil)
            break
        case 5:
            DPAppEventLog.shared.logEventName("Recent Consultations", values: ["Users opening recent consultations on home page." : "" as AnyObject])
            self.recentConsulatationobj = self.recentConsulatationArray[indexPath.row]
            DPWebServicesManager.sharedManager.saveConsultationInfo(with: self.recentConsulatationobj!)
            self.performSegue(withIdentifier: "showConsultViewController", sender: nil)
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView{
        if section == 5{
            let sectionview = UINib(nibName: "DPDoctorProfileSectionHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? DoctorProfileSectionView
            if let requiredView = sectionview{
                requiredView.sectionTitle.text = "RECENT CONSULTATION"
                return requiredView
            }
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        view.backgroundColor = UIColor.white
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       /* if section == 0 || section == 1 || section == 2 || section == 3 {
            return 0
        }
 */
        if section == 5 {
            return 40
        }
        return 0
    }
    func renewPackageAction(_ sender: UIButton) {
        if let packageDetail = self.packageDetail{
            if let boughtPackageDetail = packageDetail.boughtPackageDetail {
                if boughtPackageDetail.subscriptionType == "Subscription" {
                    //Renew Action
                    self.openPackageViewController()
                }
            }
        }
    }
    func cancelPackageButtonAction(_ sender: UIButton) {
        if let packageDetail = self.packageDetail{
            if let boughtPackageDetail = packageDetail.boughtPackageDetail {
                if boughtPackageDetail.subscriptionType == "Subscription" {
                    UIAlertController.showAlertWithMessage("Do you want to cancel the subscription?".localized, title: "SUBSCRIPTIONS", destructiveButtonTitle: "NO", actionButtonTitle: "YES", destructiveButtonHandler: { (_ , _) in
                        
                    }, actionButtonHandler: { (_ , _) in
                        if let button = sender as? UIButton{
                            if let user = DPWebServicesManager.sharedManager.user{
                                ABProgressIndicator.shared.showAnimator()
                                let params  = ["patientId" : user.userId]
                                let request = CancelSubscriptionsRequest(params: params as [String : AnyObject])
                                DPWebServicesManager.getResponse(with: request) { (response, error) in
                                    ABProgressIndicator.shared.hideAnimator()
                                    debugPrint(response)
                                    if let _ = response {
                                        self.getPackage()
                                        //debugPrint("Response\(response)")
                                        //  self.packageListDetail =  PackageListDetail(data: JSON(response["data"].dictionary))
                                        //   self.changeToSelectedSegmentColor(selectedIndex: .onetime)
                                    }
                                }
                            }
                        }
                    })
                } else {
                    //Renew Action when packages over
                    self.openPackageViewController()
                }
            } else if let _ = packageDetail.packageAdditionalDetail {
            }
        }
    }
    func moreButtonAction(_ sender : UIButton){
        UIViewController.switchToExpertTab(.Consulatation)
    }
    func textCellTopLabel(indexPath: IndexPath) -> NSAttributedString? {
        self.recentConsulatationobj = self.recentConsulatationArray[indexPath.row]
        if let createdDate = self.recentConsulatationobj?.createdDate {
            let messageDate = DXUtility.convertADate(dateStr: createdDate)
            let attributedString = JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: messageDate as Date?)
            return attributedString
        }
        return nil
    }
    func UpdateChatUnreadCount(consulatationId : String,cell : AllConsultCell){
        let messageQuery = channelRef.child(consulatationId)
        // 2. We can use the observe method to listen for new
        // messages being written to the Firebase DB
        channelRefHandle = messageQuery.observe(.value, with: { (snapshot) -> Void in
            if let channelData = snapshot.value as? Dictionary<String, AnyObject>{ // 2
                var i = 0
                for (_,obj) in channelData{
                    if let obj = JSON(obj).dictionary{
                        if let msgType = obj["msgType"]?.string,msgType == "text"{
                            if let from = obj["from"]?.string{
                                if from == "Doctor" {
                                    if let status = obj["status"]?.string{
                                        if status == "sent" || status == "Sent"{
                                            i = i+1
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if i > 0{
                    cell.unreadMessageCountLabel.text = String(describing: i)
                    cell.unreadMsgCountView.isHidden = false
                }else{
                    cell.unreadMsgCountView.isHidden = true
                }
            }
        })
    }
    func setConsultationLabelViewWithColor(cell: AllConsultCell?,color : UIColor?,With text : String){
        cell?.consulataionSatausView.backgroundColor = color
        cell?.cconsulatationStatusLabel.text = text
    }
    func alertForOpenLocationSettings(with type:OpenLocationSettingType){
        UIAlertController.showAlertWithMessage("", title:"LOCATION_DISABLE_MESSAGE".localized , actionButtonTitle:"SETTING".localized, cancelButtonTitle: "CANCEL".localized, actionButtonHandler: { (controller, action) in
            switch(type){
            case .deniedApplicationLocation:
                guard let settingsUrl = URL(string: OpenLocationSettingUrl.application) else {
                    return
                }
                self.openSettingPage(with: settingsUrl)
                break
            case .iphoneLocationServiceDisable:
                guard let settingsUrl = URL(string:OpenLocationSettingUrl.phone) else {
                    return
                }
                self.openSettingPage(with: settingsUrl)
                break
            }
        }, cancelButtonHandler: nil)
    }
    func openSettingPage(with settingsUrl:URL){
        if UIApplication.shared.canOpenURL(settingsUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    //Prints true
                })
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
}
extension DPHomeViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width-48)/4 , height:79)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(6)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
    
}
extension DPHomeViewController : DPSpecializationDelegate,DPRecentConsultationDelegate{
    func showFullListSpecialization() {
        self.performSegue(withIdentifier: "specializationListView", sender: nil)
    }
    func selectedSpecialization(with index: Int) {
        if let isRechable = DPWebServicesManager.sharedManager.reachability?.isReachable,isRechable{
        let specilizationObj = self.specializationList[index]
        var values = [String : AnyObject]()
        values["Specialisation name"] = specilizationObj.specializationName as AnyObject
        DPAppEventLog.shared.logEventName(EVENT_SPECIALIZATION_SELECTED, values: values)
        self.performSegue(withIdentifier: "showSelectedSpecializationPage", sender: specilizationObj)
        }else{
            DXUtility.showNoNetworMessage()
        }
    }
    func consultDoctor() {
        self.performSegue(withIdentifier: "specializationListView", sender: nil)
    }
}

enum OpenLocationSettingType : Int{
    case deniedApplicationLocation
    case iphoneLocationServiceDisable
}

struct OpenLocationSettingUrl {
    static let phone = "Prefs:root=PRIVACY&path=LOCATION"
    static let application = UIApplicationOpenSettingsURLString
}
class DDMoreTVCell: UITableViewCell {
    @IBOutlet weak var moreButton: UIButton!
}
//MARK: Core data
extension DPHomeViewController{
    func insertSpecializationIntoCoreData(specializationObj: DPSpecializationList){
        if let specialization = NSEntityDescription.insertNewObject(forEntityName: SECIALIZATION_ENTITY, into: DatabaseController.getContext()) as? Specialization {
            specialization.specializationName = specializationObj.specializationCategory
            specialization.specializationDescription = specializationObj.specializationCategoryDescription
            specialization.specilizationImageUrl = specializationObj.categoryImageUrl?.absoluteString
            self.refreshCoreData()
            DPHomeViewController.getImageDataAndSave(url:(specializationObj.categoryImageUrl?.absoluteString)! , completionHandler: { (data) in
                specialization.specializationImage = data as! NSData
                self.refreshCoreData()
            })
            
        }
    }
    func refreshCoreData(){
        DatabaseController.saveContext()
        self.loadSpecializationFromCoreData()
    }
    func fetchSpecializationListFromCoreData(predicate : NSPredicate? = nil) -> [Specialization]?{
        let specializationFetchRequest : NSFetchRequest<Specialization> = Specialization.fetchRequest()
        if let predicate = predicate{
            specializationFetchRequest.predicate = predicate
        }
        do{
            return try DatabaseController.getContext().fetch(specializationFetchRequest)
        }catch{
            return nil
        }
    }
    func deleteSpecializationCoredataList(){
        if let specializationList = fetchSpecializationListFromCoreData(){
            for specialiaztioObj in specializationList{
            DatabaseController.getContext().delete(specialiaztioObj)
            }
        }
    }
    class func getImageDataAndSave(url:String,completionHandler :@escaping(_ data : Data)->Void)
    {
        let httpMethod = "GET"
        let timeout = 15
       // let url = URL(string: url)
        //let urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15.0)
        NSURLConnection.sendAsynchronousRequest(
            URLRequest(url: URL(string: url)!),
            queue: OperationQueue.main,
            completionHandler: {(response: URLResponse?,
                data: Data?,
                 error: Error?) in
                if let data = data{
                    completionHandler(data)
                }
            }
        )
    }
    func loadSpecializationFromCoreData(){
        if let specializationList =  self.fetchSpecializationListFromCoreData(){
            self.specializationList = specializationList
            self.tableView.reloadData()
        }
    }
    
}
extension DPHomeViewController :HomePackageDelegate{
    func openPackageViewController() {
        self.performSegue(withIdentifier: "openPackaeViewController", sender: nil)
    }
    func getPackage(){
        if let patientID = DPWebServicesManager.sharedManager.user?.userId{
            var params  = ["patientId" : patientID]
        let request = GetPackageRequest(params: params as [String : AnyObject])
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            if let response  = response{
                debugPrint(response)
                self.packageDetail =  PackageDeatail(data: JSON(response))
                self.tableView.reloadData()
            }
        }
    }
}
}
