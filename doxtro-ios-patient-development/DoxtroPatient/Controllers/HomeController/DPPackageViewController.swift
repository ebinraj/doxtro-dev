//
//  DPPackageViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import TSMessages
enum PackageType : Int{
    case onetime
    case recurring
    init (value : Int){
        switch value{
        case 0:
            self = .onetime
            break
        case 1 :
            self = .recurring
            break
        default:
            self = .onetime
            break
        }
    }
}
class PackageViewController : UIViewController{
    var sortedSegmentViews : [UIView]?
    var packageListDetail : PackageListDetail?
    var selectedPackageTypeArrray : [AnyObject] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    var selectedPackageType : PackageType = .onetime
    var selectedIndexPAth : IndexPath?
    var razorpay : Razorpay!
    var paybleAmount : String?
    var ORDER_NUMBER : String = ""
    var createdConsultationInfo : DpConsultationDetails?
    var consultationID : String?
    var recentRazorPaySubscriptionId = ""
    var subscriptionID : String?
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var purchaseButton: DDHighlightedButton!
    
    struct RazorPayPayment {
        struct KeyID {
            static let TEST = "rzp_test_AHmF8XvSfeTdOc"
            static let LIVE = "rzp_live_mr1w5Vcvhkac5j"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        razorpay = Razorpay.initWithKey(RazorPayPayment.KeyID.LIVE, andDelegate: self)
        razorpay.setExternalWalletSelectionDelegate(self)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        getPackageList()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        self.sortedSegmentViews = segmentController.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
        setUpNewsTypeSegmentController()
        changeToSelectedSegmentColor(selectedIndex: .onetime)
        self.navigationItem.title = "Consultation Packages"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: .plain, target: self, action: #selector(tappedOnLeftBarButtonItem))
        self.noDataLabel.isHidden = true
    }
    func getOnetimePackageDetail(){
        if let userID = DPWebServicesManager.sharedManager.user?.userId,let obj = self.packageListDetail?.oneTimePackageDetail[(self.selectedIndexPAth?.row)!]{
            let params = ["subscriptionId":obj._id,"patientId":userID]
            let request = GetSubscriptionChargesRequest(params: params as [String : AnyObject])
            DPWebServicesManager.sharedManager.getPackageValues(request: request, complitionHandler: { (response, error) in
                if let paymentdata = response {
                    self.performSegue(withIdentifier: "packageCouponViewController", sender: paymentdata)
                }
            })
        }
    }
    func tappedOnLeftBarButtonItem() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func packageSelection(_ sender: Any) {
        if sender is UISegmentedControl{
            if let segmentControl = sender as? UISegmentedControl{
                changeToSelectedSegmentColor(selectedIndex: PackageType(value: segmentControl.selectedSegmentIndex))
                changePackageVaueUsing(packageType: PackageType(value: segmentControl.selectedSegmentIndex))
            }
        }
    }
    func changePackageVaueUsing(packageType : PackageType){
        switch packageType {
        case .onetime:
            break
        case .recurring :
            break
        default:
            break
        }
    }
    
    func setUpNewsTypeSegmentController(){
        segmentController.layer.masksToBounds = true
        self.segmentController.tintColor = UIColor.clear
        segmentController.backgroundColor = UIColor.white
        segmentController.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .selected)
        segmentController.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .normal)
        //NSBackgroundColorAttributeName:UIColor.magenta
    }
    func changeToSelectedSegmentColor(selectedIndex : PackageType){
        switch selectedIndex {
        case .onetime:
            self.selectedIndexPAth = nil
            self.noDataLabel.isHidden = true
            self.purchaseButton.isEnabled = true
            self.selectedPackageType = .onetime
            if let packageDeatil = self.packageListDetail?.oneTimePackageDetail{
                self.selectedPackageTypeArrray = packageDeatil
                self.tableView.reloadData()
            } else if self.packageListDetail == nil {
                self.noDataLabel.isHidden = false
                self.noDataLabel.text = "No Packages found"
                self.purchaseButton.isEnabled = false
            }
            if let view : UIView = self.sortedSegmentViews?[selectedIndex.rawValue]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[1]{
                view.backgroundColor = UIColor.white
            }
            break
        case .recurring:
            self.noDataLabel.isHidden = true
            self.purchaseButton.isEnabled = true
            self.selectedIndexPAth = nil
            self.selectedPackageType = .recurring
            if let packageDeatil = self.packageListDetail?.subscriptionPackageDetail{
                self.selectedPackageTypeArrray = packageDeatil
                self.tableView.reloadData()
            } else if self.packageListDetail == nil {
                self.noDataLabel.text = "No Subscriptions found"
                self.purchaseButton.isEnabled = false
            }
            if let array = self.packageListDetail?.subscriptionPackageDetail{
                self.selectedPackageTypeArrray = array
            }
            if let view : UIView = self.sortedSegmentViews?[selectedIndex.rawValue]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[0]{
                view.backgroundColor = UIColor.white
            }
            break
        default:
            break
        }
    }
    func getPackageList(){
        if let patientID = DPWebServicesManager.sharedManager.user?.userId{
            var params  = ["patientId" : patientID]
            let request = PackageListRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                debugPrint(response)
                self.noDataLabel.superview?.bringSubview(toFront: self.noDataLabel)
                if let response = response {
                    if let data = response["data"].array {
                        if data.count == 0 {
                            if self.segmentController.selectedSegmentIndex == 0 {
                                self.noDataLabel.isHidden = false
                                self.noDataLabel.text = "No Pacakages found"
                            }
                        }
                    } else {
                        self.packageListDetail =  PackageListDetail(data: JSON(response["data"].dictionary))
                    }
                    self.changeToSelectedSegmentColor(selectedIndex: .onetime)
                }
            }
        }
    }
    fileprivate func requestForOneTimePackage() {
        if let selectIndexPath = self.selectedIndexPAth {
            if let obj = self.packageListDetail?.oneTimePackageDetail[(selectIndexPath.row)] {
                print("Price\(obj.price)")
                self.paybleAmount = String(describing: obj.price)
            }
            if let amount = self.paybleAmount{
                if let intAmount = Int(amount){
                    if intAmount <= 0 {
                        
                    } else {
                        showPaymentForm()
                    }
                }
            }
        } else {
            TSMessage.showNotification(withTitle: "Please select any one package", type: .error)
        }
    }
    
    @IBAction func purchasePackageAction(_ sender: DDHighlightedButton) {
        if segmentController.selectedSegmentIndex == 0 {
            getOnetimePackageDetail()
        } else {
            print("tapped on subscription")
            if let selectIndexPath = self.selectedIndexPAth {
                if let obj = self.packageListDetail?.subscriptionPackageDetail[(selectIndexPath.row)] {
                    self.performSegue(withIdentifier: "paymentSubSegue", sender: obj)
                    //requestForSubscriptionWithPlanId(obj.planId)
                }
            } else {
                TSMessage.showNotification(withTitle: "Please select any one subscription", type: .error)
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "paymentSubSegue" {
            let destinationController = segue.destination as! DPSubscriptionPaymentViewController
            if let subscription = sender as? Subscription {
                destinationController.subscription = subscription
            }
        }
        if segue.identifier == "packageCouponViewController" {
            if let destinationVc = segue.destination as?PackagePaymentViewController{
                if let paymentData = sender as? PaymentData{
                    destinationVc.couponPaymentData = paymentData
                    destinationVc.packageListDetail = self.packageListDetail
                    destinationVc.selectedIndexPAth = self.selectedIndexPAth 
                }
            }
        }
    }
}
extension PackageViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "packageListCell", for: indexPath) as! PackageTypeCell
        if let packageDetailArray = self.selectedPackageTypeArrray as? [Package]{
            let packageDetail = packageDetailArray[indexPath.row]
            cell.packageTypeLabel.text = packageDetail.subscriptionTag
            cell.packagePriceCell.text = "₹ " + String(describing: packageDetail.price)
            cell.packageDurationCell.text = packageDetail.duration.uppercased()
            cell.packageNoteCell.text = packageDetail.description
            let originalPrice = "₹ " + String(describing: packageDetail.originalPrice)
            if packageDetail.originalPrice > 0 {
                cell.originalPriceLabel.attributedText = NSAttributedString().getStrikeThroughString(string: originalPrice)
            }
            
            //"Includes " + "\(packageDetail.consultationPerEmployee + packageDetail.freeGPConsultations) Consultations"
        } else if let packageDetailArray = self.selectedPackageTypeArrray as? [Subscription] {
            let packageDetail = packageDetailArray[indexPath.row]
            cell.packageTypeLabel.text = packageDetail.subscriptionTag
            cell.packagePriceCell.text = "₹ " + String(describing: packageDetail.price)
            cell.packageDurationCell.text = packageDetail.duration.uppercased()//packageDetail.paymentCycle
            cell.packageNoteCell.text = packageDetail.description
            let originalPrice = "₹ " + String(describing: packageDetail.originalPrice)
            if packageDetail.originalPrice > 0 {
                cell.originalPriceLabel.attributedText = NSAttributedString().getStrikeThroughString(string: originalPrice)
            }
            //"Includes " + "\(packageDetail.consultationPerEmployee + packageDetail.freeGPConsultations) Consultations"
        }
        if selectedIndexPAth == indexPath {
            cell.imageSelectionCell.tintColor = UIColor().greenDoxtroCgColor()
            cell.imageSelectionCell.image = UIImage(named: "packageSelect")
        } else {
            cell.imageSelectionCell.tintColor = UIColor.lightGray
            cell.imageSelectionCell.image = UIImage(named: "packageSelect")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.selectedPackageType {
        case .onetime:
            if let oneTimepPackageArray = self.packageListDetail?.oneTimePackageDetail{
                return oneTimepPackageArray.count
            }
            break
        case .recurring :
            if let subscriptionPackageArray = self.packageListDetail?.subscriptionPackageDetail{
                return subscriptionPackageArray.count
            }
            break
        default:
            break
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedIndexPAth == indexPath {
            self.selectedIndexPAth = nil
        } else {
            self.selectedIndexPAth = indexPath
        }
        self.tableView.reloadData()
    }
}
extension PackageViewController {
    func showPaymentForSubscribe(_ subscriptionId: String) {
        let imageLogo = UIImage(named: "navigationlogo")
        var options = ["image":imageLogo,
                       "name" :"Doxtro",
                       "description": "Fee",
                       "theme" : ["color" : "#27A29C"],
                       "prefill" : [:],
                       "subscription_id" : subscriptionId] as [String : Any]
        if var PrefillDictionary = options["prefill"] as? [String : String]{
            if let user = DPWebServicesManager.sharedManager.user{
                if let prefillEmail = user.emailId{
                    PrefillDictionary["email"] = prefillEmail
                }
                if let prefillMobileNumber = user.mobileNum{
                    PrefillDictionary["contact"] =  prefillMobileNumber
                }
                options["prefill"] = PrefillDictionary
            }
        }
        razorpay.open(options)
    }
    func showPaymentForm() {
        let imageLogo = UIImage(named: "navigationlogo")
        var options = ["image":imageLogo,
                       "name" :"Doxtro",
                       "description": "Fee",
                       "theme" : ["color" : "#27A29C"],
                       "prefill" : [:],
                       "external" : ["wallets" : ["paytm"]]] as [String : Any]
        if var PrefillDictionary = options["prefill"] as? [String : String]{
            if let user = DPWebServicesManager.sharedManager.user{
                if let prefillEmail = user.emailId{
                    PrefillDictionary["email"] = prefillEmail
                }
                if let prefillMobileNumber = user.mobileNum{
                    PrefillDictionary["contact"] =  prefillMobileNumber
                }
                options["prefill"] = PrefillDictionary
            }
        }
        
        if let amount = self.paybleAmount{
            if let intAmount = Int(amount){
                options["amount"] = (intAmount * 100)
            }
            razorpay.open(options)
        }
    }
    func getCheckSum(completionHandler : @escaping(_ error : Error?,_ response : CheckSumPaytm?)-> Void){
        if let user = DPWebServicesManager.sharedManager.user{
            let  randomNumber = (arc4random() % 9999999) + 1;
            ORDER_NUMBER = String(format: "Doxtro_%d", randomNumber)
            let params = ["customerId":user.userId
                ,"amount":self.paybleAmount,
                 "email":user.emailId,
                 "mobileNumber":"8123632112",
                 "orderId" :ORDER_NUMBER,
                 "type" : "live"]
            let request = GetChecksumPaaytmRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if error == nil,let response = response{
                    if let dict = response.dictionary{
                        let checksumObject = CheckSumPaytm(dict: JSON(dict["data"]?.dictionary))
                        completionHandler(nil,checksumObject)
                    }
                }else{
                    completionHandler(error,nil)
                }
            })
        }
    }
    func makePaytmOrderWith(checkSumDetails : CheckSumPaytm){
        if let user = DPWebServicesManager.sharedManager.user{
            let orderDict = ["MID":checkSumDetails.MID,
                             "CHANNEL_ID":checkSumDetails.CHANNEL_ID,
                             "INDUSTRY_TYPE_ID":checkSumDetails.INDUSTRY_TYPE_ID,
                             "WEBSITE":checkSumDetails.WEBSITE,
                             "TXN_AMOUNT":checkSumDetails.TXN_AMOUNT,
                             "ORDER_ID":checkSumDetails.ORDER_ID,
                             "CALLBACK_URL":checkSumDetails.CALLBACK_URL,
                             "CHECKSUMHASH":checkSumDetails.CHECKSUMHASH,
                             "REQUEST_TYPE":checkSumDetails.REQUEST_TYPE,
                             "CUST_ID":checkSumDetails.CUST_ID,
                             "EMAIL":user.emailId,
                             "MOBILE_NO":"8123632112"]
            if let order = PGOrder(params: orderDict){
                intiatePaymentWithOrder(order: order)
            }
        }
    }
    func intiatePaymentWithOrder(order : PGOrder){
        let viewBar = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
        viewBar.backgroundColor = UIColor().greenDoxtroCgColor()
        let button = UIButton(frame: CGRect(x: 5, y: 24, width: 70, height: 40))
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.textColor = UIColor.black
        button.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14.0)
        let tileLabel = UILabel(frame: CGRect(x: self.view.frame.width/2 - 50, y: 20, width: 100, height: 50))
        tileLabel.text = "Doxtro"
        tileLabel.font = UIFont(name: "Helvetica-Bold", size: 18.0)
        tileLabel.textColor = UIColor.white
        tileLabel.textAlignment = NSTextAlignment.center
        viewBar.addSubview(tileLabel)
        var merchantObj = PGMerchantConfiguration.default()
        if let transactionController = PGTransactionViewController.init(transactionFor: order){
            transactionController.topBar = viewBar
            transactionController.cancelButton = button
            transactionController.serverType = eServerTypeProduction
            transactionController.merchant = merchantObj
            transactionController.loggingEnabled = true
            transactionController.delegate = self
            self.present(transactionController, animated: true, completion: nil)
        }
    }
    func showPaymenStatusMessage(status : RazorPayPaymentStatus){
        switch status {
        case .authorized:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Payment Initiated you will getback you refund" , type: .message)
            break
        case .captured:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Payment Success" , type: .success)
            break
        case .created:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Payment unsuccessfull" , type: .error)
            break
        case .refunded:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Refund Initiated" , type: .message)
            break
        case .failed:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Payment unsuccessfull" , type: .error)
            break
        default:
            break
        }
    }
    func capturePayment(payTmResponse : String){
        if let dict = convertToDictionary(text: payTmResponse) {
            let dicts = JSON(dict).rawString()?.replacingOccurrences(of: "\\", with:"")
            if let obj = self.packageListDetail?.oneTimePackageDetail[(self.selectedIndexPAth?.row)!] {
                
                
                if let tranId = (dict as AnyObject)["TXNID"] as? String {
                    if let user = DPWebServicesManager.sharedManager.user{
                        
                        let params  = ["patientId" : user.userId, "payment_id" : tranId, "subscriptionId" : obj._id, "gateway" : "paytm", "type" : "live", "paytmResponse" : dicts]
                        let request = OneTimePackageRequest(params: params as [String : AnyObject])
                        DPWebServicesManager.getResponse(with: request) { (response, error) in
                            debugPrint(response)
                            if let response = response {
                                self.navigationController?.popViewController(animated: true)
                                //debugPrint("Response\(response)")
                                //  self.packageListDetail =  PackageListDetail(data: JSON(response["data"].dictionary))
                                //   self.changeToSelectedSegmentColor(selectedIndex: .onetime)
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    func convertToDictionary(text: String) -> Any? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
extension PackageViewController : RazorpayPaymentCompletionProtocol,ExternalWalletSelectionProtocol{
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        if walletName == "paytm"{
            getCheckSum(completionHandler: { (error, response) in
                if let checkSumObj = response,error == nil{
                    self.makePaytmOrderWith(checkSumDetails: checkSumObj)
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Failed to intiate", type: .error)
                }
            })
        }
    }
    func onPaymentError(_ code: Int32, description str: String) {
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: str, type: .error)
    }
    func onPaymentSuccess(_ payment_id: String) {
        print("paymentId\(payment_id)")
        requestForPackageWithPaymentId(payment_id)
    }
    func requestForPackageWithPaymentId(_ paymentId: String) {
        if let patientID = DPWebServicesManager.sharedManager.user?.userId{
            var subscriptionId = ""
            var params = [String : AnyObject]()
            if segmentController.selectedSegmentIndex == 0 {
                if let obj = self.packageListDetail?.oneTimePackageDetail[(self.selectedIndexPAth?.row)!] {
                    subscriptionId = obj._id
                }
                params  = ["patientId" : patientID as AnyObject, "payment_id" : paymentId as AnyObject, "subscriptionId" : subscriptionId as AnyObject, "gateway" : "razorPay" as AnyObject]
                
            }else {
            }
            ABProgressIndicator.shared.showAnimator()
            
            let request = OneTimePackageRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                debugPrint(response)
                if let response = response {
                    if error != nil {
                        TSMessage.showNotification(withTitle: error?.localizedDescription, type: .error)
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    func requestForSubscriptionWithPlanId(_ planId: String) {
        let params  = ["plan_id" : planId, "customer_notify" : 0, "total_count" : 3] as [String : AnyObject]
        let request = SubscriptionsRequest(params: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let response = response {
                if error != nil {
                    TSMessage.showNotification(withTitle: error?.localizedDescription, type: .error)
                }
                if let data = response["data"].dictionary {
                    self.recentRazorPaySubscriptionId = data["id"]?.string ?? ""
                    if let id = data["id"]?.string {
                        self.showPaymentForSubscribe(id)
                    }
                }
            }
        }
        
    }
    
}
//paytm delegates
extension PackageViewController : PGTransactionDelegate{
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        debugPrint(responseString)
        capturePayment(payTmResponse:responseString )
        controller.dismissController()
    }
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        controller.dismissController()
    }
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        debugPrint(error.localizedDescription)
    }
}
