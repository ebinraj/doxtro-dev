//
//  DPRecentConsultationExistCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class RecentConsultationExistCell : UITableViewCell{
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var helathConcernLabel: UILabel!
    @IBOutlet weak var titlelabels: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2)
    }
}
