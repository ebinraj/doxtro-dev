//
//  DPMedicalFreshChatTVCell.swift
//  DoxtroPatient
//
//  Created by Vinaykumar on 7/17/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import UIKit

class DPMedicalFreshChatTVCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var iconImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    enum CollectionViewContentPosition {
        case Left
        case Center
    }
    var collectionViewContentPosition: CollectionViewContentPosition = .Left
}
extension DPMedicalFreshChatTVCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.collectionView.dequeueReusableCell(withReuseIdentifier: "fcCVCellId", for: indexPath) as! DPMedicalFCCVCell
        item.iconButton.tag = indexPath.item
        switch indexPath.item {
        case 0:
            //item.titleLabel.text = "Order Medicine"
            item.iconButton.setImage(UIImage(named: "OrderMedicine"), for: .normal)
        case 1:
            //item.titleLabel.text = "Lab test"
            item.iconButton.setImage(UIImage(named: "Labtest"), for: .normal)
        case 2:
            //item.titleLabel.text = "Home care services"
            item.iconButton.setImage(UIImage(named: "HomecareService"), for: .normal)
        default:
            //item.titleLabel.text = ""
            item.iconButton.setImage(UIImage(named: ""), for: .normal)
        }
        return item
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth/3.4, height: 118.5)
        //return CGSize(width: screenWidth/3.4, height: screenWidth/4)
    }
}
