//
//  DPCalenderViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 14/02/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import TSMessages
import FSCalendar
class CalenderSlotController : UIViewController{
    @IBOutlet weak var noSlotFoundLabel: UILabel!
    @IBOutlet weak var calenderNewView: FSCalendar!
    @IBOutlet weak var selectedDateAndTimeLabel: UILabel!
    var selectedDat : Date = Date()
    @IBOutlet weak var selectedTimeSlotLabel: UILabel!
    var params : [String : AnyObject]?
    var doctorID : String?
    var timeSlotArray : [TimeSlot] = []
    var selectedTimeSlotObj : TimeSlot?
    var selectedDate : String?
    var selectedIndexPath : IndexPath?
    override func viewDidLoad() {
        super.viewDidLoad()
       setOnTodayDate()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    func setOnTodayDate(){
         self.calenderNewView.select(Date(), scrollToDate: true)
        self.selectedDateAndTimeLabel.text = getRequiredFormatDate(date: self.selectedDat,dateFormat: "dd MMMM YYYY")
        if let id = doctorID{
         getSlotForDoctor(date: getRequiredFormatDate(date: Date()), doctorID: id)
        }
    }
    func showSlotNotFoundLabel(){
        if self.timeSlotArray.count > 0{
            self.noSlotFoundLabel.isHidden = true
        }else{
            self.noSlotFoundLabel.isHidden = false
        }
    }
    @IBAction func bookPlannedConsultation(_ sender: Any) {
        if let timeSlotObj = self.selectedTimeSlotObj,let selectedDate = self.selectedDate,let patientId = DPWebServicesManager.sharedManager.user?.userId,let doctorObj = self.doctorID,var params = self.params{
            params["doctorId"] = doctorObj as AnyObject
            params["requestedDate"] = selectedDate as AnyObject
            params["requestedTime"] = timeSlotObj.startTime as AnyObject
        let request = PlannedConsultationRequest(params: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response,error) in
            ABProgressIndicator.shared.hideAnimator()
            if error == nil,let response = response{
                if let data = response["data"].dictionary{
                    let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                    consulattionInfoObj.patientFee = response["patientFee"].intValue
            DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                    self.performSegue(withIdentifier: "showConsultViewController", sender: consulattionInfoObj)
                }
            }
        }
        }else{
            if self.selectedTimeSlotObj == nil{
                TSMessage.showNotification(in: UIApplication.getRootViewController(), title: "Error", subtitle: "Please select time slot", type: .error)
            }
        }
    }
    func getSlotForDoctor(date : String,doctorID : String){
        self.selectedDate = date
        let params = ["doctorId":doctorID,"date":date]
        let request = GetDoctorSlotRequest(params: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            self.timeSlotArray.removeAll()
            if let response = response{
                if let response = response["data"].array{
                self.selectedIndexPath = nil
                for obj in response{
                self.timeSlotArray.append(TimeSlot(dict: obj))
                }
            }
            }
            self.collectionView.reloadData()
            self.showSlotNotFoundLabel()
           
        }
    }
    func calnderViewDidChange(){
       
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showConsultViewController"{
            if  let destinatioVC = segue.destination as? ConsultationViewController{
                destinatioVC.consulatationInfo = sender as? DpConsultationDetails
            }
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!
    
}
class SlotCell : UICollectionViewCell{
    @IBOutlet weak var timeSlotView: UIView!
    @IBOutlet weak var timeSlotLabel: UILabel!
}
extension CalenderSlotController : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let slotObj = timeSlotArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "slotCell", for: indexPath) as! SlotCell
        cell.timeSlotLabel.text = slotObj.duration
        if selectedIndexPath == indexPath{
            cell.timeSlotView.backgroundColor = UIColor(red: 39/255.0, green: 162/255.0, blue: 156/255.0, alpha: 0.5)
            cell.timeSlotView.layer.borderWidth = 0.5
            cell.timeSlotView.layer.borderColor = UIColor().greenDoxtroCgColor().cgColor
        }else{
            cell.timeSlotView.backgroundColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 0.5)
            cell.timeSlotView.layer.borderWidth = 0.5
            cell.timeSlotView.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeSlotArray.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var value : String = ""
        let timeSLotObj = timeSlotArray[indexPath.row]
        self.selectedTimeSlotObj = timeSLotObj
        self.selectedIndexPath = indexPath
        self.selectedDateAndTimeLabel.text = getRequiredFormatDate(date: self.selectedDat,dateFormat: "dd MMMM YYYY") + " , " + timeSLotObj.duration
        value = ""
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "slotCell", for: indexPath) as! SlotCell
        collectionView.reloadData()
    }
    func getRequiredFormatDate(date : Date,dateFormat : String = "YYYY-MM-dd" ) -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateFormat
    return dateFormatter.string(from: date)
    }
}
extension CalenderSlotController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width/3)
        return CGSize(width: width, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
extension CalenderSlotController : FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance{
    func minimumDate(for calendar: FSCalendar) -> Date {
       return Date()
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Calendar.current.date(byAdding: .day, value: 29, to: Date())!
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.selectedTimeSlotObj = nil
        var value : String = ""
           self.selectedDat = date
       value = getRequiredFormatDate(date: self.selectedDat,dateFormat: "dd MMMM YYYY")
        if let duration = self.selectedTimeSlotObj?.duration{
         value = value + " , " + duration
        }
        self.selectedDateAndTimeLabel.text =   value
        if let id = doctorID{
          getSlotForDoctor(date: getRequiredFormatDate(date: date), doctorID: id)
        }
        
        
    }
}
