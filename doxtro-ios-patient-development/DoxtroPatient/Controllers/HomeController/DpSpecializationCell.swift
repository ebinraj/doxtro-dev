//
//  DpSpecializationCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 12/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol DPSpecializationDelegate {
    func showFullListSpecialization()
    func selectedSpecialization(with index : Int)
}
class DpSpecializationCell: UITableViewCell{
    var viewArray : [UIView] = []
    var delegate : DPSpecializationDelegate?
    var specializationListArray : [Specialization] = [Specialization]()
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    @IBAction func showSpecializationFullList(_ sender: Any) {
        self.delegate?.showFullListSpecialization()
    }
    func configureCell(specializationListArray:[Specialization]){
         self.specializationListArray = specializationListArray
         collectionView.reloadData()
    }
}
extension DpSpecializationCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let specializationObj = specializationListArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeSpecializationCell", for: indexPath) as! HomeSpecializationCell
        setImageForCell(cell: cell, specializationObj: specializationObj)
        cell.specializationLabel.text = specializationObj.specializationName
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.specializationListArray.count
    }
    func setImageForCell(cell:HomeSpecializationCell,specializationObj : Specialization){
        if let isRechanble  = DPWebServicesManager.sharedManager.reachability?.isReachable,!isRechanble{
            if let categoryImageData = specializationObj.specializationImage{
                cell.specializationImageView.image = UIImage(data: categoryImageData as Data)
            }
        }else{
            if let categoryImageUrl = specializationObj.specilizationImageUrl{
                if let url = URL(string: categoryImageUrl){
                    cell.specializationImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "imagePlaceholder"), imageTransition: UIImageView.ImageTransition.crossDissolve(0.1))
                }
                
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         self.delegate?.selectedSpecialization(with: indexPath.row)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.height)
    }
}
