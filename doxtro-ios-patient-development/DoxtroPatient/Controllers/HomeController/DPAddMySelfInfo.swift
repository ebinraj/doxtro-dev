//
//  DPAddMySelfInfo.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 05/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
protocol RelativeUpdateInfoDelegate {
    func updateRelativeInfoDelegate(age : String,gender : String,relativeId : String)
    func removeUpdatView()
}
class AddMySelfInfoView : UIView{
    
     var sortedSegmentViews : [UIView]?
     var selectedgenderType : DPgenderType = .Male
   
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    @IBOutlet weak var ageTextField: UITextField!
    @IBAction func removeView(_ sender: Any) {
        self.delegate?.removeUpdatView()
    }
    var memberObj : DPRelativeMember?
    var delegate : RelativeUpdateInfoDelegate?
    override func layoutSubviews() {
        super.layoutSubviews()
        self.ageTextField.addBootomBorderLine(width: 2, color: UIColor.gray.cgColor)
         
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpNewsTypeSegmentController()
        self.sortedSegmentViews = genderSegmentControl.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
        changeToSelectedSegmentColor(selectedIndex: 0)
    }
    func setUpNewsTypeSegmentController(){
        genderSegmentControl.layer.masksToBounds = true
        self.genderSegmentControl.tintColor = UIColor.clear
        genderSegmentControl.backgroundColor = UIColor.white
        genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .selected)
        genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .normal)
        //NSBackgroundColorAttributeName:UIColor.magenta
    }
    @IBAction func genderSelection(_ sender: Any) {
        if sender is UISegmentedControl{
            if let segmentControl = sender as? UISegmentedControl{
                changeToSelectedSegmentColor(selectedIndex: segmentControl.selectedSegmentIndex)
            }
        }
    }
    func changeToSelectedSegmentColor(selectedIndex : Int){
        switch selectedIndex {
        case 0:
            self.selectedgenderType = .Male
            if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[1]{
                view.backgroundColor = UIColor.white
            }
            break
        case 1:
            self.selectedgenderType = .Female
            if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[0]{
                view.backgroundColor = UIColor.white
            }
            break
        default:
            break
        }
    }
    @IBAction func saveInfoClicked(_ sender: Any) {
        if self.ageTextField.text == ""{
         TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Enter Age", type: .error)
        }else{
            if let relativeId = self.memberObj?.id{
            self.delegate?.updateRelativeInfoDelegate(age: self.ageTextField.text!, gender: self.selectedgenderType.rawValue,relativeId: relativeId)
            }else{
                 TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "RelativesNoteFound", type: .error)
            }
        }
    }
}
