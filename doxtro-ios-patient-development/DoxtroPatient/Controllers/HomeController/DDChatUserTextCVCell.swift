//
//  DDChatUserTextCVCell.swift
//  DoxtroDoctor
//
//  Created by vinaykumar on 8/30/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit

class DDChatUserTextCVCell: UICollectionViewCell {
    @IBOutlet weak var userText: UILabel!
    
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var customView: DDCustomView!
}
