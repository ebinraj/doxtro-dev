//
//  DPPlanedConsultaionDoctorList.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 12/02/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class PlannedDoctorListViewController : UIViewController{
    var category : String?
    
    @IBOutlet weak var noDoctorFoundLabel: UILabel!
    var params : [String : AnyObject]?
    @IBOutlet weak var tableView: UITableView!
    var doctorList : [DPDoctorList] = []
    var array = [1,2,3,4,5]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.separatorStyle = .none
        if let category = self.category{
            getDoctorList(category: category)
        }
        self.title = "Select your Doctor"
    }
    func getDoctorList(category : String){
        if let params = self.params{
        if let consultationType = params["consultationType"] as? String{
        let params = ["category":category,"consultationType" : consultationType]
        let request = PlannedConsultationDoctorList(params:params as [String : AnyObject]  )
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if  let response = response{
                if let data = JSON(response["data"]).array{
                    self.doctorList.removeAll()
                    for obj in data{
                     self.doctorList.append(DPDoctorList(dict: obj))
                    }
                    self.tableView.reloadData()
                }
            }
            self.checkToShowNoDoctorInstruction()
        }
    }
        }
    }
    func checkToShowNoDoctorInstruction(){
        if self.doctorList.count > 0{
            self.noDoctorFoundLabel.isHidden = true
        }else{
            self.noDoctorFoundLabel.isHidden = false
        }
    }
    
}
extension PlannedDoctorListViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.doctorList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let doctor = self.doctorList[indexPath.row]
        let cell  = tableView.dequeueReusableCell(withIdentifier: "plannedDoctorCell", for: indexPath) as! PlannedDoctorCell
        cell.doctorNameLabel.text = doctor.firstName
        cell.bookButton.tag = indexPath.row
        cell.delegate = self
        var newCategory : String = ""
        var categorys : String = ""
        for category in doctor.category{
            categorys = category
            break
        }
        for (index,degree) in doctor.qualification.enumerated(){
            if index == 0{
                newCategory = degree.degree!
            }else{
                if degree.degree == "Others"{
                    newCategory = newCategory + " , " + degree.additionalData
                }else{
                    newCategory = newCategory + " , " + degree.degree!
                }
                
            }
        }
        if newCategory != "" && categorys != ""{
            categorys = categorys + "("
            newCategory =  categorys + newCategory + ")"
        }else{
            newCategory = categorys + newCategory
        }
        //newCategory = newCategory //+ " | " //+ String(describing: doctor.experience) + " yr exp"
        if let url = URL(string: doctor.profilePic){
            cell.doctorProfileImageView.af_setImage(withURL: url)
        }else{
            cell.doctorProfileImageView.image = UIImage(named: "imagePlaceholder")
        }
        cell.doctorSpecializationLabel.text = newCategory
        cell.successfulConsultationLabel.text = "\(String(describing:doctor.ratingUserCount)) Patients Consulted" + " | " + String(describing: doctor.experience) + " yr exp"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let doctor = self.doctorList[indexPath.row]
        let docotorID = doctor._id
        if docotorID != ""{
        self.performSegue(withIdentifier: "doctorProfile", sender: (doctor,params))
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "doctorProfile"{
            let destinationVC = segue.destination as! DoctorProfileViewController
            
            destinationVC.isPlanned = true
            if let doctorObj = sender as? (DPDoctorList,[String : AnyObject]){
                destinationVC.doctorObj = doctorObj.0
                destinationVC.doctorId = doctorObj.0._id
                destinationVC.doctorFee = String(describing  : doctorObj.0.consultingFee)
            }
            if let params = sender as? (DPDoctorList,[String : AnyObject]){
                destinationVC.params = params.1
            }
//            if let params = sender as? [String : AnyObject]{
//                destinationVC.params = params
//            }
//
        }else if segue.identifier == "openCalenderView"{
         let destinationVC = segue.destination as! CalenderSlotController
            if let doctorObj = sender as? (DPDoctorList,[String : AnyObject]){
             destinationVC.doctorID = doctorObj.0._id
            }
            if let params = sender as? (DPDoctorList,[String : AnyObject]){
             destinationVC.params = params.1
            }
        }
    }
}
protocol PlannerDoctorCellDelegate {
    func openCalendePage(index : Int)
}
class PlannedDoctorCell : UITableViewCell{
    @IBOutlet weak var successfulConsultationLabel: UILabel!
    @IBOutlet weak var doctorSpecializationLabel: UILabel!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorProfileImageView: UIImageView!
    var delegate : PlannerDoctorCellDelegate?
    @IBOutlet weak var bookButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
 doctorProfileImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2.0)
    }
    
    @IBAction func openCalenderView(_ sender: Any) {
        if let button = sender as? UIButton{
            self.delegate?.openCalendePage(index: button.tag)
        }
    }
}
extension PlannedDoctorListViewController : PlannerDoctorCellDelegate{
    func openCalendePage(index: Int) {
      let doctorObj = doctorList[index]
      self.performSegue(withIdentifier: "openCalenderView", sender: (doctorObj,params))
    }
}
