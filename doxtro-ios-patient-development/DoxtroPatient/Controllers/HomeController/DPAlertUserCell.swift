//
//  DPAlertUserCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class UserAlertCell : UICollectionViewCell{
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var alerUserTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 1).cgColor
        mainView.clipsToBounds = true
        userImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2)
    }
}
