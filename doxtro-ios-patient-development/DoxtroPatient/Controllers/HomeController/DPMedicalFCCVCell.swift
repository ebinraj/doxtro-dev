//
//  DPMedicalFCCVCell.swift
//  DoxtroPatient
//
//  Created by Vinaykumar on 7/17/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import UIKit

class DPMedicalFCCVCell: UICollectionViewCell {
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBAction func tapedOnFreshChatIcon(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            FreshChatController.sharedManager.presentConversationControllerWithFilters(["order_medicine"], "Order Medicines")
            
        case 1:
            FreshChatController.sharedManager.presentConversationControllerWithFilters(["lab_test"], "Lab Test")
            
        case 2:
            FreshChatController.sharedManager.presentConversationControllerWithFilters(["home_care_service"], "Home Care Service")
            
        default:
            break
        }
    }
}
