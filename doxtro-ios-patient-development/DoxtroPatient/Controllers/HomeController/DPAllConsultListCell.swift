//
//  DPAllConsultListCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class AllConsultCell : UITableViewCell{
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var unreadMsgCountView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var unreadMessageCountLabel: UILabel!
    @IBOutlet weak var concernLabel: UILabel!
    @IBOutlet weak var cconsulatationStatusLabel: UILabel!
    @IBOutlet weak var imageConsultationType: UIImageView!
    @IBOutlet weak var relationAgeLabel: UILabel!
    @IBOutlet weak var relationAgeView: UIView!
    @IBOutlet weak var consulataionSatausView: UIView!
    //var messageCount : Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        relationAgeView.layer.borderColor = UIColor.darkGray.cgColor
        relationAgeView.layer.borderWidth = 1
        relationAgeView.clipsToBounds = true
        self.profileImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 1)
    }
//    func updateMessageCount(){
//        if let msgCount = messageCount{
//            if msgCount > 0{
//                self.unreadMessageCountLabel.text = String(describing:msgCount)
//                self.unreadMsgCountView.isHidden = false
//            }else{
//                self.unreadMsgCountView.isHidden = true
//            }
//        }else{
//            self.unreadMsgCountView.isHidden = true
//        }
//    }
}
