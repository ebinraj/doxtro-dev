//
//  DPDoctorProfile.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class DoctorProfileCollectionViewCell
 : UICollectionViewCell  {
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var requestCall: UILabel!
    @IBOutlet weak var doctorNameLAbel: UILabel!
    @IBOutlet weak var doctorprofileImage: UIImageView!
    @IBOutlet weak var specializationInfoLabel: UILabel!
    var doctorId : String = ""
    @IBOutlet weak var experienceLabel: UILabel!
    var delegate : DoctorProfileDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        doctorprofileImage.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        let path = UIBezierPath(roundedRect:self.requestCall.bounds,
                               byRoundingCorners:[.topRight, .bottomRight],
                                cornerRadii: CGSize(width: 20, height:  20))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.requestCall.layer.mask = maskLayer
        let Buttonpath = UIBezierPath(roundedRect:self.phoneButton.bounds,
                                       byRoundingCorners:[.topLeft, .bottomLeft],
                                      cornerRadii: CGSize(width: 20, height:  20))
        let buttonmaskLayer = CAShapeLayer()
        buttonmaskLayer.path = Buttonpath.cgPath
        self.phoneButton.layer.mask = buttonmaskLayer
    }
    @IBAction func viewFullprofileDoctor(_ sender: Any) {
        self.delegate?.showDoctorProfile(wirh: doctorId)
    }
    @IBOutlet weak var viewFullProfile: UIButton!
    
    @IBAction func requestCall(_ sender: Any) {
        self.requestCallClicked!()
    }
     var requestCallClicked : (() -> Void)?
}
