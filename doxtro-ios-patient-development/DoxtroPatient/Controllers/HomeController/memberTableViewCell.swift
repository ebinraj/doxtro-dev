//
//  memberTableViewCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 21/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
protocol DPAddNewMemberDelegate  {
    func removeAddNewMemberView()
    func saveAddNewMemberInfo(params : [String : AnyObject])
}
protocol DPSelectedRealtiveMemberDelegate {
    func setSelectedRelativeMember(relativeMemberObj : DPRelativeMember)
    func openEditProfilePage()
}
let REFRESH_RELATIVE_MEMBER_NOTIFICATION_KEY = "RefreshRelativeMemberList"
class DPMemberTableViewCell: UITableViewCell {
    @IBOutlet weak var addMemberView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var relativeMemberArray : [DPRelativeMember] = []
    var selectedIndexPath : IndexPath?
    var selectedMemberObj : DPRelativeMember?
    var delegate : DPSelectedRealtiveMemberDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleAddNewMemberViewTap(sender:)))
        addMemberView.addGestureRecognizer(tap)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        getRelativeList()
        addObserverForRefreshListOfRelativeMembers()
    }
    func addObserverForRefreshListOfRelativeMembers(){
        NotificationCenter.default.addObserver(self, selector: #selector(DPMemberTableViewCell.refreshMemberList), name: NSNotification.Name(rawValue: "RefreshRelativeMemberList"), object: nil)
    }
    func refreshMemberList(){
        getRelativeList()
    }
    
    func handleAddNewMemberViewTap(sender: UITapGestureRecognizer) {
        //commented for checking my self info before adding
//        var isMyself = false
//        for value in self.relativeMemberArray{
//            if value.relation == "MySelf"{
//                isMyself = true
//            }
//        }
//        if !isMyself{
//              self.checkForMySelfInfo()
//        }else{
             showAddNewMemeberView()
        //}
    }
    func showAddNewMemeberView() {
        let view =  Bundle.main.loadNibNamed("DPAddNewMemberView", owner: nil, options: nil)
        if let mainView = view?[0] as? DpAddMemberView{
            mainView.delegate = self
            mainView.frame = (UIApplication.getTopViewController()?.view.bounds)!
            UIApplication.getTopViewController()?.view.addSubview(mainView)
        }
        
    }
    
    func getRelativeList(){
        DPWebServicesManager.sharedManager.getRelativeList(completionHandler: { (error, response) in
            if let response = response{
                self.relativeMemberArray = response
                self.changeToMyselfMode()
                self.checkForMySelfInfo()
                if let selectedObj = self.relativeMemberArray.first{
                    self.selectedIndexPath = nil
                    self.selectedMemberObj = selectedObj
                    self.delegate?.setSelectedRelativeMember(relativeMemberObj: selectedObj)
                }
                self.collectionView.reloadData()
            }
        })
    }
    func changeToMyselfMode(){
        self.relativeMemberArray.reverse()
    }
    func checkForMySelfInfo(){
        for value in self.relativeMemberArray{
            if value.relation == "Myself"{
                if let age = value.age,let gender = value.gender{
                    if age == 0 && gender == ""{
                   self.showMySelfView(memberInfo: value)
                    }
                }
            }
        }
    }
}
//Update Member
extension DPMemberTableViewCell : RelativeUpdateInfoDelegate{
    func removeUpdatView() {
        rmoveRelativeUpdateView()
    }
    
    func updateRelativeInfoDelegate(age: String, gender: String, relativeId: String) {
        let params = ["age" : age,"gender" : gender,"_id" : relativeId ]
        let request = UpdateRelativeMemberInfoRequest(params: params as [String : AnyObject])
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            self.getRelativeList()
            DPWebServicesManager.sharedManager.updateUserProfile()
        }
        rmoveRelativeUpdateView()
    }
    func rmoveRelativeUpdateView(){
        for memeberView in (UIApplication.getTopViewController()?.view.subviews)!{
            if memeberView is AddMySelfInfoView{
                memeberView.removeFromSuperview()
            }
        }
    }
    func showMySelfView(memberInfo: DPRelativeMember) {
        let view =  Bundle.main.loadNibNamed("DPMySelfView", owner: nil, options: nil)
        if let mainView = view?[0] as? AddMySelfInfoView{
            mainView.delegate = self
            mainView.memberObj = memberInfo
            mainView.frame = (UIApplication.getTopViewController()?.view.bounds)!
           UIApplication.getTopViewController()?.view.addSubview(mainView)
        }
}
}
extension DPMemberTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.relativeMemberArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memberCillectionViewCell", for: indexPath) as? DPMemberCollectionViewCell
         cell?.delegate = self
        cell?.indexPath = indexPath
        let memberObj = self.relativeMemberArray[indexPath.row]
        if let relation = memberObj.relation,let gender = memberObj.gender{
            cell?.memberImageView?.image = relation.imageForRelation(with: gender)

        }
               if memberObj.relation == "Myself"{
                    cell?.memberNameLabel?.text = memberObj.relation
                }else{
                cell?.memberNameLabel?.text = memberObj.name
        }
        if self.selectedIndexPath == nil{
            if indexPath.row == 0{
                self.selectedIndexPath = indexPath
            }
        }
        cell?.configureCellSelection(selectedIndexPath: self.selectedIndexPath, currentIndexPath: indexPath)
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? DPMemberCollectionViewCell
        cell?.memberImageView?.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor)
        self.selectedIndexPath = indexPath
        let selectedMemberObj  = self.relativeMemberArray[indexPath.row]
        self.selectedMemberObj = selectedMemberObj
        if selectedMemberObj.relation == "Myself"{
            if let age = selectedMemberObj.age , let gender = self.selectedMemberObj?.gender{
                if age == 0 || gender == ""{
                self.showMySelfView(memberInfo: selectedMemberObj)
                }
            }
        }
        if let selectedMemberObj = self.selectedMemberObj{
            self.delegate?.setSelectedRelativeMember(relativeMemberObj: selectedMemberObj)
        }
        collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/4, height: collectionView.bounds.height)
    }
}
extension DPMemberTableViewCell : DPAddNewMemberDelegate{
    func saveAddNewMemberInfo(params : [String : AnyObject]) {
        let createMemberrequest  = createMemberRequest(params: params)
        DPWebServicesManager.getResponse(with: createMemberrequest, completionHandler: { (response, error) in
            if let _ = response?["data"]{
                
            }
            self.getRelativeList()
            self.removeAddNewMemberView()
        })
    }
    func removeAddNewMemberView(){
        for memeberView in (UIApplication.getTopViewController()?.view.subviews)!{
            if memeberView is DpAddMemberView{
                memeberView.removeFromSuperview()
            }
        }
    }
}
extension String{
    func  imageForRelation(with Gender : String)->UIImage{
        switch self{
        case "Son","Daughter" :
            if Gender == "Male"{
                return UIImage(named: "childmale")!
            }else{
                return UIImage(named: "childfemale")!
            }
        case "Dad","Mom":
            if Gender == "Male"{
                return UIImage(named: "parentsmale")!
            }else{
                return UIImage(named: "parentsfemale")!
            }
        case "Brother","Sister":
            if Gender == "Male"{
                return UIImage(named: "sibilingsmale")!
            }else{
                return UIImage(named: "sibilingsfemale")!
            }
        case "Myself","Spouse","MySelf" :
            if Gender == "Male"{
                return UIImage(named: "myselfmale")!
            }else{
                return UIImage(named: "myselffemale")!
            }
        case "Chat","Planed-Chat" :
            return UIImage(named: "textconsultation")!
            
        case "Audio","Planned-Audio" :
            return UIImage(named: "audioconsultation")!
            
        default :
            return UIImage(named: "DummyUser")!
        }
    }
}
extension DPMemberTableViewCell {
    func addMySelfMember(){
        var isMyself = false
        for value in self.relativeMemberArray{
            if value.relation == "Myself"{
                isMyself = true
            }
        }
        if !isMyself{
            if let user = DPWebServicesManager.sharedManager.user{
                if let age = user.age,let gender = user.gender,let name = user.name{
                    if age != "" && name != "" && gender != "" {
                        let params  : [String : String] = ["patientId":user.userId ?? "","relation": "Myself",
                                                           "name": "Myself",
                                                           "age": age ,
                                                           "gender": gender]
                        self.saveAddNewMemberInfo(params : params as [String : AnyObject])
                    }
                }
            }
        }
    }
}
extension DPMemberTableViewCell : ShowLongPressView,LongPressDeleteDelgate{
    func longPressView(row : Int) {
        let memberObj  = self.relativeMemberArray[row]
        showAddNewMemeberViews(with: memberObj)
    }
    func removeLongPressView() {
        if let views = UIApplication.getTopViewController()?.view{
        for memeberView in views.subviews{
            if memeberView is LongPressMemberView{
                memeberView.removeFromSuperview()
            }
        }
        }
    }
    func showAddNewMemeberViews(with relativeObj : DPRelativeMember) {
        let view =  Bundle.main.loadNibNamed("DPLongPressMemberView", owner: nil, options: nil)
        if let mainView = view?[0] as? LongPressMemberView{
            mainView.delegate = self
            mainView.nameLabel.text = relativeObj.name
            mainView.ageLabel.text = String(describing: relativeObj.age!)
            mainView.genderLabel.text = relativeObj.gender
            mainView.relationLabel.text = relativeObj.relation
            if let views = UIApplication.getTopViewController()?.view{
            mainView.frame = views.bounds
            views.addSubview(mainView)
            }
        }
        
    }
}
