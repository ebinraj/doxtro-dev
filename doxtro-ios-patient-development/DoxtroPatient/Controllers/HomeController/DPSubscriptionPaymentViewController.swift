//
//  DPSubscriptionPaymentViewController.swift
//  DoxtroPatient
//
//  Created by vinaykumar on 2/3/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import UIKit
import TSMessages
import SwiftyJSON
class DPSubscriptionPaymentViewController: UIViewController {
    @IBOutlet var dropDownButtons: [UIButton]!
    @IBOutlet weak var dropDownImage: UIImageView!
    @IBOutlet weak var selectButton: DDHighlightedButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var packageTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var paymentCycleLabel: UILabel!
    @IBOutlet weak var proceedToPayButton: UIButton!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var dropDownView: UIView!
    var subscription : Subscription?

    @IBOutlet weak var durationLabel: UILabel!
    var totalCount: Int = 0
    var recentRazorPaySubscriptionId = ""
    var razorpay : Razorpay!
    struct RazorPayPayment {
        struct KeyID {
            static let TEST = "rzp_test_AHmF8XvSfeTdOc"
            static let LIVE = "rzp_live_mr1w5Vcvhkac5j"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       //Configure the button
          setUpComponents()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpComponents() {
        if let text = subscription?.text {
            titleLabel.text = text
        }
        if let subscriptionTag = subscription?.subscriptionTag {
            packageTitleLabel.text = subscriptionTag
        }
        if let price = subscription?.price {
            priceLabel.text = "₹ " + String(describing: price)
            if let priceText = priceLabel.text {
                proceedToPayButton.setTitle("PROCEED TO PAY \(priceText)", for: .normal)
            }
            
        }
        if let paymentCycle = subscription?.paymentCycle {
            paymentCycleLabel.text = "PAYMENT CYCLE : \(paymentCycle.uppercased())"
        }
        totalCount = 3
        selectButton.setTitle("3 Months", for: .normal)
        stackView.superview?.bringSubview(toFront: stackView)
        dropDownImage.tintColor = UIColor.lightGray
        razorpay = Razorpay.initWithKey(RazorPayPayment.KeyID.LIVE, andDelegate: self)
        self.navigationItem.title = "Payment"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: .plain, target: self, action: #selector(tappedOnLeftBarButtonItem))
    }
    func tappedOnLeftBarButtonItem() {
      self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewWillLayoutSubviews()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // self.dropDownButton.removeFromSuperview()
       
    }
    @IBAction func dropDownButtonsTapped(_ sender: UIButton) {
        guard let title = sender.currentTitle else {
            return
        }
        selectButton.setTitle(title, for: .normal)
        switch title {
        case "3 Months":
            totalCount = 3
            break
        case "6 Months":
            totalCount = 6
            break
        case "1 Year":
            totalCount = 12
            break
        default:
            break
        }
        dropDownButtons.forEach { (button) in
            UIView.animate(withDuration: 0.3, animations: {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func handleSelectButton(_ sender: DDHighlightedButton) {
        dropDownButtons.forEach { (button) in
            UIView.animate(withDuration: 0.3, animations: {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            })
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func proceedToPayAction(_ sender: UIButton) {
        if let planId = subscription?.planId {
            if totalCount == 0 {
                TSMessage.showNotification(withTitle: "Please select duration", type: .error)
                return
            }
            requestForSubscriptionWithPlanId(planId)
        }
    }
}
extension DPSubscriptionPaymentViewController {
    func requestForSubscriptionWithPlanId(_ planId: String) {
        ABProgressIndicator.shared.showAnimator()
        let params  = ["plan_id" : planId, "customer_notify" : 0, "total_count" : totalCount] as [String : AnyObject]
        let request = SubscriptionsRequest(params: params as [String : AnyObject])
        
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()

            debugPrint(response)
            if let response = response {
                if error != nil {
                    TSMessage.showNotification(withTitle: error?.localizedDescription, type: .error)
                }
                if let data = response["data"].dictionary {
                    self.recentRazorPaySubscriptionId = data["id"]?.string ?? ""
                    if let id = data["id"]?.string {
                        self.showPaymentForSubscribe(id)
                    }
                }
                
                //debugPrint("Response\(response)")
                //  self.packageListDetail =  PackageListDetail(data: JSON(response["data"].dictionary))
                //   self.changeToSelectedSegmentColor(selectedIndex: .onetime)
            }
        }
    }
    func showPaymentForSubscribe(_ subscriptionId: String) {
        let imageLogo = UIImage(named: "navigationlogo")
        var options = ["image":imageLogo,
                       "name" :"Doxtro",
                       "description": "Fee",
                       "theme" : ["color" : "#27A29C"],
                       "prefill" : [:],
                       "subscription_id" : subscriptionId] as [String : Any]
        if var PrefillDictionary = options["prefill"] as? [String : String]{
            if let user = DPWebServicesManager.sharedManager.user{
                if let prefillEmail = user.emailId{
                    PrefillDictionary["email"] = prefillEmail
                }
                if let prefillMobileNumber = user.mobileNum{
                    PrefillDictionary["contact"] =  prefillMobileNumber
                }
                options["prefill"] = PrefillDictionary
            }
        }
        razorpay.open(options)
    }
}
extension DPSubscriptionPaymentViewController : RazorpayPaymentCompletionProtocol{
    func onPaymentError(_ code: Int32, description str: String) {
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: str, type: .error)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        print("paymentId\(payment_id)")
        requestForPackageWithPaymentId(payment_id)
    }
    func requestForPackageWithPaymentId(_ paymentId: String) {
        if let patientID = DPWebServicesManager.sharedManager.user?.userId{
            var subscriptionId = ""
            var params = [String : AnyObject]()
            if let id = subscription?._id {
                subscriptionId = id
            }
            ABProgressIndicator.shared.showAnimator()
            params  = ["patientId" : patientID as AnyObject, "payment_id" : paymentId as AnyObject, "subscriptionId" : subscriptionId as AnyObject, "gateway" : "razorPay" as AnyObject, "razorPaySubscriptionId" : self.recentRazorPaySubscriptionId as AnyObject]
            let request = OneTimePackageRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let _ = response {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                if let dict = response?.dictionary{
                    self.paidFeesEventTrack(response: dict,paymentMode: "razorPay")
                }
            }
        }
    }
    func paidFeesEventTrack(response: [String : JSON],paymentMode : String = "") {
        var values = [String : AnyObject]()
        values["on click"] = true as AnyObject
        if let data = response["data"]?.dictionary {
            if let amount = subscription?.price{
                values["af_revenue"] = amount as AnyObject
            }
            values["af_currency"] = "INR" as AnyObject
            //values["Specialization name"] = data["specializationCategory"]?.string as AnyObject
        }
        values["Payment Mode"] = paymentMode as AnyObject
        values["Purchase Type"] = "Subscription" as AnyObject
        DPAppEventLog.shared.logEventName(EVENT_PAID_FEES, values: values)
        //Using Doxtro Cash
    }
}

