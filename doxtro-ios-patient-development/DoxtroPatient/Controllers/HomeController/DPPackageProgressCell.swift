//
//  DPPackageProgressCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class PackageProgressCell : UITableViewCell{
    @IBOutlet weak var packageTypeLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var renewDateLabel: UILabel!
    @IBOutlet weak var remainingConsultationLablel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var renewButton: UIButton!
    
    func configureProgressBar(packageDetail : BoughtPackageDeatail){
        progressBar.tintColor = packageDetail.progressBarColor
        let progress : Float = Float((packageDetail.totalAvailConsultation/packageDetail.totalConsultations))
        progressBar.setProgress(progress, animated: true)
        
    }
}
