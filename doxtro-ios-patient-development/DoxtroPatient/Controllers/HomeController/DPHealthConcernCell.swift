//
//  DPHealthConcernCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 28/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol DpHealthCncernDelegateDelegate {
    func setHelathCocern(helathConcernText : String)
    func forChangeSubmitButtonStatus(text : UITextView)
}
class DPHelthConcernCell : UITableViewCell,UITextViewDelegate{
    var defaultText : String = "Enter your health concern here "
    var delegate : DpHealthCncernDelegateDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        helathConcernTextView.text = defaultText
        helathConcernTextView.delegate = self
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.delegate?.forChangeSubmitButtonStatus(text: textView)
        if textView.text == defaultText{
            textView.text = ""
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        self.delegate?.forChangeSubmitButtonStatus(text: textView)
        self.delegate?.setHelathCocern(helathConcernText : textView.text)
    }
    @IBOutlet weak var helathConcernTextView: UITextView!
}
protocol DPSubmitDetailsCellDelegate {
    func submitDeatails()
}
class DPSubmitButtonCell : UITableViewCell{
    var delegate : DPSubmitDetailsCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBOutlet weak var plannedButton: UIButton!
    @IBAction func submitDetailsForChat(_ sender: Any) {
        self.delegate?.submitDeatails()
    }
    @IBOutlet weak var submitButton: UIButton!
}
