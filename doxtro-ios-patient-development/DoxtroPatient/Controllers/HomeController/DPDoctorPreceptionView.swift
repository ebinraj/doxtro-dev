//
//  DPDoctorPreceptionView.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 13/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol ShowFullPrecriptionDelegate{
    func showFullPrescription(reportType : ReportType)
    func setReminder()
    func deleteRminder()
}
class DoctorPreceptionCell : UICollectionViewCell{
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var dunsLabel: UILabel!
    
    @IBOutlet weak var mainTimeLabel: UILabel!
    var reportType : ReportType?
    @IBAction func setReminderClicked(_ sender: Any) {
        self.delegate?.setReminder()
    }
    var delegate : ShowFullPrecriptionDelegate?
    @IBAction func expandButtonClicked(_ sender: Any) {
        if let reportType  = self.reportType{
        self.delegate?.showFullPrescription(reportType : reportType)
        }
    }
    @IBOutlet weak var reminderView: UIView!
    @IBOutlet weak var serReminder: UIButton!
    @IBOutlet weak var expandButtonClicked: UIButton!
    @IBOutlet weak var doctorInfoView: UIView!
    @IBOutlet weak var DoctorName: UILabel!
    @IBOutlet weak var qualificationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var patientInfoLabel: UILabel!
    
    @IBOutlet weak var medicineName: UILabel!
    @IBOutlet weak var maedicineDuration: UILabel!
    @IBOutlet weak var doseTime: UILabel!
    
    @IBOutlet weak var reminderStackView: UIStackView!
    @IBAction func deletReminder(_ sender: Any) {
        self.delegate?.deleteRminder()
    }
    @IBOutlet weak var timeTakenLabel: UILabel!
    @IBOutlet weak var prescriptionTextLabel: UILabel!
    @IBOutlet weak var pillsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeBorder(borderView: bannerView)
        makeBorder(borderView: doctorInfoView)
        makeBorder(borderView: mainView)
         let addTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.clickedPrescription))
         self.mainView.addGestureRecognizer(addTapGesture)
    }
    func clickedPrescription(){
        if let reportType = self.reportType{
            self.delegate?.showFullPrescription(reportType : reportType)
        }
    }
    func makeBorder(borderView : UIView){
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor().greenDoxtroCgColor().cgColor
    }
}
