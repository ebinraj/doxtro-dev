//
//  DPPackageDoxtroCashViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 20/03/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
import SwiftyJSON
class PackagePaymentViewController: UIViewController{
    var paybleAmount : String?
    var razorpay : Razorpay!
    var ORDER_NUMBER : String = ""
    var recentRazorPaySubscriptionId = ""
    var packageListDetail : PackageListDetail?
    var selectedIndexPAth : IndexPath?
    var couponPaymentData : PaymentData?
    @IBOutlet weak var afterApplyCouponView: UIStackView!
    var couponInfo : CouponInfo?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fotterTitleLabel: UILabel!
    @IBOutlet weak var doxtroCashLabel: UILabel!
    @IBOutlet weak var usedDoxtroCash: UILabel!
    @IBOutlet weak var paymentButton: UIButton!
    @IBOutlet weak var totalPaybleLabel: UILabel!
    //@IBOutlet weak var doxtroCash: UILabel!
    @IBOutlet weak var ConsultationFee: UILabel!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var doctorSpecializationLabel: UILabel!
    
    
    @IBOutlet weak var couponDiscount: UILabel!
    @IBOutlet weak var applyCouponButton: UIButton!
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var removeCouponBUtton: UIButton!
    @IBOutlet weak var couponNameLabel: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentButton.layer.cornerRadius = 4
        couponTextField.addBootomBorderLine(width: 1, color:(UIColor().grayDoxtroCgColor()?.cgColor)!)
       // doctorImageView.makeCircleandBorder(with: (UIColor().grayDoxtroCgColor()?.cgColor)!, width: 2)
        self.tabBarController?.tabBar.isHidden = true
        updateDefaultValue()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let path = UIBezierPath(roundedRect:self.couponNameLabel.bounds,
                                byRoundingCorners:[.topLeft, .bottomLeft],
                                cornerRadii: CGSize(width: 20, height:  20))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.couponNameLabel.layer.mask = maskLayer
        let Buttonpath = UIBezierPath(roundedRect:self.removeCouponBUtton.bounds,
                                      byRoundingCorners:[.topRight, .bottomRight],
                                      cornerRadii: CGSize(width: 20, height:  20))
        let buttonmaskLayer = CAShapeLayer()
        buttonmaskLayer.path = Buttonpath.cgPath
        self.removeCouponBUtton.layer.mask = buttonmaskLayer
    }
    @IBAction func removeCoupon(_ sender: Any) {
        //updateDefaultValue()
        if let paymenTdata = self.couponPaymentData{
            removeCoupon()
        }
    }
    @IBAction func applyCouponClicked(_ sender: Any){
        applyCoupon()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let packageDetail = self.packageListDetail,let selectedIndex = self.selectedIndexPAth{
         self.titleLabel.text = packageDetail.oneTimePackageDetail[selectedIndex.row].subscriptionTag
            self.doctorSpecializationLabel.text = packageDetail.oneTimePackageDetail[selectedIndex.row].duration
        }
        razorpay = Razorpay.initWithKey(RazorPayPayment.KeyID.LIVE, andDelegate: self)
         razorpay.setExternalWalletSelectionDelegate(self)
    }
    func removeCoupon(){
        if let couponInfo = self.couponPaymentData?.couponUsed,let user = DPWebServicesManager.sharedManager.user,let selectedIndexPathpackage = self.selectedIndexPAth,let packageDeatil = self.packageListDetail?.oneTimePackageDetail {
            if packageDeatil.indices.contains(selectedIndexPathpackage.row){
                let packageObj = packageDeatil[(selectedIndexPathpackage.row)]
                let params = ["couponTag":couponInfo,"packageId" : packageObj._id,"patientId":user.userId] as [String : AnyObject]
                let request = RemoveCouponRequest(params: params)
                ABProgressIndicator.shared.showAnimator()
                DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                    ABProgressIndicator.shared.hideAnimator()
                    if let response = response?.dictionary{
                        if let data = response["data"]?.dictionary{
                            let paymentData = PaymentData(dict: JSON(data))
                            self.switchToNocouponView(paymentData: paymentData)
                            self.couponPaymentData = paymentData
                            self.updateDefaultValue()
                        }
                    }
                })
            }
        }
    }
    func applyCoupon(){
        if let user = DPWebServicesManager.sharedManager.user,let packageDeatil = self.packageListDetail?.oneTimePackageDetail,let selectedIndexPathpackage = self.selectedIndexPAth {
            if packageDeatil.indices.contains(selectedIndexPathpackage.row){
                let packageObj = packageDeatil[(selectedIndexPathpackage.row)]
            ABProgressIndicator.shared.showAnimator()
            let parameters = [ "couponTag": self.couponTextField.text ?? "","packageId" : packageObj._id,"patientId":user.userId ?? "","gender":user.gender ?? ""]
            let request = ApplyCouponRequest(params: parameters as [String : AnyObject] )
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                self.couponTextField.text = ""
                if error == nil ,let response = response?["data"].dictionary{
                    let couponInfo = CouponInfo(dict: JSON(response))
                    self.UpdateCouponInfoValue(with: couponInfo)
                    self.changeTextFieldAfterApplyCoupon(couponTag: couponInfo.couponTag)
                }else {
                    if let message = response?["message"].string{
                        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:message , type: .error)
                    }
                }
            }
        }
    }
}
    func UpdateCouponInfoValue(with couponinfo : CouponInfo){
        self.couponInfo = couponinfo
        self.ConsultationFee.text = "₹\(String(describing: couponinfo.consultingFee))"
        self.couponDiscount.text = "- ₹\(String(describing: couponinfo.couponDiscount))"
        self.totalPaybleLabel.text = "₹\(String(describing: couponinfo.totalPayable))"
        self.doxtroCashLabel.text = "₹" + String(describing:  couponinfo.remainingDoxtroCash)
        self.usedDoxtroCash.text =   "₹" + String(describing:  couponinfo.usedDoxtroCash)
        if couponinfo.isFree{
            self.paymentButton.setTitle("START CONSULTATION", for: .normal)
        }else{
            self.paymentButton.setTitle("PAY ₹\(String(describing: couponinfo.totalPayable)) TO CONSULT", for: .normal)
        }
        self.paybleAmount = String(describing: couponinfo.totalPayable)
        self.couponDiscount.isHidden = false
        // self.applyCouponButton.isHidden = true
    }
    func changeTextFieldAfterApplyCoupon(couponTag : String){
       // self.couponTextField.isHidden = true
        self.afterApplyCouponView.isHidden = false
        self.couponNameLabel.clipsToBounds = true
        self.couponNameLabel.text = ("  " + couponTag + "    ")
        self.couponNameLabel.backgroundColor = UIColor().greenDoxtroCgColor()
        self.removeCouponBUtton.backgroundColor = UIColor().greenDoxtroCgColor()
    }
    func switchToNocouponView(paymentData : PaymentData){
        self.couponInfo = nil
        self.paybleAmount = String(describing: paymentData.consultingFee)
       self.couponDiscount.isHidden = true
        self.applyCouponButton.isHidden = false
        self.afterApplyCouponView.isHidden = true
        self.couponTextField.isHidden = false
        self.paymentButton.setTitle("PAY ₹\(paymentData.consultingFee) TO CONSULT", for: .normal)
        self.totalPaybleLabel.text = "₹" + ("\(paymentData.patientFee)")
        self.paybleAmount = String(describing: paymentData.patientFee)
        self.paymentButton.setTitle("PAY ₹\(String(describing: paymentData.patientFee)) TO CONSULT", for: .normal)
    }

    func updateDefaultValue(){
        if let paymentdata = self.couponPaymentData{
            self.paybleAmount = String(describing: paymentdata.amount)
            self.ConsultationFee.text = "₹" + ("\(paymentdata.price)")
            self.totalPaybleLabel.text = "₹" + ("\(paymentdata.amount)")
            self.doxtroCashLabel.text = "₹" + ("\(paymentdata.remainingDoxtroCash)")
            if paymentdata.usedDoxtroCash <= 0 {
                self.usedDoxtroCash.text = " ₹" + ("\(paymentdata.usedDoxtroCash)")
            }else{
                self.usedDoxtroCash.text = "- ₹" + ("\(paymentdata.usedDoxtroCash)")
            }

            let isFree = paymentdata.isFree
            if  isFree{
                self.paymentButton.setTitle("START CONSULTATION", for: .normal)
            }else{
                self.paymentButton.setTitle("PAY ₹\(paymentdata.amount) TO CONSULT", for: .normal)
            }
            
        }
        guard let _ = self.couponInfo else{
            self.afterApplyCouponView.isHidden = true
            return
        }
    }
    @IBAction func paymentButtonClicked(_ sender: Any) {
        if let amount = self.paybleAmount{
            if let intAmount = Int(amount){
                if intAmount <= 0 {
                    makeFreepayment()
                }else{
                    showPaymentForm()
                }
            }
        }
    }
    
    func showPaymentForm() {
        let imageLogo = UIImage(named: "navigationlogo")
        var options = ["image":imageLogo,
                       "name" :"Doxtro",
                       "description": "Fee",
                       "theme" : ["color" : "#27A29C"],
                       "prefill" : [:],
                       "external" : ["wallets" : ["paytm"]]] as [String : Any]
        if var PrefillDictionary = options["prefill"] as? [String : String]{
            if let user = DPWebServicesManager.sharedManager.user{
                if let prefillEmail = user.emailId{
                    PrefillDictionary["email"] = prefillEmail
                }
                if let prefillMobileNumber = user.mobileNum{
                    PrefillDictionary["contact"] =  prefillMobileNumber
                }
                options["prefill"] = PrefillDictionary
            }
        }
        
        if let amount = self.paybleAmount{
            if let intAmount = Int(amount){
                options["amount"] = (intAmount * 100)
            }
            razorpay.open(options)
        }
    }
    func getCheckSum(completionHandler : @escaping(_ error : Error?,_ response : CheckSumPaytm?)-> Void){
        if let user = DPWebServicesManager.sharedManager.user{
            let  randomNumber = (arc4random() % 9999999) + 1;
            ORDER_NUMBER = String(format: "Doxtro_%d", randomNumber)
            let params = ["customerId":user.userId
                ,"amount":self.paybleAmount,
                 "email":user.emailId,
                 "mobileNumber":"8123632112",
                 "orderId" :ORDER_NUMBER,
                 "type" : "live"]
            let request = GetChecksumPaaytmRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if error == nil,let response = response{
                    if let dict = response.dictionary{
                        let checksumObject = CheckSumPaytm(dict: JSON(dict["data"]?.dictionary))
                        completionHandler(nil,checksumObject)
                    }
                }else{
                    completionHandler(error,nil)
                }
            })
        }
    }
    func makePaytmOrderWith(checkSumDetails : CheckSumPaytm){
        if let user = DPWebServicesManager.sharedManager.user{
            let orderDict = ["MID":checkSumDetails.MID,
                             "CHANNEL_ID":checkSumDetails.CHANNEL_ID,
                             "INDUSTRY_TYPE_ID":checkSumDetails.INDUSTRY_TYPE_ID,
                             "WEBSITE":checkSumDetails.WEBSITE,
                             "TXN_AMOUNT":checkSumDetails.TXN_AMOUNT,
                             "ORDER_ID":checkSumDetails.ORDER_ID,
                             "CALLBACK_URL":checkSumDetails.CALLBACK_URL,
                             "CHECKSUMHASH":checkSumDetails.CHECKSUMHASH,
                             "REQUEST_TYPE":checkSumDetails.REQUEST_TYPE,
                             "CUST_ID":checkSumDetails.CUST_ID,
                             "EMAIL":user.emailId,
                             "MOBILE_NO":"8123632112"]
            if let order = PGOrder(params: orderDict){
                intiatePaymentWithOrder(order: order)
            }
        }
    }
    func intiatePaymentWithOrder(order : PGOrder){
        let viewBar = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
        viewBar.backgroundColor = UIColor().greenDoxtroCgColor()
        let button = UIButton(frame: CGRect(x: 5, y: 24, width: 70, height: 40))
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.textColor = UIColor.black
        button.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14.0)
        let tileLabel = UILabel(frame: CGRect(x: self.view.frame.width/2 - 50, y: 20, width: 100, height: 50))
        tileLabel.text = "Doxtro"
        tileLabel.font = UIFont(name: "Helvetica-Bold", size: 18.0)
        tileLabel.textColor = UIColor.white
        tileLabel.textAlignment = NSTextAlignment.center
        viewBar.addSubview(tileLabel)
        var merchantObj = PGMerchantConfiguration.default()
        if let transactionController = PGTransactionViewController.init(transactionFor: order){
            transactionController.topBar = viewBar
            transactionController.cancelButton = button
            transactionController.serverType = eServerTypeProduction
            transactionController.merchant = merchantObj
            transactionController.loggingEnabled = true
            transactionController.delegate = self
            self.present(transactionController, animated: true, completion: nil)
        }
    }
    func showPaymenStatusMessage(status : RazorPayPaymentStatus){
        switch status {
        case .authorized:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Payment Initiated you will getback you refund" , type: .message)
            break
        case .captured:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Payment Success" , type: .success)
            break
        case .created:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Payment unsuccessfull" , type: .error)
            break
        case .refunded:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Refund Initiated" , type: .message)
            break
        case .failed:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Payment unsuccessfull" , type: .error)
            break
        default:
            break
        }
    }
    @IBAction func initateAddMoney(_ sender: Any) {
        let destinationVc = self.storyboard?.instantiateViewController(withIdentifier: "doxtroCashController") as! DoxtroCashVIewController
       // destinationVc.consultationId = self.createdConsultationInfo?.consultationId
        if let paymentdata = self.couponPaymentData{
            destinationVc.doxtroCash = paymentdata.doxtroCash
        }
        destinationVc.completionHandler = {(doxtroCash,paymentData) in
            self.refreshDoxtroCAsh()

            
        }
        self.present(destinationVc, animated: true, completion: nil)
    }
    
    func refreshDoxtroCAsh(){
        if let userID = DPWebServicesManager.sharedManager.user?.userId,let obj = self.packageListDetail?.oneTimePackageDetail[(self.selectedIndexPAth?.row)!]{
            let params = ["subscriptionId":obj._id,"patientId":userID]
            let request = GetSubscriptionChargesRequest(params: params as [String : AnyObject])
            DPWebServicesManager.sharedManager.getPackageValues(request: request, complitionHandler: { (response, error) in
                debugPrint(response)
            if let paymentdata = response {
                        self.couponPaymentData = paymentdata
                        self.updateDefaultValue()
            }
        })
        }
    }
    func capturePayment(payTmResponse : String){
        if let dict = convertToDictionary(text: payTmResponse) {
            let dicts = JSON(dict).rawString()?.replacingOccurrences(of: "\\", with:"")
            if let obj = self.packageListDetail?.oneTimePackageDetail[(self.selectedIndexPAth?.row)!] {
                
                
                if let tranId = (dict as AnyObject)["TXNID"] as? String {
                    if let user = DPWebServicesManager.sharedManager.user{
                        var params  = ["patientId" : user.userId, "payment_id" : tranId, "subscriptionId" : obj._id, "gateway" : "paytm", "type" : "live", "paytmResponse" : dicts]
                        if let couponInfo = self.couponInfo{
                            params["couponTag"] = couponInfo.couponTag
                            params["couponId"] = couponInfo.couponId
                        }
                        if let gender = DPWebServicesManager.sharedManager.user?.gender{
                            params["gender"] = gender
                        }
                        let request = OneTimePackageRequest(params: params as [String : AnyObject])
                        DPWebServicesManager.getResponse(with: request) { (response, error) in
                            debugPrint(response)
                            if let response = response {
                                self.navigationController?.popViewController(animated: true)
                                //debugPrint("Response\(response)")
                                //  self.packageListDetail =  PackageListDetail(data: JSON(response["data"].dictionary))
                                //   self.changeToSelectedSegmentColor(selectedIndex: .onetime)
                            }
                            if let dict = response?.dictionary{
                            self.paidFeesEventTrack(response: dict,paymentMode: "Wallet")
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    
    func updateDefaultValueToNormal(){
        if let paymentdata = self.couponPaymentData{
            self.paybleAmount = String(describing: paymentdata.patientFee)
            self.ConsultationFee.text = "₹" + ("\(paymentdata.consultingFee)")
            self.totalPaybleLabel.text = "₹" + ("\(paymentdata.patientFee)")
            self.doxtroCashLabel.text = "₹" + ("\(paymentdata.remainingDoxtroCash)")
            if paymentdata.usedDoxtroCash <= 0 {
                self.usedDoxtroCash.text = " ₹" + ("\(paymentdata.usedDoxtroCash)")
            }else{
                self.usedDoxtroCash.text = "- ₹" + ("\(paymentdata.usedDoxtroCash)")
            }
            
            let isFree = paymentdata.isFree
            if  isFree{
               // self.paymentButton.setTitle("START FREE CONSULTATION", for: .normal)
            }else{
                //self.paymentButton.setTitle("PAY ₹\(paymentdata.patientFee) TO CONSULT", for: .normal)
            }
        }
    }
    func convertToDictionary(text: String) -> Any? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
 }
extension PackagePaymentViewController : RazorpayPaymentCompletionProtocol,ExternalWalletSelectionProtocol{
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        if walletName == "paytm"{
            getCheckSum(completionHandler: { (error, response) in
                if let checkSumObj = response,error == nil{
                    self.makePaytmOrderWith(checkSumDetails: checkSumObj)
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Failed to intiate", type: .error)
                }
            })
        }
    }
    func onPaymentError(_ code: Int32, description str: String) {
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: str, type: .error)
    }
    func onPaymentSuccess(_ payment_id: String) {
        print("paymentId\(payment_id)")
        requestForPackageWithPaymentId(payment_id)
    }
    func requestForPackageWithPaymentId(_ paymentId: String) {
        if let patientID = DPWebServicesManager.sharedManager.user?.userId{
            var subscriptionId = ""
            var params = [String : AnyObject]()
                if let obj = self.packageListDetail?.oneTimePackageDetail[(self.selectedIndexPAth?.row)!] {
                    subscriptionId = obj._id
                }
                params  = ["patientId" : patientID as AnyObject, "payment_id" : paymentId as AnyObject, "subscriptionId" : subscriptionId as AnyObject, "gateway" : "razorPay" as AnyObject]
            if let couponInfo = self.couponInfo{
                params["couponTag"] = couponInfo.couponTag as AnyObject
                params["couponId"] = couponInfo.couponId as AnyObject
            }
            if let gender = DPWebServicesManager.sharedManager.user?.gender{
                params["gender"] = gender as AnyObject
            }
            ABProgressIndicator.shared.showAnimator()
            
            let request = OneTimePackageRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                debugPrint(response)
                if let response = response {
                    if error != nil {
                        TSMessage.showNotification(withTitle: error?.localizedDescription, type: .error)
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                if let dict = response?.dictionary{
                    self.paidFeesEventTrack(response: dict,paymentMode: "razorPay")
                }
            }
        }
    }
    func makeFreepayment(){
        if let patientID = DPWebServicesManager.sharedManager.user?.userId{
            var subscriptionId = ""
            var params = [String : AnyObject]()
            if let obj = self.packageListDetail?.oneTimePackageDetail[(self.selectedIndexPAth?.row)!] {
                subscriptionId = obj._id
            }
            params  = ["patientId" : patientID as AnyObject, "payment_id" : "" as AnyObject, "subscriptionId" : subscriptionId as AnyObject, "gateway" : "doxtroCash" as AnyObject]
            if let gender = DPWebServicesManager.sharedManager.user?.gender{
                 params["gender"] = gender as AnyObject
            }
            if let couponInfo = self.couponInfo{
                params["couponTag"] = couponInfo.couponTag as AnyObject
                params["couponId"] = couponInfo.couponId as AnyObject
            }
            ABProgressIndicator.shared.showAnimator()
            let request = OneTimePackageRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                debugPrint(response)
                if let response = response {
                    if error != nil {
                        TSMessage.showNotification(withTitle: error?.localizedDescription, type: .error)
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}
//paytm delegates
extension PackagePaymentViewController : PGTransactionDelegate{
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        debugPrint(responseString)
        capturePayment(payTmResponse:responseString )
        controller.dismissController()
    }
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        controller.dismissController()
    }
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        debugPrint(error.localizedDescription)
    }
    func paidFeesEventTrack(response: [String : JSON],paymentMode : String = "") {
        var values = [String : AnyObject]()
        values["on click"] = true as AnyObject
        if let data = response["data"]?.dictionary {
            if let amount = couponPaymentData?.amount{
                values["af_revenue"] = amount as AnyObject
            }
            values["af_currency"] = "INR" as AnyObject
            //values["Specialization name"] = data["specializationCategory"]?.string as AnyObject
        }
       
            values["Payment Mode"] = paymentMode as AnyObject
                if let doxtroCash = couponPaymentData?.usedDoxtroCash,doxtroCash != 0{
                    values["Using Doxtro Cash"] = true as AnyObject
                    values["Doxtro Cash Value"] = couponPaymentData?.usedDoxtroCash as AnyObject
                }
                values["Balance in Doxtro Wallet"] = couponPaymentData?.doxtroCash as AnyObject
                values["Purchase Type"] = "One Time Package" as AnyObject
            DPAppEventLog.shared.logEventName(EVENT_PAID_FEES, values: values)
            //Using Doxtro Cash
    }
}

