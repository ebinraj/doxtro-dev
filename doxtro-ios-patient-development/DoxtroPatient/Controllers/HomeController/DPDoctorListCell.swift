//
//  DPDoctorListCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol DoctorProfileDelegate {
    func showDoctorProfile(wirh doctorId : String)
}
class DoctorListCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var doctorList : [TextData] = []
    var delegate : DoctorProfileDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib( nibName: "DPDcotorCell", bundle: nil), forCellWithReuseIdentifier: "doctorCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
}
extension DoctorListCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return doctorList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "doctorCell", for: indexPath) as? DoctorCell
        let doctorObj = doctorList[indexPath.row]
        cell?.doctorNameLabel.text = doctorObj.firstName
        cell?.viewProfile.tag = indexPath.row
        cell?.viewProfile.addTarget(self, action: #selector(DoctorListCell.showDoctorProfile(sender:)), for: .touchUpInside)
        var specializationQualification : String = ""
        if doctorObj.qualifucationArray.count > 0{
        for (index,degree) in doctorObj.qualifucationArray.enumerated(){
                specializationQualification = specializationQualification  + degree.degree!
            if (doctorObj.qualifucationArray.count - 1) != index {
                specializationQualification = specializationQualification + " | "
            }
            
        }
        }else{
        specializationQualification = "No Education Details"
        }
        for value in doctorObj.specialization{
         cell?.firstSpecializationLabel.text = value
            break
        }
        cell?.secondSpecializationLabel.text = specializationQualification
        if doctorObj.experience == ""{
                cell?.experienceLabel.text = ("0") + " yrs"
        }else{
             cell?.experienceLabel.text = (doctorObj.experience ?? "0") + " yrs"
        }
        if let url = doctorObj.profilePic{
           cell?.doctorProfileImage.af_setImage(withURL:url, placeholderImage: UIImage(named: "imagePlaceholder"))
        }else{
            cell?.doctorProfileImage.image = UIImage(named : "StubImage")
        }
        
        /*if doctorObj.category.count > 0{
        cell?.firstSpecializationLabel.text = doctorObj.category[0]
        }
        if doctorObj.category.count > 1{
        cell?.secondSpecializationLabel.text = doctorObj.category[1]
        }*/
        return cell!
    }
    func showDoctorProfile(sender : UIButton){
         showDoctorProfileUsingINdex(index: sender.tag)
    }
    func showDoctorProfileUsingINdex(index : Int){
        let doctorOBj  = doctorList[index]
        if let doctorId = doctorOBj.Rid{
            self.delegate?.showDoctorProfile(wirh: doctorId)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showDoctorProfileUsingINdex(index : indexPath.row)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width * 0.6, height: 180)
    }
}
