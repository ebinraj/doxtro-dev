//
//  DPSpecializationView.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 12/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class DPSpecializationView: UIView {
    @IBOutlet weak var specializationIcon: UIImageView!
    @IBOutlet weak var specializationLabel: UILabel!
}
