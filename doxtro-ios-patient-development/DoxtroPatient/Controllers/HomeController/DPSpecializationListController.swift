//
//  DPSpecializationListController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 11/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
class DPSpecializationListController : UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    var specializationList : [Specialization] = []
    var specilizationObj : Specialization?
    var isSpecNavigate: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        self.tabBarController?.tabBar.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = "Specialization List"
               
    }
    @IBAction func submitSelectedSpecializationList(_ sender: Any) {
        if let selectedSpecializationObj = self.specilizationObj{
        self.performSegue(withIdentifier: "showSelectedSpecializationPage", sender: selectedSpecializationObj)
        }else{
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Please select specialization to continue", type: .error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "showSelectedSpecializationPage"{
            let destinationController = segue.destination as? DPSelectedSpecilizationController
            destinationController?.selectedSpecializationObj = sender as? Specialization
         }
    }
}
extension DPSpecializationListController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "specializationTableViewCell") as! DPSpecailizationTableViewCell
        let specializationObj  = self.specializationList[indexPath.row]
        cell.specializationDescriptionLabel.text = specializationObj.specializationDescription
        cell.specializationTitleLabel.text = specializationObj.specializationName
        if let categoryImageUrlData = specializationObj.specializationImage{
            cell.specializationImageView.image = UIImage(data: (categoryImageUrlData  as Data))
        }
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.specializationList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         specilizationObj = self.specializationList[indexPath.row]
    }
}
