//
//  DDChatTextTypeCVCell.swift
//  DoxtroDoctor
//
//  Created by vinaykumar on 8/30/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit

class DDChatTextTypeCVCell: UICollectionViewCell {
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var customView: DDCustomView!
    @IBOutlet weak var statusImageView: UIImageView!
}
