//
//  DPConsultingWaitingCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 31/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
//import AVKit
class ConsultingWaitingCell : UICollectionViewCell{
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var minutetimeLabel: UILabel!
    var timer : Timer?
    var secondcount = 30
    var minuteCount = 2
    var movieUrl : URL?
    var moviePlayer :MPMoviePlayerViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
    if DPWebServicesManager.sharedManager.consultationInfo?.status == .Waiting{
//        self.minutetimeLabel.text = "00:"
//        self.secondTimeLabel.text = "00"
//        self.comeSoonLabel.text = "Will come back to you soon"
        self.minutetimeLabel.text = "05:"
        self.secondTimeLabel.text = "00"
        mainVew.backgroundColor = UIColor(red: 247/255, green: 234/255, blue: 187/255, alpha: 1)
      //  startCountdown()
        showSplashVideo()
    }
    }
    func startCountdown(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        timer?.fire()
    }

    @IBOutlet weak var comeSoonLabel: UILabel!
    func updateCounter() {
        if(secondcount > 0) {
            secondcount = secondcount - 1
            if secondcount < 10 {
            self.secondTimeLabel.text = "" + "0" + String(secondcount)
            }else{
            self.secondTimeLabel.text = "" + String(secondcount)
            }
        }else{
            if minuteCount == 0{
                timer?.invalidate()
                self.comeSoonLabel.text = "will come back to you soon"
                return
            }
            minuteCount = minuteCount - 1
            self.minutetimeLabel.text =  "0" + String(minuteCount) + ":"
            secondcount = 59
            self.secondTimeLabel.text = String(secondcount)
        }
    }
    @IBOutlet weak var mainVew: UIView!
    
//    func configureCell(chatObj : consul)
    @IBOutlet weak var secondTimeLabel: UILabel!
    
    func showSplashVideo(){
        if  let moviewPath =  Bundle.main.path(forResource: "search_v7_cp", ofType: "mp4", inDirectory: "Video"){
            movieUrl = URL(fileURLWithPath: moviewPath)
        }
        moviePlayer = MPMoviePlayerViewController(contentURL: movieUrl)
        moviePlayer?.moviePlayer.controlStyle = .none
        moviePlayer?.moviePlayer.scalingMode = .aspectFit
        moviePlayer?.moviePlayer.setFullscreen(true, animated: false)
        moviePlayer?.moviePlayer.play()
        moviePlayer?.moviePlayer.backgroundView.backgroundColor = UIColor(red: 247/255, green: 234/255, blue: 187/255, alpha: 1)
        moviePlayer?.moviePlayer.repeatMode = MPMovieRepeatMode.one
        moviePlayer?.moviePlayer.view.frame = self.playerView.frame
        self.playerView.addSubview((moviePlayer?.moviePlayer.view)!)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
         moviePlayer?.moviePlayer.view.frame = self.playerView.frame
    }

}

   
/*
 let player = AVPlayer(url: movieUrl!)
 player.play()
 let playerController = AVPlayerViewController()
 
 playerController.player = player
 //self.addChildViewController(playerController)
 self.playerView.addSubview(playerController.view)
 playerController.view.frame = self.playerView.frame
 playerController.view.backgroundColor = UIColor(red: 247/255, green: 234/255, blue: 187/255, alpha: 1)
 NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { _ in
 player.seek(to: kCMTimeZero)
 player.play()
 }
 
 */
