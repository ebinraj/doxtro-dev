//
//  DPDoctorCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class DoctorCell: UICollectionViewCell {
    @IBOutlet weak var doctorProfileImage: UIImageView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var firstSpecializationLabel: UILabel!
    @IBOutlet weak var viewProfile: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var secondSpecializationLabel: UILabel!
    var delegate : DoctorProfileDelegate?
    var row : Int?
    @IBOutlet weak var experienceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.borderWidth = 1
        mainView.layer.cornerRadius = 8
        mainView.layer.borderColor = UIColor().greenDoxtroCgColor().cgColor
        mainView.clipsToBounds = true
        doctorProfileImage.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2)
    }
}
