//
//  DPHelpDoxtroView.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 12/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
protocol SupportDoxtroDelegate {
    func mailDoxtro()
    func callDoxtro()
}
let DOXTRO_SUPPORT_EMAIL = "support@doxtro.com"
let DOXTRO_SUPPORT_PHONE_NUMBER = "+91-7823999900"
class HelpDoxtroView : UIView,MFMailComposeViewControllerDelegate{
    @IBOutlet weak var chatButton: UIButton!
    
    @IBOutlet weak var chatImageView: UIImageView!
    @IBOutlet weak var phoneImageView: NSLayoutConstraint!
    @IBOutlet weak var mailImageView: UIImageView!
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var mailButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let recognizer  = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        mainView.addGestureRecognizer(recognizer)
        mailButton.addBootomBorderLine(width: 1,color: UIColor().greenDoxtroCgColor().cgColor)
        callButton.addBootomBorderLine(width: 1,color: UIColor().greenDoxtroCgColor().cgColor)
        chatButton.addBootomBorderLine(width: 1,color: UIColor().greenDoxtroCgColor().cgColor)
        
    }
    func handleTap(){
        removeSupportView()
    }
    @IBAction func intiateChat(_ sender: Any) {
        removeSupportView()
        FreshChatController.sharedManager.setUserInfoToFreshChat()
        FreshChatController.sharedManager.presentConversationController()
    }
    @IBAction func callDoxtro(_ sender: Any) {
        guard let number = URL(string: "tel://" + DOXTRO_SUPPORT_PHONE_NUMBER) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number)
        } else {
             UIApplication.shared.openURL(number)
        }
      removeSupportView()
    }
    @IBOutlet weak var mainView: UIView!
    @IBAction func mailDoxtro(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            UIApplication.getTopViewController()?.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            let googleUrlString = "googlegmail:///co?to="+DOXTRO_SUPPORT_EMAIL
            if let googleUrl = NSURL(string: googleUrlString) {
                // show alert to choose app
                if UIApplication.shared.canOpenURL(googleUrl as URL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(googleUrl as URL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(googleUrl as URL)
                    }
                    
                }
            }else{
              self.showSendMailErrorAlert()
            }
        }
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.navigationBar.tintColor = UIColor().greenDoxtroCgColor()
        mailComposerVC.setToRecipients([DOXTRO_SUPPORT_EMAIL])
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: {
            self.removeSupportView()
        })
    }
    func removeSupportView(){
        if let controller = UIApplication.getTopViewController(){
            for supportView in controller.view.subviews{
                if supportView is HelpDoxtroView{
                supportView.removeFromSuperview()
                }
            }
        }
    }
    
}

