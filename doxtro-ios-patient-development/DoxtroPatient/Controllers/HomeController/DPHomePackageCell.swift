//
//  DPHomePackageCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol HomePackageDelegate{
    func openPackageViewController()
}
class HomePackageCell: UITableViewCell {
    var delegate : HomePackageDelegate?
    @IBOutlet weak var packageImageView: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewPackageButton: UIButton!
}
