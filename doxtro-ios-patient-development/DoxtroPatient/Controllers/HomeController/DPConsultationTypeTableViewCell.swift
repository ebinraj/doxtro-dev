//
//  DPConsultationTypeTableViewCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 25/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol DPSelectedConsultationTypeDelegate  {
    func setSelectedlanguage(language : String)
    func setSelectedConsultationType(with type : InitateConsultationType)
}
var consultationRequestedEventParms = [String : AnyObject]()
class DPConsultationTypeTableViewCell : UITableViewCell{
    var languageArray : [String] = []
    var selectedLanguageIndexPath : IndexPath?
    var selectedLanguage : String?
    var delegate : DPSelectedConsultationTypeDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        getPreferredLangugaeList()
    }
    func getPreferredLangugaeList(){
        let request = GetPreferredLanguageListRequest()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            if let data = response?["data"].array{
                self.languageArray.removeAll()
                for language in data{
                    self.languageArray.append(language.stringValue)
                }
                self.selectedLanguage = self.languageArray.first
            }
            self.collectionView.reloadData()
        }
    }
    //RadioIcon
    //RadioSelectedIcon
    @IBAction func textConsultationClicked(_ sender: Any) {
        self.textConsulataionButton.setImage(UIImage(named: "RadioSelectedIcon"), for: .normal)
            self.audioConsulatationButton.setImage(UIImage(named: "RadioIcon"), for: .normal)
        self.delegate?.setSelectedConsultationType(with: .Chat)
    }
    @IBOutlet weak var audioConsulatationButton: UIButton!
    
    @IBOutlet weak var textConsulataionButton: UIButton!
    @IBAction func checkBoxSelected(_ sender: Any) {
        var values = [String : AnyObject]()
        values["Consultation Type"] = "Chat" as AnyObject
        self.audioConsulatationButton.setImage(UIImage(named: "RadioSelectedIcon"), for: .normal)
            self.textConsulataionButton.setImage(UIImage(named: "RadioIcon"), for: .normal)
        self.delegate?.setSelectedConsultationType(with: .Audio)
    
    }
}
extension DPConsultationTypeTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.languageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "languageTypeCell", for: indexPath) as? DPPreferredLanguageCollectionViewCell
        cell?.languageTypeLabel.text = self.languageArray[indexPath.row]
        if indexPath.row == 0,self.selectedLanguageIndexPath == nil{
         self.selectedLanguageIndexPath = indexPath
            self.delegate?.setSelectedlanguage(language: self.languageArray[indexPath.row]
)
        }
        if self.selectedLanguageIndexPath != nil, let selectedIndexPath = self.selectedLanguageIndexPath{
            if selectedIndexPath == indexPath{
                cell?.labelView.backgroundColor = UIColor(red: 39/255.0, green: 162/255.0, blue: 156/255.0, alpha: 0.5)
                cell?.labelView.layer.borderWidth = 0.5
                cell?.labelView.layer.borderColor = UIColor().greenDoxtroCgColor().cgColor
            }else{
                cell?.labelView.backgroundColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 0.5)
                cell?.labelView.layer.borderWidth = 0.5
                cell?.labelView.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
            }
        }else{
            cell?.labelView.backgroundColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 0.5)
            cell?.labelView.layer.borderWidth = 0.5
            cell?.labelView.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
        }
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/6, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     //   var values = [String : AnyObject]()
        
        self.selectedLanguageIndexPath = indexPath
        self.selectedLanguage = self.languageArray[indexPath.row]
        if let language = self.selectedLanguage{
            consultationRequestedEventParms["Preferred Language"] = language as AnyObject
            self.delegate?.setSelectedlanguage(language: language)
        self.delegate?.setSelectedlanguage(language: language)
        }
        collectionView.reloadData()
    }
}
class DPPreferredLanguageCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var languageTypeLabel: UILabel!
    @IBOutlet weak var labelView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
         languageTypeLabel.layer.cornerRadius = 4
         labelView.layer.cornerRadius = 8
    }
}
