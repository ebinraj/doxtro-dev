//
//  DPPackageTypeCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 29/01/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class PackageTypeCell: UITableViewCell{
    
    @IBOutlet weak var imageSelectionCell: UIImageView!
    @IBOutlet weak var packageNoteCell: UILabel!
    @IBOutlet weak var packageDurationCell: UILabel!
    @IBOutlet weak var packagePriceCell: UILabel!
    @IBOutlet weak var packageTypeLabel: UILabel!
    @IBOutlet weak var originalPriceLabel: UILabel!
}
