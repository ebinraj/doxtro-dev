//
//  DPBasicDetailsViewController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/24/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
class DPConsultCVItemCell: UICollectionViewCell {
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var consultLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
class DPPreferedLanguageCVCell: UICollectionViewCell {
    @IBOutlet weak var languageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
class DPAttchmentImageCVCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
class DPAttchmentAddnewCVCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class DPSelectedSpecializationTVCell:UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
class DPConsultForTVCell:UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    let consultForArray:[String] = ["Myself", "Dad", "Daughter"]
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK:- UICollectionView Delegate & Datasource methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return self.consultForArray.count
        } else {
          return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.collectionView.dequeueReusableCell(withReuseIdentifier: "consultingItemId", for: indexPath) as! DPConsultCVItemCell
        if indexPath.section == 1 {
            item.consultLabel.text = "Add new member"
        } else {
            item.consultLabel.text = self.consultForArray[indexPath.item]
        }
        item.avatarView.backgroundColor = UIColor.gray
        item.avatarView.layer.cornerRadius = item.avatarView.frame.size.width/2
        return item
    }
}
class DPSelectionTypeTVCell:UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    let languages:[String] = ["English", "Hindi", "Tamil", "Telugu"]
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK:- UICollectionView Delegate & Datasource methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.languages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.collectionView.dequeueReusableCell(withReuseIdentifier: "languageItemId", for: indexPath) as! DPPreferedLanguageCVCell
        item.languageLabel.text = self.languages[indexPath.item]
        item.layer.cornerRadius = 10.0
        item.layer.borderWidth = 1.0
        item.layer.borderColor = ColorMethod.hexStringToUIColor(hex: darkGray_hex).cgColor
        item.layer.backgroundColor = ColorMethod.hexStringToUIColor(hex: lightGray_hex).cgColor
        return item
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "NewMemberId") as! DPAddNewMemberViewController
        viewController.modalPresentationStyle = .overCurrentContext
        UIApplication.shared.keyWindow?.rootViewController?.present(viewController, animated: false, completion: nil)
    }
}
class DPHealthConcernTVCell:UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
class DPAttchementsTVCell:UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK:- UICollectionView Delegate & Datasource methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return 1
        } else {
            return 2
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            if indexPath.section == 1 { //MARK:- TODO this later
            let item = self.collectionView.dequeueReusableCell(withReuseIdentifier: "addNewItemId", for: indexPath) as! DPAttchmentAddnewCVCell
                item.layer.backgroundColor = ColorMethod.hexStringToUIColor(hex: lightGray_hex).cgColor
            return item
        } else {
            let item = self.collectionView.dequeueReusableCell(withReuseIdentifier: "imageItemId", for: indexPath) as! DPAttchmentImageCVCell
                item.layer.backgroundColor = ColorMethod.hexStringToUIColor(hex: lightGray_hex).cgColor
            return item
        }
    }
}

class DPBasicDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 86
        self.tableView.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UITableView datasource & delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellId1", for: indexPath) as! DPSelectedSpecializationTVCell
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellId2", for: indexPath) as! DPConsultForTVCell
            cell.selectionStyle = .none
            cell.collectionView.reloadData()
            return cell
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellId3", for: indexPath) as! DPSelectionTypeTVCell
            cell.selectionStyle = .none
            cell.collectionView.reloadData()
            return cell
        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellId4", for: indexPath) as! DPHealthConcernTVCell
            cell.selectionStyle = .none
            return cell
        case 4:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellId5", for: indexPath) as! DPAttchementsTVCell
            cell.selectionStyle = .none
            cell.collectionView.reloadData()
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
