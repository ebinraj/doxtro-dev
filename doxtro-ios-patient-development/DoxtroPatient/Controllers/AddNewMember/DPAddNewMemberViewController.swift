//
//  DPAddNewMemberViewController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/25/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
class DPMembersCVCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
class DPNewMemberTVCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    
    let numberOfRows = 5
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nameTextField.useUnderline(underlineColor: UIColor.gray)
        self.ageTextField.useUnderline(underlineColor: UIColor.gray)

    }
    @IBAction func genderButtonAction(_ sender: Any) {
        let button = sender as! UIButton
        if button.tag == 1 {
            self.maleButton.setImage(UIImage(named:"RadioSelectedIcon"), for: .normal)
            self.femaleButton.setImage(UIImage(named:"RadioIcon"), for: .normal)
        } else {
            self.maleButton.setImage(UIImage(named:"RadioIcon"), for: .normal)
            self.femaleButton.setImage(UIImage(named:"RadioSelectedIcon"), for: .normal)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfRows
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! DPMembersCVCell
        return item
    }
}
class DPAddNewMemberViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let touchRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(didTapOnTable(_:)))
        self.tableView.addGestureRecognizer(touchRecognizer)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func didTapOnTable(_ sender: Any) {
      self.dismiss(animated: false, completion: nil)
    }
    //MARK: - UITableView datasource & delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DPNewMemberTVCell
        cell.selectionStyle = .none
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
