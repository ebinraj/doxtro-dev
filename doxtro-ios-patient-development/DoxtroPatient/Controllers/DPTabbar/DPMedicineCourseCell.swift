//
//  DPMedicineCourseCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol MedicineCourseDelegate {
    func addMedicationCourse(healthRecordType : HealthRecordType)
    func deleteMedicatonCourse(row : Int,healthRecordType : HealthRecordType)
}
class MedicationCourseCell : UITableViewCell{
    var delegate : MedicineCourseDelegate?
    @IBOutlet weak var medicineNameLabel: UILabel!
    @IBAction func addMedicineButtonClicked(_ sender: Any) {
        self.delegate?.addMedicationCourse(healthRecordType : .medication)
    }
    @IBAction func deleteButtonClicked(_ sender: Any) {
        UIAlertController.showAlertWithMessage("STR_DELETE_ITEM_MSG".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
            
        }, actionButtonHandler: { (_ , _) in
            if let button = sender as? UIButton{
                self.delegate?.deleteMedicatonCourse(row: button.tag,healthRecordType : .medication)
            }
        })
    }
    @IBOutlet weak var dleteButton: UIButton!
    @IBOutlet weak var addMedicineButton: UIButton!
}
