//
//  DPPageViewController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/23/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SwiftyJSON
class DPPageViewController: UIPageViewController, UIPageViewControllerDelegate {
    var timer : Timer?
    var count = -1
    var imageArray : [URL] = []
    var coreDataImageArray = [UIImage]()
    lazy var channelRef: DatabaseReference = Database.database().reference().child("BannerImages")
    var channelRefHandle: DatabaseHandle?
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let viewBounds = self.view.bounds
        for view in self.view.subviews {
            if view is UIScrollView {
                view.frame = self.view.frame
            } else {
                let page = view as? UIPageControl
                page?.frame = CGRect(x: 15, y: viewBounds.height - 20, width: 5*15, height: 20)
                page?.center = CGPoint(x: self.view.center.x, y: self.view.frame.height - 10)
                page?.contentHorizontalAlignment = .left
                page?.contentMode = .left
                page?.backgroundColor = UIColor.clear
                page?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                page?.pageIndicatorTintColor = UIColor.lightGray
                page?.currentPageIndicatorTintColor = UIColor().greenDoxtroCgColor()
                self.view.bringSubview(toFront: view)
            }
        }
    }
    func getPageControllerArray(){
        if let isRechable = DPWebServicesManager.sharedManager.reachability?.isReachable,isRechable{
            
           // channelRef.observeSingleEvent(of: .value, with: { (snapshot) in
            channelRef.observe(.value, with: { (snapshot) in
                if let channelData = snapshot.value as? Dictionary<String, AnyObject>{ // 2
                    self.imageArray.removeAll()
                    for(key,value) in channelData{
                        if let dict = JSON(value).dictionary{
                            if let imageString = dict["imgUrl"]?.string{
                                if let imageUrl = URL(string: imageString){
                                    self.imageArray.append(imageUrl)
                                    self.insertIntoCoreData(using: imageUrl)
                                }
                            }
                        }
                    }
                    self.startResendOtpCountdown()
                    //                        for (_,obj) in channelData{
                    //                            if let obj = JSON(obj).dictionary{
                    //                                if let msgType = obj["msgType"]?.string,msgType == "text"{
                    //                                    if let from = obj["from"]?.string{
                    //                                        if from == "Doctor" {
                    //                                            if let status = obj["status"]?.string{
                    //                                                if status == "sent" || status == "Sent"{
                    //
                    //                                                }
                    //                                            }
                    //                                        }
                    //                                    }
                    //                                }
                    //                            }
                    //                        }
                } else {
                  self.imageArray.removeAll()
                }
            }) { (error) in
                print(error.localizedDescription)
            }
        }else{
            if let bannerImageList = DatabaseController.fetchBannerImage(){
                for imageObj in bannerImageList{
                    if  let image = UIImage(data: imageObj.bannerImage! as Data){
                    self.coreDataImageArray.append(image)
                    }
                }
                self.startResendOtpCountdown()
            }
        }
    }
    func insertIntoCoreData(using url : URL){
        DatabaseController.deleteBanerFromCoredataList()
        DPHomeViewController.getImageDataAndSave(url: url.absoluteString) { (data) in
            DatabaseController.insertBannerImageInCoreData(obj:data as NSData)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(singleTapped(_ :))))
        getPageControllerArray()
        setViewControllers([viewControllerAtIndex(index: 0)!],
                           direction: .forward,
                           animated: true,
                           completion: nil)

        // Do any additional setup after loading the view.
    }
    func singleTapped(_ recognizer: UITapGestureRecognizer) {
        // Users clicking on the home page banners.
        DPAppEventLog.shared.logEventName(EVENT_BANNER_VISITORS, values: ["Users clicking on the home page banners." : "" as AnyObject])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DPPageViewController: UIPageViewControllerDataSource {
    
    func viewControllerAtIndex(index: Int) -> DPPageContentViewController? {
        /*
         if (pageContent.count == 0) ||
         (index >= pageContent.count) {
         return nil
         }
         */
        
        let storyBoard = UIStoryboard(name: "DPTabbar",
                                      bundle: Bundle.main)
        let dataViewController = storyBoard.instantiateViewController(withIdentifier: "contentView") as! DPPageContentViewController
        dataViewController.index = index
        if (self.imageArray.count) - 1 >= index {
        dataViewController.imageURL = self.imageArray[index]
        }
        if index < self.coreDataImageArray.count{
        dataViewController.image = self.coreDataImageArray[index]
        }
        
        return dataViewController
    }
    
    func indexOfViewController(viewController: DPPageContentViewController) -> Int {
        
       /* if let index:Int = viewController.index {
            return index
        } else {
            return NSNotFound
        }*/
        return viewController.index
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        // var index = [(APPChildViewController *)viewController index];
        var index:Int = indexOfViewController(viewController: viewController as! DPPageContentViewController)
        if (index == 0) {
            return nil;
        }
        index -= 1
        return self.viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index:Int = indexOfViewController(viewController: viewController as! DPPageContentViewController)
        index += 1;
        
        if (index == 5) {
            return nil;
        }
        return self.viewControllerAtIndex(index: index)
    }
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        if let isrechable = DPWebServicesManager.sharedManager.reachability?.isReachable,!isrechable{
            return self.coreDataImageArray.count
        }
        return  self.imageArray.count
    }
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return count
    }
    
}
extension DPPageViewController{
    func startResendOtpCountdown(){
        timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        timer?.fire()
    }
    func updateCounter() {
        if let isrechable = DPWebServicesManager.sharedManager.reachability?.isReachable,!isrechable{
            if(count >= (self.coreDataImageArray.count)-1) {
                count = -1
            }
        }else{
            if(count >= (self.imageArray.count)-1) {
                count = -1
            }
        }
            count = count + 1
            setViewControllers([viewControllerAtIndex(index: count)!],
                               direction: .forward,
                               animated: true,
                               completion: nil)
    }
}
