//
//  DPPageContentViewController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/23/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit

class DPPageContentViewController: UIViewController {
    var index:Int = Int()
    var imageURL : URL?
    var image : UIImage?
    @IBOutlet weak var pageImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = self.imageURL{
        self.pageImageView.af_setImage(withURL: url)
        }else{
            if let isRechable = DPWebServicesManager.sharedManager.reachability?.isReachable,!isRechable{
                self.pageImageView.image = image
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
