//
//  DPPatientTransactionHistory.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 15/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class PatientTransactionHistory : UIViewController{
    var transactionHistoryArray : [TransactionHistory] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        getTransactionHistory()
    }
    func showNoRecord(){
        if transactionHistoryArray.count > 0{
            self.noRecordFoundLabel.isHidden = true
        }else{
            self.noRecordFoundLabel.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    @IBOutlet weak var noRecordFoundLabel: UILabel!
    @IBAction func dismissController(_ sender: Any) {
        self.dismissController()
    }
    @IBOutlet weak var tableView: UITableView!
    func getTransactionHistory(){
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
            let params  = ["patientId" : userID]
            let request = GetTransactionHistory(params: params as [String : AnyObject])
            ABProgressIndicator.shared.showAnimator()
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let response = response {
                    if let data = response["data"].array{
                        self.transactionHistoryArray.removeAll()
                        for obj in data{
                            self.transactionHistoryArray.append(TransactionHistory(dict: obj))
                        }
                        self.showNoRecord()
                        self.tableView.reloadData()
                      //  debugPrint(response)
                    }
                }else{
                     self.showNoRecord()
                }
            })
        }
    }
}
extension PatientTransactionHistory : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transactioHistoryObj = self.transactionHistoryArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactonHistoryCell", for: indexPath) as? TransactionHistoryTableViewCell
        cell?.doctorName.text = transactioHistoryObj.doctorName
        cell?.transactionDate.text = DXUtility.convertDateAndTime(dateStr:transactioHistoryObj.date)
        cell?.transactionType.text = "  " + transactioHistoryObj.method! + "  "
        cell?.amount.text =  String(describing: transactioHistoryObj.amount!)
        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transactionHistoryArray.count
    }
}
