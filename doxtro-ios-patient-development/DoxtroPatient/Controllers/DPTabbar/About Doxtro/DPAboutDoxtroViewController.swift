//
//  DPAboutDoxtroViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 19/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class AboutViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
    }
    let text  = "\nDoxtro is an online healthcare platform that connects patients in need with doctors and medical experts, anywhere in India." +
    
   "\n\nA team of healthcare professionals and technologists, Doxtro believes that it is not only important to provide better patient care at an affordable cost but also to deliver it timely and hassle-free.The healthcare industry is not suffering from a lack of doctors,rather from sound delivery solutions that restricts proper accessibility." +
    
    "\n\nMajority of the good doctors are in the city and it becomes impossible to access quality healthcare professionals in rural and remote locations." +

    "\n\nRecognizing this, Doxtro has tapped the potential of online technology to solve this problem for medical consumers in India.On doxtro, patients can get instant care advice, treatment options and answers to their medical queries from high quality doctors and medical experts, anytime, anywhere through the app." +

    "\n\nDoxtro is the best way to seek medical help for patients, ensuring easy access to doctors for billions of people, creating an experience that is truly magical for both healthcare experts and patients alike.\n"
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "aboutTableViewCell", for: indexPath) as? AboutTableViewCell
        cell?.aboutLabe.text = text
        return cell!
    }
    @IBAction func back(_ sender: Any) {
        self.dismissController()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    @IBOutlet weak var tableView: UITableView!
}
class AboutTableViewCell : UITableViewCell{
    @IBOutlet weak var aboutLabe: UILabel!
}
