//
//  DPAddMedicalHistoryViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 03/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
import Firebase
import FirebaseStorage
class AddMedicineController : UIViewController{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noteTextField: UITextField!
    @IBOutlet weak var medicineNameTextField: UITextField!
    var relativesId : String?
    var imageArray : [AnyObject] = []
    var urlArray : [[String:String]] = []
    var counter = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.medicineNameTextField.addBootomBorderLine(width: 2)
        self.noteTextField.addBootomBorderLine(width: 2)
    }
    @IBAction func submitButtonClicked(_ sender: Any) {
        createRecord()
    }
    @IBAction func addImagebuttonClicked(_ sender: Any) {
        openImageAndDocumentsPicker(withDocuments: true)
      //  openRespectiveAttachmentType(with: .photoLibrary)
    }
}
extension AddMedicineController : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let image = self.imageArray[indexPath.row] as? UIImage{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as? ImageCell
        cell?.medicineImageView.image = image
        return cell!
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "medicalHistoryDocumentsCell", for: indexPath) as? MedicalHitoryDocumentCell
        if let image = self.imageArray[indexPath.row] as? String{
        cell?.fileName.text = image
        }
        return cell!
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/4, height: collectionView.bounds.height)
    }
}
extension AddMedicineController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
             load(image:editedImage)
            self.imageArray.append(editedImage)
        }
        else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            load(image:image)
            self.imageArray.append(image)
        }
        self.collectionView.reloadData()
        picker.dismiss(animated: true, completion:nil)
    }
    func openRespectiveAttachmentType(with sourceType : UIImagePickerControllerSourceType = .photoLibrary,documents : Bool = false){
        if !documents{
            let imagePickerController = UIImagePickerController()
             imagePickerController.navigationBar.tintColor = UIColor().greenDoxtroCgColor()
            imagePickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePickerController.allowsEditing = true
            if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                imagePickerController.sourceType = sourceType
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }else{
            let importMenu = UIDocumentMenuViewController(documentTypes: ["public.composite-content"], in: .import)
            importMenu.delegate = self as? UIDocumentMenuDelegate
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion:{(
               // debugPrint("ayuhs")
                )})
        }
    }
    func createRecord(){
        if self.medicineNameTextField.text != ""{
            if let userId = DPWebServicesManager.sharedManager.user?.userId,let relativesId = self.relativesId{
                var params : [String : AnyObject]  = ["patientId": userId as AnyObject,
                                                      "relativesId":relativesId as AnyObject,
                                                      "tag": "history" as AnyObject,
                                                      "content": self.medicineNameTextField.text! as AnyObject,
                                                      "note": self.noteTextField.text! as AnyObject,
                                                      "url" : self.urlArray as AnyObject]
                self.saveUserInfo(params: params)
            }
        }else{
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "ERROR", subtitle: "Enter Medicine Name", type: .error)
        }
    }
        func load(image : UIImage){
            let resizeImage = DXUtility.shared.resizeImage(image: image, newWidth: 400)
            if let uploadData = UIImagePNGRepresentation(resizeImage) {
                ABProgressIndicator.shared.showAnimator()
                FirebaseController.storageRef.putData(uploadData, metadata: nil, completion: { (metaData, error) in
                    if error == nil,let response = metaData {
                        if let user = DPWebServicesManager.sharedManager.user{
                            if  let url = response.downloadURL()?.absoluteString{
                                self.urlArray.append(["url":url,"urlType":"image"])
                            }
                            ABProgressIndicator.shared.hideAnimator()
                        }else{
                            ABProgressIndicator.shared.hideAnimator()
                        }
                    }else{
                        ABProgressIndicator.shared.hideAnimator()
                        
                    }
                })
            }
        }
//    func saveUserInfo(params : [String : Any]){
//        let request = CreateHelathRecordRequest(params: params as [String : AnyObject])
//        ABProgressIndicator.shared.showAnimator()
//        DPWebServicesManager.getResponse(with: request, completionHandler: { (resposne, error) in
//            ABProgressIndicator.shared.hideAnimator()
//            if error == nil ,let response = resposne {
//                if let data = response["data"].array{
//
//                }else{
//
//                }
//            }
//             self.navigationController?.popViewController(animated: false)
//        })
//
//    }
    /*
     func load(image : UIImage){
     let resizeImage = DXUtility.shared.resizeImage(image: image, newWidth: 400)
     if let uploadData = UIImagePNGRepresentation(resizeImage) {
     ABProgressIndicator.shared.showAnimator()
     FirebaseController.storageRef.putData(uploadData, metadata: nil, completion: { (metaData, error) in
     if error == nil,let response = metaData {
     if let user = DPWebServicesManager.sharedManager.user{
     if  let url = response.downloadURL()?.absoluteString{
     self.urlArray.append(url)
     self.counter = self.counter + 1
     if self.imageArray.count == self.counter{
     self.params!["url"] = self.urlArray as AnyObject
     self.saveUserInfo(params: self.params!)
     }else{
     self.load(image: self.imageArray[self.counter])
     }
     }
     ABProgressIndicator.shared.hideAnimator()
     
     }else{
     ABProgressIndicator.shared.hideAnimator()
     }
     }else{
     ABProgressIndicator.shared.hideAnimator()
     
     }
     })
     }
     }
 */
    func saveUserInfo(params : [String : Any]){
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.alamofireRequest(url: kWebServiceBase+"/healthRecords/create", httpMethod: "POST", details:params as [String : AnyObject]) { (resposne, error) in
            ABProgressIndicator.shared.hideAnimator()
            if error == nil ,let response = resposne {
                if let data = response["data"].array{
                    
                }else{
                    
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MedicalHitoryRefresh"), object: nil)
            self.navigationController?.popViewController(animated: false)
        }
    }
}
class ImageCell : UICollectionViewCell{
    @IBOutlet weak var medicineImageView: UIImageView!
}
extension AddMedicineController : UIDocumentMenuDelegate,UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        
        let cico = url as URL
        self.imageArray.append(cico.lastPathComponent as AnyObject)
        uploadDocuments(fileUrl: cico,fileName: cico.lastPathComponent)
        
        //optional, case PDF -> render
        //displayPDFweb.loadRequest(NSURLRequest(url: cico) as URLRequest)
        
        
        
        
    }
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        
        
        dismiss(animated: true, completion: nil)
        
        
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
//    func opennPDf()
//    {
//        self.documentInteractionController = UIDocumentInteractionController(url: URL(string: "file:///private/var/mobile/Containers/Data/Application/75585354-9BA3-4C73-9A15-25DE4BBECEDE/tmp/com.doxtro.app-Inbox/iOSTechOverview.pdf")!)
//        self.documentInteractionController?.delegate = self
//        self.documentInteractionController?.presentPreview(animated: true)
//    }
    func  uploadDocuments(fileUrl : URL,fileName : String){
        ABProgressIndicator.shared.showAnimator()
        // Create a reference to the file you want to upload
        let docRef = Storage.storage().reference().child("chat_documents/").child((DPWebServicesManager.sharedManager.user?.userId)! + "user.png" + "\(Date())")
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = docRef.putFile(from: fileUrl, metadata: nil) { metadata, error in
            ABProgressIndicator.shared.hideAnimator()
            if let error = error {
                
            } else{
                // Metadata contains file metadata such as size, content-type, and download URL.
                if let downloadURL = metadata!.downloadURL()?.absoluteString{
                    self.urlArray.append(["url":downloadURL,"urlType":"documents","documentName":fileName])
                    self.collectionView.reloadData()
                }
            }
        }
        
    }
}
