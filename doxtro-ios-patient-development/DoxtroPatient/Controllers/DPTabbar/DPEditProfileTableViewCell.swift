//
//  DPEditProfileTableViewCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
protocol DPUpdateProfileDelegate{
    func updateUserProfile(with params : [String : AnyObject],photo : Bool)
    func showHeightPicker()
}
enum DPgenderType : String {
    case Male = "Male"
    case Female = "Female"
}
class DPEditProfileTableViewCell : UITableViewCell{

    @IBOutlet weak var fullNameTextField: UITextField!
    
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var heightTextField: UITextField!
    
    @IBOutlet weak var mobileTextField: UITextField!
    var selectedGender : DPgenderType = .Male
    @IBOutlet weak var weightTextFeld: UITextField!
    var delegate : DPUpdateProfileDelegate?
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    var sortedSegmentViews : [UIView]?
    @IBAction func genderSelection(_ sender: Any) {
        if sender is UISegmentedControl{
            if let segmentControl = sender as? UISegmentedControl{
                changeToSelectedSegmentColor(selectedIndex: segmentControl.selectedSegmentIndex)
            }
        }
        
    }
    @IBAction func updateUserProfile(_ sender: Any) {
        if validation(){
            let controller = UIAlertController.showAlertWithMessageForEditProfile("INFO_SAVE_MESSAGE".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
            }, actionButtonHandler: { (_ , _) in
                if let user = DPWebServicesManager.sharedManager.user{
                    let params  = ["firstName" : self.fullNameTextField.text!,
                                   "age":self.ageTextField.text!,
                                   "emailid":self.emailTextField.text!,
                                   "mobile":self.mobileTextField.text!,
                                   "height":self.heightTextField.text!,
                                   "gender":self.selectedGender.rawValue,
                                   "weight":self.weightTextFeld.text!,
                                   "_id" :user.userId]
                    self.delegate?.updateUserProfile(with: params as [String : AnyObject],photo: false)
                }

            })
          UIApplication.getTopViewController()?.show(controller, sender: nil)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mobileTextField.isEnabled = false
        setUpNewsTypeSegmentController()
        fullNameTextField.addBootomBorderLine(width: 1)
        fullNameTextField.delegate = self
        ageTextField.addBootomBorderLine(width: 1)
        ageTextField.delegate = self
        emailTextField.addBootomBorderLine(width: 1)
        emailTextField.delegate = self
        heightTextField.addBootomBorderLine(width: 1)
        heightTextField.delegate = self
        mobileTextField.addBootomBorderLine(width: 1)
        mobileTextField.delegate = self
        weightTextFeld.addBootomBorderLine(width: 1)
        weightTextFeld.delegate = self
       // mobileNumberCountryCode.addBootomBorderLine()
        self.sortedSegmentViews = genderSegmentControl.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
        changeToSelectedSegmentColor(selectedIndex: 0)
    }
    
    func setUpNewsTypeSegmentController(){
        genderSegmentControl.layer.masksToBounds = true
        self.genderSegmentControl.tintColor = UIColor.clear
        genderSegmentControl.backgroundColor = UIColor.white
        genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .selected)
        genderSegmentControl.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.black,NSFontAttributeName:UIFont.boldSystemFont(ofSize: 16.0)], for: .normal)
        //NSBackgroundColorAttributeName:UIColor.magenta
    }
}
//segment Control
extension DPEditProfileTableViewCell{
    func changeToSelectedSegmentColor(selectedIndex : Int){
        switch selectedIndex {
        case 0:
            self.selectedGender = .Male
            if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[1]{
                view.backgroundColor = UIColor.white
            }
            break
        case 1:
            self.selectedGender = .Female
            if let view : UIView = self.sortedSegmentViews?[selectedIndex]{
                view.backgroundColor = UIColor().greenDoxtroCgColor()
                view.layer.cornerRadius = 4
            }
            if let view : UIView = self.sortedSegmentViews?[0]{
                view.backgroundColor = UIColor.white
            }
            break
        default:
            break
        }
    }
}
extension DPEditProfileTableViewCell : UITextFieldDelegate{
    func validation()->Bool{
        var isValidate : Bool = false
        if self.fullNameTextField.text?.characters.count == 0 {
        showErroWithMessage(with: "Enter your name")
        } else if self.ageTextField.text?.characters.count == 0 {
        showErroWithMessage(with: "Enter your age")
        } else if self.emailTextField.text?.characters.count == 0 {
        showErroWithMessage(with: "Enter your email")
        }else if self.mobileTextField.text?.characters.count == 0 {
        showErroWithMessage(with: "Enter your mobile number")
        }else{
         guard  DXUtility.shared.isValidEmail(testStr: self.emailTextField.text!)else{
            showErroWithMessage(with: "Enter Valid Email Address")
            return isValidate
          }
//            guard DXUtility.isValidPhoneNumber(phoneNumber: self.mobileTextField.text!) else {
//                showErroWithMessage(with: "Enter valid mobile Number")
//            return isValidate
//        }
         isValidate = true
        }
        return isValidate
    }
    func showErroWithMessage(with message : String){
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: message, type: .error)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case emailTextField:
            return isValidUPtolength(withTextField: textField,length: 50,range:range,string : string)
        case ageTextField,weightTextFeld :
            return isValidUPtolength(withTextField: textField,length: 3,range:range,string : string)
        case mobileTextField :
            return isValidUPtolength(withTextField: textField,length: 10,range:range,string : string)
        case fullNameTextField :
            return isValidUPtolength(withTextField: textField,length: 30,range:range,string : string)
        default:
        return true
        }
    }
    func isValidUPtolength(withTextField : UITextField,length : Int,range: NSRange,string : String)->Bool{
        let currentString: NSString = NSString(string: withTextField.text!)
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= length
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == heightTextField{
            self.delegate?.showHeightPicker()
            return false
        }
        return true
    }
}
