//
//  DPRatingViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 08/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//
import Foundation
import TSMessages
enum RatingValue : String{
    case Bad
    case Good
    case Average
    case Excellent
    case Terrible
    init (value : Float){
        switch value{
        case 1.0:
            self = .Terrible
            break
        case 2.0:
            self = .Bad
            break
        case 3.0:
            self = .Average
            break
        case 4.0:
            self = .Good
            break
        case 5.0:
            self = .Excellent
            break
        default:
            self = .Excellent
            break
        }
    }
}
class DPRatingViewController: UIViewController {
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var notNow: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    //rating related
    @IBOutlet weak var ratingValueLabel: UILabel!
    @IBOutlet weak var feedBackTextField: UITextField!
    @IBOutlet weak var ratingQuestionLabel: UILabel!
    @IBOutlet weak var ratingQuestionFirstOpion: UILabel!
    @IBOutlet weak var ratingQuestionSecondOption: UILabel!
    var selectedIndexPath : IndexPath?
    //What went wrong (rating 1&2): Late Reply, Not satisfied, Rude response, Something else
    //What went well (rating 3& above)    : Quick response, Great advice, Very friendly, Something else

    let whatWentWellOptionArray = ["Quick response","Great advice","Very friendly","Something else"]
    let whatWentWrongOptionArray = ["Late Reply","Not satisfied","Rude response","Something else"]
    var selectedArray : [String] = []
    @IBOutlet weak var behaviourOptionStackView: UIStackView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    //dotor info
    @IBOutlet weak var doctorQualificationLabel: UILabel!
    @IBOutlet weak var dotorInfoLabel: UILabel!
    @IBOutlet weak var doctorImage: UIImageView!
    var feedbackConsulatationArrray : [DpConsultationDetails] = []
    var consultationObj : DpConsultationDetails?
    var selectedBehaviour : String = ""
    var selectedBehaviourTag : Int?
    var selectedBehaviourString : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedArray = whatWentWellOptionArray
        ratingView.delegate = self
        blurView.layer.cornerRadius = 8.0
        doctorImage.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor,width: 2.0)
        serRatingQuestionAndOption(with: 5.0)
        addInformationFromConsulatationObject(consulatationArray: self.feedbackConsulatationArrray)
        setBehaviourOptionView()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
         self.feedBackTextField.addBootomBorderLine(width: 2.0)
    }
    func setBehaviourOptionView(){
//        for (index,mainView) in behaviourOptionStackView.subviews.enumerated(){
//            mainView.tag = index
//            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
//            mainView.addGestureRecognizer(tap)
//                for label in mainView.subviews{
//                    if label is UILabel{
//                        let labels = label as! UILabel
//                        if selectedBehaviourTag == index{
//                        labels.textColor = UIColor.white
//                        }else{
//                        labels.textColor = UIColor.gray
//                        }
//                    }
//                }
//                if selectedBehaviourTag == index{
//                    mainView.backgroundColor = UIColor().greenDoxtroCgColor()
//                }else{
//                    mainView.backgroundColor = UIColor.white
//            }
//        }
    }
    func handleTap(sender: UITapGestureRecognizer) {
        if let tag = sender.view?.tag{
            selectedBehaviourTag = tag
            setBehaviourOptionView()
        }
    }
    func addInformationFromConsulatationObject(consulatationArray : [DpConsultationDetails]){
        if let consulatation = consulatationArray.first{
            self.consultationObj = consulatation
            setDoctorProfileWith(consultationObj: consulatation)
        }
    }
    func setDoctorProfileWith(consultationObj : DpConsultationDetails){
        if let doctorObj = consultationObj.doctorObj {
            self.dotorInfoLabel.text = doctorObj.firstName
            if doctorObj.categoryArray.count > 0{
                self.doctorQualificationLabel.text = doctorObj.categoryArray.first
            }
        }
    }
    @IBAction func processRating(_ sender: UIButton) {
        if let consultationId = self.consultationObj?.consultationId{
            if let tag = selectedIndexPath{
                if ratingView.rating > 2.0{
                    self.selectedBehaviourString = whatWentWellOptionArray[tag.row]
                }else{
                    self.selectedBehaviourString = whatWentWrongOptionArray[tag.row]
                }
            }
             let params = ["consultationId":consultationId,"feedback":["rating":ratingView.rating,"ratingValue" :RatingValue(value: ratingView.rating).rawValue,"behaviour":selectedBehaviourString,"text":self.feedBackTextField.text!]] as [String : Any]
             submitFeedback(params: params as [String : AnyObject])
        }
    }
    func submitFeedback(params : [String : AnyObject]){
        let request = SubmitFeedback(params: params)
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let response = response {
                if let message = response["message"].string{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Success", subtitle: message, type: .success)
                }
                if let data = response["data"].array{
                    guard data.count > 0 else{
                        self.dismissController()
                        return
                    }
                    self.feedbackConsulatationArrray.removeAll()
                    for value in data{
                    let consulattionInfoObj = DpConsultationDetails(dict: value)
                    self.feedbackConsulatationArrray.append(consulattionInfoObj)
                    }
                    self.addInformationFromConsulatationObject(consulatationArray: self.feedbackConsulatationArrray)
                }else{
                    
                }
            }else{
                self.dismissController()
            }
        }
    }
//    func handleTap(sender: UITapGestureRecognizer) {
//        self.delegate?.selectedSpecialization(with: (sender.view?.tag)!)
//    }
}

extension DPRatingViewController: FloatRatingViewDelegate {
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        self.ratingValueLabel.text = RatingValue(value: rating).rawValue
        setBehaviourOptionView()
        serRatingQuestionAndOption(with: rating)
    }
    func serRatingQuestionAndOption(with rating : Float){
        switch RatingValue(value: rating){
        case .Bad,.Terrible :
             self.ratingQuestionLabel.text = "What went wrong?"
            self.selectedArray = whatWentWrongOptionArray
             self.selectedIndexPath = nil
            self.collectionView.reloadData()
            break
        case .Average,.Good,.Excellent :
             self.ratingQuestionLabel.text = "What went well?"
            self.selectedArray = whatWentWellOptionArray
             self.selectedIndexPath = nil
            self.collectionView.reloadData()
            break
        }
    }
    func setRatingQuestionAndOPtion(ratingQuetion : String,firstOption : String,seconOption : String){
//        self.ratingQuestionLabel.text = ratingQuetion
//        self.ratingQuestionFirstOpion.text = firstOption
//        self.ratingQuestionSecondOption.text = seconOption
    }
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Float) {
         self.ratingValueLabel.text = RatingValue(value: rating).rawValue
    }
}
class BehaviourCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var behaviourLabel: UILabel!
     @IBOutlet weak var behaviourLabelView: UIView!
}
extension DPRatingViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "behaviourCell", for: indexPath) as? BehaviourCollectionViewCell
        cell?.behaviourLabel.text = self.selectedArray[indexPath.row]
        if let slectedindex = self.selectedIndexPath{
            if slectedindex == indexPath{
                cell?.behaviourLabel.textColor = UIColor().greenDoxtroCgColor()
                cell?.behaviourLabelView.backgroundColor = UIColor(red: 239, green: 239, blue: 239)
            }else{
                 cell?.behaviourLabel.textColor = UIColor.black
                 cell?.behaviourLabelView.backgroundColor = UIColor.white
            }
        }else{
            cell?.behaviourLabel.textColor = UIColor.black
            cell?.behaviourLabelView.backgroundColor = UIColor.white
        }
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return whatWentWellOptionArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/4, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        collectionView.reloadData()
    }
}
