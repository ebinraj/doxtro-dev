//
//  DPMedicalHistory.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 03/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class MedicalHistory : UITableViewCell{
    var delegate : MedicineCourseDelegate?
    @IBOutlet weak var medicineNameLabel: UILabel!
    var urls:[UrlUtility] = []
    @IBAction func deleteButtonClicked(_ sender: Any) {
    UIAlertController.showAlertWithMessage("STR_DELETE_ITEM_MSG".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
            
        }, actionButtonHandler: { (_ , _) in
            if let button = sender as? UIButton{
                self.delegate?.deleteMedicatonCourse(row: button.tag,healthRecordType : .history)
            }
        })
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var showingTimeLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var dleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    func refresh(){
       if  self.collectionView != nil{
        self.collectionView.reloadData()
    }
    }
}
extension MedicalHistory :  UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let url = self.urls[indexPath.row]
        if url.urlType == "image"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "medicalHistoryimageCell", for: indexPath) as? MedicalHistoryImageCell
            if let url = url.url{
            cell?.hitoryImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "imagePlaceholder"))
            }
            
            return cell!
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "medicalHistoryDocumentsCell", for: indexPath) as? MedicalHitoryDocumentCell
        cell?.fileName.text = url.documentName
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.urls.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/4, height: 80)
    }
}
class MedicalHistoryImageCell : UICollectionViewCell{
    @IBOutlet weak var hitoryImageView: UIImageView!
}
class MedicalHitoryDocumentCell : UICollectionViewCell{

    @IBOutlet weak var fileName: UILabel!
}

