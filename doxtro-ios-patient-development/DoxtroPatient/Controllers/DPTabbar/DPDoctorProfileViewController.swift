//
//  DPDoctorProfileViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 05/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class DoctorProfileViewController : UIViewController{
    var doctorId : String?
    var doctorInfo : DoctorProfile?
    var sectionHeaderArray : [String] = ["","ABOUT ME","AREAS OF EXPERTISE","USER FEEDBACK","EDUCATION","PROFESSIONAL DETAILS"]
    
    @IBAction func openCalenderViewForDoctor(_ sender: Any) {
        if isArticle{
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "healtFeedselectedSpecializationlist") as! DPHealthFeedSelectedSpecilizationController
            viewController.doctorObj = self.doctorInfo
            self.navigationController?.pushViewController(viewController, animated: true)
        }else{
         self.performSegue(withIdentifier: "doctorProfileCalenderView", sender: nil)
        }
    }
    @IBOutlet weak var bookButtonView: UIView!
    var isPlanned : Bool = false
    var isArticle : Bool = false
    var doctorFee : String?
    var params : [String : AnyObject]?
    var doctorObj :  DPDoctorList?
    @IBOutlet weak var ratingView: FloatRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        doctorProfile()
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isPlanned{
        self.bookButtonView.isHidden = false
        }else if isArticle{
            self.bookButtonView.isHidden = false
        }else{
            self.bookButtonView.isHidden = true
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openCalenderView"{
            let destinationVC = segue.destination as! CalenderSlotController
            if let doctorObj = sender as? (DPDoctorList,[String : AnyObject]){
                destinationVC.doctorID = doctorObj.0._id
            }
            if let params = sender as? (DPDoctorList,[String : AnyObject]){
                destinationVC.params = params.1
            }
        }else if segue.identifier == "doctorProfileCalenderView"{
            let destinationVC = segue.destination as! CalenderSlotController
            if let doctorObj = self.doctorObj{
                destinationVC.doctorID = doctorObj._id
            }
            if let params = self.params{
                destinationVC.params = params
            }
        }
    }
    func doctorProfile(){
        let params = ["doctorId":doctorId]
        DPAppEventLog.shared.logEventName(EVENT_PROCEED_TO_PAY, values: ["Visited doctor's profile": true as AnyObject])
        let request = DoctorProfileRequest(prams: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if error == nil,let response = response{
                //debugPrint(response)
                if let data = response.dictionary{
                    self.doctorInfo = DoctorProfile(dict: JSON(data))
                }
                self.tableView.reloadData()
            }
        }
    }
    @IBOutlet weak var tableView: UITableView!
}
extension DoctorProfileViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4{
            if let qualification = self.doctorInfo?.profile?.qualification{
                return qualification.count
            }
        }
        if section == 5{
            if let worksFor = self.doctorInfo?.profile?.workFor{
                return worksFor.count
            }
        }
        if section == 3{
            if let feedback = self.doctorInfo?.feedBack{
                return feedback.count
            }
            
        }

        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "doctorProfileMain", for: indexPath) as? MainProfileCell
            if let doctorInfo = self.doctorInfo{
                cell?.configureCell(doctorInfo: doctorInfo)
            }
            if let doctorFee = self.doctorFee{
            cell?.doxtroFee = " ₹" + doctorFee
            }
            return cell!
        }else if indexPath.section == 1{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "doctorAboutMe") as? DoctorAboutMeCell
            cell?.aboutMeLabel.text = self.doctorInfo?.profile?.bio
            return cell!
        }else if indexPath.section == 2{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "areaOfExpertise", for: indexPath) as? AreaOfExpertiseTableViewCell
            if let doctorProfile = self.doctorInfo?.profile{
                cell?.specializationList = doctorProfile.specialization
                if doctorProfile.specialization.count > 2{
                    cell?.frame = CGRect(x: 0, y: 0, width: (cell?.frame.width)!, height: 200)
                }else{
                    cell?.frame = CGRect(x: 0, y: 0, width: (cell?.frame.width)!, height: (cell?.frame.height)!)
                }
            }
            return cell!
        }else if indexPath.section == 3{
            if let feebackInfo = self.doctorInfo?.feedBack[indexPath.row]{
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "feedbackCell", for: indexPath) as? FeeDBackCell
                cell?.nameLabel.text = feebackInfo.userName
                cell?.feedbackDescriptionLabel.text = feebackInfo.text
                cell?.timrLabel.text = DXUtility.convertDateAndTime(dateStr: feebackInfo.assignedAt)
                if let rating = feebackInfo.rating{
                    if rating == 0 || rating > 5{
                    cell?.ratingView.isHidden = true
                    }else{
                    cell?.ratingView.isHidden = false
                            cell?.ratingView.rating = Float(rating)
                    }
                }
                
                if let profilePicURl = feebackInfo.profilePic{
                    cell?.profileImage.af_setImage(withURL: profilePicURl, placeholderImage: UIImage(named: "imagePlaceholder"))
                }
                return cell!
            }
        }else if indexPath.section == 4{
            if let doctorInfo = self.doctorInfo?.profile?.qualification[indexPath.row]{
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "educationCell", for: indexPath) as? DoctorEducationCell
                cell?.collegeName.text = doctorInfo.college
                if let type = doctorInfo.type,let degree = doctorInfo.degree{
                    if degree == "Others"{
                         cell?.degreeName.text = type + " - " + "Others- "+doctorInfo.additionalData
                    }else{
                        cell?.degreeName.text = type + " - " + degree
                    }
                
                }
                if let yearTo = doctorInfo.yearTo,let yearFrom = doctorInfo.yearFrom,yearTo != "",yearFrom != ""{
                cell?.yearLabel.text = yearTo +  " - " + yearFrom
                }
                return cell!
                
            }
        }else if indexPath.section == 5{
                        if let doctorInfo = self.doctorInfo?.profile?.workFor[indexPath.row]{
                            let cell = self.tableView.dequeueReusableCell(withIdentifier: "workForCell", for: indexPath) as? WorkForCell
                            cell?.collegeName.text = doctorInfo.organization
                            cell?.yearLabel.text = "Experience: " + doctorInfo.experience + " year"
                            return cell!
            
                        }
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2{
            if let specialization = self.doctorInfo?.profile?.specialization{
                if specialization.count > 2 {
                    return 90
                }
                return 40
            }
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView{
        if section != 0{
            let sectionview = UINib(nibName: "DPDoctorProfileSectionHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? DoctorProfileSectionView
            if let requiredView = sectionview{
                requiredView.sectionTitle.text = sectionHeaderArray[section]
                return requiredView
            }
        }
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 40
    }
}
class DoctorAboutMeCell : UITableViewCell{
    @IBOutlet weak var aboutMeLabel: UILabel!
}
class MainProfileCell : UITableViewCell{
    @IBOutlet weak var specializationDegreeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var doctorProfileImage: UIImageView!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var consultedLabel: UILabel!
    @IBOutlet weak var newDegreeCell: UILabel!
    var doxtroFee : String?
    override func awakeFromNib() {
        super.awakeFromNib()
        doctorProfileImage.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2.0)
    }
    func configureCell(doctorInfo : DoctorProfile){
        if let doctorProfile = doctorInfo.profile{
            if let imageUrl = doctorProfile.profilePic{
                self.doctorProfileImage.af_setImage(withURL:imageUrl, placeholderImage: UIImage(named: "imagePlaceholder"))
            }
            self.nameLabel.text = doctorProfile.firstName
            self.experianceLabel.text = (doctorProfile.experience ?? "") + " yrs experience"
            var specializationQualification : String = ""
            for (index,degree) in doctorProfile.category.enumerated(){
                if index == 0{
                    specializationQualification = degree
                }else{
                    specializationQualification = specializationQualification + " , " + degree
                }
                
            }
            self.specializationDegreeLabel.text = specializationQualification
             var newCategory : String = ""
            for (index,degree) in doctorProfile.qualification.enumerated(){
                if index == 0{
                    newCategory = degree.degree!
                }else{
                    if degree.degree == "Others"{
                        newCategory = newCategory + " , " + degree.additionalData
                    }else{
                        newCategory = newCategory + " , " + degree.degree!
                    }
                    
                }
                
            }
            self.newDegreeCell.text = newCategory
        }
        if let doxtroFee = self.doxtroFee{
            self.feeLabel.text = "Doxtro Fee : " + "\(doxtroFee)"
        }
        if let consulattionCount = doctorInfo.consultationCount{
            self.consultedLabel.text = "👍 " + "\(consulattionCount)" + " Patient Consulted"
        }
    }
}


class AreaOfExpertiseTableViewCell : UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    var specializationList : [String] = []
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
      //  print(specializationList.count)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return specializationList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExpertiseCollectionViewCell", for: indexPath) as? AreaOfExpertiseCollectionViewCell
        cell?.expertiseLabel.text = specializationList[indexPath.row]
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width * 0.45, height: collectionView.bounds.height * 0.41)
    }
}
class AreaOfExpertiseCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var expertiseLabel: UILabel!
}

class DoctorEducationCell : UITableViewCell{
    @IBOutlet weak var collegeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var degreeName: UILabel!
}

class WorkForCell : UITableViewCell{
    @IBOutlet weak var collegeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBOutlet weak var yearLabel: UILabel!
}
class FeeDBackCell : UITableViewCell{
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var feedbackDescriptionLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var timrLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 1)
    }
}

//healtFeedselectedSpecializationlist
//let viewController = storyBoard.instantiateViewController(withIdentifier: "selectedSpecializationlist") as! DPSelectedSpecilizationController
