//
//  DPSettingTabBarController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 31/07/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
let REMINDER_DEFAULT_KEY = "ReminderStatus"
let settingPageArray = ["Doxtro cash","Transaction history","Reminders","Refer and earn","Deactivate account","Terms and conditions","Help","About","Logout"]
class DPSettingTabBarController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userAgeLabel: UILabel!
    var doxtroCash = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        userImageView.makeCircleandBorder(with:(UIColor().greenDoxtroCgColor().cgColor))
        self.tableView.separatorStyle = .none
        self.tableView.register(UINib(nibName: "DPSettingPageCell", bundle: nil), forCellReuseIdentifier: "settingPageCell")
        self.tableView.register(UINib(nibName: "DPReminderCell", bundle: nil), forCellReuseIdentifier: "settingReminderCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          self.tableView.reloadData()
        showDoxtroCash(isshowIndicator: false)
        if let user = DPWebServicesManager.sharedManager.user{
            self.userNameLabel.text = user.name
            if (user.gender == "" && user.age == "") {
              self.userAgeLabel.text = ""
            }else if user.gender == "" {
                self.userAgeLabel.text = user.age
            }else if user.age == "" {
                self.userAgeLabel.text = user.gender
            }else {
                self.userAgeLabel.text = user.gender! + ", " + user.age!
            }
            if let url = URL(string: user.profileUrl ?? "") {
                self.userImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "imagePlaceholder"))
            }
            
        }
    }
    func showDoxtroCash(isshowIndicator : Bool = true){
       if isshowIndicator{
        ABProgressIndicator.shared.showAnimator()
        }
         DoxtroCash.getDoxtroCash(completationHandler: { (doxtroCash) in
            if isshowIndicator{
                ABProgressIndicator.shared.hideAnimator()
            }
            self.refreshDoxtroCAsh(doxtroCash: doxtroCash)
        })
    }
    func refreshDoxtroCAsh(doxtroCash : Int ){
        self.doxtroCash = doxtroCash
        if let row = settingPageArray.index(of: "Doxtro cash"){
            self.tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: UITableViewRowAnimation.fade)
        }
    }
  }

extension DPSettingTabBarController{
    @IBAction func editProfile(_ sender: Any) {
        self.performSegue(withIdentifier: "editProfile", sender: nil)
    }
}
extension DPSettingTabBarController :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return settingPageArray.count
    }
    func showDoxtroSupportView(){
            let view =  Bundle.main.loadNibNamed("DPHelpDoxtroView", owner: nil, options: nil)
            if let mainView = view?[0] as? HelpDoxtroView{
              //  mainView.delegate = self
                mainView.frame = (UIApplication.getTopViewController()?.view.bounds)!
                UIApplication.getTopViewController()?.view.addSubview(mainView)
            }
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if settingPageArray[indexPath.row] == "Reminders" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingReminderCell", for: indexPath) as? ReminderCell
            cell?.settingTitleLabel.text = settingPageArray[indexPath.row]
            cell?.settingIconImageView.image = UIImage(named:settingPageArray[indexPath.row])
            cell?.delegate = self
            cell?.setSwitchButtonStatus()
            return cell!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingPageCell") as? DPSettingPageCell
        cell?.settingTitleLabel.text = settingPageArray[indexPath.row]
        cell?.settingIconImageView.image = UIImage(named:settingPageArray[indexPath.row])
        if settingPageArray[indexPath.row] == "Doxtro cash" {
            cell?.commingSoonLabel.isHidden = false
            cell?.commingSoonLabel.text = "   ₹ " + String(describing: self.doxtroCash) + "    "
            cell?.setSwitchButtonStatus()
        }else{
             cell?.commingSoonLabel.isHidden = true
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIScreen.main.bounds.height-194)/9
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        if let cell = tableView.cellForRow(at: indexPath) as? DPSettingPageCell{
            if cell.settingTitleLabel.text == "Logout"{
                UIAlertController.showAlertWithMessage("STR_LOGOUT_MSG".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
                    
                }, actionButtonHandler: { (_ , _) in
                    self.logout()
                    DPWebServicesManager.userLogout()
                    UIApplication.appDelegate().setUpViewController(With: "Main", and: "signIn")
                })
            }else if cell.settingTitleLabel.text == "Transaction history"{
                self.performSegue(withIdentifier: "showTransactionHistory", sender: nil)
            }else if cell.settingTitleLabel.text == "Terms and conditions" {
                if let url = NSURL(string: "http://doxtro.com/termsandprivacy"){
                    loadtermsViewController(url: "http://doxtro.com/termsandprivacy")
                }
                DPAppEventLog.shared.logEventName(EVENT_SETTINGS, values: ["Terms & Conditions" : "" as AnyObject])
            }else if cell.settingTitleLabel.text == "About" {
                self.performSegue(withIdentifier: "openAboutController", sender: nil)
//                if let url = NSURL(string: "http://doxtro.com/termsandprivacy") {
//                    loadtermsViewController(url: "http://doxtro.com/termsandprivacy",navTitle : "Doxtro")
//                }
                DPAppEventLog.shared.logEventName(EVENT_SETTINGS, values: ["About us" : "" as AnyObject])
            } else if cell.settingTitleLabel.text == "Help" {
                showDoxtroSupportView()
                DPAppEventLog.shared.logEventName(EVENT_SETTINGS, values: ["Help" : "" as AnyObject])
            }else if cell.settingTitleLabel.text == "Deactivate account"{
                DPWebServicesManager.DeactiveUserAccount()
            }else if cell.settingTitleLabel.text == "Doxtro cash"{
                let destinationVc = self.storyboard?.instantiateViewController(withIdentifier: "doxtroCashController") as! DoxtroCashVIewController
                destinationVc.doxtroCash = self.doxtroCash
                destinationVc.completionHandler = {(doxtroCash,paymentData) in
                     self.refreshDoxtroCAsh(doxtroCash: doxtroCash)
                }
                self.present(destinationVc, animated: true, completion: nil)
            }else if cell.settingTitleLabel.text == "Refer and earn"{
                let destinationVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "referelCodeController") as! ReferelViewController
                self.present(destinationVC, animated: true, completion: nil)
            }
        }
    }
    func logout(){
        if let userId = DPWebServicesManager.sharedManager.user?.userId,let deviceID  = UIApplication.appDelegate().deviceToken as? String{
        let params : [String : AnyObject] = ["userId":userId as AnyObject,"deviceId":deviceID as AnyObject]
        let request = UserLogoutRequest(params: params)
        DPWebServicesManager.getResponse(with: request) { (response , error) in
           // debugPrint(response)
        }
    }
    }
}
extension DPSettingTabBarController : ReminderSwitchButtonDelegate{
    func switchButtonStatus(_ sender: Any) {
        if let switchButon = sender as? UISwitch{
            if switchButon.isOn{
                 DPUser.saveReminderStatus(status: true)
            }else{
                 DPUser.saveReminderStatus(status: false)
            }
        }
    }
    
}
