//
//  DPSettingPageCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 31/07/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol ReminderSwitchButtonDelegate {
    func switchButtonStatus(_ sender: Any)
}
class DPSettingPageCell : UITableViewCell{
    var delegate : ReminderSwitchButtonDelegate?
    @IBAction func switchButtonValueAction(_ sender: Any) {
        self.delegate?.switchButtonStatus(sender: sender)
    }
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var settingTitleLabel: UILabel!
    @IBOutlet weak var commingSoonLabel: UILabel!
    @IBOutlet weak var settingIconImageView: UIImageView!
    func setSwitchButtonStatus(){
        if let status = DPUser.getReminderStatus(){
            if status{
                self.switchButton.isOn = true
            self.switchButton.setOn(true, animated: true)
            }else{
                 self.switchButton.isOn = false
                self.switchButton.setOn(false, animated: true)
            }
        }
    }
}
class ReminderCell : UITableViewCell{
    var delegate : ReminderSwitchButtonDelegate?
    @IBAction func switchButtonValueAction(_ sender: Any) {
        self.delegate?.switchButtonStatus(sender: sender)
    }
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var settingTitleLabel: UILabel!
    @IBOutlet weak var settingIconImageView: UIImageView!
    func setSwitchButtonStatus(){
        if let status = DPUser.getReminderStatus(){
            if status{
                self.switchButton.isOn = true
                self.switchButton.setOn(true, animated: true)
            }else{
                self.switchButton.isOn = false
                self.switchButton.setOn(false, animated: true)
            }
        }else{
            self.switchButton.isOn = false
            self.switchButton.setOn(false, animated: true)
        }
    }
}
