//
//  DPIntorleranceCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 02/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class IntorleranceCell : UITableViewCell{
    var delegate : MedicineCourseDelegate?
    @IBOutlet weak var medicineNameLabel: UILabel!
    @IBAction func addMedicineButtonClicked(_ sender: Any) {
        self.delegate?.addMedicationCourse(healthRecordType : .intolerance)
    }
    @IBAction func deleteButtonClicked(_ sender: Any) {
        UIAlertController.showAlertWithMessage("STR_DELETE_ITEM_MSG".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
            
        }, actionButtonHandler: { (_ , _) in
            if let button = sender as? UIButton{
                self.delegate?.deleteMedicatonCourse(row: button.tag,healthRecordType : .intolerance)
            }
        })
    }
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var dleteButton: UIButton!
    @IBOutlet weak var addMedicineButton: UIButton!
}
