//
//  DPTakeMedicineView.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol AddNewMedicineDelegate  {
    func removeView()
    func saveMedicationInfo(healthRecordType : HealthRecordType,medicineTextField : UITextField,contentTextField : UITextField)
}
class TakeMedicineView : UIView{
    var delegate : AddNewMedicineDelegate?
    var  healthRecordType: HealthRecordType = .medication
    @IBAction func removeView(_ sender: Any) {
        self.delegate?.removeView()
    }
    @IBOutlet weak var titleView: UIView!
    
    @IBAction func saveMedicine(_ sender: Any) {
        self.delegate?.saveMedicationInfo(healthRecordType : healthRecordType,medicineTextField: self.medicineNameTextField,contentTextField: disesaseCauseTextField)
    }
    @IBOutlet weak var disesaseCauseTextField: UITextField!
    @IBOutlet weak var medicineNameTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
     self.medicineNameTextField.addBootomBorderLine(width: 2)
    self.disesaseCauseTextField.addBootomBorderLine(width: 2)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        let path = UIBezierPath(roundedRect:self.titleView.bounds,
                                byRoundingCorners:[.topLeft, .topRight],
                                cornerRadii: CGSize(width: 4, height:  4))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.titleView.layer.mask = maskLayer
    }
}
