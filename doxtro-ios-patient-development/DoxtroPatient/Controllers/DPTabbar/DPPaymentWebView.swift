//
//  DPPaymentWebView.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 14/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class PaymentWebViewController : UIViewController,UIWebViewDelegate{
    @IBOutlet weak var paymentWebView: UIWebView!
    var request : NSMutableURLRequest?
    var completionHandler: ((_ value : String) -> Void)?
    var consultationId : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentWebView.delegate = self
       paymentWebView.loadRequest((request! as URLRequest))
    }
    @IBAction func dismissWebView(_ sender: Any) {
                   self.dismiss(animated: true, completion: nil)
       
    }
    @IBOutlet weak var dismissWebView: UILabel!
    
    @IBOutlet weak var buttonclickec: UIButton!
    func webViewDidFinishLoad(_ webView: UIWebView) {
       
       /* NSString *string = webView.request.URL.absoluteString;
        if ([string rangeOfString:@"/ccavResponseHandler.jsp"].location != NSNotFound) {
            NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
            
            NSString *transStatus = @"Not Known";
            
            if (([html rangeOfString:@"Aborted"].location != NSNotFound) ||
                ([html rangeOfString:@"Cancel"].location != NSNotFound)) {
                transStatus = @"Transaction Cancelled";
            }else if (([html rangeOfString:@"Success"].location != NSNotFound)) {
                transStatus = @"Transaction Successful";
            }else if (([html rangeOfString:@"Fail"].location != NSNotFound)) {
                transStatus = @"Transaction Failed";
            }
        */

    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        //print(error)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        paymentWebView.loadRequest((request! as URLRequest))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
