//
//  DPEditProfileController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/08/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
import FirebaseStorage
import AlamofireImage
let FootArray : [String] = ["Foot","1","2","3","4","5","6","7","8","9","10"]
let inchArray : [String] = ["Inch","1","2","3","4","5","6","7","8","9","10","11","12"]
protocol DPHeightPickerDeledate{
    func removeHeightPickerView()
    func doneWithSelectingHeight(with height : String)
}
class DPheightPickerViewController : UIView,UIPickerViewDelegate,UIPickerViewDataSource{
    @IBOutlet weak var footPicker: UIPickerView!
    var selectedHeight : String = ""
    var delegate : DPHeightPickerDeledate?
    var footString = ""
    var inchString = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        self.footPicker.delegate = self
        self.footPicker.dataSource = self
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return FootArray.count
        case 1:
            return inchArray.count
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return FootArray[row]
        case 1:
            return inchArray[row]
        default:
            return ""
        }

    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            footString = FootArray[row] + "' "
            break
        case 1:
            inchString = inchArray[row] + "''"
            break
        default:
            break
        }
        self.selectedHeight = footString + inchString
    }
    @IBAction func doneWithSelectingHeight(_ sender: Any) {
        self.delegate?.doneWithSelectingHeight(with: self.selectedHeight)
    }
    
    @IBAction func removeHeightPickerView(_ sender: Any) {
        self.delegate?.removeHeightPickerView()
    }
}
class DPEditProfileController : UIViewController{
    @IBOutlet weak var tableView: UITableView!
    @IBAction func dismissController(_ sender: Any) {
        self.dismissController()
    }
    @IBOutlet weak var userProfileImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.register(UINib(nibName: "DPEditProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "editProfileCellIID")
        self.userProfileImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor)
    }
}
extension DPEditProfileController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            uploadPhoto(image: editedImage)
        }
        else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadPhoto(image: image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func uploadPhoto(image: UIImage) {
        guard let isReachable = DPWebServicesManager.sharedManager.reachability?.isReachable, isReachable == true  else {
            ABProgressIndicator.shared.hideAnimator()
             TSMessage.showNotification(in:self, title: "Network Error", subtitle:  "Your internet connection disappeared!", type: .error)
            return
        }
        let storageRef: StorageReference = Storage.storage().reference().child("Profile_Images").child("user.png")
        let resizeImage = DXUtility.shared.resizeImage(image: image, newWidth: 400)
        if let uploadData = UIImagePNGRepresentation(resizeImage) {
            ABProgressIndicator.shared.showAnimator()
            storageRef.putData(uploadData, metadata: nil, completion: { (metaData, error) in
                if error == nil {
                    if let user = DPWebServicesManager.sharedManager.user{
                    self.updateUserProfile(with: ["profilePic": metaData?.downloadURL()?.absoluteString as AnyObject,"_id": user.userId as AnyObject],photo: true)
                    }else{
                         ABProgressIndicator.shared.hideAnimator()
                    }
                }else{
                ABProgressIndicator.shared.hideAnimator()
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: error?.localizedDescription, type: .error)
                }
            })
        }
    }
}
extension DPEditProfileController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"editProfileCellIID") as?DPEditProfileTableViewCell
        if let user = DPWebServicesManager.sharedManager.user{
            cell?.ageTextField.text = user.age
            cell?.mobileTextField.text = user.mobileNum
            cell?.fullNameTextField.text = user.name
            cell?.emailTextField.text = user.emailId
            cell?.weightTextFeld.text = user.weight
            cell?.heightTextField.text = user.height
            if let url = URL(string: user.profileUrl ?? "") {
              self.userProfileImageView.af_setImage(withURL: url, placeholderImage: UIImage(named: "imagePlaceholder"))
            }
            self.userProfileImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor)
            if user.gender == "Male"{
                cell?.genderSegmentControl.selectedSegmentIndex = 0
                cell?.changeToSelectedSegmentColor(selectedIndex: 0)
            }else{
                cell?.genderSegmentControl.selectedSegmentIndex = 1
                cell?.changeToSelectedSegmentColor(selectedIndex: 1)
            }
            cell?.delegate = self
        }
     return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 478
    }
}
extension DPEditProfileController : DPUpdateProfileDelegate{
    func updateUserProfile(with params: [String : AnyObject],photo : Bool = false) {
        let updateProfileRequest = UpdateProfileRequest(params: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: updateProfileRequest ,completionHandler: { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let data = response?["data"].dictionary{
                let userinfo = DPUser(data: data)
                DPWebServicesManager.sharedManager.saveUser(with: userinfo)
                self.getallSpecializaion()
                if let message = response?["message"].stringValue{
                TSMessage.showNotification(withTitle: "Updated", subtitle:message, type: .success)
                    if !photo{
                    self.dismissController()
                    }
                }
            }else{
                if let message = response?["message"].stringValue{
                TSMessage.showNotification(withTitle: "Error", subtitle:message, type: .error)
                }
            }
            self.tableView.reloadData()
        })
    }
    func getallSpecializaion(){
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
            let prams = ["patienId" :userID ]
            let request = GetAllSpecialization(params: prams as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if let resposne  = response{
                    if let specializationArray = resposne["data"].array{
                        if let user =  DPWebServicesManager.sharedManager.user{
                            var values = [String : AnyObject]()
                            values["Name"] = user.name as AnyObject
                            values["Email"] = user.emailId as AnyObject
                            values["Phone"] = user.mobileNum as AnyObject
                            values["Age"] = user.age as AnyObject
                            values["Gender"] = user.gender as AnyObject
                             values["specialization"] = specializationArray as AnyObject
                            DPAppEventLog.shared.logEventName(EVENT_COMPLETED_SIGNUP, values: values)
                            CleverTap.sharedInstance()?.onUserLogin(values)
                        }
                    }
                }
                
                
            })
        }
    }
//    func createMember(){
//            if let user = DPWebServicesManager.sharedManager.user{
//                if let age = user.age,let gender = user.gender,let name = user.name{
//                    if age != "" && name != "" && gender != "" {
//                        let params  : [String : String] = ["patientId":user.userId ?? "","relation": "Myself",
//                                                           "name": "Myself",
//                                                           "age": age ,
//                                                           "gender": gender]
//
//                        self.saveAddNewMemberInfo(params : params as [String : AnyObject])
//                    }
//                }
//            }
//    }
    func saveAddNewMemberInfo(params : [String : AnyObject]) {
        let createMemberrequest  = createMemberRequest(params: params)
        ABProgressIndicator.shared.showAnimator()
        self.navigationController?.popViewController(animated: true)
        DPWebServicesManager.getResponse(with: createMemberrequest, completionHandler: { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if let _ = response?["data"]{
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: REFRESH_RELATIVE_MEMBER_NOTIFICATION_KEY)))
            }else{
                if let message = response?["message"].stringValue{
                    TSMessage.showNotification(withTitle: "Error", subtitle:message, type: .error)
                }
            }
        })
    }
    func showHeightPicker() {
         self.view.endEditing(true)
         let view =  Bundle.main.loadNibNamed("DPHeightPickerController", owner: nil, options: nil)
         if let mainView = view?[0] as? DPheightPickerViewController{
            mainView.delegate = self
            mainView.frame = self.view.bounds
            self.view.addSubview(mainView)
        }
        
    }
    @IBAction func uploadPhotoClicked(_ sender: Any) {
        openImageAndDocumentsPicker()
    }
}
extension UIViewController{
    func openImageAndDocumentsPicker(withDocuments : Bool = false){
        let imagePickerController = UIImagePickerController()
         imagePickerController.navigationBar.tintColor = UIColor().greenDoxtroCgColor()
        imagePickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePickerController.allowsEditing = true
        let actionSheet = UIAlertController(title: "Photo source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (alertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
        }))
        if withDocuments{
            let importMenu = UIDocumentMenuViewController(documentTypes: ["public.composite-content"], in: .import)
            importMenu.delegate = self as? UIDocumentMenuDelegate
            importMenu.modalPresentationStyle = .formSheet
            actionSheet.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (alertAction) in
                self.present(importMenu, animated: true, completion: nil)
            }))
        }
        present(actionSheet, animated: true, completion: nil)
    }
}
extension DPEditProfileController : DPHeightPickerDeledate{
    func removeHeightPickerView() {
        for heightView in self.view.subviews{
            if heightView is DPheightPickerViewController{
                heightView.removeFromSuperview()
            }
        }
    }
    func doneWithSelectingHeight(with height: String) {
        let cell = tableView.visibleCells[0] as? DPEditProfileTableViewCell
        cell?.heightTextField.text = height
        for heightView in self.view.subviews{
            if heightView is DPheightPickerViewController{
                heightView.removeFromSuperview()
            }
        }
    }
}
