//
//  DPLongPressMemberView.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 08/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol LongPressDeleteDelgate {
    func removeLongPressView()
}
class LongPressMemberView : UIView{
    var delegate : LongPressDeleteDelgate?
    @IBAction func removeView(_ sender: Any) {
        self.delegate?.removeLongPressView()
    }
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var relationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
}
