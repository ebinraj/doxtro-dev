//
//  DPPaymentController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 14/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
struct CCAvenue {
    struct Credential {
        static let ACCESS_CODE = "AVWX73EI08BB77XWBB"
        static let MERCHANT_ID = "141395"
        static let TRANSACTIO_PATH = "transaction/initTrans"
        static let PAYMENT_OPTION_PATH = "transaction/transaction.do"
    }
    struct URLS {
        static let LIVE = "https://secure.ccavenue.com/"
        static let TEST = "https://test.ccavenue.com/"
    }
}
/*
 "data" : {
 "ORDER_ID" : "Doxtro_136275",
 "MID" : "Doxtro67641407951767",
 "WEBSITE" : "APP_STAGING",
 "CUST_ID" : "598985e230548821d4dd5be1",
 "CALLBACK_URL" : "https:\/\/pguat.paytm.com\/paytmchecksum\/paytmCallback.jsp",
 "CHANNEL_ID" : "WAP",
 "EMAIL" : "ayyshdac1992@gmail.com",
 "TXN_AMOUNT" : "240",
 "CHECKSUMHASH" : "OQwIHTZnPu3HE5iw\/yYg39DMoZYvi8xtkm+9Pq46aCzE+6k45rz5ElzONWTqvkpStxcsSa8enPF0kJSpc9pUpSxDA70UU0ohOWs+kgJPCeY=",
 "INDUSTRY_TYPE_ID" : "Retail",
 "MOBILE_NO" : "+918123632112",
 "REQUEST_TYPE" : "DEFAULT"
 },
 "message" : "checksum generated"
 })
 */
class CheckSumPaytm{
    var ORDER_ID : String?
    var MID : String?
    var WEBSITE : String?
    var CUST_ID : String?
    var CALLBACK_URL : String?
    var CHANNEL_ID : String?
    var EMAIL : String?
    var TXN_AMOUNT : String?
    var CHECKSUMHASH : String?
    var INDUSTRY_TYPE_ID : String?
    var MOBILE_NO : String?
    var REQUEST_TYPE : String?
    init(dict : JSON){
        self.ORDER_ID = dict["ORDER_ID"].string ?? ""
        self.MID = dict["MID"].string ?? ""
        self.WEBSITE = dict["WEBSITE"].string ?? ""
        self.CUST_ID = dict["CUST_ID"].string ?? ""
        self.CALLBACK_URL = dict["CALLBACK_URL"].string ?? ""
        self.CHANNEL_ID = dict["CHANNEL_ID"].string ?? ""
        self.EMAIL = dict["EMAIL"].string ?? ""
        self.TXN_AMOUNT = dict["TXN_AMOUNT"].string ?? ""
        self.CHECKSUMHASH = dict["CHECKSUMHASH"].string ?? ""
        self.INDUSTRY_TYPE_ID = dict["INDUSTRY_TYPE_ID"].string ?? ""
        self.MOBILE_NO = dict["MOBILE_NO"].string ?? ""
        self.REQUEST_TYPE = dict["REQUEST_TYPE"].string ?? ""
    }
    
}
class DPPaymentController {
    static func getRsaKey(orderID : String,consultationId : String,completationHandler : @escaping(_ reponse : JSON?,_ error : Error?)->Void){
        let paranms  = ["order_id" : orderID,"consultationId" : consultationId,"env":"live"]
        let request = GetRsaRequest(params: paranms as [String : AnyObject])
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            if let response = response{
                completationHandler(response,nil)
            }else if error != nil{
                completationHandler(nil,error)
            }
        }
    }
}
class CouponInfo{
    var consultingFee : Int = 0
    var totalPayable :  Int = 0
    var couponDiscount :Int = 0
    var doxtroCash :  Int = 0
    var usedDoxtroCash : Int = 0
    var couponTag : String = ""
    var isFree : Bool = false
    var couponId : String = ""
    var  remainingDoxtroCash : Int = 0
    init(dict : JSON) {
        self.consultingFee = dict["consultingFee"].int ?? dict["price"].intValue
        self.totalPayable = dict["totalPayable"].int ?? dict["amount"].intValue
        self.couponDiscount = dict["couponDiscount"].intValue
        self.doxtroCash = dict["doxtroCash"].intValue
        self.usedDoxtroCash = dict["usedDoxtroCash"].intValue
        self.couponTag = dict["couponTag"].stringValue
        self.isFree = dict["isFree"].boolValue
        self.couponId = dict["couponId"].stringValue
        self.remainingDoxtroCash = dict["remainingDoxtroCash"].int ?? 0
    }
    
}


extension String{
    func Url()->URL?{
        if let url = URL(string: self){
           return url
        }
        return nil
    }
}
