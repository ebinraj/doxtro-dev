//
//  DPHealthTabBarController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 31/07/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Firebase
import FirebaseStorage
//"history", "report", "medication", "intolerance"
enum HealthRecordType : String{
    case history
    case report
    case medication
    case intolerance
    case none
    init(value : String){
        switch value{
        case "history":
            self = .history
         break
        case "report":
            self = .report
            break
        case "medication":
            self = .medication
            break
        case "intolerance":
            self = .intolerance
            break
        default:
            self = .none
            break
        }
    }
}
import TSMessages
class DPHealthTabBarController : UIViewController{
    var relativeId : String?
    var relativeMemberArray : [DPRelativeMember] = []
    var healthRecord : HealthRecord?
    var completionHandler: ((_ image : UIImage) -> Void)?
    @IBOutlet weak var tableView: UITableView!
   let helathSectionArray = ["MEDICAL HISTORY","DIAGNOSTIC RECORDS","CURRENT MEDICATION COURSE","ALLERGIES/ DRUG INTOLERANCE"]
    var selectedIndexPath : IndexPath?
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "DPNoResultFoundCell", bundle: nil), forCellReuseIdentifier: "NoResultFoundCell")
        self.tableView.register(UINib(nibName: "DPAddNewButtonCell", bundle: nil), forCellReuseIdentifier: "addNewButtonCell")
         self.automaticallyAdjustsScrollViewInsets = false
        self.title = "My health"
        self.navigationController?.navigationBar.barTintColor = UIColor().greenDoxtroCgColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        //userImageView.makeCircleandBorder(with:(UIColor().greenDoxtroCgColor().cgColor))
        getRelativeList()
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshData), name: NSNotification.Name(rawValue: "MedicalHitoryRefresh"), object: nil)
        // self.automaticallyAdjustsScrollViewInsets = false
    }
    func refreshData(){
        if let relativeId  = self.relativeId{
            self.fetchDiagnosticRecord(relativeId: relativeId)
        }
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        if let user = DPWebServicesManager.sharedManager.user{
//            self.userNameLabel.text = user.name
//            if let url = URL(string: user.profileUrl ?? "") {
//                self.userImageView.af_setImage(withURL: url)
//                self.userAgeLabel.text = user.gender! + ", " + user.age!
//            }
//
//        }
//    }
   func getRelativeList(){
    DPWebServicesManager.sharedManager.getRelativeList(completionHandler: { (error, response) in
    if let response = response{
    self.relativeMemberArray = response
      self.relativeMemberArray.reverse()
        self.collectionView.reloadData()
        if let firstMemberId = self.relativeMemberArray.first?.id{
            self.relativeId = firstMemberId
        self.fetchDiagnosticRecord(relativeId: firstMemberId)
        }
    }
    })
    }
    func fetchDiagnosticRecord(relativeId : String){
        let params = ["relativesId" : relativeId]
        let request = FetchHealthRecordRequest(params:params as [String : AnyObject] )
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with:request) { (response, error) in
             ABProgressIndicator.shared.hideAnimator()
            //debugPrint(response)
            if error == nil,let response = response {
                if let data = response["data"].array{
                      self.healthRecord = HealthRecord(dicts: data)
                    self.tableView.reloadData()
                }
            }else{
                
            }
        }
    }
}
//
//extension DPHealthTabBarController{
//    @IBAction func editProfile(_ sender: Any) {
//        self.performSegue(withIdentifier: "editProfile", sender: nil)
//    }
//}
extension DPHealthTabBarController : ShowLongPressView,LongPressDeleteDelgate{
    func longPressView(row : Int) {
        let memberObj  = self.relativeMemberArray[row]
        showAddNewMemeberViews(with: memberObj)
    }
    func removeLongPressView() {
        for memeberView in (self.view.subviews){
            if memeberView is LongPressMemberView{
                memeberView.removeFromSuperview()
            }
        }
    }
    func showAddNewMemeberViews(with relativeObj : DPRelativeMember) {
        let view =  Bundle.main.loadNibNamed("DPLongPressMemberView", owner: nil, options: nil)
        if let mainView = view?[0] as? LongPressMemberView{
            mainView.delegate = self
            mainView.nameLabel.text = relativeObj.name
            mainView.ageLabel.text = String(describing: relativeObj.age!)
            mainView.genderLabel.text = relativeObj.gender
            mainView.relationLabel.text = relativeObj.relation
            mainView.frame = (self.view.bounds)
            self.view.addSubview(mainView)
        }
        
    }
}
extension DPHealthTabBarController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.relativeMemberArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memberCillectionViewCell", for: indexPath) as? DPMemberCollectionViewCell
        let memberObj = self.relativeMemberArray[indexPath.row]
        if let relation = memberObj.relation,let gender = memberObj.gender{
            cell?.memberImageView?.image = relation.imageForRelation(with: gender)
        }
        cell?.delegate = self
        cell?.indexPath = indexPath
        cell?.memberNameLabel?.text = memberObj.name
        if self.selectedIndexPath == nil{
            if indexPath.row == 0{
                self.selectedIndexPath = indexPath
            }
        }
        if memberObj.relation == "Myself" || memberObj.relation == "MySelf"{
            cell?.memberNameLabel?.text = memberObj.relation
        }else{
            cell?.memberNameLabel?.text = memberObj.name
        }
        cell?.configureCellSelection(selectedIndexPath: selectedIndexPath, currentIndexPath: indexPath)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         self.selectedIndexPath = indexPath
        self.collectionView.reloadData()
         let selectedMemberObj = self.relativeMemberArray[indexPath.row]
        if let patientId = selectedMemberObj.patientId,let relativeId = selectedMemberObj.id{
              showHistoryFor(patienId: patientId, relativeId: relativeId)
        }
        if let relativeId = selectedMemberObj.id{
            self.relativeId = relativeId
        }
    }
    func showHistoryFor(patienId : String,relativeId : String){
        self.fetchDiagnosticRecord(relativeId: relativeId)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/4, height: collectionView.bounds.height)
    }
}
extension DPHealthTabBarController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let healthRecord = self.healthRecord{
            if section == 0{
                if healthRecord.medicalHisory.count == 0{
                    return (healthRecord.medicalHisory.count) + 2
                }
               return healthRecord.medicalHisory.count  + 1
            }else if section == 1{
            return 1
        }else if section == 2{
            if healthRecord.medication.count == 0{
                 return (healthRecord.medication.count) + 2
            }
            return (healthRecord.medication.count) + 1
        }else if section == 3{
            if healthRecord.intolerance.count == 0{
                return (healthRecord.intolerance.count) + 2
            }
         return (healthRecord.intolerance.count) + 1
        }
      }
     return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if (healthRecord?.medicalHisory.count)! > 0 {
                if (healthRecord?.medicalHisory.count)!-1 >= indexPath.row{
              let cell = tableView.dequeueReusableCell(withIdentifier: "medicalHistory", for: indexPath) as? MedicalHistory
                    let obj = healthRecord?.medicalHisory[indexPath.row]
                    cell?.delegate = self
                    cell?.medicineNameLabel.text = obj?.content
                    cell?.dleteButton.tag = indexPath.row
                    cell?.noteLabel.text = obj?.note
                    cell?.showingTimeLabel.text = DXUtility.returnChatRequiredTime(timeString: (obj?.date)!)
                    if let url = obj?.url{
                    cell?.urls = url
                    }
                    cell?.refresh()
                    return cell!
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "addNewButtonCell", for: indexPath) as? AddNewButtonCell
                    cell?.healthRecordType = .history
                    cell?.delegate = self
                    return cell!
                }
            }else{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NoResultFoundCell", for: indexPath) as? NoResultFoundCell
                    cell?.messageLabel.text = "No Record Found"
                    return cell!
                }else if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "addNewButtonCell", for: indexPath) as? AddNewButtonCell
                    cell?.healthRecordType = .history
                    cell?.delegate = self
                    return cell!
                }
            }
        }else if indexPath.section == 1{
        let cell = tableView.dequeueReusableCell(withIdentifier: "diagnosticRecords", for: indexPath) as? DiagnosticCell
            if let healthRecord  = self.healthRecord{
            cell?.diagnosticReportArray = (healthRecord.diagnostic)
            }
            cell?.delegate = self
            cell?.deleteDelegate = self
            cell?.refresh()
            return cell!
        }else if indexPath.section == 2{
            if (healthRecord?.medication.count)! > 0 {
            if (healthRecord?.medication.count)!-1 >= indexPath.row{
            let obj = healthRecord?.medication[indexPath.row]
             let cell = tableView.dequeueReusableCell(withIdentifier: "medicationCourse", for: indexPath) as? MedicationCourseCell
            cell?.delegate = self
            cell?.medicineNameLabel.text = obj?.content
            cell?.dleteButton.tag = indexPath.row
                    return cell!
                    
                }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "addNewButtonCell", for: indexPath) as? AddNewButtonCell
                cell?.healthRecordType = .medication
                cell?.delegate = self
                return cell!
            }
            }else{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NoResultFoundCell", for: indexPath) as? NoResultFoundCell
                    cell?.messageLabel.text = "No Record Found"
                    return cell!
                }else if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "addNewButtonCell", for: indexPath) as? AddNewButtonCell
                    cell?.healthRecordType = .medication
                    cell?.delegate = self
                    return cell!
                }
            }
        }else if indexPath.section == 3{
            if (healthRecord?.intolerance.count)! > 0 {
                if (healthRecord?.intolerance.count)!-1 >= indexPath.row{
           let obj = healthRecord?.intolerance[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "intorlerance", for: indexPath) as? IntorleranceCell
                cell?.delegate = self
           cell?.medicineNameLabel.text = obj?.content
            cell?.dleteButton.tag = indexPath.row
            cell?.noteLabel.text = obj?.note
            return cell!
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "addNewButtonCell", for: indexPath) as? AddNewButtonCell
                    cell?.healthRecordType = .intolerance
                    cell?.delegate = self
                    return cell!
                }
            }else{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NoResultFoundCell", for: indexPath) as? NoResultFoundCell
                    cell?.messageLabel.text = "No Record Found"
                    return cell!
                }else if indexPath.row == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "addNewButtonCell", for: indexPath) as? AddNewButtonCell
                    cell?.healthRecordType = .intolerance
                    cell?.delegate = self
                    return cell!
                }
            }
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
        if (healthRecord?.medicalHisory.count)! - 1 >= indexPath.row{
        let obj = healthRecord?.medicalHisory[indexPath.row]
        if let url = obj?.url{
            if url.count > 0{
                 return UITableViewAutomaticDimension
            }else{
              return 60
            }
        }else{
          return 60
        }
        }else{
        return UITableViewAutomaticDimension
        }
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView{
            let sectionview = UINib(nibName: "DPDoctorProfileSectionHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? DoctorProfileSectionView
            if let requiredView = sectionview{
                requiredView.sectionTitle.text = helathSectionArray[section]
                return requiredView
            }
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
extension DPHealthTabBarController : UIImagePickerControllerDelegate,UINavigationControllerDelegate,DiagNosticCellDelegate{
    func uploadImage(image : UIImage){
        FirebaseController.uploadPhoto(image: image,completionHandler: {(error,response) in
            if error == nil,let data = response{
                if  let url = data.downloadURL()?.absoluteString{
                    self.createHealthRecords(imageUrl: [["url":url,"urlType":"image"]])
                }
            }else{
                
            }
        })
    }
    func createHealthRecords(imageUrl : [[String : String]]){
        if let userId = DPWebServicesManager.sharedManager.user?.userId{
            let params : [String : AnyObject] = ["patientId":userId as AnyObject,"relativesId" : relativeId as AnyObject,"tag" : "report" as AnyObject,"url" : imageUrl as AnyObject]
            self.createRecord(params: params, recordType: .report)
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
        self.uploadImage(image: editedImage)
        }
        else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
          self.uploadImage(image: image)
        }
        picker.dismiss(animated: true, completion:nil)
}
    func openIMagePicker(completationHandler: @escaping (UIImage) -> Void) {
       //openRespectiveAttachmentType()
        openImageAndDocumentsPicker(withDocuments: true)
    }
    func openRespectiveAttachmentType(with sourceType : UIImagePickerControllerSourceType = .photoLibrary,documents : Bool = false){
        if !documents{
            let imagePickerController = UIImagePickerController()
            imagePickerController.navigationBar.tintColor = UIColor().greenDoxtroCgColor()
            imagePickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePickerController.allowsEditing = true
            if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                imagePickerController.sourceType = sourceType
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }else{
            let importMenu = UIDocumentMenuViewController(documentTypes: ["public.composite-content"], in: .import)
            importMenu.delegate = self as? UIDocumentMenuDelegate
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion:{(
                //debugPrint("ayuhs")
                )})
        }
    }
}
extension DPHealthTabBarController : MedicineCourseDelegate,AddNewMedicineDelegate,AddRecordViewDelegate{
    func deleteMedicatonCourse(row: Int,healthRecordType: HealthRecordType) {
        if healthRecordType == .medication{
        let obj = healthRecord?.medication[row]
        if let _id = obj?._id{
            deleteRecordWith(id: _id)
            }
            
        }else if healthRecordType == .intolerance{
            let obj = healthRecord?.intolerance[row]
            if let _id = obj?._id{
                deleteRecordWith(id: _id)
            }
        }else if healthRecordType == .history{
         let obj = healthRecord?.medicalHisory[row]
            if let _id = obj?._id{
                deleteRecordWith(id: _id)
            }
        }else if healthRecordType == .report{
            let obj = healthRecord?.diagnostic[row]
            if let _id = obj?._id{
                deleteRecordWith(id: _id)
            }
        }
    }
    func deleteRecordWith(id : String){
        let params = ["_id":id]
        let request = DeleteHealthRecordRequest(params: params as [String : AnyObject])
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            if error == nil,let response = response {
                if let data = response["data"].array{
                    if let obj = data.first?.dictionary{
                            if let relativeID = self.relativeId{
                            self.fetchDiagnosticRecord(relativeId:relativeID )
                            }
                    }
                }
            }else{
                
            }
            
        }
    }
    func saveMedicationInfo(healthRecordType: HealthRecordType, medicineTextField: UITextField, contentTextField: UITextField) {
        //debugPrint(healthRecordType)
        if medicineTextField.text != ""{
            if let userId = DPWebServicesManager.sharedManager.user?.userId{
                var params  = ["patientId":userId ,"relativesId" : relativeId,"tag" : healthRecordType.rawValue,"content" : medicineTextField.text!]
                if healthRecordType == .intolerance{
                    params["note"] = contentTextField.text!
                }
                createRecord(params: params  as [String : AnyObject], recordType: healthRecordType)
            }
        }else{
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "ERROR", subtitle: "Enter Medicine Name", type: .error)
        }
        
    }
//    func createRecord(params :[String : AnyObject],recordType : HealthRecordType){
//        let request = CreateHelathRecordRequest(params: params as [String : AnyObject])
//        ABProgressIndicator.shared.showAnimator()
//        DPWebServicesManager.getResponse(with: request, completionHandler: { (resposne, error) in
//            ABProgressIndicator.shared.hideAnimator()
//            if error == nil ,let response = resposne {
//                if let data = response["data"].array{
//                    if let relativeID = self.relativeId{
//                    self.fetchDiagnosticRecord(relativeId: relativeID)
//                    }
//                }
//            }
//        })
//         self.removeView()
//    }
    func createRecord(params :[String : AnyObject],recordType : HealthRecordType){
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.alamofireRequest(url: kWebServiceBase+"/healthRecords/create", httpMethod: "POST", details:params as [String : AnyObject]) { (resposne, error) in
            ABProgressIndicator.shared.hideAnimator()
            if error == nil ,let response = resposne {
                if let data = response["data"].array{
                    if let relativeID = self.relativeId{
                        self.fetchDiagnosticRecord(relativeId: relativeID)
                    }
                }
            }
        }
         self.removeView()
    }
    func openAddRecordView(healthRecordType: HealthRecordType) {
        if healthRecordType == .history{
            self.performSegue(withIdentifier: "showAddMedicalHisory", sender: nil)
        }else {
        showAddNewMemeberView(with: healthRecordType)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddMedicalHisory"{
            if let controller = segue.destination as? AddMedicineController{
                controller.relativesId = self.relativeId
            }
        }
    }
    func addMedicationCourse(healthRecordType: HealthRecordType) {
        showAddNewMemeberView(with: healthRecordType)
    }
    func removeView() {
        for memeberView in (self.view.subviews){
            if memeberView is TakeMedicineView{
                memeberView.removeFromSuperview()
            }
        }
    }
    func showAddNewMemeberView(with healthRecordType : HealthRecordType) {
        let view =  Bundle.main.loadNibNamed("DPTakeMedicineView", owner: nil, options: nil)
        if let mainView = view?[0] as? TakeMedicineView{
            mainView.healthRecordType = healthRecordType
            mainView.delegate = self
            mainView.titleLabel.text = "Add Current Medicine"
            if healthRecordType == .medication{
            mainView.disesaseCauseTextField.isHidden = true
            }else if healthRecordType == .intolerance{
                mainView.disesaseCauseTextField.isHidden = false
            }
            mainView.frame = (self.view.bounds)
            self.view.addSubview(mainView)
        }
        
   }
}
enum DPTab: Int {
    case home, Consulatation, healthFeed, health, settings
}
extension UIViewController {
    
    class func indexForTab(_ tab: DPTab) -> Int {
        switch tab {
        case .home:
            return 0
        case .Consulatation:
            return 1
        case .healthFeed :
            return 3
        case .health:
            return 2
        case .settings:
            return 4
        }
    }
    
    class func controllerWithSwitchTab(_ tab: DPTab, pop: Bool, animated: Bool) -> UIViewController? {
        switchToTab(tab, pop: pop, animated: animated)
        return rootcontrollerForTab(tab)
    }
    
    class func switchToTab(_ tab: DPTab, pop: Bool, animated: Bool) {
        let selectedIndex = selectedTab()
        let _ = switchToExpertTab(tab)
        if let selectedIndex = selectedIndex, let tab = DPTab(rawValue: selectedIndex), pop {
            let _ = navigationControllerForTab(tab)?.popToRootViewController(animated: animated)
        }
    }
    
    class func selectedTab() -> Int? {
        return tabViewController()?.selectedIndex
    }
    
    class func switchToExpertTab(_ tab: DPTab) -> Bool {
        return switchToIndex(indexForTab(tab))
    }
    
    class func rootcontrollerForTab(_ tab: DPTab) -> UIViewController? {
        return navigationControllerForTab(tab)?.viewControllers.first
    }
    
    class func navigationControllerForTab(_ tab: DPTab) -> UINavigationController? {
        return controllerForIndex(indexForTab(tab)) as? UINavigationController
    }
    
    class func tabViewController() -> UITabBarController? {
        return UIApplication.getRootViewController() as? UITabBarController
    }
    
    class func controllerForIndex(_ index: Int) -> UIViewController? {
        if let rootViewController = tabViewController() ,let controllers = rootViewController.viewControllers, controllers.count > index {
            return controllers[index]
        } else {
            return nil
        }
    }
    
    class func switchToIndex(_ index: Int) -> Bool {
        if let rootViewController = UIApplication.getRootViewController() as? UITabBarController, let controller = controllerForIndex(index) {
            rootViewController.selectedViewController = controller
            return true
        } else {
            return false
        }
    }
}
extension UIApplication{
    class func getRootViewController() -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
}
extension DPHealthTabBarController : UIDocumentMenuDelegate,UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        
        let cico = url as URL
        uploadDocuments(fileUrl: cico,fileName: cico.lastPathComponent)
        
        //optional, case PDF -> render
        //displayPDFweb.loadRequest(NSURLRequest(url: cico) as URLRequest)
        
        
        
        
    }
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        
        
        dismiss(animated: true, completion: nil)
        
        
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    //    func opennPDf()
    //    {
    //        self.documentInteractionController = UIDocumentInteractionController(url: URL(string: "file:///private/var/mobile/Containers/Data/Application/75585354-9BA3-4C73-9A15-25DE4BBECEDE/tmp/com.doxtro.app-Inbox/iOSTechOverview.pdf")!)
    //        self.documentInteractionController?.delegate = self
    //        self.documentInteractionController?.presentPreview(animated: true)
    //    }
    func  uploadDocuments(fileUrl : URL,fileName : String){
        ABProgressIndicator.shared.showAnimator()
        // Create a reference to the file you want to upload
        let docRef = Storage.storage().reference().child("chat_documents/").child((DPWebServicesManager.sharedManager.user?.userId)! + "user.png" + "\(Date())")
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = docRef.putFile(from: fileUrl, metadata: nil) { metadata, error in
            ABProgressIndicator.shared.hideAnimator()
            if let error = error {
                
            } else{
                // Metadata contains file metadata such as size, content-type, and download URL.
                if let downloadURL = metadata!.downloadURL()?.absoluteString{
                   self.createHealthRecords(imageUrl:[["url":downloadURL,"urlType":"documents","documentName":fileName]])
                }
            }
        }
        
    }
}
