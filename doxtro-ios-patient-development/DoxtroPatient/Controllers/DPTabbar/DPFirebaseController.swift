//
//  DPFirebaseController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
class FirebaseController{
    static let storageRef: StorageReference = Storage.storage().reference().child("chat_documents/").child((DPWebServicesManager.sharedManager.user?.userId)! + "user.png" + "\(Date())")
    class func uploadPhoto(image: UIImage,completionHandler: @escaping(_ error : Error?,_ response : StorageMetadata?)->Void) {
        let resizeImage = DXUtility.shared.resizeImage(image: image, newWidth: 400)
        if let uploadData = UIImagePNGRepresentation(resizeImage) {
            ABProgressIndicator.shared.showAnimator()
           FirebaseController.storageRef.putData(uploadData, metadata: nil, completion: { (metaData, error) in
                if error == nil {
                    if let user = DPWebServicesManager.sharedManager.user{
                        ABProgressIndicator.shared.hideAnimator()
                        completionHandler(nil,metaData)
                        //self.send(with: (metaData?.downloadURL()?.absoluteString)!,msgType: "image")
                        //self.updateUserProfile(with: ["profilePic": metaData?.downloadURL()?.absoluteString as AnyObject,"_id": user.userId as AnyObject])
                    }else{
                        ABProgressIndicator.shared.hideAnimator()
                    }
                }else{
                    ABProgressIndicator.shared.hideAnimator()
                    completionHandler(error,nil)
                }
            })
        }
}
    class func deleteFilesUsingReference(downloadUrl : String){
         let desertRef: StorageReference = Storage.storage().reference(forURL:downloadUrl)
        // Delete the file
        desertRef.delete { (error) in
           // debugPrint(error)
        }
    }
}
