//
//  DPOtherUtilityCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 02/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
protocol AddRecordViewDelegate {
    func openAddRecordView(healthRecordType : HealthRecordType)
}
class NoResultFoundCell : UITableViewCell{
    @IBOutlet weak var messageLabel: UILabel!
}
class AddNewButtonCell : UITableViewCell{
    var delegate : AddRecordViewDelegate?
    var healthRecordType : HealthRecordType?
    @IBAction func addNewButtonClicked(_ sender: Any) {
        if let healthRecordType = self.healthRecordType{
        self.delegate?.openAddRecordView(healthRecordType : healthRecordType)
        }
    }
    @IBOutlet weak var addNewButton: UIButton!
}
