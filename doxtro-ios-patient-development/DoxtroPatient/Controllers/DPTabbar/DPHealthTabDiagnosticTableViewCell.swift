//
//  DPHealthTabDiagnosticTableViewCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 30/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
import SwiftyJSON
protocol DiagNosticCellDelegate {
    func openIMagePicker(completationHandler : @escaping(_ value : UIImage)-> Void)
}
class DiagnosticCell : UITableViewCell{
    var relativeId : String = "59cb74cb3e35b324012c8f43"
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate : DiagNosticCellDelegate?
    var deleteDelegate : MedicineCourseDelegate?
    var diagnosticReportArray : [HealthModel] = []
    func refresh(){
        if self.collectionView != nil{
        self.collectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    @IBAction func addImage(_ sender: Any) {
        self.delegate?.openIMagePicker(completationHandler: { (value) in
            
        })
    }
}
extension DiagnosticCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reportObj = self.diagnosticReportArray[indexPath.row]
        if let url = reportObj.url.first{
        if url.urlType == "image"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "diagnosticCellImage", for: indexPath) as? DiagnosticCollectionViewCell
            if let contentUrl =  url.url{
                cell?.reportImageView.af_setImage(withURL: contentUrl, placeholderImage: UIImage(named: "imagePlaceholder"))
                cell?.deleteButton.tag = indexPath.row
                cell?.delegate = self
            }
            return cell!
        }
    }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "medicalHistoryDocumentsCells", for: indexPath) as? DiagnosticReportDocumentCell
        if let url = reportObj.url.first{
        cell?.fileName.text = url.documentName
        }
        cell?.deleteButton.tag = indexPath.row
        cell?.delegate = self
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.diagnosticReportArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/3, height: collectionView.bounds.height)
    }
}
extension DiagnosticCell: DiagnosticImageDeleteDelegate{
    func deleteImageWith(row : Int) {
        self.deleteDelegate?.deleteMedicatonCourse(row: row, healthRecordType: .report)
    }
}
protocol DiagnosticImageDeleteDelegate {
    func deleteImageWith(row : Int)
    
}
class DiagnosticCollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var reportImageView: UIImageView!
    var delegate : DiagnosticImageDeleteDelegate?
    @IBAction func deleteButtonClicked(_ sender: Any) {
 UIAlertController.showAlertWithMessage("STR_DELETE_ITEM_MSG".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
            
        }, actionButtonHandler: { (_ , _) in
            if let button = sender as? UIButton{
                self.delegate?.deleteImageWith(row : button.tag)
            }
        })
    }
    
    @IBOutlet weak var deleteButton: UIButton!
}

class DiagnosticReportDocumentCell : UICollectionViewCell{
    
    @IBOutlet weak var fileName: UILabel!
    var delegate : DiagnosticImageDeleteDelegate?
    @IBAction func deleteButtonClicked(_ sender: Any) {
        UIAlertController.showAlertWithMessage("STR_DELETE_ITEM_MSG".localized, title: "", destructiveButtonTitle: "No", actionButtonTitle: "Yes", destructiveButtonHandler: { (_ , _) in
            
        }, actionButtonHandler: { (_ , _) in
            if let button = sender as? UIButton{
                self.delegate?.deleteImageWith(row : button.tag)
            }
        })
    }
    
    @IBOutlet weak var deleteButton: UIButton!
}

