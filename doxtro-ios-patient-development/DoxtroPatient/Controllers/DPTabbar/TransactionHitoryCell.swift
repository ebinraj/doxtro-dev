//
//  TransactionHitoryCell.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 16/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class TransactionHistoryTableViewCell : UITableViewCell{
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profileImageView.makeCircleandBorder(with: UIColor().greenDoxtroCgColor().cgColor, width: 2.0)
    }
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var transactionType: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
}
