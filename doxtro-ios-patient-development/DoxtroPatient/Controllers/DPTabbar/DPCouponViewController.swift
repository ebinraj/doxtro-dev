//
//  DPCouponViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 11/09/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
import SwiftyJSON
import NPSegmentedControl
import Alamofire
enum HttpType : Int{
    case Google
    case custom
}
class CouponViewController : UIViewController{
    @IBOutlet weak var fotterTitleLabel: UILabel!
    var consultationID : String?
    var paybleAmount : String?
    var createdConsultationInfo : DpConsultationDetails?
    var ORDER_NUMBER : String = ""
    var couponInfo : CouponInfo?
    var couponPaymentData : PaymentData?
    @IBAction func removeCoupon(_ sender: Any) {
        //updateDefaultValue()
        if let paymenTdata = self.couponPaymentData{
            removeCoupon()
        }
    }
    @IBOutlet weak var doxtroCashLabel: UILabel!
    @IBAction func initateAddMoney(_ sender: Any) {
        let destinationVc = self.storyboard?.instantiateViewController(withIdentifier: "doxtroCashController") as! DoxtroCashVIewController
        destinationVc.consultationId = self.createdConsultationInfo?.consultationId
        if let paymentdata = self.couponPaymentData{
            destinationVc.doxtroCash = paymentdata.doxtroCash
        }
        destinationVc.completionHandler = {(doxtroCash,paymentData) in
            self.refreshDoxtroCAsh(doxtroCash: doxtroCash)
            self.couponPaymentData = paymentData
            self.updateDefaultValue()
        }
        self.present(destinationVc, animated: true, completion: nil)
    }
    @IBOutlet weak var usedDoxtroCash: UILabel!
    func refreshDoxtroCAsh(doxtroCash : Int ){
        self.doxtroCashLabel.text = String(describing:doxtroCash)
    }
    @IBOutlet weak var removeCouponBUtton: UIButton!
    @IBOutlet weak var couponNameLabel: UILabel!
    @IBOutlet weak var afterApplyCouponView: UIStackView!
    @IBOutlet weak var applyCouponButton: UIButton!
    @IBOutlet weak var paymentButton: UIButton!
    @IBOutlet weak var totalPaybleLabel: UILabel!
    @IBOutlet weak var couponDiscount: UILabel!
    //@IBOutlet weak var doxtroCash: UILabel!
    @IBOutlet weak var ConsultationFee: UILabel!
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var doctorSpecializationLabel: UILabel!
    var razorpay : Razorpay!
    var razorPayEventParams = [String: AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentButton.layer.cornerRadius = 4
        couponTextField.addBootomBorderLine(width: 1, color:(UIColor().grayDoxtroCgColor()?.cgColor)!)
        doctorImageView.makeCircleandBorder(with: (UIColor().grayDoxtroCgColor()?.cgColor)!, width: 2)
        if let category = createdConsultationInfo?.specializationCategory{
            self.doctorSpecializationLabel.text = category
            self.fotterTitleLabel.text = "Chat with " + category + " instantly"
        }
        updateDefaultValue()
        addEventsWhenViewLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initilizeRazorPay()
    }
    func initilizeRazorPay(){
        razorpay = Razorpay.initWithKey(RazorPayPayment.KeyID.LIVE, andDelegate: self)
        razorpay.setExternalWalletSelectionDelegate(self)
    }
    func removeCoupon(){
        if let couponInfo = self.couponPaymentData?.couponUsed,let consulatationID = self.createdConsultationInfo?.consultationId{
            let params = ["couponTag":couponInfo,"consultationId":consulatationID] as [String : AnyObject]
            let request = RemoveCouponRequest(params: params)
            ABProgressIndicator.shared.showAnimator()
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let response = response?.dictionary{
                    if let data = response["data"]?.dictionary{
                        let paymentData = PaymentData(dict: JSON(data))
                        self.switchToNocouponView(paymentData: paymentData)
                        self.couponPaymentData = paymentData
                        self.updateDefaultValue()
                    }
                }
            })
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let path = UIBezierPath(roundedRect:self.couponNameLabel.bounds,
                                byRoundingCorners:[.topLeft, .bottomLeft],
                                cornerRadii: CGSize(width: 20, height:  20))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.couponNameLabel.layer.mask = maskLayer
        let Buttonpath = UIBezierPath(roundedRect:self.removeCouponBUtton.bounds,
                                      byRoundingCorners:[.topRight, .bottomRight],
                                      cornerRadii: CGSize(width: 20, height:  20))
        let buttonmaskLayer = CAShapeLayer()
        buttonmaskLayer.path = Buttonpath.cgPath
        self.removeCouponBUtton.layer.mask = buttonmaskLayer
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC  = segue.destination as? PaymentWebViewController{
            if let dict = sender as? NSMutableURLRequest{
                destinationVC.request = dict
                destinationVC.completionHandler = {(value : String) in
                self.makePayments()
                }
                //destinationVC.navTitile = "AYush"
            }
        }
    }
}
//paytm Stuff
extension CouponViewController{
    func getCheckSum(completionHandler : @escaping(_ ecaprror : Error?,_ response : CheckSumPaytm?)-> Void){
        if let user = DPWebServicesManager.sharedManager.user{
            let  randomNumber = (arc4random() % 9999999) + 1;
            ORDER_NUMBER = String(format: "Doxtro_%d", randomNumber)
            let params = ["customerId":user.userId
                ,"amount":self.paybleAmount,
                 "email":user.emailId,
                 "mobileNumber":"8123632112",
                 "orderId" :ORDER_NUMBER,
                 "type" : "live"]
            let request = GetChecksumPaaytmRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if error == nil,let response = response{
                    if let dict = response.dictionary{
                        let checksumObject = CheckSumPaytm(dict: JSON(dict["data"]?.dictionary))
                        completionHandler(nil,checksumObject)
                    }
                }else{
                    completionHandler(error,nil)
                }
            })
        }
    }
    func makePaytmOrderWith(checkSumDetails : CheckSumPaytm){
        if let user = DPWebServicesManager.sharedManager.user{
            let orderDict = ["MID":checkSumDetails.MID,
                             "CHANNEL_ID":checkSumDetails.CHANNEL_ID,
                             "INDUSTRY_TYPE_ID":checkSumDetails.INDUSTRY_TYPE_ID,
                             "WEBSITE":checkSumDetails.WEBSITE,
                             "TXN_AMOUNT":checkSumDetails.TXN_AMOUNT,
                             "ORDER_ID":checkSumDetails.ORDER_ID,
                             "CALLBACK_URL":checkSumDetails.CALLBACK_URL,
                             "CHECKSUMHASH":checkSumDetails.CHECKSUMHASH,
                             "REQUEST_TYPE":checkSumDetails.REQUEST_TYPE,
                             "CUST_ID":checkSumDetails.CUST_ID,
                             "EMAIL":user.emailId,
                             "MOBILE_NO":"8123632112"]
            if let order = PGOrder(params: orderDict){
                intiatePaymentWithOrder(order: order)
            }
        }
    }
    
    func intiatePaymentWithOrder(order : PGOrder){
        
        let viewBar = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
        viewBar.backgroundColor = UIColor().greenDoxtroCgColor()
        let button = UIButton(frame: CGRect(x: 5, y: 24, width: 70, height: 40))
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.textColor = UIColor.black
        button.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14.0)
        let tileLabel = UILabel(frame: CGRect(x: self.view.frame.width/2 - 50, y: 20, width: 100, height: 50))
        tileLabel.text = "Doxtro"
        tileLabel.font = UIFont(name: "Helvetica-Bold", size: 18.0)
        tileLabel.textColor = UIColor.white
        tileLabel.textAlignment = NSTextAlignment.center
        viewBar.addSubview(tileLabel)
        // let imageVIew  = UIImageView(frame: CGRect(x: 0, y: 16, width: 70, height: 40))
        //        //add title
        //        UILabel *mTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 10, 1, 100, 50)];
        //        [mTitleLabel setText:@"Payment"];
        //        [mTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
        //        mTitleLabel.textColor = [UIColor whiteColor];
        //        [mNavBar addSubview:mTitleLabel];
        var merchantObj = PGMerchantConfiguration.default()
        if let transactionController = PGTransactionViewController.init(transactionFor: order){
            transactionController.topBar = viewBar
            transactionController.cancelButton = button
            transactionController.serverType = eServerTypeProduction
            transactionController.merchant = merchantObj
            transactionController.loggingEnabled = true
            transactionController.delegate = self
            self.present(transactionController, animated: true, completion: nil)
        }
        
    }
    
    func capturePayment(payTmResponse : String){
        if let dict = convertToDictionary(text: payTmResponse){
            let dicts = JSON(dict).rawString()?.replacingOccurrences(of: "\\", with:"")
            if let user = DPWebServicesManager.sharedManager.user{
                var param = ["consultationId": self.consultationID,
                             "customerId": user.userId,
                             "paytmResponse": dicts,
                             "type" : "live"] as [String : Any]
                if let couponInfo = self.couponInfo{
                    param["couponTag"] = couponInfo.couponTag
                    param["couponId"] = couponInfo.couponId
                }

                let request = PaytmPaymentCaptureRequest(params: param as [String : AnyObject])
                DPWebServicesManager.getResponse(with: request, completionHandler: { (Response, error) in
                    if error == nil,let response  = Response?.dictionary{
                        if let data = response["data"]?.dictionary{
                            let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                            consulattionInfoObj.patientFee = response["patientFee"]?.intValue
                            DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                            //self.updateCosultationListAndRecentOCnsultation()
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            if let message = response["message"]?.string{
                                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: message, type: .error)
                            }
                        }
                        if let data = response["additionalDetails"]?.dictionary{
                            let paymentInfo = PaymentInfo(dictionary: JSON(data))
                            if let status = paymentInfo.status{
                                self.showPaymenStatusMessage(status : status)
                            }else{
                                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Error to get Authorized" , type: .error)
                            }
                        }
                        self.paidFeesEventTrack(response: response)
                    }
                })
            }
        }
    }
    func convertToDictionary(text: String) -> Any? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
//paytm delegates
extension CouponViewController : PGTransactionDelegate{
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        debugPrint(responseString)
        capturePayment(payTmResponse:responseString )
        controller.dismissController()
    }
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        controller.dismissController()
    }
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        debugPrint(error.localizedDescription)
    }
}
extension CouponViewController{
    func applyCoupon(){
        if let user = DPWebServicesManager.sharedManager.user{
            ABProgressIndicator.shared.showAnimator()
            let parameters = [ "couponTag": self.couponTextField.text!,
                               "consultationId" : self.consultationID]
            let request = ApplyCouponRequest(params: parameters as [String : AnyObject] )
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                self.couponTextField.text = ""
                if error == nil ,let response = response?["data"].dictionary{
                    let couponInfo = CouponInfo(dict: JSON(response))
                    self.UpdateCouponInfoValue(with: couponInfo)
                    self.changeTextFieldAfterApplyCoupon(couponTag: couponInfo.couponTag)
                }else {
                    if let message = response?["message"].string{
                        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:message , type: .error)
                    }
                }
            }
        }
    }
    func changeTextFieldAfterApplyCoupon(couponTag : String){
        //self.couponTextField.isHidden = true
        self.afterApplyCouponView.isHidden = false
        self.couponNameLabel.clipsToBounds = true
        self.couponNameLabel.text = ("  " + couponTag + "    ")
        self.couponNameLabel.backgroundColor = UIColor().greenDoxtroCgColor()
        self.removeCouponBUtton.backgroundColor = UIColor().greenDoxtroCgColor()
    }
    func UpdateCouponInfoValue(with couponinfo : CouponInfo){
        self.couponInfo = couponinfo
        self.ConsultationFee.text = "₹\(String(describing: couponinfo.consultingFee))"
        self.couponDiscount.text = "- ₹\(String(describing: couponinfo.couponDiscount))"
        self.totalPaybleLabel.text = "₹\(String(describing: couponinfo.totalPayable))"
        self.doxtroCashLabel.text = "₹" + String(describing:  couponinfo.remainingDoxtroCash)
        self.usedDoxtroCash.text =   "₹" + String(describing:  couponinfo.usedDoxtroCash)
        if couponinfo.isFree{
            self.paymentButton.setTitle("START CONSULTATION", for: .normal)
        }else{
            self.paymentButton.setTitle("PAY ₹\(String(describing: couponinfo.totalPayable)) TO CONSULT", for: .normal)
        }
        self.paybleAmount = String(describing: couponinfo.totalPayable)
        self.couponDiscount.isHidden = false
       //self.applyCouponButton.isHidden = true
        trackEventAfterCouponDiscount()
    }
    func switchToNocouponView(paymentData : PaymentData){
        self.couponInfo = nil
        self.paybleAmount = String(describing: paymentData.consultingFee)
        self.couponDiscount.isHidden = true
        self.applyCouponButton.isHidden = false
        self.afterApplyCouponView.isHidden = true
        self.couponTextField.isHidden = false
        self.paymentButton.setTitle("PAY ₹\(paymentData.consultingFee) TO CONSULT", for: .normal)
        self.totalPaybleLabel.text = "₹" + ("\(paymentData.patientFee)")
        self.paybleAmount = String(describing: paymentData.patientFee)
        self.paymentButton.setTitle("PAY ₹\(String(describing: paymentData.patientFee)) TO CONSULT", for: .normal)
    }
    func updateDefaultValue(){
        if let paymentdata = self.couponPaymentData{
            self.paybleAmount = String(describing: paymentdata.patientFee)
            self.ConsultationFee.text = "₹" + ("\(paymentdata.consultingFee)")
            self.totalPaybleLabel.text = "₹" + ("\(paymentdata.patientFee)")
            self.couponDiscount.text =  "- ₹" + ("\(paymentdata.couponDiscount)")
            self.doxtroCashLabel.text = "₹" + ("\(paymentdata.remainingDoxtroCash)")
            if paymentdata.usedDoxtroCash <= 0 {
                self.usedDoxtroCash.text = " ₹" + ("\(paymentdata.usedDoxtroCash)")
            }else{
                self.usedDoxtroCash.text = "- ₹" + ("\(paymentdata.usedDoxtroCash)")
            }
            
            let isFree = paymentdata.isFree
            if  isFree{
                self.paymentButton.setTitle("START CONSULTATION", for: .normal)
            }else{
                self.paymentButton.setTitle("PAY ₹\(paymentdata.patientFee) TO CONSULT", for: .normal)
            }
            if let isPreApplied = paymentdata.isPreApplied{
                if isPreApplied{
                    self.couponDiscount.isHidden = false
                   // self.applyCouponButton.isHidden = true
                    self.afterApplyCouponView.isHidden = false
                    //self.couponTextField.isHidden = true
                    self.couponNameLabel.text = "  " + paymentdata.couponUsed! + "    "
                }else{
                    self.couponDiscount.isHidden = true
                    self.applyCouponButton.isHidden = false
                   // self.afterApplyCouponView.isHidden = true
                    self.couponTextField.isHidden = false
                }
            }
            
        }
    }
}
//MARK: IBACTION
extension CouponViewController {
    @IBAction func paymentButtonClicked(_ sender: Any) {
        if let amount = self.paybleAmount{
            if let intAmount = Int(amount){
                if intAmount <= 0 {
                    makeFreePayment()
                }else{
                    showPaymentForm()
                }
            }
        }
        razorPayEventParams["Fees Displayed"] = true as AnyObject
        razorPayEventParams["Pay consultation fee"] = self.paybleAmount as AnyObject
        
        DPAppEventLog.shared.logEventName(EVENT_PROCEED_TO_RAZORPAY, values: razorPayEventParams)
    }
    func showPaymenStatusMessage(status : RazorPayPaymentStatus){
        switch status {
        case .authorized:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Payment Initiated you will getback you refund" , type: .message)
            break
        case .captured:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Payment Success" , type: .success)
            break
        case .created:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Payment unsuccessfull" , type: .error)
            break
        case .refunded:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Message", subtitle:"Refund Initiated" , type: .message)
            break
        case .failed:
            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Payment unsuccessfull" , type: .error)
            break
        default:
            break
        }
    }
    @IBAction func applyCouponClicked(_ sender: Any) {
        applyCoupon()
    }
    @IBAction func removeButton(_ sender: Any) {
        
    }
    /*
     Customize segment control
     */
}
extension CouponViewController{
    func makePayments() {
        if let user = DPWebServicesManager.sharedManager.user{
            let params = ["consultationId":self.createdConsultationInfo?.consultationId,"firstName" : user.name]
            let request = MakePaymentRequest(params: params as [String : AnyObject] )
            ABProgressIndicator.shared.showAnimator()
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if error == nil,let response = response{
                    if let data = response["data"].dictionary{
                        let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                        consulattionInfoObj.patientFee = response["patientFee"].intValue
                        DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                        //self.updateCosultationListAndRecentOCnsultation()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            })
        }
    }
    //    func updateCosultationListAndRecentOCnsultation(){
    //        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "RefreshConsultationList")))
    //    }
}
extension CouponViewController : RazorpayPaymentCompletionProtocol,ExternalWalletSelectionProtocol{
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        if walletName == "paytm"{
            getCheckSum(completionHandler: { (error, response) in
                if let checkSumObj = response,error == nil{
                    self.makePaytmOrderWith(checkSumDetails: checkSumObj)
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Failed to intiate", type: .error)
                }
            })
        }
    }
    func onPaymentError(_ code: Int32, description str: String) {
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: str, type: .error)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        if let user = DPWebServicesManager.sharedManager.user,let consultationId = self.createdConsultationInfo?.consultationId{
            var params = ["consultationId" :consultationId ,"payment_id" :payment_id,"patientId" : user.userId ,"type":"live"]
            if let couponInfo = self.couponInfo{
                params["couponTag"] = couponInfo.couponTag
                params["couponId"] = couponInfo.couponId
            }
            let request = PayementCaptureRequest(params : params as [String : AnyObject])
            ABProgressIndicator.shared.showAnimator()
            DPWebServicesManager.getResponse(with: request) { (response, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let response  = response?.dictionary{
                    if let data = response["data"]?.dictionary{
                        let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                        consulattionInfoObj.patientFee = response["patientFee"]?.intValue
                        DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                        //self.updateCosultationListAndRecentOCnsultation()
                        self.navigationController?.popViewController(animated: true)
                    }
                    if let data = response["additionalDetails"]?.dictionary{
                        let paymentInfo = PaymentInfo(dictionary: JSON(data))
                        if let status = paymentInfo.status{
                            self.showPaymenStatusMessage(status : status)
                        }else{
                            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Error to get Authorized" , type: .error)
                        }
                    }
                    self.paidFeesEventTrack(response: response)
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Error to get Authorized" , type: .error)
                }
            }
        }
    }
    
}
extension CouponViewController{
    func makeFreePayment(){
        if let consulattaionID = self.consultationID{
            var params : [String : AnyObject] = ["consultationId" : consulattaionID as AnyObject]
            if let couponInfo = self.couponInfo{
                params["couponTag"] = couponInfo.couponTag as AnyObject
                params["couponId"] = couponInfo.couponId as AnyObject
            }
            let request = StartFreeConsultationRequest(params: params)
            ABProgressIndicator.shared.showAnimator()
            DPWebServicesManager.getResponse(with: request) { (resposne, error) in
                ABProgressIndicator.shared.hideAnimator()
                if let response  = resposne?.dictionary{
                    if let data = response["data"]?.dictionary{
                        let consulattionInfoObj = DpConsultationDetails(dict: JSON(data))
                        consulattionInfoObj.patientFee = response["patientFee"]?.intValue
                        DPWebServicesManager.sharedManager.saveConsultationInfo(with: consulattionInfoObj)
                        //self.updateCosultationListAndRecentOCnsultation()
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        if let message = response["message"]?.string{
                            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: message, type: .error)
                        }
                    }
                    if let data = response["additionalDetails"]?.dictionary{
                        let paymentInfo = PaymentInfo(dictionary: JSON(data))
                        if let status = paymentInfo.status{
                            self.showPaymenStatusMessage(status : status)
                        }else{
                            TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Error to get Authorized" , type: .error)
                        }
                    }
                    self.paidFeesEventTrack(response: response)
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle:"Error to get Authorized" , type: .error)
                }
            }
        }
    }
}
extension CouponViewController {
    func trackEventAfterCouponDiscount() {
        //        DPAppEventLog.shared.logEventName(EVENT_PROCEED_TO_RAZORPAY, values: ["Discount Value" : self.couponDiscount.text as AnyObject])
        razorPayEventParams["Discount Value"] = self.couponDiscount.text as AnyObject
        // Fees Displayed
    }
    func addEventsWhenViewLoad() {
        razorPayEventParams["Category Name"] = doctorSpecializationLabel.text as AnyObject
        razorPayEventParams["Offer Code"] = couponTextField.text as AnyObject
        
        //        DPAppEventLog.shared.logEventName(EVENT_PROCEED_TO_RAZORPAY, values: ["Category Name" : doctorSpecializationLabel.text as AnyObject, "Offer Code" : couponTextField.text as AnyObject])
    }
    //MARK:23 rd nov 2017
    func paidFeesEventTrack(response: [String : JSON]) {
        var values = [String : AnyObject]()
        values["on click"] = true as AnyObject
        if let data = response["data"]?.dictionary {
            if let amount = data["amount"]?.int{
                values["af_revenue"] = (amount/100) as AnyObject
            }
            values["af_currency"] = "INR" as AnyObject
            values["Specialization name"] = data["specializationCategory"]?.string as AnyObject
        }
        values["Purchase Type"] = "Consultation" as AnyObject
        if let additionDetails = response["additionalDetails"]?.dictionary {
            values["Payment Mode"] = additionDetails["method"]?.string as AnyObject
            
            if let wallet = additionDetails["wallet"]?.string {
                if wallet != "" {
                    values["Using Doxtro Cash"] = true as AnyObject
                    values["Doxtro Cash Value"] = wallet as AnyObject
                }
                values["Balance in Doxtro Wallet"] = additionDetails["amount"]?.int as AnyObject
            }
            DPAppEventLog.shared.logEventName(EVENT_PAID_FEES, values: values)
            //Using Doxtro Cash
        }
    }
    func showPaymentForm() {
        let imageLogo = UIImage(named: "navigationlogo")
        var options = ["image":imageLogo,
                       "name" :"Doxtro",
                       "description": "Fee",
                       "theme" : ["color" : "#27A29C"],
                       "prefill" : [:],
                       "external" : ["wallets" : ["paytm"]]] as [String : Any]
        if var PrefillDictionary = options["prefill"] as? [String : String]{
            if let user = DPWebServicesManager.sharedManager.user{
                if let prefillEmail = user.emailId{
                    PrefillDictionary["email"] = prefillEmail
                }
                if let prefillMobileNumber = user.mobileNum{
                    PrefillDictionary["contact"] =  prefillMobileNumber
                }
                options["prefill"] = PrefillDictionary
            }
        }
        
        if let amount = self.paybleAmount{
            if let intAmount = Int(amount){
                options["amount"] = (intAmount * 100)
            }
            razorpay.open(options)
        }
    }
    
    /*
     {
     @"amount": @"1000", // mandatory, in paise//
     // all optional other than amount.
     @"image": @"https://url-to-image.png",//
     @"name": @"business or product name",//
     @"description": @"purchase description",//
     @"prefill" : @{//
     @"email": @"pranav@razorpay.com",
     @"contact": @"8879524924"
     },
     @"theme": @{//
     @"color": @"#F37254"
     }
     };
     */
}
// merchant_id=141395&order_id=ak@2017&redirect_url=http://52.70.206.226&cancel_url=http://52.70.206.226&language=EN&billing_name=Charli&billing_address=Room no 1101, near Railway station Ambad&billing_city=Indore&billing_state=MH&billing_zip=425001&billing_country=India&billing_tel=9595226054&billing_email=abc@test.com&delivery_name=Chaplin&delivery_address=room no.701 near bus stand&delivery_city=Hyderabad&delivery_state=Andhra&delivery_zip=425001&delivery_country=India&delivery_tel=9595226054&merchant_param1=additional Info.&merchant_param2=additional Info.&merchant_param3=additional Info.&merchant_param4=additional Info.&payment_option=OPTDBCRD&card_type=DBCRD&card_name=State Bank of India&data_accept=Y&enc_val=UnJQc1luH8V9bFFAksPay7YlNe5FVKZEqW4e7OodAf0h429KdWLU1jy9LB3qShL7vJwiHyWmx1n7ataRUUFNFL62rGpxqPTyiv6nnhdk9noTfLaDhPGT5vruUuliAPENbE7FtqwgwBro0zsUpyqdHR7RWbWbLvMz8DO8Ql%2BmMCJx08Ttd44%2Bzn2vWR2F6Ug6a6vSKl7Mtk83%2F7l6IwXS2UgZsp%2Bi7n5tn8AB%2FvcXoHlFNvlJll4xB7QJEhV%2BnAYKM1zXQh%2FoYi8lpuEtgVVpfe%2FH0qGYGvf0nH%2FU74kAT5RvSR6MTxm5BpC5sm0pLM2Oy0BrhaakswXFw%2FBxSwZ49w%3D%3D&issuing_bank=State Bank of India&access_code=AVWX73EI08BB77XWBB&mobile_no=9595226054&emi_plan_id=(null)&emi_tenure_id=(null)
/*extension DPPaymentController{
 func ApiCallForRSAKey(){
 let session = URLSession.shared
 do{
 
 stringRSAKey = stringRSAKey.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
 stringRSAKey = "-----BEGIN PUBLIC KEY-----\n\(stringRSAKey)\n-----END PUBLIC KEY-----" as NSString
 let stringRequestParam = "amount=\(1)&currency=\(stringCurrencyValue)"
 //                    let stringRequestParam = "amount=\(stringAmount_Pay)&currency=\(stringCurrencyValue)"
 let ccTool = CCTool()
 var stringEncryptValue :NSString = ccTool.encryptRSA(stringRequestParam, key: (stringRSAKey as NSString) as String!) as NSString
 let regExpStr : String = "!*'();:@&=+$,/?%#[]"
 stringEncryptValue = CFURLCreateStringByAddingPercentEscapes(nil, stringEncryptValue as CFString, nil, (regExpStr as CFString), CFStringBuiltInEncodings.UTF8.rawValue) as String as String as NSString
 let stringEncryptedValue = "access_code="+"\(stringAccessCodePayment)"+"&merchant_id="+"\(stringMerchantID)"+"&order_id="+"\(stringGloabalCart_ID)"+"&redirect_url="+"\(urlRedirectURL)"+"&cancel_url="+"\(urlCancelTransactionURL)"+"&enc_val="+"\(stringEncryptValue)"+"&billing_name="+"\(stringPersonName_Order)"+"&billing_address="+"\(self.stringResultAddress)"+"&billing_tel="+"\(stringContactNo_shipping)"+"&billing_city="+"\(dictionaryGloabalAddress["city"]!)"+"&billing_state="+"\(dictionaryGloabalAddress["state"]!)"+"&billing_zip="+"\(dictionaryGloabalAddress["zipcode"]!)"+"&billing_country="+"\(dictionaryGloabalAddress["country"]!)"+"&billing_email="+"\(userDictionary["email"]!)"
 print(stringEncryptedValue)
 let myRequestData = NSData.init(bytes: stringEncryptedValue.cString(using: .utf8), length: stringEncryptedValue.characters.count) as NSData
 let request = NSMutableURLRequest(url: NSURL(string: urlTrasnsactionURL) as! URL) as NSMutableURLRequest
 request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
 request.setValue(urlTrasnsactionURL, forHTTPHeaderField: "Referer")
 request.httpMethod = "POST"
 request.httpBody = myRequestData as Data
 
 DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
 DispatchQueue.main.async(execute: {
 self.webviewForPayment.loadRequest((request as NSMutableURLRequest) as URLRequest)
 })
 })
 
 }
 }else{
 
 }
 
 
 }catch{
 
 }
 
 }
 }
 func goForMakePayment(){
 let paramsString  = "amount=100.00&currency=INR&card_number=\(self.cardNumberTextField.text)&expiry_month=\(self.cardExpiryTextField.text)&expiry_year=21&cvv_number=\(self.cardCvvTextField.text)&customer_identifier=\(self.cardHolderNameTextField.text)"
 if let rsaInfo = self.rsaInfo{
 if let strValue  = rsaInfo.key{
 //                var requredKey : String = ""
 //           strValue.enumerateLines(invoking: { (strValue,  bools : inout Bool) in
 //            requredKey.append(strValue)
 //           })
 let addedKey = "-----BEGIN PUBLIC KEY-----\n\(strValue)\n-----END PUBLIC KEY-----\n"
 let cctoolObj = CCTool()
 var encryptValue = cctoolObj.encryptRSA(paramsString, key: addedKey)
 encryptValue = CFBridgingRetain(CFURLCreateStringByAddingPercentEscapes(nil, encryptValue! as CFString, "!*'();:@&=+$,/?%#[]" as CFString, nil,CFStringEncoding(String.Encoding.utf8.rawValue))) as? String
 if let redirect = self.rsaInfo?.redirectUrl?.absoluteString,let cancel = self.rsaInfo?.cancelUrl?.absoluteString{
 let encryptStr = "enc_val=\(encryptValue!)&merchant_id=141395&order_id=\(ORDER_NUMBER)&redirect_url=\(redirect)&cancel_url=\(cancel)&language=EN&payment_option=OPTDBCRD&card_type=DBCRD&card_name=master&data_accept=Y&issuing_bank=citibank&access_code=AVWX73EI08BB77XWBB,payOptDesc=Debit Card"
 
 let urlString = "https://secure.ccavenue.com/transaction/initTrans"
 let myencryptStrData = encryptStr.data(using: .utf8, allowLossyConversion: true)
 let mutablerequest = NSMutableURLRequest(url: URL(string: urlString)!)
 mutablerequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
 mutablerequest.setValue(urlString, forHTTPHeaderField: "Referer")
 mutablerequest.httpMethod = "POST"
 mutablerequest.httpBody = myencryptStrData
 self.performSegue(withIdentifier: "showWebView", sender: mutablerequest)
 }
 }
 
 }
 }
 
 NSString *myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@",amount,currency];
 CCTool *ccTool = [[CCTool alloc] init];
 NSString *encVal = [ccTool encryptRSA:myRequestString key:rsaKey];
 encVal = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
 (CFStringRef)encVal,
 NULL,
 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
 kCFStringEncodingUTF8 ));
 
 //Preparing for a webview call
 NSString *urlAsString = [NSString stringWithFormat:@"https://secure.ccavenue.com/transaction/initTrans"];
 NSString *encryptedStr = [NSString stringWithFormat:@"merchant_id=%@&order_id=%@&redirect_url=%@&cancel_url=%@&enc_val=%@&access_code=%@",merchantId,orderId,redirectUrl,cancelUrl,encVal,accessCode];
 
 NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
 NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
 [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
 [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
 [request setHTTPMethod: @"POST"];
 [request setHTTPBody: myRequestData];
 [_viewWeb loadRequest:request];
 
 func makePayment(){
 let paramsForEncrypting  = "amount=1.00&currency=INR"
 if let rsaInfo = self.rsaInfo{
 if let strValue  = rsaInfo.key{
 var stringRSAKey = strValue.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
 stringRSAKey = "-----BEGIN PUBLIC KEY-----\n\(stringRSAKey)\n-----END PUBLIC KEY-----" as NSString
 let cctoolObj = CCTool()
 
 var encryptValue = cctoolObj.encryptRSA(paramsForEncrypting, key:(stringRSAKey as NSString) as String!) as NSString
 encryptValue = CFURLCreateStringByAddingPercentEscapes(nil, encryptValue as CFString, nil, ("!*'();:@&=+$,/?%#[]" as CFString), CFStringBuiltInEncodings.UTF8.rawValue) as String as NSString
 if let redirect = self.rsaInfo?.redirectUrl?.absoluteString,let cancel = self.rsaInfo?.cancelUrl?.absoluteString{
 let encryptStr = "enc_val=\(encryptValue)&merchant_id=\(CCAvenue.Credential.MERCHANT_ID)&order_id=\(ORDER_NUMBER)&redirect_url=\(redirect)&cancel_url=\(cancel)&access_code=\(CCAvenue.Credential.ACCESS_CODE)&mobile_no=8123632112&billing_name=Ayush&billing_address=E-11,shayam colony&billing_city=Bangalore&billing_state=KA&billing_zip=425001&billing_country=India&billing_tel=9595226054&billing_email=abc@test.com&delivery_name=Ayush&delivery_address=room no.701 near bus stand&delivery_city=Hyderabad&delivery_state=Andhra&delivery_zip=425001&delivery_country=India&delivery_tel=9595226054"
 
 let urlString = CCAvenue.URLS.LIVE + CCAvenue.Credential.TRANSACTIO_PATH
 let myencryptStrData = Data(bytes: encryptStr.cString(using: .utf8)!, count: encryptStr.length)
 let mutablerequest = NSMutableURLRequest(url: URL(string: urlString)!)
 mutablerequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
 mutablerequest.setValue(urlString, forHTTPHeaderField: "Referer")
 mutablerequest.httpMethod = "POST"
 mutablerequest.httpBody = myencryptStrData
 self.performSegue(withIdentifier: "showWebView", sender: mutablerequest)
 }
 }
 }
 }
 func getRSA(){
 if let consultationId = consultationID{
 ABProgressIndicator.shared.showAnimator()
 var  randomNumber = (arc4random() % 9999999) + 1;
 ORDER_NUMBER = String(format: "Doxtro_%d", randomNumber)
 DPPaymentController.getRsaKey(orderID: ORDER_NUMBER, consultationId: consultationId) { (response, error) in
 ABProgressIndicator.shared.hideAnimator()
 if let response = response{
 if let data = response["data"].dictionary{
 debugPrint(response)
 self.rsaInfo = RSAInfo(dict: JSON(data))
 }else{
 TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Failed to get reference Please try in some time", type: .error)
 }
 }else if let error = error{
 TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Unable to proceed Please try in some time" + error.localizedDescription, type: .error)
 }
 }
 }else{
 TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Failed to get consultation reference", type: .error)
 }
 }
 func getPayMentOption(){
 let params = ["command":"getJsonDataVault","currency":"INR","amount":"100.00","access_code":CCAvenue.Credential.ACCESS_CODE,"customer_identifier":"ayush"]
 let request = GetPaymentOptions(params: params as [String : AnyObject])
 DPWebServicesManager.getResponse(with: request) { (response, error) in
 if let response = response{
 debugPrint(response)
 //let ayush = PaymentOption(dict: response)
 }
 }
 
 }
 */
class StackView: UIStackView {
    private var color: UIColor?
    override var backgroundColor: UIColor? {
        get { return color }
        set {
            color = newValue
            self.setNeedsLayout()
        }
    }
    
    private lazy var backgroundLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        self.layer.insertSublayer(layer, at: 0)
        return layer
    }()
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundLayer.path = UIBezierPath(rect: self.bounds).cgPath
        backgroundLayer.fillColor = self.backgroundColor?.cgColor
    }
}
