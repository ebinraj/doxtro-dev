//
//  DPShareActivity.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 07/11/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit

protocol JYSystemShareDelegate : NSObjectProtocol {
    
    func systemShareSuccess(_ uuid: String)
}

class JYSystemShare: NSObject {
    
    weak var delegate:JYSystemShareDelegate?
    func shareActionViewControllerWithItems(_ shareSubject:String?,
                                            shareContent:String?,
                                            shareURL:String?,
                                            shareUUID: String?) -> UIActivityViewController? {
        var shareSubject = shareSubject
        if let shareSubj:String = shareSubject {
            shareSubject = "Doxtro: " + shareSubj
        } else {
            shareSubject = "Doxtro"
        }
        let shareText:String = shareContent ?? ""
        let shareURLString:String = shareURL ?? ""
        let uuid: String = shareUUID ?? ""
        if let shareURL:URL = URL(string: shareURLString)
        {
            let activityVC:UIActivityViewController = UIActivityViewController(activityItems: [shareText, shareURL], applicationActivities: nil)
            activityVC.setValue(shareSubject, forKey: "subject")
            activityVC.excludedActivityTypes =  [
                UIActivityType.airDrop,
                UIActivityType.assignToContact,
                UIActivityType.saveToCameraRoll,
                UIActivityType.addToReadingList,
                UIActivityType.postToFlickr,
                UIActivityType.postToVimeo,
                UIActivityType.copyToPasteboard
            ]
            
            activityVC.completionWithItemsHandler = {
                (activityType, completed, returnedItems, err) in
                if !completed {
                   // debugPrint("cancelled")
                    return
                }
                self.delegate?.systemShareSuccess(uuid)
                
                if activityType == UIActivityType.postToFacebook {
                    //debugPrint("posted to fb")
                   
                }
                
                if activityType == UIActivityType.postToTwitter {
                    //debugPrint("posted to twitter")
                   
                }
                
                if activityType == UIActivityType.mail {
                   // debugPrint("mail sent!")
                   
                }
                
//                //alert message on success:
//                let alertTitle = JYCopyStrings.str("STR_APP_TITLE")
//                let message = JYCopyStrings.str("STR_SHARE_THANK")
//                let alertView = UIAlertView(title: alertTitle, message: message, delegate: nil, cancelButtonTitle: JYCopyStrings.str("STR_OK"))
//                alertView.show()
            }
            
            
            return activityVC
        }
        
        return nil
    }
    
    
    class func socialShareTrigger(_ shareURL: String) {
        let shareVC:JYSystemShare = JYSystemShare()
        if let shareActivityVC:UIActivityViewController =
            shareVC.shareActionViewControllerWithItems("",
                                                       shareContent: "",
                                                       shareURL: shareURL,
                                                       shareUUID: nil) {
            if let controller = UIApplication.getTopViewController() {
                controller.present(shareActivityVC,
                                   animated: true,
                                   completion: { () -> Void in
                                    debugPrint("share completed..")
                })
            }
        }
    }
    
}

