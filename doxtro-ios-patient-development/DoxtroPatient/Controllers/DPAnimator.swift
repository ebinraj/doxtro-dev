//
//  DPAnimator.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 02/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit

//MARK: UIViewController extension

/**
 Extend UIViewController to dismiss view controller easily.
 */
extension UIViewController {
    @IBAction func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: PresentationAnimator public methods

/**
 This can used as custom transition animator base class, subclssing will reduce the work u need to do.
 
 */
class PresentationAnimator: NSObject {
    
    //Need to update this value, for right animation to execute.
    var isPresenting: Bool = true
    
    required override init() {
        super.init()
    }
    
    /**
     Return a animator object, even subclass can use this method to create its instance.
     */
    class func animator() -> Self {
        let animator = self.init()
        return animator
    }
    
    /**
     This will take all the responsibility of presenting new view controller, with custom animation.
     
     - Parameter controller: View controller to b presented
     - Parameter FromController: View controller from which you are presenting this view controller.
     
     - Note : Keep a strong reference to this animator, else dismiss animation won't happen as expected.
     */
    func presentController(_ controller: UIViewController, fromController: UIViewController, completionHandler: (() -> Void)? = nil) {
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        fromController.present(controller, animated: true, completion: completionHandler)
    }
}

//MARK: PresentationAnimator, UIViewControllerTransitioningDelegate methods

extension PresentationAnimator: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self;
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        return self
    }
}

//MARK: PresentationAnimator, UIViewControllerAnimatedTransitioning methods

extension PresentationAnimator: UIViewControllerAnimatedTransitioning{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toView = toViewController.view
        let fromView = fromViewController.view
        let containerView = transitionContext.containerView
        let duration = self .transitionDuration(using: transitionContext)
        
        animateFromController(fromViewController, toController: toViewController, fromView: fromView!, toView: toView!, containerView: containerView, duration: duration, transitionContext: transitionContext)
    }
}

//MARK: Custom methods

extension PresentationAnimator {
    
    /**
     This mathod will be called with all the required parameter to implement the animation.
     
     - Parameter fromController: Controller from new view controller is presented or controller which is dismissed.
     - Parameter toController: Presenting controller or controler which presented, current dismissing controller.
     - Parameter fromView: view of fromController
     - Parameter toView: view of toController
     - Parameter duration: duration for animation
     - Parameter transitionContext: Contect of animation.
     
     Note: Depending on the isPresenting falg status, you need to implement presentation or dismiss animation.
     */
    func animateFromController(_ fromController:UIViewController, toController: UIViewController, fromView: UIView, toView: UIView, containerView: UIView, duration: TimeInterval, transitionContext: UIViewControllerContextTransitioning) {
        
        if isPresenting {
            setupForAnimationFromController(fromController, toController: toController, fromView: fromView, toView: toView, containerView: containerView)
            
            setupBeforAnimationFromView(fromView, toView: toView)
            containerView.addSubview(toView)
            UIView.animate(withDuration: duration, animations: { [weak self] () -> Void in
                self?.setupForPresentationAnimationFromView(fromView, toView: toView)
                }, completion: { (success) -> Void in
                    transitionContext.completeTransition(success)
            })
            
        } else {
            UIView.animate(withDuration: duration, animations: { [weak self] () -> Void in
                self?.setupDismissAnimationFromView(fromView, toView: toView)
                }, completion: { [weak self] (success) -> Void in
                    fromView.removeFromSuperview()
                    transitionContext.completeTransition(success)
                    self?.cleanUpFromController(fromController, toController: toController, fromView: fromView, toView: toView, containerView: containerView)
            })
        }
    }
    
    /**
     Ui setup for presentation animation.
     */
    func setupForAnimationFromController(_ fromController:UIViewController, toController: UIViewController, fromView: UIView, toView: UIView, containerView: UIView) {
        
    }
    
    /**
     Initial setup before presntation animation.
     */
    func setupBeforAnimationFromView(_ fromView: UIView, toView: UIView) {
        toView.transform = CGAffineTransform(scaleX: 0, y: 0)
        toView.alpha = 0.0
    }
    
    /**
     Setup views for presentation animation
     */
    func setupForPresentationAnimationFromView(_ fromView: UIView, toView: UIView) {
        toView.transform = CGAffineTransform.identity
        toView.alpha = 1.0
    }
    
    /**
     Setup view for dismiss animation.
     */
    func setupDismissAnimationFromView(_ fromView: UIView, toView: UIView) {
        setupBeforAnimationFromView(toView, toView: fromView)
    }
    
    /**
     Clean up after dismiss animation completes.
     */
    func cleanUpFromController(_ fromController:UIViewController, toController: UIViewController, fromView: UIView, toView: UIView, containerView: UIView) {
        
    }
}

class ZoomToFullscreen: PresentationAnimator {
    
    var oldFrame: CGRect?
    var imageView: UIImageView?
    var backgroundView: UIView?
    weak var originalImage: UIImageView?
    
    func presentController(_ controller: UIViewController, fromController: UIViewController, fromImageView: UIImageView) {
        originalImage = fromImageView
        oldFrame = fromImageView.convert(fromImageView.bounds, to: nil)
        imageView = UIImageView(frame: oldFrame ?? CGRect.zero)
        imageView?.contentMode = .scaleAspectFit
        imageView?.image = fromImageView.image
        backgroundView = UIView(frame: oldFrame ?? CGRect.zero)
        backgroundView?.backgroundColor = UIColor.black
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        fromController.present(controller, animated: true, completion: nil)
    }
    
    override func animateFromController(_ fromController:UIViewController, toController: UIViewController, fromView: UIView, toView: UIView, containerView: UIView, duration: TimeInterval, transitionContext: UIViewControllerContextTransitioning) {
        
        if isPresenting {
            containerView.addSubview(toView)
            
            if let imageView = imageView, let backgroundView = backgroundView , imageView.window == nil {
                containerView.addSubview(backgroundView)
                containerView.addSubview(imageView)
            }
            originalImage?.isHidden = true
            toView.isHidden = true
            backgroundView?.alpha = 0.0
            UIView.animate(withDuration: duration, animations: { [weak self] () -> Void in
                self?.imageView?.frame = toView.frame
                self?.backgroundView?.frame = toView.frame
                self?.backgroundView?.alpha = 1.0
                }, completion: { [weak self] (success) -> Void in
                    transitionContext.completeTransition(success)
                    self?.imageView?.isHidden = true
                    self?.backgroundView?.isHidden = true
                    toView.isHidden = false
            })
            
        } else {
            imageView?.isHidden = false
            backgroundView?.isHidden = false
            fromView.isHidden = true
            UIView.animate(withDuration: duration, animations: { [weak self] () -> Void in
                self?.imageView?.frame = self?.oldFrame ?? CGRect.zero
                self?.backgroundView?.frame = self?.oldFrame ?? CGRect.zero
                self?.backgroundView?.alpha = 0.0
                }, completion: { [weak self] (success) -> Void in
                    self?.originalImage?.isHidden = false
                    self?.imageView?.isHidden = true
                    self?.imageView?.removeFromSuperview()
                    self?.backgroundView?.removeFromSuperview()
                    fromView.removeFromSuperview()
                    transitionContext.completeTransition(success)
                    
            })
        }
    }
    
}

class RevealAnimator: PresentationAnimator {
    
    /**
     RevealType represents different sides of screen from which new view controller can reveal.
     */
    enum RevealType : Int {
        case left, right, bottom, top
        
        func percentageValueX(_ value: CGFloat, frame: CGRect) -> CGFloat {
            switch self {
            case .left:
                return -frame.width
            case .right:
                return frame.width
            default:
                return 0.0
            }
        }
        
        func percentageValueY(_ value: CGFloat, frame: CGRect) -> CGFloat {
            switch self {
            case .top:
                return -frame.height
            case .bottom:
                return frame.height
            default:
                return 0.0
            }
        }
        
        func presentedViewForFrame(_ frame: CGRect, percentage: CGFloat) -> CGRect {
            switch self {
            case .top:
                return CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: frame.height * percentage)
            case .bottom:
                let height = frame.height * percentage
                return CGRect(x: frame.origin.x, y: frame.height - height, width: frame.width, height: height)
            case .left:
                return CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width * percentage, height: frame.height)
            case .right:
                let width = frame.width * percentage
                return CGRect(x: frame.width - width, y: frame.origin.y, width: frame.width * percentage, height: frame.height)
            }
        }
    }
    
    var shouldPush: Bool = true
    var revealType: RevealType = .left
    var percetageToCover: CGFloat = 0.8
    weak var blurView: UIView?
    weak var presentedController: UIViewController?
    
    override func setupForAnimationFromController(_ fromController:UIViewController, toController: UIViewController, fromView: UIView, toView: UIView, containerView: UIView) {
        toView.frame = revealType.presentedViewForFrame(fromView.bounds, percentage: percetageToCover)
        
        //add gesture recognizer and blur view
        if self.blurView == nil {
            let blurView = UIView(frame: fromView.bounds)
            blurView.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.0)
            containerView.addSubview(blurView)
            
            self.blurView = blurView
            self.presentedController = toController
            
            let gesture = UITapGestureRecognizer(target: fromController, action:#selector(UIViewController.dismissController))
            gesture.numberOfTapsRequired = 1
            blurView.addGestureRecognizer(gesture)
        }
    }
    
    override func setupBeforAnimationFromView(_ fromView: UIView, toView: UIView) {
        toView.transform = CGAffineTransform(translationX: revealType.percentageValueX(1, frame: toView.frame), y: revealType.percentageValueY(1, frame: toView.frame))
        blurView?.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.0)
        
        if shouldPush {
            fromView.transform = CGAffineTransform.identity
        }
    }
    
    override func setupForPresentationAnimationFromView(_ fromView: UIView, toView: UIView) {
        toView.transform = CGAffineTransform.identity
        blurView?.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.5)
        
        if shouldPush {
            fromView.transform = CGAffineTransform(translationX: -revealType.percentageValueX(percetageToCover, frame: toView.frame), y: -revealType.percentageValueY(percetageToCover, frame: toView.frame))
        }
    }
    
    override func cleanUpFromController(_ fromController:UIViewController, toController: UIViewController, fromView: UIView, toView: UIView, containerView: UIView) {
        blurView?.removeFromSuperview()
    }
}

class JPOverlayAnimator: PresentationAnimator{
    
    override func animateFromController(_ fromController:UIViewController, toController: UIViewController, fromView: UIView, toView: UIView, containerView: UIView, duration: TimeInterval, transitionContext: UIViewControllerContextTransitioning) {
        
        if isPresenting {
            toView.transform = CGAffineTransform(scaleX: 0, y: 0)
            toView.alpha = 0.0
            containerView.addSubview(toView)
            UIView.animate(withDuration: duration, animations: { () -> Void in
                toView.transform = CGAffineTransform.identity
                toView.alpha = 1.0
            }, completion: { (success) -> Void in
                transitionContext.completeTransition(success)
            })
            
        } else {
            UIView.animate(withDuration: duration, animations: { () -> Void in
                fromView.transform = CGAffineTransform(scaleX: 0, y: 0)
                toView.alpha = 0.0
            }, completion: { (success) -> Void in
                fromView.removeFromSuperview()
                transitionContext.completeTransition(success)
            })
        }
        
    }
}


