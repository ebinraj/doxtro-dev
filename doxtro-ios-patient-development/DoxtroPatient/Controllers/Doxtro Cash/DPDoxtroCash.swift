//
//  DPDoxtroCash.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 27/02/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import TSMessages
class DoxtroCash{
    static func getDoxtroCash(completationHandler : @escaping(_ doxtroCash : Int)->Void){
        if let userID = DPWebServicesManager.sharedManager.user?.userId{
        let params = ["_id":userID]
            let request = GetDoxtroCashRequest(params: params as [String:AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if let response = response{
                    if let data = response["data"].dictionary {
                        if let doxtroCash = data["doxtroCash"]?.int{
                            completationHandler(doxtroCash)
                        }else{
                            TSMessage.showNotification(in: UIApplication.getRootViewController(), title: "Network Error", subtitle: "", type: .error)
                        }
                    }
                }
            })
    }
 }
}

