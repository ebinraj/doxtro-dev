//
//  DPRazorPayViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/03/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import TSMessages
class DpRazorPaymentController : NSObject,RazorpayPaymentCompletionProtocol,ExternalWalletSelectionProtocol{
    var razorpay : Razorpay!
    static let sharedManager = DpRazorPaymentController()
    var paybaleAmount : Int?
    var completionHandler : ((_ rozorPayPaymentId : String?,_ walletName : String?,_ paymentData :String?)->Void)?
    //Initilizing RazorPayCredential
    func initializaRazorPay(){
        razorpay = Razorpay.initWithKey(RazorPayPayment.KeyID.LIVE, andDelegate: self)
        //razor delegate method for external wallet method callback like paytm
        razorpay.setExternalWalletSelectionDelegate(self)
    }
    //open razorpay payment form
    func showPaymentForm(paybleAmount : Int?) {
        self.paybaleAmount = paybleAmount
        self.initializaRazorPay()
        let imageLogo = UIImage(named: "navigationlogo")
        var options = ["image":imageLogo,
                       "name" :"Doxtro",
                       "description": "Fee",
                       "theme" : ["color" : "#27A29C"],
                       "prefill" : [:],
                       "external" : ["wallets" : ["paytm"]]] as [String : Any]
        if var PrefillDictionary = options["prefill"] as? [String : String]{
            if let user = DPWebServicesManager.sharedManager.user{
                if let prefillEmail = user.emailId{
                    PrefillDictionary["email"] = prefillEmail
                }
                if let prefillMobileNumber = user.mobileNum{
                    PrefillDictionary["contact"] =  prefillMobileNumber
                }
                options["prefill"] = PrefillDictionary
            }
        }
        if let amount = paybleAmount{
            options["amount"] = (amount * 100)
            razorpay.open(options)
        }
    }
    //when user select paytm as a wallet option
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        if walletName == "paytm"{
            if let paybleAmount = self.paybaleAmount{
                handlePaytmResponse()
                DPPaytmController.sharedManager.openPaytmWith(amount: paybleAmount)
            }else{
                TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Failed to fetch amount", type: .error)
            }
            //completionHandler!(nil,walletName,paymentData)
        }
    }
    func handlePaytmResponse(){
        DPPaytmController.sharedManager.completionHandler = {(paytmResponse) in
            self.completionHandler!(nil,"paytm",paytmResponse)
        }
    }
    func onPaymentError(_ code: Int32, description str: String) {
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: str, type: .error)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        completionHandler!(payment_id,nil,nil)
    }
}
struct RazorPayPayment {
    struct KeyID {
        static let TEST = "rzp_test_AHmF8XvSfeTdOc"
        static let LIVE = "rzp_live_mr1w5Vcvhkac5j"
    }
}
