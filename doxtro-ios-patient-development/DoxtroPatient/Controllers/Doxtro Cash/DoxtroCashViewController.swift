//
//  DoxtroCashViewController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 27/02/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import UIKit
import TSMessages
import SwiftyJSON
let MAXIMUM_ADD_CASH_VALUE = 5000
let MINIMUM_ADD_CASH_VALUE = 100
class DoxtroCashVIewController : UIViewController{
    let amountArray : [Int] = [200,300,500]
    @IBOutlet weak var collectionVIew: UICollectionView!
    @IBOutlet weak var addDoxtroCashTextField: UITextField!
    @IBOutlet weak var doxtroCashLabel: UILabel!
    @IBOutlet weak var rupeesTextField: UITextField!
    @IBOutlet weak var makePaymentButton: UIButton!
    var consultationId  : String?
    var selectedLanguageIndexPath : IndexPath?
    var doxtroCash : Int = 0
    var addDoxtroCash : Int = 0
    var completionHandler : ((_ doxtroCash : Int,_ paymentData : PaymentData)->Void)?
    @IBAction func makePayment(_ sender: Any) {
        if let value = Int(addDoxtroCashTextField.text!){
            if value <= MAXIMUM_ADD_CASH_VALUE && value >= MINIMUM_ADD_CASH_VALUE {
                handleRazorPayResponse()
                DpRazorPaymentController.sharedManager.showPaymentForm(paybleAmount: value)
            }
        }
    
    }
    @IBAction func dismissViewCOntroller(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        addObserverToTextField()
        addDoxtroCashTextField.becomeFirstResponder()
        addDoxtroCashTextField.delegate = self
        addDoxtroCashTextField.useUnderline(underlineColor: UIColor().grayDoxtroCgColor()!)
        rupeesTextField.useUnderline(underlineColor: UIColor().grayDoxtroCgColor()!)
        self.doxtroCashLabel.text = "₹ "+String(describing: self.doxtroCash)
        collectionVIew.delegate = self
        collectionVIew.dataSource = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.addDoxtroCashTextField.resignFirstResponder()
    }
    func addObserverToTextField(){
        addDoxtroCashTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
    }
    func textFieldDidChange(sender : UITextField){
        if sender == addDoxtroCashTextField{
            if let value = Int(sender.text!){
                if value <= MAXIMUM_ADD_CASH_VALUE  && value >= MINIMUM_ADD_CASH_VALUE  {
                    setMakePaymentButtonStatus(isLessThanMaximum: true)
                }else{
                    setMakePaymentButtonStatus(isLessThanMaximum: false)
                }
            }
        }
    }
    func setMakePaymentButtonStatus(isLessThanMaximum : Bool){
        if isLessThanMaximum{
            self.makePaymentButton.backgroundColor = UIColor().greenDoxtroCgColor()
            self.makePaymentButton.isEnabled = true
        }else{
            self.makePaymentButton.backgroundColor = UIColor(red: 88.0/255.0, green: 89.0/255.0, blue: 91.0/255.0, alpha: 1)
            self.makePaymentButton.isEnabled = false
        }
    }
}
extension DoxtroCashVIewController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        if  string != numberFiltered{
            return false
        }
        return isValidUPtolength(withTextField: textField, length: 4, range: range, string: string)
    }
    func isValidUPtolength(withTextField : UITextField,length : Int,range: NSRange,string : String)->Bool{
        let currentString: NSString = NSString(string: withTextField.text!)
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= length
    }
    
}
extension DoxtroCashVIewController : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "doxtroCashCell", for: indexPath) as! DoxtroCashCell
        cell.doxtroCashLabel.text = "+ ₹"+String(describing: amountArray[indexPath.row])
        if let selectedIndexPath = self.selectedLanguageIndexPath{
        if selectedIndexPath == indexPath{
            cell.doxtroCashLabelVIew.backgroundColor = UIColor(red: 39/255.0, green: 162/255.0, blue: 156/255.0, alpha: 0.5)
            cell.doxtroCashLabelVIew.layer.borderWidth = 0.5
            cell.doxtroCashLabelVIew.layer.borderColor = UIColor().greenDoxtroCgColor().cgColor
        }else{
            cell.doxtroCashLabelVIew.backgroundColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 0.5)
            cell.doxtroCashLabelVIew.layer.borderWidth = 0.5
            cell.doxtroCashLabelVIew.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
        }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedLanguageIndexPath = indexPath
        addAndShowDoxtroCasOnTextField(addCash: amountArray[indexPath.row])
        self.textFieldDidChange(sender: addDoxtroCashTextField)
        collectionView.reloadData()
    }
    func addAndShowDoxtroCasOnTextField(addCash : Int){
        if let currentTextFieldValue = Int(self.addDoxtroCashTextField.text!){
            self.addDoxtroCashTextField.text = String(describing: (currentTextFieldValue + addCash))
        }else{
           self.addDoxtroCashTextField.text = String(describing: addCash)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return amountArray.count
    }
}
extension DoxtroCashVIewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/4, height: collectionView.frame.height)
    }
}
extension DoxtroCashVIewController{
    func handleRazorPayResponse(){
        DpRazorPaymentController.sharedManager.completionHandler =  {(paymentId ,walletName,paytmResponse) in
            if let userId = DPWebServicesManager.sharedManager.user?.userId{
                var params = ["patientId" : userId ,"type" : "live"]
                if let paymentId = paymentId{
                    params["payment_id"] = paymentId
                    params["gateway"] = "razorPay"
                }else if walletName  == "paytm"{
                    params["gateway"] = "paytm"
                    params["paytmResponse"] = paytmResponse
                }
                if let consultationID = self.consultationId{
                    params["consultationId"] = consultationID
                }
                self.addMoneyToDoxtro(params: params as[String : AnyObject])
            }
        }
    }
    func addMoneyToDoxtro(params : [String : AnyObject]){
        let request = AddDoxtroCashRequest(params: params)
        ABProgressIndicator.shared.showAnimator()
        DPWebServicesManager.getResponse(with: request) { (response, error) in
            ABProgressIndicator.shared.hideAnimator()
            debugPrint(response)
            if let response = response!["data"].dictionary,error == nil {
                
                let paymentData = PaymentData(dict: JSON(response))
                if let doxtroCash = response["doxtroCash"]?.int{
                     self.completionHandler!(doxtroCash,paymentData)
                }
                self.dismissController()
                TSMessage.showNotification(in: UIApplication.getRootViewController(), title: "Success", subtitle: "added successfully", type: .success)
            }else{
               TSMessage.showNotification(in: UIApplication.getRootViewController(), title: "Error", subtitle: "Failed to add Money in doxtro Cash", type: .error)
            }
        }
    }
}
class DoxtroCashCell : UICollectionViewCell{
    @IBOutlet weak var doxtroCashLabelVIew: UIView!
    @IBOutlet weak var doxtroCashLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        doxtroCashLabel.layer.cornerRadius = 4
        doxtroCashLabelVIew.layer.borderColor = UIColor().grayDoxtroCgColor()?.cgColor
        doxtroCashLabelVIew.layer.borderWidth = 0.5
    }
}

