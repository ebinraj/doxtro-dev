//
//  DPPaytmViewCpontroller.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 01/03/18.
//  Copyright © 2018 Above solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
import TSMessages
class DPPaytmController : NSObject{
    static var sharedManager : DPPaytmController = DPPaytmController()
    lazy var ORDER_NUMBER : String = {
       [unowned self] in
        return String(format: "Doxtro_%d", (arc4random() % 9999999) + 1)
        }()
    var completionHandler : ((_ payTmResponse : String)->Void)?
    
    func openPaytmWith(amount : Int){
        let paybleAmount = String(describing: amount)
        ABProgressIndicator.shared.showAnimator()
        //calling check sum api for genrate checksum for varfting
            getCheckSum(amount:paybleAmount,completionHandler: { (error, response) in
                ABProgressIndicator.shared.hideAnimator()
                if let checkSumObj = response,error == nil{
                    self.makePaytmOrderWith(checkSumDetails: checkSumObj)
                }else{
                    TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: "Failed to intiate", type: .error)
                }
            })
    }
    func getCheckSum(amount : String ,completionHandler : @escaping(_ error : Error?,_ response : CheckSumPaytm?)-> Void){
        if let user = DPWebServicesManager.sharedManager.user{
            let params = ["customerId":user.userId
                ,"amount":"1",
                 "email":user.emailId,
                 "mobileNumber":"8123632112",
                 "orderId" :ORDER_NUMBER,
                 "type" : "live"]
            let request = GetChecksumPaaytmRequest(params: params as [String : AnyObject])
            DPWebServicesManager.getResponse(with: request, completionHandler: { (response, error) in
                if error == nil,let response = response{
                    if let dict = response.dictionary{
                        let checksumObject = CheckSumPaytm(dict: JSON(dict["data"]?.dictionary))
                        completionHandler(nil,checksumObject)
                    }
                }else{
                    completionHandler(error,nil)
                }
            })
        }
    }
    func makePaytmOrderWith(checkSumDetails : CheckSumPaytm){
        if let user = DPWebServicesManager.sharedManager.user{
            //making order using checksum api response
            let orderDict = ["MID":checkSumDetails.MID,
                             "CHANNEL_ID":checkSumDetails.CHANNEL_ID,
                             "INDUSTRY_TYPE_ID":checkSumDetails.INDUSTRY_TYPE_ID,
                             "WEBSITE":checkSumDetails.WEBSITE,
                             "TXN_AMOUNT":checkSumDetails.TXN_AMOUNT,
                             "ORDER_ID":checkSumDetails.ORDER_ID,
                             "CALLBACK_URL":checkSumDetails.CALLBACK_URL,
                             "CHECKSUMHASH":checkSumDetails.CHECKSUMHASH,
                             "REQUEST_TYPE":checkSumDetails.REQUEST_TYPE,
                             "CUST_ID":checkSumDetails.CUST_ID,
                             "EMAIL":user.emailId,
                             "MOBILE_NO":"8123632112"]
            if let order = PGOrder(params: orderDict){
                intiatePaymentWithOrder(order: order)
            }
        }
    }
    func intiatePaymentWithOrder(order : PGOrder){
        
        let viewBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64))
        viewBar.backgroundColor = UIColor().greenDoxtroCgColor()
        let button = UIButton(frame: CGRect(x: 5, y: 24, width: 70, height: 40))
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.textColor = UIColor.black
        button.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14.0)
        let tileLabel = UILabel(frame: CGRect(x:  UIScreen.main.bounds.width/2 - 50, y: 20, width: 100, height: 50))
        tileLabel.text = "Doxtro"
        tileLabel.font = UIFont(name: "Helvetica-Bold", size: 18.0)
        tileLabel.textColor = UIColor.white
        tileLabel.textAlignment = NSTextAlignment.center
        viewBar.addSubview(tileLabel)
        var merchantObj = PGMerchantConfiguration.default()
        if let transactionController = PGTransactionViewController.init(transactionFor: order){
            transactionController.topBar = viewBar
            transactionController.cancelButton = button
            transactionController.serverType = eServerTypeProduction
            transactionController.merchant = merchantObj
            transactionController.loggingEnabled = true
            transactionController.delegate = self
            UIApplication.getTopViewController()?.present(transactionController, animated: true, completion: nil)
        }
    }
}
extension DPPaytmController : PGTransactionDelegate{
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
                    debugPrint(responseString)
                    self.completionHandler!(responseString)
                    controller.dismissController()
    }
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        controller.dismissController()
    }
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        TSMessage.showNotification(in: UIApplication.getTopViewController(), title: "Error", subtitle: error.localizedDescription, type: .error)
    }
}

//paytm uichanges
// let imageVIew  = UIImageView(frame: CGRect(x: 0, y: 16, width: 70, height: 40))
//        //add title
//        UILabel *mTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 10, 1, 100, 50)];
//        [mTitleLabel setText:@"Payment"];
//        [mTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
//        mTitleLabel.textColor = [UIColor whiteColor];
//        [mNavBar addSubview:mTitleLabel];
