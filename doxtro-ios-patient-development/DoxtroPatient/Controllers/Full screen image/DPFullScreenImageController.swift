//
//  DPFullScreenImageController.swift
//  DoxtroPatient
//
//  Created by Ayush Yadav on 02/10/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import Foundation
import UIKit
class JPFullScreenImageController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var constraintLeft: NSLayoutConstraint!
    @IBOutlet weak var constraintRight: NSLayoutConstraint!
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    var lastZoomScale: CGFloat = 1
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.updateZoom()
    }
    
    func setupView() {
        if let user = DPWebServicesManager.sharedManager.user{
            title = user.name
        }
        
        if let image = image {
            profileImageView.image = image
        }
    }
    
    //MARK: UIScrollViewDelegate method
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return profileImageView;
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        self.updateConstraints()
    }
    
    func updateConstraints() {
        let imageWidth = profileImageView.image?.size.width ?? 0
        let imageHeight = profileImageView.image?.size.height ?? 0
        let viewWidth = self.view.bounds.size.width
        let viewHeight = self.view.bounds.size.height
        
        // center image if it is smaller than screen
        var hPadding = (viewWidth - self.scrollView.zoomScale * imageWidth) / 2
        if (hPadding < 0) {
            hPadding = 0
        }
        
        var vPadding = (viewHeight - self.scrollView.zoomScale * imageHeight) / 2
        if (vPadding < 0) {
            vPadding = 0
        }
        
        self.constraintLeft.constant = hPadding
        self.constraintRight.constant = hPadding
        self.constraintTop.constant = vPadding
        self.constraintBottom.constant = vPadding
        
        // Makes zoom out animation smooth and starting from the right point not from (0, 0)
        self.view.layoutIfNeeded()
    }
    
    // Zoom to show as much image as possible unless image is smaller than screen
    func updateZoom() {
        var minZoom = min(self.view.bounds.size.width / (profileImageView.image?.size.width ?? 0), self.view.bounds.size.height / (profileImageView.image?.size.height ?? 0))
        
        self.scrollView.minimumZoomScale = minZoom
        
        // Force scrollViewDidZoom fire if zoom did not change
        if (minZoom == self.lastZoomScale) {
            minZoom += 0.000001
        }
        self.scrollView.zoomScale = minZoom
        self.lastZoomScale = minZoom
    }
    
    @IBAction func dismissFullscreenController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
}
