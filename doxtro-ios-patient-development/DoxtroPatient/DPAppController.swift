//
//  DPAppController.swift
//  DoxtroPatient
//
//  Created by vinay kumar on 5/22/17.
//  Copyright © 2017 Above solutions. All rights reserved.
//

import UIKit
import NPSegmentedControl
class DPAppController: NSObject {

}
open class DDCustomView: UIView {
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    @IBInspectable
    public var borderColor: UIColor = UIColor.gray {
        didSet {
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    @IBInspectable
    public var borderWidth: CGFloat = 1.0 {
        didSet {
            self.layer.borderWidth = self.borderWidth
        }
    }
    @IBInspectable
    public var shadowOffset: CGSize = CGSize(width: 2.0, height: 2.0){
        didSet {
            self.layer.shadowOffset = self.shadowOffset
        }
    }
    @IBInspectable
    public var masksToBounds: Bool = false {
        didSet {
            self.layer.masksToBounds = self.masksToBounds
        }
    }
    @IBInspectable
    public var shadowColor: UIColor = UIColor.darkGray {
        didSet {
            self.layer.shadowColor = self.shadowColor.cgColor
        }
    }
    @IBInspectable
    public var shadowOpacity: Float = 0.7 {
        didSet {
            self.layer.shadowOpacity = self.shadowOpacity
        }
    }
}

@IBDesignable
open class DDHighlightedButton: UIButton {
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override open func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        if let touchNotNil = touch {
            if self.point(inside: touchNotNil.location(in: self), with: event) {
                buttonAnimation()
            }
        }
    }
    func buttonAnimation() {
        transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 0.6,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.80),
                       initialSpringVelocity: CGFloat(8.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
    }
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    @IBInspectable
    public var borderColor: UIColor = UIColor.gray {
        didSet {
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    @IBInspectable
    public var borderWidth: CGFloat = 1.0 {
        didSet {
            self.layer.borderWidth = self.borderWidth
        }
    }
}
extension UIView {
    func addDropShadowToView(targetView:UIView? ){
        targetView?.layer.masksToBounds = false
        targetView?.layer.shadowColor = UIColor.darkGray.cgColor
        targetView?.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        targetView?.layer.shadowOpacity = 0.7
    }
    func useUnderline(underlineColor:UIColor) {
        let border = CALayer()
        let borderWidth = CGFloat(0.5)
        border.borderColor = underlineColor.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - borderWidth, width:screenWidth, height:self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    func applyBorder(borderColor:UIColor, cornerRadius:CGFloat) {
        self.layer.borderWidth = 1
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
    
    func addBootomBorderLine (width : CGFloat,color : CGColor = UIColor(red: 139/255.0, green: 139/255.0, blue: 139/255.0, alpha: 0.5).cgColor){
        let border = CALayer()
        let width = width
        border.borderColor = color
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    func addCursor(){
        let border = CALayer()
        let width = CGFloat(4)
        border.borderColor = UIColor().greenDoxtroCgColor().cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    func addDashedBOrderLine(color : CGColor){
        var yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = color
        yourViewBorder.lineDashPattern = [6, 6]
        yourViewBorder.frame = self.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.lineWidth = 2.0
        
        yourViewBorder.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(yourViewBorder)
    }
}
extension  NPSegmentedControl {
    func setSelectedIndex(index:Int) {
        selectCell(index: index, animate: true)
        self.sendActions(for: .valueChanged)
    }
}
extension UITextField {
    func addLeftImageInTextFields(_ imageStr: String) {
        let imageView = UIImageView();
        let image = UIImage(named: imageStr);
        imageView.image = image;
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0, y: 5, width: 20, height: 20)
        self.addSubview(imageView)
        let leftView = UIView.init(frame: CGRect(x: 10, y: 0, width: 30, height: 30))
        self.leftView = leftView
        self.leftViewMode = UITextFieldViewMode.always
    }
}
extension UIColor{
    func greenDoxtroCgColor()->UIColor{
        return UIColor(red: 39/255.0, green: 162/255.0, blue: 156/255.0, alpha: 1)
    }
    func yellowDoxtroCgColor()->UIColor?{
        return UIColor(red: 255/255.0, green: 198/255.0, blue: 8/255.0, alpha: 1)
    }
    func grayDoxtroCgColor()->UIColor?{
        return UIColor(red: 148/255.0, green: 149/255.0, blue: 153/255.0, alpha: 1)
    }
    func darkGreenDoxtroCgColor()->UIColor{
        return UIColor(red: 39/255.0, green: 120/255.0, blue: 126/255.0, alpha: 1)
    }
}
extension UIApplication {
    class func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
}
}
extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
extension URL {
    func valueOf(queryParamaterName: String) -> String? {
        
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
extension NSAttributedString {
    
    func getStrikeThroughString(string: String)->NSAttributedString {
        let attributeString = NSAttributedString(string: string, attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue, NSFontAttributeName : UIFont(name: "Asap-Medium", size: 10)])//UIFont.boldSystemFont(ofSize: 10)])
        return attributeString
    }
}

