const fs = require('fs');
const path = require('path');
var fbadmin = require("firebase-admin");
let serviceAccount = {
    "type": "service_account",
    "project_id": "doxtrov2",
    "private_key_id": "1938575525cc3bb6350042ecb51d0edda42fa2c1",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCsmciD+FmFhR0Y\n0XAxYY9VP0SztLGJk6hSkOIG8ZQgJnkSMVWGljNK1rKqeqjgXn8ZUsfbiJLdX0yb\nv+CwadPt6lvoBi0JSjC2Xb4eNCxMQC5Pzh2N3LwFaDUA5pG8TiDwZ6q1DDP7Jshq\nnS4FCPj62N4FVXMQmrofoSf53sMsxPkj++h7KwEsQrrvmcKnJkXKaCdqGFtZ5L9I\nP92KBTjf7iHn/JWPyqzvBZmPLAC+TUQHTvDe5WG1jxoKJmQ5wMdrL2bmwUXPc7fW\n0/1T2Rl/ulFlf/brrYh/KqFYGd6+Yw++gRiu2nz8ttPg8bgSR0DiUV8+fGT8aSi8\n+PYbyUfjAgMBAAECggEASHM28XpubXb+SzwiyX+nsENh/cMD4Pl7psjNL9f48vPj\nT0kpGtjyh+AOWDQrrh7MYB9Y6CRwbX8Cw7/ru3F4tb3xvTfWcLdVMj+wPbJtw8YP\nCo285x74R5bB5PJjyzTKa89AH0YTjcFuUB5RQmjNRlKBA6EmLxqbtQorIx4oNCJ7\n7wYf2EgbMlNFf23PsyCxvBOVwYdHn14vjSGadRxEXHhm2ljrZy7iPaj5Z6T13pyu\nVECbwq3DY4VeSe2pgaJJz0fDlOKvrXd2RUtotJpznG1OCghgbLlFyAb3IwKQzTdG\nTYt6ncRCb/meGrVOl1mHwksGogCTQnds71NHYfjhYQKBgQDS4lG24ScEL3NCm8om\ndr337NodpIJAiLHwtoBuTh+9+oNZZbJ62mFOBkktAZNCrGL99RguWcedcZuNdutf\ns825qIeZO2/leS/Mz/64aujnJ6tKwIqgDP9kT2+lV8VcEC3z0YLCSNwTZg4ZDcS2\n205rU3BRQbHNpaGCLr3x/13bgwKBgQDRhsPbHktegSPpxzJBf85gf3lfErVdzw4k\ne53yTPO1gcuW2bhlLr+NnDnL/joXnqgfme0+SRRGJ3pG1rn81qbiu+h1DqkHHF4U\nT1Cb+I6FKtsu6GKGjC1aRijw6xB2h3A/bLksuqQl8SFcckK1OeSWmKEH6RRgAX/F\n30cvq2NUIQKBgQCtzjXG4l6Dxo/acAf2F5uOQLmsALARfBgb6m6DewQSUev7vCOT\nuXTFF80ruMakBuw3ns9lND2e98lamI55e/mscMgBdMlOecj5pturJwH5seotW+9k\n5hF/nTDJN0PEEtmswBpjAiXQHyazY3OjubAyKLzDW7+tXDt2zgti7jNu/QKBgQCS\nMUSwu3d9DgAf2pPPLbyyid8Gnggb1x+YBwSSjmyxJUoHDAxj1PVkei+9jLm9xz6p\nJf5pqhXSSTNBAAuvRZoVs2SjYEJBquJdq2WkHg5bDib2mMA9/sqaJT/lsS3Adgm0\nx0iA1A06R/+uAC9ZQXakOQSKf29u+TzMtFh4A05/4QKBgC51OYpm5vU2QEE96l1i\n2yRl0Qt2Igt94BJccgWllr9sCARkt4ppQyhPtRPlauEyCz2pm5/QdVFKLeHkKZYO\nnfs7A1hNyaC7Qs++g0mDsh2Orr+8Amgdr4Vlpd5DcpwgbsZOoPs6h99cy1fdljeM\nLHygmToSQyXbfdYgKiwCz9L6\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-3difm@doxtrov2.iam.gserviceaccount.com",
    "client_id": "106678191266970295906",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-3difm%40doxtrov2.iam.gserviceaccount.com"
}

fbadmin.initializeApp({
    credential: fbadmin.credential.cert(serviceAccount),
    databaseURL: "https://doxtrov2.firebaseio.com"
});

//put consultationId here
let consultationId = "5a24960aa52cee250f62c8eb";

let parentRef = fbadmin.database().ref('/Consultations/' + consultationId);
parentRef.once("value", function (snapshot) {
    //To keep backup of chat messages before update    
    let filePath = path.join(__dirname, consultationId + '.json');
    fs.writeFile(filePath, JSON.stringify(snapshot.val()), 'utf8', function (err) {
        if (err)
            return console.log(err);
        console.log("The file was saved!");
    })

    let prescriptionKeys = ["data", "from", "msgType", "receiver", "sender", "status", "timeStamp"];
    let snapObj = {};
    let prescriptionObj = {};
    snapshot.forEach(function (item) {
        console.log(item.key);
        if (prescriptionKeys.indexOf(item.key) != -1) {
            console.log("got key")
            prescriptionObj[item.key] = item.val();
            snapObj[item.key] = null;
        }
    });
    
    snapObj["Doctor_" + prescriptionObj.timeStamp] = prescriptionObj;
    /*  let filePath1 = path.join(__dirname, consultationId + '1.json');
    fs.writeFile(filePath1, JSON.stringify(snapObj), 'utf8', function (err) {
        if (err)
            return console.log(err);
        console.log("The file was saved!");
    }) */
    parentRef.update(snapObj);
})

