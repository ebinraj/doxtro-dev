/**
 * This script is used migrate doctor records from collection user, businesspartner, address, tokenDetails to "doctor"
 */
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017/doxtro-prod';
//const url = 'mongodb://doxtrodev.above-inc.net:27017/Doxtro-Development';



// Use connect method to connect to the Server of old database
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const consultation = db.collection('consultation');
        const payment = db.collection('payment');
        const ObjectID = mongodb.ObjectID;
        let limit = 10000, skip = 0;
        consultation.find().limit(limit).skip(0).toArray(function (err, data) {

            if (data.length != 0) {
                console.log(data.length + " consultation records found.");
                let count = 0, totalUpdatedCount = 0;
                getData();

                function getData() {

                    if (data[count].paymentId) {
                        payment.findOne({ _id: data[count].paymentId }, 'createdAt', function (err, cn) {
                            let paymentDate;
                            if (!err && cn)
                                paymentDate = cn.createdAt;
                            else
                                paymentDate = data[count].createdAt;

                            consultation.update({_id: data[count]._id }, { $set: { paymentDate: paymentDate } }, function (err, ct) {
                                if (!err && ct) {
                                    console.log("ct", ct.result.nModified);
                                    totalUpdatedCount += ct.result.nModified;
                                }
                                else
                                    console.log("failed to update : ", data[count]._id);
                                count++;
                                console.log("count", count);
                                if (count < data.length) {
                                    getData();
                                } else {
                                    console.log("done!!!", totalUpdatedCount);
                                }
                            })
                        })
                    } else {
                        consultation.update({ _id: data[count]._id }, { $set: {paymentDate: data[count].createdAt } }, function (err, ct) {
                            if (!err && ct) {
                                console.log("ct", ct.result.nModified);
                                totalUpdatedCount += ct.result.nModified;
                            }
                            else
                                console.log("failed to update : ", data[count]._id);
                            count++;
                            console.log("count", count);
                            if (count < data.length) {
                                getData();
                            } else {
                                console.log("done!!!", totalUpdatedCount);
                            }
                        })

                    }
                }
            } else {
                console.log("No consultation record found");
                //Close connection
                db.close();
            }
        });

    }
});
