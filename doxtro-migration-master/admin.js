/**
 * This script is used migrate admin records from collection user, businesspartner, address, tokenDetails to "admin"
 */
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

const url = 'mongodb://localhost:27017/doxtro1';
//new database url;
const newUrl = 'mongodb://localhost:27017/doxtro-prod';
var newDB;

MongoClient.connect(newUrl, function (err, db) {
    if (err)
        console.log('Unable to connect to the mongoDB server. Error:', err);
    else
        newDB = db;
});

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const user = db.collection('user');
        const businesspartner = db.collection('businesspartner');
        const address = db.collection('address');
        const mediaObject = db.collection('mediaobject');
        const tokendetails = db.collection('tokendetails');
        const ObjectID = mongodb.ObjectID;

        user.find({ tag: { $in: ['Admin', 'SuperAdmin', 'Bot', 'TeleCaller', 'Sales'] } }).toArray(function (err, data) {
            if (data.length != 0) {
                console.log(data.length + " admin records found.");
                let count = 0, list = [];
                getData();

                function getData() {
                    //create new record structure
                    let userRecord = {
                        _id: data[count].businessPartnerId,
                        tag: (data[count].tag).charAt(0).toLowerCase() + (data[count].tag).slice(1),
                        password: data[count].password,
                        passwordExpirationTime: data[count].passwordExpirationTime,
                        accountStatus: data[count].accountStatus,
                        createdAt: data[count].createdAt,
                        updatedAt: data[count].updatedAt,
                        accountActivationTime: data[count].createdAt,
                        deleted: data[count].logicalDelete == false ? false : true,
                        userId: "team#" + (count + 1),
                        __v: 0
                    }
                    if(userRecord.deleted == true)
                        userRecord.deletedAt = userRecord.updatedAt;
                    businesspartner.findOne({ _id: data[count].businessPartnerId }, function (err, dt) {
                        if (!err && dt) {
                            userRecord.emailid = dt.emailid;
                            userRecord.firstName = dt.givenName;
                        }
                        address.findOne({ objectId: (data[count].businessPartnerId).toString() }, function (err, add) {
                            if (!err && add)
                                userRecord.mobile = add.mobile;
                            mediaObject.findOne({ "tag": "profilePic", objectId: (data[count].businessPartnerId).toString() }, function (err, md) {
                                if (!err && md)
                                    userRecord.profilePic = md.profilePic;
                                    list.push(userRecord);
                                    count++;
                                    if (count < data.length) {
                                        getData();
                                    } else {
                                        //insert into new collection
                                        newDB.collection('admin').insertMany(list, function (err, data) {
                                            if (data.insertedCount) {
                                                console.log(data.insertedCount + " admin records created");
                                            } else {
                                                console.log("No admin records created");
                                                //Close connection
                                                db.close();
                                                newDB.close();
                                            }
                                        })
                                    }
                            })
                        })

                    })
                }
            } else {
                console.log("No admin records found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
