/**
 * This script is used migrate relatives("Myself") records from collection "patient" to "relatives".
 */
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');

const MongoClient = mongodb.MongoClient;

const url = 'mongodb://localhost:27017/doxtro1';
const newUrl = 'mongodb://localhost:27017/doxtro-prod';
var newDB;
MongoClient.connect(newUrl, function (err, db) {
    newDB = db;
});

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const patient = db.collection('patient');
        const businesspartner = db.collection('businesspartner');
        const ObjectID = mongodb.ObjectID;
        var newRelation;
      
        let aggregateArray = [
            { $match: { relation: 'Myself' } },
            { $group: { _id: "$objectId", relatives: { $push: "$$ROOT" } }}
        ]
        patient.aggregate(aggregateArray).toArray(function (err, data) {
            if (data.length != 0) {
                console.log(data.length + " relatives records found for Myself");
                let list = [], count = 0;
                addData();

                function addData() {
                    let record = data[count].relatives;
                    let lastRecord = record[record.length - 1];
                    let idList = [];
                    for (let i = 0; i < (record.length -1); i++) {
                        idList.push(record[i]._id)
                    }
                    let userRecord = {
                        idList: idList,
                        _id: lastRecord._id,
                        patientId: ObjectID(lastRecord.objectId),
                        createdAt : lastRecord.createdAt,
                        updatedAt : lastRecord.updatedAt,
                        deleted : lastRecord.logicalDelete == false ? false : true,
                        __v : 0,
                        relation :'Myself'
                    };
                    
                    if(userRecord.deleted == true)
                        userRecord.deletedAt = userRecord.updatedAt;
                    businesspartner.findOne({ _id: ObjectID(lastRecord.objectId) }, function (err, bp) {
                        if (!err && bp) {
                            userRecord.name = bp.givenName ? bp.givenName : lastRecord.name;
                            userRecord.age = bp.age ? bp.age : lastRecord.age;
                            userRecord.gender = bp.gender ? bp.gender : lastRecord.gender;
                        }
                        list.push(userRecord);
                        count++;
                        if (count < data.length) {
                            addData();
                        } else {
                            newDB.collection('relatives').insertMany(list, function (err, data) {
                                if (data.insertedCount) {
                                    console.log(data.insertedCount + " records created");
                                } else {
                                    console.log("No data created");
                                    //Close connection
                                    db.close();
                                    newDB.close();
                                }
                            })

                        }

                    })
                }

            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
