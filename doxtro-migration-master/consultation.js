/**
 * This script is used migrate consultation records from collection "consultation","rating" to "consultation".
 */

//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

/*const url = 'mongodb://localhost:27017/Doxtro_Live_dump';

const newUrl = 'mongodb://localhost:27017/Dox';*/

const url = 'mongodb://localhost:27017/doxtro1';
//new database url;
const newUrl = 'mongodb://localhost:27017/doxtro-prod';
var newDB;
MongoClient.connect(newUrl, function (err, db) {
    newDB = db;
});

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const consultation = db.collection('consultation');
        const businesspartner = db.collection('businesspartner');
        const patient = db.collection('patient');
        const rating = db.collection('rating');
        const payment = db.collection('payment');
        const ObjectID = mongodb.ObjectID;
        Date.prototype.addHours = function (h) { this.setTime(this.getTime() + (h * 60 * 60 * 1000)); return this; }

        consultation.find({}).limit(100).toArray(function (err, data) {

            if (data.length != 0) {
                console.log(data.length + " consultation records found");
                let count = 0, list = [];
                getData();

                function getData() {
                  
                    let consultationRecord = data[count];

                    //defaults
                    consultationRecord.consultationLanguage = "English";
                    consultationRecord.consultationType = "Chat";
                    consultationRecord.__v = 0;
                    consultationRecord.rejectedBy = [];
                    consultationRecord.attachedDocuments = [];
                    consultationRecord.consultationSeq = "consultation#" + parseInt(count + 1);

                    consultationRecord.relativesId = ObjectID(consultationRecord.patientId);
                    delete consultationRecord.patientId;

                    consultationRecord.patientId = ObjectID(consultationRecord.objectId);
                    delete consultationRecord.objectId;

                    consultationRecord.specializationCategory = consultationRecord.speciality;
                    delete consultationRecord.speciality;

                    consultationRecord.deleted = consultationRecord.logicalDelete == false ? false : true,
                    delete consultationRecord.logicalDelete;

                    consultationRecord.assignRequestStatus = consultationRecord.assignRequestStatus ? consultationRecord.assignRequestStatus : "pending";
                    consultationRecord.paymentStatus = consultationRecord.paymentStatus ? (consultationRecord.paymentStatus).charAt(0).toLowerCase() + (consultationRecord.paymentStatus).slice(1) : "pending",
                    consultationRecord.prescriptionId = consultationRecord.prescriptionId ? [consultationRecord.prescriptionId] : [];

                    if(consultationRecord.paymentStatus == 'free'){
                        //consultationRecord.paymentId = payment._id;
                        consultationRecord.patientFee = 0;
                        consultationRecord.consultingFee = 0;
                    }
                    if(consultationRecord.parentId)
                        consultationRecord.parentId = ObjectID(consultationRecord.parentId);
                    if (consultationRecord.deleted == true)
                        consultationRecord.deletedAt = consultationRecord.updatedAt;
                    if (consultationRecord.assignedAt)
                        consultationRecord.closedDate = consultationRecord.assignedAt.addHours(48);
                     if(consultationRecord.doctorId)
                        consultationRecord.doctorId = ObjectID(consultationRecord.doctorId);
                     if(consultationRecord.assignedBy)
                        consultationRecord.assignedBy = ObjectID(consultationRecord.assignedBy);

                    //relatives record
                    patient.findOne({ _id: consultationRecord.relativesId }, function (err, pt) {
                        if (!err && pt) {
                            consultationRecord.consultingFor = pt.relation;
                            consultation.notificationText = pt.name + "," + pt.gender + "," + pt.age;
                        }
                        //userName
                        businesspartner.findOne({ _id: consultationRecord.patientId }, function (err, bp) {
                            if (!err && bp) {
                                consultationRecord.userName = bp.givenName;
                            }
                            //patientFee, consultingFee
                            payment.findOne({ "status": "Credit", objectId: (consultationRecord._id).toString() }, function (err, payment) {
                                if (!err && payment) {
                                    consultationRecord.paymentId = payment._id;
                                    consultationRecord.patientFee = payment.amount;
                                    consultationRecord.consultingFee = payment.amount;
                                }
                                //feedback status
                                rating.findOne({ objectId: (consultationRecord._id).toString() }, function (err, fb) {
                                    if (!err && fb) {
                                        //'skipped', 'submitted'
                                        consultationRecord.feedbackStatus = "submitted";
                                        consultationRecord.feedback = {
                                            rating: parseInt(fb.ratingValue[0]),
                                            date: fb.createdAt
                                        }
                                        switch (fb.ratingValue[0]) {
                                            case "5": consultationRecord.feedback.ratingValue = "Excellent";
                                                break;
                                            case "4": consultationRecord.feedback.ratingValue = "Good";
                                                break;
                                            case "3": consultationRecord.feedback.ratingValue = "Average"
                                                break;
                                            case "2": consultationRecord.feedback.ratingValue = "Bad";
                                                break;
                                            case "1": consultationRecord.feedback.ratingValue = "Terrible";
                                                break;
                                            default: consultationRecord.feedback.ratingValue = "Good";
                                                break;
                                        }
                                    } else {

                                        if (consultationRecord.status == 'Closed')
                                            consultationRecord.feedbackStatus = "skipped";
                                        else
                                            consultationRecord.feedbackStatus = "pending";

                                    }
                                    list.push(consultationRecord);
                                    count++;
                                    if (count < data.length) {
                                        getData();
                                    } else {
                                        //insert into new collection
                                        newDB.collection('consultation').insertMany(list, function (err, data) {
                                            if (!err && data.insertedCount) {
                                                console.log(data.insertedCount + " consultation records created");
                                            } else {
                                                console.log(err);
                                                console.log("No consultation records created");
                                                //Close connection
                                                db.close();
                                                newDB.close();
                                            }
                                        })
                                    }

                                })//rating ends here
                            })

                        })
                    })
                }
            } else {
                console.log("No consultation records found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
