/**
 * This script is used to add firbase urls field in documents of collection "doctor".
 * It should be executed after successful migration of "doctor" collection.
 * No old databse connection required.
 */
//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
var fbadmin = require("firebase-admin");
const UUID = require("uuid-v4");

let serviceAccount = require("./config/serviceAccountKey.json");

fbadmin.initializeApp({
    credential: fbadmin.credential.cert(serviceAccount),
    databaseURL: "https://doxtrodevelopment.firebaseio.com",
    storageBucket: "doxtrodevelopment.appspot.com"
});

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running
const url = 'mongodb://localhost:27017/doxtrophase1';
//const url = 'mongodb://localhost:27017/Doxtro_Live_dump';
const newUrl = 'mongodb://localhost:27017/doxtro2dev';
const bucketname = "doxtrodevelopment.appspot.com";
const chatimgfolder = "documents";

//dbs
var newDB;
var db;

let ObjectID = mongodb.ObjectID;
let count = 0;
let totalModifiedCount = 0;
let Objlist = [];

MongoClient.connect(newUrl, function(err, ldb) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        db = ldb;
        doctor = db.collection('doctor');
        startmigration();
    }
});

function startmigration() {
    return new Promise((resolve, reject) => {

        return doctor.find({ documents: { $exists: true } }, { documents: 1 }).limit(500).skip(500).toArray(function(err, data) {

            if (err || data.length == 0) {
                console.log("No data found to migrate");
            } else {
                let docList = [];
                for (let i = 0; i < data.length; i++) {
                    for (let j = 0; j < data[i].documents.length; j++) {
                        //create document array
                        data[i].documents[j].objectId = data[i]._id;
                        docList.push(data[i].documents[j]);
                    }
                }
                console.log("total docs", docList.length);
                let promises = [];
                docList.forEach(function(dt) {
                    //create new
                    //console.log("uploading for _id : " + dt._id);
                    let uuid = UUID();
                    promises.push(
                        fbadmin.storage().bucket("doxtrodevelopment.appspot.com").upload('./../uploads/' + dt.url, {
                            destination: chatimgfolder + '/' + dt.url,
                            uploadType: "media",
                            metadata: {
                                metadata: {
                                    firebaseStorageDownloadTokens: uuid
                                }
                            }
                        }).then(result => {
                            let file = result[0];
                            let dataurl = "https://firebasestorage.googleapis.com/v0/b/" + bucketname + "/o/" + encodeURIComponent(file.name) + "?alt=media&token=" + uuid;
                            doctor.update({ _id: dt.objectId, "documents._id": dt._id }, { $set: { "documents.$.url": dataurl } }, function(err, result) {
                                if (!err && result && result.result.nModified) {
                                    totalModifiedCount++;
                                } else {
                                    console.log("failed to update for _id : ", dt._id);
                                }
                                Objlist.push({ url: dataurl, _id: dt._id });
                            })
                        }).catch(error => {
                            console.log("failed to upload to firebase for _id : ", dt._id, error.message);
                            Objlist.push({ _id: dt._id, error: error.message });
                        })
                    );
                });

                Promise.all(promises).then(() => {
                    console.log(totalModifiedCount, " records modified.");
                    var filePath = path.join(__dirname, 'records/doctorDocuments.json');
                    fs.writeFile(filePath, JSON.stringify(Objlist), 'utf8', function(err) {
                        if (err) {
                            return console.log(err);
                        }
                        console.log("The file was saved!");
                    });
                    resolve(Objlist);
                }).catch(error => {
                    console.log("error", error);
                    reject(error);
                });
            }


        });
    });


}