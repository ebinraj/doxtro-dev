//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
var fbadmin = require("firebase-admin");
const UUID = require("uuid-v4");

let serviceAccount = require("./config/serviceAccountKey.json");

fbadmin.initializeApp({
    credential: fbadmin.credential.cert(serviceAccount),
    databaseURL: "https://doxtrodevelopment.firebaseio.com",
    storageBucket: "doxtrodevelopment.appspot.com"
});

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running
const url = 'mongodb://localhost:27017/doxtrophase1';
//const url = 'mongodb://localhost:27017/Doxtro_Live_dump';
const newUrl = 'mongodb://localhost:27017/doxtro2dev';
const bucketname = "doxtrodevelopment.appspot.com";
const chatimgfolder = "chat_documents_phase1";

//dbs
var newDB;
var db;

let consultation;
let usercomment;
let prescription;
let ObjectID = mongodb.ObjectID;

let consultationCount = 0;
let userCommentCount = 0;
let docName = "Doctor";

let Objlist = [];

MongoClient.connect(newUrl, function(err, ndb) {
    newDB = ndb;
});

MongoClient.connect(url, function(err, ldb) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {

        db = ldb;
        consultation = db.collection('consultation');
        usercomment = db.collection('usercomment');
        prescription = db.collection('prescription');


        startmigration();
    }
});

function startmigration() {

    //change limit and skip before start
    consultation.find({}, { doctorId: 1, prescriptionId: 1, objectId: 1 }).limit(10000).skip(2049).toArray(function(err, data) {
        loopConsultation(data);
    });
}

function loopConsultation(data) {

    getUserCommentsFromConsultation(data[consultationCount]._id).then(result => {
        loopUserComments(result, data);
    });
}

function getUserCommentsFromConsultation(consultationId) {
    return new Promise(function(resolve, reject) {

        usercomment.find({ audience: { $in: [(consultationId).toString()] }, logicalDelete: false }).toArray(function(err, rchat) {
            resolve(rchat);
        });

    });
}

function loopUserComments(chats, consultationlist) {
    processChat(chats[userCommentCount], consultationlist[consultationCount]).then(result => {
        userCommentCount++;
        if (userCommentCount < chats.length) {
            loopUserComments(chats, consultationlist);
        } else {
            console.log(userCommentCount);
            userCommentCount = 0;
            consultationCount++;
            if (consultationCount < consultationlist.length)
                loopConsultation(consultationlist);
            else {

                var filePath = path.join(__dirname, 'records/chat.json');
                fs.writeFile(filePath, JSON.stringify(Objlist), 'utf8', function(err) {
                    if (err) {
                        return console.log(err);
                    }

                    console.log("The file was saved!");
                });

            }
        }
    });
}

function processChat(chat, coll) {
    return new Promise(function(resolve, reject) {

        let commentObj = chat;
        let commentText, chatObj = {};

        try {

            commentText = JSON.parse(commentObj.commentText);

            if (commentText.msgType != "feedback") {
                chatObj = {
                    _id: commentObj.author,
                    sender: commentText.name,
                    status: "read",
                    msgType: commentText.msgType,
                    //data: commentText.data,
                    timeStamp: new Date(commentObj.createdAt).getTime()
                };

                if (commentText.msgType == "image") {

                    let uuid = UUID();

                    fbadmin.storage().bucket("doxtrodevelopment.appspot.com").upload('./../uploads/' + commentText.data.url, {
                        destination: chatimgfolder + '/' + commentText.data.url,
                        uploadType: "media",
                        metadata: {
                            metadata: {
                                firebaseStorageDownloadTokens: uuid
                            }
                        }
                    }).then(result => {

                        let file = result[0];
                        let dataurl = "https://firebasestorage.googleapis.com/v0/b/" + bucketname + "/o/" + encodeURIComponent(file.name) + "?alt=media&token=" + uuid;
                        console.log(dataurl);

                        chatObj.data = dataurl;
                        chatObj.from = 'Patient';
                        chatObj.receiver = coll.doctorId ? coll.doctorId : null;

                        let fbchatobj = fbadmin.database().ref('/Consultations/' + coll._id);
                        let newobj = {};
                        newobj[chatObj.from + "_" + chatObj.msgType + "_ " + chatObj.timeStamp] = chatObj;
                        fbchatobj.update(newobj);

                        Objlist.push(chatObj);

                        resolve(chatObj);

                    }, reject => {
                        console.log("reject", reject);
                        resolve(reject);
                    });



                } else {

                    if (commentText.msgType == "text") {
                        chatObj.data = commentText.data.text;
                        chatObj.from = 'Doctor';
                        chatObj.receiver = coll.objectId;
                    } else if (commentText.msgType == "userText") {
                        chatObj.data = commentText.data.text;
                        chatObj.from = 'Patient';
                        chatObj.receiver = coll.doctorId ? coll.doctorId : null;
                    } else if (commentText.msgType == "alert") {
                        chatObj.data = commentText.data.text;
                        chatObj.from = 'Bot';
                        chatObj.sender = "Doxtro Assistant";
                        //chatObj.receiver = coll.objectId;
                    } else if (commentText.msgType == "profile") {
                        chatObj.from = 'Bot';
                        chatObj.sender = "Doxtro Assistant";
                        //chatObj.receiver = coll.objectId;
                        chatObj.data = {
                            _id: commentText.data.doc_id,
                            firstName: commentText.data.doc_name,
                            experience: (commentText.data.doc_spl_exp).split("\n")[0] ? (commentText.data.doc_spl_exp).split("\n")[0] : '',
                            profilePic: commentText.data.doc_pic ? commentText.data.doc_pic : '',
                        };

                        try {
                            chatObj.data.qualification = JSON.parse(commentText.data.doc_qual);
                        } catch (e) { console.log("failed to parse obj"); }
                    } else if (commentText.msgType == "payment") {
                        chatObj.from = 'Bot';
                        chatObj.sender = "Doxtro Assistant";
                        chatObj.data = {
                            consultingFee: parseInt(commentText.data.amount),
                            isPreApplied: false,
                            totalPayable: parseInt(commentText.data.amount),
                            //status : data.status
                        };
                    } else if (commentText.msgType == "suggestion") {
                        chatObj.from = 'Bot';
                        chatObj.sender = "Doxtro Assistant";
                        chatObj.data = [];
                        let list = commentText.data.list;
                        for (let i = 0; i < list.length; i++) {
                            let obj = {
                                _id: list[i].doc_id,
                                firstName: list[i].doc_name,
                                experience: (list[i].doc_spl_exp).split("\n")[1] ? (list[i].doc_spl_exp).split("\n")[1] : '',
                                profilePic: list[i].doc_pic ? list[i].doc_pic : '',
                                category: (list[i].doc_spl_exp).split("\n")[0] ? [(list[i].doc_spl_exp).split("\n")[0]] : []
                            };

                            try {
                                obj.qualification = JSON.parse(list[i].doc_qual);
                            } catch (e) { console.log("failed to parse obj"); }
                            chatObj.data.push(obj);
                        }
                    } else if (commentText.msgType == "alertForDoctor") {
                        chatObj.data = commentText.data.text;
                        chatObj.from = 'Bot';
                        chatObj.sender = "Doxtro Assistant";
                    } else if (commentText.msgType == "prescription") {
                        //get prescription from db
                        chatObj.from = 'Doctor';
                        chatObj.receiver = coll.objectId;
                        chatObj.data = {};
                        chatObj.data.medication = [{
                            name: commentText.data.med_name,
                            duration: isNaN(parseInt(commentText.data.med_duration)) ? "0" : parseInt(commentText.data.med_duration)
                        }];
                        chatObj.data.prescriptionId = coll.prescriptionId;
                    }

                    //for firebase
                    let fbchatobj = fbadmin.database().ref('/Consultations_staging/' + coll._id);
                    let newobj = {};
                    newobj[chatObj.from + "_" + chatObj.msgType + "_ " + chatObj.timeStamp] = chatObj;
                    fbchatobj.update(newobj);
                    Objlist.push(chatObj);

                    resolve(chatObj);

                }
            } else {
                resolve(null);
            }

        } catch (e) {
            console.log("err", e.message);
            console.log("failed to parse for", commentObj._id);
            resolve(e);
        } //try to parse commentText

    });
}