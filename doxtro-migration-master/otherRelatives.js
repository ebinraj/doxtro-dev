/**
 * This script is used migrate relatives(other than "Myself") records from collection "patient" to "relatives".
 */

//let relativesArr = ["Parent", "Sibling", "Spouse", "Child", "Other"(12), "In-law"(8), "Grand parent"(3), "Grandparent"(13), "Others"(171)]
//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
//We need to work with "MongoClient" interface in order to connect to a mongodb server.
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.
//var url = 'mongodb://52.202.0.43:27017/Doxtro-staging';
const url = 'mongodb://localhost:27017/doxtro1';
const newUrl = 'mongodb://localhost:27017/doxtro-prod';
var newDB;
MongoClient.connect(newUrl, function (err, db) {
    newDB = db;
});
// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const patient = db.collection('patient');
        const ObjectID = mongodb.ObjectID;
        let relation = 'Sibling';
        var filePath = path.join(__dirname, 'records/' + relation + '_duplicates.json');
        let aggregateArray = [
            { $match: { relation: relation } },
            { $group: { _id: "$objectId", relatives: { $push: "$$ROOT" } } }
        ]
        patient.aggregate(aggregateArray).toArray(function (err, data) {

            if (data.length != 0) {
                //console.log(data.length + " records found");
                let count = 0, list = [], c = 1, duplicateList = [];
                addData();

                function addData() {
                    let relativeRecords = data[count].relatives;

                    for (let i = 0; i < relativeRecords.length; i++) {
                        let tempStr = ObjectID(relativeRecords[i].objectId) + relativeRecords[i].name + relativeRecords[i].age + relativeRecords[i].gender;
                        let isExist = false, existsIndex;
                        for (let j = 0; j < list.length; j++) {
                            let compareStr = list[j].patientId + list[j].name + list[j].age + list[j].gender;
                            if (compareStr.localeCompare(tempStr) == 0) {
                                isExist = true;
                                existsIndex = j;
                                break;
                            }
                        }
                        if (isExist == false) {
                            let isFemale = relativeRecords[i].gender == 'Male' ? false : true;
                            let userRecord = {
                                _id: relativeRecords[i]._id,
                                idList: [],
                                patientId: ObjectID(relativeRecords[i].objectId),
                                createdAt: relativeRecords[i].createdAt,
                                updatedAt: relativeRecords[i].updatedAt,
                                deleted: relativeRecords[i].logicalDelete == false ? false : true,
                                __v: 0,
                                name: relativeRecords[i].name,
                                age: relativeRecords[i].age,
                                gender: relativeRecords[i].gender
                            };
                            if (userRecord.deleted == true)
                                userRecord.deletedAt = userRecord.updatedAt;
                            switch (relation) {
                                case 'Parent': userRecord.relation = isFemale ? 'Mom' : 'Dad';
                                    break;
                                case 'Sibling': userRecord.relation = isFemale ? 'Sister' : 'Brother';
                                    break;
                                case 'Spouse': userRecord.relation = 'Spouse'
                                    break;
                                case 'Child': userRecord.relation = isFemale ? 'Daughter' : 'Son';
                                    break;
                                case 'Other': userRecord.relation = 'Others'
                                    break;
                                case 'In-law': userRecord.relation = 'Others'
                                    break;
                                case 'Grand parent': userRecord.relation = 'Others'
                                    break;
                                case 'Grandparent': userRecord.relation = 'Others'
                                    break;
                                case 'Others': userRecord.relation = 'Others'
                                    break;
                            }
                            list.push(userRecord);
                        } else {
                            //record already exists
                            list[existsIndex].idList.push(relativeRecords[i]._id);
                            relativeRecords[i].index = c;
                            c++;
                            duplicateList.push(relativeRecords[i]);
                        }

                    }//end of for loop
                    count++;
                    if (count < data.length) {
                        addData();
                    } else {
                        //console.log("list", list)
                        newDB.collection('relatives').insertMany(list, function (err, data) {
                            if (data.insertedCount) {
                                console.log(data.insertedCount + " records created");
                            } else {
                                console.log("No data created");
                                //Close connection
                                db.close();
                                newDB.close();
                            }
                        })
                        //create duplicate records list for log
                        fs.appendFile(filePath, JSON.stringify(duplicateList), function (err) {
                            if (err) throw err;
                            console.log('Saved!');
                        });


                    }
                }
            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })
    }
});



