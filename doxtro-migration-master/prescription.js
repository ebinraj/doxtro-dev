//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
//We need to work with "MongoClient" interface in order to connect to a mongodb server.
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.
//var url = 'mongodb://52.202.0.43:27017/Doxtro-staging';
const url = 'mongodb://localhost:27017/doxtro1';
const newUrl = 'mongodb://localhost:27017/doxtro-prod';
var newDB;
MongoClient.connect(newUrl, function (err, db) {
    newDB = db;
});
// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const prescription = db.collection('prescription');
        const ObjectID = mongodb.ObjectID;
        prescription.find({}).toArray(function (err, data) {
            if (data.length != 0) {
                console.log(data.length + " records found");
                //create new prescription record
                for (let i = 0; i < data.length; i++) {

                    let prescriptionRecord = data[i];
                    prescriptionRecord.consultationId = ObjectID(prescriptionRecord.objectId);
                    delete prescriptionRecord.objectId;
                    prescriptionRecord.deleted = prescriptionRecord.logicalDelete == false ? false : true;
                    delete prescriptionRecord.logicalDelete;
                    delete prescriptionRecord.tenantId;
                    prescriptionRecord.type = "prescription";
                    prescriptionRecord.tag = "latest";
                    prescriptionRecord.diagnostic = [];
                    prescriptionRecord.__v = 0;

                    if(prescriptionRecord.deleted == true)
                        prescriptionRecord.deletedAt = prescriptionRecord.updatedAt

                    if (prescriptionRecord.medication && prescriptionRecord.medication.length > 0) {
                        for (let j = 0; j < prescriptionRecord.medication.length; j++) {
                            let medication = prescriptionRecord.medication[j];
                            medication._id = new ObjectID();
                            //check foodWarning
                            if (medication.foodWarning) {
                                if ((medication.foodWarning).toLowerCase() == 'before food') {
                                    medication.foodWarning = "Before meal"
                                } else if ((medication.foodWarning).toLowerCase() == 'after food') {
                                    medication.foodWarning = "After meal"
                                }
                            }
                            //check doseTime

                            if (medication.doseTime && (medication.doseTime).length > 0) {
                                let dt = medication.doseTime;
                                let doseTimeArr = [];
                                if (dt.indexOf("Morning") != -1 || dt.indexOf("morning") != -1)
                                    doseTimeArr[0] = "Morning"
                                else
                                    doseTimeArr[0] = ""

                                if (dt.indexOf("Afternoon") != -1 || dt.indexOf("afternoon") != -1)
                                    doseTimeArr[1] = "Afternoon"
                                else
                                    doseTimeArr[1] = ""

                                if (dt.indexOf("Night") != -1 || dt.indexOf("night") != -1)
                                    doseTimeArr[2] = "Night"
                                else
                                    doseTimeArr[2] = ""

                                medication.doseTime = doseTimeArr;
                            }

                            //check duration and duration unit
                            //let allowedValues = ["days", "week", "Month"];
                            if (medication.duration && medication.duration != "") {
                                let medi = medication.duration;
                                medication.duration = parseInt(medi);
                                if (medi.includes('days') || medi.includes('day')) {
                                    medication.durationUnit = "Days";
                                } else
                                if (medi.includes('month') || medi.includes('months')) {
                                    medication.durationUnit = "Months";
                                } else
                                if (medi.includes('week') || medi.includes('weeks')) {
                                        medication.durationUnit = "Weeks";
                                }
                            }
                        }//medication for loop ends here
                    }


                }
                //insert into new collection
                newDB.collection('prescription').insertMany(data, function (err, data) {
                    if (data.insertedCount) {
                        console.log(data.insertedCount + " records created");
                    } else {
                        console.log("No data created");
                        //Close connection
                        db.close();
                        newDB.close();
                    }
                })
            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
