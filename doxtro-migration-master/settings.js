const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
const url = 'mongodb://localhost:27017/doxtrophase1';

const newUrl = 'mongodb://localhost:27017/doxtro2dev';
var newDB;
MongoClient.connect(newUrl, function (err, db) {
    newDB = db;
});

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const settings = db.collection('settings');
        const ObjectID = mongodb.ObjectID;
        let aggregateArray = [
            { $match: { logicalDelete: false, category: { $in: ["Payment", "Doctor_Fee", "Commission"] } } },
            { $group: { _id: "$subCategory", charges: { $push: "$$ROOT" } } },
        ]
        settings.aggregate(aggregateArray).toArray(function (err, data) {
            if (data.length != 0) {
                //create json file
                let list = [];
                for (let i = 0; i < data.length; i++) {
                    let dt = {
                        category: 'charges',
                        subCategory: data[i]._id,
                        deleted: false,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    }
                    let charges = {}
                    for (let j = 0; j < data[i].charges.length; j++) {

                        switch (data[i].charges[j].category) {
                            case 'Payment': charges.consultationFee = data[i].charges[j].entity
                                break;
                            case 'Doctor_Fee': charges.doctorFee = data[i].charges[j].entity
                                break;
                            case 'Commission': charges.serviceCharge = data[i].charges[j].entity
                                break;
                        }
                    }
                    dt.charges = charges;
                    list.push(dt);
                }
                newDB.collection('settings').insertMany(list, function (err, data) {
                    if (data.insertedCount) {
                        console.log(data.insertedCount + " records created");
                    } else {
                        console.log("No data created");
                        //Close connection
                        db.close();
                        newDB.close();
                    }
                })
            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
