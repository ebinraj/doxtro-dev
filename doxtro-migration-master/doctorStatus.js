/**
 * This script is used migrate doctor records from collection user, businesspartner, address, tokenDetails to "doctor"
 */
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

/*const url = 'mongodb://localhost:27017/Doxtro_Live_dump';
//new database url;
const newUrl = 'mongodb://localhost:27017/Dox';*/

const url = 'mongodb://localhost:27017/doxtro1';
//new database url;
const newUrl = 'mongodb://localhost:27017/doxtro-prod';

var newDB;
MongoClient.connect(newUrl, function (err, db) {
    if (err)
        console.log('Unable to connect to the mongoDB server. Error:', err);
    else
        newDB = db;
});

// Use connect method to connect to the Server of old database
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const user = db.collection('user');
        const ObjectID = mongodb.ObjectID;
        //degree list
        user.find({ tag: 'Doctor' }, { status: 1 }).toArray(function (err, data) {
            if (data.length != 0) {
                let count = 0, insertedCount = 0;
                getData();

                function getData() {
                    //insert into new collection
                    newDB.collection('doctor').update({ _id: data[count].businessPartnerId }, { $set: { status: data[count].status } }, function (err, dt) {
                        if (!err && dt) {
                            insertedCount++;
                        } else{
                            console.log("failed to update for " + data[count].businessPartnerId);
                        }
                        count++;
                        if (count < data.length) {
                            getData();
                        } else {
                            console.log(insertedCount + " records updated.");
                            //Close connection
                            db.close();
                            newDB.close();
                        }
                    })
                }

            } else {
                console.log("No doctor record found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
