//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
//We need to work with "MongoClient" interface in order to connect to a mongodb server.
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.

/*const url = 'mongodb://localhost:27017/Doxtro_Live_dump';
//new database url;
const newUrl = 'mongodb://localhost:27017/Dox';*/

const url = 'mongodb://localhost:27017/doxtro1';
//new database url;
const newUrl = 'mongodb://localhost:27017/doxtro-prod';

var newDB;
MongoClient.connect(newUrl, function(err, db) {
    if (err)
        console.log('Unable to connect to the mongoDB server. Error:', err);
    else
        newDB = db;
});
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const user = db.collection('user');
        const businesspartner = db.collection('businesspartner');
        const address = db.collection('address');
        const mediaObject = db.collection('mediaobject');
        const tokendetails = db.collection('tokendetails');
        const ObjectID = mongodb.ObjectID;
        user.find({ tag: 'Patient' }).limit(500).skip(1000).toArray(function(err, data) {
            if (data.length != 0) {
                console.log("patient found", data.length);
                let count = 0,
                    list = [];
                getData();

                function getData() {
                    let userRecord = {
                        _id: data[count].businessPartnerId,
                        tag: 'patient',
                        accountStatus: data[count].accountStatus,
                        createdAt: data[count].createdAt,
                        updatedAt: data[count].updatedAt,
                        accountActivationTime: data[count].accountActivationTime ? data[count].accountActivationTime : data[count].createdAt,
                        deleted: data[count].logicalDelete == false ? false : true,
                        userId: "patient#" + (count + 1)
                    }
                    if (userRecord.deleted == true)
                        userRecord.deletedAt = userRecord.updatedAt;
                    businesspartner.findOne({ _id: data[count].businessPartnerId }, function(err, dt) {
                        if (!err && dt) {
                            userRecord.emailid = dt.emailid;
                            userRecord.firstName = dt.givenName;
                            userRecord.age = dt.age;
                        }
                        address.findOne({ objectId: (data[count].businessPartnerId).toString() }, function(err, add) {
                            if (!err && add)
                                userRecord.mobile = add.mobile;
                            mediaObject.findOne({ "tag": "profilePic", objectId: data[count].businessPartnerId }, function(err, md) {
                                if (!err && md)
                                    userRecord.profilePic = md.profilePic;
                                tokendetails.findOne({ "businessPartnerId": data[count].businessPartnerId }, function(err, td) {
                                    if (!err && td) {
                                        userRecord.otp = td.token;
                                        userRecord.isExpired = td.isExpired;
                                    }
                                    list.push(userRecord);
                                    console.log("count", count);
                                    count++;
                                    if (count < data.length) {
                                        getData();
                                    } else {
                                        //insert into new collection
                                        newDB.collection('patient').insertMany(list, function(err, data) {
                                                if (data.insertedCount) {
                                                    console.log(data.insertedCount + " records created");
                                                } else {
                                                    console.log("No data created");
                                                    //Close connection
                                                    db.close();
                                                    newDB.close();
                                                }
                                            })
                                            //create json file
                                            /* var filePath = path.join(__dirname, 'records/patient.json');
                                         fs.writeFile(filePath, JSON.stringify(patientList), 'utf8', function (err) {
                                             if (err) {
                                                 return console.log(err);
                                             }
     
                                             console.log("The file was saved!");
                                         });*/
                                    }
                                });
                            });
                        });

                    });
                }
                //find data in bp and address
                //check the tag
                //save data accordingly
            } else {
                console.log("No data found");
                //Close connection
                db.close();
            }
        });

    }
});