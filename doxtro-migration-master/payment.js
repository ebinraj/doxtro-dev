//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
//We need to work with "MongoClient" interface in order to connect to a mongodb server.
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.
//var url = 'mongodb://52.202.0.43:27017/Doxtro-staging';
const url = 'mongodb://localhost:27017/doxtro1';
//new database url;
const newUrl = 'mongodb://localhost:27017/doxtro-prod';

var newDB;
MongoClient.connect(newUrl, function(err, db) {
    if (err)
        console.log('Unable to connect to the mongoDB server. Error:', err);
    else
        newDB = db;
});
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const payment = db.collection('payment');
        const consultation = newDB.collection('consultation');
        const ObjectID = mongodb.ObjectID;
        payment.find({}).toArray(function(err, data) {
            if (data.length != 0) {
                console.log("payment record found", data.length);
                let count = 0,
                    list = [];
                getData();

                function getData() {
                    let paymentObj = {
                        _id: data[count]._id,
                        status: data[count].status,
                        currency: "INR",
                        amount: parseInt(data[count].amount) * 100,
                        entity: "payment",
                        id: data[count].payment_id,
                        fee: parseInt(data[count].fees) * 100,
                        international: false,
                        method: "netbanking",
                        amount_refunded: 0,
                        refund_status: null,
                        createdAt: data[count].createdAt,
                        updatedAt: data[count].updatedAt,
                        captured : true,
                        gateway : 'instamojo',
                        consultationId : ObjectID(data[count].objectId)
                    }

                    consultation.findOne({ _id: ObjectID(data[count].objectId) }, function(err, ct) {
                        if (!err && ct) {
                            paymentObj.patientId = ObjectID(ct.patientId);
                            paymentObj.doctorId = ObjectID(ct.doctorId);
                        }

                        list.push(paymentObj);
                        count++;

                        if (count < data.length) {
                            getData();
                        } else {

                            //insert into new collection
                            newDB.collection('payment').insertMany(list, function(err, data) {
                                if (data.insertedCount) {
                                    console.log(data.insertedCount + " records created");
                                } else {
                                    console.log("No data created");
                                    //Close connection
                                    db.close();
                                    newDB.close();
                                }
                            });

                        }

                    }); //closing consultation find one

                }
                //find data in bp and address
                //check the tag
                //save data accordingly
            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});