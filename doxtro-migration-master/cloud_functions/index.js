const functions = require('firebase-functions');
const request = require('request-promise');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.pushNotificationsDev = functions.database.ref('/Consultations/{consultationId}/{id}').onWrite(event => {
    console.log("event triggered!!!");
    const record = event.data.val();
    //check the receiver of message
    console.log("checking for consultation", event.params.consultationId);
    //check record
    if (record) {
        //record found
        let message;
        switch (record.msgType) {
            case 'text': message = record.sender + " : " + record.data;
                break;
            case 'userText': message = record.sender + " : " + record.data;
                break;
            case 'image': message = record.sender + ' has sent an image';
                break;
            case 'docImage': message = record.sender + ' has sent an image';
                break;
            case 'prescription': message = record.sender + ' has sent a prescription';
                break;
            case 'diagnostic': message = record.sender + ' has sent a diagnostic';
                break;
            case 'healthRecords': message = record.sender + ' has shared health record';
                break;
            case 'requestCall': message = record.sender + ' has requested for a call';
                break;
        }
        if (message) {
            if (record.receiver) {
                //receiver exists
                console.log("receiver : ", record.receiver);
                const receiver = record.receiver;
                //get deviceIds for users
                const userRef = admin.database().ref("/Users/" + receiver);
                userRef.once("value", function (snapshot) {
                    if (snapshot.val()) {
                        //user record found
                        //check receiver is online/offline
                        console.log(receiver + ": " + snapshot.val().status);
                        if ((snapshot.val().status == "offline" || record.msgType == "requestCall") && snapshot.val().deviceId) {
                            //send push notification
                            let payload = {
                                notification: {
                                    title: 'Doxtro',
                                    body: message,
                                    sound : "push_notification_sound.mp3"
                                },
                                data: {
                                    type: 'chat',
                                    consultationId: event.params.consultationId
                                    //data : JSON.stringify({consultationId : event.params.consultationId})
                                }
                            }

                            let options = {
                                priority: "high",
                                timeToLive: 60 * 60 * 24,
                                mutableContent: true,
                                contentAvailable: true
                            };
                            console.log("Sending push notification to : ", receiver + " for msgType : ", record.msgType);
                            admin.messaging().sendToDevice(snapshot.val().deviceId, payload, options).then(response => {
                                console.log("response of push Notification : ", response, response.results);
                            }).catch(error => {
                                console.log("Error in sending push Notification : ", error);
                            })
                        } else {
                            console.log("receiver is online or deviceId not found for receiver : ", receiver);
                        }
                    } else {
                        //No user record found
                        console.log("user record not found for receiver : ", receiver);
                    }
                })

            } else {
                //No receiver found
                console.log("No receiver found for msgType : ", record.msgType)
            }
        } else {
            //push notification not allowed to this msgType
            console.log("Not allowed to send push notification to msgType : ", record.msgType);
        }
    } else {
        //no record found
        console.log("No record found for consultationId : ", event.params.consultationId);
    }
})

exports.pushNotificationsStaging = functions.database.ref('/Consultations_staging/{consultationId}/{id}').onWrite(event => {
    console.log("event triggered!!!");
    const record = event.data.val();
    //check the receiver of message
    console.log("checking for consultation", event.params.consultationId);
    //check record
    if (record) {
        //record found
        let message;
        switch (record.msgType) {
            case 'text': message = record.sender + " : " + record.data;
                break;
            case 'userText': message = record.sender + " : " + record.data;
                break;
            case 'image': message = record.sender + ' has sent an image';
                break;
            case 'docImage': message = record.sender + ' has sent an image';
                break;
            case 'prescription': message = record.sender + ' has sent a prescription';
                break;
            case 'diagnostic': message = record.sender + ' has sent a diagnostic';
                break;
            case 'healthRecords': message = record.sender + ' has shared health record';
                break;
            case 'requestCall': message = record.sender + ' has requested for a call';
                break;
        }
        if (message) {
            if (record.receiver) {
                //receiver exists
                console.log("receiver : ", record.receiver);
                const receiver = record.receiver;
                //get deviceIds for users
                const userRef = admin.database().ref("/Users/" + receiver);
                userRef.once("value", function (snapshot) {
                    if (snapshot.val()) {
                        //user record found
                        //check receiver is online/offline
                        console.log(receiver + ": " + snapshot.val().status);
                        if ((snapshot.val().status == "offline" || record.msgType == "requestCall") && snapshot.val().deviceId) {
                            //send push notification
                            let payload = {
                                notification: {
                                    title: 'Doxtro',
                                    body: message,
                                    sound : "push_notification_sound.mp3"
                                },
                                data: {
                                    type: 'chat',
                                    consultationId: event.params.consultationId
                                    //data : JSON.stringify({consultationId : event.params.consultationId})
                                }
                            }

                            let options = {
                                priority: "high",
                                timeToLive: 60 * 60 * 24,
                                mutableContent: true,
                                contentAvailable: true
                            };
                            console.log("Sending push notification to : ", receiver + " for msgType : ", record.msgType);
                            admin.messaging().sendToDevice(snapshot.val().deviceId, payload, options).then(response => {
                                console.log("response of push Notification : ", response);
                            }).catch(error => {
                                console.log("Error in sending push Notification : ", error);
                            })
                        } else {
                            console.log("receiver is online or deviceId not found for receiver : ", receiver);
                        }
                    } else {
                        //No user record found
                        console.log("user record not found for receiver : ", receiver);
                    }
                })

            } else {
                //No receiver found
                console.log("No receiver found for msgType : ", record.msgType)
            }
        } else {
            //push notification not allowed to this msgType
            console.log("Not allowed to send push notification to msgType : ", record.msgType);
        }
    } else {
        //no record found
        console.log("No record found for consultationId : ", event.params.consultationId);
    }
})
