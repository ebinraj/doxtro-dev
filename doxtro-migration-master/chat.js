//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
var fbadmin = require("firebase-admin");

let serviceAccount = require("./config/serviceAccountKey.json");

fbadmin.initializeApp({
    credential: fbadmin.credential.cert(serviceAccount),
    databaseURL: "https://doxtrodevelopment.firebaseio.com"
});

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running
const url = 'mongodb://localhost:27017/doxtrophase1';
//const url = 'mongodb://localhost:27017/Doxtro_Live_dump';
const newUrl = 'mongodb://localhost:27017/doxtro2dev';
var newDB;
MongoClient.connect(newUrl, function(err, db) {
    newDB = db;
});
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const consultation = db.collection('consultation');
        const usercomment = db.collection('usercomment');
        const prescription = db.collection('prescription');
        const ObjectID = mongodb.ObjectID;
        consultation.find({ _id: ObjectID("581209d9b87aca8b12a2f487") }, { doctorId: 1, prescriptionId: 1, objectId: 1 }).toArray(function(err, data) {
            if (data.length != 0) {
                let count = 0;
                list = [];
                getData();

                function getData() {
                    let coll = data[count];
                    let elements = [];
                    let docName = "Doctor";
                    usercomment.find({ audience: { $in: [(coll._id).toString()] }, logicalDelete: false }).toArray(function(err, chat) {

                            if (!err && chat.length > 0) {
                                for (let i = 0; i < chat.length; i++) {
                                    let commentObj = chat[i];
                                    let commentText, chatObj = {};
                                    try {
                                        commentText = JSON.parse(commentObj.commentText);
                                        if (commentText.msgType != "feedback") {
                                            chatObj = {
                                                _id: commentObj.author,
                                                sender: commentText.name,
                                                status: "read",
                                                msgType: commentText.msgType,
                                                //data: commentText.data,
                                                timeStamp: new Date(commentObj.createdAt).getTime()
                                            };

                                            if (commentText.msgType == "text") {
                                                chatObj.data = commentText.data.text;
                                                chatObj.from = 'Doctor';
                                                chatObj.receiver = coll.objectId;
                                            } else if (commentText.msgType == "userText") {
                                                chatObj.data = commentText.data.text;
                                                chatObj.from = 'Patient';
                                                chatObj.receiver = coll.doctorId;
                                            } else if (commentText.msgType == "alert") {
                                                chatObj.data = commentText.data.text;
                                                chatObj.from = 'Bot';
                                                chatObj.sender = "Doxtro Assistant"
                                                    //chatObj.receiver = coll.objectId;
                                            } else if (commentText.msgType == "image") {
                                                chatObj.data = commentText.data.url;
                                                chatObj.from = 'Patient';
                                                chatObj.receiver = coll.doctorId;
                                            } else if (commentText.msgType == "profile") {
                                                chatObj.from = 'Bot';
                                                chatObj.sender = "Doxtro Assistant";
                                                //chatObj.receiver = coll.objectId;
                                                chatObj.data = {
                                                    _id: commentText.data.doc_id,
                                                    firstName: commentText.data.doc_name,
                                                    experience: (commentText.data.doc_spl_exp).split("\n")[0] ? (commentText.data.doc_spl_exp).split("\n")[0] : '',
                                                    profilePic: commentText.data.doc_pic ? commentText.data.doc_pic : '',
                                                };

                                                try {
                                                    chatObj.data.qualification = JSON.parse(commentText.data.doc_qual);
                                                } catch (e) { console.log("failed to parse obj"); }
                                            } else if (commentText.msgType == "payment") {
                                                chatObj.from = 'Bot';
                                                chatObj.sender = "Doxtro Assistant";
                                                chatObj.data = {
                                                    consultingFee: parseInt(commentText.data.amount),
                                                    isPreApplied: false,
                                                    totalPayable: parseInt(commentText.data.amount),
                                                    //status : data.status
                                                }
                                            } else if (commentText.msgType == "suggestion") {
                                                chatObj.from = 'Bot';
                                                chatObj.sender = "Doxtro Assistant";
                                                chatObj.data = [];
                                                let list = commentText.data.list;
                                                for (let i = 0; i < list.length; i++) {
                                                    let obj = {
                                                        _id: list[i].doc_id,
                                                        firstName: list[i].doc_name,
                                                        experience: (list[i].doc_spl_exp).split("\n")[1] ? (list[i].doc_spl_exp).split("\n")[1] : '',
                                                        profilePic: list[i].doc_pic ? list[i].doc_pic : '',
                                                        category: (list[i].doc_spl_exp).split("\n")[0] ? [(list[i].doc_spl_exp).split("\n")[0]] : []
                                                    }

                                                    try {
                                                        obj.qualification = JSON.parse(list[i].doc_qual);
                                                    } catch (e) { console.log("failed to parse obj"); }
                                                    chatObj.data.push(obj);
                                                }
                                            } else if (commentText.msgType == "alertForDoctor") {
                                                chatObj.data = commentText.data.text;
                                                chatObj.from = 'Bot';
                                                chatObj.sender = "Doxtro Assistant";
                                            } else if (commentText.msgType == "prescription") {
                                                //get prescription from db
                                                chatObj.from = 'Doctor';
                                                chatObj.receiver = coll.objectId;
                                                chatObj.data = {};
                                                chatObj.data.medication = [{
                                                    name: commentText.data.med_name,
                                                    duration: parseInt(commentText.data.med_duration)
                                                }];
                                                chatObj.data.prescriptionId = coll.prescriptionId;
                                            }

                                            //for firebase
                                            let fbchatobj = fbadmin.database().ref('/Consultationsold/581209d9b87aca8b12a2f487');
                                            let newobj = {};
                                            newobj[chatObj.from + "_" + chatObj.timeStamp] = chatObj;
                                            fbchatobj.update(newobj);


                                            list.push(chatObj);
                                        }


                                    } catch (e) {
                                        console.log("err", e.message);
                                        console.log("failed to parse for", commentObj._id);
                                    } //try to parse commentText

                                }
                            } //if chat

                            count++;
                            if (count < data.length) {
                                getData();
                            } else {
                                //create json file
                                var filePath = path.join(__dirname, 'records/chat.json');
                                fs.writeFile(filePath, JSON.stringify(list), 'utf8', function(err) {
                                    if (err) {
                                        return console.log(err);
                                    }

                                    console.log("The file was saved!");
                                });
                            }
                        }) //end of usercomment
                }

            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});


/*{
    _id : "_id of sender",
    sender : "name_of_Sender",
    receiver : "_id_of_receiver",
    from : "Patient/Doctor/Admin/Bot",
    status : "Sent/Read/Delivered",
    msgType : "userIntro/alert/suggestion/payment/userText/text/image/...",
    data : "required message or data(json_object)  // This object may change according to message type (removed text key to avoid unnecessary nesting),
    timeStamp : "datetime"
}
 */