/**
 * This script is used to add category field in document of collection "doctor".
 * It should be executed after successful migration of "doctor" collection.
 * No old databse connection required.
 */
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
const newUrl = 'mongodb://localhost:27017/doxtro-prod';

// Use connect method to connect to the Server of new database
MongoClient.connect(newUrl, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', newUrl);
        const doctor = db.collection('doctor');
        const specialization = db.collection('specialization');
        const ObjectID = mongodb.ObjectID;

        doctor.find({}, { specialization: 1 }).toArray(function (err, data) {
            
            if (data.length != 0) {

                console.log(data.length + "doctor records found.");
                let count = 0; list = [], totalUpdatedCount = 0;
                getData();

                function getData() {
                    specialization.find({ specialization: { $in: data[count].specialization } }, { category: 1 }).toArray(function (err, categories) {
                        if (!err && categories && categories.length > 0) {

                           //create array of category
                            let ct = [];
                            for (let i = 0; i < categories.length; i++){
                                if(ct.indexOf(categories[i].category) == -1)
                                     ct.push(categories[i].category);
                            }
                               

                            //update doctor record for categories
                            doctor.update({ _id: data[count]._id }, { $set: { category: ct } }, function (err, dt) {
                                
                                //log updated record count
                                if(!err && dt)
                                    totalUpdatedCount += dt.result.nModified;
                                
                                count++;
                                if (count < data.length) {
                                    getData();
                                } else {
                                    console.log(totalUpdatedCount + " doctor records updated.");
                                    //Close connection
                                    db.close();
                                }
                            })

                        } else {
                            //No category found
                            count++;
                            if (count < data.length) {
                                getData();
                            } else {
                                console.log(totalUpdatedCount + " doctor records updated");
                                //Close connection
                                db.close();
                            }
                        }
                    })
                }
            } else {
                console.log("No doctor records found");
                //Close connection
                db.close();
            }
        })

    }
});
