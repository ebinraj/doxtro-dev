/**
 * This script is used to replace duplicate relativesId from consultation records.
 * It should be executed after successful migration of "consultation" and "relatives" collection.
 * No old databse connection required.
 */

const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

const newUrl = 'mongodb://localhost:27017/doxtro-prod';

// Use connect method to connect to the Server
MongoClient.connect(newUrl, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', newUrl);
        const consultation = db.collection('consultation');
        const relatives = db.collection('relatives');
        const ObjectID = mongodb.ObjectID;

        //find relatives with duplicate ids
        relatives.find({ idList: { $exists: true, $ne: [] } }, { idList: 1 }).toArray(function (err, data) {
            
            if (data.length != 0) {
                console.log(data.length, "relative records found");
                let logList = [], count = 0, consultationCount = 0;
                getData();

                function getData() {
                    let relatives = data[count];
                    //find consultation records for the given relatives
                    consultation.find({ relativesId: { $in: relatives.idList } }, { relativesId: 1 }).toArray(function (err, list) {
                        if (!err && list.length > 0) {
                            //replace duplicate relativesId 
                            consultation.update({ relativesId: { $in: relatives.idList } }, { $set: { relativesId: relatives._id } }, { multi: true }, function (err, dt) {
                                if (!err && dt) {
                                    consultationCount += dt.result.nModified;
                                    console.log(count + ". Success" + relatives._id);
                                } else {
                                    console.log(count + ". Failed" + relatives._id);
                                }
                                logList.push({ list: list, newId: relatives._id });
                                count++;
                                if (count < data.length) {
                                    getData();
                                } else {
                                    console.log(consultationCount + " records updated");
                                    //create json file to maintain log
                                    var filePath = path.join(__dirname, 'records/consultations_' + relatives._id +'.json');
                                    fs.writeFile(filePath, JSON.stringify(logList), 'utf8', function (err) {
                                        if (err) 
                                            return console.log(err);
                                        console.log("The file was saved!");
                                    });
                                    db.close();
                                }

                            })
                        } else {
                            //no consultation found
                            console.log(count + ". No consultation found" + relatives._id);
                            count++;
                            if (count < data.length) {
                                getData();
                            } else {
                                console.log(consultationCount + " records updated");
                                var filePath = path.join(__dirname, 'records/consultations_' + relatives._id +'.json');
                                fs.writeFile(filePath, JSON.stringify(logList), 'utf8', function (err) {
                                    if (err) 
                                            return console.log(err);
                                        console.log("The file was saved!");
                                });
                                 //Close connection
                                db.close();
                            }
                        }

                    })

                }

            } else {
                console.log("No data found");
                //Close connection
                db.close();
            }
        })

    }
});
