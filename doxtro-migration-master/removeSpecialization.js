/**
 * This script is used to remove specific specialization
 * It should be executed after successful migration of "doctor" collection.
 * No old databse connection required.
 */
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
const newUrl = 'mongodb://localhost:27017/Dox';

// Use connect method to connect to the Server of new database
MongoClient.connect(newUrl, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', newUrl);
        const doctor = db.collection('doctor');
        const ObjectID = mongodb.ObjectID;
        const removedCat = ["Plastic Surgeon", "Immunologist", "Anesthesiologist", "Geriatrician"];
        const removedArr = ["Aesthetic Plastic Surgeon", "Craniofacial Surgeon", "Laser Surgeon", "Pediatric Plastic Surgeon", "Reconstructive Microsurgeon", "Reconstructive Surgeon",
            "Allergy Specialist", "Immunologist", "Critical Care Specialist", "Labour Induction", "Pain Management",
            "Geriatric Dentist", "Geriatric Medicine Specialist", "Geriatric Neurologist", "Geriatric OT", "Geriatric Phsyiotherapist",
            "Geriatric Podiatrist", "Geriatrician", "Geriatrics Veterinarian"];

        doctor.find({ specialization: { $in: removedArr } }, { specialization: 1 }).toArray(function (err, data) {

            if (data.length != 0) {

                console.log(data.length + "doctor records found.");
                let count = 0; list = [], totalUpdatedCount = 0;
                getData();

                function getData() {
                    let specialization = data[count].specialization;
                    let newSpecializationArr = [];
                    //create array of category
                    let ct = [];
                    for (let i = 0; i < specialization.length; i++) {
                        let spe = specialization[i];
                        let idx = removedArr.indexOf(spe);
                        if (idx == -1) {
                            //add element
                            newSpecializationArr.push(spe);
                        }
                    }

                    //update doctor record for categories
                    doctor.update({ _id: data[count]._id }, { $set: { specialization: newSpecializationArr } }, function (err, dt) {

                        //log updated record count
                        if (!err && dt) {
                            console.log("updated for ", data[count]._id);
                            totalUpdatedCount += dt.result.nModified;
                        } else {
                            console.log("failed to updated for ", data[count]._id);
                        }
                        console.log("count", count);
                        count++;
                        if (count < data.length) {
                            getData();
                        } else {
                            console.log(totalUpdatedCount + " doctor records updated.");
                            //Close connection
                            db.close();
                        }
                    })

                }
            } else {
                console.log("No doctor records found");
                //Close connection
                db.close();
            }
        })

    }
});
