const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.
const newUrl = 'mongodb://doxtrodev.above-inc.net:27017/doxtro2dev';
var filePath = path.join(__dirname, 'records/myself.json');
// Use connect method to connect to the Server of new database
MongoClient.connect(newUrl, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', newUrl);
        const patient = db.collection('patient');
        const relatives = db.collection('relatives');
        const ObjectID = mongodb.ObjectID;
        let idList = [];
    
    patient.find().sort({ createdAt: -1 }).toArray(function (err, data) {

            if (data.length != 0) {

                console.log(data.length + "patient records found.");
                let count = 0; list = [], totalUpdatedCount = 0, notFoundCount = 0;
                getData();

                function getData() {
                    relatives.findOne({ relation: "Myself", patientId: data[count]._id }, function (err, relative) {
                        if (err) {
                            //err;
                            console.log(count + ". " + data[count]._id + " :error");
                            count++;

                            if (count < data.length) {
                                getData();
                            } else {
                                console.log(totalUpdatedCount + " relatives records updated");
                                fs.writeFile(filePath, JSON.stringify(list), 'utf8', function (err) {
                                    if (err)
                                        return console.log(err);
                                    console.log("The file was saved!");
                                });
                                //Close connection
                                db.close();
                            }
                        } else if (!relative) {

                            //create array of category
                            let relativesDetails = {
                                //name: data[count].firstName,
                                //age: data[count].age,
                                //gender: data[count].gender,
                                patientId: data[count]._id,
                                relation: 'Myself',
                                createdAt: data[count].createdAt,
                                updatedAt: data[count].updatedAt,
                                deleted: false,
                                __v: 0
                            };
                            if (data[count].firstName)
                                relativesDetails.name = data[count].firstName;
                            if (data[count].age)
                                relativesDetails.age = data[count].age;
                            if (data[count].gender)
                                relativesDetails.age = data[count].gender;
                            //update doctor record for categories
                            relatives.insert(relativesDetails, function (err, dt) {
                                console.log(count + ". " + data[count]._id + " :myself added");
                                //log updated record count
                                if (!err && dt) {
                                    totalUpdatedCount++;
                                    list.push(data[count]._id);
                                }

                                count++;
                                if (count < data.length) {
                                    getData();
                                } else {
                                    console.log(totalUpdatedCount + " relatives records added.");
                                    fs.writeFile(filePath, JSON.stringify(list), 'utf8', function (err) {
                                        if (err)
                                            return console.log(err);
                                        console.log("The file was saved!");
                                    });
                                    //Close connection
                                    db.close();
                                }
                            })

                        } else {
                            //relation found
                            console.log(count + ". " + data[count]._id + " :already exists");
                            count++;
                            if (count < data.length) {
                                getData();
                            } else {

                                console.log(totalUpdatedCount + " relatives records added.");
                                fs.writeFile(filePath, JSON.stringify(list), 'utf8', function (err) {
                                    if (err)
                                        return console.log(err);
                                    console.log("The file was saved!");
                                });
                                //Close connection
                                db.close();
                            }
                        }
                    })
                }
            } else {
                console.log("No doctor records found");
                //Close connection
                db.close();
            }
        })
    }
});
