const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;
//const url = 'mongodb://localhost:27017/doxtro-prod';
const url = 'mongodb://doxtrodev.above-inc.net:27017/Doxtro-Development';
let consultation, settings, doctor, ObjectID;
//let limit = 2, skip = 0;
let totalUpdatedCount = 0;
let count = 0;
// Use connect method to connect to the Server of old database
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        consultation = db.collection('consultation');
        settings = db.collection('settings');
        doctor = db.collection('doctor');
        ObjectID = mongodb.ObjectID;

        new Promise((resolve, reject) => {
            consultation.find({doctorId : {$exists : true}})/*.limit(limit).skip(skip)*/.toArray(function (err, data) {
                if (data && data.length != 0) {
                    console.log(data.length + " consultation records found.");
                    let promises = [], results = [], errors = [];
                    data.forEach(function (dt) {
                        promises.push(
                            findDoctor(dt).then(result => {
                                console.log("count", count);
                                count++;
                                results.push(result);
                            }).catch(error => {
                                console.log("count", count);
                                count++;
                                console.log("Err", error);
                                errors.push(error.msg);
                            })
                        )
                    })
                    Promise.all(promises).then(() => {
                        resolve({ data: results, error: errors, message: "Created successfully." });
                    }).catch(error => {
                        reject(error);
                    });
                } else {
                    console.log("No consultation record found");
                    reject({ status: 404 })
                }
            })
        }).then(result => {
            console.log("done!!!", totalUpdatedCount);
        }).catch(error => {
            console.log("err", error);
            db.close();
        })


        //doctor find
        function findDoctor(dt) {
            return new Promise((resolve, reject) => {
                console.log("checking for doctorId : ", (dt.doctorId).toString())
                let docs = {};
                return doctor.findOne({_id: dt.doctorId }, function (err, doc) {
                    if (!err && doc){
                        docs = doc;
                    }else{
                        console.log("No doctor record found : ", dt.doctorId);
                    }
                    return calculateFee(docs, dt.consultationType, dt.specializationCategory).then(result => {
                        console.log("result", result);
                        return consultation.update({ _id: dt._id }, { $set: { doctorCharges: result } }, function (err, res) {
                            if (!err && res) {
                                console.log("updated " + res.result.nModified + " records for : " + dt._id);
                                totalUpdatedCount += res.result.nModified;
                                resolve({ status: 200 });
                            } else {
                                console.log("failed to update for : ", dt._id);
                                //resolve({ status: 400 });
                                reject({failedCOnsultationId : dt._id});
                            }
                        })
                    }).catch(error => {
                        console.log("error in findDoctor", error);
                        //reject(error);
                        //resolve({ status: 404 });
                        reject({failedCOnsultationId : dt._id});
                    })

                })
            })

        }

        //calculate fee
        function calculateFee(doctorDetails, consultationType, specializationCategory) {
            console.log(consultationType, specializationCategory);
            return new Promise((resolve, reject) => {
                let doctorCharges = { doctorFee: 0, serviceCharge: 0, finalDoctorFee: 0 }
                switch (consultationType) {
                    case 'Chat': doctorCharges.doctorFee = doctorDetails.doctorFee;
                        break;
                    case 'Audio': doctorCharges.doctorFee = doctorDetails.doctorAudioFee;
                        break;
                    case 'Planned_Chat': doctorCharges.doctorFee = doctorDetails.plannedChatFee;
                        break;
                    case 'Planned _Audio': doctorCharges.doctorFee = doctorDetails.plannedAudioFee;
                        break;
                }
                //check doctor fee
                if (doctorCharges.doctorFee > 0) {
                    doctorCharges.finalDoctorFee = doctorCharges.doctorFee;
                    resolve(doctorCharges);
                } else {
                    //find in settings
                    let query = {
                        category: "charges",
                        subCategory: specializationCategory,
                        consultationType: consultationType
                    }
                    return settings.findOne(query).then(result => {
                        if (!err && result && result.charges) {
                            //check doctor fee
                            if (result.charges.doctorFee > 0)
                                doctorCharges.doctorFee = result.charges.doctorFee;
                            else
                                doctorCharges.doctorFee = 100;
                            //check servicecharge
                            if (result.charges.serviceCharge != undefined)
                                doctorCharges.serviceCharge = result.charges.serviceCharge;
                            else
                                doctorCharges.serviceCharge = 3;
                            
                        }else{
                            doctorCharges.doctorFee = 100;
                            doctorCharges.serviceCharge = 3;
                        }
                        //calculate final doctor fee
                        query.serviceChargeVal = (doctorCharges.serviceCharge / 100) * doctorCharges.doctorFee;
                        query.serviceCharge = parseFloat(query.serviceChargeVal).toFixed(2);
                        query.intermediateVal = parseFloat(doctorCharges.doctorFee) - parseFloat(query.serviceCharge);
                        doctorCharges.finalDoctorFee = parseInt(query.intermediateVal ? query.intermediateVal : 0);
                        resolve(doctorCharges);
                    }).catch(error => {
                        console.log("error in finding doctor value", error);
                        resolve(doctorCharges);
                    })
                }

            })
        }
    }
});

