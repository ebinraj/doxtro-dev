const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

const url = 'mongodb://localhost:27017/doxtrophase1';

const newUrl = 'mongodb://localhost:27017/doxtro2dev';

/*const url = 'mongodb://doxtrodev.above-inc.net:27017/doxtrophase1';
//new database url;
const newUrl = 'mongodb://doxtrodev.above-inc.net:27017/doxtro2dev';*/
var newDB;
MongoClient.connect(newUrl, function (err, db) {
    newDB = db;
});

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const specialization = db.collection('specialization');
        const ObjectID = mongodb.ObjectID;
        specialization.find({}).toArray(function (err, data) {
            if (data.length != 0) {
                for (let i = 0; i < data.length; i++) {
                    data[i].__v = 0;
                    data[i].deleted = data[i].logicalDelete == false ? false : true;
                    delete data[i].logicalDelete;
                    switch (data[i].category) {

                        case "General Physician": data[i].description = "All common illnesses";
                            break;

                        case "Dermatologist": data[i].description = "Acne, Pimples, Rashes, hair fall, eczema, urticaria, itching, pigmentation, allergic reactions and other skin conditions"
                            break;

                        case "Nutritionist": data[i].description = "Weight Loss, Nutrient Deficiency, Diet Chart"
                            break;

                        case "Gynecologist": data[i].description = "Pregnancy, Infertility, irregular periods, vaginal itching, vaginal discharge, breast tenderness, breast pain / lump";
                            break;

                        case "Psychiatrist / Mental Health": data[i].description = "Depression, Anxiety, Stress, Bipolar disorder, schizophrenia, mood disorders, eating disorders, mania and phobia";
                            break;

                        case "Psychologist": data[i].description = "Stress related to work, family, relationships and society";
                            break;

                        case "Pediatrician": data[i].description = "All child and infant health problems"
                            break;

                        case "Sexologist": data[i].description = "Sexually transmitted diseases, Erectile Dysfunction, Premature Ejaculation";
                            break;


                        case "Diabetologist": data[i].description = "Diabetes"
                            break;

                        case "ENT": data[i].description = "Throat pain, ear pain, change in voice, difficulty in hearing, nose block, ear block, snoring";
                            break;

                        case "Eye Surgeon": data[i].description = "Blurred vision, decreased vision, head ache, watering of eyes, redness of eyes, burning sensation of eyes, dry eyes";
                            break;

                        case "Orthopedist": data[i].description = "Fracture, Joint pain, Bony pain, Lower back pain, Neck pain, Difficulty in movements of any limbs, Joint swelling";
                            break;

                        case "Dentist": data[i].description = "Tooth pain, tooth decay, tooth sensitivity, broken tooth";

                        case "Cardiologist": data[i].description = "Heart Diseases, Cardiac Surgery, Second opinion about other cardiac problems";
                            break;

                        case "Gastroenterologist": data[i].description = "Heart burn, constipation, diarrhea, abdominal bloating, blood in vomiting or blood in stools";
                            break;

                        case "Oncologists": data[i].description = "All cancer related queries";
                            break;

                        case "endocrinologists": data[i].description = "sudden weight gain/ weight loss, sweating, hyperpigmentation, thyroid disorder";
                            break;

                        case "Neurologist": data[i].description = "weakness, Tingling sensations, numbness, Stroke, Epilepsy/Seizures/Fits";
                            break;
                        default: data[i].description = "Lorem ipsum dolor sit amet, nam dicant deterruisset ex. Ex sea minim expetenda, solum posidonium in eam, vim eros erant dignissim ut.";
                            break;
                    }
                }
                newDB.collection('specialization').insertMany(data, function (err, data) {
                    if (data.insertedCount) {
                        console.log(data.insertedCount + " records created");
                    } else {
                        console.log("No data created");
                        //Close connection
                        db.close();
                        newDB.close();
                    }
                })
            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
