/**
 * This script is used migrate doctor's availability records from collection "calendarevent" to "availability".
 */

//lets require/import the mongodb native drivers.
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

//const url = 'mongodb://localhost:27017/Doxtro_Live_dump';

//new database url;
//const newUrl = 'mongodb://localhost:27017/Dox';
const url = 'mongodb://localhost:27017/doxtro1';

//new database url;
const newUrl = 'mongodb://localhost:27017/doxtro-prod';
var newDB;
MongoClient.connect(newUrl, function (err, db) {
    if (err)
        console.log('Unable to connect to the mongoDB server. Error:', err);
    else
        newDB = db;
});

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const availability = db.collection('calendarevent');
        const ObjectID = mongodb.ObjectID;

        availability.find({ logicalDelete: false }).toArray(function (err, data) {
            if (data.length != 0) {
                console.log(data.length + " availability records found");

                //json structure
                for (let i = 0; i < data.length; i++) {
                    data[i].userId = ObjectID(data[i].userId);
                    data[i].deleted = data[i].logicalDelete == false ? false : true;
                    delete data[i].logicalDelete;
                }

                //insert into new collection
                newDB.collection('availability').insertMany(data, function (err, data) {
                    if (data.insertedCount) {
                        console.log(data.insertedCount + "availability records created");
                    } else {
                        console.log("No availability records created");
                        //Close connection
                        db.close();
                        newDB.close();
                    }
                })
            } else {
                console.log("No availability records found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});
