/**
 * This script is used migrate doctor records from collection user, businesspartner, address, tokenDetails to "doctor"
 */
const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

/*const url = 'mongodb://localhost:27017/Doxtro_Live_dump';
//new database url;
const newUrl = 'mongodb://localhost:27017/Dox';*/

const url = 'mongodb://localhost:27017/doxtro1';
//new database url;
const newUrl = 'mongodb://localhost:27017/doxtro-prod';

var newDB;
MongoClient.connect(newUrl, function(err, db) {
    if (err)
        console.log('Unable to connect to the mongoDB server. Error:', err);
    else
        newDB = db;
});

// Use connect method to connect to the Server of old database
MongoClient.connect(url, function(err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        const user = db.collection('user');
        const businesspartner = db.collection('businesspartner');
        const address = db.collection('address');
        const mediaObject = db.collection('mediaobject');
        const tokendetails = db.collection('tokendetails');
        const bankdetails = db.collection('bankdetails');
        const ObjectID = mongodb.ObjectID;
        //degree list
        const entityArray = ["MBBS", "BSc", "MD", "BAMS", "BUMS", "BHMS", "BSMS", "BMS", "BA", "B com", "BS", "BDS", "MDS", "MSc", "MS", "DDVL", "DNB", "DGO", "DVD", "DLO", "D Ortho", "DO/DOMS", "DCH", "DPM", "Others"];

        user.find({ tag: 'Doctor'}).toArray(function(err, data) {

            if (data.length != 0) {
                console.log(data.length + " doctor records found.");
                let count = 0,
                    list = [];
                getData();

                function getData() {
                    //create new record structure
                    let userRecord = {
                        _id: ObjectID(data[count].businessPartnerId),
                        tag: 'doctor',
                        accountStatus: data[count].accountStatus,
                        createdAt: data[count].createdAt,
                        updatedAt: data[count].updatedAt,
                        status: data[count].status,
                        accountActivationTime: data[count].accountActivationTime ? data[count].accountActivationTime : data[count].createdAt,
                        deleted: data[count].logicalDelete == false ? false : true,
                        userId: "doctor#" + (count + 1),
                        language: "English",
                        __v : 0,
                        availabilityStatus : "available"
                    };

                    if (userRecord.deleted == true)
                        userRecord.deletedAt = userRecord.updatedAt;

                    //get personal details
                    businesspartner.findOne({ _id: data[count].businessPartnerId }, function(err, dt) {
                        if (!err && dt) {
                            if (dt.emailid)
                                userRecord.emailid = dt.emailid;
                            if (dt.jobTitle)
                                userRecord.specialization = dt.jobTitle;
                            if (dt.givenName)
                                userRecord.firstName = dt.givenName;
                            if (dt.age)
                                userRecord.age = dt.age;
                            if (dt.gender)
                                userRecord.gender = dt.gender;
                            if (dt.experience)
                                userRecord.experience = (dt.experience).toString();
                            if (dt.bio)
                                userRecord.bio = dt.bio;
                            if (dt.duns)
                                userRecord.duns = dt.duns;
                            if (dt.awards)
                                userRecord.awards = dt.awards;
                            if (dt.memberships)
                                userRecord.memberships = dt.memberships;
                            try {
                                userRecord.WorksFor = JSON.parse(dt.WorksFor);
                                userRecord.qualification = JSON.parse(dt.qualification);
                            } catch (e) {}

                            //JSON structure changes
                            if (userRecord.WorksFor) {
                                for (let i = 0; i < userRecord.WorksFor.length; i++) {
                                    userRecord.WorksFor[i] = {
                                            organization: userRecord.WorksFor[i].organisation,
                                            city: userRecord.WorksFor[i].city
                                        }
                                        //userRecord.WorksFor[i].organization = userRecord.WorksFor[i].organisation;
                                        //delete userRecord.WorksFor[i].organisation;

                                }
                            }

                            //JSON structure changes
                            if (userRecord.qualification) {
                                if (entityArray.indexOf(userRecord.qualification[0].degree) == -1) {
                                    //UG qualification
                                    userRecord.qualification[0].type = "UG";
                                    userRecord.qualification[0].additionalData = userRecord.qualification[0].degree;
                                    userRecord.qualification[0].degree = "Others";
                                    _id = new ObjectID();
                                } else {
                                    userRecord.qualification[0].type = "UG";
                                    _id = new ObjectID();
                                }
                                if (entityArray.indexOf(userRecord.qualification[1].degree) == -1) {
                                    //pg qualification
                                    userRecord.qualification[1].type = "PG";
                                    userRecord.qualification[1].additionalData = userRecord.qualification[1].degree;
                                    userRecord.qualification[1].degree = "Others";
                                } else {
                                    userRecord.qualification[1].type = "PG";
                                    _id = new ObjectID();
                                }
                            }
                        }
                        //mobile

                        address.findOne({ objectId: (data[count].businessPartnerId).toString() }, function(err, add) {

                            if (!err && add)
                                userRecord.mobile = add.mobile;
                            //profilePic
                            mediaObject.findOne({ "tag": "profilePic", objectId: (data[count].businessPartnerId).toString() }, function(err, md) {
                                if (!err && md && md.contentUrl)
                                    userRecord.profilePic = md.contentUrl;
                                //OTP
                                tokendetails.findOne({ "businessPartnerId": data[count].businessPartnerId }, function(err, td) {
                                    if (!err && td) {
                                        userRecord.otp = td.token;
                                        userRecord.isExpired = td.isExpired;
                                    }
                                    //bankdetails
                                    bankdetails.find({ objectId: (data[count].businessPartnerId).toString() }, { IFSC: 1, accountHolderName: 1, accountNumber: 1, }).toArray(function(err, bank) {
                                        if (!err && bank.length > 0)
                                            userRecord.bankDetails = bank;
                                        //documents
                                        mediaObject.find({ objectId: (data[count].businessPartnerId).toString(), "tag": { $in: ["idProof", "certificates"] } }, { tag: 1, status: 1, reason: 1, contentUrl: 1 }).toArray(function(err, docs) {
                                            if (!err && docs.length > 0) {
                                                userRecord.documents = docs;
                                                for (let j = 0; j < userRecord.documents.length; j++) {
                                                    userRecord.documents[j].url = userRecord.documents[j].contentUrl;
                                                    delete userRecord.documents[j].contentUrl;
                                                }
                                            }

                                            list.push(userRecord);
                                            count++;
                                            if (count < data.length) {
                                                getData();
                                            } else {
                                                //insert into new collection
                                                newDB.collection('doctor').insertMany(list, function(err, data) {
                                                    if (data.insertedCount) {
                                                        console.log(data.insertedCount + " doctor records created");
                                                    } else {
                                                        console.log("No doctor record created");
                                                        //Close connection
                                                        db.close();
                                                        newDB.close();
                                                    }
                                                });

                                            }
                                        });
                                    }); //bankdeatils ends here
                                });
                            });
                        });

                    });
                }
            } else {
                console.log("No doctor record found");
                //Close connection
                db.close();
                newDB.close();
            }
        });

    }
});