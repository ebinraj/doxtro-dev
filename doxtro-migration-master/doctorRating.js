const mongodb = require('mongodb');
const fs = require('fs');
const path = require('path');
const MongoClient = mongodb.MongoClient;

//const url = 'mongodb://localhost:27017/Doxtro_Live_dump';
//const url = 'mongodb://localhost:27017/Dox';

// Connection URL. This is where your mongodb server is running.
const url = 'mongodb://localhost:27017/doxtro-prod';


// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);

        const consultation = db.collection('consultation');
        const doctor = db.collection('doctor');
        const ObjectID = mongodb.ObjectID;

        doctor.find({}).toArray(function (err, data) {
            if (data.length != 0) {
                console.log(data.length + " doctor records found.");
                let count = 0, list = [], successCount = 0, failedCount = 0, totalUserCount = 0;
                getData();

                function getData() {
                    let docRecord = data[count];
                    consultation.find({ doctorId: docRecord._id, feedback: { $exists: true } }, { feedback: 1 }).toArray(function (err, dt) {
                        if (err) {
                            console.log("error in consultation find ", err.message);
                            count++;
                            if (count < data.length) {
                                //console.log("-----" + count + "-------------------------");
                                getData();
                            } else {
                                console.log("done!!!", successCount, failedCount, totalUserCount);
                            }
                        }

                        else if (dt.length == 0) {
                            //console.log("No consultation/feedback record found, ", docRecord._id);
                            count++;
                            if (count < data.length) {
                                //console.log("-----" + count + "-------------------------");
                                getData();
                            } else {
                                console.log("done!!!", successCount, failedCount, totalUserCount);
                            }
                        }

                        else if (dt.length > 0) {
                            //feedback found
                            console.log("feedback found ", dt.length, docRecord._id);
                            let rating = 0;
                            for (let i = 0; i < dt.length; i++) {
                                rating += parseInt(dt[i].feedback.rating);
                            }
                            let record = {
                                ratingUserCount: dt.length,
                                rating: rating,
                                averageRating: parseInt(rating / dt.length)
                            }
                            totalUserCount += record.ratingUserCount;
                            doctor.update({ _id: docRecord._id }, { $set: record }, function (err, doc) {
                                if (err) {
                                    failedCount++;
                                    console.log("failed to update ", docRecord._id);
                                } else if (!dt) {
                                    failedCount++;
                                    console.log("no record found ", docRecord._id);
                                } else if (dt) {
                                    successCount++;
                                }
                                count++;
                                if (count < data.length) {
                                    //console.log("-----" + count + "-------------------------");
                                    getData();
                                } else {
                                    console.log("done!!!", successCount, failedCount, totalUserCount);
                                }
                            })
                        }

                    })
                }
            } else {
                console.log("No data found");
                //Close connection
                db.close();
                newDB.close();
            }
        })

    }
});


