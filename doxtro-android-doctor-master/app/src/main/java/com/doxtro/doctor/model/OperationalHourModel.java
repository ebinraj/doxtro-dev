package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 11/08/17.
 */

public class OperationalHourModel {

    @SerializedName("dayOfWeek")
    private String dayOfWeek;

    @SerializedName("startTime")
    private Integer startTime;

    @SerializedName("endTime")
    private Integer endTime;

    public OperationalHourModel() {
        //empty constructor
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "endTime = " + endTime + ", startTime = " + startTime + ", dayOfWeek = " + dayOfWeek;
    }

}
