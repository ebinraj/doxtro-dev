package com.doxtro.doctor.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.EarningsAdapter;
import com.doxtro.doctor.model.EarningDataModel;
import com.doxtro.doctor.model.EarningsModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 16/10/17.
 */

public class AllEarningsActivity extends AppCompatActivity {
    @BindView(R.id.entire_list_RV)
    RecyclerView earningRV;

    private List<EarningDataModel> earningsList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.activity_consulation_list, null);
        setContentView(view);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Earnings");
        getMonthlyEarnings();
    }

    private void getMonthlyEarnings() {
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
        Call<EarningsModel> call = apiService.getMonthEarnings(SharePref.getDoctorID(this));
        call.enqueue(new Callback<EarningsModel>() {
            @Override
            public void onResponse(Call<EarningsModel> call, Response<EarningsModel> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().getData() != null) {
                            earningsList = response.body().getData();
                            initRV();
                        }
                    } else {
                        DialogUtils.appToast(AllEarningsActivity.this, new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<EarningsModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    private void initRV() {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        EarningsAdapter earningsAdapter = new EarningsAdapter(this, earningsList, Constants.MORE);
        earningRV.setLayoutManager(layoutManager);
        earningRV.setAdapter(earningsAdapter);
    }
}
