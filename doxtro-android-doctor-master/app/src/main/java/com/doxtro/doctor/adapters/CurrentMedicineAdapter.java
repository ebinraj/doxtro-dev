package com.doxtro.doctor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.HealthRecordsModel;

import java.util.List;

/**
 * Created by ghanshyamnayma on 14/07/17.
 */

public class CurrentMedicineAdapter extends RecyclerView.Adapter<CurrentMedicineAdapter.ViewHolder> {
    private List<HealthRecordsModel.Data> currentMedicineList;



    public CurrentMedicineAdapter(Context context, List<HealthRecordsModel.Data> currentMedicineList, TextView noRecords) {
        this.currentMedicineList = currentMedicineList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_current_medicine, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.medicineName.setText("" + currentMedicineList.get(position).getContent());
        Log.e("Medication course data", currentMedicineList.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return currentMedicineList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView medicineName;
        ImageView imgDelete;

        private ViewHolder(View itemView) {
            super(itemView);
            medicineName = (TextView) itemView.findViewById(R.id.tv_medicine_name);
            //imgDelete = (ImageView) itemView.findViewById(R.id.iv_delete_medicine);
        }
    }

}
