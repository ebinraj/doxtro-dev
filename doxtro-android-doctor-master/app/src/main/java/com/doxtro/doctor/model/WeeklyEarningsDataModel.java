package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

public class WeeklyEarningsDataModel {
    @SerializedName("week")

    private Integer week;
    @SerializedName("totalEarnings")
    private Integer totalEarnings;

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getTotalEarnings() {
        return totalEarnings;
    }

    public void setTotalEarnings(Integer totalEarnings) {
        this.totalEarnings = totalEarnings;
    }
}
