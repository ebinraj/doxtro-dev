package com.doxtro.doctor.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.ConsulationListActivity;
import com.doxtro.doctor.activities.HomeActivity;
import com.doxtro.doctor.adapters.ConsultationAdapter;
import com.doxtro.doctor.model.ConsultationRequestModel;
import com.doxtro.doctor.model.ConsultationsDataModel;
import com.doxtro.doctor.model.OperationalHourModel;
import com.doxtro.doctor.model.ProfileModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DateTimeUtility;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.EventsUtility;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;
import com.doxtro.doctor.utils.ShareUtility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressWarnings("unchecked")
public class HomeFragment extends Fragment {

    @BindView(R.id.common_base_toolbarT)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_availability_indicator)
    Switch availabilitySwitch;
    @BindView(R.id.toolbar_logo)
    ImageView toolbarLogoIV;
    @BindView(R.id.home_consultation_request_RV)
    RecyclerView consultRequestRV;
    @BindView(R.id.consultation_request_more)
    TextView requestMore;
    @BindView(R.id.no_requests_TV)
    TextView noRequestTV;
    @BindView(R.id.home_recent_consultation_RV)
    RecyclerView recentConsultationRV;
    @BindView(R.id.recent_consultation_moreTV)
    TextView recentMore;
    @BindView(R.id.no_consultationsTV)
    TextView noRecentConsultsTV;
    @BindView(R.id.home_date_day_TV)
    TextView dateDayTV;
    @BindView(R.id.home_working_hour_Tv)
    TextView workingHours;
    @BindView(R.id.consultation_request_refreshTV)
    TextView requestRefreshTV;


    private RecyclerView.LayoutManager layoutManager;
    ConsultationAdapter consultationAdapter;
    private Context mContext;
    private ArrayList<ConsultationsDataModel> todaysRequest = new ArrayList<>();

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        toolbar.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.color.white));
        toolbarTitle.setVisibility(View.GONE);
        availabilitySwitch.setVisibility(View.VISIBLE);
        toolbarLogoIV.setVisibility(View.VISIBLE);
        dateDayTV.setText(DateTimeUtility.getCurrentDate() + ", " + DateTimeUtility.getCurrentDay());
        Log.i("token", ShareUtility.getToken());
        if (NetworkUtil.getConnectionStatus(getActivity()) != 0) {
            fetchDoctorProfile();
            todaysRequest.clear();
            fetchTodaysConsultations();
            fetchConsultations(getContext());
        } else
            DialogUtils.appToast(getActivity(), getString(R.string.no_internet_connection));
        requestRefreshTV.setOnClickListener(v -> {
            DialogUtils.showProgressDialog(getContext(), getString(R.string.pleaseWaittxt));
            consultRequestRV.removeAllViews();
            todaysRequest.clear();
            fetchTodaysConsultations();
            // consultationAdapter.notifyDataSetChanged();
        });
        return view;

    }

    private void setWorkingHours() {
        if (SharePref.getDoctorDetail(getContext()) != null) {

            if (SharePref.getDoctorDetail(getContext()).getData().getAvailability() != null) {
                List<OperationalHourModel> availabilitydata = SharePref.getDoctorDetail(getContext()).getData().getAvailability();
                List<String> times = new ArrayList<>();
                for (int i = 0; i < availabilitydata.size(); i++) {

                    if (availabilitydata.get(i).getDayOfWeek().equals(DateTimeUtility.getCurrentDay())) {
//                        times.clear();
                        String startTime = availabilitydata.get(i).getStartTime().toString();
                        String endTime = availabilitydata.get(i).getEndTime().toString();

                        String startT = "";
                        String endT = "";
                        switch (startTime.length()) {
                            case 1:
                                startT = "00:00";
                                break;
                            case 2:
                                startT = "00:" + startTime;
                                break;
                            case 3:
                                String hour = startTime.substring(0, 1);
                                String minute = startTime.substring(1, 3);
                                startT = hour + ":" + minute;
                                break;
                            case 4:
                                String hour1 = startTime.substring(0, 2);
                                String minute1 = startTime.substring(2, 4);
                                startT = hour1 + ":" + minute1;
                                break;

                        }
                        switch (endTime.length()) {
                            case 1:
                                endT = "00:00";
                                break;
                            case 2:
                                endT = "00:" + endT;
                                break;
                            case 3:
                                String hour = endTime.substring(0, 1);
                                String minute = endTime.substring(1, 3);
                                endT = hour + ":" + minute;
                                break;
                            case 4:
                                String hour1 = endTime.substring(0, 2);
                                String minute1 = endTime.substring(2, 4);
                                endT = hour1 + ":" + minute1;
                                break;
                        }
                        String time = startT + " : " + endT;
                        times.add(time);
                        Log.i("Working hours", times.toString());
                    }
                    workingHours.setText(Html.fromHtml("Working Hours" + times.toString().replaceAll("\\[|\\]|\\,", "<br/>")));

                }
            }
        }
    }

    //method to fetch today's consultation list
    private void fetchTodaysConsultations() {
        String docId = SharePref.getDoctorID(mContext);
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ConsultationRequestModel> call = apiService.getTodaysRequest(docId);
        call.enqueue(new Callback<ConsultationRequestModel>() {
            @Override
            public void onResponse(Call<ConsultationRequestModel> call, Response<ConsultationRequestModel> response) {
                try {
                    DialogUtils.dismissProgressDialog();
                    if (response.isSuccessful()) {
//                        DialogUtils.dismissProgressDialog();
                        if (response.body().getData() != null)
                            if (response.body().getData().size() > 0) {
                                noRequestTV.setVisibility(View.GONE);
                                todaysRequest.addAll(response.body().getData());
                                initRequestRV(todaysRequest);
                                if (todaysRequest.size() > 2) {
                                    requestMore.setVisibility(View.VISIBLE);
                                    requestMore.setOnClickListener((View v) -> {
                                        Intent intent = new Intent(getActivity(), ConsulationListActivity.class);
                                        ConsulationListActivity.getConsultationList(todaysRequest);
                                        ((Activity) mContext).startActivityForResult(intent, 107);

                                    });
                                }
                            } else {
                                noRequestTV.setVisibility(View.VISIBLE);
                                requestMore.setVisibility(View.GONE);
                            }
                    } else {
                        DialogUtils.appToast(mContext, new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ConsultationRequestModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    static private HashMap<String, Object> unreadMsgMap = new HashMap<>();
    List<ConsultationsDataModel> consultations = new ArrayList<>();

    private void fetchConsultations(Context context) {
        String docId = SharePref.getDoctorID(context);
        ApiInterface apiService = ApiClient.getClient(context).create(ApiInterface.class);
        String skip = "0";
        String limit = "3";

        Call<ConsultationRequestModel> call = apiService.getConsultations(docId, skip, limit);
        call.enqueue(new Callback<ConsultationRequestModel>() {
            @Override
            public void onResponse(Call<ConsultationRequestModel> call, Response<ConsultationRequestModel> response) {
                try {
                    DialogUtils.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        consultations.addAll(response.body().getData());

                        /*Load list primary -->Azhar*/
                        if (consultations.size() > 0) {
                            DialogUtils.dismissProgressDialog();
                            noRecentConsultsTV.setVisibility(View.GONE);

                            initRecentConsultationRV(consultations);

                            if (consultations.size() > 2) {
                                recentMore.setVisibility(View.VISIBLE);
                                recentMore.setOnClickListener(v -> {
                                    EventsUtility.eventTrack(context, Constants.EVENT_RECENT_CONSULTATION_CLICK, null);
                                    ((HomeActivity) getActivity()).getViewPager().setCurrentItem(1);
                                });
                            }
                        } else {
                            noRecentConsultsTV.setVisibility(View.VISIBLE);
                        }


                        /*Load count secondary -->Azhar*/
                        for (int i = 0; i < consultations.size(); i++) {
                            String consultStatus = consultations.get(i).getStatus();
                            final String consultationId = consultations.get(i).getId();

                            if (consultStatus.equalsIgnoreCase(Constants.ONGOING) || consultStatus.equalsIgnoreCase(Constants.STATUS_COMPLETED) || consultStatus.equalsIgnoreCase(Constants.CLOSED)) {
                                Log.e("CON-STATUS:", consultationId + " : " + consultStatus);
                                DatabaseReference messagesReference = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_STORAGE).child(consultationId);
                                messagesReference.keepSynced(true);
                                messagesReference.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        try {
                                            if (dataSnapshot.getValue() != null) {
                                                HashMap<String, Object> chatHistory = (HashMap<String, Object>) dataSnapshot.getValue();
                                                int unreadMsgCount = 0;
                                                for (String key : chatHistory.keySet()) {
                                                    HashMap<String, Object> chatDetails = (HashMap<String, Object>) chatHistory.get(key);
                                                    String msgType = chatDetails.get(Constants.MSG_TYPE).toString();
                                                    if (msgType.equalsIgnoreCase(Constants.HEALTH_RECORD_MSGTYPE)
                                                            || msgType.equalsIgnoreCase(Constants.USER_TEXT)
                                                            || msgType.equalsIgnoreCase(Constants.IMAGE)
                                                            || msgType.equalsIgnoreCase(Constants.TYPE_DOCUMENT)
                                                            || msgType.equalsIgnoreCase(Constants.TYPE_CALL_REQUEST)
                                                            )
                                                        if (chatDetails.containsKey("status")) {
                                                            if (!chatDetails.get("status").toString().equalsIgnoreCase("read")) {
                                                                unreadMsgCount++;
                                                                unreadMsgMap.put(consultationId, unreadMsgCount);
                                                            }
                                                        }
                                                }
                                                if (unreadMsgMap.size() > 0) {
                                                    noRecentConsultsTV.setVisibility(View.GONE);
                                                }
                                                initRecentConsultationRV(consultations);

                                                Log.e("MSG-COUNT", consultationId + ":" + unreadMsgCount);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("Error", databaseError.getMessage());
                                    }
                                });
                            }
                        }
                    } else {
                        DialogUtils.appToast(getActivity(), "Consultation false state ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ConsultationRequestModel> call, Throwable t) {
                // Log error here since request failed
                DialogUtils.dismissProgressDialog();
            }
        });
    }

    //initialize recent consulatation list if data is avaialable
    private void initRecentConsultationRV(List<ConsultationsDataModel> data) {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recentConsultationRV.setLayoutManager(layoutManager);
        consultationAdapter = new ConsultationAdapter(data, getContext(), Constants.HOME_RECENT, unreadMsgMap, null);
        recentConsultationRV.setAdapter(consultationAdapter);
    }

    //initialize  consulatation request list if data is avaialable
    private void initRequestRV(List<ConsultationsDataModel> data) {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        consultRequestRV.setLayoutManager(layoutManager);
        consultationAdapter = new ConsultationAdapter(data, getContext(), Constants.HOME_REQUEST, null, null);
        consultRequestRV.setAdapter(consultationAdapter);
    }

    //fetch doctor profile
    private void fetchDoctorProfile() {
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ProfileModel> call = apiService.getDoctorProfile(SharePref.getDoctorID(getContext()));
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    DialogUtils.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.i("profile response", response.body().toString());
                        SharePref.setDoctorDetail(getContext(), response.body());
                        setWorkingHours();
                        initSwitch();
                        //  getActivity().finish();
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    //initailize the available switch
    private void initSwitch() {
        try {
            if (SharePref.getDoctorDetail(getContext()).getData().getAvailabilityStatus() != null) {
                if (SharePref.getDoctorDetail(getContext()).getData().getAvailabilityStatus().contains("available")) {
                    availabilitySwitch.setChecked(true);
                    availabilitySwitch.setText(getString(R.string.onlinetxt));
                    availabilitySwitch.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.appColor));
                } else {
                    availabilitySwitch.setChecked(false);
                    availabilitySwitch.setText(getString(R.string.onlinetxt));
                    availabilitySwitch.setText(getString(R.string.offlineTxt));
                }
            }
            availabilitySwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        Log.i("Status : ", String.valueOf(isChecked));
                        String status = (isChecked) ? getString(R.string.availableTxt) : getString(R.string.offlineTxt).toLowerCase();
                        if (NetworkUtil.getConnectionStatus(getContext()) != 0) {
                            updateAvailabilityStatus(status);
                        } else {

                            DialogUtils.appToast(getContext(), getString(R.string.no_internet_connection));
                        }
                        if (isChecked) {
                            availabilitySwitch.setText(getString(R.string.onlinetxt));
                        } else {
                            availabilitySwitch.setText(getString(R.string.offlineTxt));
                        }

                    }

            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //api to update the available status
    private void updateAvailabilityStatus(String status) {
        String doctorID = SharePref.getDoctorID(getContext());
        DialogUtils.showProgressDialog(getContext(), getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);

        Call<JsonObject> call = apiService.updateAvailabilityStatus(doctorID, status);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {

                    if (response.isSuccessful()) {
                        Toast.makeText(getContext(), "Availability status Updated!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Update failed! Please update again", Toast.LENGTH_SHORT).show();
                        // DialogUtils.appToast(HomeActivity.this, new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 105) {
                Log.e("refresh", "refresh");
                unreadMsgMap.clear();
                consultations.clear();
                fetchConsultations(getContext());

                //  Toast.makeText(getActivity(), "refresh", Toast.LENGTH_SHORT).show();
            } else if (requestCode == 107) {
                consultations.clear();
                todaysRequest.clear();
                fetchTodaysConsultations();
                fetchConsultations(getContext());
            }
        }
    }
}