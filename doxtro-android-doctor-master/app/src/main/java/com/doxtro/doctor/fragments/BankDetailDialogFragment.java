package com.doxtro.doctor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.R;
import com.doxtro.doctor.interfaces.DialogDataListener;
import com.doxtro.doctor.model.BankDetailModel;
import com.doxtro.doctor.model.ProfileModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.SharePref;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 17/10/17.
 */

public class BankDetailDialogFragment extends DialogFragment {
    private static DialogDataListener dialoglistner;
    private static List<BankDetailModel> bankDetail = new ArrayList<>();
    @BindView(R.id.profile_dialog_save)
    Button saveBtn;
    @BindView(R.id.dialog_title)
    TextView title;
    @BindView(R.id.profile_dialog_ET1)
    EditText editText1;
    @BindView(R.id.profile_dialog_ET2)
    EditText editText2;
    @BindView(R.id.profile_dialog_ET3)
    EditText editText3;
    @BindView(R.id.profile_dialog_ET4)
    EditText editText4;


    public BankDetailDialogFragment() {
    }

    public static BankDetailDialogFragment newInstance(DialogDataListener dialogDataListener, List<BankDetailModel> bankDetailModel) {
        Bundle args = new Bundle();
        BankDetailDialogFragment fragment = new BankDetailDialogFragment();
        fragment.setArguments(args);
        bankDetail = bankDetailModel;
        if (dialogDataListener != null)
            dialoglistner = dialogDataListener;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_profile_edit, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle(getString(R.string.addbanktxt));
        // dialog.setContentView(root);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(AndroidVersionUtility.getDrawable(getContext(), R.color.white));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return dialog;
    }

    private void initViews() {
        editText1.setInputType(InputType.TYPE_CLASS_TEXT);
        editText2.setInputType(InputType.TYPE_CLASS_TEXT);
        editText1.setVisibility(View.VISIBLE);
        editText2.setVisibility(View.VISIBLE);
        editText3.setVisibility(View.VISIBLE);
        editText4.setVisibility(View.VISIBLE);
        editText1.setMaxLines(1);
        editText2.setMaxLines(1);
        editText3.setMaxLines(1);
        editText4.setMaxLines(1);
        title.setText(getString(R.string.addbanktxt));
        if (bankDetail.size() > 0) {

            BankDetailModel bankDetailModel = bankDetail.get(0);
            if (bankDetailModel.getAccountNumber().equals(""))
                editText1.setHint(getString(R.string.accountnotxt));
            else
                editText1.setText(bankDetailModel.getAccountNumber());
            if (bankDetailModel.getBank().equals(""))
                editText2.setHint(getString(R.string.selectbanktxt));
            else
                editText2.setText(bankDetailModel.getBank());
            if (bankDetailModel.getIFSC().equals(""))
                editText3.setHint(getString(R.string.ifsctxt));
            else
                editText3.setText(bankDetailModel.getIFSC());
            if (bankDetailModel.getAccountHolderName().equals(""))
                editText4.setHint(getString(R.string.accnholdernametxt));
            else
                editText4.setText(bankDetailModel.getAccountHolderName());

        } else {
            title.setText(getString(R.string.addbanktxt));

            editText1.setHint(getString(R.string.accountnotxt));

            editText2.setHint(getString(R.string.selectbanktxt));
            editText3.setHint(getString(R.string.ifsctxt));

            editText4.setHint(getString(R.string.accnholdernametxt));

        }
        saveBtn.setOnClickListener(v -> {
            try {
                if (editText1.getText().toString().equals("") && editText2.getText().toString().equals("")
                        && editText3.getText().toString().equals("") && editText4.getText().toString().equals("") || editText1.getText().toString().equals("") && editText2.getText().toString().equals("")) {
                    Toast.makeText(getContext(), "Please enter details to save!", Toast.LENGTH_SHORT).show();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.ACCOUNT_NUMBER, editText1.getText().toString());
                    bundle.putString(Constants.BANK_NAME, editText2.getText().toString());
                    bundle.putString(Constants.IFSC_CODE, editText3.getText().toString());
                    bundle.putString(Constants.ACC_HOLDER, editText4.getText().toString());
                    dialoglistner.getBankDetails(bundle);
                    JsonArray jsonArray = new JsonArray();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("bank", editText2.getText().toString());
                    jsonObject.addProperty("accountNumber", editText1.getText().toString());
                    jsonObject.addProperty("accountHolderName", editText4.getText().toString());
                    jsonObject.addProperty("IFSC", editText3.getText().toString());
                    jsonArray.add(jsonObject);
                    JsonObject jsonObject1 = new JsonObject();
                    jsonObject1.add("bankDetails", jsonArray);
                    jsonObject1.addProperty("_id", SharePref.getDoctorID(getContext()));
                    updateToServer(jsonObject1);
                    dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }

    private void updateToServer(JsonObject jsonObject) {

        ApiInterface apiService = ApiClient.getClient(getContext()).create(ApiInterface.class);
        Call<ProfileModel> call = apiService.updateDoctorProfileWithObject(jsonObject);
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    if (response.isSuccessful()) {
                        //   if(response.body().get("data").getAsJsonObject().get("bankDetails").getAsJsonArray()!=null)
                        if (response.body().getData().getBankDetails() != null && response.body().getData().getBankDetails().size() > 0)
                            BankDetailFragment.bankDetailModel = response.body().getData().getBankDetails();
                        DialogUtils.appToast(getContext(), "Bank details updated!");
                    } else {
                        DialogUtils.appToast(getActivity(), new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }


}
