package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 16/10/17.
 */

public class EarningDataModel {
    @SerializedName("date")
    private String date;
    @SerializedName("earning")
    private Integer earning;
    @SerializedName("patientId")

    private ConsultationsDataModel.PatientModel patientId;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getEarning() {
        return earning;
    }

    public void setEarning(Integer earning) {
        this.earning = earning;
    }

    public ConsultationsDataModel.PatientModel getPatientId() {
        return patientId;
    }

    public void setPatientId(ConsultationsDataModel.PatientModel patientId) {
        this.patientId = patientId;
    }

}
