package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 04/09/17.
 */

public class ConsultationRequestModel implements Serializable {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<ConsultationsDataModel> data = new ArrayList<>();

    @SerializedName("pagination")
    private Pagination pagination;

    @SerializedName("totalCount")
    private Integer totalCount;

    @SerializedName("count")
    private Integer count;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ConsultationsDataModel> getData() {
        return data;
    }

    public void setData(List<ConsultationsDataModel> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public class Pagination {

        @SerializedName("from")
        private Integer from;

        @SerializedName("to")
        private Integer to;

        public Integer getFrom() {
            return from;
        }

        public void setFrom(Integer from) {
            this.from = from;
        }

        public Integer getTo() {
            return to;
        }

        public void setTo(Integer to) {
            this.to = to;
        }

    }


}
