package com.doxtro.doctor.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.appsflyer.AppsFlyerLib;
import com.doxtro.doctor.R;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.SharePref;


public class SplashActivity extends Activity  {

    /*
     * Duration of wait
     */
    private int SPLASH_DISPLAY_LENGTH = 3000;
    private VideoView videoView;
    private RelativeLayout root_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /*if (AndroidVersionUtility.isLollipopOrLess())
            setTheme(R.style.SplashScreenTheme);*/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        root_layout = (RelativeLayout) findViewById(R.id.root_layout);
        videoView = (VideoView) findViewById(R.id.video_view_splash);
        AppsFlyerLib.getInstance().sendDeepLinkData(this);

        //if (!AndroidVersionUtility.isLollipopOrLess()) {
        root_layout.setBackgroundColor(AndroidVersionUtility.getColor(this, R.color.white));
        videoView.setZOrderOnTop(true);
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.logo_video);
        videoView.setVideoURI(uri);
        videoView.start();
        SPLASH_DISPLAY_LENGTH = 3000;
       /* }else
        {
            videoView.setVisibility(View.GONE);
            root_layout.setBackgroundColor(0);
            SPLASH_DISPLAY_LENGTH = 1000;
        }*/

        //New Handler to start the Menu-Activity
        // and close this Splash-Screen after some seconds.
        new Handler().postDelayed(() -> {

            if (SharePref.isLoggedIn(SplashActivity.this)) {
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                finish();
            } else {
                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }


    /*private VideoView mVV;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);

        setContentView(R.layout.activity_splash);

        int fileRes=0;
        Bundle e = getIntent().getExtras();
        if (e!=null) {
            fileRes = e.getInt("fileRes");
        }

        mVV = (VideoView)findViewById(R.id.video_view);
        mVV.setOnCompletionListener(this);
        mVV.setOnPreparedListener(this);
        mVV.setOnTouchListener(this);

       // if (!playFileRes(fileRes)) return;
        mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash_video));

        mVV.start();
    }

    public void stopPlaying() {
        mVV.stopPlayback();
        this.finish();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopPlaying();
        Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(mainIntent);
       finish();

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //stopPlaying();
        return true;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.setLooping(false);
    }

  */
}
