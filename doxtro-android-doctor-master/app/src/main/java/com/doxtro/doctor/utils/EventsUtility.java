package com.doxtro.doctor.utils;

import android.content.Context;

import com.appsflyer.AppsFlyerLib;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yashaswi on 26/10/17.
 */

public class EventsUtility {

    public static void eventTrack(Context context, String eventName, Map<String, Object> eventValue) {
        CleverTapAPI cleverTapAPI = null;
//        AppDelegate.initAppsFlyer();
        AppsFlyerLib.getInstance().trackEvent(context, eventName, eventValue);
        try {
            cleverTapAPI = CleverTapAPI.getInstance(context.getApplicationContext());
        } catch (CleverTapMetaDataNotFoundException e) {
            e.printStackTrace();
        } catch (CleverTapPermissionsNotSatisfied cleverTapPermissionsNotSatisfied) {
            cleverTapPermissionsNotSatisfied.printStackTrace();
        }
        cleverTapAPI.event.push(eventName, eventValue);
    }

    public static void pushProfile(Context context,HashMap<String, Object> profileData)
    {
        CleverTapAPI cleverTapAPI = null;
        try {
            cleverTapAPI = CleverTapAPI.getInstance(context.getApplicationContext());
        } catch (CleverTapMetaDataNotFoundException e) {
            e.printStackTrace();
        } catch (CleverTapPermissionsNotSatisfied cleverTapPermissionsNotSatisfied) {
            cleverTapPermissionsNotSatisfied.printStackTrace();
        }
        cleverTapAPI.profile.push(profileData);

    }
}
