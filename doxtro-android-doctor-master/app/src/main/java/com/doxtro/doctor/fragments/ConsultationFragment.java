package com.doxtro.doctor.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.doxtro.doctor.R;
import com.doxtro.doctor.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */

public class ConsultationFragment extends Fragment {
    @BindView(R.id.consualtations_tabs)
    TabLayout tabs;
    @BindView(R.id.consultations_viewpager)
    ViewPager viewPager;
    private OnGoingConsultationFragment onGoingFragment;
    private ClosedConsultationFragment closedFragment;

    public ConsultationFragment() {
        // Required empty public constructor
    }

    public static ConsultationFragment newInstance() {
        Bundle args = new Bundle();
        ConsultationFragment fragment = new ConsultationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consultation, container, false);
        ButterKnife.bind(this, view);
        setupViewPager();
        return view;
    }


    public static int tab = 0; // maintain tab state -->Azhar

    private void setupViewPager() {
        ConsultationFragmentViewPagerAdapter adapter = new ConsultationFragmentViewPagerAdapter(getActivity().getSupportFragmentManager());
        onGoingFragment = new OnGoingConsultationFragment();
        closedFragment = new ClosedConsultationFragment();
        adapter.addFragment(onGoingFragment, Constants.ONGOING);
        adapter.addFragment(closedFragment, Constants.COMPLETED);

        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);

        /*maintain tab state -->Azhar*/
        viewPager.setCurrentItem(tab);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tab = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        /*maintain tab state -->Azhar*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 106) {

                if (onGoingFragment != null)
                    onGoingFragment.onActivityResult(requestCode, resultCode, data);
                if (closedFragment != null)
                    closedFragment.onActivityResult(requestCode, resultCode, data);

                // fetchConsultations();
            }
            if(requestCode==105)
            {
                if (onGoingFragment != null)
                    onGoingFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private class ConsultationFragmentViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ConsultationFragmentViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}


