package com.doxtro.doctor.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.HomeActivity;
import com.doxtro.doctor.adapters.AvailabilityAdapter;
import com.doxtro.doctor.model.AvailabilityModel;
import com.doxtro.doctor.model.OperationalHourModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.doxtro.doctor.R.id.availability_checkbox;
import static com.doxtro.doctor.R.id.availability_clear_all;
import static com.doxtro.doctor.R.id.availability_copy_to_all;

/**
 * Created by yashaswi on 20/07/17.
 */

public class AvailabilityFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.availability_grid_view)
    GridView gridView;
    @BindView(availability_copy_to_all)
    Button copyToAllBtn;
    @BindView(availability_clear_all)
    Button clearAllBtn;
    @BindView(availability_checkbox)
    CheckBox checkBox;
    @BindView(R.id.availability_save)
    Button save;
    @BindView(R.id.days_rows)
    LinearLayout daysRowsLL;


    private ArrayList<HashMap<String, Integer>> selectedSlots = new ArrayList<>();


    private ArrayList<Integer> times = new ArrayList<>();
    private int selectedDay;
    private int noOfClicks;
    private int startSlotTime;
    private String source = "";

    public AvailabilityFragment() {
        //empty constructor
    }

    public static AvailabilityFragment newInstance(String source) {
        Bundle args = new Bundle();
        AvailabilityFragment fragment = new AvailabilityFragment();
        fragment.setArguments(args);
        args.putString(Constants.SOURCE, source);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            source = getArguments().getString(Constants.SOURCE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_availability, container, false);
        ButterKnife.bind(this, view);
        initUI();
        getAvailabilityData();
        save.setOnClickListener(this);
        return view;
    }

    //initialize the entire UI
    private void initUI() {
        initSlots();
        copyToAllBtn.setOnClickListener(this);
        clearAllBtn.setOnClickListener(this);
        checkBox.setOnClickListener(this);
    }

    //time slots initialization
    private void initSlots() {
        selectedDay = 1;
        for (int i = 0; i < daysRowsLL.getChildCount(); i++) {
            final TextView childTV = (TextView) daysRowsLL.getChildAt(i);
            childTV.setOnClickListener(v -> {
                for (int j = 0; j < daysRowsLL.getChildCount(); j++) {
                    TextView tvj = (TextView) daysRowsLL.getChildAt(j);
                    tvj.setTextColor(Color.parseColor("#000000"));
                }
                childTV.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.appColor));
                childTV.setCompoundDrawables(null, null, null, AndroidVersionUtility.getDrawable(getContext(), R.drawable.profile_selector_line));
                selectedDay = weekNameToInt(getResources().getResourceEntryName(childTV.getId()));
                noOfClicks = 0;
                gridView.setAdapter(new AvailabilityAdapter(getContext(), times, selectedSlots, selectedDay));
            });
            noOfClicks = 0;
            final int startTime = 0;
            int endTime = 2400;
            int t = startTime;

            times = new ArrayList<>();
            selectedSlots = new ArrayList<>();
            do {
                times.add(t);
                t += 30;
                times.add(t);
                if (t == 2330) {
                    t += 29;
                    times.add(t);
                } else
                    t += 70;
            } while (t < endTime - 41);


            gridView.setAdapter(new AvailabilityAdapter(getContext(), times, selectedSlots, selectedDay));
            gridView.setOnItemClickListener((parent, v, position, id) -> {
                if (checkIfSelected(position) != 0) return;
                // check if we are overlapping another slot
                noOfClicks++;
                if (noOfClicks == 1) {
                    TextView txtView = (TextView) v.findViewById(R.id.time_slot);
                    txtView.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.white));
                    startSlotTime = times.get(position);
                    txtView.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.rounded_button_appcolor_background));
                } else if (noOfClicks == 2) {
                    int endSlotTime = times.get(position);
                    if (startSlotTime == endSlotTime) return;
                    if (startSlotTime > endSlotTime) {
                        endSlotTime = startSlotTime;
                        startSlotTime = times.get(position);
                    }
                    if (checkIfOverlappingSlot(startSlotTime, endSlotTime) != 0) {
                        noOfClicks = 0;
                        return;
                    }
                    HashMap<String, Integer> slot = new HashMap<>();
                    slot.put("start", startSlotTime);
                    slot.put("end", endSlotTime);
                    slot.put("selectedDay", selectedDay);
                    selectedSlots.add(slot);
                    noOfClicks = 0;
                    gridView.setAdapter(new AvailabilityAdapter(getContext(), times, selectedSlots, selectedDay));
                }

            });
        }
        gridView.setAdapter(new AvailabilityAdapter(getContext(), times, selectedSlots, selectedDay));
    }

    //to get the already added availability slots
    private void getAvailabilityData() {
        List<OperationalHourModel> availabilityData;
        if (SharePref.getDoctorDetail(getContext()) != null && SharePref.getDoctorDetail(getContext()).getData().getAvailability().size() > 0) {
            availabilityData = SharePref.getDoctorDetail(getContext()).getData().getAvailability();
            selectedSlots.clear();
            for (int i = 0; i < availabilityData.size(); i++) {
                OperationalHourModel slot = availabilityData.get(i);
                HashMap<String, Integer> slotCopy = new HashMap<>();
                slotCopy.put("start", slot.getStartTime());
                slotCopy.put("end", slot.getEndTime());
                slotCopy.put("selectedDay", weekNameToInt(slot.getDayOfWeek()));
                this.selectedSlots.add(slotCopy);
            }
            Log.i("list", selectedSlots.toString());
            gridView.setAdapter(new AvailabilityAdapter(getContext(), times, selectedSlots, selectedDay));
        }
    }

    //to checks if the selected slots are overlapping or not
    private int checkIfOverlappingSlot(int startS, int endS) {
        for (int i = 0; i < selectedSlots.size(); i++) {
            HashMap<String, Integer> slot = selectedSlots.get(i);
            if (selectedDay == slot.get("selectedDay")) {

                int start = slot.get("start");
                int end = slot.get("end");

                if ((startS == start) || (startS == end))
                    return 1;

                if ((endS == start) || (endS == end))
                    return 1;

                if ((endS > end) && (startS < start))
                    return 1;
            }
        }
        return 0;

    }

    //to check if the slots selected already or not
    private int checkIfSelected(int position) {
        int value = times.get(position);

        /*
            0 = not selected
            1 = starting
            2 = middle
            3 = end
         */

        // loop in arrar>hasmap and find if any value starts with it,
        for (int i = 0; i < selectedSlots.size(); i++) {
            HashMap<String, Integer> slot = selectedSlots.get(i);

            if (selectedDay == slot.get("selectedDay")) {

                int start = slot.get("start");
                int end = slot.get("end");

                if (value == start) return 1;
                if (value > start && value < end) return 2;
                if (value == end) return 3;
            }
        }

        return 0;

    }

    //convert the selcted string day to int
    private static int weekNameToInt(String day) {
        switch (day) {
            case "Monday":
                return 1;
            case "Tuesday":
                return 2;
            case "Wednesday":
                return 3;
            case "Thursday":
                return 4;
            case "Friday":
                return 5;
            case "Saturday":
                return 6;
            case "Sunday":
                return 7;
        }
        return 1;
    }

    //convert the selcted int day to string
    private static String weekIntToString(int dayInt) {
        switch (dayInt) {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
        }
        return "monday";
    }

    //save the selected availability then make api call
    private void saveAvailability() {
        if (selectedSlots.size() > 0 && selectedSlots != null) {
            AvailabilityModel availabilityModel = new AvailabilityModel();
            ArrayList<OperationalHourModel> operationalHourModelList = new ArrayList<>();
            availabilityModel.setId(SharePref.getDoctorID(getContext()));
            for (int i = 0; i < selectedSlots.size(); i++) {
                HashMap<String, Integer> slot = selectedSlots.get(i);
                OperationalHourModel operationalHourModel = new OperationalHourModel();

                String selectedDays = weekIntToString(slot.get("selectedDay"));

                String startTimeString = slot.get("start").toString();
                if (startTimeString.length() < 2) startTimeString = "00" + startTimeString;
                else if (startTimeString.length() < 3) startTimeString = "0" + startTimeString;

                String endTimeString = slot.get("end").toString();
                if (endTimeString.length() < 2) endTimeString = "00" + endTimeString;
                else if (endTimeString.length() < 3) endTimeString = "0" + endTimeString;

                operationalHourModel.setDayOfWeek(selectedDays);
                operationalHourModel.setStartTime(Integer.valueOf(startTimeString));
                operationalHourModel.setEndTime(Integer.parseInt(endTimeString));
                operationalHourModelList.add(i, operationalHourModel);
                availabilityModel.setOperationalHours(operationalHourModelList);
            }
            if (NetworkUtil.getConnectionStatus(getActivity()) != 0)
                updateAvailabilityApiCall(availabilityModel);
            else
                DialogUtils.appToast(getActivity(), getString(R.string.no_internet_connection));


        } else {
            Toast.makeText(getContext(), getString(R.string.selectTimeToSaveTxt), Toast.LENGTH_SHORT).show();
        }
    }

    //api call to send the selected available slots
    private void updateAvailabilityApiCall(AvailabilityModel availabilityModel) {
        DialogUtils.showProgressDialog(getActivity(), getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<AvailabilityModel> call = apiService.updateAvailability(availabilityModel);
        call.enqueue(new Callback<AvailabilityModel>() {
            @Override
            public void onResponse(Call<AvailabilityModel> call, Response<AvailabilityModel> response) {
                try {
                    DialogUtils.dismissProgressDialog();
                    if (response.isSuccessful()) {

                        if (source.contains(Constants.BASIC_PROFILE)) {
                            Toast.makeText(getContext(), getString(R.string.availabilityUpdatedtxt), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), HomeActivity.class);
                            startActivity(intent);

                        } else {
                            Log.i("availability response",response.body().toString());
                            Toast.makeText(getContext(), getString(R.string.availabilityUpdatedtxt), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<AvailabilityModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }

        });
    }

    //to clear the selected slots * wrt single day *
    private void clearAllClick() {
        if (selectedSlots.size() > 0) {
            ArrayList<HashMap<String, Integer>> tempSelectedSlots = new ArrayList<>();
            for (int i = 0; i < selectedSlots.size(); i++) {
                HashMap<String, Integer> slot = selectedSlots.get(i);
                if (selectedDay != slot.get("selectedDay")) {
                    tempSelectedSlots.add(slot);
                }
            }
            selectedSlots.clear();
            selectedSlots = tempSelectedSlots;
            noOfClicks = 0;
            gridView.setAdapter(new AvailabilityAdapter(getContext(), times, selectedSlots, selectedDay));
            Toast.makeText(getContext(), getString(R.string.clearedText), Toast.LENGTH_LONG).show();
        } else {
            selectedSlots.clear();
            Toast.makeText(getContext(), getString(R.string.clearedText), Toast.LENGTH_LONG).show();
        }
    }

    //copy the selected slots
    private void copyAllClick() {
        noOfClicks = 0;
        if (selectedSlots.size() > 0) {
            ArrayList<HashMap<String, Integer>> tempSelectedSlots = new ArrayList<>();
            for (int i = 0; i < selectedSlots.size(); i++) {
                HashMap<String, Integer> slot = selectedSlots.get(i);
                if (selectedDay == slot.get("selectedDay")) {
                    tempSelectedSlots.add(slot);
                }
            }
            selectedSlots.clear();
            for (int i = 0; i < tempSelectedSlots.size(); i++) {
                HashMap<String, Integer> slot = tempSelectedSlots.get(i);
                for (int x = 1; x < 8; x++) {
                    HashMap<String, Integer> slotCopy = new HashMap<>();
                    slotCopy.put("start", slot.get("start"));
                    slotCopy.put("end", slot.get("end"));
                    slotCopy.put("selectedDay", x);
                    this.selectedSlots.add(slotCopy);
                }

            }
            Log.i("list", selectedSlots.toString());
            gridView.setAdapter(new AvailabilityAdapter(getContext(), times, selectedSlots, selectedDay));
            Toast.makeText(getContext(), getString(R.string.copiedtext), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), getString(R.string.selectTimeTxt), Toast.LENGTH_LONG).show();

        }
    }

    //click method
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case availability_copy_to_all:
                copyAllClick();
                break;
            case availability_clear_all:
                clearAllClick();
                break;
            case availability_checkbox:
                if (((CheckBox) view).isChecked()) {
                    copyAllClick();
                }
                break;
            case R.id.availability_save:
                if (NetworkUtil.getConnectionStatus(getActivity()) != 0) {
                    saveAvailability();
                } else {
                    DialogUtils.appToast(getContext(), getString(R.string.no_internet_connection));
                }
                break;


        }
    }
}

