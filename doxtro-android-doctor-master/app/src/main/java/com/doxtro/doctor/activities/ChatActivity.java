package com.doxtro.doctor.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.BuildConfig;
import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.ChatAdapter;
import com.doxtro.doctor.interfaces.DocumentUploadListener;
import com.doxtro.doctor.interfaces.OnTimeChangeListener;
import com.doxtro.doctor.interfaces.RequestCallListener;
import com.doxtro.doctor.model.ChatModel;
import com.doxtro.doctor.model.DiagnosticModel;
import com.doxtro.doctor.model.DoctorProfileModel;
import com.doxtro.doctor.model.ExoticCallConnectModel;
import com.doxtro.doctor.model.FindPrescriptionModel;
import com.doxtro.doctor.model.FirebasePrescriptionModel;
import com.doxtro.doctor.model.MedicationModel;
import com.doxtro.doctor.model.PatientDetailModel;
import com.doxtro.doctor.model.PrescriptionPostModel;
import com.doxtro.doctor.model.PrescriptionResponseModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DateTimeUtility;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.EventsUtility;
import com.doxtro.doctor.utils.FirebaseUtils;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.Reusable;
import com.doxtro.doctor.utils.SharePref;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yashaswi on 07/09/17.
 */

@SuppressWarnings("unchecked")
public class ChatActivity extends AppCompatActivity implements View.OnClickListener, DocumentUploadListener, OnTimeChangeListener, RequestCallListener {

    @BindView(R.id.recycler_view_chat)
    RecyclerView chatRV;
    @BindView(R.id.input_box_chat)
    EditText chatInputET;
    @BindView(R.id.common_base_toolbarT)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.chat_prescribe_medicine_TV)
    TextView prescribeTV;
    @BindView(R.id.chat_diagnostic_TV)
    TextView diagnosisTV;
    @BindView(R.id.chat_close_consultation_TV)
    TextView closeConsultationTV;
    @BindView(R.id.bottom_bar_toggle_plus)
    ImageView addImageIV;
    @BindView(R.id.bottom_bar_toggle_minus)
    ImageView minusImageIV;
    @BindView(R.id.chat_media_layout)
    LinearLayout mediaLayout;
    @BindView(R.id.chat_prescribe_layout)
    LinearLayout precribeLayout;
    @BindView(R.id.chat_upload_imageTV)
    TextView uploadImageTV;
    @BindView(R.id.chat_take_imageTV)
    TextView takeImageTV;
    @BindView(R.id.chat_upload_documentTV)
    TextView uploadDocumentTV;
    @BindView(R.id.time_global)
    TextView timestampTV;
    @BindView(R.id.chat_bottom_view)
    LinearLayout bottomBar;
    @BindView(R.id.chat_callIV)
    ImageView connectCallIV;


    private String consultationId = "";
    private static DatabaseReference messagesReference;
    private ChatAdapter adapter;
    private static String doctorId;
    private static String doctorName;
    private static String duns;
    private static List<String> qualifications = null;
    private String patientData = "";
    private String timeStamp = "";
    private String patientName = "";
    private List<PrescriptionResponseModel.Data> prescriptionData;
    private List<MedicationModel> medicationsList = null;
    private static String prescriptionUpdateKey = "";
    private List<DiagnosticModel> diagnosticsList = new ArrayList<>();
    private static String diagnosticUpdateKey = "";
    private static String patientId = "";
    private static List<String> specializations = new ArrayList<>();
    private final ArrayList<String> permissions = new ArrayList<>();

    private int firstVisibleItem, lastVisibleItem, totalItemCount;

    private OnTimeChangeListener onTimeChangeListener;
    private String status = "";

    private final List<ChatModel> chatHistory = new ArrayList<>();
    private String mCurrentPhotoPath;
    private String patientMobile = "";
    private String consultationType = "";
    private boolean isCompleted = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.activity_chat, null);
        super.onCreate(savedInstanceState);
        setContentView(view);
        ButterKnife.bind(this);
        new FirebaseUtils(this);
        onTimeChangeListener = this;

        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.CAMERA);
        updateUserStatus(Constants.ONLINE);
        try {

            doctorId = SharePref.getDoctorID(ChatActivity.this);
            doctorName = SharePref.getDoctorDetail(this).getData().getFirstName();
            if (SharePref.getDoctorDetail(this).getData().getCategory().size() > 0)
                specializations = SharePref.getDoctorDetail(this).getData().getCategory();
            duns = SharePref.getDoctorDetail(this).getData().getDuns();
            if (getIntent() != null) {
                consultationId = getIntent().getStringExtra(Constants.CONSULTATION_ID);
                patientName = getIntent().getStringExtra(Constants.PATIENT_NAME);
                status = getIntent().getStringExtra(Constants.STATUS);
                if (getIntent().getStringExtra(Constants.CONSULTATION_TYPE) != null) {
                    consultationType = getIntent().getStringExtra(Constants.CONSULTATION_TYPE);
                }
            }


            if (NetworkUtil.getConnectionStatus(this) != 0) {
                fetchAllprescriptions(consultationId);
                retrieveAllMessages(consultationId);
            } else {

                DialogUtils.appToast(this, getString(R.string.no_internet_connection));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (patientName != null)
                toolbarTitle.setText(patientName);
//                getSupportActionBar().setTitle(patientName);
            else
                toolbarTitle.setText(getString(R.string.chat));
            // getSupportActionBar().setTitle("Chat");
        }
        closeConsultationTV.setOnClickListener(v -> {
            if (NetworkUtil.getConnectionStatus(this) != 0) {
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to mark this consultation completed ?")
                        .setNegativeButton(getString(R.string.cancelTxt), null)
                        .setPositiveButton(getString(R.string.yestext), (dialog, which) -> callCloseConsultation())
                        .show();


            } else {

                DialogUtils.appToast(this, getString(R.string.no_internet_connection));
            }

        });
        addImageIV.setOnClickListener(v -> {
            minusImageIV.setVisibility(View.VISIBLE);
            addImageIV.setVisibility(View.GONE);
            precribeLayout.setVisibility(View.GONE);
            mediaLayout.setVisibility(View.VISIBLE);
        });
        minusImageIV.setOnClickListener(v -> {
            minusImageIV.setVisibility(View.GONE);
            addImageIV.setVisibility(View.VISIBLE);
            precribeLayout.setVisibility(View.VISIBLE);
            mediaLayout.setVisibility(View.GONE);
        });
        if (status != null) {
            if (status.contains(Constants.CLOSED)) {
                bottomBar.setVisibility(View.GONE);
            } else
                bottomBar.setVisibility(View.VISIBLE);
        }
        EventsUtility.eventTrack(ChatActivity.this, Constants.EVENT_CHAT_SCREEN, null);
        timeStamp = DateTimeUtility.getChatDDMMMYYYYFromString(DateTimeUtility.getCurrentTime());
        initialiseListeners();

        //Typing status -->Azhar 23/11/17
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.FIREBASE_STORAGE)
                .child(consultationId)
                .child("Bot_typingIndicator")
                .child(Constants.MSG_TYPE).setValue("typing");
        final DatabaseReference typingReference = FirebaseDatabase.getInstance().getReference()
                .child(Constants.FIREBASE_STORAGE)
                .child(consultationId)
                .child("Bot_typingIndicator")
                .child(doctorId);
        chatInputET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s)) {
                    Log.e("Msg-Type Status", "typing started event…");
                    typingReference.setValue(true);
                    new Handler().postDelayed(() -> {
                        //Set the value of typing field to true.
                        Log.e("Msg-Type Status", "typing stopped event…");
                        typingReference.setValue(false);
                    }, 2000);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        if (consultationType != null && consultationType.equalsIgnoreCase(Constants.AUDIO_TYPE)) {
            if (!status.equalsIgnoreCase(Constants.CLOSED)) {
                connectCallIV.setVisibility(View.VISIBLE);
                connectCallIV.setOnClickListener(v -> new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to start the audio consultations with the patient?")
                        .setNegativeButton(getString(R.string.cancelTxt), null)
                        .setPositiveButton(getString(R.string.yestext), (dialog, which) -> {
                            if (NetworkUtil.getConnectionStatus(this) != 0) {

                                callExoticConnectApi();
                            } else {

                                DialogUtils.appToast(this, getString(R.string.no_internet_connection));
                            }

                        })
                        .show());
            } else {
                connectCallIV.setVisibility(View.GONE);
            }
        }
    }

    private void callExoticConnectApi() {
        DialogUtils.showProgressDialog(ChatActivity.this, getString(R.string.pleaseWaittxt));
        String patientNumber = "", doctorNumber = "";
        if (this.patientMobile != null || !this.patientMobile.equals("")) {
            patientNumber = patientMobile;
        }
        if (SharePref.getDoctorDetail(ChatActivity.this).getData() != null && SharePref.getDoctorDetail(ChatActivity.this).getData().getMobile() != null) {
            doctorNumber = SharePref.getDoctorDetail(ChatActivity.this).getData().getMobile();
        }
        ApiInterface apiService = ApiClient.getExotelClient(this).create(ApiInterface.class);
        Call<ExoticCallConnectModel> call = apiService.connectExoticCall(doctorNumber, patientNumber, Constants.EXOTEL_CALLER_ID);
        call.enqueue(new Callback<ExoticCallConnectModel>() {
            @Override
            public void onResponse(Call<ExoticCallConnectModel> call, Response<ExoticCallConnectModel> response) {
                DialogUtils.dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        DialogUtils.appToast(ChatActivity.this, "Call is initiated");
                    }
                }
            }

            @Override
            public void onFailure(Call<ExoticCallConnectModel> call, Throwable t) {
                DialogUtils.dismissProgressDialog();
            }


        });
    }


    private void initialiseListeners() {
        takeImageTV.setOnClickListener(this);
        uploadImageTV.setOnClickListener(this);
        uploadDocumentTV.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            switch (requestCode) {
                case Constants.SELECT_IMAGE:
                    if (data != null) {
                        InputStream ims = getContentResolver().openInputStream(data.getData());
                        Bitmap image = BitmapFactory.decodeStream(ims);
                        image = getResizedBitmap(image, 400);
//                    Bitmap seelectedImage = CameraUtils.selctedImageFromGallery(this, data);
                        FirebaseUtils.storeInFirebase(ChatActivity.this, CameraUtils.getImageUri(image, this), Constants.CHAT_IMAGE);
                    }
                    break;
                case Constants.TAKE_IMAGE:
//                    Bitmap capturedImage = CameraUtils.capturedImage(this, data);
//                    FirebaseUtils.storeInFirebase(this, CameraUtils.getImageUri(capturedImage, this), Constants.CHAT_IMAGE);
                    try {
                        Bitmap thumbnails;
                        String compressedImage = new CameraUtils()
                                .compressImage(this, mCurrentPhotoPath);
                        Uri imageUri = Uri.parse(compressedImage);

                        File file = new File(imageUri.getPath());
                        try {
                            InputStream ims1 = new FileInputStream(file);
                            thumbnails = BitmapFactory.decodeStream(ims1);
//                            sendChatMessage("Image", Constants.IMAGE, null, time);
//                            Reusable.storeIntoDisk(thumbnails, msgKey, "Chat Images");
                            FirebaseUtils.storeInFirebase(this, CameraUtils.getImageUri(thumbnails, this), Constants.CHAT_IMAGE);
                        } catch (FileNotFoundException e) {
                            return;
                        }
                        //thumbnails = (Bitmap) data.getExtras().get("data");
                        //thumbnails = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Constants.SELECT_ANY_FILE:
                    try {
                        String type = CameraUtils.getUriType(data.getData(), this);
                        if (type.equals("image/jpeg") || type.equals("image/png") || type.equals("image/jpg") || type.equals("application/pdf")) {
                            if (type.equals("application/pdf")) {
                                FirebaseUtils.storeInFirebase(ChatActivity.this, data.getData(), Constants.CHAT_DOCUMENTS);
                            } else {
                                FirebaseUtils.storeInFirebase(ChatActivity.this, data.getData(), Constants.CHAT_IMAGE);
                            }
                        } else {
                            DialogUtils.appToast(ChatActivity.this, getString(R.string.err_format_not_supported));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void fetchAllprescriptions(String consultID) {
        //  DialogUtils.showProgressDialog(this, "prescription dialog");
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
        Call<FindPrescriptionModel> call = apiService.getPrescriptions(consultID, null);
        call.enqueue(new Callback<FindPrescriptionModel>() {
            @Override
            public void onResponse(Call<FindPrescriptionModel> call, Response<FindPrescriptionModel> response) {
                // DialogUtils.dismissProgressDialog();
                try {
                    if (response.isSuccessful()) {
                        prescriptionData = response.body().getData();
                        medicationsList = new ArrayList<>();
                        for (int i = 0; i < prescriptionData.size(); i++) {
                            if (prescriptionData.get(i).getType().equals(Constants.PRESCRIPTION))
                                medicationsList = prescriptionData.get(i).getMedication();
                            else if (prescriptionData.get(i).getType().equals(Constants.DIAGNOSTIC))
                                diagnosticsList = prescriptionData.get(i).getDiagnostic();
                        }

                    } else {
                        DialogUtils.appToast(ChatActivity.this, "No data found");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<FindPrescriptionModel> call, Throwable t) {
                // Log error here since request failed
                //      DialogUtils.dismissProgressDialog();
            }


        });

    }

    private void callCloseConsultation() {
        if (status.equalsIgnoreCase(Constants.STATUS_COMPLETED) || isCompleted) {
            Toast.makeText(this, "This consultation is already completed!", Toast.LENGTH_SHORT).show();
        } else {
            String doctorName = SharePref.getDoctorDetail(this).getData().getFirstName();
            // String status = "Closed";
            String status = "Completed";
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("consultationId", consultationId);
            jsonObject.addProperty("firstName", doctorName);
            jsonObject.addProperty("status", status);
            DialogUtils.showProgressDialog(this, getString(R.string.pleaseWaittxt));
            ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

            Call<JsonObject> call = apiService.closeConsultation(jsonObject);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {

                        if (response.isSuccessful()) {
                            DialogUtils.appToast(ChatActivity.this, response.body().get("message").getAsString());
                            isCompleted = true;
                            //  getActivity().finish();
                        } else {
                            DialogUtils.appToast(ChatActivity.this, new JSONObject(response.errorBody().string()).getString("message"));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    DialogUtils.dismissProgressDialog();

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    // Log error here since request failed
                    DialogUtils.dismissProgressDialog();
                }


            });
        }

    }

    private void retrieveAllMessages(final String consultationId) {

        try {
            messagesReference = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_STORAGE).child(consultationId);
            messagesReference.keepSynced(true);
            Query chatQuery = messagesReference.orderByChild("timeStamp");

            chatQuery.keepSynced(true);
            chatQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    try {

                        HashMap<String, Object> history = (HashMap<String, Object>) dataSnapshot.getValue();
                        assert history != null;

                        if (history.containsKey(Constants.MSG_TYPE)) {
                            if ((history.containsValue(Constants.DOCTOR_IMAGE))
                                    || (history.containsValue(Constants.HEALTH_RECORD_MSGTYPE))
                                    || (history.containsValue(Constants.DOCTOR_DOCUMENT))
                                    || (history.containsValue("text") || history.containsValue("image")
                                    || history.containsValue("userText")
                                    || history.containsValue("prescription")
                                    || history.containsValue(Constants.DIAGNOSTIC)
                                    || history.containsValue("userIntro")
                                    || history.containsValue(Constants.TYPE_DOCUMENT))
                                    || history.containsValue(Constants.TYPE_CALL_REQUEST)) {
                                chatHistory.add(new ChatModel(dataSnapshot.getKey(), history));
                            }
                            if (history.containsValue("timeStamp")) {
                                timeStamp = history.get("timeStamp").toString();
                            }

                            if (history.get("msgType").toString()
                                    .equalsIgnoreCase(Constants.PRESCRIPTION)) {
                                prescriptionUpdateKey = dataSnapshot.getKey();
                            }
                            if (history.get("msgType").equals(Constants.DIAGNOSTIC)) {
                                diagnosticUpdateKey = dataSnapshot.getKey();
                            }
                            if (history.get("msgType").equals(Constants.USER_INTRO)) {
                                patientId = history.get("_id").toString();
                                if (history.containsKey("data")) {
                                    //HashMap<String, Object> data = (HashMap<String, Object>) history.get("data");
                                    HashMap<String, Object> text = (HashMap<String, Object>) history.get("data");
                                    patientData = text.get("relativesName").toString()
                                            + text.get("relativesAge")
                                            + text.get("relativesGender").toString();
                                    if (text.containsKey("mobile")) {
                                        if (text.get("mobile") != null && text.get("mobile") != "") {
                                            patientMobile = text.get("mobile").toString();
                                        }
                                    }
                                }
                            }


                            adapter.notifyDataSetChanged();
                            chatRV.scrollToPosition(chatHistory.size() - 1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    HashMap<String, Object> history = (HashMap<String, Object>) dataSnapshot.getValue();
                    assert history != null;
                    if (history != null) {
                        String msgType = history.get(Constants.MSG_TYPE).toString();
                        if (msgType.equalsIgnoreCase(Constants.TYPING)) {
                            for (String receiverKey : history.keySet()) {
                                if (!receiverKey.equalsIgnoreCase(Constants.MSG_TYPE) &&
                                        !receiverKey.equalsIgnoreCase(doctorId)) {
                                    if ((boolean) history.get(receiverKey)) {
                                        getSupportActionBar().setSubtitle("typing");
                                    } else {
                                        getSupportActionBar().setSubtitle("");
                                    }
                                }
                            }
                        } else if (msgType.equalsIgnoreCase(Constants.PRESCRIPTION) || msgType.equalsIgnoreCase(Constants.DIAGNOSTIC)) {
                            for (int i = chatHistory.size() - 1; i >= 0; i--) {
                                if (chatHistory.get(i).getKey().equals(dataSnapshot.getKey())) {
                                    chatHistory.remove(i);
                                    break;
                                }
                            }
                            chatHistory.add(new ChatModel(dataSnapshot.getKey(), history));
                            adapter.notifyDataSetChanged();
                        } else {
                            for (int i = chatHistory.size() - 1; i >= 0; i--) {
                                if (chatHistory.get(i).getKey().equals(dataSnapshot.getKey())) {
                                    chatHistory.set(i, new ChatModel(dataSnapshot.getKey(), history));
                                    break;
                                }
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            populateView(chatHistory);
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
        updateUserStatus(Constants.OFFLINE);
    }

    @OnClick(R.id.send_button)
    public void sendMessage() {
        if (!chatInputET.getText().toString().equals("") || !chatInputET.getText().toString().isEmpty()) {
            sendMessageFirebase(null, Constants.TEXT);
        }
    }


    private void updateUserStatus(String status) {
        final String patientId = SharePref.getDoctorID(ChatActivity.this);
        DatabaseReference usersReference = FirebaseDatabase.getInstance().getReference().child("Users").child(patientId);
        HashMap<String, Object> userStatus = new HashMap<>();
        userStatus.put("_id", patientId);
        userStatus.put("status", status);
        userStatus.put("timeStamp", ServerValue.TIMESTAMP);
        usersReference.updateChildren(userStatus);
//showNotification();
    }

    private void sendMessageFirebase(String url, String text) {
        String doctorID = SharePref.getDoctorID(ChatActivity.this);
        HashMap<String, Object> newMessage = new HashMap<>();
        newMessage.put("_id", doctorID);
        newMessage.put("from", Constants.DOCTOR);
        newMessage.put("status", "sent");
        newMessage.put("sender", doctorName);
        newMessage.put("timeStamp", ServerValue.TIMESTAMP);
        newMessage.put("receiver", patientId);
        switch (text) {
            case Constants.TEXT:
                String msg = chatInputET.getText().toString();
                if (!msg.isEmpty()) {
                    newMessage.put("msgType", Constants.TEXT);
                    newMessage.put("data", msg);
                }
                break;
            case Constants.CHAT_IMAGE:
                if (url != null) {
                    newMessage.put("msgType", Constants.DOCTOR_IMAGE);
//                HashMap<String, Object> data = new HashMap<>();
//                data.put("url", url);
//                data.put("name", "Example");
                    newMessage.put("data", url);
                }
                break;
            case Constants.CHAT_DOCUMENTS:
                newMessage.put("msgType", Constants.DOCTOR_DOCUMENT);
                HashMap<String, Object> data = new HashMap<>();
                data.put("documentUrl", url);
                data.put("documentName", "Document");
                newMessage.put("data", data);
                break;
        }
        messageSentSound();
        messagesReference.push().updateChildren(newMessage);
        chatInputET.setText("");


    }

    @OnClick(R.id.chat_prescribe_medicine_TV)
    public void prescribeMedicineClick() {
        if (consultationId != null && !consultationId.isEmpty() && medicationsList != null) {
            Intent intent = new Intent(ChatActivity.this, PrescriptionActivity.class);
            intent.putExtra(Constants.CONSULTATION_ID, consultationId);
            intent.putExtra(Constants.PATIENT_DEATAIL, patientData);
            intent.putExtra(Constants.TIME_STAMP, timeStamp);
            intent.putExtra(Constants.SOURCE, Constants.CHAT);
            if (medicationsList != null) {
                PrescriptionActivity.getMedicationsData(medicationsList);
            }
            //  intent.putExtra(Constants.PRESCRIPTION, (Serializable) medicationsList);
            startActivity(intent);
        }


    }

    @OnClick(R.id.chat_diagnostic_TV)
    public void requestDiagnostic() {
        if (consultationId != null && !consultationId.isEmpty() && diagnosticsList != null) {
            Intent intent = new Intent(ChatActivity.this, DiagnosticActivity.class);
            intent.putExtra(Constants.CONSULTATION_ID, consultationId);
            intent.putExtra(Constants.PATIENT_DEATAIL, patientData);
            intent.putExtra(Constants.TIME_STAMP, timeStamp);
            intent.putExtra(Constants.SOURCE, Constants.DIAGNOSTICS);
            if (diagnosticsList != null) {
                DiagnosticActivity.getDiagnosticsData(diagnosticsList);
            }
            startActivity(intent);
        }


    }

    private void populateView(List<ChatModel> chatHistory) {
        adapter = new ChatAdapter(this, chatHistory, consultationId, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        chatRV.removeAllViews();
        chatRV.setLayoutManager(layoutManager);
        chatRV.setItemAnimator(new DefaultItemAnimator());
        chatRV.setAdapter(adapter);
        chatRV.scrollToPosition(chatHistory.size() - 1);
        chatRV.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            //  chatRV.scrollToPosition(chatHistory.size() - 1);
        });
        if (chatRV.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) chatRV
                    .getLayoutManager();


            chatRV
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            try {
                                totalItemCount = linearLayoutManager.getItemCount();
                                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                                Log.e("totalItem", String.valueOf(totalItemCount));
                                Log.e("firstVisItem", String.valueOf(firstVisibleItem));
                                Log.e("lastVisItem", String.valueOf(lastVisibleItem));

                                //Msg Receive Sound
                                ChatModel lastChats = chatHistory.get(lastVisibleItem);
                                HashMap<String, Object> lastChatList = (HashMap<String, Object>) lastChats.getChatListMap();
                                String msgType = (String) lastChatList.get(Constants.MSG_TYPE);
                                if (msgType.equalsIgnoreCase(Constants.USER_TEXT) ||
                                        msgType.equalsIgnoreCase(Constants.ALERT) ||
                                        msgType.equalsIgnoreCase(Constants.IMAGE) ||
                                        msgType.equalsIgnoreCase(Constants.TYPE_DOCUMENT) ||
                                        msgType.equalsIgnoreCase(Constants.HEALTH_RECORD_MSGTYPE) ||
                                        msgType.equalsIgnoreCase(Constants.TYPE_CALL_REQUEST)) {
                                    String status = (String) lastChatList.get("status");
                                    if (!status.equalsIgnoreCase("read")) {
                                        messageReceivedSound();
                                    }
                                }

                                //Make unread msg to read
                                int initialItem = 0;
                                DialogUtils.showProgressDialog(ChatActivity.this, "Loading...");
                                for (int visibleItem = initialItem;
                                     visibleItem <= lastVisibleItem;
                                     visibleItem++) {
                                    ChatModel chats = chatHistory.get(visibleItem);
                                    HashMap<String, Object> chatList = (HashMap<String, Object>) chats.getChatListMap();

                                    //changing status
                                    msgType = (String) chatList.get(Constants.MSG_TYPE);
                                    if (msgType.equalsIgnoreCase(Constants.USER_TEXT) ||
                                            msgType.equalsIgnoreCase(Constants.IMAGE) ||
                                            msgType.equalsIgnoreCase(Constants.TYPE_DOCUMENT) ||
                                            msgType.equalsIgnoreCase(Constants.HEALTH_RECORD_MSGTYPE) ||
                                            msgType.equalsIgnoreCase(Constants.TYPE_CALL_REQUEST)) {
                                        String status = (String) chatList.get("status");
                                        if (!status.equalsIgnoreCase("read")) {
                                            changeStatusToRead(chats.getKey());
                                        }
                                    }
                                }
                                DialogUtils.dismissProgressDialog();

                                //Show time in the top
                                ChatModel firstChats = chatHistory.get(firstVisibleItem);
                                HashMap<String, Object> firstChatList = (HashMap<String, Object>) firstChats.getChatListMap();
                                long timeStamp = (long) firstChatList.get("timeStamp");
                                setTimeTextVisibility(timeStamp);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == 0)
                                setTimeTextVisibility(0);
                            Log.e("newState", String.valueOf(newState));
                        }
                    });
        }


    }

    private void messageReceivedSound() {
        try {
            Uri sentMsgUri = Uri.parse("android.resource://"
                    + getPackageName() + "/"
                    + R.raw.msg_received);
            Ringtone r = RingtoneManager.getRingtone(this, sentMsgUri);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeStatusToRead(String key) {
        DatabaseReference messagesReference = FirebaseDatabase.getInstance().getReference()
                .child(Constants.FIREBASE_STORAGE).child(consultationId);
        messagesReference.child(key).child("status").setValue("read");
    }

    private void setTimeTextVisibility(long ts1) {

        if (ts1 == 0) {
            onTimeChangeListener.OnTimeChange("");
        } else {
            //today
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date today = cal.getTime();


            Calendar cal1 = Calendar.getInstance();
            cal1.setTimeInMillis(ts1);


            if (onTimeChangeListener != null) {
                String timeStamp = DateTimeUtility.getChatGlobalTime(ts1);
                Date dateSpecified = cal1.getTime();
                if (dateSpecified.getYear() == today.getYear() &&
                        dateSpecified.getMonth() == today.getMonth() &&
                        dateSpecified.getDate() == today.getDate()) {
                    timeStamp = "Today";
                } else if (dateSpecified.getYear() == today.getYear() &&
                        dateSpecified.getMonth() == today.getMonth() &&
                        dateSpecified.getDate() == today.getDate() - 1) {
                    timeStamp = "Yesterday";
                }
                onTimeChangeListener.OnTimeChange(timeStamp);
            }
        }
    }

    public static void sendPrescriptionData(
            PrescriptionPostModel prescriptionPostModel,
            String prescriptionId, List<String> educations,
            PatientDetailModel patientDetailModel,
            String source) {
        if (prescriptionPostModel.getType().equals(Constants.DIAGNOSTIC)) {
            DoctorProfileModel doctorProfileModel = new DoctorProfileModel();
            doctorProfileModel.setFirstName(doctorName);
            doctorProfileModel.setDuns(duns);
            doctorProfileModel.setCategory(specializations);
            PatientDetailModel patientDetails = new PatientDetailModel();
            if (patientDetailModel != null) {
                patientDetails.setName(patientDetailModel.getName());
                patientDetails.setAge(patientDetailModel.getAge());
                patientDetails.setGender(patientDetailModel.getGender());
            } else {
                patientDetails.setName("");
                patientDetails.setAge("");
                patientDetails.setGender("");
            }
            if (educations != null) {
                doctorProfileModel.setQualification(educations);
            }
            FirebasePrescriptionModel firebasePrescriptionModel = new FirebasePrescriptionModel();
            firebasePrescriptionModel.setDoctorProfile(doctorProfileModel);
            firebasePrescriptionModel.setDiagnosticId(prescriptionId);
            firebasePrescriptionModel.setDiagnostic(prescriptionPostModel.getDiagnostic());
            firebasePrescriptionModel.setAdditionalDetails(patientDetails);
            HashMap<String, Object> newMessage = new HashMap<>();
            newMessage.put("_id", doctorId);
            newMessage.put("from", Constants.DOCTOR);
            newMessage.put("msgType", Constants.DIAGNOSTIC);
            newMessage.put("status", "sent");
            newMessage.put("sender", doctorName);
            newMessage.put("timeStamp", ServerValue.TIMESTAMP);
            newMessage.put("receiver", patientId);
//        HashMap<String, Object> dataMap = new HashMap<>();
//        dataMap.put("text", firebasePrescriptionModel);
            newMessage.put("data", firebasePrescriptionModel);
            // newMessage.put("data", dataMap);
            //  if (source.contains(Constants.SEND)) {
            if (source.contains(Constants.SEND)) {
                if (diagnosticUpdateKey != null && !diagnosticUpdateKey.equals("")) {
                    messagesReference.child(diagnosticUpdateKey).updateChildren(newMessage);
                } else {
                    messagesReference.push().updateChildren(newMessage);
                }
            } else if (source.contains(Constants.EDIT_DIAGNOSTIC)) {
                if (diagnosticUpdateKey != null && !diagnosticUpdateKey.equals("")) {
                    messagesReference.child(diagnosticUpdateKey).updateChildren(newMessage);
                } else {
                    messagesReference.push().updateChildren(newMessage);
                }
            } else if (source.contains(Constants.DIAGNOSTIC_VIEW)) {
                messagesReference.push().updateChildren(newMessage);
            }
            //  messagesReference.push().updateChildren(newMessage);
            //  }
//            else if (source.contains(Constants.EDIT_PRESCRIPTION)) {
//                messagesReference.child(prescriptionUpdateKey).updateChildren(newMessage);
//            } else if (source.contains(Constants.VIEW)) {
//                messagesReference.push().updateChildren(newMessage);
//            }
            ChatActivity.qualifications = null;

        } else if (prescriptionPostModel.getType().equals("prescription")) {
            DoctorProfileModel doctorProfileModel = new DoctorProfileModel();
            doctorProfileModel.setFirstName(doctorName);
            doctorProfileModel.setDuns(duns);
            doctorProfileModel.setCategory(specializations);
            PatientDetailModel patientDetails = new PatientDetailModel();
            if (patientDetailModel != null) {
                patientDetails.setName(patientDetailModel.getName());
                patientDetails.setAge(patientDetailModel.getAge());
                patientDetails.setGender(patientDetailModel.getGender());
            } else {
                patientDetails.setName("");
                patientDetails.setAge("");
                patientDetails.setGender("");
            }
            if (educations != null) {
                doctorProfileModel.setQualification(educations);
            }
            FirebasePrescriptionModel firebasePrescriptionModel = new FirebasePrescriptionModel();
            firebasePrescriptionModel.setDoctorProfile(doctorProfileModel);
            firebasePrescriptionModel.setPrescriptionId(prescriptionId);
            firebasePrescriptionModel.setMedication(prescriptionPostModel.getMedication());
            firebasePrescriptionModel.setAdditionalDetails(patientDetails);
//        List<MedicationModel> medicationModel = prescriptionPostModel.getMedication();
            HashMap<String, Object> newMessage = new HashMap<>();
            newMessage.put("_id", doctorId);
            newMessage.put("from", Constants.DOCTOR);
            newMessage.put("msgType", Constants.PRESCRIPTION);
            newMessage.put("status", "sent");
            newMessage.put("sender", doctorName);
            newMessage.put("timeStamp", ServerValue.TIMESTAMP);
            newMessage.put("receiver", patientId);
//        HashMap<String, Object> dataMap = new HashMap<>();
//        dataMap.put("text", firebasePrescriptionModel);
            newMessage.put("data", firebasePrescriptionModel);
            // newMessage.put("data", dataMap);
            if (source.contains(Constants.SEND)) {
                if (prescriptionUpdateKey != null && !prescriptionUpdateKey.equals("")) {
                    messagesReference.child(prescriptionUpdateKey).updateChildren(newMessage);
                } else {
                    messagesReference.push().updateChildren(newMessage);
                }
            } else if (source.contains(Constants.EDIT_PRESCRIPTION)) {
                if (prescriptionUpdateKey != null && !prescriptionUpdateKey.equals("")) {
                    messagesReference.child(prescriptionUpdateKey).updateChildren(newMessage);
                } else {
                    messagesReference.push().updateChildren(newMessage);
                }
            } else if (source.contains(Constants.PRESCRIPTION_VIEW)) {
                messagesReference.push().updateChildren(newMessage);
            }
            ChatActivity.qualifications = null;

        }
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_upload_imageTV:
                if (!AndroidVersionUtility.isLollipopOrLess()) {
                    if (Reusable.checkPermission(ChatActivity.this, Constants.REQUEST_GALLERY_STORAGE)) {
                        chooseImage();
                    }
                } else chooseImage();
                break;
            case R.id.chat_take_imageTV:
                if (!AndroidVersionUtility.isLollipopOrLess()) {
                    if (Reusable.checkPermission(ChatActivity.this, Constants.REQUEST_CAMERA_STORAGE)) {
                        takeImage();
                    }
                } else takeImage();
                break;
            case R.id.chat_upload_documentTV:
                Intent intent2 = new Intent();
                intent2.setType("*/*");
                intent2.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent2, "Select File"), Constants.SELECT_ANY_FILE);
                break;
        }
    }


    @Override
    public void getDownloadUrl(String url) {

    }

    @Override
    public void getDownloadUrlType(String url, String type) {
        if (url != null) {
            sendMessageFirebase(url, type);
        }
    }

    @Override
    public void getDeleteResponse(String url, String tag) {

    }

    @Override
    public void OnTimeChange(String timeStamp) {
        // final TextView timeGlobal = (TextView) findViewById(R.id.time_global);
        if (timeStamp.isEmpty()) {
            timestampTV.setVisibility(View.GONE);
        } else {
            timestampTV.setVisibility(View.VISIBLE);
            timestampTV.setText(timeStamp);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        //intent.putExtra("refresh",true);
        setResult(Activity.RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    private void messageSentSound() {
        try {
            Uri sentMsgUri = Uri.parse("android.resource://"
                    + getPackageName() + "/"
                    + R.raw.msg_sent);
            Ringtone r = RingtoneManager.getRingtone(this, sentMsgUri);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takeImage() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile;
                photoFile = createImageFile();
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI;
                    if (Build.VERSION.SDK_INT <= 23) {
                        photoURI = Uri.fromFile(createImageFile());
                    } else {
                        photoURI = FileProvider.getUriForFile(this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                createImageFile());
                    }
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, Constants.TAKE_IMAGE);
                }
            }
        } catch (IOException ex) {
            Log.e("Error", ex.getMessage());
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select file"), Constants.SELECT_ANY_FILE);
    }

    @SuppressLint("SimpleDateFormat")
    private File createImageFile() throws IOException {
    /*File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/Doxtro/" + "Chat Images" + "/");
    dir.mkdirs();

    // Create a name for the saved image
    return new  File(dir, path + ".jpg");*/

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/Doxtro/" + "Chat Images" + "/");

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CAMERA_STORAGE: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for all permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        // Log.d(TAG, "Camera and storage permission granted");
                        takeImage();
                    } else {
                        // Log.d(TAG, "Some permissions are not granted ask again");
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(this, "Go to App settings and enable Camera and Storage permissions", Toast.LENGTH_LONG).show();
                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
            break;
            case Constants.REQUEST_GALLERY_STORAGE:
                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for all permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        // Log.d(TAG, "Camera and storage permission granted");
                        chooseImage();
                        // process the normal flow   //else any one or both the permissions are not granted
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(this, "Go to App settings and enable Camera and Storage permissions", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                break;
            default:
                break;
        }

    }

    @Override
    public void callReguested(Boolean isRequested) {
        if (isRequested) {
            if (!status.equalsIgnoreCase(Constants.CLOSED)) {
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to start the audio consultations with the patient?")
                        .setNegativeButton(getString(R.string.cancelTxt), null)
                        .setPositiveButton(getString(R.string.yestext), (dialog, which) -> {
                            if (NetworkUtil.getConnectionStatus(this) != 0) {
                                callExoticConnectApi();
                            } else {

                                DialogUtils.appToast(this, getString(R.string.no_internet_connection));
                            }

                        })
                        .show();
            }
            else {
                DialogUtils.appToast(this, "Sorry this consulatation is closed.You can not make a call!");
            }
        }
    }
}
