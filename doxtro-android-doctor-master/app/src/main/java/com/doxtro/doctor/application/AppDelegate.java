package com.doxtro.doctor.application;

import android.app.Application;
import android.util.Log;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.clevertap.android.sdk.ActivityLifecycleCallback;

import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 25/10/17.
 */

public class AppDelegate extends Application implements AppsFlyerConversionListener {

    private static final String AF_DEV_KEY = "rQXTs7AqLD4mPLR6EsYK43";

    @Override
    public void onCreate() {
        ActivityLifecycleCallback.register(this);
        super.onCreate();
//
//        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/Asap-Regular.otf");
//        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/Asap-Regular.otf");

        AppsFlyerLib.getInstance().setCollectIMEI(false);
        AppsFlyerLib.getInstance().setCollectAndroidID(false);
        AppsFlyerLib.getInstance().setDebugLog(true);
        AppsFlyerLib.getInstance().startTracking(this, AF_DEV_KEY);
    }


    @Override
    public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
        for (String attrName : conversionData.keySet()) {
            Log.d(AppsFlyerLib.LOG_TAG, "attribute: " + attrName + " = " +
                    conversionData.get(attrName));
        }
    }

    @Override
    public void onInstallConversionFailure(String errorMessage) {
        Log.d(TAG, "error getting conversion data: " + errorMessage);
    }

    @Override
    public void onAppOpenAttribution(Map<String, String> conversionData) {

    }

    @Override
    public void onAttributionFailure(String errorMessage) {
        Log.d(TAG, "error getting conversion data: " + errorMessage);
    }
}
