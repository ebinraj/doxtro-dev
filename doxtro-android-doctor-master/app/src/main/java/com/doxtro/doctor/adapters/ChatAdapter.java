package com.doxtro.doctor.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.HealthRecordActivity;
import com.doxtro.doctor.activities.ImageZoomingActivity;
import com.doxtro.doctor.activities.ViewPrescriptionACtivity;
import com.doxtro.doctor.custom.CircleImageView;
import com.doxtro.doctor.interfaces.RequestCallListener;
import com.doxtro.doctor.model.ChatModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DateTimeUtility;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.Reusable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by yashaswi on 11/09/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final RequestCallListener requestCallListener;
    private Context mContext;
    private List<ChatModel> chats = new ArrayList<>();
    private String consultationID;
    private int type;
    private String TAG;
    private String patientInfoDetails = "";
    private String docDegree = "", regNo = "";
    private String specializations = "";


    public ChatAdapter(Context context, List<ChatModel> chatHistory, String consultationId, RequestCallListener requestCallListener) {
        mContext = context;
        chats = chatHistory;
        consultationID = consultationId;
        this.requestCallListener = requestCallListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case 1:
                viewHolder = new BotAlertViewHolder(inflater.inflate(R.layout.chat_cell_alert_message, parent, false));
                break;
            case 2:
                viewHolder = new DocTextViewHolder(inflater.inflate(R.layout.chat_cell_user_message, parent, false));
                break;
            case 3:
                viewHolder = new ImageViewHolder(inflater.inflate(R.layout.chat_cell_image_message, parent, false));
                break;
            case 4:
                viewHolder = new UserTextViewHolder(inflater.inflate(R.layout.chat_cell_doc_message, parent, false));
                break;
            case 5:
                viewHolder = new PrescriptionViewHolder(inflater.inflate(R.layout.chat_cell_prescription, parent, false));
                break;
            case 6:
                viewHolder = new UserIntroViewHolder(inflater.inflate(R.layout.chat_cell_user_intro, parent, false));
                break;
            case 7:
                viewHolder = new DiagnosticViewHolder(inflater.inflate(R.layout.chat_cell_prescription, parent, false));
                break;
            case 8:
                viewHolder = new DocumentsViewHolder(inflater.inflate(R.layout.chat_cell_user_documents_message, parent, false));
                break;
            case 9:
                viewHolder = new ImageViewHolder(inflater.inflate(R.layout.chat_cell_doctor_image_message, parent, false));
                break;
            case 10:
                viewHolder = new DocumentsViewHolder(inflater.inflate(R.layout.chat_cell_documents_message, parent, false));
                break;
            case 11:
                viewHolder = new HealthRecordsViewHolder(inflater.inflate(R.layout.chat_cell_health_record, parent, false));
                break;
            case 12:
                viewHolder = new CallRequestViewHolder(inflater.inflate(R.layout.chat_cell_call_request, parent, false));
                break;
            default:
                break;

        }

//        if (viewHolder != null)
//            setAnimation(viewHolder.itemView, viewType);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder != null) {
            ChatModel chat = chats.get(position);
            HashMap<String, Object> chatList = (HashMap<String, Object>) chat.getChatListMap();

            String msgType = (String) chatList.get(Constants.MSG_TYPE);

            switch (msgType) {
                case Constants.TYPING:
                    alertMessage((BotAlertViewHolder) holder, chatList, position);
                    break;
                case Constants.TEXT:
                    docTextMessage((DocTextViewHolder) holder, chatList, position);
                    break;
                case Constants.IMAGE:
                    userImageMessage((ImageViewHolder) holder, chatList, position);
                    break;
                case Constants.USER_TEXT:
                    userTextMessage((UserTextViewHolder) holder, chatList, position);
                    break;
                case Constants.PRESCRIPTION:
                    prescriptionMessage((PrescriptionViewHolder) holder, chatList, position);
                    break;
                case Constants.USER_INTRO:
                    userIntroMessage((UserIntroViewHolder) holder, chatList, position);
                    break;
                case Constants.DIAGNOSTIC:
                    diagnosticMessage((DiagnosticViewHolder) holder, chatList, position);
                    break;
                case Constants.TYPE_DOCUMENT:
                    documentsMessage((DocumentsViewHolder) holder, chatList, position, Constants.TYPE_DOCUMENT);
                    break;
                case Constants.DOCTOR_IMAGE:
                    doctorImageMessage((ImageViewHolder) holder, chatList, position);
                    break;
                case Constants.DOCTOR_DOCUMENT:
                    documentsMessage((DocumentsViewHolder) holder, chatList, position, Constants.DOCTOR_DOCUMENT);
                    break;
                case Constants.HEALTH_RECORD_MSGTYPE:
                    healthRecordMessage((HealthRecordsViewHolder) holder, chatList, position);
                    break;
                case Constants.TYPE_CALL_REQUEST:
                    callRequestedMessage((CallRequestViewHolder) holder, chatList, position);
                    break;


            }
        }
    }


    @Override
    public int getItemViewType(int position) {

        ChatModel chat = chats.get(position);
        HashMap<String, Object> chatList = (HashMap<String, Object>) chat.getChatListMap();

        /*
        1 = alert
        2 = text
        3 = image
        4 = userText
        5 = profile
        6 = payment
         */

        try {

            String msgType = (String) chatList.get(Constants.MSG_TYPE);

            if (msgType.equalsIgnoreCase(Constants.TYPING)) {
                return 1;
            } else if (msgType.equalsIgnoreCase(Constants.TEXT)) {
                return 2;
            } else if (msgType.equalsIgnoreCase(Constants.IMAGE)) {
                return 3;
            } else if (msgType.equalsIgnoreCase(Constants.USER_TEXT)) {
                return 4;
            } else if (msgType.equalsIgnoreCase(Constants.PRESCRIPTION)) {
                return 5;
            } else if (msgType.equalsIgnoreCase(Constants.USER_INTRO)) {
                return 6;
            } else if (msgType.equalsIgnoreCase(Constants.DIAGNOSTIC)) {
                return 7;
            } else if (msgType.equalsIgnoreCase(Constants.TYPE_DOCUMENT)) {
                return 8;
            } else if (msgType.equalsIgnoreCase(Constants.DOCTOR_IMAGE)) {
                return 9;
            } else if (msgType.equalsIgnoreCase(Constants.DOCTOR_DOCUMENT)) {
                return 10;
            } else if (msgType.equalsIgnoreCase(Constants.HEALTH_RECORD_MSGTYPE)) {
                return 11;
            } else if (msgType.equalsIgnoreCase(Constants.TYPE_CALL_REQUEST)) {
                return 12;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    private class DocumentsViewHolder extends RecyclerView.ViewHolder {

        TextView tvDocName, tvDocTime;
        ImageView ivDocStatus;
        LinearLayout documentView;

        private DocumentsViewHolder(View itemView) {
            super(itemView);
            tvDocName = (TextView) itemView.findViewById(R.id.tv_docs_name);
            tvDocTime = (TextView) itemView.findViewById(R.id.time_chat_documents);
            ivDocStatus = (ImageView) itemView.findViewById(R.id.iv_msg_status_documents);
            documentView = (LinearLayout) itemView.findViewById(R.id.document_detailLL);
        }
    }

    private class HealthRecordsViewHolder extends RecyclerView.ViewHolder {
        TextView messegeTime, messageTV, titleTV;
        ImageView expandIcon;

        public HealthRecordsViewHolder(View itemView) {
            super(itemView);
            messegeTime = (TextView) itemView.findViewById(R.id.chat_health_records_time);
            expandIcon = (ImageView) itemView.findViewById(R.id.iv_expand_presc);
            messageTV = (TextView) itemView.findViewById(R.id.tv_msg_health_record);
            titleTV = (TextView) itemView.findViewById(R.id.chat_prescription_item_title);

        }
    }


    private void documentsMessage(final DocumentsViewHolder holder, HashMap<String, Object> chatList, int position, String source) {
        try {
            HashMap<String, Object> data = (HashMap<String, Object>) chatList.get("data");
            if (data.containsKey("documentUrl") && data.containsKey("documentName")) {
                final String url = data.get("documentUrl").toString();
                final String name = data.get("documentName").toString();
                long timeStamp = (long) chatList.get("timeStamp");
                holder.tvDocTime.setText(DateTimeUtility.getChatTime(timeStamp));
//                if (source.equals(Constants.DOCTOR_IMAGE)) {
//                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                    layoutParams.gravity = Gravity.END;
//                    holder.documentView.setLayoutParams(layoutParams);
//                }
                holder.tvDocName.setText(name);
                holder.itemView.setOnClickListener(view -> {
                    if (!url.startsWith("http://") || !url.startsWith("https://")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        mContext.startActivity(browserIntent);
                    }
                });

                if (chatList.containsKey("status")) {
                    String msgStatus = chatList.get("status").toString();

                    switch (msgStatus) {
                        case "delivered":
                            holder.ivDocStatus.setImageResource(R.mipmap.done_all);
                            break;
                        case "read":
                            holder.ivDocStatus.setImageResource(R.mipmap.done_all_blue);
                            break;
                        case "sent":
                            holder.ivDocStatus.setImageResource(R.mipmap.done);
                            break;
                        default:
                            holder.ivDocStatus.setImageResource(R.mipmap.done);
                            break;
                    }
                } else {
                    holder.ivDocStatus.setImageResource(R.mipmap.done);
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void healthRecordMessage(HealthRecordsViewHolder holder, HashMap<String, Object> chatList, int position) {
        try {
            String data = chatList.get("data").toString();
            long timeStamp = (long) chatList.get("timeStamp");
            holder.titleTV.setText(mContext.getString(R.string.health_record));
            holder.messageTV.setText(mContext.getString(R.string.health_record_msg));
            holder.messegeTime.setText(DateTimeUtility.getChatTime(timeStamp));
            holder.expandIcon.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, HealthRecordActivity.class);
                intent.putExtra(Constants.RELATIVE_ID, data);
                mContext.startActivity(intent);
            });
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, HealthRecordActivity.class);
                intent.putExtra(Constants.RELATIVE_ID, data);
                mContext.startActivity(intent);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callRequestedMessage(CallRequestViewHolder holder, HashMap<String, Object> chatList, int position) {
        long timeStamp = (long) chatList.get("timeStamp");
        holder.itemView.setOnClickListener(v -> requestCallListener.callReguested(true));
        holder.timeStamp.setText(DateTimeUtility.getChatTime(timeStamp));

    }


    private class BotAlertViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profileImageBot;
        TextView botMessageTitle, botMessage, botMessageTime;

        private BotAlertViewHolder(View itemView) {
            super(itemView);
            profileImageBot = (CircleImageView) itemView.findViewById(R.id.bot_msg_profile_image);
            botMessageTitle = (TextView) itemView.findViewById(R.id.chat_assistant_title);
            botMessage = (TextView) itemView.findViewById(R.id.chat_assistant_message);
            botMessageTime = (TextView) itemView.findViewById(R.id.chat_item_time);
        }
    }

    private class BotDefault extends RecyclerView.ViewHolder {

        public BotDefault(View itemView) {
            super(itemView);
        }
    }

    private class DocTextViewHolder extends RecyclerView.ViewHolder {

        TextView mDoctorMessage, mDoctorMessageTime;
        ImageView messageStatus;

        private DocTextViewHolder(View itemView) {
            super(itemView);
            //   mDoctorName = (TextView) itemView.findViewById(R.id.chat_doc_message_name);
            mDoctorMessage = (TextView) itemView.findViewById(R.id.message_user);
            mDoctorMessageTime = (TextView) itemView.findViewById(R.id.chat_doc_message_time);
            messageStatus = (ImageView) itemView.findViewById(R.id.ivMsgStatus);
        }

    }

    private class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView mChatImage, imageStatusIv;
        TextView mChatImageTime;

        private ImageViewHolder(View itemView) {
            super(itemView);
            mChatImage = (ImageView) itemView.findViewById(R.id.image_view_chat);
            mChatImageTime = (TextView) itemView.findViewById(R.id.time_chat_image);
            imageStatusIv = (ImageView) itemView.findViewById(R.id.iv_msg_status_image);
        }
    }

    private class CallRequestViewHolder extends RecyclerView.ViewHolder {
        TextView timeStamp;

        private CallRequestViewHolder(View itemView) {
            super(itemView);
            timeStamp = (TextView) itemView.findViewById(R.id.callRequestTimeTV);

        }
    }

    private class UserTextViewHolder extends RecyclerView.ViewHolder {

        TextView patientName, mUserMessage, mUserMessagsTime;

        private UserTextViewHolder(View itemView) {
            super(itemView);
            patientName = (TextView) itemView.findViewById(R.id.chat_doc_message_name);
            mUserMessage = (TextView) itemView.findViewById(R.id.chat_doc_message);
            mUserMessagsTime = (TextView) itemView.findViewById(R.id.chat_doc_message_time);
        }
    }

    private class PrescriptionViewHolder extends RecyclerView.ViewHolder {
        ImageView expandIocn;
        TextView title, docName, docQualifications, registrationNum, patientNameTV, date, prescriptionData, chatTime, specializations;


        private PrescriptionViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.chat_prescription_item_title);
            docName = (TextView) itemView.findViewById(R.id.doctor_name_presc);
            docQualifications = (TextView) itemView.findViewById(R.id.doc_edu_degree_presc);
            registrationNum = (TextView) itemView.findViewById(R.id.reg_number_presc);
            patientNameTV = (TextView) itemView.findViewById(R.id.txt_patient_name_presc);
            date = (TextView) itemView.findViewById(R.id.txt_date_presc);
            prescriptionData = (TextView) itemView.findViewById(R.id.txt_medicine_presc);
            expandIocn = (ImageView) itemView.findViewById(R.id.iv_expand_presc);
            chatTime = (TextView) itemView.findViewById(R.id.chat_doc_prescription_time);
            specializations = (TextView) itemView.findViewById(R.id.doctor_specialization_presc);


        }
    }

    private class DiagnosticViewHolder extends RecyclerView.ViewHolder {
        ImageView expandIocn, medicineIcon;
        TextView title, docName, docQualifications, registrationNum, patientNameTV, date, prescriptionData, specializationsTV, chatTime;


        private DiagnosticViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.chat_prescription_item_title);
            docName = (TextView) itemView.findViewById(R.id.doctor_name_presc);
            docQualifications = (TextView) itemView.findViewById(R.id.doc_edu_degree_presc);
            registrationNum = (TextView) itemView.findViewById(R.id.reg_number_presc);
            patientNameTV = (TextView) itemView.findViewById(R.id.txt_patient_name_presc);
            date = (TextView) itemView.findViewById(R.id.txt_date_presc);
            prescriptionData = (TextView) itemView.findViewById(R.id.txt_medicine_presc);
            expandIocn = (ImageView) itemView.findViewById(R.id.iv_expand_presc);
            medicineIcon = (ImageView) itemView.findViewById(R.id.iv_medicine_type_presc);
            specializationsTV = (TextView) itemView.findViewById(R.id.doctor_specialization_presc);
            chatTime = (TextView) itemView.findViewById(R.id.chat_doc_prescription_time);


        }
    }

    private class UserIntroViewHolder extends RecyclerView.ViewHolder {

        CircleImageView mProfilePic;
        TextView mChatTitle, mNameAgeGender, mTextConcern, mUserIntroTime;

        private UserIntroViewHolder(View itemView) {
            super(itemView);
            mProfilePic = (CircleImageView) itemView.findViewById(R.id.img_chat_first_msg);
            mChatTitle = (TextView) itemView.findViewById(R.id.chat_title);
            mNameAgeGender = (TextView) itemView.findViewById(R.id.name_age_gender_first_msg);
            mTextConcern = (TextView) itemView.findViewById(R.id.patient_health_concern);
            mUserIntroTime = (TextView) itemView.findViewById(R.id.first_msg_time);
        }
    }


    private void alertMessage(BotAlertViewHolder holder, HashMap<String, Object> chatList, int position) {
        try {
            String data = chatList.get("data").toString();
            holder.botMessage.setText(data);
            long timeStamp = (long) chatList.get("timeStamp");
            holder.botMessageTime.setText(DateTimeUtility.getChatTime(timeStamp));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void docTextMessage(DocTextViewHolder holder, HashMap<String, Object> chatList, int position) {
        try {
            String data = chatList.get("data").toString();
            long timeStamp = (long) chatList.get("timeStamp");
            holder.mDoctorMessage.setText(data);
            copyTextOnLongPress(holder.mDoctorMessage);
            // TODO replace Doctor with text Doctor text
            //  holder.mDoctorName.setTextColor(AndroidVersionUtility.getColor(g));
            holder.mDoctorMessageTime.setText(DateTimeUtility.getChatTime(timeStamp));
            if (chatList.containsKey("status")) {
                String msgStatus = chatList.get("status").toString();

                switch (msgStatus) {
                    case "delivered":
                        holder.messageStatus.setImageResource(R.mipmap.done_all);
                        break;
                    case "read":
                        holder.messageStatus.setImageResource(R.mipmap.done_all_blue);
                        break;
                    case "sent":
                        holder.messageStatus.setImageResource(R.mipmap.done);
                        break;
                    default:
                        holder.messageStatus.setImageResource(R.mipmap.done);
                        break;
                }
            } else {
                holder.messageStatus.setImageResource(R.mipmap.done);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userImageMessage(ImageViewHolder holder, HashMap<String, Object> chatList, int position) {

        try {
            String data = chatList.get("data").toString();
            long timeStamp = (long) chatList.get("timeStamp");
//            if (source.equals(Constants.DOCTOR_IMAGE)) {
//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1000, 1000);
//                layoutParams.gravity = Gravity.END;
//                holder.mChatImage.setLayoutParams(layoutParams);
//            }


//            CameraUtils.loadImageToViewByURL(mContext, holder.mChatImage, Uri.parse(data));
            DialogUtils.showProgressDialog(mContext, mContext.getString(R.string.loadingtheimagetxt));
            CameraUtils.loadImageViaGlide(mContext, holder.mChatImage, Uri.parse(data));

            holder.mChatImageTime.setText(DateTimeUtility.getChatTime(timeStamp));
            holder.mChatImageTime.setText(DateTimeUtility.getChatTime(timeStamp));
            if (chatList.containsKey("status")) {
                String msgStatus = chatList.get("status").toString();

                switch (msgStatus) {
                    case "delivered":
                        holder.imageStatusIv.setImageResource(R.mipmap.done_all);
                        break;
                    case "read":
                        holder.imageStatusIv.setImageResource(R.mipmap.done_all_blue);
                        break;
                    case "sent":
                        holder.imageStatusIv.setImageResource(R.mipmap.done);
                        break;
                    default:
                        holder.imageStatusIv.setImageResource(R.mipmap.done);
                        break;
                }
            } else {
                holder.imageStatusIv.setImageResource(R.mipmap.done);
            }
            if (data != null) {
                holder.mChatImage.setOnClickListener(v -> {
//                        ChatActivity activity = (ChatActivity) (mContext);
//                        FragmentManager fm = activity.getSupportFragmentManager();
//                        ImageZoomingFragment imageZoomingFragment = ImageZoomingFragment.newInstance(data);
//                        fm.
                    Intent intent = new Intent(mContext, ImageZoomingActivity.class);
                    intent.putExtra(Constants.URL, data);
                    mContext.startActivity(intent);
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doctorImageMessage(ImageViewHolder holder, HashMap<String, Object> chatList, int position) {

        try {
            String data = chatList.get("data").toString();
            long timeStamp = (long) chatList.get("timeStamp");
//            if (source.equals(Constants.DOCTOR_IMAGE)) {
//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1000, 1000);
//                layoutParams.gravity = Gravity.END;
//                holder.mChatImage.setLayoutParams(layoutParams);
//            }


//            CameraUtils.loadImageToViewByURL(mContext, holder.mChatImage, Uri.parse(data));
            DialogUtils.showProgressDialog(mContext, mContext.getString(R.string.loadingtheimagetxt));
            CameraUtils.loadImageViaGlide(mContext, holder.mChatImage, Uri.parse(data));

            holder.mChatImageTime.setText(DateTimeUtility.getChatTime(timeStamp));
            holder.mChatImageTime.setText(DateTimeUtility.getChatTime(timeStamp));
            if (chatList.containsKey("status")) {
                String msgStatus = chatList.get("status").toString();

                switch (msgStatus) {
                    case "delivered":
                        holder.imageStatusIv.setImageResource(R.mipmap.done_all);
                        break;
                    case "read":
                        holder.imageStatusIv.setImageResource(R.mipmap.done_all_blue);
                        break;
                    case "sent":
                        holder.imageStatusIv.setImageResource(R.mipmap.done);
                        break;
                    default:
                        holder.imageStatusIv.setImageResource(R.mipmap.done);
                        break;
                }
            } else {
                holder.imageStatusIv.setImageResource(R.mipmap.done);
            }
            if (data != null) {
                holder.mChatImage.setOnClickListener(v -> {
//                        ChatActivity activity = (ChatActivity) (mContext);
//                        FragmentManager fm = activity.getSupportFragmentManager();
//                        ImageZoomingFragment imageZoomingFragment = ImageZoomingFragment.newInstance(data);
//                        fm.
                    Intent intent = new Intent(mContext, ImageZoomingActivity.class);
                    intent.putExtra(Constants.URL, data);
                    mContext.startActivity(intent);
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userTextMessage(UserTextViewHolder holder, HashMap<String, Object> chatList, int position) {
        try {
            String data = chatList.get("data").toString();
            long timeStamp = (long) chatList.get("timeStamp");
            if (chatList.containsKey("sender")) {
                holder.patientName.setText(chatList.get("sender").toString());
            } else {
                holder.patientName.setText(mContext.getString(R.string.patientTxt));
            }
            holder.mUserMessage.setText(data);
            copyTextOnLongPress(holder.mUserMessage);
            holder.mUserMessagsTime.setText(DateTimeUtility.getChatTime(timeStamp));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    private void prescriptionMessage(PrescriptionViewHolder holder, HashMap<String, Object> chatList, int position) {
////        HashMap<String, Object> data = (HashMap<String, Object>) chatList.get("data");
////        HashMap<String, Object> text = (HashMap<String, Object>) data.get("text");
////        Log.e("Hashpmap data", text.toString());
//        holder.expandIocn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mContext, ViewPrescriptionACtivity.class);
//                intent.putExtra(Constants.CONSULTATION_ID, consultationID);
//                intent.putExtra(Constants.PRESCRIPTION_ID,chatList.get("_id").toString());
//                mContext.startActivity(intent);
//            }
//        });
//
//    }
    private void prescriptionMessage(PrescriptionViewHolder holder, HashMap<String, Object> chatList, int position) {

        try {
            String duration = "";


            Log.e("prescriptionMessage ", "" + chatList);
            HashMap<String, Object> text = (HashMap<String, Object>) chatList.get("data");
            HashMap<String, Object> doctorProfile = (HashMap<String, Object>) text.get("doctorProfile");
            HashMap<String, Object> patientData = (HashMap<String, Object>) text.get("additionalDetails");
            if (doctorProfile.get("category") != null) {
                List<String> list = (List<String>) doctorProfile.get("category");
                specializations = Reusable.arrayListToString(list, ",");
                holder.specializations.setVisibility(View.VISIBLE);
                holder.specializations.setText(specializations);

            }


            long timeStamp = (long) chatList.get("timeStamp");
            holder.chatTime.setText(DateTimeUtility.getChatTime(timeStamp));
            patientInfoDetails = patientData.get("name").toString() + " " + patientData.get("age").toString() + ", " + patientData.get("gender").toString();

            holder.title.setText("You have sent a prescription");
            if (doctorProfile.containsKey("duns")) {
                regNo = doctorProfile.get("duns").toString();
                if (regNo != null && !regNo.equalsIgnoreCase(""))
                    holder.registrationNum.setText(mContext.getString(R.string.dr_reg_number) + regNo);
            }


            if (doctorProfile.containsKey("firstName")) {
                if(doctorProfile.get("firstName").toString().contains("Dr")) {
                    holder.docName.setText(doctorProfile.get("firstName").toString());
                }else
                    holder.docName.setText("Dr. " + doctorProfile.get("firstName").toString());

            }
            holder.patientNameTV.setText("Name: " + patientInfoDetails);

            if (doctorProfile.containsKey("qualification")) {
                holder.docQualifications.setVisibility(View.VISIBLE);
                int qualificationSize = 2;
                ArrayList<String> qualification = (ArrayList<String>) doctorProfile.get("qualification");

                if (!(qualification.size() >= qualificationSize))
                    qualificationSize = qualification.size();

                for (int n = 0; n < qualificationSize; n++) {
                    Log.e("Qualification list", "qualification : " + qualification.get(n));
                }
                if (qualification.size() > 0) {
                    holder.docQualifications.setVisibility(View.VISIBLE);
                    holder.docQualifications.setText(Reusable.arrayListToString(qualification, ", "));
                    docDegree = Reusable.arrayListToString(qualification, ", ");
                }

            }

            if (text.containsKey("medication")) {
                ArrayList<Object> medications = (ArrayList<Object>) text.get("medication");

                if (medications.size() > 0) {
                    HashMap<String, Object> medicine = (HashMap<String, Object>) medications.get(0);
                    String time = "";
                    String foodWarning;
                    if (medicine.containsKey("doseTime") && medicine.get("doseTime") != null) {
                        List<String> doseTime = (List<String>) medicine.get("doseTime");
                        time = Reusable.arrayListToString(doseTime, " - ");
//                        ArrayList<Object> doseTime = (ArrayList<Object>) medicine.get("doseTime");
//
//                        String morning = "0", noon = "0", night = "0";
//                        for (int p = 0; p < doseTime.size(); p++) {
//                            if (doseTime.get(p).toString().equalsIgnoreCase("Morning")) {
//                                morning = "1";
//                            } else if (doseTime.get(p).toString().equalsIgnoreCase("Afternoon")) {
//                                noon = "1";
//                            } else if (doseTime.get(p).toString().equalsIgnoreCase("Night")) {
//                                night = "1";
//                            }
//                        }
//                        if (morning == null || noon == null || night == null)
//                            time = "";
//                        else
//                            time = morning + "-" + noon + "-" + night;

                    }
                    if (medicine.get("foodWarning") != null)
                        foodWarning = medicine.get("foodWarning").toString();
                    else
                        foodWarning = "";
                    if (medicine.get("duration") != null && medicine.get("durationUnit") != null) {
                        duration = "Duration: " + String.valueOf(medicine.get("duration")) + " " + medicine.get("durationUnit").toString();
                    }
                    holder.prescriptionData.setText((medicine.get("name") + "\n" + time + "\n" + foodWarning + "\n" + duration));

                }

            }

            String prescriptionId = text.get("prescriptionId").toString();
            Log.e(TAG, "prescriptionId " + prescriptionId);
            holder.date.setText("Date: " + DateTimeUtility.getChatTimeDDMMMYYYY(timeStamp));
            holder.expandIocn.setOnClickListener(view -> {
                Intent intent = new Intent(mContext, ViewPrescriptionACtivity.class);
                Bundle bundle = new Bundle();
                if (consultationID == null && prescriptionId == null)
                    return;
                bundle.putString(Constants.SOURCE, Constants.EDIT_PRESCRIPTION);
                bundle.putString(Constants.CONSULTATION_ID, consultationID);
                bundle.putString(Constants.PRESCRIPTION_ID, text.get("prescriptionId").toString());
                bundle.putString(Constants.DOCTOR_NAME, doctorProfile.get("firstName").toString());
                bundle.putString(Constants.DOCTOR_DEGREE, docDegree);
                bundle.putString(Constants.DOCTOR_REGNO, regNo);
                bundle.putString(Constants.PATIENT_NAME, patientData.get("name").toString());
                bundle.putString(Constants.PATIENT_AGE, patientData.get("age").toString());
                bundle.putString(Constants.PATIENT_GENDER, patientData.get("gender").toString());
                bundle.putString(Constants.TIME_STAMP, DateTimeUtility.getChatTimeDDMMMYYYY(timeStamp));
                // bundle.putString(Constants.SPECIALIZATIONS, specializations);
                //  bundle.putString(Constants.CARD_KEY,);
                intent.putExtra(Constants.PRESCRIPTION_DATA, bundle);
                mContext.startActivity(intent);
            });
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ViewPrescriptionACtivity.class);
                Bundle bundle = new Bundle();
                if (consultationID == null && prescriptionId == null)
                    return;
                bundle.putString(Constants.SOURCE, Constants.EDIT_PRESCRIPTION);
                bundle.putString(Constants.CONSULTATION_ID, consultationID);
                bundle.putString(Constants.PRESCRIPTION_ID, text.get("prescriptionId").toString());
                bundle.putString(Constants.DOCTOR_NAME, doctorProfile.get("firstName").toString());
                bundle.putString(Constants.DOCTOR_DEGREE, docDegree);
                bundle.putString(Constants.DOCTOR_REGNO, regNo);
                bundle.putString(Constants.PATIENT_NAME, patientData.get("name").toString());
                bundle.putString(Constants.PATIENT_AGE, patientData.get("age").toString());
                bundle.putString(Constants.PATIENT_GENDER, patientData.get("gender").toString());
                bundle.putString(Constants.TIME_STAMP, DateTimeUtility.getChatTimeDDMMMYYYY(timeStamp));
                //  bundle.putString(Constants.CARD_KEY,);
                intent.putExtra(Constants.PRESCRIPTION_DATA, bundle);
                mContext.startActivity(intent);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void diagnosticMessage(DiagnosticViewHolder holder, HashMap<String, Object> chatList, int position) {
        try {

            Log.e("prescriptionMessage ", "" + chatList);
            HashMap<String, Object> text = (HashMap<String, Object>) chatList.get("data");
            HashMap<String, Object> doctorProfile = (HashMap<String, Object>) text.get("doctorProfile");
            HashMap<String, Object> patientData = (HashMap<String, Object>) text.get("additionalDetails");

            patientInfoDetails = patientData.get("name").toString() + " " + patientData.get("age").toString() + ", " + patientData.get("gender").toString();
            holder.medicineIcon.setVisibility(View.GONE);
            holder.title.setText("You have requested a diagnostic report");
            if (doctorProfile.containsKey("duns")) {
                regNo = doctorProfile.get("duns").toString();
                if (regNo != null && !regNo.equalsIgnoreCase(""))
                    holder.registrationNum.setText(mContext.getString(R.string.dr_reg_number) + regNo);
            }
            if (doctorProfile.get("category") != null) {
                List<String> list = (List<String>) doctorProfile.get("category");
                specializations = Reusable.arrayListToString(list, ",");
                holder.specializationsTV.setVisibility(View.VISIBLE);
                holder.specializationsTV.setText(specializations);

            }
            long timeStamp = (long) chatList.get("timeStamp");
            holder.chatTime.setText(DateTimeUtility.getChatTime(timeStamp));

            if(doctorProfile.get("firstName").toString().contains("Dr")) {
                holder.docName.setText(doctorProfile.get("firstName").toString());
            }else
                holder.docName.setText("Dr. " + doctorProfile.get("firstName").toString());

            holder.patientNameTV.setText("Name: " + patientInfoDetails);

            if (doctorProfile.containsKey("qualification")) {
                holder.docQualifications.setVisibility(View.VISIBLE);
                int qualificationSize = 2;
                ArrayList<String> qualification = (ArrayList<String>) doctorProfile.get("qualification");

                if (!(qualification.size() >= qualificationSize))
                    qualificationSize = qualification.size();

                for (int n = 0; n < qualificationSize; n++) {
                    Log.e("Qualification list", "qualification : " + qualification.get(n));
                }
                if (qualification.size() > 0) {
                    holder.docQualifications.setVisibility(View.VISIBLE);
                    holder.docQualifications.setText(Reusable.arrayListToString(qualification, ", "));
                    docDegree = Reusable.arrayListToString(qualification, ", ");
                }

            }

            if (text.containsKey("diagnostic")) {
                ArrayList<Object> diagnostics = (ArrayList<Object>) text.get("diagnostic");

                if (diagnostics.size() > 0) {
                    HashMap<String, Object> diagnostic = (HashMap<String, Object>) diagnostics.get(0);
                    holder.prescriptionData.setText(diagnostic.get("name").toString() + "\n" + diagnostic.get("instruction").toString());
                }

            }

            String diagnosticId = text.get("diagnosticId").toString();
            holder.date.setText("Date: " + DateTimeUtility.getChatTimeDDMMMYYYY(timeStamp));
            Log.e(TAG, "diagnosticId " + diagnosticId);
            holder.expandIocn.setOnClickListener(view -> {
                Intent intent = new Intent(mContext, ViewPrescriptionACtivity.class);
                Bundle bundle = new Bundle();
                if (consultationID == null && diagnosticId == null)
                    return;
                bundle.putString(Constants.SOURCE, Constants.EDIT_DIAGNOSTIC);
                bundle.putString(Constants.CONSULTATION_ID, consultationID);
                bundle.putString(Constants.PRESCRIPTION_ID, diagnosticId);
                bundle.putString(Constants.DOCTOR_NAME, doctorProfile.get("firstName").toString());
                bundle.putString(Constants.DOCTOR_DEGREE, docDegree);
                bundle.putString(Constants.DOCTOR_REGNO, regNo);
                bundle.putString(Constants.PATIENT_NAME, patientData.get("name").toString());
                bundle.putString(Constants.PATIENT_AGE, patientData.get("age").toString());
                bundle.putString(Constants.PATIENT_GENDER, patientData.get("gender").toString());
                bundle.putString(Constants.TIME_STAMP, DateTimeUtility.getChatTimeDDMMMYYYY(timeStamp));
                //  bundle.putString(Constants.CARD_KEY,);
                intent.putExtra(Constants.PRESCRIPTION_DATA, bundle);
                mContext.startActivity(intent);
            });
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ViewPrescriptionACtivity.class);
                Bundle bundle = new Bundle();
                if (consultationID == null && diagnosticId == null)
                    return;
                bundle.putString(Constants.SOURCE, Constants.EDIT_DIAGNOSTIC);
                bundle.putString(Constants.CONSULTATION_ID, consultationID);
                bundle.putString(Constants.PRESCRIPTION_ID, diagnosticId);
                bundle.putString(Constants.DOCTOR_NAME, doctorProfile.get("firstName").toString());
                bundle.putString(Constants.DOCTOR_DEGREE, docDegree);
                bundle.putString(Constants.DOCTOR_REGNO, regNo);
                bundle.putString(Constants.PATIENT_NAME, patientData.get("name").toString());
                bundle.putString(Constants.PATIENT_AGE, patientData.get("age").toString());
                bundle.putString(Constants.PATIENT_GENDER, patientData.get("gender").toString());
                bundle.putString(Constants.TIME_STAMP, DateTimeUtility.getChatTimeDDMMMYYYY(timeStamp));
                //  bundle.putString(Constants.CARD_KEY,);
                intent.putExtra(Constants.PRESCRIPTION_DATA, bundle);
                mContext.startActivity(intent);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userIntroMessage(UserIntroViewHolder holder, HashMap<String, Object> chatList, int position) {
        try {

            HashMap<String, Object> text = (HashMap<String, Object>) chatList.get("data");
            long timeStamp = (long) chatList.get("timeStamp");
            String relation = text.get("relation").toString();
            String title = text.get("firstName").toString() + " is consulting for " + relation;
            String nameAgeGender = text.get("relativesName").toString() + ", " + text.get("relativesGender").toString() + ", " + text.get("relativesAge").toString();
//            patientName = text.get("relativesName").toString();
//            patientGender = text.get("relativesGender").toString();
//            patientAge = text.get("relativesAge").toString();
            holder.mChatTitle.setText(title);
            holder.mNameAgeGender.setText(nameAgeGender);
            holder.mTextConcern.setText(text.get("note").toString());
            holder.mUserIntroTime.setText(DateTimeUtility.getChatTime(timeStamp));
            if (text.get("relativesGender").toString().equalsIgnoreCase("Female")) {
                holder.mProfilePic.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.mipmap.ic_myself_female));
            } else {
                holder.mProfilePic.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.mipmap.ic_myself_male));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return chats.size();
    }

    // copy / paste text
    private void copyTextOnLongPress(TextView textView) {
        try {
            String stringYouExtracted = textView.getText().toString();
            int startIndex = textView.getSelectionStart();
            int endIndex = textView.getSelectionEnd();
            stringYouExtracted = stringYouExtracted.substring(startIndex, endIndex);
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", stringYouExtracted);
            clipboard.setPrimaryClip(clip);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
