package com.doxtro.doctor.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.NotificationInfo;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.ChatActivity;
import com.doxtro.doctor.activities.HomeActivity;
import com.doxtro.doctor.model.GetConsultDetailsModel;
import com.doxtro.doctor.receivers.AcceptConsultationReceiver;
import com.doxtro.doctor.receivers.RejectConsultationReceiver;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sahil on 15/11/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {


    String fcmRegId = FirebaseInstanceId.getInstance().getToken();


    String TAG = MyFirebaseMessagingService.class.getSimpleName();

    String data, notification;

    @Override
    public void handleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        data = extras.getString("data");
        notification = extras.getString("gcm.notification.body");

        Log.e(TAG, extras.toString());

        if (notification != null && data != null) {
            try {
                Log.e(TAG, data);
                //showNotification(remoteMessage.getNotification().getBody(), "");
                showNotificationWithAction();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (notification != null) {
            try {
                Log.e(TAG, notification);
                //showNotification(remoteMessage.getNotification().getBody(), "");
                getConsultationDetails(extras.getString("consultationId"));
                //showNotification(extras.getString("consultationId"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                CleverTapAPI cleverTapAPI = null;
                try {
                    cleverTapAPI = CleverTapAPI.getInstance(getApplicationContext());
                } catch (CleverTapMetaDataNotFoundException | CleverTapPermissionsNotSatisfied e) {
                    e.printStackTrace();
                }
                assert cleverTapAPI != null;
                cleverTapAPI.data.pushFcmRegistrationId(fcmRegId, true);

                /*Bundle cleverTapExtras = new Bundle();

                for (Map.Entry<String, String> entry : ) {
                    extras.putString(entry.getKey(), entry.getValue());
                }*/

                NotificationInfo info = CleverTapAPI.getNotificationInfo(extras);

                //noinspection StatementWithEmptyBody
                if (info.fromCleverTap) {
                    CleverTapAPI.createNotification(getApplicationContext(), extras);
                } else {
                    // not from CleverTap handle yourself or pass to another provider
                }

            } catch (Throwable t) {
                Log.d("MYFCMLIST", "Error parsing FCM message", t);
            }
//                Log.e("tag", remoteMessage.getNotification().getBody());
//                showNotification("This is a test notification for android patient app from local and It should be expandable", "");
        }
    }

    private void getConsultationDetails(final String consultationId) {
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Call<GetConsultDetailsModel> call = apiService.getConsultationsDetails(consultationId);
        call.enqueue(new Callback<GetConsultDetailsModel>() {
            @Override
            public void onResponse(Call<GetConsultDetailsModel> call, Response<GetConsultDetailsModel> response) {
                try {
                    if (response.isSuccessful()) {
                        List<GetConsultDetailsModel.Datum> data = response.body().getData();
                        GetConsultDetailsModel.Datum consultationDetails = data.get(0);
                        String relativeName = consultationDetails.getRelativesId().getName();
                        showNotification(consultationId, relativeName);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<GetConsultDetailsModel> call, Throwable t) {
                // Log error here since request failed
                Log.e("Error", t.toString());
                DialogUtils.dismissProgressDialog();
            }
        });
    }

    private void showNotification(String consultationId, String relativeName) {
        String title = "Doxtro";
        Intent defaultAction = new Intent(this, ChatActivity.class)
                .setAction(Intent.ACTION_DEFAULT)
                .putExtra(Constants.CONSULTATION_ID, consultationId)
                .putExtra(Constants.PATIENT_NAME, relativeName); //

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                defaultAction,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.msg_push_notification);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(notification)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1000, notificationBuilder.build());
    }


    private void showNotificationWithAction() throws JSONException {

        try {
            JSONObject dataObject = new JSONObject(data);
            String doctorId = dataObject.getString("doctorId");
            String consultationId = dataObject.getString("consultationId");
            String patientName = dataObject.getString("relativeName");

            String title = notification;
            String body = dataObject.getString("note");

            RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_custom_view);
            contentView.setImageViewResource(R.id.image, R.mipmap.ic_launcher);
            contentView.setTextViewText(R.id.sub_title, title);
            contentView.setTextViewText(R.id.text, body);
            contentView.setTextViewText(R.id.notification_consulation_accept, "Accept Request");
            contentView.setTextViewText(R.id.notification_consulation_reject, "Reject");
            contentView.setTextColor(R.id.notification_consulation_accept, AndroidVersionUtility.getColor(this, R.color.appColor));

            //accept consultation
            Intent acceptIntent = new Intent(this, AcceptConsultationReceiver.class);
            acceptIntent.putExtra("consultationState", Constants.ACCEPT);
            acceptIntent.putExtra("doctorId", doctorId);
            acceptIntent.putExtra("consultationId", consultationId);
            acceptIntent.putExtra("patientName", patientName);
            PendingIntent pendingIntentAccept = PendingIntent.getBroadcast(this, 0,
                    acceptIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            contentView.setOnClickPendingIntent(R.id.notification_consulation_accept, pendingIntentAccept);

            //cancel consultation
            Intent cancelIntent = new Intent(this, RejectConsultationReceiver.class);
            cancelIntent.putExtra("consultationState", Constants.REJECT);
            cancelIntent.putExtra("doctorId", doctorId);
            cancelIntent.putExtra("consultationId", consultationId);
            cancelIntent.putExtra("patientName", patientName);
            PendingIntent pendingIntentCancel = PendingIntent.getBroadcast(this, 0,
                    cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            contentView.setOnClickPendingIntent(R.id.notification_consulation_reject, pendingIntentCancel);


            Intent defaultAction = new Intent(this, HomeActivity.class)
                    .setAction(Intent.ACTION_DEFAULT);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    defaultAction, PendingIntent.FLAG_UPDATE_CURRENT);

            Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.msg_push_notification);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setCustomBigContentView(contentView)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(100, notificationBuilder.build());

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notifi_icon_white : R.mipmap.ic_launcher;
    }
}
