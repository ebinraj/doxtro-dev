package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yashaswi on 27/07/17.
 */

public class SpecializationResponse {

    @SerializedName("message")

    private String message;
    @SerializedName("data")

    private List<SpecializationData> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SpecializationData> getData() {
        return data;
    }

    public void setData(List<SpecializationData> data) {
        this.data = data;
    }

    public class SpecializationData {

        @SerializedName("_id")
        private String id;
        @SerializedName("specializations")
        private List<String> specializations = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<String> getSpecializations() {
            return specializations;
        }

        public void setSpecializations(List<String> specializations) {
            this.specializations = specializations;
        }
    }


}
