package com.doxtro.doctor.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.ChatActivity;
import com.doxtro.doctor.custom.CircleImageView;
import com.doxtro.doctor.model.ConsultationsDataModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DateTimeUtility;
import com.doxtro.doctor.utils.EventsUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 05/09/17.
 */

public class OpenCloseConsultationAdapter extends RecyclerView.Adapter<OpenCloseConsultationAdapter.MyViewHolder> {
    private HashMap<String, Object> unreadCounts = new HashMap<>();
    private final Context mContext;
    private List<ConsultationsDataModel> dataList = new ArrayList<>();

    public OpenCloseConsultationAdapter(Context context, List<ConsultationsDataModel> consultationList, HashMap<String, Object> unreadMsgList) {
        mContext = context;
        dataList = consultationList;
        unreadCounts = unreadMsgList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.consultation_open_close_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (dataList != null && dataList.size() > 0) {
            ConsultationsDataModel data = dataList.get(position);
            if (unreadCounts.size() > 0) {
                for (String unreadMsgKey : unreadCounts.keySet()) {
                    if (dataList.get(position).getId().equalsIgnoreCase(unreadMsgKey)) {
                        int unreadMsgCount = (int) unreadCounts.get(unreadMsgKey);
                        if (unreadMsgCount > 0) {
                            holder.unreadCountTV.setVisibility(View.VISIBLE);
                            holder.unreadCountTV.setText(String.valueOf(unreadMsgCount));
                        }
                    }
                }
            }
            if (data.getRelativesId() != null) {
                if (data.getRelativesId().getGender().equals(Constants.GENDER_MALE))
                    holder.profileImage.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.mipmap.ic_myself_male));
                else if (data.getRelativesId().getGender().equals(Constants.GENDER_FEMALE))
                    holder.profileImage.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.mipmap.ic_myself_female));

                holder.relationAgeTV.setText(data.getConsultingFor() + ", " + data.getRelativesId().getAge());
                holder.nameTV.setText(data.getRelativesId().getName());
            }

            holder.descTV.setText(data.getNote());
            holder.timeTV.setText(DateTimeUtility.getFormattedTimeSlot(data.getAssignedAt()));
            holder.itemView.setOnClickListener(v -> {
                EventsUtility.eventTrack(mContext, Constants.EVENT_CHAT_VISITORS, null);
                String consultId = dataList.get(position).getId();
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra(Constants.CONSULTATION_ID, consultId);
                intent.putExtra(Constants.CONSULTATION_TYPE, data.getConsultationType());
                intent.putExtra(Constants.PATIENT_NAME, data.getRelativesId().getName());
                intent.putExtra(Constants.STATUS, data.getStatus());
                ((Activity) mContext).startActivityForResult(intent, 106);
            });
            switch (data.getStatus()) {
                case Constants.ONGOING:
                    holder.statusTab.setVisibility(View.GONE);
                    break;
                case Constants.CLOSED:
                    holder.statusTab.setVisibility(View.VISIBLE);
                    GradientDrawable bgShape = (GradientDrawable) holder.statusTab.getBackground().getCurrent();
                    bgShape.setColor(AndroidVersionUtility.getColor(mContext, R.color.red));
                    bgShape.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.red));
                    holder.statusTab.setText("Closed");
                    holder.statusTab.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
                    break;
                case Constants.COMPLETED:
                    holder.statusTab.setVisibility(View.VISIBLE);
                    GradientDrawable bgShape1 = (GradientDrawable) holder.statusTab.getBackground().getCurrent();
                    bgShape1.setColor(AndroidVersionUtility.getColor(mContext, R.color.appColor));
                    bgShape1.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.appColor));
                    holder.statusTab.setText("Completed");
                    holder.statusTab.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
                    break;
            }

        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.consultation_profileImage)
        CircleImageView profileImage;
        @BindView(R.id.consulation_username)
        TextView nameTV;
        @BindView(R.id.consultation_timeTv)
        TextView timeTV;
        @BindView(R.id.consultation_descTV)
        TextView descTV;
        @BindView(R.id.consultation_relation_ageTV)
        TextView relationAgeTV;
        @BindView(R.id.consultation_status_tab)
        Button statusTab;
        @BindView(R.id.consultations_no_of_msgs)
        TextView unreadCountTV;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
