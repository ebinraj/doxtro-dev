package com.doxtro.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.ImageZoomingActivity;
import com.doxtro.doctor.model.UrlModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;

import java.util.ArrayList;

/**
 * Created by ghanshyamnayma on 14/07/17.
 */

public class HistoryGridAdapter extends BaseAdapter {

    private ArrayList<UrlModel> listOfImages = new ArrayList<>();
    private LayoutInflater inflater;

    public HistoryGridAdapter(Context mContext, ArrayList<UrlModel> listOfImages) {
        this.listOfImages = listOfImages;
    }

    @Override
    public int getCount() {
        return listOfImages.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {
        public ImageView imgItem;

    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder view;
        final Context mContext = parent.getContext();
        if (inflater == null)
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            view = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_item_history, null);

            view.imgItem = (ImageView) convertView.findViewById(R.id.img_history_list);
            convertView.setTag(view);
            convertView.setId(0);
        } else {
            view = (ViewHolder) convertView.getTag();
        }
        if (listOfImages.get(position).getUrl() != null) {
            if (listOfImages.get(position).getUrlType().equalsIgnoreCase("image")) {
                CameraUtils.loadImageViaGlide(mContext, view.imgItem, Uri.parse(listOfImages.get(position).getUrl()));
                view.imgItem.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, ImageZoomingActivity.class);
                    intent.putExtra(Constants.URL, listOfImages.get(position).getUrl());
                    mContext.startActivity(intent);
                });
            } else if (listOfImages.get(position).getUrlType().equalsIgnoreCase("documents")) {
                view.imgItem.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.drawable.file_placeholder));
                view.imgItem.setOnClickListener(v -> {
                    if (!listOfImages.get(position).getUrl().startsWith("http://") || !listOfImages.get(position).getUrl().startsWith("https://")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(listOfImages.get(position).getUrl()));
                        mContext.startActivity(browserIntent);
                    }
                });

            }
        }
        return convertView;
    }


}
