package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 04/09/17.
 */

public class ConsultationsDataModel {
    @SerializedName("_id")
    private String id;

    @SerializedName("patientId")
    private PatientModel patientId;

    @SerializedName("relativesId")
    private RelativesModel relativesId;

    @SerializedName("consultingFor")
    private String consultingFor;

    @SerializedName("consultationType")
    private String consultationType;

    @SerializedName("consultationLanguage")
    private String consultationLanguage;

    @SerializedName("specializationCategory")
    private String specializationCategory;

    @SerializedName("note")
    private String note;

    @SerializedName("status")
    private String status;

    @SerializedName("prescriptionId")
    private List<String> prescriptionId = new ArrayList<>();

    @SerializedName("attachedDocuments")
    private List<String> attachedDocuments = new ArrayList<>();

    @SerializedName("assignedAt")
    private String assignedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PatientModel getPatientId() {
        return patientId;
    }

    public void setPatientId(PatientModel patientId) {
        this.patientId = patientId;
    }

    public RelativesModel getRelativesId() {
        return relativesId;
    }

    public void setRelativesId(RelativesModel relativesId) {
        this.relativesId = relativesId;
    }

    public String getConsultingFor() {
        return consultingFor;
    }

    public void setConsultingFor(String consultingFor) {
        this.consultingFor = consultingFor;
    }

    public String getConsultationType() {
        return consultationType;
    }

    public void setConsultationType(String consultationType) {
        this.consultationType = consultationType;
    }

    public String getConsultationLanguage() {
        return consultationLanguage;
    }

    public void setConsultationLanguage(String consultationLanguage) {
        this.consultationLanguage = consultationLanguage;
    }

    public String getSpecializationCategory() {
        return specializationCategory;
    }

    public void setSpecializationCategory(String specializationCategory) {
        this.specializationCategory = specializationCategory;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(List<String> prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public List<String> getAttachedDocuments() {
        return attachedDocuments;
    }

    public void setAttachedDocuments(List<String> attachedDocuments) {
        this.attachedDocuments = attachedDocuments;
    }

    public String getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(String assignedAt) {
        this.assignedAt = assignedAt;
    }

    public class RelativesModel {
        @SerializedName("_id")
        private String id;

        @SerializedName("name")
        private String name;

        @SerializedName("age")
        private String age;

        @SerializedName("gender")
        private String gender;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }

    public class PatientModel {

        @SerializedName("_id")
        private String id;

        @SerializedName("firstName")
        private String firstName;

        @SerializedName("name")
        private String name;

        @SerializedName("gender")
        private String gender;

        @SerializedName("age")
        private String age;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
