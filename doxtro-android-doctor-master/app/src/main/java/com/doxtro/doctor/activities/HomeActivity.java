package com.doxtro.doctor.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.clevertap.android.sdk.CleverTapAPI;
import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.ViewPagerAdapter;
import com.doxtro.doctor.custom.BottomNavigationViewHelper;
import com.doxtro.doctor.fragments.ConsultationFragment;
import com.doxtro.doctor.fragments.HomeFragment;
import com.doxtro.doctor.fragments.ProfileFragment;
import com.doxtro.doctor.utils.Constants;

public class HomeActivity extends AppCompatActivity {


    private BottomNavigationView bottomNavigationView;
    //This is our viewPager
    private ViewPager viewPager;
    //Fragments
    private HomeFragment homeFragment;
    private ConsultationFragment consultationFragment;
    // HealthFeedFragment healthFeedFragment;
    private MenuItem prevMenuItem;

    //  private HealthFeedFragment healthFeedFragment;
    //  private OnGoingClosedConsultationFragment onGoingClosedConsultationFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.activity_home, null);
        super.onCreate(savedInstanceState);
        setContentView(view);
        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        //Initializing the bottomNavigationView
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                item -> {
                    switch (item.getItemId()) {
                        case R.id.action_home:
                            viewPager.setCurrentItem(0);
                            break;
                        case R.id.action_consultation:
                            viewPager.setCurrentItem(1);
                            break;
//                        case R.id.action_health_feed:
//                            viewPager.setCurrentItem(2);
//                            break;
                        case R.id.action_profile:
                            viewPager.setCurrentItem(2);
                            break;
                    }
                    return false;
                });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                Log.d("page", "onPageSelected: " + position);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupViewPager(viewPager);
        if (getIntent() != null) {
            int tab = getIntent().getIntExtra(Constants.TAB_SWITCH, 0);
            viewPager.setCurrentItem(tab);
        }


    }

    /* Setting up view pager with fragments*/
    private void setupViewPager(ViewPager viewPager) {
        viewPager.setOffscreenPageLimit(2);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        homeFragment = HomeFragment.newInstance();
        consultationFragment = ConsultationFragment.newInstance();
        adapter.addFragment(homeFragment);
        adapter.addFragment(consultationFragment);
        //  adapter.addFragment(new HealthFeedFragment());
        adapter.addFragment(new ProfileFragment());

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        appExitDialog(); // exit confirmation

    }

    // showing custom dialog on exit from app
    private void appExitDialog() {
        //fetch the user information to show in alert
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage("Do you want to exit Doxtro App ?");
        dialog.setCancelable(true);

        //positive button
        dialog.setPositiveButton("Yes", (dialog1, which) -> {
            finishAffinity();
            dialog1.cancel();
            HomeActivity.super.onBackPressed();
        });

        //negative button
        dialog.setNegativeButton("No", (dialog12, which) -> dialog12.cancel());

        dialog.show();
    }

    public ViewPager getViewPager() {
        if (null == viewPager) {
            viewPager = (ViewPager) findViewById(R.id.viewpager);
        }
        return viewPager;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        try {
            CleverTapAPI.getInstance(this).event.pushNotificationEvent(intent.getExtras());
        } catch (Throwable t) {
            t.printStackTrace();
        }
        super.onNewIntent(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 105) {
            //  fetchConsultations();
            if (homeFragment != null) {
                homeFragment.onActivityResult(requestCode, resultCode, data);
                consultationFragment.onActivityResult(requestCode, resultCode, data);
            }

        } else if (requestCode == 106) {
            if (consultationFragment != null) {
                consultationFragment.onActivityResult(requestCode, resultCode, data);
            }
        }else if(requestCode==107)
        {
            if (homeFragment != null) {
                homeFragment.onActivityResult(requestCode, resultCode, data);
                consultationFragment.onActivityResult(requestCode, resultCode, data);
            }
        }

    }

}
