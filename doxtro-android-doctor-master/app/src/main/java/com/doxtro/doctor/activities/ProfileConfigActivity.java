package com.doxtro.doctor.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.doxtro.doctor.R;
import com.doxtro.doctor.fragments.AvailabilityFragment;
import com.doxtro.doctor.fragments.BasicProfileFragment;
import com.doxtro.doctor.fragments.UploadDocumentFragment;
import com.doxtro.doctor.interfaces.BasicIndicatorListerner;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.FragmentsActionUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileConfigActivity extends AppCompatActivity implements View.OnClickListener, BasicIndicatorListerner {

    private static final String TAG = ProfileConfigActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 0;

    @BindView(R.id.profile_confile_frame)
    FrameLayout fragmentFrame;
    @BindView(R.id.main_layout_progress_level)
    LinearLayout progressLevel;
    @BindView(R.id.view_line1)
    View line1;
    @BindView(R.id.view_line2)
    View line2;
    @BindView(R.id.view_dot2)
    View dot2;
    @BindView(R.id.view_dot3)
    View dot3;

    private BasicProfileFragment basicProfileFragment;
    private UploadDocumentFragment uploadDocumentFragment;
    private AvailabilityFragment availabilityFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_config);
        ButterKnife.bind(this);
        initFragment();
        int permissionrequested = ContextCompat.checkSelfPermission(ProfileConfigActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionrequested != PackageManager.PERMISSION_DENIED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileConfigActivity.this,
                    Manifest.permission.READ_SMS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(ProfileConfigActivity.this,
                        new String[]{Manifest.permission.READ_SMS},
                        MY_PERMISSIONS_REQUEST_READ_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        basicProfileFragment = BasicProfileFragment.newInstance(this);
        uploadDocumentFragment= UploadDocumentFragment.newInstance(null,null,this);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Sms request granted");
                } else {
                    Log.i(TAG, "Sms request denied");
                }
            }

        }
    }

    private void initFragment() {
        BasicProfileFragment basicProfileFragment = BasicProfileFragment.newInstance(this);
        FragmentsActionUtility.addFragment(basicProfileFragment, R.id.profile_confile_frame, getSupportFragmentManager());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        super.onBackPressed();

    }

    public void hideProgressLevel() {
        progressLevel.setVisibility(View.GONE);
    }


    @Override
    public void changeIndicatorStatus(int status) {
        switch (status) {
            case 1:
                dot2.setBackground(AndroidVersionUtility.getDrawable(this, R.drawable.round_bg_app_color));
                line1.setBackground(AndroidVersionUtility.getDrawable(this, R.color.appColor));
                break;
            case 2:
                line2.setBackground(AndroidVersionUtility.getDrawable(this, R.color.appColor));
                dot3.setBackground(AndroidVersionUtility.getDrawable(this, R.drawable.round_bg_app_color));
                break;

        }

    }
}
