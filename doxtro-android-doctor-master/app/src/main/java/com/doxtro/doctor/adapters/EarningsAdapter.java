package com.doxtro.doctor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.EarningDataModel;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DateTimeUtility;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 16/10/17.
 */


public class EarningsAdapter extends RecyclerView.Adapter<EarningsAdapter.MyViewHolder> {
    private String source = "";
    private List<EarningDataModel> earningsList = new ArrayList<>();
    private final Context mContext;

    public EarningsAdapter(Context context, List<EarningDataModel> earnings, String source) {
        earningsList = earnings;
        this.source = source;
        mContext = context;
    }

    @Override
    public EarningsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.earnings_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EarningsAdapter.MyViewHolder holder, int position) {
        if (holder == null) return;
        EarningDataModel earningDataModel = earningsList.get(position);
        String date = DateTimeUtility.getFormatDDMMMYYHMMA(earningDataModel.getDate());
        String name = earningDataModel.getPatientId().getName();
        String gender = earningDataModel.getPatientId().getGender();
        String age = earningDataModel.getPatientId().getAge();
        String amount = String.valueOf(earningDataModel.getEarning());
        holder.descTV.setText(date + "\n" + name + " ," + gender + " ," + age);
        holder.amountTV.setText(mContext.getString(R.string.rupees_symbol) + amount);

    }

    @Override
    public int getItemCount() {
        int count;
        if (source.equals(Constants.MONTHLY_EARNING)) {
            if (earningsList.size() < 2) {
                count = earningsList.size();
            } else {
                count = 2;
            }
        } else {
            count = earningsList.size();
        }
        return (count);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.earnings_descTV)
        TextView descTV;
        @BindView(R.id.earnings_amountTV)
        TextView amountTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

