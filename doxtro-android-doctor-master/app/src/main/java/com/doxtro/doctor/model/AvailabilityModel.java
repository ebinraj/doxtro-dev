package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 11/08/17.
 */

public class AvailabilityModel implements Serializable {

    public AvailabilityModel()
    {
        //empty constructor
    }

    @SerializedName("_id")
    private String id;
    @SerializedName("operationalHours")
    private List<OperationalHourModel> operationalHourModels = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<OperationalHourModel> getOperationalHours() {
        return operationalHourModels;
    }

    public void setOperationalHours(List<OperationalHourModel> operationalHourModels) {
        this.operationalHourModels = operationalHourModels;
    }

}
