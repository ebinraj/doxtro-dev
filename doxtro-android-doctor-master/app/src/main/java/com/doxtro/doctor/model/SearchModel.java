package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 17/10/17.
 */

public class SearchModel {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<String> data = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
