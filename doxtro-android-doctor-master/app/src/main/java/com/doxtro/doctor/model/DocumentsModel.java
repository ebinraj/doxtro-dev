package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 08/08/17.
 */

public class DocumentsModel {

    @SerializedName("tag")
    private String tag;
    @SerializedName("url")
    private String url;
    @SerializedName("status")
    private String status;

    public DocumentsModel() {

    }


    public DocumentsModel(String tag, String url) {
        this.tag = tag;
        this.url = url;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
