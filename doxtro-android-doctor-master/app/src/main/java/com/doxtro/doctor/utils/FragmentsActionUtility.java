package com.doxtro.doctor.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by yashaswi on 26/07/17.
 *
 * common Utility  to add/replace the fragments
 */

public class FragmentsActionUtility extends AppCompatActivity {

    public static void addFragment(Fragment fragment, int frameLayout, FragmentManager fragmentManager)
    {
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.add(frameLayout,fragment);
        fragmentTransaction.commit();

    }
    public static void replaceFragment(Fragment fragment, int frameLayout, FragmentManager fragmentManager)
    {
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(frameLayout,fragment);
        fragmentTransaction.commit();

    }
}
