package com.doxtro.doctor.activities;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.AllergyDrugAdapter;
import com.doxtro.doctor.adapters.CurrentMedicineAdapter;
import com.doxtro.doctor.adapters.DiagnosticAdapter;
import com.doxtro.doctor.adapters.MedicalHistoryAdapter;
import com.doxtro.doctor.model.GetHealthRecordsModel;
import com.doxtro.doctor.model.HealthRecordsModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.NetworkUtil;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */

public class HealthRecordActivity extends AppCompatActivity {
    private static final String TAG = "HealthRecordActivity";

    private ArrayList<HealthRecordsModel.Data> currentMedicineList = new ArrayList<>();
    private ArrayList<HealthRecordsModel.Data> allergyDrugList = new ArrayList<>();
    private ArrayList<HealthRecordsModel.Data> diagnosticList = new ArrayList<>();
    private ArrayList<HealthRecordsModel.Data> historyList = new ArrayList<>();


    @BindView(R.id.recycler_view_current_medicine)
    RecyclerView mRvCurrentMedicine;

    @BindView(R.id.recycler_view_title_allergy)
    RecyclerView mRvAllergy;

    @BindView(R.id.recycler_view_diagnostic)
    RecyclerView mRvDiagnostic;

    @BindView(R.id.recycler_view_history)
    RecyclerView mRvHistory;


    @BindView(R.id.no_records_allergy)
    TextView noRecordsAllergy;

    @BindView(R.id.no_current_medicine)
    TextView noRecordsCurrentMedi;

    @BindView(R.id.tv_no_history)
    TextView noHistory;

    @BindView(R.id.health_diagnostic_report_nodataTV)
    TextView noDiagnosticDataTV;

    @BindView(R.id.common_base_toolbarT)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;




    public HealthRecordActivity() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.fragment_my_health, null);
        setContentView(view);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
        toolbarTitle.setText(getString(R.string.health_record));
        if(getIntent()!=null)
        {
            String relativeID=getIntent().getStringExtra(Constants.RELATIVE_ID);
            getAllHealthRecords(relativeID);
        }

    }


    private void populateViewCurrentMedicine(ArrayList<HealthRecordsModel.Data> arrayList) {
        if (arrayList.size() > 0) {
            mRvCurrentMedicine.setVisibility(View.VISIBLE);
            noRecordsCurrentMedi.setVisibility(View.GONE);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            mRvCurrentMedicine.setLayoutManager(layoutManager);
            CurrentMedicineAdapter adapter = new CurrentMedicineAdapter(this, arrayList, noRecordsCurrentMedi);
            mRvCurrentMedicine.setAdapter(adapter);
        } else {
            mRvCurrentMedicine.setVisibility(View.INVISIBLE);
            noRecordsCurrentMedi.setVisibility(View.VISIBLE);
        }
    }

    private void populateViewAllergy(ArrayList<HealthRecordsModel.Data> arrayList) {
        if (arrayList.size() > 0) {
            mRvAllergy.setVisibility(View.VISIBLE);
            noRecordsAllergy.setVisibility(View.GONE);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            mRvAllergy.setLayoutManager(layoutManager);
            AllergyDrugAdapter adapter = new AllergyDrugAdapter(this, arrayList, noRecordsAllergy);
            mRvAllergy.setAdapter(adapter);
        } else {
            mRvAllergy.setVisibility(View.INVISIBLE);
            noRecordsAllergy.setVisibility(View.VISIBLE);
        }
    }

    private void populateViewDiagnostic(ArrayList<HealthRecordsModel.Data> arrayList) {
        if (arrayList.size() > 0) {
            mRvDiagnostic.setVisibility(View.VISIBLE);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            mRvDiagnostic.setLayoutManager(layoutManager);
            DiagnosticAdapter adapter = new DiagnosticAdapter(this, arrayList);
            mRvDiagnostic.setAdapter(adapter);
        } else {
            mRvDiagnostic.setVisibility(View.INVISIBLE);
            noDiagnosticDataTV.setVisibility(View.VISIBLE);
        }
    }

    private void populateViewHistory(ArrayList<HealthRecordsModel.Data> arrayList) {

        if (arrayList.size() > 0) {
            mRvHistory.setVisibility(View.VISIBLE);
            noHistory.setVisibility(View.GONE);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            mRvHistory.setLayoutManager(layoutManager);
            MedicalHistoryAdapter adapter = new MedicalHistoryAdapter(this, arrayList, noHistory);
            mRvHistory.setAdapter(adapter);
        } else {
            mRvHistory.setVisibility(View.INVISIBLE);
            noHistory.setVisibility(View.VISIBLE);
        }
    }

    private void getAllHealthRecords(String relativeId) {

        if (NetworkUtil.getConnectionStatus(this) != 0) {
            DialogUtils.showProgressDialog(this, getString(R.string.pleaseWaittxt));
            ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

            Call<GetHealthRecordsModel> call = apiService.getAllHealthRecords(relativeId);
            call.enqueue(new Callback<GetHealthRecordsModel>() {
                @Override
                public void onResponse(Call<GetHealthRecordsModel> call, Response<GetHealthRecordsModel> response) {
                    try {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "Success get health " + response.body());
                            for (int x = 0; x < response.body().getData().size(); x++) {
                                switch (response.body().getData().get(x).getTag()) {

                                    case Constants.HISTORY:
                                        historyList.clear();
                                        for (int y = 0; y < response.body().getData().get(x).getData().size(); y++) {
                                            historyList.add(response.body().getData().get(x).getData().get(y));
                                        }
                                        populateViewHistory(historyList);

                                        break;
                                    case Constants.REPORT:
                                        diagnosticList.clear();
                                        for (int y = 0; y < response.body().getData().get(x).getData().size(); y++) {
                                            diagnosticList.add(response.body().getData().get(x).getData().get(y));
                                        }
                                        populateViewDiagnostic(diagnosticList);
                                        break;

                                    case Constants.MEDICATION:
                                        currentMedicineList.clear();
                                        for (int y = 0; y < response.body().getData().get(x).getData().size(); y++) {
                                            currentMedicineList.add(response.body().getData().get(x).getData().get(y));
                                        }
                                        populateViewCurrentMedicine(currentMedicineList);
                                        break;

                                    case Constants.INTOLERANCE:
                                        allergyDrugList.clear();
                                        for (int y = 0; y < response.body().getData().get(x).getData().size(); y++) {
                                            allergyDrugList.add(response.body().getData().get(x).getData().get(y));
                                        }
                                        populateViewAllergy(allergyDrugList);
                                        break;

                                    default:
                                        break;
                                }
                            }
                            //DialogUtils.appToast(context, response.body().getMessage());

                        } else {
                            Log.e(TAG, "failed to update device id");
                            DialogUtils.appToast(HealthRecordActivity.this, new JSONObject(response.errorBody().string()).getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    DialogUtils.dismissProgressDialog();
                }

                @Override
                public void onFailure(Call<GetHealthRecordsModel> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                    DialogUtils.dismissProgressDialog();
                }
            });
        } else
            DialogUtils.appToast(HealthRecordActivity.this, getString(R.string.no_internet_connection));
    }
}