package com.doxtro.doctor.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.interfaces.DialogDataListener;
import com.doxtro.doctor.model.BankDetailModel;
import com.doxtro.doctor.model.ProfileModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.SharePref;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 16/10/17.
 */

public class BankDetailFragment extends Fragment implements DialogDataListener {
    @BindView(R.id.bank_details_addTV)
    TextView addTV;
    @BindView(R.id.bank_editLL)
    LinearLayout editLL;
    @BindView(R.id.bank_account_detail)
    LinearLayout bankDetailLL;
    @BindView(R.id.bank_descTV)
    TextView descTV;
    @BindView(R.id.bank_deleteIV)
    ImageView deleteIV;
    @BindView(R.id.bank_addLL)
    LinearLayout addLL;


    private DialogDataListener dialogDataListener;
    static List<BankDetailModel> bankDetailModel;


    public static BankDetailFragment newInstance() {
        Bundle args = new Bundle();
        BankDetailFragment fragment = new BankDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bank_detail, container, false);
        ButterKnife.bind(this, view);
        dialogDataListener = this;
        initView();
        return view;
    }

    private void initView() {
        if (SharePref.getDoctorDetail(getContext()) == null
                || SharePref.getDoctorDetail(getContext()).getData() == null
                || SharePref.getDoctorDetail(getContext()).getData() == null
                || SharePref.getDoctorDetail(getContext()).getData().getBankDetails() == null)
            return;
        List<BankDetailModel> bankDetail = SharePref.getDoctorDetail(getContext()).getData().getBankDetails();
//        if (bankDetailModel == null)
        bankDetailModel = bankDetail;
        if (bankDetail.size() > 0 && !bankDetail.isEmpty()) {
            addLL.setVisibility(View.GONE);
            bankDetailLL.setVisibility(View.VISIBLE);
            String name = bankDetail.get(0).getBank();
            String accountNo = bankDetail.get(0).getAccountNumber();
            descTV.setText(name + "\n" + accountNo);

        } else {
            addLL.setGravity(View.VISIBLE);
            bankDetailLL.setVisibility(View.GONE);
        }
        addTV.setOnClickListener(v -> {
            FragmentManager fragmentManager = getFragmentManager();
            bankDetailModel.clear();
            BankDetailDialogFragment bankDetailDialogFragment = BankDetailDialogFragment.newInstance(dialogDataListener, bankDetailModel);
            bankDetailDialogFragment.show(fragmentManager, TAG);
        });

        deleteIV.setOnClickListener(v -> new AlertDialog.Builder(getContext())
                .setMessage("Are you sure you want to delete?")
                .setNegativeButton(getContext().getString(R.string.cancelTxt), null)
                .setPositiveButton(getString(R.string.yestext), (dialog, which) -> {
                    addLL.setVisibility(View.VISIBLE);
                    bankDetailLL.setVisibility(View.GONE);
                    JsonArray jsonArray = new JsonArray();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("_id", SharePref.getDoctorID(getContext()));
                    jsonObject.add("bankDetails", jsonArray);
                    updateToserver(jsonObject);
                })
                .show());
        editLL.setOnClickListener(v -> {
            FragmentManager fragmentManager = getFragmentManager();
            BankDetailDialogFragment bankDetailDialogFragment = BankDetailDialogFragment.newInstance(dialogDataListener, bankDetailModel);
            bankDetailDialogFragment.show(fragmentManager, TAG);
        });
    }

    private void updateToserver(JsonObject jsonObject) {
        ApiInterface apiService = ApiClient.getClient(getContext()).create(ApiInterface.class);
        Call<ProfileModel> call = apiService.updateDoctorProfileWithObject(jsonObject);
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().getData().getBankDetails() != null && response.body().getData().getBankDetails().size() > 0) {
                            SharePref.getDoctorDetail(getContext()).getData().setBankDetails(response.body().getData().getBankDetails());
                            BankDetailFragment.bankDetailModel = response.body().getData().getBankDetails();
                        }
                        DialogUtils.appToast(getContext(), "Bank details updated!");

                    } else {
                        DialogUtils.appToast(getActivity(), new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    @Override
    public void getSummaryData(String summary) {

    }

    @Override
    public void getProfessionalData(Bundle professionData) {

    }

    @Override
    public void getEducationData(Bundle educationData) {

    }

    @Override
    public void getBankDetails(Bundle bankData) {
        if (bankData != null) {
            addLL.setVisibility(View.GONE);
            bankDetailLL.setVisibility(View.VISIBLE);
            String name = bankData.getString(Constants.BANK_NAME);
            String accountNo = bankData.getString(Constants.ACCOUNT_NUMBER);
            descTV.setText(name + "\n" + accountNo);
        }
    }
}
