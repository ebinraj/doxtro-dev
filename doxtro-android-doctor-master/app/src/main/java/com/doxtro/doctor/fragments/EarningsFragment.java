package com.doxtro.doctor.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.doxtro.doctor.R;
import com.doxtro.doctor.custom.NonScrollableViewPager;
import com.doxtro.doctor.utils.AndroidVersionUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 20/07/17.
 */

public class EarningsFragment extends Fragment {
    @BindView(R.id.earnings_viewpager)
    NonScrollableViewPager viewPager;
    @BindView(R.id.earnings_tablayout)
    TabLayout tabLayout;

    private MonthlyEarningFragment monthlyEarningFragment;
    private BankDetailFragment bankDetailFragment;


    public EarningsFragment() {
        //empty constructor
    }

    public static EarningsFragment newInstance() {
        Bundle args = new Bundle();
        EarningsFragment fragment = new EarningsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_earnings, container, false);
        ButterKnife.bind(this, view);
        initView();
      //  initChart();
        return view;
    }

    private void initView() {
        viewPager.setPagingEnabled(false);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(0);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(AndroidVersionUtility.getColor(getContext(), R.color.black), AndroidVersionUtility.getColor(getContext(), R.color.white));
        List<Fragment> viewPagerFragments = getViewPagerFragments();
        EarningsViewPagerAdapter earningsViewPagerAdapter = new EarningsViewPagerAdapter(getChildFragmentManager(), viewPagerFragments);
        viewPager.setAdapter(earningsViewPagerAdapter);
    }

    private List<Fragment> getViewPagerFragments() {
        List<Fragment> fragmentList = new ArrayList<>();
        if (monthlyEarningFragment == null) {
            monthlyEarningFragment = MonthlyEarningFragment.newInstance();
        }
        if (bankDetailFragment == null) {
            bankDetailFragment = BankDetailFragment.newInstance();
        }
        Collections.addAll(fragmentList, monthlyEarningFragment, bankDetailFragment);
        return fragmentList;
    }

    private class EarningsViewPagerAdapter extends FragmentStatePagerAdapter {
        List<Fragment> fragments;
        final CharSequence[] fragmentsTitle = {getString(R.string.monthearningtxt), getString(R.string.bankdetailstxt)};


        public EarningsViewPagerAdapter(FragmentManager childFragmentManager, List<Fragment> viewPagerFragments) {
            super(childFragmentManager);
            fragments = viewPagerFragments;
        }

        @Override
        public Fragment getItem(int position) {
            if (fragments != null && fragments.size() > 0) {
                return fragments.get(position);
            } else
                return null;
        }

        @Override
        public int getCount() {
            return (fragments != null ? fragments.size() : 0);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentsTitle[position];
        }
    }
}
