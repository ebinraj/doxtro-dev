package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 24/07/17.
 */

public class OTPVerifyResponse {

    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private OTPVerifyData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OTPVerifyData getData() {
        return data;
    }

    public void setData(OTPVerifyData data) {
        this.data = data;
    }

    public class OTPVerifyData {

        @SerializedName("_id")
        private String id;

        @SerializedName("mobile")
        private String mobile;

        @SerializedName("tag")
        private String tag;

        @SerializedName("accountStatus")
        private String accountStatus;

        @SerializedName("firstName")
        private String firstName;

        @SerializedName("token")
        private String token;

        @SerializedName("deviceId")
        private String deviceId;

        @SerializedName("emailid")
        private String emailid;



        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getAccountStatus() {
            return accountStatus;
        }

        public void setAccountStatus(String accountStatus) {
            this.accountStatus = accountStatus;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }


        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }
    }


}
