package com.doxtro.doctor.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.HomeActivity;
import com.doxtro.doctor.adapters.UplodDocumentAdapter;
import com.doxtro.doctor.adapters.UplodIDAdapter;
import com.doxtro.doctor.interfaces.BasicIndicatorListerner;
import com.doxtro.doctor.interfaces.DocumentUploadListener;
import com.doxtro.doctor.model.BasicProfileUpdateModel;
import com.doxtro.doctor.model.DocumentsModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.EventsUtility;
import com.doxtro.doctor.utils.FirebaseUtils;
import com.doxtro.doctor.utils.FragmentsActionUtility;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.doxtro.doctor.utils.Constants.BASIC_PROFILE;
import static com.doxtro.doctor.utils.Constants.CLICK_ID;
import static com.doxtro.doctor.utils.Constants.CLICK_MEDICAL_DOCUMENTS;
import static com.doxtro.doctor.utils.Constants.EDIT_PROFILE;
import static com.doxtro.doctor.utils.Constants.PICK_ID;
import static com.doxtro.doctor.utils.Constants.PICK_MEDICAL_DOCUMENTS;
import static com.doxtro.doctor.utils.Constants.SELECT_ID;
import static com.doxtro.doctor.utils.Constants.SELECT_MEDICAL_DOCUMENT;
import static com.doxtro.doctor.utils.Constants.SOURCE;

/**
 * Created by yashaswi on 20/07/17.
 */

public class UploadDocumentFragment extends Fragment implements View.OnClickListener, DocumentUploadListener {

    @BindView(R.id.upload_medicalC_gridview)
    GridView docGridview;
    @BindView(R.id.upload_document_nextBtn)
    Button nextBtn;
    @BindView(R.id.upload_documents_skipBtn)
    Button skipConfig;
    @BindView(R.id.upload_medicalC_add)
    TextView addMedicalCertificateTV;
    @BindView(R.id.idproof_gridview)
    GridView idGridView;
    @BindView(R.id.upload_id_add)
    TextView addIdTV;
    @BindView(R.id.skipNextLL)
    LinearLayout skipNext;
    @BindView(R.id.upload_document_saveBtn)
    Button saveBtn;
    @BindView(R.id.profile_statusTV)
    TextView profileStatus;


    private UplodDocumentAdapter uplodDocumentAdapter;
    private UplodIDAdapter uploadIDAdapter;
    private ArrayList<String> medicalDocuments = new ArrayList<>();
    private String displayName = "";
    private String tag = "";
    private ArrayList<DocumentsModel> documentsModels = new ArrayList<>();

    private static BasicProfileUpdateModel basicPtofileData = new BasicProfileUpdateModel();
    private FirebaseUtils firebaseUtils;
    private ArrayList<String> idProofs = new ArrayList<>();
    private String source = "";
    private static BasicIndicatorListerner indicatorListerner;


    public UploadDocumentFragment() {
        //empty constructor
    }


    //fragment instance
    public static UploadDocumentFragment newInstance(BasicProfileUpdateModel basicProfileModel, String source, BasicIndicatorListerner basicIndicatorListerner) {
        Bundle args = new Bundle();
        UploadDocumentFragment fragment = new UploadDocumentFragment();
        fragment.setArguments(args);
        args.putString(SOURCE, source);
        if (basicProfileModel != null) {
            basicPtofileData = basicProfileModel;
            Log.i("ABout yourself data", basicPtofileData.getBio());
        }
        if (basicIndicatorListerner != null) {
            indicatorListerner = basicIndicatorListerner;
        }

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.source = getArguments().getString(SOURCE);
        }
        Log.i("documents List", basicPtofileData.getDocuments().toString());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload_document, container, false);
        ButterKnife.bind(this, view);
        initListeners();
        if (SharePref.getDoctorDetail(getContext()) != null) {
            if (SharePref.getDoctorDetail(getContext()).getData() != null && SharePref.getDoctorDetail(getContext()).getData() != null) {
                String status = SharePref.getDoctorDetail(getContext()).getData().getStatus();
                switch (status) {
                    case Constants.PENDING:
                        profileStatus.setText("Profile Status : Pending");
                        profileStatus.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.grey_text));
                        break;
                    case Constants.READY:
                        profileStatus.setText("Profile Status : Ready");
                        profileStatus.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.grey_text));
                        break;
                    case Constants.APPROVED:
                        profileStatus.setText("Profile Status : Approved");
                        profileStatus.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.green));
                        break;
                    case Constants.REJECTED:
                        profileStatus.setText("Profile Status : Rejected");
                        profileStatus.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.red));
                        break;
                }
            }
        }
        firebaseUtils = new FirebaseUtils(this);
        docGridview.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
        idGridView.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
        if (source.contains(BASIC_PROFILE)) {
            skipNext.setVisibility(View.VISIBLE);
            saveBtn.setVisibility(View.GONE);
        } else if (source.contains(EDIT_PROFILE)) {
            saveBtn.setVisibility(View.VISIBLE);
            skipNext.setVisibility(View.GONE);
            if (NetworkUtil.getConnectionStatus(getActivity()) != 0)
                getDocumentsUploaded();
            else
                DialogUtils.appToast(getActivity(), getString(R.string.no_internet_connection));
        }
        return view;
    }


    //initialize all the listeners
    private void initListeners() {
        nextBtn.setOnClickListener(this);
        skipConfig.setOnClickListener(this);
        addMedicalCertificateTV.setOnClickListener(this);
        addIdTV.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
    }

    //api call to get the documents uploaded
    private void getDocumentsUploaded() {
        try {
            ArrayList<DocumentsModel> documentsUploaded = SharePref.getDoctorDetail(getContext()).getData().getDocuments();

            if (documentsUploaded.size() > 0) {
                ArrayList<DocumentsModel> certificates = new ArrayList<>();
                ArrayList<DocumentsModel> ids = new ArrayList<>();
                documentsModels = documentsUploaded;
                for (int i = 0; i < documentsModels.size(); i++) {
                    if (documentsModels.get(i).getTag().contains(getString(R.string.certificates))) {
                        //   certificates.add("Medical Certificate");
                        certificates.add(documentsModels.get(i));
                        uplodDocumentAdapter = new UplodDocumentAdapter(getContext(), certificates);
                        docGridview.setAdapter(uplodDocumentAdapter);
                    } else if (documentsModels.get(i).getTag().contains(getString(R.string.idProof))) {
                        //  ids.add("Id proofs");
                        ids.add(documentsModels.get(i));
                        uploadIDAdapter = new UplodIDAdapter(getContext(), ids);
                        idGridView.setAdapter(uploadIDAdapter);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //for the data received from intents
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Uri uri = data.getData();
            switch (requestCode) {
                case PICK_MEDICAL_DOCUMENTS:
                    FirebaseUtils.storeInFirebase(getContext(), uri, Constants.TYPE_DOCUMENT);
                    displayName = FirebaseUtils.getDisplayName();
                    medicalDocuments.add(displayName);
                    tag = getString(R.string.certificates);
                    break;
                case PICK_ID:
                    FirebaseUtils.storeInFirebase(getContext(), uri, Constants.TYPE_DOCUMENT);
                    displayName = FirebaseUtils.getDisplayName();
                    idProofs.add(displayName);
                    tag = getString(R.string.idProof);
                    break;
                case CLICK_MEDICAL_DOCUMENTS:
                    Bitmap capturedCertificate = CameraUtils.capturedImage(getActivity(), data);
                    FirebaseUtils.storeInFirebase(getContext(), getImageUri(capturedCertificate), Constants.TYPE_DOCUMENT);
                    displayName = FirebaseUtils.getDisplayName();
                    medicalDocuments.add(displayName);
                    tag = getString(R.string.certificates);
                    break;
                case CLICK_ID:
                    Bitmap caturedID = CameraUtils.capturedImage(getActivity(), data);
                    FirebaseUtils.storeInFirebase(getContext(), getImageUri(caturedID), Constants.TYPE_DOCUMENT);
                    displayName = FirebaseUtils.getDisplayName();
                    idProofs.add(displayName);
                    tag = getString(R.string.idProof);
                    break;

            }
        }
    }

    private Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    //when the user adds each documents
    private void addDocumentsList(String url) {
        DocumentsModel document = new DocumentsModel();
        document.setTag(tag);
        document.setUrl(url);
        documentsModels.add(document);
        ArrayList<DocumentsModel> certificates = new ArrayList<>();
        ArrayList<DocumentsModel> ids = new ArrayList<>();
        for (int i = 0; i < documentsModels.size(); i++) {
            if (documentsModels.get(i).getTag().contains(getString(R.string.certificates))) {
                certificates.add(documentsModels.get(i));
                uplodDocumentAdapter = new UplodDocumentAdapter(getContext(), certificates);
                docGridview.setAdapter(uplodDocumentAdapter);
            } else {
                ids.add(documentsModels.get(i));
                uploadIDAdapter = new UplodIDAdapter(getContext(), ids);
                idGridView.setAdapter(uploadIDAdapter);
            }
        }
    }

    //allow user to click or choose the document
    private void chooseDocumentAlertDialog(String type) {
        CharSequence[] items = {getContext().getString(R.string.chooseDocument), getContext().getString(R.string.takephoto)};
        new AlertDialog.Builder(getContext())
                .setTitle("Select your option")
                .setNegativeButton(getContext().getString(R.string.cancelTxt), null)
                .setItems(items, (dialog, which) -> {
                    if (which == 0) {
                        pickDocument(type);
                    } else if (which == 1) {
                        clickDocument(type);
                    } else {
                        dialog.dismiss();
                    }
                }).show();
    }

    //to click the doc picture
    private void clickDocument(String type) {
        try {
            int requestId = ((type.contains(SELECT_MEDICAL_DOCUMENT)) ? CLICK_MEDICAL_DOCUMENTS : Constants.CLICK_ID);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, requestId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //to pick the document
    private void pickDocument(String type) {
        int requestId = ((type.contains(SELECT_MEDICAL_DOCUMENT)) ? Constants.PICK_MEDICAL_DOCUMENTS : Constants.PICK_ID);
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), requestId);
    }


    //Api call to upload all the documents and basic data entered
    private void updloadDoctorProfile() {
        if (SharePref.getDoctorDetail(getContext()) != null) {
            if (SharePref.getDoctorDetail(getContext()).getData().getWorksFor().size() > 0) {
                basicPtofileData.setWorksFor(SharePref.getDoctorDetail(getContext()).getData().getWorksFor());
            }
            if (SharePref.getDoctorDetail(getContext()).getData().getQualification().size() > 0) {
                basicPtofileData.setQualification(SharePref.getDoctorDetail(getContext()).getData().getQualification());
            }
            if (SharePref.getDoctorDetail(getContext()).getData() != null && SharePref.getDoctorDetail(getContext()).getData().getBio() != null) {
                basicPtofileData.setBio(SharePref.getDoctorDetail(getContext()).getData().getBio());
            }
            if (SharePref.getDoctorDetail(getContext()).getData() != null && SharePref.getDoctorDetail(getContext()).getData().getCategory() != null) {
                basicPtofileData.setCategory(SharePref.getDoctorDetail(getContext()).getData().getCategory());
            }
            if(SharePref.getDoctorDetail(getContext()).getData()!=null && SharePref.getDoctorDetail(getContext()).getData().getBankDetails()!=null)
            {
                basicPtofileData.setBankDetails(SharePref.getDoctorDetail(getContext()).getData().getBankDetails());
            }
        }
        if (basicPtofileData.getDuns() == null)
            basicPtofileData.setDuns("");
        basicPtofileData.setDocuments(documentsModels);
        SharePref.setProfileConfigShown(getContext(), true);
        DialogUtils.showProgressDialog(getActivity(), getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<BasicProfileUpdateModel> call = apiService.updateDoctorProfile(basicPtofileData);
        call.enqueue(new Callback<BasicProfileUpdateModel>() {
            @Override
            public void onResponse(Call<BasicProfileUpdateModel> call, Response<BasicProfileUpdateModel> response) {
                try {
                    if (response.isSuccessful()) {

                        if (source.contains(Constants.BASIC_PROFILE)) {
                            HashMap<String, Object> profileUpdate = new HashMap<>();
                            profileUpdate.put(Constants.KEY_NAME, response.body().getFirstName());// String
                            profileUpdate.put(Constants.KEY_EMAIL, response.body().getEmailid());               // Email address of the user
                            profileUpdate.put(Constants.KEY_GENDER, response.body().getGender());                 // Phone (with the country code, starting with +)
                            profileUpdate.put(Constants.KEY_MOBILE, response.body().getMobile());                 // Phone (with the country code, starting with +)
                            EventsUtility.pushProfile(getContext(),profileUpdate);
                            Toast.makeText(getContext(), "Profile saved!", Toast.LENGTH_SHORT).show();
                            DialogUtils.dismissProgressDialog();
                            indicatorListerner.changeIndicatorStatus(2);
                            AvailabilityFragment availabilityFragment = AvailabilityFragment.newInstance(Constants.BASIC_PROFILE);
                            FragmentsActionUtility.replaceFragment(availabilityFragment, R.id.profile_confile_frame, getFragmentManager());
                        } else {
                            Toast.makeText(getContext(), "Document saved!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<BasicProfileUpdateModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }


    //click method
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upload_document_nextBtn:
                if (NetworkUtil.getConnectionStatus(getActivity()) != 0)
                    updloadDoctorProfile();
                else
                    DialogUtils.appToast(getActivity(), getString(R.string.no_internet_connection));
                break;
            case R.id.upload_documents_skipBtn:
                getContext().startActivity(new Intent(getActivity(), HomeActivity.class));
                break;
            case R.id.upload_medicalC_add:
                chooseDocumentAlertDialog(SELECT_MEDICAL_DOCUMENT);
                break;
            case R.id.upload_id_add:
                chooseDocumentAlertDialog(SELECT_ID);
                break;
            case R.id.upload_document_saveBtn:
                if (NetworkUtil.getConnectionStatus(getActivity()) != 0)
                    updloadDoctorProfile();
                else
                    DialogUtils.appToast(getActivity(), getString(R.string.no_internet_connection));
                break;
        }

    }


    //interface method when firebase sends the url
    @Override
    public void getDownloadUrl(String url) {

        if (basicPtofileData.getId() != null) {
            addDocumentsList(url);
        } else {
            basicPtofileData.setId(SharePref.getDoctorID(getContext()));
            addDocumentsList(url);
        }
    }

    @Override
    public void getDownloadUrlType(String url, String type) {

    }

    @Override
    public void getDeleteResponse(String url, String tag) {
        // change to the feasible logic to delete
        ArrayList<DocumentsModel> certificates = new ArrayList<>();
        ArrayList<DocumentsModel> ids = new ArrayList<>();
        for (int i = 0; i < documentsModels.size(); i++) {
            if (documentsModels.get(i).getTag().equals(getString(R.string.certificates))) {
                certificates.add(documentsModels.get(i));
//                uplodDocumentAdapter.updateData(certificates);
            } else if (documentsModels.get(i).getTag().equals(getString(R.string.idProof))) {
                ids.add(documentsModels.get(i));
//                uploadIDAdapter.updateData(ids);
            }
            if (tag.equals(getString(R.string.certificates))) {
                for (int j = 0; j < certificates.size(); j++) {
                    if (certificates.get(j).getUrl().equals(url))
                        certificates.remove(j);
                    uplodDocumentAdapter.updateData(certificates);

                }
            } else {
                for (int j = 0; j < ids.size(); j++) {
                    if (ids.get(j).getUrl().equals(url))
                        ids.remove(j);
                    uploadIDAdapter.updateData(ids);

                }
            }
            if (documentsModels.get(i).getUrl().equals(url)) {
                documentsModels.remove(i);
            }

        }

    }
//        Log.e("removed", "yes");
}
