package com.doxtro.doctor.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.util.Log;

import com.doxtro.doctor.R;
import com.doxtro.doctor.interfaces.DocumentUploadListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

/**
 * Created by yashaswi on 07/08/17.
 */

public class FirebaseUtils {


    private static Uri downloadUrl;
    private static String displayName;
    private static StorageReference mStorageRef;
    private static DocumentUploadListener documentUploadListener;

    public FirebaseUtils(DocumentUploadListener documentUploadListener) {
        this.documentUploadListener = documentUploadListener;
    }

    public static void storeInFirebase(Context context, Uri uri, String type) {
        DialogUtils.showProgressDialog(context, context.getString(R.string.pleaseWaittxt));
        StorageReference riversRef = null;
        mStorageRef = FirebaseStorage.getInstance().getReference();
        File myFile = new File(uri.toString());
        if (uri.toString().startsWith("content://")) {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    Log.i("display name", displayName);
                }
            } finally {
                cursor.close();
            }
        } else if (uri.toString().startsWith("file://")) {
            displayName = myFile.getName();
            Log.i("display name", displayName);
        }
        if (type.equals(Constants.TYPE_DOCUMENT)) {
            riversRef = mStorageRef.child("doctor_documents_android/" + SharePref.getDoctorID(context) + displayName);
        } else if (type.equals(Constants.TYPE_PICTURE)) {
            riversRef = mStorageRef.child("doctor_picture_android/" + SharePref.getDoctorID(context));
        } else if (type.equals(Constants.CHAT_IMAGE) || type.equals(Constants.CHAT_DOCUMENTS)) {
            riversRef = mStorageRef.child("chat_documents/" + SharePref.getDoctorID(context) + DateTimeUtility.getCurrentTimeInMilliSec());
        }
        UploadTask uploadTask = riversRef.putFile(uri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                DialogUtils.dismissProgressDialog();
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("downloadUrl", "" + downloadUrl);
                DialogUtils.dismissProgressDialog();
                documentUploadListener.getDownloadUrl(downloadUrl.toString());
                documentUploadListener.getDownloadUrlType(downloadUrl.toString(), type);
                //  getDocumentName(docUri, type);
            }
        });
    }

    public static void deleteFromFirebase(String url, Context context, String tag) {
        mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl(url);
        mStorageRef.delete().addOnSuccessListener(aVoid -> {
            DialogUtils.appToast(context, "Document deleted successfully");
            documentUploadListener.getDeleteResponse(url,tag);
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });


    }

    public static String getDisplayName() {
        return displayName;
    }


}
