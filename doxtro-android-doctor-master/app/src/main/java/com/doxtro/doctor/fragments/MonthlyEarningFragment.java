package com.doxtro.doctor.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.AllEarningsActivity;
import com.doxtro.doctor.adapters.EarningsAdapter;
import com.doxtro.doctor.model.EarningDataModel;
import com.doxtro.doctor.model.EarningsModel;
import com.doxtro.doctor.model.WeeklyEarningsModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.EventsUtility;
import com.doxtro.doctor.utils.SharePref;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 16/10/17.
 */

public class MonthlyEarningFragment extends Fragment implements OnChartValueSelectedListener {
    @BindView(R.id.earningRV)
    RecyclerView earningRV;
    @BindView(R.id.earning_moreTV)
    TextView moreTV;
    @BindView(R.id.barchart)
    BarChart barChartSimple;
    @BindView(R.id.earnings_noDataTV)
    TextView monthlyEarningNOTV;


    private RecyclerView.LayoutManager layoutManager;
    private List<EarningDataModel> earningsList = new ArrayList<>();

    public MonthlyEarningFragment() {
    }

    public static MonthlyEarningFragment newInstance() {
        Bundle args = new Bundle();
        MonthlyEarningFragment fragment = new MonthlyEarningFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_month_earning, container, false);
        ButterKnife.bind(this, view);
        getMonthlyEarnings();
        initChart();
        return view;
    }

    private void initChart() {
        getWeeklyEarnings();
        // all of these values along with X axis values are set in the bar chart
        barChartSimple.getAxisLeft().setEnabled(true); // it enables/disables left Axis Line(border)
        barChartSimple.getAxisRight().setEnabled(true); //it enables/disables right Axis Line(border)
        barChartSimple.getXAxis().setEnabled(true); // it enables/disables X Axis Line(border)
        barChartSimple.getLegend().setEnabled(true); // legend at bottom can enable and disable for showing like red color for something and green for something
        barChartSimple.setDescription(""); // it will show description on graph
        barChartSimple.animateX(2000); // animate graph with y axis
        barChartSimple.getXAxis().setDrawGridLines(false); // it enables/disables vertical lines in background
        barChartSimple.getAxisRight().setDrawGridLines(false); // see below line
        barChartSimple.getAxisLeft().setDrawGridLines(false); // it enables/disables horizontal lines in background but both left and right axis must be false
        barChartSimple.getAxisLeft().setDrawLabels(true); // it enables/disables values on axis Y left
        barChartSimple.getAxisRight().setDrawLabels(false); // it enables/disables values on axis Y right
        barChartSimple.getXAxis().setDrawLabels(true); // it enables/disables values on axis X, top/bottom whatever selected in xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        barChartSimple.setDrawGridBackground(false); // it enables/disables back ground if we want to give a back ground color to grid
        barChartSimple.getAxisLeft().setTextColor(Color.parseColor("#58595B")); // sets color for values on Axis Left
        barChartSimple.getAxisRight().setTextColor(Color.parseColor("#FFCE00")); // sets color for values on Axis Right
        barChartSimple.setGridBackgroundColor(Color.GRAY); // grid bach ground color
        barChartSimple.setBackgroundColor(Color.TRANSPARENT); // sets the color to chart back ground ( not grid background)
        barChartSimple.invalidate(); // to make the bar chart reflect this latest data, we need to call this invalidate method
        XAxis xAxis = barChartSimple.getXAxis(); // default X axis will appears at top ,
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); // this will set x axis values in selected position(BOTTOM/TOP/BOTH_SIDED/BOTTOM_INSIDE,TOP_INSIDE) of the graph
        xAxis.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.grey_text));
        barChartSimple.getAxisRight().setEnabled(false);
        barChartSimple.setDrawBarShadow(false);// shows back ground of bars from top to bottom as shadow
        //   barChartSimple.setOnChartValueSelectedListener(this);
        barChartSimple.notifyDataSetChanged();
    }

    private void getWeeklyEarnings() {
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<WeeklyEarningsModel> call = apiService.getWeeklyEarnings(SharePref.getDoctorID(getContext()));
        call.enqueue(new Callback<WeeklyEarningsModel>() {
            @Override
            public void onResponse(Call<WeeklyEarningsModel> call, Response<WeeklyEarningsModel> response) {
                try {
                    if (response.isSuccessful()) {
                        BarData data = new BarData(getXAxisValues(response.body()), getDataSet(response.body()));
                        data.setHighlightEnabled(true); // it enables/disables selected value as high light
                        data.setDrawValues(true); // it enables/disables values on top of every bar ( at Every index)
                        data.setValueTextColor(AndroidVersionUtility.getColor(getContext(), R.color.grey_text)); // it sets text color to values which is showing on top of every bar
                        data.setValueTextSize(10f); // sets value's text size (in float)
                        //data.setValueFormatter(ValueFormator); // here we can set value formatter
                        barChartSimple.setData(data);
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<WeeklyEarningsModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }


    private void getMonthlyEarnings() {
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<EarningsModel> call = apiService.getMonthEarnings(SharePref.getDoctorID(getContext()));
        call.enqueue(new Callback<EarningsModel>() {
            @Override
            public void onResponse(Call<EarningsModel> call, Response<EarningsModel> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().getData() != null) {
                            earningsList = response.body().getData();
                            int firstFortnightMonth = 0;
                            int secondFortnightMonth = 0;
                            for (int i = 0; i < earningsList.size(); i++) {
                                String date = earningsList.get(i).getDate();

                                @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat =
                                        new SimpleDateFormat(Constants.SERVER_DATE_FORMAT);
                                Date dDate = simpleDateFormat.parse(date);
                                int month = dDate.getMonth();
                                int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
                                if (month == currentMonth) {
                                    int earning = earningsList.get(i).getEarning();
                                    if (dDate.getDate() <= 15) {
                                        firstFortnightMonth = firstFortnightMonth + earning;
                                    } else {
                                        secondFortnightMonth = secondFortnightMonth + earning;
                                    }
                                }
                                Log.e("EarningsDate", dDate.toString());
                            }

                            try {
                                Map<String, Object> eventValue = new HashMap<>();
                                eventValue.put("1st to 15th", firstFortnightMonth);
                                eventValue.put("16th to 31st", secondFortnightMonth);
                                eventValue.put("currencyType", "INR");
                                EventsUtility.eventTrack(getActivity(),
                                        Constants.EVENT_EARNINGS,
                                        eventValue);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (earningsList.size() > 0) {
                                initRV();
                            } else {
                                monthlyEarningNOTV.setVisibility(View.VISIBLE);
                            }
                            if (earningsList.size() > 2) {
                                moreTV.setVisibility(View.VISIBLE);
                            }
                            moreTV.setOnClickListener(v -> {
                                Intent intent = new Intent(getActivity(), AllEarningsActivity.class);
                                startActivity(intent);

                            });
                        }
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<EarningsModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    private ArrayList<String> getXAxisValues(WeeklyEarningsModel earningsModel) {
        ArrayList<String> xAxis = new ArrayList<>();
        for (int i = 0; i < earningsModel.getData().size(); i++) {
            xAxis.add(i, String.valueOf(earningsModel.getData().get(i).getWeek() + " Wk"));
        }
        return xAxis;
    }


    private void initRV() {
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        earningRV.setLayoutManager(layoutManager);
        EarningsAdapter recentEarningAdapter = new EarningsAdapter(getContext(), earningsList, Constants.MONTHLY_EARNING);
        earningRV.setAdapter(recentEarningAdapter);

    }

    private ArrayList<IBarDataSet> getDataSet(WeeklyEarningsModel earningsModel) {
        ArrayList<IBarDataSet> dataSets;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        List<String> yValues = new ArrayList<>();
        for (int i = 0; i < earningsModel.getData().size(); i++) {
            yValues.add(i, String.valueOf(earningsModel.getData().get(i).getTotalEarnings()));
        }


        // float number1 = 670.000f;
        for (int value = 0; value < yValues.size(); value++) {
            BarEntry barEntry1 = new BarEntry(Float.parseFloat(yValues.get(value)), value); //
            valueSet1.add(barEntry1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Doctor earnings");
        barDataSet1.setColor(Color.parseColor("#1CA39D")); // set single color to bar
        //barDataSet1.setColor(Color.rgb(0, 155, 0)); // can set color like this
        //barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS); // set multi color to bar

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        return dataSets;
    }


    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        //  Toast.makeText(getContext(), "Selected value : " + e.getVal(), Toast.LENGTH_SHORT).show();
        // get index
        Log.e("Index : ", String.valueOf(e.getXIndex()));
    }

    @Override
    public void onNothingSelected() {
        // Toast.makeText(getContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();

    }
}
