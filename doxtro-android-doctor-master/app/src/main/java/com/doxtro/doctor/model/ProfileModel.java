package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 16/08/17.
 */

public class ProfileModel {

    @SerializedName("message")
    private String message;

    @SerializedName("profilePercentage")
    private Integer profilePercentage;

    @SerializedName("data")
    private BasicProfileUpdateModel data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getProfilePercentage() {
        return profilePercentage;
    }

    public void setProfilePercentage(Integer profilePercentage) {
        this.profilePercentage = profilePercentage;
    }

    public BasicProfileUpdateModel getData() {
        return data;
    }

    public void setData(BasicProfileUpdateModel data) {
        this.data = data;
    }




}