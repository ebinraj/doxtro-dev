package com.doxtro.doctor.helpers;

/**
 * Created by yashaswi on 03/08/17.
 */

public class SpecilizationExpandableRow {
    private String subSpecialization = "";

    public SpecilizationExpandableRow(String subSpecialization) {
        super();
        this.subSpecialization = subSpecialization;
    }

    public String getSubSpecialization() {
        return subSpecialization;
    }

    public void setSubSpecialization(String subSpecialization) {
        this.subSpecialization = subSpecialization;
    }
}
