package com.doxtro.doctor.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.EditBasicInfoActivity;
import com.doxtro.doctor.fragments.EditInfoDialogFragment;
import com.doxtro.doctor.interfaces.DialogDataListener;
import com.doxtro.doctor.model.ProfessionModel;
import com.doxtro.doctor.model.QualificationModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 14/09/17.
 */

public class ProfessionEducationAdapter extends RecyclerView.Adapter<ProfessionEducationAdapter.MyViewHolder> implements DialogDataListener {
    private List<String> degrees = new ArrayList<>();
    private final Context mContext;
    private List<ProfessionModel> professionList = new ArrayList<>();
    private List<QualificationModel> educationList = new ArrayList<>();
    private String source = "";
    private EditInfoDialogFragment editInfoDialogFragment;

    public ProfessionEducationAdapter(String source, Context context, List<ProfessionModel> professionList, List<QualificationModel> educationList, List<String> degreeList) {
        mContext = context;
        this.professionList = professionList;
        this.educationList = educationList;
        this.source = source;
        if (degrees != null) {
            degrees = degreeList;
        }
    }

    @Override
    public ProfessionEducationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.profession_education_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfessionEducationAdapter.MyViewHolder holder, int position) {
        if (holder != null) {
            if (source.contains(Constants.EDUCATION)) {
                try {
                    QualificationModel qualificationModel = educationList.get(position);
                    if (!educationList.get(position).getInstitute().equals("")) {
                        holder.textView1.setText(educationList.get(position).getInstitute());
                    } else {
                        holder.textView1.setVisibility(View.GONE);
                    }
                    if (qualificationModel.getAdditionalData() != null && !qualificationModel.getAdditionalData().equals("")) {
                        holder.textView2.setText(qualificationModel.getType() + " - " + qualificationModel.getSpecialization() + " - " + qualificationModel.getAdditionalData());
                    } else {
                        holder.textView2.setText(qualificationModel.getType() + " - " + qualificationModel.getSpecialization());
                    }
                    if (qualificationModel.getYearTo() != null && qualificationModel.getYearFrom() != null) {
                        if (!qualificationModel.getYearTo().equals("") || !qualificationModel.getYearFrom().equals("")) {
                            holder.textView3.setText(qualificationModel.getYearFrom() + " - " + qualificationModel.getYearTo());
                        }
                    }
                    holder.editIV.setOnClickListener(v -> callEditDialog(Constants.EDUCATION, position));
                    holder.iconIV.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.drawable.ic_education));
                    holder.deleteIV.setOnClickListener(v -> {
                        educationList.remove(position);
                        notifyDataSetChanged();
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ProfessionModel professionModel = professionList.get(position);

                try {
                    //   holder.iconIV.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.drawable.ic_add));
                    holder.textView1.setText(professionModel.getOrganization());
                    if (professionModel.getExperience() != null) {
                        if (!professionModel.getExperience().isEmpty()) {

//                    if (Integer.parseInt(professionModel.getExperience()) == 1) {
                            holder.textView2.setText(professionModel.getExperience() + " year");
//                    } else {
                            holder.textView2.setText(professionModel.getExperience() + " years");
//                    }
                        } else {
                            holder.textView2.setText("0 years");
                        }
                    } else {
                        holder.textView2.setText("0 years");
                    }

                    holder.iconIV.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.drawable.ic_hospital));
                    holder.editIV.setOnClickListener(v -> callEditDialog(Constants.PROFESSION, position));
                    holder.deleteIV.setOnClickListener(v -> {
                        professionList.remove(position);
                        notifyDataSetChanged();
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void callEditDialog(String source, int position) {
        EditBasicInfoActivity activity = (EditBasicInfoActivity) (mContext);
        FragmentManager fm = activity.getSupportFragmentManager();
        if (source.contains(Constants.PROFESSION)) {
            editInfoDialogFragment = EditInfoDialogFragment.newInstance(source, position, true, professionList, null, null, this);
        } else if (source.contains(Constants.EDUCATION)) {
            editInfoDialogFragment = EditInfoDialogFragment.newInstance(source, position, true, null, educationList, degrees, this);
        }
        editInfoDialogFragment.show(fm, "profession_alert");
        this.notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return ((source.contains(Constants.EDUCATION)) ? educationList.size() : professionList.size());
    }

    @Override
    public void getSummaryData(String summary) {

    }

    @Override
    public void getProfessionalData(Bundle professionData) {
        int position = professionData.getInt("position");
        String organization = professionData.getString("working_at");
        String experience = professionData.getString("experience");
        professionList.get(position).setOrganization(organization);
        professionList.get(position).setExperience(experience);
        notifyItemChanged(position);

    }

    @Override
    public void getEducationData(Bundle educationData) {
        int position = educationData.getInt("position");
        String organization = educationData.getString("organization");
        String type = educationData.getString("type");
        String degree = educationData.getString("degree");
        String additionalData = educationData.getString("additional_data");
        String yearTo = educationData.getString("yearTo");
        String yearFrom = educationData.getString("yearFrom");
        educationList.get(position).setSpecialization(degree);
        educationList.get(position).setType(type);
        educationList.get(position).setInstitute(organization);
        educationList.get(position).setYearTo(yearTo);
        educationList.get(position).setYearFrom(yearFrom);
        educationList.get(position).setAdditionalData(additionalData);
        //  educationList.get(v).set
        // educationList.add(qualificationModel);
        notifyItemChanged(position);
        // notifyDataSetChanged();
    }

    @Override
    public void getBankDetails(Bundle bankData) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profession_education_icon)
        ImageView iconIV;
        @BindView(R.id.profession_education_textview1)
        TextView textView1;
        @BindView(R.id.profession_education_textview2)
        TextView textView2;
        @BindView(R.id.profession_education_textview3)
        TextView textView3;
        @BindView(R.id.profession_education_editIV)
        ImageView editIV;
        @BindView(R.id.profession_education_deleteIV)
        ImageView deleteIV;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
