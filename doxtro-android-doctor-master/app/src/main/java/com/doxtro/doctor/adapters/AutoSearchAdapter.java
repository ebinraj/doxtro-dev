package com.doxtro.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.doxtro.doctor.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 13/10/17.
 */

public class AutoSearchAdapter extends RecyclerView.Adapter<AutoSearchAdapter.MyViewHolder> {
    private Context mContext;
    private List<String> searchList;
    private EditText editText;


    public AutoSearchAdapter(Context context, List<String> searchList, EditText presET) {
        mContext = context;
        this.searchList = searchList;
        editText = presET;

    }

    @Override
    public AutoSearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.search_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AutoSearchAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (holder == null || searchList == null) return;
        String searchData = searchList.get(position);
        holder.searchName.setText(searchData);
        holder.itemView.setOnClickListener(v -> editText.setText(searchList.get(position)));
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.search_suggestionTV)
        TextView searchName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
