package com.doxtro.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.utils.AndroidVersionUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 13/10/17.
 */

public class AutoSuggestionAdapter extends BaseAdapter {
    private List<String> suggestions = new ArrayList<>();
    private TextView medicineTV;
    private RelativeLayout mainView;
    private final Context mContext;
    private final EditText editText;

    public AutoSuggestionAdapter(Context context, List<String> suggestionList, EditText presET) {
        suggestions = suggestionList;
        mContext = context;
        editText=presET;
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    @Override
    public Object getItem(int position) {
        return suggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.upload_document_item, parent, false);
        medicineTV = (TextView) convertView.findViewById(R.id.document_name);
        mainView = (RelativeLayout) convertView.findViewById(R.id.document_item_mainLL);
        GradientDrawable bgShape = (GradientDrawable) mainView.getBackground().getCurrent();
        bgShape.setColor(AndroidVersionUtility.getColor(mContext, R.color.appColor));
        bgShape.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.appColor));
        medicineTV.setText(suggestions.get(position));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 10, 10, 10);
        medicineTV.setLayoutParams(layoutParams);
        convertView.setOnClickListener(v -> editText.setText(suggestions.get(position).toString()));

        return convertView;
    }
}

