package com.doxtro.doctor.interfaces;

/**
 * Created by yashaswi on 09/08/17.
 */

public interface DocumentUploadListener {

    public void getDownloadUrl(String url);

    public void getDownloadUrlType(String url, String type);

    public void getDeleteResponse(String url, String tag);
}
