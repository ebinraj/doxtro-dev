package com.doxtro.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.utils.AndroidVersionUtility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by yashaswi on 20/07/17.
 */

public class AvailabilityAdapter extends BaseAdapter {

    private final Context mContext;
    private  ArrayList<Integer> times;
    private ArrayList<HashMap<String, Integer>> selectedSlotsArr;

    private int selectedDay;

    private GradientDrawable leftSelectedSlot;
    private GradientDrawable rightSelectedSlot;
    private GradientDrawable halfSelectedSlot;

    public AvailabilityAdapter(Context context, ArrayList<Integer> times, ArrayList<HashMap<String, Integer>> selectedSlots, int day) {
        mContext = context;
        this.times = times;
        selectedSlotsArr = selectedSlots;
        selectedDay = day;

        int mRadius = 70;

        leftSelectedSlot = new GradientDrawable();
        leftSelectedSlot.setColor(mContext.getResources().getColor(R.color.appColor));
        leftSelectedSlot.setCornerRadii(new float[] { mRadius, mRadius, 0, 0, 0, 0, mRadius, mRadius });

        rightSelectedSlot = new GradientDrawable();
        rightSelectedSlot.setColor(mContext.getResources().getColor(R.color.appColor));
        rightSelectedSlot.setCornerRadii(new float[] {0, 0, mRadius, mRadius, mRadius, mRadius, 0, 0});

        halfSelectedSlot = new GradientDrawable();
        halfSelectedSlot.setColor(mContext.getResources().getColor(R.color.light_appcolor));
        halfSelectedSlot.setCornerRadii(new float[] {mRadius, mRadius, mRadius, mRadius, mRadius, mRadius, mRadius, mRadius});
    }

    public int getCount() {
        return times.size();
    }

    public Object getItem(int position) {

        String timeString = times.get(position).toString();

        if (timeString.length() < 2) timeString = "00" + timeString;

        else if (timeString.length() < 3) timeString = "0" + timeString;

        return timeString.substring(0, timeString.length() - 2) + ":" + timeString.substring(timeString.length() - 2);
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    @SuppressLint({"InflateParams", "ViewHolder"})
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(mContext).inflate(R.layout.availablility_time_slot, null);

        TextView timeSlotTV = (TextView) convertView.findViewById(R.id.time_slot);
        timeSlotTV.setText(getItem(position).toString());
//        if (checkIfSelected(position) == 1 || checkIfSelected(position) == 2 || checkIfSelected(position) == 3) {
//            timeSlotTV.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.rounded_button_appcolor_background));
//            timeSlotTV.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
//        }
        if(checkIfSelected(position) == 1) {
            timeSlotTV.setBackground(leftSelectedSlot);
            timeSlotTV.setTextColor(AndroidVersionUtility.getColor(mContext,R.color.white));
           // timeSlotTV.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.rounded_button_appcolor_background));
          //  timeSlotTV.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
        }
        else if(checkIfSelected(position) == 2) {
//            timeSlotTV.setBackground(halfSelectedSlot);
            timeSlotTV.setBackground(AndroidVersionUtility.getDrawable(mContext,R.color.grey));
            timeSlotTV.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
        }
        else if(checkIfSelected(position) == 3) {
            timeSlotTV.setBackground(rightSelectedSlot);
            timeSlotTV.setTextColor(AndroidVersionUtility.getColor(mContext,R.color.white));
//            timeSlotTV.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.rounded_button_appcolor_background));
//            timeSlotTV.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
        }
        return convertView;

    }

    //check if slots selected previously or not
    private int checkIfSelected(int position) {

        int value = times.get(position);

        /*
            0 = not selected
            1 = starting
            2 = middle
            3 = end
         */

        // loop in arrar>hasmap and find if any value starts with it,
        for (int i = 0; i < selectedSlotsArr.size(); i++) {
            HashMap<String, Integer> slot = selectedSlotsArr.get(i);
            if (selectedDay == slot.get("selectedDay")) {
                int start = slot.get("start");
                int end = slot.get("end");

                if (value == start) return 1;
                if (value > start && value < end) return 2;
                if (value == end) return 3;
            }
        }

        return 0;

    }

}
