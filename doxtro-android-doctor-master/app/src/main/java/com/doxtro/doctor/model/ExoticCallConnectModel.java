package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 07/12/17.
 */

public class ExoticCallConnectModel {

    @SerializedName("Call")
    private Call call;

    public Call getCall() {
        return call;
    }

    public void setCall(Call call) {
        this.call = call;
    }


    private class Call {
        @SerializedName("Sid")
        private String sid;

        @SerializedName("ParentCallSid")
        private Object parentCallSid;

        @SerializedName("DateCreated")
        private String dateCreated;

        @SerializedName("DateUpdated")
        private String dateUpdated;

        @SerializedName("AccountSid")
        private String accountSid;
        @SerializedName("To")

        private String to;
        @SerializedName("From")

        private String from;
        @SerializedName("PhoneNumberSid")

        private String phoneNumberSid;
        @SerializedName("Status")

        private String status;
        @SerializedName("StartTime")

        private String startTime;
        @SerializedName("EndTime")

        private Object endTime;
        @SerializedName("Duration")

        private Object duration;
        @SerializedName("Price")

        private Object price;
        @SerializedName("Direction")

        private String direction;
        @SerializedName("AnsweredBy")

        private Object answeredBy;
        @SerializedName("ForwardedFrom")

        private Object forwardedFrom;
        @SerializedName("CallerName")

        private Object callerName;
        @SerializedName("Uri")

        private String uri;
        @SerializedName("RecordingUrl")
        private Object recordingUrl;

        public String getSid() {
            return sid;
        }

        public void setSid(String sid) {
            this.sid = sid;
        }

        public Object getParentCallSid() {
            return parentCallSid;
        }

        public void setParentCallSid(Object parentCallSid) {
            this.parentCallSid = parentCallSid;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getDateUpdated() {
            return dateUpdated;
        }

        public void setDateUpdated(String dateUpdated) {
            this.dateUpdated = dateUpdated;
        }

        public String getAccountSid() {
            return accountSid;
        }

        public void setAccountSid(String accountSid) {
            this.accountSid = accountSid;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getPhoneNumberSid() {
            return phoneNumberSid;
        }

        public void setPhoneNumberSid(String phoneNumberSid) {
            this.phoneNumberSid = phoneNumberSid;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public Object getEndTime() {
            return endTime;
        }

        public void setEndTime(Object endTime) {
            this.endTime = endTime;
        }

        public Object getDuration() {
            return duration;
        }

        public void setDuration(Object duration) {
            this.duration = duration;
        }

        public Object getPrice() {
            return price;
        }

        public void setPrice(Object price) {
            this.price = price;
        }

        public String getDirection() {
            return direction;
        }

        public void setDirection(String direction) {
            this.direction = direction;
        }

        public Object getAnsweredBy() {
            return answeredBy;
        }

        public void setAnsweredBy(Object answeredBy) {
            this.answeredBy = answeredBy;
        }

        public Object getForwardedFrom() {
            return forwardedFrom;
        }

        public void setForwardedFrom(Object forwardedFrom) {
            this.forwardedFrom = forwardedFrom;
        }

        public Object getCallerName() {
            return callerName;
        }

        public void setCallerName(Object callerName) {
            this.callerName = callerName;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public Object getRecordingUrl() {
            return recordingUrl;
        }

        public void setRecordingUrl(Object recordingUrl) {
            this.recordingUrl = recordingUrl;
        }

    }
}
