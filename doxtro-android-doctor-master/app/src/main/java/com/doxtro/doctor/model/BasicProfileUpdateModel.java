package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 07/08/17.
 */

// model class is serializable since i have to pass the entire model from one class to another

public class BasicProfileUpdateModel implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("emailid")
    private String emailid;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("bio")
    private String bio;

    @SerializedName("experience")
    private String experience;

    @SerializedName("gender")
    private String gender;

    @SerializedName("duns")
    private String duns = "";

    @SerializedName("userId")
    private String userId;

    @SerializedName("bankDetails")
    private List<BankDetailModel> bankDetails = null;

    @SerializedName("documents")
    private ArrayList<DocumentsModel> documents = new ArrayList<>();

    @SerializedName("accountActivationTime")
    private String accountActivationTime;

    @SerializedName("availabilityStatus")
    private String availabilityStatus;

    @SerializedName("status")
    private String status;

    @SerializedName("accountStatus")
    private String accountStatus;

    @SerializedName("language")
    private List<String> language = new ArrayList<>();

    @SerializedName("WorksFor")
    private List<ProfessionModel> worksFor = new ArrayList<>();

    @SerializedName("qualification")
    private List<QualificationModel> qualification = new ArrayList<>();

    @SerializedName("category")
    private List<String> category = new ArrayList<>();

    @SerializedName("specialization")
    private List<String> specialization = new ArrayList<>();

    @SerializedName("tag")
    private String tag;

    @SerializedName("profilePic")
    private String profilePic;

    @SerializedName("availability")
    private List<OperationalHourModel> availability = null;


    public List<OperationalHourModel> getAvailability() {
        return availability;
    }

    public void setAvailability(List<OperationalHourModel> availability) {
        this.availability = availability;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDuns() {
        return duns;
    }

    public void setDuns(String duns) {
        this.duns = duns;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<BankDetailModel> getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(List<BankDetailModel> bankDetails) {
        this.bankDetails = bankDetails;
    }

    public ArrayList<DocumentsModel> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<DocumentsModel> documents) {
        this.documents = documents;
    }

    public String getAccountActivationTime() {
        return accountActivationTime;
    }

    public void setAccountActivationTime(String accountActivationTime) {
        this.accountActivationTime = accountActivationTime;
    }

    public String getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(String availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public List<ProfessionModel> getWorksFor() {
        return worksFor;
    }

    public void setWorksFor(List<ProfessionModel> worksFor) {
        this.worksFor = worksFor;
    }

    public List<QualificationModel> getQualification() {
        return qualification;
    }

    public void setQualification(List<QualificationModel> qualification) {
        this.qualification = qualification;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public List<String> getSpecialization() {
        return specialization;
    }

    public void setSpecialization(List<String> specialization) {
        this.specialization = specialization;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

}
