package com.doxtro.doctor.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.DialogUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 10/08/17.
 */

public class LanguageDialogFragment extends DialogFragment {
    @BindView(R.id.language_listview)
    ListView languageListView;
    @BindView(R.id.language_save)
    Button saveBtn;

    private List<String> languageList = new ArrayList<>();
    private LanguageAdapter languageAdapter;
    private ArrayList<String> languageSelected = new ArrayList<>();


    public LanguageDialogFragment() {
        //empty constructor
    }

    public static LanguageDialogFragment newInstance() {
        Bundle args = new Bundle();
        LanguageDialogFragment fragment = new LanguageDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_languages, container, false);
        ButterKnife.bind(this, view);
        getLanguageList();
        languageAdapter = new LanguageAdapter(languageList);
        saveBtn.setOnClickListener(v -> sendSelectedLanguage());
        return view;
    }

    private void sendSelectedLanguage() {
        if (languageSelected != null) {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("language_selected", languageSelected);
            Intent intent = getActivity().getIntent();
            intent.putExtra("languages", bundle);
            getTargetFragment().onActivityResult(getTargetRequestCode(), 5, intent);
        }
        dismiss();
    }

    private void getLanguageList() {
        DialogUtils.showProgressDialog(getActivity(), "Please wait a while..");
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<JsonObject> call = apiService.getLanguages();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.isSuccessful()) {
                        JsonArray jsonArray = response.body().getAsJsonArray("data");
                        for (int i = 0; i < jsonArray.size(); i++) {
                            languageList.add(i, jsonArray.get(i).getAsString());
                            Log.i("item", jsonArray.get(i).toString());
                        }
                        if (languageList != null && languageList.size() > 0) {
                            languageListView.setAdapter(languageAdapter);
                        }
                        saveBtn.setVisibility(View.VISIBLE);
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return dialog;
    }

    private class LanguageAdapter extends BaseAdapter {
        List<String> list = new ArrayList<>();

        public LanguageAdapter(List<String> languageList) {
            list = languageList;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint({"InflateParams", "ViewHolder"})
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.languages_item, null);
            final TextView textView = (TextView) convertView.findViewById(R.id.language_name);
            CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.language_checkbox);
            textView.setText(list.get(position));
            checkBox.setOnClickListener(v -> {
                if (((CheckBox) v).isChecked()) {
                    if (!languageSelected.contains(textView.getText().toString())) {
                        languageSelected.add(textView.getText().toString());
                    }
                }else {
                    if (languageSelected.contains(textView.getText().toString())) {
                        languageSelected.remove(textView.getText().toString());
                    }
                }
            });
            return convertView;
        }
    }


}
