package com.doxtro.doctor.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.LoginResponse;
import com.doxtro.doctor.model.OTPVerifyResponse;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.EventsUtility;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OTPActivity extends AppCompatActivity {

    @BindView(R.id.layout_enter_otp)
    LinearLayout layoutEnterOtp;

    @BindView(R.id.et_otp)
    EditText mEdtOtp;
    @BindView(R.id.layout_resend_otp)
    LinearLayout mLayoutResendOtp;
    @BindView(R.id.btn_submit_otp)
    Button mBtnSubmitOtp;
    @BindView(R.id.tv_otp_timer)
    TextView mTvTimer;
    @BindView(R.id.tv_resend)
    TextView mTvResend;


    private static final String TAG = OTPActivity.class.getSimpleName();
    private String gender = "Male";
    private String text;
    private boolean delete = false;
    private static final int MY_PERMISSIONS_REQUEST_READ_SMS = 0;

    private String mobileNumber;
    private CountDownTimer countDown;
    private String source = "";

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Log.e(TAG, "Received OTP : " + message);
                mEdtOtp.setText(message);
                stopTimer();
                onClickSubmitOtp();
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.activity_otp, null);
        setContentView(view);
        ButterKnife.bind(this);

        showPermissionDialog();


        startTimer();
        if (getIntent() != null) {
            mobileNumber = getIntent().getStringExtra(Constants.MOBILE);
            source = getIntent().getStringExtra(Constants.SOURCE);
        }


        mEdtOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                text = mEdtOtp.getText().toString();
                if (count > after)
                    delete = true;

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                StringBuilder sb = new StringBuilder(s.toString());
                int replacePosition = mEdtOtp.getSelectionEnd();

                if (s.length() != 6) { //where 6 is the character underline per text
                    if (!delete) {
                        if (replacePosition < s.length())
                            sb.deleteCharAt(replacePosition);
                    } else {
                        sb.insert(replacePosition, '_');
                    }

                    if (replacePosition < s.length() || delete) {
                        mEdtOtp.setText(sb.toString());
                        mEdtOtp.setSelection(replacePosition);
                    } else {
                        mEdtOtp.setText(text);
                        mEdtOtp.setSelection(replacePosition - 1);
                    }
                }
                delete = false;


            }

            @Override
            public void afterTextChanged(Editable s) {

            }


        });
    }


    private void showPermissionDialog() {

        int permissionCheck = ContextCompat.checkSelfPermission(OTPActivity.this,
                android.Manifest.permission.READ_SMS);
        if (permissionCheck
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(OTPActivity.this,
                    Manifest.permission.READ_SMS)) {


            } else {


                ActivityCompat.requestPermissions(OTPActivity.this,
                        new String[]{Manifest.permission.READ_SMS},
                        MY_PERMISSIONS_REQUEST_READ_SMS);

            }
        }
    }


    @OnClick(R.id.layout_resend_otp)
    public void onClickResendOtp() {

        if (NetworkUtil.getConnectionStatus(this) != 0)
            resendOTPApiCall();
        else
            DialogUtils.appToast(this, getString(R.string.no_internet_connection));

        //  DialogUtils.appToast(this, "Otp sent to your registered mobile number");
    }

    @OnClick(R.id.btn_submit_otp)
    public void onClickSubmitOtp() {
        String otp = mEdtOtp.getText().toString();
        if (NetworkUtil.getConnectionStatus(OTPActivity.this) != 0) {
            submitOTPApiCall(otp);
        } else {

            DialogUtils.appToast(OTPActivity.this, getString(R.string.no_internet_connection));
        }

    }

    private void submitOTPApiCall(String otp) {
        DialogUtils.showProgressDialog(this, "Verifying OTP...");
        ApiInterface apiService = ApiClient.getClient(OTPActivity.this).create(ApiInterface.class);

        String doctorID = SharePref.getDoctorID(OTPActivity.this);
        Call<OTPVerifyResponse> call = apiService.verifyDoctor(doctorID, otp, Constants.PHONE_TYPE, SharePref.getDeviceId(this));
        call.enqueue(new Callback<OTPVerifyResponse>() {
            @Override
            public void onResponse(Call<OTPVerifyResponse> call, Response<OTPVerifyResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        stopTimer();
                        SharePref.setToken(OTPActivity.this, response.body().getData().getToken());
                        DialogUtils.appToast(OTPActivity.this, response.body().getMessage());
                        EventsUtility.eventTrack(OTPActivity.this, Constants.EVENT_OTP_VERIFIED, null);
                        SharePref.setLoggedIn(OTPActivity.this, true);
                        if (source.equals(Constants.SOURCE_SIGN_UP)) {
                            Intent mainIntent = new Intent(OTPActivity.this, ProfileConfigActivity.class);
                            startActivity(mainIntent);
                            finish();
                        } else {
                            Intent mainIntent = new Intent(OTPActivity.this, HomeActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(mainIntent);
                        }


                    } else {
                        layoutEnterOtp.setVisibility(View.VISIBLE);  // hides enter otp layout
                        DialogUtils.appToast(OTPActivity.this, new JSONObject(response.errorBody().string()).getString("message"));
                    }

                    DialogUtils.dismissProgressDialog();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                DialogUtils.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<OTPVerifyResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }
        });
    }

    private void resendOTPApiCall() {
        DialogUtils.showProgressDialog(OTPActivity.this, "Resending OTP...");
        ApiInterface apiService = ApiClient.getClient(OTPActivity.this).create(ApiInterface.class);
        startTimer();
        Call<LoginResponse> call = apiService.signInDoctor(mobileNumber);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //int statusCode = response.code();
                //String movies = response.body();
                try {
                    if (response.isSuccessful()) {
                        DialogUtils.appToast(OTPActivity.this, response.body().getMessage());
                        Log.e(TAG, "OTP Resend onResponse  : " + response.body().getData().get_id());
                        SharePref.setDoctorID(OTPActivity.this, response.body().getData().get_id());
                    } else {
                        DialogUtils.appToast(OTPActivity.this, new JSONObject(response.errorBody().string()).getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Sms request granted");
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

                } else {
                    Log.i(TAG, "Sms request denied");
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                }
            }

        }
    }

    //resend otp timer
    private void startTimer() {
        mLayoutResendOtp.setClickable(false);
        mLayoutResendOtp.setFocusable(false);
        mTvResend.setTextSize(16f);
        mTvResend.setTypeface(mTvResend.getTypeface(), Typeface.NORMAL);
        mTvResend.setTextColor(Color.parseColor("#808080"));

        countDown = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTvTimer.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                mTvTimer.setText("");
                mTvResend.setTextColor(AndroidVersionUtility.getColor(OTPActivity.this, R.color.black));
                mTvResend.setTypeface(mTvResend.getTypeface(), Typeface.BOLD);
                mTvResend.setTextSize(18f);
                mLayoutResendOtp.setClickable(true);
                mLayoutResendOtp.setFocusable(true);
            }

        };
        countDown.start();
    }

    private void stopTimer() {
        try {
            countDown.cancel();
            mTvTimer.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}



