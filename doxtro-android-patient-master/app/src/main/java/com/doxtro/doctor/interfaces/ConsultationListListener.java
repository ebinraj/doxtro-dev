package com.doxtro.doctor.interfaces;

import com.doxtro.doctor.model.ConsultationsDataModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by azharuddin on 18/9/17.
 */

public interface ConsultationListListener {
    public void getConsultationList(List<ConsultationsDataModel> consultationList, HashMap<String, Object> unreadMsgList);
}
