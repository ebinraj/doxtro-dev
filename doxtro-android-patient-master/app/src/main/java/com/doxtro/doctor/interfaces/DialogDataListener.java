package com.doxtro.doctor.interfaces;

import android.os.Bundle;

/**
 * Created by yashaswi on 30/08/17.
 */

public interface DialogDataListener {

    public void getSummaryData(String summary);

    public void getProfessionalData(Bundle professionData);

    public void getEducationData(Bundle educationData);

    public void getBankDetails(Bundle bankData);
}
