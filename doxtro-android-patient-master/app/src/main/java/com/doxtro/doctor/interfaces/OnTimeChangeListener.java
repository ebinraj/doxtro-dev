package com.doxtro.doctor.interfaces;

/**
 * Created by azharuddin on 11/10/17.
 */

public interface OnTimeChangeListener {
    public void OnTimeChange(String timeStamp);
}
