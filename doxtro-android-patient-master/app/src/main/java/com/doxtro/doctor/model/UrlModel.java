package com.doxtro.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 29/11/17.
 */

public class UrlModel {
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("urlType")
    @Expose
    private String urlType;
    @SerializedName("documentName")
    @Expose
    private String documentName;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

}
