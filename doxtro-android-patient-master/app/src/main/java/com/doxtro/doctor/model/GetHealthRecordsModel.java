package com.doxtro.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ghanshyamnayma on 26/10/17.
 */

public class GetHealthRecordsModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("tag")
        @Expose
        private String tag;
        @SerializedName("data")
        @Expose
        private List<HealthRecordsModel.Data> data = null;

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public List<HealthRecordsModel.Data> getData() {
            return data;
        }

        public void setData(List<HealthRecordsModel.Data> data) {
            this.data = data;
        }



        @Override
        public String toString()
        {
            return "ClassPojo [tag = "+tag+", data = "+data+"]";
        }
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }
}
