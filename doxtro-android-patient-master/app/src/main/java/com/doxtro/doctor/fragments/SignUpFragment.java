package com.doxtro.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.OTPActivity;
import com.doxtro.doctor.activities.WebViewActivity;
import com.doxtro.doctor.model.LoginResponse;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.EventsUtility;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.doxtro.doctor.utils.Validation.isValidEmail;

/**
 * Created by ghanshyamnayma on 12/07/17.
 */

public class SignUpFragment extends Fragment {

    @BindView(R.id.et_name_sign_up)
    EditText mEdtName;

    @BindView(R.id.et_email_sign_up)
    EditText mEdtEmail;

    @BindView(R.id.et_mobile_sign_up)
    EditText mEdtMobile;

    @BindView(R.id.terms_link_tv)
    TextView termsLinkTV;


    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ButterKnife.bind(this, view);
        String customColorText = getResources().getString(R.string.terms_conditions);
        termsLinkTV.setText(Html.fromHtml(customColorText));
        return view;
    }

    @OnClick(R.id.btn_submit_sign_up)
    public void onClickSubmit() {
        String name = mEdtName.getText().toString();
        String email = mEdtEmail.getText().toString();
        String mobile = mEdtMobile.getText().toString();

        if (name.isEmpty() && name.length() == 0) {
            mEdtName.setError(getString(R.string.err_enter_name));
        }

        if (email.length() == 0 || email.isEmpty()) {
            mEdtEmail.setError(getString(R.string.err_enter_email));
        } else if (!isValidEmail(email)) {
            mEdtEmail.setError(getString(R.string.err_invalid_email));
        }

        if (mobile.isEmpty()) {
            mEdtMobile.setError(getString(R.string.err_enter_mobile));
        } else if (mobile.length() < 10 && !mobile.isEmpty()){
            mEdtMobile.setError(getString(R.string.err_enter_correct_mobile));
        }else if (!mobile.isEmpty() && mobile.length() == 10 && !email.isEmpty() && isValidEmail(email) && !name.isEmpty()) {
            //checking internet availability
            if (NetworkUtil.getConnectionStatus(getActivity()) != 0)
                newDoctorSignUp(name, email, mobile);
            else
                DialogUtils.appToast(getActivity(),getString(R.string.no_internet_connection));
        }

    }

    @OnClick(R.id.terms_link_tv)
    public void onTermsLinkClicked()
    {
        String webUrl="http://doxtro.com/terms-and-privacy";
        Intent viewIntent = new Intent(getContext(), WebViewActivity.class);
        viewIntent.putExtra(Constants.URL,webUrl);
        startActivity(viewIntent);
    }

    private void newDoctorSignUp(String name, String email, String mobile) {
        DialogUtils.showProgressDialog(getActivity(), "Please wait a while..");
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);

        Call<LoginResponse> call = apiService.signUpDoctor(name, email, mobile);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                try {

                    if (response.isSuccessful()) {
                        String name = mEdtName.getText().toString();
                        String email = mEdtEmail.getText().toString();
                        String mobile = mEdtMobile.getText().toString();

                        Map<String, Object> eventValue = new HashMap<>();
                        eventValue.put("Name",name);
                        eventValue.put("Mobile",mobile);
                        eventValue.put("Email",email);
                        EventsUtility.eventTrack(getContext(),Constants.EVENT_SIGNUP_COMPLETED,eventValue);
                        SharePref.setDoctorID(getContext(), response.body().getData().get_id());
                        Intent intent = new Intent(getActivity(), OTPActivity.class);
                        intent.putExtra(Constants.SOURCE,Constants.SOURCE_SIGN_UP);
                        intent.putExtra(Constants.MOBILE, "" + mobile);
                        startActivity(intent);

                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

}
