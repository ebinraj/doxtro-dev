package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 03/10/17.
 */

public class DiagnosticModel {
    @SerializedName("name")
    private String name="";

    @SerializedName("instruction")
    private String instruction="";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}
