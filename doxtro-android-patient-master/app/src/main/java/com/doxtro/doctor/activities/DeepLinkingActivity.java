package com.doxtro.doctor.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.doxtro.doctor.R;
import com.doxtro.doctor.utils.Constants;

import java.util.Map;

/**
 * Created by yashaswi on 14/11/17.
 */

public class DeepLinkingActivity extends AppCompatActivity implements AppsFlyerConversionListener {
    public static DeepLinkingActivity deepLinkingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deep_linking);
        AppsFlyerLib.getInstance().registerConversionListener(this, this);
    }

    private void handleDeeplinks(Map<String, String> conversionData) {
        Intent intent;
        Context context = getApplicationContext();
            if (conversionData.containsKey("host")) {
                String host = conversionData.get("host");
                if (host.equalsIgnoreCase(Constants.LINK_DOCUMENTS)
                        || host.equalsIgnoreCase(Constants.LINK_AVAILABILITY)
                        || host.equalsIgnoreCase(Constants.LINK_BANKDETAILS)) {
                    intent = new Intent(context, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(Constants.TAB_SWITCH, 2);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                    finish();
                } else {
                    intent = new Intent(context, HomeActivity.class);
                    intent.setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK |
                                    Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(intent);
                }
            }
    }

    @Override
    public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
        for (String attrName : conversionData.keySet()) {
            Log.d(AppsFlyerLib.LOG_TAG, "attribute: " + attrName + " = " +
                    conversionData.get(attrName));
        }
    }

    @Override
    public void onInstallConversionFailure(String errorMessage) {
        Log.d("AppsFlyerError", "error getting conversion data: " + errorMessage);
    }

    @Override
    public void onAppOpenAttribution(Map<String, String> conversionData) {
        handleDeeplinks(conversionData);
    }

    @Override
    public void onAttributionFailure(String errorMessage) {
        Log.d("AppsFlyerError", "error getting conversion data: " + errorMessage);
    }
}
