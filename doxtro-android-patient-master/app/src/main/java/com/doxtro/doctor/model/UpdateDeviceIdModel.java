package com.doxtro.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ghanshyamnayma on 05/09/17.
 */

public class UpdateDeviceIdModel {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("deviceId")
        @Expose
        private String deviceId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        @Override
        public String toString() {
            return "ClassPojo [_id = " + id + ", deviceId = " + deviceId + "]";
        }

    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", data = " + data + "]";
    }

}
