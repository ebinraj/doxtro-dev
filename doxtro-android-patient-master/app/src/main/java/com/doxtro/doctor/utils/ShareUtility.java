package com.doxtro.doctor.utils;

/**
 * @author ghanshyamnayma on 13/06/17.
 * @file TODO: Add a class header comment!
 * @copyright ABOVE Solutions India Pvt. Ltd.All Rights Reserved.
 */

public class ShareUtility {
    public static String token = "";

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        ShareUtility.token = token;
    }
}
