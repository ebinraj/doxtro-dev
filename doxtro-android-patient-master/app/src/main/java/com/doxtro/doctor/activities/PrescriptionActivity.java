package com.doxtro.doctor.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.AutoSearchAdapter;
import com.doxtro.doctor.adapters.AutoSuggestionAdapter;
import com.doxtro.doctor.adapters.PrescriptionAdapter;
import com.doxtro.doctor.model.MedicationModel;
import com.doxtro.doctor.model.PatientDetailModel;
import com.doxtro.doctor.model.PrescriptionPostModel;
import com.doxtro.doctor.model.PrescriptionResponseModel;
import com.doxtro.doctor.model.SearchModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.Reusable;
import com.doxtro.doctor.utils.SharePref;
import com.doxtro.doctor.utils.SoftInput;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class PrescriptionActivity extends AppCompatActivity {

    @BindView(R.id.prescription_add)
    Button addBtn;
    @BindView(R.id.prescriptionET)
    EditText presET;
    @BindView(R.id.prescription_send_btn)
    Button sendBtn;
    @BindView(R.id.prescription_recycler_view)
    RecyclerView prescriptionRV;
    @BindView(R.id.prescription_view_btn)
    Button viewSaveBtn;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.common_base_toolbarT)
    Toolbar toolbar;
    @BindView(R.id.prescription_auto_serachRV)
    RecyclerView searchRV;
    @BindView(R.id.prescription_suggestionGV)
    GridView autoSuggestionGV;
    @BindView(R.id.prescription_auto_serachLL)
    RelativeLayout autoSearchLL;
    @BindView(R.id.prescription_search_nodataTV)
    TextView noDataTV;

    private RecyclerView.LayoutManager layoutManager;
    private static List<MedicationModel> prescriptionlist = new ArrayList<>();
    private PrescriptionAdapter prescriptionAdapter;
    private static String consultationId = "";
    private String patientData = "";
    private String timeStamp = "";
    private String source = "";
    private PatientDetailModel patientDetailModel = new PatientDetailModel();
    public static PrescriptionActivity prescriptionActivity;
    private final List<String> autoSuggestList = new ArrayList<>();
    private AutoSuggestionAdapter autoSuggestionAdapter;
    private static List<String> searchResultList = new ArrayList<>();
    //  private MentionController mentionController;
    //  private List<MedicationModel> medications=new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.activity_prescription, null);
        super.onCreate(savedInstanceState);
        setContentView(view);
        ButterKnife.bind(this);
        prescriptionActivity = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTitle.setText(getString(R.string.prescriptionTxt));
        toolbar.setNavigationOnClickListener(v -> finish());
        consultationId = getIntent().getStringExtra(Constants.CONSULTATION_ID);
        patientData = getIntent().getStringExtra(Constants.PATIENT_DEATAIL);
        timeStamp = getIntent().getStringExtra(Constants.TIME_STAMP);
        source = getIntent().getStringExtra(Constants.SOURCE);
        presET.addTextChangedListener(searchTextWatcher);

        //   prescriptionlist = (List<MedicationModel>) getIntent().getSerializableExtra(Constants.PRESCRIPTION);
        initViews();
        getMedicineSuggestions();

    }

    private final TextWatcher searchTextWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.i("Text", s.toString());
            if (!s.toString().equals("")) {
                callSearchApi(s.toString());
            } else {
                autoSearchLL.setVisibility(View.GONE);
            }
//            new MentionPeopleAPICall().getMentionPeopleListVolley(mContext, s.toString(), null, mReloadListlistner);
        }

        public void afterTextChanged(Editable s) {
        }
    };

    private void callSearchApi(String text) {
        //  DialogUtils.showProgressDialog(PrescriptionActivity.this, getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(PrescriptionActivity.this).create(ApiInterface.class);
        Call<SearchModel> call = apiService.getSearchResults(text, Constants.PRESCRIPTION);
        call.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                try {
                    if (response.isSuccessful()) {

                        searchResultList = response.body().getData();
                        if (searchResultList.size() > 0) {
                            noDataTV.setVisibility(View.GONE);
                            searchRV.setVisibility(View.VISIBLE);
                            initSearchRV();
                        }else {
                            noDataTV.setVisibility(View.VISIBLE);
                            noDataTV.setText("No suggestions found!");
                            searchRV.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    private void initSearchRV() {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        AutoSearchAdapter autoSearchAdapter = new AutoSearchAdapter(PrescriptionActivity.this, searchResultList,presET);
        autoSearchLL.setVisibility(View.VISIBLE);
        searchRV.setLayoutManager(layoutManager);
        searchRV.setAdapter(autoSearchAdapter);

    }

    private void getMedicineSuggestions() {
        DialogUtils.showProgressDialog(PrescriptionActivity.this, getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(PrescriptionActivity.this).create(ApiInterface.class);
        Call<JsonObject> call = apiService.getAutoSuggestions(SharePref.getDoctorID(this), Constants.PRESCRIPTION);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.isSuccessful()) {
                        JsonArray jsonArray = response.body().getAsJsonArray("data");
                        for (int i = 0; i < jsonArray.size(); i++) {
                            autoSuggestList.add(i, jsonArray.get(i).getAsString());
                            Log.i("item", jsonArray.get(i).toString());
                        }
                        if (autoSuggestList != null && autoSuggestList.size() > 0) {
                            autoSuggestionAdapter = new AutoSuggestionAdapter(PrescriptionActivity.this, autoSuggestList, presET);
                            autoSuggestionGV.setAdapter(autoSuggestionAdapter);
                        } else {
                            autoSuggestionGV.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }


    private void initViews() {

        //   mentionController = new MentionController(, this, mMentionListView, mSaySometingET, android.R.id.text1, R.layout.mention_people_list_item);

        if (prescriptionlist.size() == 0) {
            prescriptionRV.removeAllViews();
            prescriptionlist.clear();
            sendBtn.setText("Send prescriptionlist");
            viewSaveBtn.setVisibility(View.VISIBLE);
            sendBtn.setOnClickListener(v -> {
                if (prescriptionlist != null && !prescriptionlist.isEmpty()) {
                    sendPrescriptionData(prescriptionlist, Constants.SEND);
                }
            });
            viewSaveBtn.setOnClickListener(v -> {
                if (prescriptionlist != null && !prescriptionlist.isEmpty()) {
                    sendPrescriptionData(prescriptionlist, Constants.PRESCRIPTION_VIEW);
                }
            });
        } else {
            sendBtn.setText("Update and Send");
            sendBtn.setBackground(AndroidVersionUtility.getDrawable(this, R.drawable.round_bg_app_color));
            sendBtn.setTextColor(AndroidVersionUtility.getColor(this, R.color.white));
            viewSaveBtn.setVisibility(View.GONE);
            if (prescriptionlist != null) {
                initRV(prescriptionlist);
            }
            sendBtn.setOnClickListener(v -> {
                if (prescriptionlist != null && !prescriptionlist.isEmpty()) {
                    sendPrescriptionData(prescriptionlist, Constants.EDIT_PRESCRIPTION);
                }
            });
        }
    }


    private void sendPrescriptionData(List<MedicationModel> prescriptionlist, String source) {
        PrescriptionPostModel prescriptionPostModel = new PrescriptionPostModel();
        prescriptionPostModel.setConsultationId(consultationId);
        prescriptionPostModel.setType(Constants.PRESCRIPTION);
        prescriptionPostModel.setMedication(prescriptionlist);
        DialogUtils.showProgressDialog(this, getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Call<PrescriptionResponseModel> call = apiService.sendPrescription(prescriptionPostModel);
        call.enqueue(new Callback<PrescriptionResponseModel>() {
            @Override
            public void onResponse(Call<PrescriptionResponseModel> call, Response<PrescriptionResponseModel> response) {
                try {
                    if (response.isSuccessful()) {
                        String prescriptionId = response.body().getData().getId();
                        patientDetailModel = response.body().getAdditionalDetails();
                        if (source.contains(Constants.SEND)) {
                            Log.i("response", response.body().getData().toString());
                            sendToChatWindow(prescriptionPostModel, prescriptionId, patientDetailModel, Constants.SEND);
                        } else if (source.contains(Constants.PRESCRIPTION_VIEW)) {
                            viewAndSavePrescriptions(prescriptionId, patientDetailModel);
                        } else if (source.contains(Constants.EDIT_PRESCRIPTION)) {
                            sendToChatWindow(prescriptionPostModel, prescriptionId, patientDetailModel, Constants.EDIT_PRESCRIPTION);
                        }
                    } else {
                        DialogUtils.appToast(PrescriptionActivity.this, new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<PrescriptionResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    private void viewAndSavePrescriptions(String prescriptionId, PatientDetailModel patientsData) {
//        String specializations="";
       String docName = SharePref.getDoctorDetail(PrescriptionActivity.this).getData().getFirstName();
        List<String> qualifications = getEducationList();
        String docDegree = Reusable.arrayListToString(qualifications, ",");
        String duns = ((SharePref.getDoctorDetail(PrescriptionActivity.this).getData().getDuns() != null) ? SharePref.getDoctorDetail(PrescriptionActivity.this).getData().getDuns() : "");
//        if(SharePref.getDoctorDetail(this).getData().getCategory()!=null)
//        {
//            specializations=Reusable.arrayListToString(SharePref.getDoctorDetail(this).getData().getCategory(),",");
//        }
        Intent intent = new Intent(PrescriptionActivity.this, ViewPrescriptionACtivity.class);
        Bundle bundle = new Bundle();
        if (consultationId == null && prescriptionId == null)
            return;
        bundle.putString(Constants.SOURCE, Constants.PRESCRIPTION);
        bundle.putString(Constants.CONSULTATION_ID, consultationId);
        bundle.putString(Constants.PRESCRIPTION_ID, prescriptionId);
        bundle.putString(Constants.DOCTOR_NAME, docName);
        bundle.putString(Constants.DOCTOR_DEGREE, docDegree);
        bundle.putString(Constants.DOCTOR_REGNO, duns);
//        bundle.putString(Constants.PATIENT_DEATAIL, patientInfo);
        bundle.putString(Constants.TIME_STAMP, timeStamp);
        bundle.putString(Constants.PATIENT_NAME, patientsData.getName());
        bundle.putString(Constants.PATIENT_AGE, patientsData.getAge());
        bundle.putString(Constants.PATIENT_GENDER, patientsData.getGender());
       // bundle.putString(Constants.SPECIALIZATIONS,specializations);
        intent.putExtra(Constants.PRESCRIPTION_DATA, bundle);

        this.startActivity(intent);
    }

    private List<String> getEducationList() {
        List<String> qualifications = new ArrayList<>();
        if (SharePref.getDoctorDetail(this).getData().getQualification() != null && !SharePref.getDoctorDetail(this).getData().getQualification().isEmpty())
            for (int i = 0; i < SharePref.getDoctorDetail(this).getData().getQualification().size(); i++) {
                List<String> degrees = new ArrayList<>();
                if (!degrees.contains(SharePref.getDoctorDetail(this).getData().getQualification().get(i).getSpecialization())) {
                    degrees.add(SharePref.getDoctorDetail(this).getData().getQualification().get(i).getSpecialization());
                }
                for (int n = 0; n < degrees.size(); n++) {
                    qualifications.add(degrees.get(n));
                }
            }
        return qualifications;
    }

    private void sendToChatWindow(PrescriptionPostModel prescriptionPostModel, String prescriptionId, PatientDetailModel patientDetailModel, String source) {
        List<String> qualifications = getEducationList();
        ChatActivity.sendPrescriptionData(prescriptionPostModel, prescriptionId, qualifications, patientDetailModel, source);
        finish();
        ViewPrescriptionACtivity.viewPrescriptionACtivity.finish();
        ViewPrescriptionACtivity.viewPrescriptionACtivity = null;
        //PrescriptionActivity.this.finish();
        // messagesReference.push().updateChildren(newMessage);
    }

    private void initRV(List<MedicationModel> prescriptions) {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        prescriptionRV.setLayoutManager(layoutManager);
        if (prescriptionAdapter == null) {
            prescriptionAdapter = new PrescriptionAdapter(this, Constants.PRESCRIPTION, null, prescriptions, sendBtn, viewSaveBtn);
            prescriptionRV.setAdapter(prescriptionAdapter);
        } else {
            if (prescriptions != null && prescriptions.isEmpty()) {
                prescriptionAdapter.notifyItemInserted(prescriptions.size() - 1);
                //  prescriptionAdapter.notifyDataSetChanged();

            } else {
                prescriptionAdapter.notifyDataSetChanged();
            }
        }


    }

    @OnClick(R.id.prescription_add)
    public void addPrescriptionClick() {
        SoftInput.closeSoftInput(this);
        if (presET != null && !presET.getText().toString().equals("")) {
            MedicationModel medicationModel = new MedicationModel();
            medicationModel.setName(presET.getText().toString());
            prescriptionlist.add(medicationModel);
            //reversing the list
            Collections.reverse(prescriptionlist);
            initRV(prescriptionlist);
            presET.setText("");
            sendBtn.setBackground(AndroidVersionUtility.getDrawable(this, R.drawable.round_bg_app_color));
            sendBtn.setTextColor(AndroidVersionUtility.getColor(this, R.color.white));
            sendBtn.setClickable(true);
            viewSaveBtn.setBackground(AndroidVersionUtility.getDrawable(this, R.drawable.round_bg_app_color));
            viewSaveBtn.setTextColor(AndroidVersionUtility.getColor(this, R.color.white));
            viewSaveBtn.setClickable(true);

        }
    }


    public static void getMedicationsData(List<MedicationModel> medicationModelList) {
        if (medicationModelList != null) {
            prescriptionlist = new ArrayList<>();
            prescriptionlist = medicationModelList;
        }
    }


}
