package com.doxtro.doctor.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.interfaces.DialogDataListener;
import com.doxtro.doctor.model.ProfessionModel;
import com.doxtro.doctor.model.QualificationModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.Validation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 25/07/17.
 */

public class EditInfoDialogFragment extends DialogFragment implements View.OnClickListener {


    @BindView(R.id.dialog_title)
    TextView title;
    @BindView(R.id.profile_dialog_ET1)
    EditText editText1;
    @BindView(R.id.profile_dialog_ET2)
    EditText editText2;
    @BindView(R.id.profile_dialog_ET3)
    EditText editText3;
    @BindView(R.id.profile_dialog_save)
    Button saveBtn;
    @BindView(R.id.profile_dialog_spinner1)
    Spinner spinner1;
    @BindView(R.id.dialog_spinner2)
    Spinner spinner2;
    @BindView(R.id.dialog_year_layout)
    LinearLayout yearMainLL;
    @BindView(R.id.dialog_year_toET)
    EditText yearToET;
    @BindView(R.id.dialog_year_fromET)
    EditText yearFromET;


    private int position = 0;
    private String source = "";
    private boolean toEdit = false;
    private static List<ProfessionModel> professionList = new ArrayList<>();
    private static List<QualificationModel> educationList = new ArrayList<>();
    private static DialogDataListener dialogListener;
    private static List<String> degreeList;
    private String summaryData;
    private String typeSpinnerValue = "";
    private String degreeValue = "";


    public EditInfoDialogFragment() {
        //empty constructor
    }

    //instance of the fragment with 3 param
    public static EditInfoDialogFragment newInstance(String source, int pos, boolean isEdit, List<ProfessionModel> professions, List<QualificationModel> educations, List<String> degrees, DialogDataListener dialogDataListener) {
        Bundle args = new Bundle();
        EditInfoDialogFragment fragment = new EditInfoDialogFragment();
        args.putString(Constants.SOURCE, source);
        args.putInt(Constants.POSITION, pos);
        args.putBoolean(Constants.ISEDIT, isEdit);
        if (professions != null) {
            professionList = professions;
        }
        if (degrees != null) {
            degreeList = degrees;
        }
        if (educations != null) {
            educationList = educations;
        }
        fragment.setArguments(args);
        if (dialogDataListener != null) {
            dialogListener = dialogDataListener;
        }
        return fragment;
    }

    //instance of the fragment with 1 param
    public static EditInfoDialogFragment newInstance(String source, DialogDataListener dialogDataListener, List<String> degrees) {
        Bundle args = new Bundle();
        EditInfoDialogFragment fragment = new EditInfoDialogFragment();
        args.putString(Constants.SOURCE, source);
        fragment.setArguments(args);
        if (dialogDataListener != null) {
            dialogListener = dialogDataListener;
        }
        if (degrees != null) {
            degreeList = degrees;
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.source = getArguments().getString(Constants.SOURCE);
            this.position = getArguments().getInt(Constants.POSITION);
            this.toEdit = getArguments().getBoolean(Constants.ISEDIT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_profile_edit, container, false);
        ButterKnife.bind(this, view);
        initViews();
        saveBtn.setOnClickListener(this);
        return view;
    }

    //initialize the views
    private void initViews() {
        switch (source) {
            case Constants.PROFESSION:
                title.setText(getString(R.string.professionaltxt));
                editText1.setVisibility(View.VISIBLE);

                editText1.setInputType(InputType.TYPE_CLASS_TEXT);

                editText2.setVisibility(View.VISIBLE);
                editText2.setFilters(new InputFilter[]{new Validation.DecimalDigitsInputFilter(3, 2)});
                if (toEdit) {
                    //qualificationModels = SharePref.getDoctorDetail(getContext()).getData().getProfile().getQualification();
                    editText1.setText(professionList.get(this.position).getOrganization());
                    editText2.setText(professionList.get(position).getExperience());

                } else {
                    editText1.setHint(getString(R.string.workingatTxt));
                    editText2.setHint(getString(R.string.exptxt));
                }
                break;
            case Constants.SUMMARY:
                title.setText(getString(R.string.summaryText));
                editText1.setVisibility(View.VISIBLE);
                editText1.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.border_box));
                editText1.setText(summaryData);
                editText1.setHint("Enter your bio!");
                break;
            case Constants.EDUCATION:
                title.setText(getString(R.string.educationtext));
                spinner1.setVisibility(View.VISIBLE);
                initSpinners();
                editText1.setVisibility(View.VISIBLE);
                editText1.setInputType(InputType.TYPE_CLASS_TEXT);
                editText1.setLines(1);
                spinner2.setVisibility(View.VISIBLE);
                yearMainLL.setVisibility(View.VISIBLE);
                if (toEdit && educationList.get(position).getInstitute() != null) {
                    //qualificationModels = SharePref.getDoctorDetail(getContext()).getData().getProfile().getQualification();x
                    editText1.setText(educationList.get(this.position).getInstitute());
                    if (educationList.get(position).getAdditionalData() != null && !educationList.get(position).getAdditionalData().equals("")) {
                        editText2.setText(educationList.get(position).getAdditionalData());
                    } else {
                        editText2.setVisibility(View.GONE);
                    }
                    yearToET.setText(educationList.get(position).getYearTo());
                    yearFromET.setText(educationList.get(position).getYearFrom());

                } else {
                    editText1.setHint(getString(R.string.studiedAt));
                    // editText2.setHint(getString(R.string.exptxt));
                }
                break;


        }
    }


    //initialize the spinners
    private void initSpinners() {
        String[] educationTypes = {"UG", "PG"};
        List<String> educationDegrees = degreeList;
        ArrayAdapter myAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, educationTypes);
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(myAdapter);
        ArrayAdapter myAdapter1 = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, educationDegrees);
        myAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(myAdapter1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeSpinnerValue = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                degreeValue = parent.getItemAtPosition(position).toString();
                if (degreeValue.equals("Others")) {
                    editText2.setVisibility(View.VISIBLE);
                    editText2.setHint("Other degree");
                    editText2.setInputType(InputType.TYPE_CLASS_TEXT);
                } else {
                    editText2.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle("Summary");
        // dialog.setContentView(root);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(AndroidVersionUtility.getDrawable(getContext(), R.color.white));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.profile_dialog_ET2:
//                getContext().startActivity(new Intent(getActivity(), SpecializationActivity.class));
//                break;
            case R.id.profile_dialog_save:
                Bundle bundle = new Bundle();
                switch (source) {
                    case Constants.PROFESSION:
                        String organization = editText1.getText().toString();
                        bundle.putString("working_at", organization);
                        bundle.putString("experience", editText2.getText().toString());
                        bundle.putInt("position", position);
                        dialogListener.getProfessionalData(bundle);
                        dismiss();
                        break;
                    case Constants.SUMMARY:
                        if (!editText1.getText().toString().isEmpty()) {
                            dialogListener.getSummaryData(editText1.getText().toString());
                        }
                        dismiss();
                        break;
                    case Constants.EDUCATION:
//                        String college = ((!editText1.getText().toString().equals("")) ? editText1.getText().toString() : "Nil");
                        String college = editText1.getText().toString();
                        String additional_data = editText2.getText().toString();
                        bundle.putString("type", typeSpinnerValue);
                        bundle.putString("organization", college);
                        bundle.putString("degree", degreeValue);
                        bundle.putString("additional_data", additional_data);
                        bundle.putString("yearTo", yearToET.getText().toString());
                        bundle.putString("yearFrom", yearFromET.getText().toString());
                        bundle.putInt("position", position);
                        dialogListener.getEducationData(bundle);
                        dismiss();
                        break;


                }
                break;

        }

    }


    public void sendSummary(String summary) {
        summaryData = summary;
    }
}
