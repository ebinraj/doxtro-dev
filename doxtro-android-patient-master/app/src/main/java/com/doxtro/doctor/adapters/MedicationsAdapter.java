package com.doxtro.doctor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.DiagnosticModel;
import com.doxtro.doctor.model.MedicationModel;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.Reusable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 04/10/17.
 */

public class MedicationsAdapter extends RecyclerView.Adapter<MedicationsAdapter.MyViewHolder> {
    private String source = "";
    private List<DiagnosticModel> diagnostics = new ArrayList<>();
    private List<MedicationModel> medications = new ArrayList<>();
    private final Context mcontext;

    public MedicationsAdapter(Context context, List<MedicationModel> medication, List<DiagnosticModel> diagnostic, String src) {
        mcontext = context;
        source = src;
        if (medication != null)
            medications = medication;
        if (diagnostics != null)
            diagnostics = diagnostic;
    }

    @Override
    public MedicationsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.medications_item_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MedicationsAdapter.MyViewHolder holder, int position) {
        if (source.equals(Constants.PRESCRIPTION)) {
            MedicationModel medicationModel = medications.get(position);
            String name = ((medicationModel.getName() != null) ? medicationModel.getName() : " ");
            String foodWarning = ((medicationModel.getFoodWarning() != null) ? medicationModel.getFoodWarning() : " ");
            // String duration = ((medicationModel.getDuration() != null) ? medicationModel.getDuration() : " ");
            List<String> doseTime = medicationModel.getDoseTime();

           // String morning = "0", noon = "0", night = "0";
             String time = Reusable.arrayListToString(doseTime, " - ");
//            for (int p = 0; p < doseTime.size(); p++) {
//                if (doseTime.get(p).equalsIgnoreCase("Morning")) {
//                    morning = "1";
//                } else if (doseTime.get(p).equalsIgnoreCase("Afternoon")) {
//                    noon = "1";
//                } else if (doseTime.get(p).equalsIgnoreCase("Night")) {
//                    night = "1";
//                }
//                time = morning + "-" + noon + "-" + night;
//            }
            if (!foodWarning.equals("")) {
                holder.foodWarningTV.setVisibility(View.VISIBLE);
                holder.foodWarningTV.setText("Time: " + foodWarning);
            }
            if (!time.equals("")) {
                holder.doseTimeTV.setVisibility(View.VISIBLE);
                holder.doseTimeTV.setText(time);
            }
            if (medicationModel.getDurationUnit() != null) {
                holder.durationTV.setVisibility(View.VISIBLE);
                holder.durationTV.setText("Duration: " + medicationModel.getDuration() + " " + medicationModel.getDurationUnit());
            }
            holder.serialNoTV.setText(String.valueOf(position + 1 + "."));
            holder.medicineNameTV.setText(name);
            //   holder.durationTV.setText(duration);
            holder.instructionTV.setText(medicationModel.getInstruction());
        } else if (source.equals(Constants.DIAGNOSTIC)) {
            DiagnosticModel diagnosticModel = diagnostics.get(position);
            holder.tabIcon.setVisibility(View.GONE);
            holder.serialNoTV.setText(String.valueOf(position + 1 + "."));
            holder.foodWarningTV.setVisibility(View.GONE);
            holder.doseTimeTV.setVisibility(View.GONE);
            holder.durationTV.setVisibility(View.GONE);
            holder.medicineNameTV.setText(diagnosticModel.getName());
            holder.instructionTV.setText(diagnosticModel.getInstruction());
        }

    }

    @Override
    public int getItemCount() {
        return ((source.equals(Constants.PRESCRIPTION)) ? medications.size() : diagnostics.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.medicine_name)
        TextView medicineNameTV;
        @BindView(R.id.medicine_warnings)
        TextView foodWarningTV;
        @BindView(R.id.medicine_time)
        TextView doseTimeTV;
        @BindView(R.id.medicine_duration)
        TextView durationTV;
        @BindView(R.id.medicine_instruction)
        TextView instructionTV;
        @BindView(R.id.txt_serial_no)
        TextView serialNoTV;
        @BindView(R.id.iv_medicine_type_presc)
        ImageView tabIcon;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
