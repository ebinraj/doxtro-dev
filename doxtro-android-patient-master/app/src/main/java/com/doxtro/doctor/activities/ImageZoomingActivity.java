package com.doxtro.doctor.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;


public class ImageZoomingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_image_zoom);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            ImageView imageView = (ImageView) findViewById(R.id.image_view);
            String imageUrl = getIntent().getStringExtra(Constants.URL);
            CameraUtils.loadImageViaGlide(this, imageView, Uri.parse(imageUrl));
        }

        ImageView ivBack = (ImageView) findViewById(R.id.iv_back);
        ivBack.setOnClickListener(v -> onBackPressed());
    }
}
