package com.doxtro.doctor.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.EditBasicInfoActivity;
import com.doxtro.doctor.custom.CircleImageView;
import com.doxtro.doctor.interfaces.DocumentUploadListener;
import com.doxtro.doctor.model.BasicProfileUpdateModel;
import com.doxtro.doctor.model.ProfileModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.Reusable;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */

public class ProfileFragment extends Fragment implements DocumentUploadListener {

    @BindView(R.id.profile_image)
    CircleImageView profileImageIV;
    @BindView(R.id.profile_edit_IV)
    ImageView editIV;
    @BindView(R.id.profile_name)
    TextView nameTV;
    @BindView(R.id.profile_specialisation)
    TextView specialisationTV;
    @BindView(R.id.profile_tablayout)
    TabLayout tableLayout;
    @BindView(R.id.profile_viewpager)
    ViewPager viewPager;
    @BindView(R.id.profile_fragement_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.profile_image_edit_IV)
    ImageView picEditIV;


    private AvailabilityFragment availabilityFragment;
    private UploadDocumentFragment uploadDocumentFragment;
    private EarningsFragment earningsFragment;
    private ProfileModel profileData;
    private static final int REQUEST_CODE_MY_PROFILE = 111;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_edit_profile, container, false);
        ButterKnife.bind(this, view);
        //  DialogUtils.showProgressDialog(getContext(), getString(R.string.pleaseWaittxt));
        picEditIV.setVisibility(View.GONE);
        // In case shared pref data is null
//        profileData=SharePref.getDoctorDetail(getContext());
//        if (profileData == null) {
//            if (NetworkUtil.getConnectionStatus(getContext()) != 0) {
//                fetchDoctorProfile();
//            } else {
//
//                DialogUtils.appToast(getContext(), getString(R.string.no_internet_connection));
//            }
//
//        } else {
//            initHeaderView();
//        }
        if (SharePref.getDoctorDetail(getContext()) != null) {
            profileData = SharePref.getDoctorDetail(getContext());
            initHeaderView();
            initViewPager();
        } else {
            fetchDoctorProfile();
        }

        return view;
    }

    public static Bitmap selectedImage;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (selectedImage != null)
            profileImageIV.setImageBitmap(selectedImage);
        initHeaderView();
    }

    private void getProfilePicFromSharedPref() {
        profileData = SharePref.getDoctorDetail(getContext());
        if (profileData != null) {
            if (profileData.getData().getProfilePic() != null) {
                String profileUrl = profileData.getData().getProfilePic();
                CameraUtils.loadImageViaGlide(getActivity(), profileImageIV, Uri.parse(profileUrl));
            } else {
                DialogUtils.dismissProgressDialog();
            }
        }
    }

    /*public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }*/


    //initializing the top view in the layout
    private void initHeaderView() {
        try {
            if (this.profileData.getData() != null && this.profileData.getData() != null) {
                String docName = this.profileData.getData().getFirstName();
                List<String> specializations = this.profileData.getData().getCategory();
//                    StringBuilder stringBuilder = new StringBuilder();
//                    for (String specialization : specializations) {
//                        stringBuilder.append(" | ").append(specialization).append(" | ");
//                    }
                //   profileImageIV.setImageURI(Uri.parse(profileUrl));

                //  CameraUtils.loadImage(getContext(), profileImageIV, Uri.parse(profileUrl));
                String doctorName = "";
                if (docName != null) {
                    doctorName = docName;
                    if (!doctorName.contains("Dr.") && !doctorName.contains("Dr"))
                        doctorName = "Dr. " + doctorName;
                }
                nameTV.setText(doctorName);
                specialisationTV.setText(Reusable.arrayListToString(specializations, " | "));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean _areSharedPrefUpdated = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !_areSharedPrefUpdated) {
            if (selectedImage != null)
                profileImageIV.setImageBitmap(selectedImage);
            initHeaderView();
            _areSharedPrefUpdated = true;
        }
    }

    //changes made in the edit screen has to be refreshed when he switch between the home tabs
    private void fetchDoctorProfile() {
        DialogUtils.showProgressDialog(getContext(), "Profile fragment dialog");
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        Call<ProfileModel> call = apiService.getDoctorProfile(SharePref.getDoctorID(getContext()));
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    DialogUtils.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.i("profile response", response.body().toString());
                        SharePref.setDoctorDetail(getContext(), response.body());
                        //SharePref.saveProfilePic(getContext(), response.body().getData().getProfile().getProfilePic());
                        profileData = response.body();
                        if (selectedImage == null)
                            getProfilePicFromSharedPref();
                        else
                            profileImageIV.setImageBitmap(selectedImage);
                        initHeaderView();
                        initViewPager();
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    //initialize the viewpager
    private void initViewPager() {
        try {
            viewPager.setOffscreenPageLimit(2);
            tableLayout.setupWithViewPager(viewPager);
            List<Fragment> viewPagerFragments = setViewPagerFragment();
            ProfileViewPagerAdapter viewPagerAdapter = new ProfileViewPagerAdapter(getChildFragmentManager(), viewPagerFragments);
            viewPager.setAdapter(viewPagerAdapter);
            viewPager.setCurrentItem(0);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //setting the fragments for view pager
    private List<Fragment> setViewPagerFragment() {
        List<Fragment> fragmentList = new ArrayList<>();
        if (availabilityFragment == null) {
            availabilityFragment = AvailabilityFragment.newInstance(Constants.EDIT_PROFILE);
        }
        if (uploadDocumentFragment == null) {
            BasicProfileUpdateModel basicProfileUpdateModel = new BasicProfileUpdateModel();
            basicProfileUpdateModel.setId(SharePref.getDoctorID(getContext()));
            uploadDocumentFragment = UploadDocumentFragment.newInstance(null, Constants.EDIT_PROFILE, null);
        }
        if (earningsFragment == null) {
            earningsFragment = EarningsFragment.newInstance();
        }
        Collections.addAll(fragmentList, availabilityFragment, uploadDocumentFragment, earningsFragment);
        return fragmentList;

    }

    //on edit button clicked
    @OnClick(R.id.profile_edit_IV)
    public void onEditBtnClick() {
        startActivityForResult(new Intent(getActivity(), EditBasicInfoActivity.class), REQUEST_CODE_MY_PROFILE);
    }


    //adapter for viewpager
    private class ProfileViewPagerAdapter extends FragmentStatePagerAdapter {
        final List<Fragment> fragments;
        final CharSequence[] fragmentsTitle = {getString(R.string.avialabilityTxt), getString(R.string.uploadDocTxt), getString(R.string.earningsTxt)};

        ProfileViewPagerAdapter(FragmentManager fragmentManager, List<Fragment> viewPagerFragments) {
            super(fragmentManager);
            fragments = viewPagerFragments;
        }

        @Override
        public Fragment getItem(int position) {
            if (fragments != null && fragments.size() > 0) {
                return fragments.get(position);
            } else
                return null;
        }

        @Override
        public int getCount() {
            return (fragments != null ? fragments.size() : 0);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentsTitle[position];
        }
    }


    @Override
    public void getDownloadUrl(String url) {
        Log.i("Tag url", url);
    }

    @Override
    public void getDownloadUrlType(String url, String type) {

    }

    @Override
    public void getDeleteResponse(String url, String tag) {

    }

}
