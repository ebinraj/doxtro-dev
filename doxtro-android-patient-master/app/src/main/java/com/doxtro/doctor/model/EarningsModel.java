package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yashaswi on 16/10/17.
 */

public class EarningsModel {
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<EarningDataModel> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<EarningDataModel> getData() {
        return data;
    }

    public void setData(List<EarningDataModel> data) {
        this.data = data;
    }

}
