package com.doxtro.doctor.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.ImageZoomingActivity;
import com.doxtro.doctor.model.HealthRecordsModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ghanshyamnayma on 14/07/17.
 */

public class DiagnosticAdapter extends RecyclerView.Adapter<DiagnosticAdapter.ViewHolder> {
    private List<HealthRecordsModel.Data> diagnosticList = new ArrayList<>();
    private final Context mContext;

    public DiagnosticAdapter(Context context, List<HealthRecordsModel.Data> diagnosticList) {
        this.mContext = context;
        this.diagnosticList = diagnosticList;
    }

    public void update(List<HealthRecordsModel.Data> allergyMedicineList) {
        this.diagnosticList = allergyMedicineList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_diagnostic, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (holder != null) {
            if (diagnosticList.get(position).getUrl() != null) {
                for (int i = 0; i < diagnosticList.get(position).getUrl().size(); i++) {
                    if (diagnosticList.get(position).getUrl().get(i).getUrl() != null) {
                        if (diagnosticList.get(position).getUrl().get(i).getUrlType().equalsIgnoreCase("image")) {
                            CameraUtils.loadImageViaGlide(mContext, holder.imgDiagnostic, Uri.parse(diagnosticList.get(position).getUrl().get(i).getUrl()));
                            int finalI = i;
                            holder.imgDiagnostic.setOnClickListener(v -> {
                                Intent intent = new Intent(mContext, ImageZoomingActivity.class);
                                intent.putExtra(Constants.URL, diagnosticList.get(position).getUrl().get(finalI).getUrl());
                                mContext.startActivity(intent);
                            });
                        } else if (diagnosticList.get(position).getUrl().get(i).getUrlType().equalsIgnoreCase("documents")) {
                            holder.imgDiagnostic.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.drawable.file_placeholder));
                            int finalI = i;
                            holder.imgDiagnostic.setOnClickListener(v -> {
                                if (!diagnosticList.get(position).getUrl().get(finalI).getUrl().startsWith("http://") || !diagnosticList.get(position).getUrl().get(finalI).getUrl().startsWith("https://")) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(diagnosticList.get(position).getUrl().get(finalI).getUrl()));
                                    mContext.startActivity(browserIntent);
                                }
                            });
                        }
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return diagnosticList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgDiagnostic;

        private ViewHolder(View itemView) {
            super(itemView);
            imgDiagnostic = (ImageView) itemView.findViewById(R.id.iv_diagnostic);
        }
    }

// showing custom dialog on delete

}
