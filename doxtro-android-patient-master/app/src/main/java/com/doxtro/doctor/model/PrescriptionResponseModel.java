package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 18/09/17.
 */

public class PrescriptionResponseModel {


    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private Data data;

    @SerializedName("additionalDetails")
    private PatientDetailModel additionalDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public PatientDetailModel getAdditionalDetails() {
        return additionalDetails;
    }

    public void setAdditionalDetails(PatientDetailModel additionalDetails) {
        this.additionalDetails = additionalDetails;
    }


    public class Data {

        @SerializedName("consultationId")
        private String consultationId;

        @SerializedName("type")
        private String type;

        @SerializedName("_id")
        private String id;

        @SerializedName("diagnostic")
        private List<DiagnosticModel> diagnostic = new ArrayList<>();

        @SerializedName("medication")
        private List<MedicationModel> medication = new ArrayList<>();

        @SerializedName("date")
        private String date;

        public String getConsultationId() {
            return consultationId;
        }

        public void setConsultationId(String consultationId) {
            this.consultationId = consultationId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<MedicationModel> getMedication() {
            return medication;
        }

        public void setMedication(List<MedicationModel> medication) {
            this.medication = medication;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }


        public List<DiagnosticModel> getDiagnostic() {
            return diagnostic;
        }

        public void setDiagnostic(List<DiagnosticModel> diagnostic) {
            this.diagnostic = diagnostic;
        }
    }



}
