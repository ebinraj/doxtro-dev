package com.doxtro.doctor.helpers;

import java.util.ArrayList;

/**
 * Created by yashaswi on 03/08/17.
 */

public class SpecilizationExpandableHeader {
    private String name;
    private ArrayList<SpecilizationExpandableRow> specializationList = new ArrayList<>();

    public SpecilizationExpandableHeader(String name,
                                  ArrayList<SpecilizationExpandableRow> specializationList) {
        super();
        this.name = name;
        this.specializationList = specializationList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SpecilizationExpandableRow> getSpecializationList() {
        return specializationList;
    }

    public void setSpecializationList(ArrayList<SpecilizationExpandableRow> specializationList) {
        this.specializationList = specializationList;
    }

}
