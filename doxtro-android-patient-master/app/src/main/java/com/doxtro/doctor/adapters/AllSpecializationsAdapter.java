package com.doxtro.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.helpers.SpecilizationExpandableHeader;
import com.doxtro.doctor.helpers.SpecilizationExpandableRow;

import java.util.ArrayList;

/**
 * Created by yashaswi on 03/08/17.
 */

public class AllSpecializationsAdapter  extends BaseExpandableListAdapter{
    private Context context;
    private ArrayList<SpecilizationExpandableHeader> specializationList;
    private ArrayList<SpecilizationExpandableHeader> originalList;

    //selected speciality
    private ArrayList<String> specialityMap=new ArrayList<>(0);


    public AllSpecializationsAdapter(Context context,
                                ArrayList<SpecilizationExpandableHeader> specializationList,
                                ArrayList<String> specialityMap) {
        this.context = context;
        this.specializationList = new ArrayList<>();
        this.specializationList.addAll(specializationList);
        this.originalList = new ArrayList<>();
        this.originalList.addAll(specializationList);
        this.specialityMap = new ArrayList<>();
        this.specialityMap = specialityMap;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<SpecilizationExpandableRow> countryList =
                specializationList.get(groupPosition).getSpecializationList();
        return countryList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        SpecilizationExpandableRow childItem =
                (SpecilizationExpandableRow) getChild(groupPosition, childPosition);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.specialization_child_item, null);
        }

        TextView subSpecialization = (TextView) view.findViewById(R.id.specialization_sub_category_TV);
        subSpecialization.setText(childItem.getSubSpecialization().trim());

        ImageView subSpecializationRadioUnChecked = (ImageView) view.findViewById(R.id.specialization_sub_category_add);
        ImageView subSpecializationRadioChecked = (ImageView) view.findViewById(R.id.specialization_sub_category_remove);

        if(specialityMap!=null) {
            if (specialityMap.contains(subSpecialization.getText().toString())) {
                subSpecializationRadioUnChecked.setVisibility(View.GONE);
                subSpecializationRadioChecked.setVisibility(View.VISIBLE);
            } else {
                subSpecializationRadioUnChecked.setVisibility(View.VISIBLE);
                subSpecializationRadioChecked.setVisibility(View.GONE);
            }
        }


        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        ArrayList<SpecilizationExpandableRow> subSpecializationList =
                specializationList.get(groupPosition).getSpecializationList();
        return subSpecializationList.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return specializationList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return specializationList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {


        SpecilizationExpandableHeader specialization =
                (SpecilizationExpandableHeader) getGroup(groupPosition);

        if (view == null) {
            LayoutInflater layoutInflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.specialization_main_item, null);
        }

        TextView heading = (TextView) view.findViewById(R.id.specialization_category_TV);
        heading.setText(specialization.getName().trim());

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void filterData(String query) {

        query = query.toLowerCase();
        Log.v("MyListAdapter", String.valueOf(specializationList.size()));
        specializationList.clear();

        if (query.isEmpty()) {
            specializationList.addAll(originalList);
        } else {

            for (SpecilizationExpandableHeader specialization : originalList) {

                ArrayList<SpecilizationExpandableRow> subSpecializationList =
                        specialization.getSpecializationList();
                ArrayList<SpecilizationExpandableRow> newList = new ArrayList<>();
                for (SpecilizationExpandableRow subSpecialization : subSpecializationList) {
                    if (subSpecialization.getSubSpecialization().toLowerCase().contains(query)) {
                        newList.add(subSpecialization);
                    }
                }
                if (newList.size() > 0) {
                    SpecilizationExpandableHeader nContinent =
                            new SpecilizationExpandableHeader(specialization.getName(), newList);
                    specializationList.add(nContinent);
                }
            }
        }

        Log.v("Adapter", String.valueOf(specializationList.size()));
        notifyDataSetChanged();

    }
}
