package com.doxtro.doctor.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.doxtro.doctor.activities.ChatActivity;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Reusable {


    public static int convertFloatIntoDp(Context context, float number) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, number, context.getResources().getDisplayMetrics());
    }

    // checking cameara and storage permission
    @TargetApi(Build.VERSION_CODES.M)
    public static boolean checkPermission(final Activity context, final int requestCode) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionSendMessage = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
            int locationPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            List<String> listPermissionsNeeded = new ArrayList<>();
            if (locationPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(context, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), requestCode);
                return false;
            }
        }
        return true;
    }

    public static String generateString() {
        return UUID.randomUUID().toString();
    }

    // This will append all string array list item to a single string and all item will be separated by separator (like | or , or blank space)
    public static String arrayListToString(List<String> stringArrayLists, String separator) {
        StringBuilder builder = new StringBuilder();

        for (String string : stringArrayLists) {
            if (builder.length() > 0) {
                builder.append(separator);
            }
            builder.append(string);
        }
        return builder.toString();
    }

    /*
    *  @param str is a string which we need to convert All starting words caps of the sentence
    *  @param defaultStr if anything went wrong in string caps conversion, this method will return
    *  default string whatever we will pass as param 2
    * */
    public static String convertEachFirstLetterCaps(String str, String defaultStr) {
        try {
            str = str.trim();
            String[] strArray = str.split(" ");
            StringBuilder builder = new StringBuilder();
            for (String s : strArray) {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                builder.append(cap + " ");
            }
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return defaultStr; // if something went wrong It will return default string whatever we will pass as param 2
        }

    }

    public static void respondToRequest(Context mContext,
                                        String consultID,
                                        String responseType,
                                        View itemView,
                                        String patientName) {
        try {
            String doctorID = SharePref.getDoctorID(mContext);
            ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);
            Call<JsonObject> call = apiService.sendCosultationResponse(doctorID, consultID, responseType);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        if (response.isSuccessful()) {
                            DialogUtils.appToast(mContext, response.body().getAsJsonObject().get("message").getAsString());
                            Log.i("Response Status", response.message());
                            if (responseType.contains(Constants.ACCEPT)) {
                                if (itemView != null)
                                    itemView.setVisibility(View.GONE);
                                Intent intent = new Intent(mContext, ChatActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(Constants.PATIENT_NAME, patientName);
                                intent.putExtra(Constants.CONSULTATION_TYPE,response.body().get("data").getAsJsonObject().get("consultationType").getAsString());
                                intent.putExtra(Constants.CONSULTATION_ID, response.body().get("data").getAsJsonObject().get("consultationId").getAsString());
                                mContext.startActivity(intent);
                                Map<String, Object> eventValue = new HashMap<String, Object>();
                                eventValue.put("Consultation Accepted","Yes");
                                EventsUtility.eventTrack(mContext,Constants.EVENT_CONSULTATION_ACCEPTED,eventValue);
                                Log.i("Consultation ID", response.body().get("data").getAsJsonObject().get("consultationId").getAsString());
                            } else {
                                if (itemView != null)
                                    itemView.setVisibility(View.GONE);
                                Map<String, Object> eventValue = new HashMap<String, Object>();
                                eventValue.put("Consultation Accepted","No");
                                EventsUtility.eventTrack(mContext,Constants.EVENT_CONSULTATION_ACCEPTED,eventValue);
                            }

                        } else {
                            DialogUtils.appToast(mContext, new JSONObject(response.errorBody().string()).getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("Error", t.toString());
                    DialogUtils.dismissProgressDialog();
                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
