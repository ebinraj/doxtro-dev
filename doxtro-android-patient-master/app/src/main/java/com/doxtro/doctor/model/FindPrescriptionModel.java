package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yashaswi on 19/09/17.
 */

public class FindPrescriptionModel {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<PrescriptionResponseModel.Data> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PrescriptionResponseModel.Data> getData() {
        return data;
    }

    public void setData(List<PrescriptionResponseModel.Data> data) {
        this.data = data;
    }

}
