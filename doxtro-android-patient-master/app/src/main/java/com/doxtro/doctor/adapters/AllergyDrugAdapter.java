package com.doxtro.doctor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.HealthRecordsModel;

import java.util.ArrayList;

public class AllergyDrugAdapter extends RecyclerView.Adapter<AllergyDrugAdapter.ViewHolder> {
    private ArrayList<HealthRecordsModel.Data> allergyMedicineList;


    public AllergyDrugAdapter(Context context, ArrayList<HealthRecordsModel.Data> allergyMedicineList, TextView noRecords) {
        this.allergyMedicineList = allergyMedicineList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_allergy, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.medicineName.setText("" + allergyMedicineList.get(position).getContent());
            holder.medicineReason.setText("" + allergyMedicineList.get(position).getNote());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return allergyMedicineList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView medicineName, medicineReason;

        private ViewHolder(View itemView) {
            super(itemView);
            medicineName = (TextView) itemView.findViewById(R.id.tv_allergy_title);
            medicineReason = (TextView) itemView.findViewById(R.id.tv_allergy_reason);
        }
    }

    // showing custom dialog on delete

}
