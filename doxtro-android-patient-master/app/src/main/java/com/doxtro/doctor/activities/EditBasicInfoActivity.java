package com.doxtro.doctor.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.ProfessionEducationAdapter;
import com.doxtro.doctor.custom.CircleImageView;
import com.doxtro.doctor.fragments.EditInfoDialogFragment;
import com.doxtro.doctor.fragments.ProfileFragment;
import com.doxtro.doctor.interfaces.DialogDataListener;
import com.doxtro.doctor.interfaces.DocumentUploadListener;
import com.doxtro.doctor.model.BasicProfileUpdateModel;
import com.doxtro.doctor.model.DegreeResponseModel;
import com.doxtro.doctor.model.ProfessionModel;
import com.doxtro.doctor.model.ProfileModel;
import com.doxtro.doctor.model.QualificationModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.FirebaseUtils;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.Reusable;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("deprecation")
public class EditBasicInfoActivity extends AppCompatActivity implements View.OnClickListener, DocumentUploadListener, DialogDataListener {

    private static final String TAG = EditBasicInfoActivity.class.getSimpleName();
    @BindView(R.id.profile_image)
    CircleImageView profileImageIV;
    @BindView(R.id.profile_edit_IV)
    ImageView editIV;
    @BindView(R.id.profile_name)
    TextView nameTV;
    @BindView(R.id.profile_specialisation)
    TextView specialisationTV;
    @BindView(R.id.profile_edit_summary)
    ImageView summaryEditIV;
    @BindView(R.id.profile_summaryTV)
    TextView summaryTV;
    @BindView(R.id.professionalRV)
    RecyclerView professionRV;
    @BindView(R.id.profile_experience_add)
    TextView addProfession;
    @BindView(R.id.profile_ug_spinner)
    Spinner ugSpinner;
    @BindView(R.id.profile_pg_spinner)
    Spinner pgSpinner;
    @BindView(R.id.profile_ug_collegeET)
    EditText ugCollegeET;
    @BindView(R.id.profile_pg_collegeET)
    EditText pgCollegeET;
    @BindView(R.id.profile_ug_degreeET)
    EditText ugOtherDegreeET;
    @BindView(R.id.profile_pg_degreeET)
    EditText pgOtherDegreeET;
    @BindView(R.id.profile_image_edit_IV)
    ImageView picEditIV;
    @BindView(R.id.save_edit_profile_btn)
    Button saveBtn;
    @BindView(R.id.profile_education_add)
    TextView educationAddTV;
    @BindView(R.id.educationList)
    RecyclerView educationRV;


    private ProfileModel profile = new ProfileModel();


    private List<ProfessionModel> professionList = new ArrayList<>();
    private String downloadURL = "";
    private List<String> degreeList = new ArrayList<>();
    private List<QualificationModel> educationDataList = new ArrayList<>();

    @SuppressLint("InflateParams")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(this).inflate(R.layout.activity_edit_basic_info, null);
        setContentView(view);
        ButterKnife.bind(this);
        profile = SharePref.getDoctorDetail(EditBasicInfoActivity.this);
        new FirebaseUtils(this);
        getEducationDegree();
        updateViews();
        initListeners();

    }

    private void getEducationDegree() {
        DialogUtils.showProgressDialog(this, "Please wait a while..");
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Call<DegreeResponseModel> call = apiService.getDegree();
        call.enqueue(new Callback<DegreeResponseModel>() {
            @Override
            public void onResponse(Call<DegreeResponseModel> call, Response<DegreeResponseModel> response) {
                try {

                    if (response.isSuccessful()) {
                        degreeList = response.body().getData();
                        initEducations();
                        Log.i("List", degreeList.toString());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<DegreeResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }


    private void fetchDoctorData() {
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
        Call<ProfileModel> call = apiService.getDoctorProfile(SharePref.getDoctorID(this));
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    if (response.isSuccessful()) {
                        Log.i("profile response", response.body().toString());
                        SharePref.setDoctorDetail(EditBasicInfoActivity.this, response.body());
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        DialogUtils.appToast(EditBasicInfoActivity.this, new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (data != null) {
                switch (requestCode) {
                    //when image is selected from gallery
                    case Constants.SELECT_IMAGE:
                        Bitmap selectedImage = CameraUtils.selctedImageFromGallery(this, data);
                        ProfileFragment.selectedImage = selectedImage;
                        FirebaseUtils.storeInFirebase(this, CameraUtils.getImageUri(selectedImage, this), Constants.TYPE_PICTURE);
                        profileImageIV.setImageBitmap(selectedImage);
                        break;
                    //when image is clicked
                    case Constants.TAKE_IMAGE:
                        Bitmap capturedImage = CameraUtils.capturedImage(this, data);
                        ProfileFragment.selectedImage = capturedImage;
                        FirebaseUtils.storeInFirebase(this, CameraUtils.getImageUri(capturedImage, this), Constants.TYPE_PICTURE);
                        profileImageIV.setImageBitmap(capturedImage);
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initListeners() {
        picEditIV.setOnClickListener(this);
        profileImageIV.setOnClickListener(this);
        summaryEditIV.setOnClickListener(this);
        addProfession.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        pgCollegeET.setOnClickListener(this);
        ugCollegeET.setOnClickListener(this);
        educationAddTV.setOnClickListener(this);
    }

    private void updateViews() {
        professionRV.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
        educationRV.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });
        editIV.setVisibility(View.GONE);
        if (profile != null) {
            BasicProfileUpdateModel profileModel = profile.getData();
            if (profileModel != null && profileModel.getProfilePic() != null) {
                Uri profileUri = Uri.parse(profileModel.getProfilePic());
                if (ProfileFragment.selectedImage == null)
                    CameraUtils.loadImageViaGlide(EditBasicInfoActivity.this, profileImageIV, profileUri);
                else
                    profileImageIV.setImageBitmap(ProfileFragment.selectedImage);
            }
            assert profileModel != null;
            String doctorName = "";
            if (profileModel.getFirstName() != null) {
                doctorName = profileModel.getFirstName();
                if (!doctorName.contains("Dr.") && !doctorName.contains("Dr"))
                    doctorName = "Dr. " + doctorName;
            }
           nameTV.setText(doctorName);
           // nameTV.setText(Html.fromHtml(getString(R.string.drTxt) + ));
            List<String> specializations = profile.getData().getCategory();
            specialisationTV.setText(Reusable.arrayListToString(specializations, " | "));
            if (profileModel.getBio() != null) {
                summaryTV.setText(profileModel.getBio());
            }

            initProfessions();
        }
    }

    private void initProfessions() {
        professionList = SharePref.getDoctorDetail(this).getData().getWorksFor();
        if (professionList != null && !professionList.isEmpty()) {
            ProfessionEducationAdapter adapter = new ProfessionEducationAdapter(Constants.PROFESSION, this, professionList, null, null);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            professionRV.setLayoutManager(layoutManager);
            professionRV.setAdapter(adapter);
        }
    }

    private void initEducations() {
        educationDataList = SharePref.getDoctorDetail(this).getData().getQualification();
        if (!educationDataList.isEmpty() && !degreeList.isEmpty()) {
            ProfessionEducationAdapter adapter = new ProfessionEducationAdapter(Constants.EDUCATION, this, null, educationDataList, degreeList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            educationRV.setLayoutManager(layoutManager);
            educationRV.setAdapter(adapter);
        }


    }


    @Override
    public void onClick(View v) {
        FragmentTransaction ft = (this).getSupportFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.profile_image_edit_IV:
                changeImageAlertDialog();
                break;
            case R.id.profile_image:
                changeImageAlertDialog();
                break;
            case R.id.profile_edit_summary:
                EditInfoDialogFragment editInfoDialogFragment = EditInfoDialogFragment.newInstance(Constants.SUMMARY, this, null);
                editInfoDialogFragment.sendSummary(summaryTV.getText().toString());
                editInfoDialogFragment.show(ft, TAG);
                break;
            case R.id.profile_experience_add:
                EditInfoDialogFragment editInfoDialogFragment1 = EditInfoDialogFragment.newInstance(Constants.PROFESSION, 0, false, null, null, null, this);
                editInfoDialogFragment1.show(ft, TAG);
                break;
            case R.id.save_edit_profile_btn:
                onSaveClicked();
                break;
            case R.id.profile_education_add:
                if (degreeList.size() > 0) {
                    EditInfoDialogFragment editInfoDialogFragment2 = EditInfoDialogFragment.newInstance(Constants.EDUCATION, 0, false, null, null, degreeList, this);
                    editInfoDialogFragment2.show(ft, TAG);
                }
                break;

        }

    }

    private void onSaveClicked() {
        try {
            BasicProfileUpdateModel basicProfileUpdateModel = new BasicProfileUpdateModel();
            basicProfileUpdateModel.setId(SharePref.getDoctorID(this));
            basicProfileUpdateModel.setBio(summaryTV.getText().toString());
            basicProfileUpdateModel.setWorksFor(professionList);
            basicProfileUpdateModel.setQualification(educationDataList);
            //  basicProfileUpdateModel.setWorksFor(professionList);
            basicProfileUpdateModel.setDocuments(SharePref.getDoctorDetail(this).getData().getDocuments());
            basicProfileUpdateModel.setCategory(SharePref.getDoctorDetail(this).getData().getCategory());
            basicProfileUpdateModel.setLanguage(SharePref.getDoctorDetail(this).getData().getLanguage());
            basicProfileUpdateModel.setSpecialization(SharePref.getDoctorDetail(this).getData().getSpecialization());
            if (downloadURL != null && !downloadURL.equals("")) {
                basicProfileUpdateModel.setProfilePic(downloadURL);
            }
            if (NetworkUtil.getConnectionStatus(this) != 0) {
                callProfileUpdateAPI(basicProfileUpdateModel);
            } else {

                DialogUtils.appToast(this, getString(R.string.no_internet_connection));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callProfileUpdateAPI(BasicProfileUpdateModel basicProfileUpdateModel) {
//        SharePref.setProfileConfigShown(getContext(), true);
        DialogUtils.showProgressDialog(this, getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
        Call<BasicProfileUpdateModel> call = apiService.updateDoctorProfile(basicProfileUpdateModel);
        call.enqueue(new Callback<BasicProfileUpdateModel>() {
            @Override
            public void onResponse(Call<BasicProfileUpdateModel> call, Response<BasicProfileUpdateModel> response) {
                try {
                    if (response.isSuccessful()) {
                        SharePref.getDoctorDetail(EditBasicInfoActivity.this).setData(basicProfileUpdateModel);
                        Toast.makeText(EditBasicInfoActivity.this, "Profile saved!", Toast.LENGTH_SHORT).show();
                        fetchDoctorData();

                    } else {
                        DialogUtils.appToast(EditBasicInfoActivity.this, new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<BasicProfileUpdateModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }


    //dialog to let user to click or choose an image
    private void changeImageAlertDialog() {
        try {

            CharSequence[] items = {this.getString(R.string.choosePhoto), this.getString(R.string.takephoto)};
            new AlertDialog.Builder(this)
                    .setTitle("Select your option")
                    .setNegativeButton(this.getString(R.string.cancelTxt), null)
                    .setItems(items, (dialog, which) -> {
                        if (which == 0) {
                            pickImage();
                        } else if (which == 1) {
                            takeImage();
                        } else {
                            dialog.dismiss();
                        }
                    }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //method to pick an image from gallery
    private void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), Constants.SELECT_IMAGE);
    }

    //method to click an image
    private void takeImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, Constants.TAKE_IMAGE);
    }

    //
    @Override
    public void getDownloadUrl(String url) {
        Log.i("Tag url", url);
        if (url != null) {
            downloadURL = url;
            //     BasicProfileUpdateModel basicProfileModel=new BasicProfileUpdateModel();
        }
    }

    @Override
    public void getDownloadUrlType(String url, String type) {

    }

    @Override
    public void getDeleteResponse(String url, String tag) {

    }

    @Override
    public void getSummaryData(String summary) {
        if (summary != null) {
            summaryTV.setText(summary);
        }
    }

    @Override
    public void getProfessionalData(Bundle professionData) {
        addProfessionExperience(professionData);
    }

    private void addProfessionExperience(Bundle professionData) {
        ProfessionModel professionModel = new ProfessionModel();
        String organization = professionData.getString("working_at");
        String experience = professionData.getString("experience");
        professionModel.setOrganization(organization);
        professionModel.setExperience(experience);
        professionList.add(professionModel);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ProfessionEducationAdapter adapter = new ProfessionEducationAdapter(Constants.PROFESSION, EditBasicInfoActivity.this, professionList, null, degreeList);
        professionRV.setLayoutManager(layoutManager);
        professionRV.setAdapter(adapter);

    }

    @Override
    public void getEducationData(Bundle educationData) {
        addEducation(educationData);

    }

    @Override
    public void getBankDetails(Bundle bankData) {

    }

    private void addEducation(Bundle educationData) {
        QualificationModel qualificationModel = new QualificationModel();
        String organization = educationData.getString("organization");
        String type = educationData.getString("type");
        String degree = educationData.getString("degree");
        String additionalData = educationData.getString("additional_data");
        String yearTo = educationData.getString("yearTo");
        String yearFrom = educationData.getString("yearFrom");
        qualificationModel.setSpecialization(degree);
        qualificationModel.setType(type);
        qualificationModel.setInstitute(organization);
        qualificationModel.setYearTo(yearTo);
        qualificationModel.setYearFrom(yearFrom);
        qualificationModel.setAdditionalData(additionalData);
        educationDataList.add(qualificationModel);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ProfessionEducationAdapter adapter = new ProfessionEducationAdapter(Constants.EDUCATION, EditBasicInfoActivity.this, null, educationDataList, degreeList);
        educationRV.setLayoutManager(layoutManager);
        educationRV.setAdapter(adapter);

    }


}
