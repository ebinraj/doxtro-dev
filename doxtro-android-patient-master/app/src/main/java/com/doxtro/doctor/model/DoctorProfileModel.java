package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yashaswi on 19/09/17.
 */

public class DoctorProfileModel {
    @SerializedName("duns")
    private String duns;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("qualification")
    private List<String> qualification;

    @SerializedName("category")
    private List<String> category;


    public String getDuns() {
        return duns;
    }

    public void setDuns(String duns) {
        this.duns = duns;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<String> getQualification() {
        return qualification;
    }

    public void setQualification(List<String> qualification) {
        this.qualification = qualification;
    }


    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> specializations) {
        this.category = specializations;
    }
}
