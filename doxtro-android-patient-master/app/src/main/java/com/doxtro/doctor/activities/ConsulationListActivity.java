package com.doxtro.doctor.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.ConsultationAdapter;
import com.doxtro.doctor.model.ConsultationsDataModel;
import com.doxtro.doctor.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConsulationListActivity extends AppCompatActivity {
    private static List<ConsultationsDataModel> dataList=new ArrayList<>();
    @BindView(R.id.entire_list_RV)
    RecyclerView consultRequestRV;
    @BindView(R.id.noDataText)
    TextView noDataTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulation_list);
        ButterKnife.bind(this);
        initRequestRV();
    }

    private void initRequestRV() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        consultRequestRV.setLayoutManager(layoutManager);
        ConsultationAdapter consultationAdapter = new ConsultationAdapter(dataList, this, Constants.MORE, null,noDataTV);
        consultRequestRV.setAdapter(consultationAdapter);

    }
    public static void getConsultationList(List<ConsultationsDataModel> consultationsDataModels)
    {
        dataList=consultationsDataModels;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        //intent.putExtra("refresh",true);
        setResult(Activity.RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

}
