package com.doxtro.doctor.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @author ghanshyamnayma on 24/05/17.
 * @file TODO: Add a class header comment!
 * @copyright ABOVE Solutions India Pvt. Ltd.All Rights Reserved.
 */

/*
* Used to close and open SoftInput keyboard
* */
public class SoftInput {

    public static void closeSoftInput(Activity activity)
    {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
