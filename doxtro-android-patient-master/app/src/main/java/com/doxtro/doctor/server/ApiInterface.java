package com.doxtro.doctor.server;


import com.doxtro.doctor.model.AvailabilityModel;
import com.doxtro.doctor.model.BasicProfileUpdateModel;
import com.doxtro.doctor.model.ConsultationRequestModel;
import com.doxtro.doctor.model.DegreeResponseModel;
import com.doxtro.doctor.model.EarningsModel;
import com.doxtro.doctor.model.ExoticCallConnectModel;
import com.doxtro.doctor.model.FindPrescriptionModel;
import com.doxtro.doctor.model.GetConsultDetailsModel;
import com.doxtro.doctor.model.GetHealthRecordsModel;
import com.doxtro.doctor.model.LoginResponse;
import com.doxtro.doctor.model.OTPVerifyResponse;
import com.doxtro.doctor.model.PrescriptionPostModel;
import com.doxtro.doctor.model.PrescriptionResponseModel;
import com.doxtro.doctor.model.ProfileModel;
import com.doxtro.doctor.model.SearchModel;
import com.doxtro.doctor.model.SpecializationResponse;
import com.doxtro.doctor.model.UpdateDeviceIdModel;
import com.doxtro.doctor.model.WeeklyEarningsModel;
import com.doxtro.doctor.utils.Constants;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by ghanshyamnayma on 13/07/17.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST(Constants.signUp)
    Call<LoginResponse> signUpDoctor(@Field("firstName") String firstName, @Field("emailid") String emailID, @Field("mobile") String maobileNumber);

    @FormUrlEncoded
    @POST(Constants.signIn)
    Call<LoginResponse> signInDoctor(@Field("mobile") String mobileNumber);

    @FormUrlEncoded
    @POST(Constants.otpVerify)
    Call<OTPVerifyResponse> verifyDoctor(@Field("_id") String id, @Field("otp") String otp, @Field("typeOfPhone") String typeOfPhone, @Field("deviceId") String deviceId);

    @GET(Constants.specializationList)
    Call<SpecializationResponse> getSpecializations();

    @PUT(Constants.setProfile)
    Call<BasicProfileUpdateModel> updateDoctorProfile(@Body BasicProfileUpdateModel basicPtofileData);


    @GET(Constants.getProfile)
    Call<ProfileModel> getDoctorProfile(@Query("_id") String id);

    @GET(Constants.getLanguages)
    Call<JsonObject> getLanguages();

    @POST(Constants.setAvailability)
    Call<AvailabilityModel> updateAvailability(@Body AvailabilityModel availabilityModel);

    @FormUrlEncoded
    @POST(Constants.updateAvailabilityStatus)
    Call<JsonObject> updateAvailabilityStatus(@Field("_id") String id, @Field("availabilityStatus") String status);

    @GET(Constants.getConsultationRequests)
    Call<ConsultationRequestModel> getTodaysRequest(@Query("doctorId") String id);

    @FormUrlEncoded
    @POST(Constants.respondToRequest)
    Call<JsonObject> sendCosultationResponse(@Field("doctorId") String id, @Field("consultationId") String consultID, @Field("responseText") String response);

    @GET(Constants.getAllConsultation)
    Call<ConsultationRequestModel> getConsultations(@Query("doctorId") String id,
                                                    @Query("skip") String skip,
                                                    @Query("limit") String limit,
                                                    @Query("status") String sortField);

    @GET(Constants.getAllConsultation)
    Call<ConsultationRequestModel> getConsultations(@Query("doctorId") String id,
                                                    @Query("skip") String skip,
                                                    @Query("limit") String limit);

    @GET(Constants.getConsultationDetails)
    Call<GetConsultDetailsModel> getConsultationsDetails(@Query("consultationId") String consultationId);


    @GET(Constants.getAllDegrees)
    Call<DegreeResponseModel> getDegree();

    @POST(Constants.createPrescription)
    Call<PrescriptionResponseModel> sendPrescription(@Body PrescriptionPostModel prescriptionPostModel);

    @GET(Constants.getAllPrescriptions)
    Call<FindPrescriptionModel> getPrescriptions(@Query("consultationId") String consultationID, @Query("type") String type);

    @POST(Constants.updatePrescriptions)
    Call<PrescriptionResponseModel> updatePrescriptions(@Body PrescriptionPostModel prescriptionPostModel);

    @POST(Constants.closeConsultation)
    Call<JsonObject> closeConsultation(@Body JsonObject jsonObject);

    @FormUrlEncoded
    @POST(Constants.updateDeviceID)
    Call<UpdateDeviceIdModel> updateDeviceIdToServer(@Field("_id") String patientId, @Field("deviceId") String deviceId, @Field("typeOfApp") String typeOfApp, @Field("typeOfPhone") String typeOfPhone);

    @GET(Constants.getAutoSuggestions)
    Call<JsonObject> getAutoSuggestions(@Query("doctorId") String doctorID, @Query("type") String type);

    @GET(Constants.getEarnings)
    Call<EarningsModel> getMonthEarnings(@Query("doctorId") String doctorID);

    @PUT(Constants.setProfile)
    Call<ProfileModel> updateDoctorProfileWithObject(@Body JsonObject jsonObject);

    @GET(Constants.getSearchResults)
    Call<SearchModel> getSearchResults(@Query("searchText") String searchText, @Query("type") String type);

    @GET(Constants.getWeeklyEarnings)
    Call<WeeklyEarningsModel> getWeeklyEarnings(@Query("doctorId") String doctorID);

    @GET(Constants.getHealthRecord)
        // get all health record
    Call<GetHealthRecordsModel> getAllHealthRecords(@Query("relativesId") String relativesId);

    @FormUrlEncoded
    @POST(Constants.exoticCallConnect)
    Call<ExoticCallConnectModel> connectExoticCall(@Field("From") String from, @Field("To") String to, @Field("CallerId") String callerID);

}


