package com.doxtro.doctor.activities;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.AllSpecializationsAdapter;
import com.doxtro.doctor.adapters.SelectedSpecializationAdapter;
import com.doxtro.doctor.helpers.SpecilizationExpandableHeader;
import com.doxtro.doctor.helpers.SpecilizationExpandableRow;
import com.doxtro.doctor.model.SpecializationResponse;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yashaswi on 27/07/17.
 */

public class SpecializationActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
    private static final String TAG = SpecializationActivity.class.getSimpleName();
    @BindView(R.id.specialization_entire_specialization_list)
    ExpandableListView entireSpecializationELV;
    @BindView(R.id.specialization_selected_list)
    ExpandableListView selectedSpecializationELV;
    @BindView(R.id.specialization_searchview)
    SearchView searchView;
    @BindView(R.id.toolbar_doneBtn)
    TextView done;
    @BindView(R.id.common_base_toolbarT)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolBarTitle;


    private SelectedSpecializationAdapter selectedSpecializationAdapter;
    private ArrayList<String> specialityMap = new ArrayList<>(0);

    private ArrayList<SpecilizationExpandableHeader> specializationDataList = new ArrayList<>();
    private AllSpecializationsAdapter allSpecializationAdapter;

    private ArrayList<String> selectedheader = new ArrayList<>();
    private List<String> mainCategories = new ArrayList<>();

    private ArrayList<String> selectedSpecilaization = new ArrayList<>();


    public static SpecializationActivity newInstance() {
        return new SpecializationActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.fragment_specialization, null);
        setContentView(view);
        ButterKnife.bind(this);
        if (getIntent() != null && getIntent().getStringArrayListExtra("sub_categories") != null) {
            selectedSpecilaization = getIntent().getStringArrayListExtra("sub_categories");
            Log.e("Selected specializations", selectedSpecilaization.toString());
        }
        updateSelectedSpecializations();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        done.setVisibility(View.VISIBLE);


        if (NetworkUtil.getConnectionStatus(this) != 0) {
            getSpecializationList();
        } else {
            DialogUtils.appToast(this, getString(R.string.no_internet_connection));
        }
        done.setOnClickListener(v -> {

            Intent returnIntent = getIntent();
            returnIntent.putExtra("categories", selectedheader);
            returnIntent.putExtra("sub_categories", specialityMap);
            setResult(RESULT_OK, returnIntent);
            finish();


        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolBarTitle.setText(getString(R.string.specialization));
        toolbar.setNavigationOnClickListener(v -> finish());


    }

    private void updateSelectedSpecializations() {

        if (selectedSpecilaization.size() > 0) {
            specialityMap = selectedSpecilaization;
        } else {
            specialityMap.add(0, getString(R.string.noSpecializationtxt));
        }
//        if (specialityMap.size() == 0) {
//        }
        selectedSpecializationAdapter = new SelectedSpecializationAdapter(getApplicationContext(),
                specialityMap);
        selectedSpecializationELV.setAdapter(selectedSpecializationAdapter);
        int count = selectedSpecializationAdapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            selectedSpecializationELV.expandGroup(i);
        }

        selectedSpecializationELV.setOnChildClickListener((expandableListView, view, groupPos, childPos, l) -> {
            TextView subSpecialization = (TextView) view.findViewById(R.id.specialization_sub_category_TV);
            ImageView subSpecializationAdd = (ImageView) view.findViewById(R.id.specialization_sub_category_add);
            ImageView subSpecializationRemove = (ImageView) view.findViewById(R.id.specialization_sub_category_remove);

            if (subSpecializationAdd.getVisibility() == View.VISIBLE) {
                if (specialityMap.size() < 3) {
                    subSpecializationAdd.setVisibility(View.GONE);
                    subSpecializationRemove.setVisibility(View.VISIBLE);
                    specialityMap.add(subSpecialization.getText().toString());
                    if (!selectedheader.contains(specializationDataList.get(groupPos).getName())) {
                        selectedheader.add(specializationDataList.get(groupPos).getName());
                    }
                } else {
                    Toast.makeText(this, "Sorry! You can select only upto 3 specializations!", Toast.LENGTH_SHORT).show();
                }
            } else {
                subSpecializationAdd.setVisibility(View.VISIBLE);
                subSpecializationRemove.setVisibility(View.GONE);
                specialityMap.remove(subSpecialization.getText().toString());
                selectedheader.remove(specializationDataList.get(groupPos).getName());
                if (specialityMap.size() == 0) {
                    specialityMap.add(0, getString(R.string.noSpecializationtxt));
                }
            }
            selectedSpecializationAdapter.notifyDataSetChanged();
            return false;


        });


    }

    private void getSpecializationList() {
        DialogUtils.showProgressDialog(this, "Please wait a while..");
        ApiInterface apiService = ApiClient.getClient(SpecializationActivity.this).create(ApiInterface.class);

        Call<SpecializationResponse> call = apiService.getSpecializations();
        call.enqueue(new Callback<SpecializationResponse>() {
            @Override
            public void onResponse(Call<SpecializationResponse> call, Response<SpecializationResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        updateData(response.body().getData());
                    } else {
                        DialogUtils.appToast(SpecializationActivity.this, new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<SpecializationResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });

    }

    private void updateData(List<SpecializationResponse.SpecializationData> responseData) {
        for (int i = 0; i < responseData.size(); i++) {
            ArrayList<SpecilizationExpandableRow> sub_specialization_list = new ArrayList<>();
            String heading = responseData.get(i).getId();
            List<String> sub = responseData.get(i).getSpecializations();
            for (int j = 0; j < sub.size(); j++) {
                String value = sub.get(j);
                SpecilizationExpandableRow sub_specialization =
                        new SpecilizationExpandableRow(value);
                sub_specialization_list.add(sub_specialization);
            }
            mainCategories.add(i, heading);
            SpecilizationExpandableHeader specialization =
                    new SpecilizationExpandableHeader(heading, sub_specialization_list);

            specializationDataList.add(specialization);

        }
        if (mainCategories.size() > 0 && mainCategories != null) {
            SharePref.setSpecializationCategories(this, mainCategories);
        }
        displayAllSpecializations();
    }

    private void displayAllSpecializations() {
        if (specialityMap.size() == 1)
            specialityMap.clear();
        allSpecializationAdapter = new AllSpecializationsAdapter(SpecializationActivity.this,
                specializationDataList,
                specialityMap);
        entireSpecializationELV.setAdapter(allSpecializationAdapter);
        //  setGroupIndicatorToRight(entireSpecializationELV);
        //expand all Groups
        expandAll();
        entireSpecializationELV.setOnChildClickListener((expandableListView, view, groupPos, i1, l) -> {
            /*Toast.makeText(getApplicationContext(), "i : " + i + "\ti1 : " + i1 + "\tl : " + l,
                    Toast.LENGTH_SHORT).show();*/

            TextView subSpecialization = (TextView) view.findViewById(R.id.specialization_sub_category_TV);

            ImageView subSpecializationRadioUnChecked = (ImageView) view.findViewById(R.id.specialization_sub_category_add);
            ImageView subSpecializationRadioChecked = (ImageView) view.findViewById(R.id.specialization_sub_category_remove);


            if (subSpecializationRadioUnChecked.getVisibility() == View.VISIBLE) {
                if (specialityMap.size() == 1 && specialityMap.get(0).contains(getString(R.string.noSpecializationtxt))) {
                    specialityMap.clear();
                }
                if (specialityMap.size() < 3) {
                    subSpecializationRadioUnChecked.setVisibility(View.GONE);
                    subSpecializationRadioChecked.setVisibility(View.VISIBLE);
                    specialityMap.add(subSpecialization.getText().toString());
                    if (!selectedheader.contains(specializationDataList.get(groupPos).getName())) {
                        selectedheader.add(specializationDataList.get(groupPos).getName());
                    }
                } else {
                    Toast.makeText(this, "Sorry! You can select only upto 3 specializations!", Toast.LENGTH_SHORT).show();
                }

            } else {
                subSpecializationRadioUnChecked.setVisibility(View.VISIBLE);
                subSpecializationRadioChecked.setVisibility(View.GONE);
                if (specialityMap.size() == 0) {
                    specialityMap.add(0, "No specialization selected");
                    subSpecializationRadioUnChecked.setVisibility(View.GONE);
                    subSpecializationRadioChecked.setVisibility(View.GONE);
                }
                specialityMap.remove(subSpecialization.getText().toString());
                selectedheader.remove(specializationDataList.get(groupPos).getName());

            }
            selectedSpecializationAdapter.notifyDataSetChanged();
            return false;

        });


    }


    private void expandAll() {
        int count = allSpecializationAdapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            entireSpecializationELV.expandGroup(i);
        }
    }

    // to set the group indicator to right end bcz default method doesn't work
//    private void setGroupIndicatorToRight(ExpandableListView expandableListView) {
//    /* Get the screen width */
//        DisplayMetrics dm = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        int width = dm.widthPixels;
//        expandableListView.setIndicatorBounds(width - getDipsFromPixel(35), width - getDipsFromPixel(5));
//    }

    // Convert pixel to dip

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Selected specialisations", specialityMap.toString());
        Log.i("Selected categories", selectedheader.toString());

    }

    @Override
    public boolean onClose() {
        allSpecializationAdapter.filterData("");
        expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        allSpecializationAdapter.filterData(query);
        expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (allSpecializationAdapter != null) {
            allSpecializationAdapter.filterData(newText);
            expandAll();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
