package com.doxtro.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.OTPActivity;
import com.doxtro.doctor.model.LoginResponse;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ghanshyamnayma on 12/07/17.
 */

public class SignInFragment extends Fragment {

    private static final String TAG = SignInFragment.class.getSimpleName();
    @BindView(R.id.et_mobile_sign_in)
    EditText mEdtMobile;

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.btn_submit_sign_in)
    public void onClickSubmit() {
        String mobile = mEdtMobile.getText().toString();
        if (mobile.isEmpty() && mobile.length() != 10) {
            mEdtMobile.setError(getString(R.string.err_enter_correct_mobile));
        } else {
            if (NetworkUtil.getConnectionStatus(getActivity()) != 0) {
                doctorSignIn(mobile);
            } else {

                DialogUtils.appToast(getContext(), getString(R.string.no_internet_connection));
            }

        }

    }

    private void doctorSignIn(final String mobile) {
        DialogUtils.showProgressDialog(getActivity(), "Please wait a while..");
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);

        Call<LoginResponse> call = apiService.signInDoctor(mobile);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                try {

                    if (response.isSuccessful()) {
                        SharePref.setDoctorID(getContext(), response.body().getData().get_id());
                        Intent intent = new Intent(getActivity(), OTPActivity.class);
                        intent.putExtra(Constants.SOURCE,Constants.SOURCE_SIGN_IN);
                        intent.putExtra(Constants.MOBILE,"" + mobile);
                        startActivity(intent);

                      //  getActivity().finish();
                    } else {
                        DialogUtils.appToast(getContext(), new JSONObject(response.errorBody().string()).getString("message"));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

}
