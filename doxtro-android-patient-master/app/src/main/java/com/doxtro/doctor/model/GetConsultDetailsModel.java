package com.doxtro.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azharuddin on 8/11/17.
 */

public class GetConsultDetailsModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("_id")
        @Expose
        private String id;

        @SerializedName("patientId")
        @Expose
        private ConsultationsDataModel.PatientModel patientId;

        @SerializedName("relativesId")
        @Expose
        private RelativesId relativesId;
        @SerializedName("doctorId")
        @Expose
        private DoctorId doctorId;
        @SerializedName("consultingFor")
        @Expose
        private String consultingFor;
        @SerializedName("consultationType")
        @Expose
        private String consultationType;
        @SerializedName("consultationLanguage")
        @Expose
        private String consultationLanguage;
        @SerializedName("specializationCategory")
        @Expose
        private String specializationCategory;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("prescriptionId")
        @Expose
        private List<String> prescriptionId = new ArrayList<>();
        @SerializedName("attachedDocuments")
        @Expose
        private List<String> attachedDocuments = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ConsultationsDataModel.PatientModel getPatientId() {
            return patientId;
        }

        public void setPatientId(ConsultationsDataModel.PatientModel patientId) {
            this.patientId = patientId;
        }

        public RelativesId getRelativesId() {
            return relativesId;
        }

        public void setRelativesId(RelativesId relativesId) {
            this.relativesId = relativesId;
        }

        public DoctorId getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(DoctorId doctorId) {
            this.doctorId = doctorId;
        }

        public String getConsultingFor() {
            return consultingFor;
        }

        public void setConsultingFor(String consultingFor) {
            this.consultingFor = consultingFor;
        }

        public String getConsultationType() {
            return consultationType;
        }

        public void setConsultationType(String consultationType) {
            this.consultationType = consultationType;
        }

        public String getConsultationLanguage() {
            return consultationLanguage;
        }

        public void setConsultationLanguage(String consultationLanguage) {
            this.consultationLanguage = consultationLanguage;
        }

        public String getSpecializationCategory() {
            return specializationCategory;
        }

        public void setSpecializationCategory(String specializationCategory) {
            this.specializationCategory = specializationCategory;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getPrescriptionId() {
            return prescriptionId;
        }

        public void setPrescriptionId(List<String> prescriptionId) {
            this.prescriptionId = prescriptionId;
        }

        public List<String> getAttachedDocuments() {
            return attachedDocuments;
        }

        public void setAttachedDocuments(List<String> attachedDocuments) {
            this.attachedDocuments = attachedDocuments;
        }

        public class DoctorId {

            @SerializedName("_id")
            @Expose
            private String id;
            @SerializedName("firstName")
            @Expose
            private String firstName;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

        }

        public class RelativesId {

            @SerializedName("_id")
            @Expose
            private String id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("age")
            @Expose
            private String age;
            @SerializedName("gender")
            @Expose
            private String gender;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

        }

    }

}