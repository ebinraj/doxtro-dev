package com.doxtro.doctor.services;

import android.content.Context;
import android.util.Log;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.UpdateDeviceIdModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sahil on 15/11/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("refreshedToken ", "" + refreshedToken);
        //AppEventsLogger.setPushNotificationsRegistrationId(refreshedToken);

        Log.e("MyInstanceIDLS", "Refreshed token: " + refreshedToken);
        if (SharePref.getDoctorID(getApplicationContext()).equalsIgnoreCase("NA")
                || SharePref.getDoctorID(getApplicationContext()).equalsIgnoreCase("")
                || SharePref.getDoctorID(getApplicationContext()) == null) {
            SharePref.setDeviceId(getApplicationContext(), refreshedToken);
        } else {
            updateDeviceId(getApplicationContext(), refreshedToken);
        }
    }


    public static void updateDeviceId(final Context mContext, String deviceId) {

        if (NetworkUtil.getConnectionStatus(mContext) != 0) {
            // DialogUtils.showProgressDialog(mContext, "Updating Device id...");
            ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);

            Call<UpdateDeviceIdModel> call = apiService.updateDeviceIdToServer(
                    SharePref.getDoctorID(mContext),
                    deviceId,
                    Constants.APP_TYPE,
                    Constants.PHONE_TYPE);
            call.enqueue(new Callback<UpdateDeviceIdModel>() {
                @Override
                public void onResponse(Call<UpdateDeviceIdModel> call, Response<UpdateDeviceIdModel> response) {
                    try {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "Success " + response.body());
                            DialogUtils.appToast(mContext, response.body().getMessage());

                        } else {
                            Log.e(TAG, "failed to update device id");
                            DialogUtils.appToast(mContext, new JSONObject(response.errorBody().string()).getString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //DialogUtils.dismissProgressDialog();
                }

                @Override
                public void onFailure(Call<UpdateDeviceIdModel> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                    //DialogUtils.dismissProgressDialog();
                }
            });
        } else
            DialogUtils.appToast(mContext, mContext.getString(R.string.no_internet_connection));
    }
}

