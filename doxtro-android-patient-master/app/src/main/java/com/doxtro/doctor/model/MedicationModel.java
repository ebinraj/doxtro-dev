package com.doxtro.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 18/09/17.
 */

public class MedicationModel implements Serializable {


    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("doseValue")
    @Expose
    private String doseValue;

    @SerializedName("doseUnit")
    @Expose
    private String doseUnit = "";

    @SerializedName("duration")
    @Expose
    private int duration = 0;

    @SerializedName("instruction")
    @Expose
    private String instruction;

    @SerializedName("doseTime")
    @Expose
    private List<String> doseTime = new ArrayList<>();

    @SerializedName("foodWarning")
    @Expose
    private String foodWarning = "";

    @SerializedName("durationUnit")
    @Expose
    private String durationUnit="";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoseValue() {
        return doseValue;
    }

    public void setDoseValue(String doseValue) {
        this.doseValue = doseValue;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public List<String> getDoseTime() {
        return doseTime;
    }

    public void setDoseTime(List<String> doseTime) {
        this.doseTime = doseTime;
    }

    public String getFoodWarning() {
        return foodWarning;
    }

    public void setFoodWarning(String foodWarning) {
        this.foodWarning = foodWarning;
    }

    //transient bcz need not to send this key to the server
    private transient boolean isBefore=false;

    private transient boolean isAfter=false;

    public boolean isBefore() {
        return isBefore;
    }

    public void setBefore(boolean before) {
        isBefore = before;
    }

    public boolean isAfter() {
        return isAfter;
    }

    public void setAfter(boolean after) {
        isAfter = after;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }
}
