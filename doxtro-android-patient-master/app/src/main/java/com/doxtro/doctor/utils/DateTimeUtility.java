package com.doxtro.doctor.utils;

import android.annotation.SuppressLint;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author ghanshyamnayma on 14/06/17.
 * @file TODO: Date Format and Date compare related utility methods here
 * @copyright ABOVE Solutions India Pvt. Ltd.All Rights Reserved.
 */

public class DateTimeUtility {

    public static String getFormattedTime(String date) {
        try {
            String formattedDate;
            Date date1 = new SimpleDateFormat(Constants.DEFAULT_DATE_FORMAT).parse(date);
            if (DateUtils.isToday(date1.getTime()))
                formattedDate = "Today";
            else
                formattedDate = (String) DateUtils.getRelativeTimeSpanString(date1.getTime());
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "" + date;
        }
    }

    public static String getDefualtCurrentDate() {
        return new SimpleDateFormat(Constants.DEFAULT_DATE_FORMAT).format(new Date());
    }

    public static String getCurrentDate() {
        return new SimpleDateFormat("dd MMM").format(new Date());
    }

    public static String getCurrentDateinDDMMYYYYFormat() {
        return new SimpleDateFormat("dd MMM yyyy").format(new Date());
    }

    public static String getCurrentDay() {
        return new SimpleDateFormat("EEEE").format(new Date());
    }

    public static String getFormattedTimeSlot(String date) {
        SimpleDateFormat sdfmt2 = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT);
        sdfmt2.setTimeZone(TimeZone.getTimeZone("IST"));

        String dateX = "";
        try {
            Date dDate = sdfmt2.parse(date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
            dateX = simpleDateFormat.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateX;
    }

    public static int getCurrentTimeInMilliSec()
    {
        Calendar c = Calendar.getInstance();
        int mseconds = c.get(Calendar.MILLISECOND);
        return mseconds;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getCurrentTime() {
        String pattern = Constants.SERVER_DATE_FORMAT; //2017-07-25T11:47:04.404Z
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        return simpleDateFormat.format(new Date());
    }

    public static String getFormatDDMMMYYYY(String date) {
        SimpleDateFormat sdfmt2 = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT);
        sdfmt2.setTimeZone(TimeZone.getTimeZone("IST"));

        String dateX = "";
        try {
            Date dDate = sdfmt2.parse(date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
            dateX = simpleDateFormat.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateX;
    }

    public static String getChatTime(long date) {
        SimpleDateFormat sdfmt2 = new SimpleDateFormat("h:mm a");
//        sdfmt2.setTimeZone(TimeZone.getTimeZone("IST"));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        return sdfmt2.format(calendar.getTime());
    }

    public static String getChatTimeDDMMMYYYY(long date) {
        SimpleDateFormat sdfmt2 = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        return sdfmt2.format(calendar.getTime());
    }
    public static String getChatDDMMMYYYYFromString(String date) {
        SimpleDateFormat sdfmt2 = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT);
        sdfmt2.setTimeZone(TimeZone.getTimeZone("IST"));
        String dateX = "";
        try {
            Date dDate = sdfmt2.parse(date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            dateX = simpleDateFormat.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateX;
    }

    public static String getFormatDDMMMYYHMMA(String date) {
        SimpleDateFormat sdfmt2 = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT);
        sdfmt2.setTimeZone(TimeZone.getTimeZone("IST"));
        String dateX = "";
        try {
            Date dDate = sdfmt2.parse(date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yy, h:mm a");
            dateX = simpleDateFormat.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateX;
    }

    public static String getChatGlobalTime(long date) {
        SimpleDateFormat sdfmt2 = new SimpleDateFormat("dd/MMM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        return sdfmt2.format(calendar.getTime());
    }

}
