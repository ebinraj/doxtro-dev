package com.doxtro.doctor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ghanshyamnayma on 26/10/17.
 */

public class HealthRecordsModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Data> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;
        @SerializedName("patientId")
        @Expose
        private String patientId;
        @SerializedName("relativesId")
        @Expose
        private String relativesId;
        @SerializedName("tag")
        @Expose
        private String tag;
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("note")
        @Expose
        private String note;
        @SerializedName("deleted")
        @Expose
        private Boolean deleted;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("date")
        @Expose
        private String date;

        @SerializedName("url")
        @Expose
        private ArrayList<UrlModel> url = null;

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getPatientId() {
            return patientId;
        }

        public void setPatientId(String patientId) {
            this.patientId = patientId;
        }

        public String getRelativesId() {
            return relativesId;
        }

        public void setRelativesId(String relativesId) {
            this.relativesId = relativesId;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Boolean getDeleted() {
            return deleted;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public ArrayList<UrlModel> getUrl() {
            return url;
        }

        public void setUrl(ArrayList<UrlModel> url) {
            this.url = url;
        }



        @Override
        public String toString() {
            return "ClassPojo [content = " + content + ", updatedAt = " + updatedAt + ", patientId = " + patientId + ", _id = " + id + ", tag = " + tag + ", createdAt = " + createdAt + ", relativesId = " + relativesId + ", __v = " + v + ", date = " + date + ", deleted = " + deleted + ", url = " + url + ", note = " + note + "]";
        }
    }

}
