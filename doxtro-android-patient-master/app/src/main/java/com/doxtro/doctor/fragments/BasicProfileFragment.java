package com.doxtro.doctor.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.HomeActivity;
import com.doxtro.doctor.activities.SpecializationActivity;
import com.doxtro.doctor.custom.CircleImageView;
import com.doxtro.doctor.interfaces.BasicIndicatorListerner;
import com.doxtro.doctor.interfaces.DialogDataListener;
import com.doxtro.doctor.interfaces.DocumentUploadListener;
import com.doxtro.doctor.model.BasicProfileUpdateModel;
import com.doxtro.doctor.model.DegreeResponseModel;
import com.doxtro.doctor.model.ProfessionModel;
import com.doxtro.doctor.model.QualificationModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.CameraUtils;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.FirebaseUtils;
import com.doxtro.doctor.utils.FragmentsActionUtility;
import com.doxtro.doctor.utils.NetworkUtil;
import com.doxtro.doctor.utils.SharePref;
import com.doxtro.doctor.utils.Validation;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 26/07/17.
 */

//DocumentUploadLister is a interface implemented to get the url once th user upload image/document
public class BasicProfileFragment extends Fragment implements View.OnClickListener, DocumentUploadListener, DialogDataListener {
    @BindView(R.id.btn_next)
    Button nextButton;
    @BindView(R.id.btn_skip_config)
    Button skipConfig;
    @BindView(R.id.profile_specialization)
    TextView specialisationTV;
    @BindView(R.id.img_edit_profile)
    CircleImageView profilePicIV;
    @BindView(R.id.specialization_horizontal_scroll_LL)
    LinearLayout specilizationScrollViewLL;
    @BindView(R.id.et_text_input__mci_no)
    EditText mciNumberET;
    @BindView(R.id.et_text_input_exp_years)
    EditText experienceET;
    @BindView(R.id.et_text_input_about_yourself)
    EditText aboutYourselfET;
    @BindView(R.id.profile_gender_male)
    TextView maleTV;
    @BindView(R.id.profile_gender_female)
    TextView femaleTV;
    @BindView(R.id.languages_text)
    TextView languagesTV;
    @BindView(R.id.languagesLL)
    LinearLayout languagesScrollViewLL;
    @BindView(R.id.profile_education_tapaddTV)
    TextView addEducationTV;
    @BindView(R.id.profile_education_clearTV)
    TextView clearEducationTV;
    @BindView(R.id.profile_profession_tapaddTV)
    TextView tapAddTv;
    @BindView(R.id.add_professional_LL)
    LinearLayout professionalLL;
    @BindView(R.id.profile_profession_clearTV)
    TextView professionClearTV;
    @BindView(R.id.add_education_LL)
    LinearLayout educationLL;


    private ArrayList<String> specializationCategories = new ArrayList<>();
    private ArrayList<String> specializationSubCategories = new ArrayList<>();
    private String genderSelected;
    private String profilePicURL = "";
    private ArrayList<String> languagesSelected = new ArrayList<>();
    private final BasicProfileUpdateModel basicProfileUpdateModel = new BasicProfileUpdateModel();
    private final int LANGUAGE_FRAGMENT = 5;
    private static BasicIndicatorListerner indicatorListerner;
    private FirebaseUtils firebaseUtils;
    private List<ProfessionModel> professionList = new ArrayList<>();
    private List<QualificationModel> educationList = new ArrayList<>();
    private List<String> degreeList = new ArrayList<>();
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private ArrayList<String> selectedSpecializations = new ArrayList<>();
    private String mCurrentPhotoPath;


    public BasicProfileFragment() {
        //empty constructor
    }

    //instance of the fragment
    public static BasicProfileFragment newInstance(BasicIndicatorListerner basicIndicatorListerner) {
        Bundle args = new Bundle();
        BasicProfileFragment fragment = new BasicProfileFragment();
        fragment.setArguments(args);
        indicatorListerner = basicIndicatorListerner;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basic_profile_config, container, false);
        ButterKnife.bind(this, view);
        setUpListerners();
        // initSpinners();
        //  educationTitleTV.setTypeface(Typeface.DEFAULT);
        //default gender to male
        genderSelected = getString(R.string.maleText);
        //year format filters
        experienceET.setFilters(new InputFilter[]{new Validation.DecimalDigitsInputFilter(3, 2)});
        aboutYourselfET.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        });

        if (NetworkUtil.getConnectionStatus(getActivity()) != 0)
            getEducationDegree();
        else
            DialogUtils.appToast(getActivity(), getString(R.string.no_internet_connection));

        return view;
    }

    private void getEducationDegree() {
        DialogUtils.showProgressDialog(getContext(), "Please wait a while..");
        ApiInterface apiService = ApiClient.getClient(getContext()).create(ApiInterface.class);

        Call<DegreeResponseModel> call = apiService.getDegree();
        call.enqueue(new Callback<DegreeResponseModel>() {
            @Override
            public void onResponse(Call<DegreeResponseModel> call, Response<DegreeResponseModel> response) {
                try {

                    if (response.isSuccessful()) {
                        degreeList = response.body().getData();
                        //  initEducations();
                        Log.i("List", degreeList.toString());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<DegreeResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }


    //set all the required listeners here
    private void setUpListerners() {
        firebaseUtils = new FirebaseUtils(this);
        nextButton.setOnClickListener(this);
        skipConfig.setOnClickListener(this);
        specialisationTV.setOnClickListener(this);
        profilePicIV.setOnClickListener(this);
        mciNumberET.setOnClickListener(this);
        femaleTV.setOnClickListener(this);
        maleTV.setOnClickListener(this);
        languagesTV.setOnClickListener(this);
        tapAddTv.setOnClickListener(this);
        professionClearTV.setOnClickListener(this);
        addEducationTV.setOnClickListener(this);
        clearEducationTV.setOnClickListener(this);
    }


    // when the data is received through intents
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (data != null) {
                switch (requestCode) {
                    //when image is selected from gallery
                    case Constants.SELECT_IMAGE:
                        Bitmap seelectedImage = CameraUtils.selctedImageFromGallery(getActivity(), data);
                        FirebaseUtils.storeInFirebase(getContext(), getImageUri(seelectedImage), Constants.TYPE_PICTURE);
                        profilePicIV.setImageBitmap(seelectedImage);
                        break;
                    //when image is clicked
                    case Constants.TAKE_IMAGE:
                        Bitmap capturedImage = CameraUtils.capturedImage(getActivity(), data);
                        FirebaseUtils.storeInFirebase(getContext(), getImageUri(capturedImage), Constants.TYPE_PICTURE);
                        profilePicIV.setImageBitmap(capturedImage);
                        break;
                    //when specialization categories comes
                    case Constants.CATEGORY_RESULT:
                        //noinspection unchecked
                        specializationCategories = (ArrayList<String>) data.getExtras().get("categories");
                        //noinspection unchecked
                        specializationSubCategories = (ArrayList<String>) data.getExtras().get("sub_categories");
                        updateSpecializations();
                        break;
                    //when language list has be sent
                    case Constants.LANGUAGE_RESULT:
                        Bundle bundle = data.getExtras().getBundle("languages");
                        assert bundle != null;
                        languagesSelected = bundle.getStringArrayList("language_selected");
                        updateLanguages();
                        break;
                    //professional detail from dialog is received

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    //display the specializations selected
    private void updateSpecializations() {
        specialisationTV.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.grey_text));
        specilizationScrollViewLL.removeAllViews();
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(10, 10, 10, 10);
        if (specializationSubCategories.size() <= 0) {
            TextView newSpeciality = new TextView(getContext());
            newSpeciality.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.white));
            newSpeciality.setPadding(10, 10, 10, 10);
            newSpeciality.setText(getString(R.string.nospectxt));
            newSpeciality.setTextSize(16);
            specilizationScrollViewLL.addView(newSpeciality);
        } else {
            for (int i = 0; i < specializationSubCategories.size(); i++) {
                String value = specializationSubCategories.get(i);
                TextView newSpeciality = new TextView(getContext());
                newSpeciality.setPadding(10, 10, 10, 10);
                newSpeciality.setLayoutParams(llp);
                newSpeciality.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.white));
                newSpeciality.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.round_bg_app_color));
                newSpeciality.setText(value);
                newSpeciality.setTextSize(16);
                specilizationScrollViewLL.addView(newSpeciality);
                selectedSpecializations.add(specializationCategories.get(i));
            }
        }
    }


    //display the profession data fetched from dialog
    private void addProfessionView(String workingAt, String experience) {
        if (experience.equals(""))
            experience = "0 years";
        //adding textview dynamically each time doctor adds the professional detail
        TextView newProfessional = new TextView(getContext());
        newProfessional.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.black));
        newProfessional.setPadding(10, 10, 10, 10);
        newProfessional.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.border_box));
        newProfessional.setText(Html.fromHtml(workingAt
                + "<br/>" + "Experience : " + experience));
        newProfessional.setTextSize(16);
        professionalLL.addView(newProfessional);
        professionClearTV.setVisibility(View.VISIBLE);
        ProfessionModel qualificationModel = new ProfessionModel();
        qualificationModel.setOrganization(workingAt);
        qualificationModel.setExperience(experience);
        professionList.add(qualificationModel);
    }

    //method to show the specialization list
    private void showSpecializations() {
        Intent intent = new Intent(getActivity(), SpecializationActivity.class);
        //  intent.putExtra("selected_specializations",selectedSpecializations);
        intent.putExtra("categories", specializationCategories);
        intent.putExtra("sub_categories", specializationSubCategories);
        startActivityForResult(intent, Constants.CATEGORY_RESULT);
    }

    //dialog to add professional experiences
    @SuppressLint("CommitTransaction")
    private void addProfessionalDialog(String source) {
        FragmentManager fragmentManager = getFragmentManager();
        EditInfoDialogFragment editInfoDialogFragment1 = EditInfoDialogFragment.newInstance(source, this, degreeList);
        editInfoDialogFragment1.show(fragmentManager, TAG);
    }


    //dialog to select languages
    @SuppressLint("CommitTransaction")
    private void showLanguageOptions() {
        LanguageDialogFragment languageDialogFragment = LanguageDialogFragment.newInstance();
        languageDialogFragment.setTargetFragment(this, LANGUAGE_FRAGMENT);
        languageDialogFragment.show(getFragmentManager().beginTransaction(), TAG);
    }

    //method to set the gender value
    private void selectGender(String gender) {
        if (gender.equals(getString(R.string.male))) {
            genderSelected = gender;
            maleTV.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.gender_selected));
            maleTV.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.white));
            femaleTV.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.gender_unselected));
            femaleTV.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.grey_text));
        } else {
            genderSelected = gender;
            femaleTV.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.gender_selected));
            femaleTV.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.white));
            maleTV.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.gender_unselected));
            maleTV.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.grey_text));
        }
    }
    //when


    //method called when next button is clicked
    //setting all the data in the model and sending to documents model
    private void nextButtonPressed() {
        String duns = mciNumberET.getText().toString();
        String experience = experienceET.getText().toString();
        String bio = aboutYourselfET.getText().toString();

        basicProfileUpdateModel.setId(SharePref.getDoctorID(getContext()));
        basicProfileUpdateModel.setCategory(specializationCategories);
        basicProfileUpdateModel.setSpecialization(specializationSubCategories);
        basicProfileUpdateModel.setDuns(duns);
        basicProfileUpdateModel.setGender(genderSelected);
        basicProfileUpdateModel.setExperience(experience);
        basicProfileUpdateModel.setBio(bio);
        basicProfileUpdateModel.setLanguage(languagesSelected);
        basicProfileUpdateModel.setQualification(educationList);
        basicProfileUpdateModel.setWorksFor(professionList);

        if (specializationCategories.size() != 0 && specializationSubCategories.size() != 0 && !experience.equals("")) {
            //set the interface method value to change th status indicator at the top
            indicatorListerner.changeIndicatorStatus(1);
            UploadDocumentFragment uploadDocumentFragment1 = UploadDocumentFragment.newInstance(basicProfileUpdateModel, Constants.BASIC_PROFILE, null);
            FragmentsActionUtility.replaceFragment(uploadDocumentFragment1, R.id.profile_confile_frame, getFragmentManager());
        } else {
            specialisationTV.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.red));
            experienceET.setError(getString(R.string.enterExErrorTV));
            Toast.makeText(getContext(), "Please enter the mandatory fields..", Toast.LENGTH_SHORT).show();
        }
    }

    //method to select or click an image
    private void choosePhotoAlertDialog() {
        CharSequence[] items = {getContext().getString(R.string.choosePhoto), getContext().getString(R.string.takephoto)};
        new AlertDialog.Builder(getContext())
                .setTitle("Select your option")
                .setNegativeButton(getContext().getString(R.string.cancelTxt), null)
                .setItems(items, (dialog, which) -> {
                    if (which == 0) {
                        if (!AndroidVersionUtility.isLollipopOrLess()) {
                            if (checkPermission(getActivity(), Constants.REQUEST_GALLERY_STORAGE)) {
                                chooseImage();
                            }
                        } else chooseImage();
                    } else if (which == 1) {
                        if (!AndroidVersionUtility.isLollipopOrLess()) {
                            if (checkPermission(getActivity(), Constants.REQUEST_CAMERA_STORAGE)) {
                                takeImage();
                            }
                        } else takeImage();
                    } else {
                        dialog.dismiss();
                    }
                }).show();
    }

    //method to pick an image from gallery
    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select file"), Constants.SELECT_ANY_FILE);
    }

    private void takeImage() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Constants.TAKE_IMAGE);
//            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            // Ensure that there's a camera activity to handle the intent
//            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//                // Create the File where the photo should go
//                File photoFile;
//                photoFile = createImageFile();
//                // Continue only if the File was successfully created
//                if (photoFile != null) {
//                    Uri photoURI;
//                    if (Build.VERSION.SDK_INT <= 23) {
//                        photoURI = Uri.fromFile(createImageFile());
//                    } else {
//                        photoURI = FileProvider.getUriForFile(getActivity(),
//                                BuildConfig.APPLICATION_ID + ".provider",
//                                createImageFile());
//                    }
//                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                    startActivityForResult(takePictureIntent, Constants.TAKE_IMAGE);
//                }
        } catch (Exception ex) {
            Log.e("Error", ex.getMessage());
        }
    }

    //method to click an image
//    private void takeImage() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, Constants.TAKE_IMAGE);
//    }

    //method to show the language list
    private void updateLanguages() {
        //adding a text dynamically when doc selects the language
        languagesScrollViewLL.removeAllViews();
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(10, 10, 10, 10);

        if (languagesSelected.size() <= 0) {
            TextView newLanguage = new TextView(getContext());
            newLanguage.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.black));
            newLanguage.setPadding(10, 0, 10, 0);
            newLanguage.setText(getString(R.string.nolanguagetxt));
            newLanguage.setTextSize(16);
            languagesScrollViewLL.addView(newLanguage);
        } else {
            for (int i = 0; i < languagesSelected.size(); i++) {
                String value = languagesSelected.get(i);
                TextView newLanguage = new TextView(getContext());
                newLanguage.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.white));
                newLanguage.setPadding(10, 0, 10, 0);
                newLanguage.setLayoutParams(llp);
                newLanguage.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.round_bg_app_color));
                newLanguage.setText(value);
                newLanguage.setTextSize(16);
                languagesScrollViewLL.addView(newLanguage);
            }
        }
    }

    // for all the clicks
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                nextButtonPressed();
                break;
            case R.id.btn_skip_config:
                getContext().startActivity(new Intent(getActivity(), HomeActivity.class));
                break;
            case R.id.profile_specialization:
                showSpecializations();
                break;
            case R.id.img_edit_profile:
                choosePhotoAlertDialog();
                break;
            case R.id.profile_gender_female:
                selectGender("Female");
                break;
            case R.id.profile_gender_male:
                selectGender("Male");
                break;
            case R.id.languages_text:
                showLanguageOptions();
                break;
            case R.id.profile_profession_tapaddTV:
                addProfessionalDialog(Constants.PROFESSION);
                break;
            case R.id.profile_profession_clearTV:
                professionalLL.removeAllViews();
                professionClearTV.setVisibility(View.INVISIBLE);
                break;
            case R.id.profile_education_tapaddTV:
                addProfessionalDialog(Constants.EDUCATION);
                break;
            case R.id.profile_education_clearTV:
                educationLL.removeAllViews();
                clearEducationTV.setVisibility(View.INVISIBLE);
                //   addProfessionalDialog(Constants.PROFESSION);
                break;

        }
    }


    //interface method called once we get the url fom firebase
    @Override
    public void getDownloadUrl(String url) {
        Log.i("Tag url", url);
        profilePicURL = url;
        basicProfileUpdateModel.setProfilePic(profilePicURL);

    }

    @Override
    public void getDownloadUrlType(String url, String type) {

    }

    @Override
    public void getDeleteResponse(String url, String tag) {

    }

    @Override
    public void getSummaryData(String summary) {
    }

    @Override
    public void getProfessionalData(Bundle professionData) {
        String organization = professionData.getString("working_at");
        String experience = professionData.getString("experience");
        addProfessionView(organization, experience);
    }

    @Override
    public void getEducationData(Bundle educationData) {
        String organization = "", additionalData = "";
        QualificationModel qualificationModel = new QualificationModel();
        if (educationData.getString("organization") != null) {
            organization = educationData.getString("organization");
        }
//         = (!(educationData.getString("organization").equals("")) ? educationData.getString("organization") : "Nil");
        String type = educationData.getString("type");
        String degree = educationData.getString("degree");
        if (educationData.getString("additional_data") != null) {
            additionalData = educationData.getString("additional_data");
        }
//        String additionalData = (!(educationData.getString("additional_data").equals("")) ? educationData.getString("additional_data") : "Nil");
        String yearTo = educationData.getString("yearTo");
        String yearFrom = educationData.getString("yearFrom");
        TextView newProfessional = new TextView(getContext());
        newProfessional.setTextColor(AndroidVersionUtility.getColor(getContext(), R.color.black));
        newProfessional.setPadding(10, 10, 10, 10);
        newProfessional.setBackground(AndroidVersionUtility.getDrawable(getContext(), R.drawable.border_box));
        newProfessional.setText(Html.fromHtml("<b>" + organization + "</b>"
                + "<br/>" + type + " - " + degree + " - " + additionalData
                + "<br/>" + yearFrom + "-" + yearTo
        ));
        newProfessional.setTextSize(16);
        educationLL.addView(newProfessional);
        clearEducationTV.setVisibility(View.VISIBLE);
        qualificationModel.setSpecialization(degree);
        qualificationModel.setType(type);
        qualificationModel.setInstitute(organization);
        qualificationModel.setYearTo(yearTo);
        qualificationModel.setYearFrom(yearFrom);
        qualificationModel.setAdditionalData(additionalData);
        educationList.add(qualificationModel);

    }

    @Override
    public void getBankDetails(Bundle bankData) {

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CAMERA_STORAGE: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for all permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        // Log.d(TAG, "Camera and storage permission granted");
                        takeImage();
                    } else {
                        // Log.d(TAG, "Some permissions are not granted ask again");
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getContext(), "Go to App settings and enable Camera and Storage permissions", Toast.LENGTH_LONG).show();
                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
            break;
            case Constants.REQUEST_GALLERY_STORAGE:
                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for all permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        // Log.d(TAG, "Camera and storage permission granted");
                        chooseImage();
                        // process the normal flow   //else any one or both the permissions are not granted
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getContext(), "Go to App settings and enable Camera and Storage permissions", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                break;
            default:
                break;
        }

    }

    private boolean checkPermission(Context context, final int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionSendMessage = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
            int locationPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int readPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
            List<String> listPermissionsNeeded = new ArrayList<>();
            if (locationPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (!listPermissionsNeeded.isEmpty()) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), requestCode);
                return false;
            }
        }
        return true;
    }
}
