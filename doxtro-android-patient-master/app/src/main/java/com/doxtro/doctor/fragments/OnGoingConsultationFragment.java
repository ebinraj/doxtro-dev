package com.doxtro.doctor.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.OpenCloseConsultationAdapter;
import com.doxtro.doctor.model.ConsultationRequestModel;
import com.doxtro.doctor.model.ConsultationsDataModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.SharePref;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yashaswi on 05/09/17.
 */

@SuppressWarnings("unchecked")
public class OnGoingConsultationFragment extends Fragment {
    @BindView(R.id.open_close_consulatation_RV)
    RecyclerView openRV;
    @BindView(R.id.noConsultsTV)
    TextView noConsultTV;

    public OnGoingConsultationFragment() {
        //empty constructor
    }

    private List<ConsultationsDataModel> consultations = new ArrayList<>();

    public static int PAGE_SIZE = 0;
    private String skip = "0";
    private String limit = "20";

    private boolean isLoading = false;
    private boolean isLastPage = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_open_close_consultations, container, false);
        ButterKnife.bind(this, view);
        fetchConsultations(); // --> starting it wil load first 20 data
        return view;
    }
    static HashMap<String, Object> unreadMsgMap = new HashMap<>();
    private void fetchConsultations() {
        openRV.removeAllViews();
        isLoading = true;
        String docId = SharePref.getDoctorID(getActivity());
        ApiInterface apiService = ApiClient.getClient(getActivity()).create(ApiInterface.class);

        Call<ConsultationRequestModel> call = apiService.getConsultations(docId, skip, limit, "Ongoing");
        call.enqueue(new Callback<ConsultationRequestModel>() {
            @Override
            public void onResponse(Call<ConsultationRequestModel> call, Response<ConsultationRequestModel> response) {
                try {
                    isLoading = false;
                    DialogUtils.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        /*Load list primary -->Azhar*/
                        if (response.body().getData().size() > 0) {

                            consultations.addAll(response.body().getData());

                            PAGE_SIZE = response.body().getTotalCount();
                            isLastPage = consultations.size() >= PAGE_SIZE;

                            /*Load list primary -->Azhar*/
                            if (consultations.size() > 0) {
                                noConsultTV.setVisibility(View.GONE);
                                initRV();
                            }

                            /*Load count secondary -->Azhar*/
                            for (int i = 0; i < consultations.size(); i++) {
                                String consultStatus = consultations.get(i).getStatus();
                                final String consultationId = consultations.get(i).getId();

                                if (consultStatus.equalsIgnoreCase(Constants.ONGOING)
                                        || consultStatus.equalsIgnoreCase(Constants.STATUS_COMPLETED)
                                        || consultStatus.equalsIgnoreCase(Constants.CLOSED)) {
                                    Log.e("CON-STATUS:", consultationId + " : " + consultStatus);
                                    DatabaseReference messagesReference = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_STORAGE).child(consultationId);
                                    messagesReference.keepSynced(true);
                                    messagesReference.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            try {
                                                if (dataSnapshot.getValue() != null) {
                                                    HashMap<String, Object> chatHistory = (HashMap<String, Object>) dataSnapshot.getValue();
                                                    int unreadMsgCount = 0;
                                                    for (String key : chatHistory.keySet()) {
                                                        HashMap<String, Object> chatDetails = (HashMap<String, Object>) chatHistory.get(key);
                                                        String msgType = chatDetails.get(Constants.MSG_TYPE).toString();
                                                        if (msgType.equalsIgnoreCase(Constants.HEALTH_RECORD_MSGTYPE)
                                                                || msgType.equalsIgnoreCase(Constants.USER_TEXT)
                                                                || msgType.equalsIgnoreCase(Constants.IMAGE)
                                                                || msgType.equalsIgnoreCase(Constants.TYPE_DOCUMENT)
                                                                || msgType.equalsIgnoreCase(Constants.TYPE_CALL_REQUEST))
                                                            if (chatDetails.containsKey("status")) {
                                                                if (!chatDetails.get("status").toString().equalsIgnoreCase("read")) {
                                                                    unreadMsgCount++;
                                                                    unreadMsgMap.put(consultationId, unreadMsgCount);
                                                                }
                                                            }
                                                    }
                                                    initRV();
                                                    Log.e("MSG-COUNT", consultationId + ":" + unreadMsgCount);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            Log.e("Error", databaseError.getMessage());
                                        }
                                    });
                                }
                            }
                        } else {
                            skip = String.valueOf(Integer.parseInt(skip) - Integer.parseInt(limit));
                        }
                    } else {
                        noConsultTV.setVisibility(View.VISIBLE);
                        DialogUtils.appToast(getActivity(), "Consultation false state ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ConsultationRequestModel> call, Throwable t) {
                // Log error here since request failed
                DialogUtils.dismissProgressDialog();
            }
        });
    }

    private void initRV() {
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        openRV.setLayoutManager(layoutManager);
        OpenCloseConsultationAdapter consultationAdapter =
                new OpenCloseConsultationAdapter(getContext(), consultations, unreadMsgMap);
        openRV.setAdapter(consultationAdapter);

        //show bottom when ever update api after first time
        if (!skip.equalsIgnoreCase("0")) {
            openRV.scrollToPosition(openRV.getAdapter().getItemCount() - Integer.parseInt(limit));
        }

        //pagination
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) openRV
                .getLayoutManager();
        openRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount < PAGE_SIZE) {
                        skip = String.valueOf(Integer.parseInt(skip) + Integer.parseInt(limit));
                        fetchConsultations();
                    }
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 106 || requestCode == 105) {
                Log.e("refresh", "refresh");
                consultations.clear();
                skip = "0";
                unreadMsgMap.clear();
                fetchConsultations();
                //  Toast.makeText(getActivity(), "refresh", Toast.LENGTH_SHORT).show();
            }
        }
    }
}


