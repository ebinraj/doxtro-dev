package com.doxtro.doctor.server;

import android.content.Context;

import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.SharePref;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ghanshyamnayma on 13/07/17.
 */

public class ApiClient {
    public static final String BASE_URL = Constants.BASE_URL_STAGE;
    private static Retrofit retrofit = null;

    public static Retrofit getClient(Context context) {
        final String BASE_URL = Constants.BASE_URL_PROD;
        Retrofit retrofit = null;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .addHeader(Constants.CONTENT_TYPE, Constants.CONTENT_TYPE_JSON)
                    .addHeader(Constants.AUTHORIZATION, SharePref.getToken(context))
                    .build();
            return chain.proceed(request);
        });
        httpClient.addInterceptor(interceptor);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        OkHttpClient client = httpClient.build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getExotelClient(Context context) {
        String BASE_URL_EXOTEL = Constants.BASE_EXOTEL_URL;
        Retrofit retrofit1 = null;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            String credentials = Credentials.basic(Constants.EXOTEL_SID, Constants.EXOTEL_TOKEN);
            Request request = chain.request();
            Request authenticatedRequest = request.newBuilder()
                    .header("Authorization", credentials).build();
            return chain.proceed(authenticatedRequest);
        });
        httpClient.addInterceptor(interceptor);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        OkHttpClient client = httpClient.build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        if (retrofit1 == null) {
            retrofit1 = new Retrofit.Builder()
                    .baseUrl(BASE_URL_EXOTEL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit1;
    }

}

