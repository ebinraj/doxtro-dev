package com.doxtro.doctor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.DiagnosticModel;
import com.doxtro.doctor.model.MedicationModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yashaswi on 18/09/17.
 */

public class PrescriptionAdapter extends RecyclerView.Adapter<PrescriptionAdapter.MyViewHolder> {
    private String source = "";
    private final Context mContext;
    private List<MedicationModel> prescriptionList = new ArrayList<>();
    private List<DiagnosticModel> diagnosticList = new ArrayList<>();
    private final Button viewBtn, sendBtn;
    private final String durationList[] = {"Days", "Weeks", "Months"};
    private List<String> prescriptionTime;
    private String durationUnit = "";
    private String durationValue = "";
    private String instructionTxt = "";
    private String tabName = "";
    private static int index = 0;



    public PrescriptionAdapter(Context context, String source, List<DiagnosticModel> diagnostics, List<MedicationModel> prescriptions, Button sendBtn, Button viewSaveBtn) {
        mContext = context;
        if (prescriptions != null)
            prescriptionList = prescriptions;
        if (diagnostics != null)
            diagnosticList = diagnostics;
        viewBtn = viewSaveBtn;
        this.source = source;
        this.sendBtn = sendBtn;
    }

    @Override
    public PrescriptionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.prescription_item_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PrescriptionAdapter.MyViewHolder holder, int position) {
        if (source.equals(Constants.PRESCRIPTION)) {
            if (holder == null && prescriptionList == null && prescriptionList.isEmpty())
                return;
            prescriptionTime = null;
            MedicationModel medicationModel = prescriptionList.get(position);
            holder.name.setText(medicationModel.getName());
            holder.beforeMealCB.setOnCheckedChangeListener(null);
            if (medicationModel.getFoodWarning().contains(mContext.getString(R.string.beforeMealTxt))) {
                holder.beforeMealCB.setChecked(true);
            } else {
                holder.beforeMealCB.setChecked(false);
            }
            holder.afterMealCB.setOnCheckedChangeListener(null);
            if (medicationModel.getFoodWarning().contains(mContext.getString(R.string.afterMealTxt))) {
                holder.afterMealCB.setChecked(true);
            } else {
                holder.afterMealCB.setChecked(false);
            }
            holder.instructionET.setOnTouchListener((v, event) -> {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            });
            holder.instructionET.setText(medicationModel.getInstruction());
            if (medicationModel.getDoseTime().contains("Morning")) {
                holder.mrngBtn.setSelected(true);
                holder.mrngBtn.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
            } else {
                holder.mrngBtn.setSelected(false);
            }
            if (medicationModel.getDoseTime().contains("Afternoon")) {
                holder.aftBtn.setSelected(true);
                holder.aftBtn.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
            } else {
                holder.aftBtn.setSelected(false);
            }
            if (medicationModel.getDoseTime().contains("Night")) {
                holder.nightBtn.setSelected(true);
                holder.nightBtn.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
            } else {
                holder.nightBtn.setSelected(false);
            }
            ArrayAdapter<String> myAdapter1 = new ArrayAdapter<>(mContext, android.R.layout.simple_list_item_1, durationList);
            myAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.durationSpinner.setAdapter(myAdapter1);
            holder.durationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    durationUnit = parent.getItemAtPosition(position).toString();
                    if (!durationValue.equals("")) {
                        medicationModel.setDuration(Integer.parseInt(durationValue));
                    }
                    medicationModel.setDurationUnit(durationUnit);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            if (medicationModel.getDurationUnit() != null) {
                holder.durationET.setText(String.valueOf(medicationModel.getDuration()));
                if (medicationModel.getDurationUnit().equalsIgnoreCase("Days")) {

                    int pos = myAdapter1.getPosition("Days");

                    holder.durationSpinner.setSelection(pos);
                } else if (medicationModel.getDurationUnit().equalsIgnoreCase("Weeks")) {
                    int pos = myAdapter1.getPosition("Weeks");
                    holder.durationSpinner.setSelection(pos);
                } else if(medicationModel.getDurationUnit().equalsIgnoreCase("Months")) {
                    int pos = myAdapter1.getPosition("Months");
                    holder.durationSpinner.setSelection(pos);
                }else
                {
                    int pos = myAdapter1.getPosition("Days");
                    holder.durationSpinner.setSelection(pos);
                }

            }

            holder.durationET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    durationValue = s.toString();
                    if (durationValue.equals("")) return;
                    medicationModel.setDuration(Integer.parseInt(durationValue));
                    medicationModel.setDurationUnit(durationUnit);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    durationValue = s.toString();
                    if (durationValue.equals("")) return;
                    medicationModel.setDuration(Integer.parseInt(durationValue));
                    medicationModel.setDurationUnit(durationUnit);

                }
            });
            holder.instructionET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    instructionTxt = s.toString();
                    if (!instructionTxt.equals("")) {
                        medicationModel.setInstruction(instructionTxt);
                    } else
                        medicationModel.setInstruction("");
                }

                @Override
                public void afterTextChanged(Editable s) {
                    instructionTxt = s.toString();
                    if (!instructionTxt.equals("")) {
                        medicationModel.setInstruction(instructionTxt);
                    } else
                        medicationModel.setInstruction("");
                }
            });
            holder.name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tabName = s.toString();
                    medicationModel.setName(tabName);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    tabName = s.toString();
                    medicationModel.setName(tabName);
                }
            });
            holder.cancelIV.setOnClickListener(v -> {
                prescriptionList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, getItemCount());
                notifyDataSetChanged();
                if (prescriptionList.size() == 0) {
                    viewBtn.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.box_gray));
                    sendBtn.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.box_gray));
                    viewBtn.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                    sendBtn.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                }
            });
            holder.editIV.setOnClickListener(v -> {
                holder.name.setFocusable(true);
                holder.name.setCursorVisible(true);

            });


            holder.beforeMealCB.setOnCheckedChangeListener((buttonView, isChecked) -> {
//                    if (isChecked) {
//                        foodWarningList.add(mContext.getString(R.string.beforeMealTxt));
//                    } else {
//                        foodWarningList.remove(mContext.getString(R.string.beforeMealTxt));
//                    }
                holder.beforeMealCB.setChecked(isChecked);
//                    holder.beforeMealCB.setChecked(!isChecked);
//
                medicationModel.setBefore(isChecked);
                if (isChecked) {
                    medicationModel.setFoodWarning(mContext.getString(R.string.beforeMealTxt));
                } else {
                    medicationModel.setFoodWarning("");
                }
//                    if (foodWarningList.size() == 2) {
//                        String foodwarning = Reusable.arrayListToString(foodWarningList, " , ");
//                        medicationModel.setFoodWarning(foodwarning);
//                    } else  if(foodWarningList.size() == 1){
//                        if (foodWarningList.contains(mContext.getString(R.string.beforeMealTxt)))
//                            medicationModel.setFoodWarning(mContext.getString(R.string.beforeMealTxt));
//                        else
//                            medicationModel.setFoodWarning(mContext.getString(R.string.afterMealTxt));
//                    }
            });
            holder.afterMealCB.setOnCheckedChangeListener((buttonView, isChecked) -> {
//                    if (isChecked) {
//                        foodWarningList.add(mContext.getString(R.string.afterMealTxt));
//                    } else {
//                        foodWarningList.remove(mContext.getString(R.string.afterMealTxt));
//                    }
                holder.afterMealCB.setChecked(isChecked);
//                    medicationModel.setFoodWarning(mContext.getString(R.string.afterMealTxt));
                medicationModel.setAfter(isChecked);
                if (isChecked) {
                    medicationModel.setFoodWarning(mContext.getString(R.string.afterMealTxt));
                } else {
                    medicationModel.setFoodWarning("");
                }
//                    if (foodWarningList.size() == 2) {
//                        String foodwarning = Reusable.arrayListToString(foodWarningList, " , ");
//                        medicationModel.setFoodWarning(foodwarning);
//                    } else  if(foodWarningList.size() == 1){
//                        if (foodWarningList.contains(mContext.getString(R.string.afterMealTxt)))
//                            medicationModel.setFoodWarning(mContext.getString(R.string.afterMealTxt));
//                        else
//                            medicationModel.setFoodWarning(mContext.getString(R.string.beforeMealTxt));
//                    }

            });


            holder.mrngBtn.setOnClickListener(v -> {
                addPrescriptionTime(holder.mrngBtn, "Morning", position);
            });
            holder.aftBtn.setOnClickListener(v -> addPrescriptionTime(holder.aftBtn, "Afternoon", position));
            holder.nightBtn.setOnClickListener(v -> addPrescriptionTime(holder.nightBtn, "Night", position));
//            if (prescriptionTime == null) {
//                prescriptionTime = new ArrayList<>();
////                if (prescriptionTime.isEmpty()) {
////                    prescriptionTime.add(0, "");
////                    prescriptionTime.add(1, "");
////                    prescriptionTime.add(2, "");
////                    prescriptionList.get(position).setDoseTime(prescriptionTime);
////                }
//
//            }

        } else {
            if (holder == null && diagnosticList == null && diagnosticList.isEmpty())
                return;
            DiagnosticModel diagnosticModel = diagnosticList.get(position);
            holder.foodWarningLL.setVisibility(View.GONE);
            holder.durationLL.setVisibility(View.GONE);
            holder.doseTimeLL.setVisibility(View.GONE);
            holder.name.setText(diagnosticModel.getName());
            holder.instructionET.setText(diagnosticModel.getInstruction());
            holder.instructionET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    instructionTxt = s.toString();
                    diagnosticModel.setInstruction(instructionTxt);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    instructionTxt = s.toString();
                    diagnosticModel.setInstruction(instructionTxt);
                }
            });
            holder.name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tabName = s.toString();
                    diagnosticModel.setName(tabName);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    tabName = s.toString();
                    diagnosticModel.setName(tabName);
                }
            });
            holder.cancelIV.setOnClickListener(v -> {
                diagnosticList.remove(position);
                notifyItemRemoved(position);
                //  notifyItemRangeChanged(position, getItemCount());
                notifyDataSetChanged();

//                prescriptionList.remove(position);
//                notifyItemRemoved(position);
//              //  notifyItemRangeChanged(position, getItemCount());
//                notifyDataSetChanged();
                if (diagnosticList.size() == 0) {
                    viewBtn.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.box_gray));
                    sendBtn.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.box_gray));
                    viewBtn.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                    sendBtn.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                }
            });
            holder.editIV.setOnClickListener(v -> {
                holder.name.setFocusable(true);
                holder.name.setCursorVisible(true);

            });


        }
    }

    private void addPrescriptionTime(Button button, String time, int position) {
//        if (prescriptionTime == null) {
//            prescriptionTime = new ArrayList<>();
//        }
        if (prescriptionTime == null) {
            prescriptionTime = new ArrayList<>();
        }
        if(prescriptionList.get(position).getDoseTime()!=null && prescriptionList.get(position).getDoseTime().size()>0)
        {
            prescriptionTime=prescriptionList.get(position).getDoseTime();
        }
//
        if (!button.isSelected()) {
            button.setSelected(true);
            button.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.white));
            switch (time) {
                case "Morning":
                    //  prescriptionTime.remove(0);
                    prescriptionTime.add(time);
                    break;
                case "Afternoon":
                    //    prescriptionTime.remove(1);
                    prescriptionTime.add(time);
                    break;
                case "Night":
                    //  prescriptionTime.remove(2);
                    prescriptionTime.add(time);
                    break;
            }
            //  prescriptionTime.add(time);
        } else {
            button.setSelected(false);
            button.setTextColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
            if (prescriptionTime.contains(time)) {
                index = prescriptionTime.lastIndexOf(time);
                prescriptionTime.remove(index);
//                prescriptionTime.set(index, "");
            }
        }
        if(prescriptionTime.size()==3)
        {
            prescriptionTime.clear();
            prescriptionTime.add("Morning");
            prescriptionTime.add("Afternoon");
            prescriptionTime.add("Night");
        }else if(prescriptionTime.size()==2 && prescriptionTime.contains("Morning") && prescriptionTime.contains("Afternoon"))
        {
            prescriptionTime.clear();
            prescriptionTime.add("Morning");
            prescriptionTime.add("Afternoon");
        }else if(prescriptionTime.size()==2 && prescriptionTime.contains("Morning") && prescriptionTime.contains("Night"))
        {
            prescriptionTime.clear();
            prescriptionTime.add("Morning");
            prescriptionTime.add("Night");
        }else if(prescriptionTime.size()==2 && prescriptionTime.contains("Afternoon") && prescriptionTime.contains("Night"))
        {
            prescriptionTime.clear();
            prescriptionTime.add("Afternoon");
            prescriptionTime.add("Night");
        }
        prescriptionList.get(position).setDoseTime(prescriptionTime);

    }

    @Override
    public int getItemCount() {
        return ((source.equals(Constants.PRESCRIPTION)) ? prescriptionList.size() : diagnosticList.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.prescription_item_name)
        EditText name;
        @BindView(R.id.prescription_item_edit)
        ImageView editIV;
        @BindView(R.id.prescription_item_cancel)
        ImageView cancelIV;
        @BindView(R.id.prescription_duration_spinner)
        Spinner durationSpinner;
        @BindView(R.id.prescription_before_checkbox)
        CheckBox beforeMealCB;
        @BindView(R.id.prescription_after_checkbox)
        CheckBox afterMealCB;
        @BindView(R.id.prescription_mor)
        Button mrngBtn;
        @BindView(R.id.prescription_aft)
        Button aftBtn;
        @BindView(R.id.prescription_nig)
        Button nightBtn;
        @BindView(R.id.prescription_duration_ET)
        EditText durationET;
        @BindView(R.id.prescription_instruction_Et)
        EditText instructionET;
        @BindView(R.id.prescription_foodWarningLL)
        LinearLayout foodWarningLL;
        @BindView(R.id.prescription_durationLL)
        LinearLayout durationLL;
        @BindView(R.id.prescription_doseTimeLL)
        LinearLayout doseTimeLL;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.setIsRecyclable(false);
        }
    }
}
