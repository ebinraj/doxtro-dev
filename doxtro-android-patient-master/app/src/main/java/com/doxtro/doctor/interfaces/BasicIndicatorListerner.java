package com.doxtro.doctor.interfaces;

/**
 * Created by yashaswi on 18/08/17.
 */

public interface BasicIndicatorListerner {

    public void changeIndicatorStatus(int status);
}
