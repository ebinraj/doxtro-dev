package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 19/09/17.
 */

public class FirebasePrescriptionModel implements Serializable {
    @SerializedName("doctorProfile")
    private DoctorProfileModel doctorProfile;

    @SerializedName("medication")
    private List<MedicationModel> medication=new ArrayList<>();

    @SerializedName("prescriptionId")
    private String prescriptionId;

    @SerializedName("diagnosticId")
    private String diagnosticId;

    @SerializedName("diagnostic")
    private List<DiagnosticModel> diagnostic = new ArrayList<>();

    @SerializedName("additionalDetails")
    private PatientDetailModel additionalDetails;

    public DoctorProfileModel getDoctorProfile() {
        return doctorProfile;
    }

    public void setDoctorProfile(DoctorProfileModel doctorProfile) {
        this.doctorProfile = doctorProfile;
    }

    public List<MedicationModel> getMedication() {
        return medication;
    }

    public void setMedication(List<MedicationModel> medication) {
        this.medication = medication;
    }

    public String getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(String prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public PatientDetailModel getAdditionalDetails() {
        return additionalDetails;
    }

    public void setAdditionalDetails(PatientDetailModel additionalDetails) {
        this.additionalDetails = additionalDetails;
    }

    public List<DiagnosticModel> getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(List<DiagnosticModel> diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getDiagnosticId() {
        return diagnosticId;
    }

    public void setDiagnosticId(String diagnosticId) {
        this.diagnosticId = diagnosticId;
    }
}
