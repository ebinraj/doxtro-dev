package com.doxtro.doctor.model;

/**
 * Created by ghanshyamnayma on 13/07/17.
 */

public class LoginResponse {

    private String message;

    private Data data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }


    public class Data
    {
        private String _id;

        public String get_id ()
        {
            return _id;
        }

        public void set_id (String _id)
        {
            this._id = _id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [_id = "+_id+"]";
        }
    }
}
