package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 18/09/17.
 */

public class PrescriptionPostModel implements Serializable {
    @SerializedName("medication")
    private List<MedicationModel> medication = new ArrayList<>();

    @SerializedName("consultationId")
    private String consultationId;

    @SerializedName("type")
    private String type;

    @SerializedName("diagnostic")
    private List<DiagnosticModel> diagnostic = new ArrayList<>();

    public List<MedicationModel> getMedication() {
        return medication;
    }

    public void setMedication(List<MedicationModel> medication) {
        this.medication = medication;
    }

    public String getConsultationId() {
        return consultationId;
    }

    public void setConsultationId(String consultationId) {
        this.consultationId = consultationId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public List<DiagnosticModel> getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(List<DiagnosticModel> diagnostic) {
        this.diagnostic = diagnostic;
    }
}
