package com.doxtro.doctor.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.activities.ChatActivity;
import com.doxtro.doctor.custom.CircleImageView;
import com.doxtro.doctor.model.ConsultationsDataModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.SharePref;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by yashaswi on 30/08/17.
 */

public class ConsultationAdapter extends RecyclerView.Adapter<ConsultationAdapter.MyViewHolder> {
    private HashMap<String, Object> unreadMsgList = new HashMap<>();
    private final Context mContext;
    private String origin;
    private List<ConsultationsDataModel> consultationList = new ArrayList<>();
    private int count;
    private TextView noDataTV;

    public ConsultationAdapter(List<ConsultationsDataModel> data, Context context, String source, HashMap<String, Object> unreadMsgList, TextView nodataText) {
        mContext = context;
        origin = source;
        consultationList = data;
        if (nodataText != null)
            noDataTV = nodataText;
        this.unreadMsgList = unreadMsgList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v1 = inflater.inflate(R.layout.consultion_list_item, parent, false);
        return new MyViewHolder(v1);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (consultationList != null && consultationList.size() > 0 && consultationList.get(position).getRelativesId() != null) {
            ConsultationsDataModel data = consultationList.get(position);
            if (unreadMsgList != null && unreadMsgList.size() > 0) {
                for (String unreadMsgKey : unreadMsgList.keySet()) {
                    if (data.getId().equalsIgnoreCase(unreadMsgKey)) {
                        int unreadMsgCount = (int) unreadMsgList.get(unreadMsgKey);
                        if (unreadMsgCount > 0) {
                            holder.unreadCountTV.setVisibility(View.VISIBLE);
                            holder.unreadCountTV.setText(String.valueOf(unreadMsgCount));
                        } else {
                            holder.unreadCountTV.setVisibility(View.GONE);
                        }
                    } else {
                        holder.unreadCountTV.setVisibility(View.GONE);
                    }
                }
            } else {
                holder.unreadCountTV.setVisibility(View.GONE);
            }
            ConsultationsDataModel.RelativesModel relativesModel = consultationList.get(position).getRelativesId();
            String patientName = consultationList.get(position).getRelativesId().getName();
            holder.titleNameTV.setText(relativesModel.getName() + ", " + relativesModel.getGender() + ", " + relativesModel.getAge());
            holder.descTV.setText(consultationList.get(position).getNote());
            holder.accept.setOnClickListener((View v) -> respondToRequest(consultationList.get(position).getId(), Constants.ACCEPT, position, holder.itemView, patientName));
            holder.reject.setOnClickListener(v -> respondToRequest(consultationList.get(position).getId(), Constants.REJECT, position, holder.itemView, patientName));
            if (relativesModel.getGender().equals(Constants.GENDER_MALE))
                holder.profileImag.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.mipmap.ic_myself_male));
            else if (relativesModel.getGender().equals(Constants.GENDER_FEMALE))
                holder.profileImag.setImageDrawable(AndroidVersionUtility.getDrawable(mContext, R.mipmap.ic_myself_female));


        }
        if (origin.contains(Constants.MORE)) {
            holder.mainLayout.setBackground(AndroidVersionUtility.getDrawable(mContext, R.drawable.box_white));
        }
        if (origin.contains(Constants.HOME_RECENT)) {
            holder.actionMainLL.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra(Constants.CONSULTATION_ID, consultationList.get(position).getId());
                intent.putExtra(Constants.PATIENT_NAME, consultationList.get(position).getRelativesId().getName());
                intent.putExtra(Constants.CONSULTATION_TYPE, consultationList.get(position).getConsultationType());
                intent.putExtra(Constants.STATUS, consultationList.get(position).getStatus());
                ((Activity) mContext).startActivityForResult(intent, 105);
            });
        }

    }

    private void respondToRequest(String consultID, String responseType, int pos, View itemView, String patientName) {
        try {
            String doctorID = SharePref.getDoctorID(mContext);
            ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);
            Call<JsonObject> call = apiService.sendCosultationResponse(doctorID, consultID, responseType);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        if (response.isSuccessful()) {
                            DialogUtils.appToast(mContext, response.body().getAsJsonObject().get("message").getAsString());
                            Log.i("Response Status", response.message());

                            if (responseType.contains(Constants.ACCEPT)) {
                                Intent intent = new Intent(mContext, ChatActivity.class);
                                intent.putExtra(Constants.PATIENT_NAME, patientName);
                                intent.putExtra(Constants.STATUS, "");
                                intent.putExtra(Constants.CONSULTATION_TYPE, response.body().get("data").getAsJsonObject().get("consultationType").getAsString());
                                intent.putExtra(Constants.CONSULTATION_ID, response.body().get("data").getAsJsonObject().get("consultationId").getAsString());
                                ((Activity) mContext).startActivityForResult(intent, 105);
                                Log.i("Consultation ID", response.body().get("data").getAsJsonObject().get("consultationId").getAsString());
                                consultationList.remove(pos);
                                notifyDataSetChanged();
                                if (consultationList.size() == 0)
                                    noDataTV.setVisibility(View.VISIBLE);
                            } else if (responseType.contains(Constants.REJECT)) {
                                consultationList.remove(pos);
                                notifyDataSetChanged();
                                if (consultationList.size() == 0)
                                    noDataTV.setVisibility(View.VISIBLE);
                            }

                        } else {
                            DialogUtils.appToast(mContext, new JSONObject(response.errorBody().string()).getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                    DialogUtils.dismissProgressDialog();
                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    private void getConsultationDetails(final String consultationId, String patientName) {
//        ApiInterface apiService = ApiClient.getClient(mContext).create(ApiInterface.class);
//
//        Call<GetConsultDetailsModel> call = apiService.getConsultationsDetails(consultationId);
//        call.enqueue(new Callback<GetConsultDetailsModel>() {
//            @Override
//            public void onResponse(Call<GetConsultDetailsModel> call, Response<GetConsultDetailsModel> response) {
//                try {
//                    if (response.isSuccessful()) {
//                        Intent intent = new Intent(mContext, ChatActivity.class);
//                        intent.putExtra(Constants.PATIENT_NAME, patientName);
//                        intent.putExtra(Constants.STATUS, response.body().getData().);
//                        intent.putExtra(Constants.CONSULTATION_ID, response.body().get("data").getAsJsonObject().get("consultationId").getAsString());
//                        ((Activity) mContext).startActivityForResult(intent, 105);
//                        Log.i("Consultation ID", response.body().get("data").getAsJsonObject().get("consultationId").getAsString());
//                        consultationList.remove(pos);
//                        notifyDataSetChanged();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                DialogUtils.dismissProgressDialog();
//            }
//
//            @Override
//            public void onFailure(Call<GetConsultDetailsModel> call, Throwable t) {
//                // Log error here since request failed
//                Log.e("Error", t.toString());
//                DialogUtils.dismissProgressDialog();
//            }
//        });
//    }


    @Override
    public int getItemCount() {
        if (consultationList.size() > 2) {
            count = 2;
        } else {
            count = this.consultationList.size();
        }
        return ((origin.contains(Constants.MORE)) ? this.consultationList.size() : count);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.consultation_profileImage)
        CircleImageView profileImag;
        @BindView(R.id.consulation_name)
        TextView titleNameTV;
        @BindView(R.id.consulation_desc)
        TextView descTV;
        @BindView(R.id.consulation_accept)
        TextView accept;
        @BindView(R.id.consulation_reject)
        TextView reject;
        @BindView(R.id.consultation_item_main_LL)
        LinearLayout mainLayout;
        @BindView(R.id.consultation_actionLL)
        LinearLayout actionMainLL;
        @BindView(R.id.consultations_no_of_msgs)
        TextView unreadCountTV;


        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
