package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yashaswi on 17/08/17.
 */

public class QualificationModel {

    @SerializedName("college")
    private String institute;

    @SerializedName("type")
    private String type;

    @SerializedName("degree")
    private String specialization;

    @SerializedName("additionalData")
    private String additionalData;

    @SerializedName("yearTo")
    private String yearTo;

    @SerializedName("yearFrom")
    private String yearFrom;


    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String spec) {
        this.specialization = spec;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public String getYearTo() {
        return yearTo;
    }

    public void setYearTo(String yearTo) {
        this.yearTo = yearTo;
    }

    public String getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(String yearFrom) {
        this.yearFrom = yearFrom;
    }
}
