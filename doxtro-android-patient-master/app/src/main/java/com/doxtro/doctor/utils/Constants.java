package com.doxtro.doctor.utils;

/**
 * @author ghanshyamnayma on 10/05/17.
 * @file TODO: All constant used in the application which will be here
 * @copyright ABOVE Solutions India Pvt. Ltd.All Rights Reserved.
 */

public class
Constants {

    public static final String BASE_URL_DEV = "http://doxtrodev.above-inc.net/api/v1/";
    public static final String BASE_URL_QA = "http://doxtroqa.above-inc.net/api/v1/";
    public static final String BASE_URL_STAGE = "https://doxtrostaging.above-inc.net/api/v1/";
    public static final String BASE_URL_PROD="https://apps.doxtro.com/api/v1/";
    public static final String FIREBASE_STORAGE = "Consultations";
  //    public static final String FIREBASE_STORAGE = "Consultations_staging";

//complete exotel requirements
    public static final String BASE_EXOTEL_URL = "https://api.exotel.com/v1/Accounts/";
    public static final String EXOTEL_TOKEN = "67ac0d1b35a63d9147f2dfa18bcbfa366947c969";
    public static final String EXOTEL_SID = "doxtro";
    public static final String EXOTEL_CALLER_ID = "04439570780";



    public static final String SELECTED_SPECIALIZATION = "selectedSpecialization";
    public static final String DEFAULT_DATE_FORMAT = "dd MMM yyyy";
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String MOBILE = "mobile";
    public static final String DOXTRO = "Doxtro";

    public static final String TYPE_PICTURE = "Profile Pictures";
    public static final String DOCTOR = "Doctor";

    public static final String PRESCRIPTION_DATA = "Prescription Data";
    public static final String PRESCRIPTION_ID = "Presrciption ID";
    public static final String APP_TYPE = "doctor";
    public static final String DEVICE_ID = "deviceId";
    public static final String PHONE_TYPE = "android";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
    public static final String PENDING = "pending";
    public static final String ACCEPTED = "accepted";
    public static final String REJECTED = "rejected";
    public static final String READY = "ready";
    public static final String APPROVED = "approved";
    public static final String CONSULTATION_TYPE = "consultation_type";
    public static final String AUDIO_TYPE = "Audio";


    public static String SHARED_PREF_NAME = "USER_NAME";
    public static final String HEALTH_RECORD = "Health record";
    public static final String DOXTRO_CASH = "Doxtro cash";
    public static final String TRANSACTION_HISTORY = "Transaction history";
    public static final String REMINDERS = "Reminders";
    public static final String OFFERS = "Offers";
    public static final String REFER_AND_EARN = "Refer and earn";
    public static final String SETTINGS = "Settings";
    public static final String TERMS_AND_CONDITIONS = "Terms and conditions";
    public static final String HELP = "Help";
    public static final String ABOUT = "About";
    public static final String LOGOUT = "Logout";
    public static final String CONTENT_TYPE = "content-Type";
    public static final String CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String AUTHORIZATION = "Authorization";
    public static final String LOGIN = "Login";


    public static final String PROFILE_CONFIG = "Profile_Config";
    public static final String SPECIALIZATIONS = "Specializations";
    public static final String DOCTOR_DEATIL = "Doctor_Detail";
    public static final String UG_TYPE = "UG";
    public static final String PG_TYPE = "PG";
    public static final String TOKEN = "Token";
    public static final String ACCEPT = "accept";
    public static final String REJECT = "reject";
    public static final String ONGOING = "Ongoing";
    public static final String CLOSED = "Closed";
    public static final String COMPLETED = "Completed";
    public static final String GENDER_MALE = "Male";
    public static final String GENDER_FEMALE = "Female";
    public static final String TAB_SWITCH = "tab_switch";

    public static final String EDIT_INFO_FRAGMENT = "EDIT_INFO_FRAGMENT";
    public static final String FRGMENT_EDIT_DIALOG = "FRGMENT_EDIT_DIALOG";
    public static final String SUMMARY = "SUMMARY";
    public static final String PROFESSION = "PROFESSION";
    public static final String EDUCATION = "EDUCATION";
    public static final String SOURCE = "SOURCE";
    public static final String POSITION = "POSITION";
    public static final String ISEDIT = "ISEDIT";
    public static final String BASIC_PROFILE = "BASIC_PROFILE";
    public static final String EDIT_PROFILE = "EDIT_PROFILE";
    public static final String SELECT_MEDICAL_DOCUMENT = "MEDICAL_DOCUMENT";
    public static final String SELECT_ID = "ID_PROOF";
    public static final String MORE = "MORE";
    public static final String HOME = "HOME";
    public static final String HOME_REQUEST = "HOME_REQUEST";
    public static final String HOME_RECENT = "HOME_ALL_CONSULTS";
    public static final String SOURCE_ONGOING = "ONGOING";
    public static final String SOURCE_CLOSED = "CLOSED";
    public static final String URL = "URL";
    public static final String DOCTOR_NAME = "NAME";
    public static final String DOCTOR_DEGREE = "DEGREE";
    public static final String DOCTOR_REGNO = "REG NUMBER";
    public static final String PATIENT_DEATAIL = "PATIENT DEATAIL";
    public static final String TIME_STAMP = "TIME_STAMP";
    public static final String SEND = "SEND";
    //    public static final String VIEW = "VIEW";
    public static final String CHAT = "CHAT";
    public static final String EDIT_PRESCRIPTION = "EDIT_PRESCRIPTION";
    public static final String PATIENT_NAME = "PATIENT_NAME";
    public static final String PATIENT_AGE = "PATIENT_AGE";
    public static final String PATIENT_GENDER = "PATIENT_GENDER";
    public static final String DIAGNOSTICS = "DIAGNOSTICS";
    public static final String EDIT_DIAGNOSTIC = "EDIT_DIAGNOSTIC";
    public static final String PRESCRIPTION_VIEW = "PRESCRIPTION_VIEW";
    public static final String DIAGNOSTIC_VIEW = "DIAGNOSTIC_VIEW";
    public static final String CHAT_IMAGE = "CHAT_IMAGE";
    public static final String CHAT_DOCUMENTS = "CHAT_DOCUMENTS";
    public static final String MONTHLY_EARNING = "MONTHLY_EARNING";
    public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    public static final String BANK_NAME = "BANK_NAME";
    public static final String IFSC_CODE = "IFSC_CODE";
    public static final String ACC_HOLDER = "ACC_HOLDER";
    public static final String STATUS = "STATUS";
    public static final String PRESCRIPTIONC = "PRESCRIPTION";
    public static final String SOURCE_SIGN_IN = "SIGN_IN";
    public static final String SOURCE_SIGN_UP = "SIGN_UP";


    /* health records */
    public static final String HISTORY = "history";// medical history type
    public static final String REPORT = "report"; // diagnostic reports
    public static final String MEDICATION = "medication"; // current medications
    public static final String INTOLERANCE = "intolerance"; // alergy and drugs


    public static final String STATUS_COMPLETED = "completed";
    public static final String STATUS_CLOSED = "closed";
    public static final String STATUS_NEW = "new";


    public static final String MSG_TYPE = "msgType";
    public static final String TYPING = "typing";
    public static final String ALERT = "alert";
    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String DOCTOR_IMAGE = "docImage";
    public static final String USER_TEXT = "userText";
    public static final String PRESCRIPTION = "prescription";
    public static final String DIAGNOSTIC = "diagnostic";
    public static final String USER_INTRO = "userIntro";
    public static final String TYPE_DOCUMENT = "documents";
    public static final String DOCTOR_DOCUMENT = "docDocuments";
    public static final String HEALTH_RECORD_MSGTYPE = "healthRecords";
    public static final String TYPE_CALL_REQUEST = "requestCall";


    /*
  * User Profile Info
  * */
    public static final String isLoggedIn = "isLoggedIn";
    public static final String doctorID = "";
    public static final String fullName = "";
    public static final String mobile = "";
    public static final String age = "";
    public static final String gender = "";
    public static final String showProfile = "showProfile";
    public static final String mainSpecializations = "mainSpecializations";
    public static final String saveProfile = "saveProfile";
    public static final String token = "token";
    public static final String CONSULTATION_ID = "ConsultationId";
    public static final String RELATIVE_ID = "relativeID";


    // All the URLs
    public static final String signUp = "doctor/signUp";
    public static final String signIn = "doctor/signIn";
    public static final String otpVerify = "doctor/verify";
    public static final String specializationList = "specialization/getAllSpecialization";
    public static final String setProfile = "doctor/profile";
    public static final String getLanguages = "settings/getLanguages";
    public static final String setAvailability = "availability/create";
    public static final String getProfile = "doctor/getProfile";
    public static final String updateAvailabilityStatus = "doctor/updateAvailabilityStatus";
    public static final String getConsultationRequests = "consultation/getRequests";
    public static final String respondToRequest = "consultation/sendResponse";
    public static final String getAllConsultation = "consultation/getConsultations";
    public static final String getConsultationDetails = "consultation/getConsultationDetails";
    public static final String getAllDegrees = "settings/getDegree";
    public static final String createPrescription = "prescription/create";
    public static final String getAllPrescriptions = "prescription/find";
    public static final String updatePrescriptions = "prescription/update";
    public static final String closeConsultation = "consultation/updateStatus";
    public static final String updateDeviceID = "doctor/updateDeviceId";
    public static final String getAutoSuggestions = "prescription/autoSuggestion";
    public static final String getEarnings = "doctor/earnings";
    public static final String getSearchResults = "prescription/search";
    public static final String getWeeklyEarnings = "doctor/weeklyEarnings";
    public static final String getHealthRecord = "healthRecords/getAllRecords";
    public static final String exoticCallConnect = "doxtro//Calls/connect.json";


    //request code values
    public static final int SELECT_IMAGE = 0;
    public static final int TAKE_IMAGE = 1;
    public static final int SELECT_ANY_FILE = 2;
    public static final int CATEGORY_RESULT = 2;
    public static final int SUMMARY_DIALOG = 1;
    public static final int PROFESSIONAL_DIALOG = 6;
    public static final int LANGUAGE_RESULT = 5;
    public static final int PICK_MEDICAL_DOCUMENTS = 7;
    public static final int CLICK_MEDICAL_DOCUMENTS = 8;
    public static final int PICK_ID = 9;
    public static final int CLICK_ID = 10;
    public static final int ACTIVITY_CLOSE = 11;
    public static final int REQUEST_CAMERA_STORAGE = 101;
    public static final int REQUEST_GALLERY_STORAGE = 201;


    // for all the events to be tracked in appsflyer
    public static final String EVENT_SIGNUP_COMPLETED = "Completed Signup";
    public static final String EVENT_OTP_VERIFIED = "OTP Verified";
    public static final String EVENT_RECENT_CONSULTATION_CLICK = "Recent Consultations";
    public static final String EVENT_CHAT_SCREEN = "Chat Screen";
    public static final String EVENT_CHAT_VISITORS = "Chat visitors";
    public static final String EVENT_CONSULTATION_ACCEPTED = "Consultation Accepted";
    public static final String EVENT_EARNINGS = "Earnings";


    //for clevertap profile info

    public static final String KEY_NAME = "Name";
    public static final String KEY_AGE = "Age";
    public static final String KEY_EMAIL = "Email";
    public static final String KEY_GENDER = "Gender";
    public static final String KEY_MOBILE = "Mobile";


    //for appsflyer deeplink
    public static final String LINK_CHAT = "chat";
    public static final String LINK_DOCUMENTS = "documents";
    public static final String LINK_AVAILABILITY = "availability";
    public static final String LINK_BANKDETAILS = "bank_details";


}
