package com.doxtro.doctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashaswi on 18/10/17.
 */

public class WeeklyEarningsModel {
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<WeeklyEarningsDataModel> data = new ArrayList<>();
    @SerializedName("additionalDetails")
    private AdditionalDetails additionalDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WeeklyEarningsDataModel> getData() {
        return data;
    }

    public void setData(List<WeeklyEarningsDataModel> data) {
        this.data = data;
    }

    public AdditionalDetails getAdditionalDetails() {
        return additionalDetails;
    }


}

class AdditionalDetails {
    @SerializedName("month")
    private String month;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }


}

