package com.doxtro.doctor.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/*
*  can be used for SharedPreference Values
* */
public class PrefUtils {

    /**
     * Called to save supplied value in shared preferences against given key.
     * @param context Context of caller activity
     * @param  //Key of value to save against
     * @param value Value to save
     */
    public static void saveToPrefs(Context context, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.SHARED_PREF_NAME,value);
        editor.commit();
    }

    /**
     * Called to retrieve required value from shared preferences, identified by given key.
     * Default value will be returned of no value found or error occurred.
     * @param context Context of caller activity
     * @param //key Key to find value against
     * @param defaultValue Value to return if no data found against given key
     * @return Return the value found against given key, default if not found or any error occurs
     */
    public static String getFromPrefs(Context context, String defaultValue) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            return sharedPrefs.getString(Constants.SHARED_PREF_NAME, defaultValue);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }
    /**
     *
     * @param context Context of caller activity
     * @param //key Key to delete from SharedPreferences
     */
    public static void removeFromPrefs(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.remove(Constants.SHARED_PREF_NAME);
        editor.commit();
    }
}