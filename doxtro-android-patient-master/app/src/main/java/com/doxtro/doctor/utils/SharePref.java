package com.doxtro.doctor.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.doxtro.doctor.model.ProfileModel;
import com.google.gson.Gson;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ghanshyamnayma on 23/07/17.
 */

public class SharePref {

    public static int MODE = 0;
    public static Gson gson = new Gson();


    public static boolean isLoggedIn(Context context) {
        SharedPreferences editor = context.getSharedPreferences(Constants.LOGIN, Context.MODE_PRIVATE);
        return editor.getBoolean(Constants.isLoggedIn, false);
    }

    public static boolean setLoggedIn(Context context, boolean flag) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.LOGIN, context.MODE_PRIVATE).edit();
        editor.putBoolean(Constants.isLoggedIn, flag);
        return editor.commit();
    }

    public static boolean setDoctorDetail(Context context, ProfileModel doctorProfile) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.DOCTOR_DEATIL, Context.MODE_PRIVATE).edit();
        try {
            String result = gson.toJson(doctorProfile);
            editor.putString(Constants.saveProfile, result);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return editor.commit();
    }

    public static ProfileModel getDoctorDetail(Context context) {
        ProfileModel doctorProfile = null;
        try {
            String result = context.getSharedPreferences(Constants.DOCTOR_DEATIL, Context.MODE_PRIVATE).getString(Constants.saveProfile, null);
            if (!TextUtils.isEmpty(result)) {
                doctorProfile = gson.fromJson(result, ProfileModel.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doctorProfile;
    }

    public static boolean isProfileConfigShown(Context context) {
        SharedPreferences editor = context.getSharedPreferences(Constants.PROFILE_CONFIG, Context.MODE_PRIVATE);
        return editor.getBoolean(Constants.showProfile, false);
    }

    public static boolean setProfileConfigShown(Context context, boolean flag) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PROFILE_CONFIG, context.MODE_PRIVATE).edit();
        editor.putBoolean(Constants.showProfile, flag);
        return editor.commit();
    }

    public static boolean saveUserLoginData(Context mContext, String doctorId, String fullName, String mobile, String age, String gender) {
        boolean isSaved;
        // if insert the value into shared preference return true otherwise false to call insert once again//
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.LOGIN, MODE).edit();
            editor.putString(Constants.isLoggedIn, "LoggedIn");
            editor.putString(Constants.doctorID, doctorId);
            editor.putString(Constants.fullName, fullName);
            editor.putString(Constants.mobile, mobile);
            editor.putString(Constants.age, age);
            editor.putString(Constants.gender, gender);
            editor.commit();
            isSaved = true;

        } catch (Exception e) {
            isSaved = false;
        }
        return isSaved;
    }

    public static boolean deleteSharedPrefData(Context mContext) {
        boolean isSaved;
        // if insert the value into shared preference return true otherwise false to call insert once again//
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.LOGIN, MODE).edit();
            editor.putString(Constants.isLoggedIn, "NA");
            editor.putString(Constants.fullName, "");
            editor.putString(Constants.mobile, "");
            editor.putString(Constants.age, "");
            editor.putString(Constants.gender, "");
            editor.putString(Constants.doctorID, "");
            editor.commit();
            isSaved = true;

        } catch (Exception e) {
            isSaved = false;
        }
        return isSaved;
    }

    public static String getDoctorID(Context mContext) {
        SharedPreferences preferences = mContext.getSharedPreferences(Constants.LOGIN, MODE);
        return preferences.getString(Constants.doctorID, "NA");
    }

    public static void setDoctorID(Context mContext, String docID) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.LOGIN, MODE).edit();
        editor.putString(Constants.doctorID, docID);
        editor.apply();
    }

    public static void setSpecializationCategories(Context context, List<String> mainCategories) {
        Set<String> hashSet = new HashSet<>();
        for (int i = 0; i < mainCategories.size(); i++) {
            hashSet.add(mainCategories.get(i));
        }
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.SPECIALIZATIONS, Context.MODE_PRIVATE).edit();
        editor.putStringSet(Constants.mainSpecializations, hashSet);
        editor.apply();
    }

    public static Set<String> getSpecializations(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.SPECIALIZATIONS, Context.MODE_PRIVATE);
        return preferences.getStringSet(Constants.mainSpecializations, null);
    }

    public static String getToken(Context mContext) {
        SharedPreferences preferences = mContext.getSharedPreferences(Constants.TOKEN, Context.MODE_PRIVATE);
        return preferences.getString(Constants.token, "NA");
    }

    public static void setToken(Context mContext, String token) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.TOKEN, Context.MODE_PRIVATE).edit();
        editor.putString(Constants.token, token);
        editor.apply();
    }
    public static String getDeviceId(Context mContext) {
        SharedPreferences preferences = mContext.getSharedPreferences(Constants.LOGIN, MODE);
        return preferences.getString(Constants.DEVICE_ID, "");
    }

    public static void setDeviceId(Context mContext, String _deviceId) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.LOGIN, MODE).edit();
        editor.putString(Constants.DEVICE_ID, _deviceId);
        editor.apply();
    }
}
