package com.doxtro.doctor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.HealthRecordsModel;
import com.doxtro.doctor.utils.DateTimeUtility;

import java.util.List;

/**
 * Created by ghanshyamnayma on 14/07/17.
 */

public class MedicalHistoryAdapter extends RecyclerView.Adapter<MedicalHistoryAdapter.ViewHolder> {
    private List<HealthRecordsModel.Data> historyList;
    private final Context mContext;
    private final TextView noRecords;



    public MedicalHistoryAdapter(Context context, List<HealthRecordsModel.Data> historyList, TextView noRecords) {
        this.mContext = context;
        this.historyList = historyList;
        this.noRecords = noRecords;
    }

    public void update(List<HealthRecordsModel.Data> allergyMedicineList) {
        this.historyList = allergyMedicineList;
        if (historyList.size() == 0)
            noRecords.setVisibility(View.VISIBLE);
        else
            noRecords.setVisibility(View.GONE);

        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_history, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.tvTitle.setText("" + historyList.get(position).getContent());
            holder.tvSubTitle.setText("" + historyList.get(position).getNote());
            holder.tvDateHistory.setText("" + DateTimeUtility.getFormatDDMMMYYYY(historyList.get(position).getDate()));
            HistoryGridAdapter mAdapter = new HistoryGridAdapter(mContext,historyList.get(position).getUrl());
            holder.mGridView.setAdapter(mAdapter);
            holder.mGridView.setOnTouchListener((v, event) -> {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
       final GridView mGridView;
       final TextView tvTitle, tvSubTitle, tvDateHistory;

        private ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.title_history);
            tvSubTitle = (TextView) itemView.findViewById(R.id.sub_title_history);
            tvDateHistory = (TextView) itemView.findViewById(R.id.date_history);
            mGridView = (GridView) itemView.findViewById(R.id.gridview_history);
        }
    }



}
