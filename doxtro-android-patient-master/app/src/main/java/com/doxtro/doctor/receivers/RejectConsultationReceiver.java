package com.doxtro.doctor.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.Reusable;

/**
 * Created by azharuddin on 30/10/17.
 */

public class RejectConsultationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String doctorId = intent.getExtras().getString("doctorId");
        String consultationId = intent.getExtras().getString("consultationId");
        String patientName = intent.getExtras().getString("patientName");
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(100);
        Reusable.respondToRequest(context,
                consultationId,
                Constants.REJECT,
                null,
                patientName);
//        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        manager.cancel(0);
    }
}
