package com.doxtro.doctor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.adapters.MedicationsAdapter;
import com.doxtro.doctor.model.BasicProfileUpdateModel;
import com.doxtro.doctor.model.DiagnosticModel;
import com.doxtro.doctor.model.FindPrescriptionModel;
import com.doxtro.doctor.model.MedicationModel;
import com.doxtro.doctor.model.PatientDetailModel;
import com.doxtro.doctor.model.PrescriptionPostModel;
import com.doxtro.doctor.model.PrescriptionResponseModel;
import com.doxtro.doctor.model.QualificationModel;
import com.doxtro.doctor.server.ApiClient;
import com.doxtro.doctor.server.ApiInterface;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.DialogUtils;
import com.doxtro.doctor.utils.Reusable;
import com.doxtro.doctor.utils.SharePref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ViewPrescriptionACtivity extends AppCompatActivity {

    @BindView(R.id.recycler_view_presc_medicines)
    RecyclerView medicationRV;
    @BindView(R.id.doctor_name_presc)
    TextView docNameTV;
    @BindView(R.id.doc_edu_degree_presc)
    TextView docDegreeTV;
    @BindView(R.id.reg_number_presc)
    TextView docRegNoTV;
    @BindView(R.id.txt_patient_name_presc)
    TextView patientDetailTV;
    @BindView(R.id.txt_date_presc)
    TextView dateTV;
    @BindView(R.id.prescription_edit_btn)
    Button editBtn;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.common_base_toolbarT)
    Toolbar toolbar;
    @BindView(R.id.doctor_sign_name)
    TextView signNameTV;
    @BindView(R.id.doc_sign_degree)
    TextView signDegreeTV;
    @BindView(R.id.reg_number_sign)
    TextView signRegNoTV;
    @BindView(R.id.doctor_specialization_presc)
    TextView specializationTV;


    private List<PrescriptionResponseModel.Data> prescriptionData;
    private List<MedicationModel> medicationsList = new ArrayList<>();
    public static ViewPrescriptionACtivity viewPrescriptionACtivity;
    private List<DiagnosticModel> diagnosticList = new ArrayList<>();
    private String specializations = "";
    // private static String docName, docDegree, docRegNo, patientInfo,timeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_prescription_activity);
        ButterKnife.bind(this);
        viewPrescriptionACtivity = this;
        Bundle bundle = getIntent().getBundleExtra(Constants.PRESCRIPTION_DATA);
        if (bundle != null) {
            initData(bundle);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> finish());
    }

    private String consultationID;

    private void initData(Bundle prescriptions) {


        consultationID = prescriptions.getString(Constants.CONSULTATION_ID);
        String docName = prescriptions.getString(Constants.DOCTOR_NAME);
        String docDegree = prescriptions.getString(Constants.DOCTOR_DEGREE);
        String docRegNo = prescriptions.getString(Constants.DOCTOR_REGNO);
        String timeStamp = prescriptions.getString(Constants.TIME_STAMP);
        String patientInfoName = prescriptions.getString(Constants.PATIENT_NAME);
        String patientInfoAge = prescriptions.getString(Constants.PATIENT_AGE);
        String patientInfoGender = prescriptions.getString(Constants.PATIENT_GENDER);
        // String specializations = prescriptions.getString(Constants.SPECIALIZATIONS);

        if (docName.contains("Dr")) {
            docNameTV.setText(docName);
            signNameTV.setText(docName);
        } else {
            docNameTV.setText("Dr. " + docName);
            signNameTV.setText("Dr. " + docName);
        }

        if (SharePref.getDoctorDetail(this).getData().getCategory() != null) {
            specializations = Reusable.arrayListToString(SharePref.getDoctorDetail(this).getData().getCategory(), ",");
            specializationTV.setVisibility(View.VISIBLE);
            specializationTV.setText(specializations);
        }

        if (docDegree != null) {
            docDegreeTV.setText(docDegree);
            docDegreeTV.setVisibility(View.VISIBLE);
            signDegreeTV.setText(docDegree);
            signDegreeTV.setVisibility(View.VISIBLE);
        }
        if (docRegNo != null && !docRegNo.equalsIgnoreCase("")) {
            docRegNoTV.setText(getString(R.string.dr_reg_number) + " : " + docRegNo);
            signRegNoTV.setText(getString(R.string.dr_reg_number) + " : " + docRegNo);
        }
        dateTV.setText("Date: " + timeStamp);
        patientDetailTV.setText("Name : " + patientInfoName + " " + patientInfoAge + ", " + patientInfoGender);

        String source = prescriptions.getString(Constants.SOURCE);

        switch (source) {
            case Constants.PRESCRIPTION:
                toolbarTitle.setText(getString(R.string.prescriptionTxt));
                getAllPrescriptions(consultationID, Constants.PRESCRIPTION);
                editBtn.setText(getString(R.string.sendprescriptionTxt));
                editBtn.setOnClickListener(v -> {
                    PrescriptionPostModel prescriptionPostModel = new PrescriptionPostModel();
                    prescriptionPostModel.setConsultationId(consultationID);
                    prescriptionPostModel.setType(Constants.PRESCRIPTION);
                    prescriptionPostModel.setMedication(medicationsList);
                    PatientDetailModel patientDetailModel = new PatientDetailModel();
                    patientDetailModel.setName(patientInfoName);
                    patientDetailModel.setAge(patientInfoAge);
                    patientDetailModel.setGender(patientInfoGender);
                    sendPrescriptionData(prescriptionPostModel, Constants.PRESCRIPTION_VIEW);
                });
                break;
            case Constants.DIAGNOSTIC:
                toolbarTitle.setText(getString(R.string.diagnosticText));
                getAllPrescriptions(consultationID, Constants.DIAGNOSTIC);
                editBtn.setText(getString(R.string.requestDiagnostic));
                editBtn.setOnClickListener(v -> {
                    PrescriptionPostModel prescriptionPostModel = new PrescriptionPostModel();
                    prescriptionPostModel.setConsultationId(consultationID);
                    prescriptionPostModel.setType(Constants.DIAGNOSTIC);
                    prescriptionPostModel.setDiagnostic(diagnosticList);
                    PatientDetailModel patientDetailModel = new PatientDetailModel();
                    patientDetailModel.setName(patientInfoName);
                    patientDetailModel.setAge(patientInfoAge);
                    patientDetailModel.setGender(patientInfoGender);
                    sendPrescriptionData(prescriptionPostModel, Constants.DIAGNOSTIC_VIEW);
                });
                break;
            case Constants.EDIT_PRESCRIPTION:
                toolbarTitle.setText(getString(R.string.prescriptionTxt));
                getAllPrescriptions(consultationID, Constants.PRESCRIPTION);
                editBtn.setText(getString(R.string.editPrescriptionTxt));
                editBtn.setOnClickListener(v -> {
//                PrescriptionPostModel prescriptionPostModel = new PrescriptionPostModel();
//                prescriptionPostModel.setConsultationId(consultationID);
//                prescriptionPostModel.setType(Constants.PRESCRIPTION);
//                prescriptionPostModel.setMedication(medicationsList);
//                ChatActivity.sendPrescriptionData(prescriptionPostModel, prescriptionID, null);
                    //                PrescriptionActivity.getMedicationsData(prescriptionData.get(0).getMedication());
                    PrescriptionActivity.getMedicationsData(medicationsList);
                    Intent intent = new Intent(ViewPrescriptionACtivity.this, PrescriptionActivity.class);
                    intent.putExtra(Constants.SOURCE, Constants.EDIT_PRESCRIPTION);
                    intent.putExtra(Constants.CONSULTATION_ID, consultationID);
                    if (medicationsList != null && medicationsList.size() > 0) {
                        PrescriptionActivity.getMedicationsData(medicationsList);
                    } else {
                        PrescriptionActivity.getMedicationsData(new ArrayList<>());
                    }
                    startActivity(intent);

                });
                break;
            case Constants.EDIT_DIAGNOSTIC:
                toolbarTitle.setText(getString(R.string.diagnosticText));
                editBtn.setText(getString(R.string.editDiagnosticText));
                getAllPrescriptions(consultationID, Constants.DIAGNOSTIC);

                editBtn.setOnClickListener(v -> {
                    DiagnosticActivity.getDiagnosticsData(diagnosticList);
                    Intent intent = new Intent(ViewPrescriptionACtivity.this, DiagnosticActivity.class);
                    intent.putExtra(Constants.SOURCE, Constants.EDIT_DIAGNOSTIC);
                    intent.putExtra(Constants.CONSULTATION_ID, consultationID);
                    startActivity(intent);
                });

                break;
        }


    }


    private void sendPrescriptionData(PrescriptionPostModel prescriptionPostModel, String source) {
        List<String> educations = new ArrayList<>();
        if (SharePref.getDoctorDetail(this) != null && SharePref.getDoctorDetail(this).getData() != null) {
            BasicProfileUpdateModel basicProfileUpdateModel = SharePref.getDoctorDetail(this).getData();
            List<QualificationModel> qualificationModels = basicProfileUpdateModel.getQualification();
            for (QualificationModel qualificationModel : qualificationModels) {
                educations.add(qualificationModel.getSpecialization());
            }
        }
        DialogUtils.showProgressDialog(this, getString(R.string.pleaseWaittxt));
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);
        Call<PrescriptionResponseModel> call = apiService.sendPrescription(prescriptionPostModel);
        call.enqueue(new Callback<PrescriptionResponseModel>() {
            @Override
            public void onResponse(Call<PrescriptionResponseModel> call, Response<PrescriptionResponseModel> response) {
                try {
                    if (response.isSuccessful()) {
                        String prescriptionId = response.body().getData().getId();
                        PatientDetailModel patientDetailModel = response.body().getAdditionalDetails();
                        ChatActivity.sendPrescriptionData(prescriptionPostModel, prescriptionId, educations, patientDetailModel, source);
                        if (source.contains(Constants.PRESCRIPTION) || source.contains(Constants.PRESCRIPTIONC)) {
                            PrescriptionActivity.prescriptionActivity.finish();
                            PrescriptionActivity.prescriptionActivity = null;
                        } else {
                            DiagnosticActivity.diagnosticActivity.finish();
                            DiagnosticActivity.diagnosticActivity = null;
                        }
                        finish();

                    } else {
                        DialogUtils.appToast(ViewPrescriptionACtivity.this, new JSONObject(response.errorBody().string()).getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<PrescriptionResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                DialogUtils.dismissProgressDialog();
            }


        });
    }

    private void getAllPrescriptions(String consultationID, String type) {

        DialogUtils.showProgressDialog(this, "Please wait a while..");
        ApiInterface apiService = ApiClient.getClient(this).create(ApiInterface.class);

        Call<FindPrescriptionModel> call = apiService.getPrescriptions(consultationID, type);
        call.enqueue(new Callback<FindPrescriptionModel>() {
            @Override
            public void onResponse(Call<FindPrescriptionModel> call, Response<FindPrescriptionModel> response) {
                try {
                    if (response.isSuccessful()) {
                        prescriptionData = response.body().getData();
                        if (prescriptionData.get(0).getType().equals(Constants.DIAGNOSTIC)) {
                            diagnosticList = prescriptionData.get(0).getDiagnostic();
                            initRV(null, diagnosticList, Constants.DIAGNOSTIC);
                        } else if (prescriptionData.get(0).getType().equals(Constants.PRESCRIPTION)) {
                            medicationsList = prescriptionData.get(0).getMedication();
                            initRV(medicationsList, null, Constants.PRESCRIPTION);
                        }
                    } else {
                        DialogUtils.appToast(ViewPrescriptionACtivity.this, "No data found");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DialogUtils.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<FindPrescriptionModel> call, Throwable t) {
                // Log error here since request failed
                DialogUtils.dismissProgressDialog();
            }


        });

    }

    private void initRV(List<MedicationModel> medication, List<DiagnosticModel> diagnostics, String source) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        MedicationsAdapter medicationsAdapter = new MedicationsAdapter(this, medication, diagnostics, source);
        medicationRV.setLayoutManager(layoutManager);
        medicationRV.setAdapter(medicationsAdapter);
    }


}
