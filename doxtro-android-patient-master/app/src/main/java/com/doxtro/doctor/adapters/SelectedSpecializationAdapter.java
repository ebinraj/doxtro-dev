package com.doxtro.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.utils.AndroidVersionUtility;

import java.util.ArrayList;

/**
 * Created by yashaswi on 03/08/17.
 */

public class SelectedSpecializationAdapter extends BaseExpandableListAdapter {

    private final Context context;

    //selected speciality
    private ArrayList<String> specialityMap = new ArrayList<>(0);


    public SelectedSpecializationAdapter(Context context,
                                         ArrayList<String> specialityMap) {
        this.context = context;
        this.specialityMap = new ArrayList<>();
        this.specialityMap = specialityMap;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return specialityMap.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.specialization_child_item, null);
        }
        if (childPosition < specialityMap.size()) {
            TextView subSpecialization = (TextView) view.findViewById(R.id.specialization_sub_category_TV);
            subSpecialization.setText(specialityMap.get(childPosition));
            subSpecialization.setTextColor(subSpecialization.getResources().getColor(R.color.black));

            ImageView subSpecializationRadioUnChecked = (ImageView) view.findViewById(R.id.specialization_sub_category_add);
            ImageView subSpecializationRadioChecked = (ImageView) view.findViewById(R.id.specialization_sub_category_remove);
            if (specialityMap.size() == 1 && specialityMap.get(0).contains(context.getString(R.string.noSpecializationtxt))) {
                subSpecializationRadioUnChecked.setVisibility(View.GONE);
                subSpecializationRadioChecked.setVisibility(View.GONE);
            } else {

                if (specialityMap.contains(subSpecialization.getText().toString())) {
                    subSpecializationRadioUnChecked.setVisibility(View.GONE);
                    subSpecializationRadioChecked.setVisibility(View.VISIBLE);
                } else {
                    subSpecializationRadioUnChecked.setVisibility(View.VISIBLE);
                    subSpecializationRadioChecked.setVisibility(View.GONE);

                }
            }

        }
        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return specialityMap.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return "Selected Specialization";
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {


        if (view == null) {
            LayoutInflater layoutInflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.specialization_main_item, null);
        }
        LinearLayout mainLayout = (LinearLayout) view.findViewById(R.id.specialization_main_LL);
        TextView heading = (TextView) view.findViewById(R.id.specialization_category_TV);
        heading.setTextColor(context.getResources().getColor(R.color.black));
        mainLayout.setBackground(AndroidVersionUtility.getDrawable(context, R.color.light_appcolor));
        heading.setText("Selected Specialization");

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
