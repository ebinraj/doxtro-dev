package com.doxtro.doctor.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doxtro.doctor.R;
import com.doxtro.doctor.model.DocumentsModel;
import com.doxtro.doctor.utils.AndroidVersionUtility;
import com.doxtro.doctor.utils.Constants;
import com.doxtro.doctor.utils.FirebaseUtils;

import java.util.ArrayList;

/**
 * Created by yashaswi on 21/07/17.
 */

public class UplodIDAdapter extends BaseAdapter {
    private ArrayList<DocumentsModel> documents;
    private String source = "";
    private TextView docsName;
    private ImageView deleteIcon;
    private RelativeLayout docMainLL;

    private final Context mContext;
    private String documentURL = "";


    public UplodIDAdapter(Context context, ArrayList<DocumentsModel> documentsModels) {
        //  docs = medicalDocuments;
        documents = documentsModels;
        mContext = context;

    }

    @Override
    public int getCount() {
        return documents.size();
    }

    @Override
    public Object getItem(int position) {
        return documents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"InflateParams", "ViewHolder"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.upload_document_item, null);
        //  new FirebaseUtils(this);
        docsName = (TextView) convertView.findViewById(R.id.document_name);
        deleteIcon = (ImageView) convertView.findViewById(R.id.delete_documentIV);
        docMainLL = (RelativeLayout) convertView.findViewById(R.id.document_item_mainLL);
        DocumentsModel document = documents.get(position);
        if (document.getTag().equals(mContext.getString(R.string.certificates)))
            docsName.setText("Medical Certificate");
        else if (document.getTag().equals(mContext.getString(R.string.idProof)))
            docsName.setText("ID Proof");
//        if (source.contains(Constants.EDIT_PROFILE)) {
        if (document.getStatus() != null) {
            switch (document.getStatus()) {
                case Constants.PENDING:
                    GradientDrawable bgShape = (GradientDrawable) docMainLL.getBackground().getCurrent();
                    bgShape.setColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                    bgShape.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                    break;
                case Constants.ACCEPTED:
                    GradientDrawable bgShape1 = (GradientDrawable) docMainLL.getBackground().getCurrent();
                    bgShape1.setColor(AndroidVersionUtility.getColor(mContext, R.color.green));
                    bgShape1.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.green));
                    //   docMainLL.setBackgroundColor(AndroidVersionUtility.getColor(mContext,R.color.red));
                    break;
                case Constants.REJECTED:
                    GradientDrawable bgShape2 = (GradientDrawable) docMainLL.getBackground().getCurrent();
                    bgShape2.setColor(AndroidVersionUtility.getColor(mContext, R.color.red));
                    bgShape2.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.red));
                    //   docMainLL.setBackgroundColor(AndroidVersionUtility.getColor(mContext,R.color.red));
                    break;
                default:
                    GradientDrawable bgShape3 = (GradientDrawable) docMainLL.getBackground().getCurrent();
                    bgShape3.setColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                    bgShape3.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.grey_text));
                    deleteIcon.setVisibility(View.VISIBLE);
                    break;
            }
        } else {
            GradientDrawable bgShape3 = (GradientDrawable) docMainLL.getBackground().getCurrent();
            bgShape3.setColor(AndroidVersionUtility.getColor(mContext, R.color.grey_text));
            bgShape3.setStroke(2, AndroidVersionUtility.getColor(mContext, R.color.grey_text));
            deleteIcon.setVisibility(View.VISIBLE);

//            }
        }
        docsName.setOnClickListener(v -> {
            documentURL = documents.get(position).getUrl();
            if (!documentURL.startsWith("http://") || !documentURL.startsWith("https://")) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(documentURL));
                mContext.startActivity(browserIntent);
            }
        });
        deleteIcon.setOnClickListener(v -> {
            documentURL = documents.get(position).getUrl();
            FirebaseUtils.deleteFromFirebase(documentURL, mContext, documents.get(position).getTag());

        });
        return convertView;
    }

    public void updateData(ArrayList<DocumentsModel> certificates) {
        documents = certificates;
        notifyDataSetChanged();
        Log.e("size", "" + documents.size());
    }
}
