package com.doxtro.doctor.model;

import java.util.ArrayList;

/**
 * Created by ghanshyamnayma on 26/10/17.
 */

public class HealthRecordReqModel {

    private String content;

    private String patientId;

    private String tag;

    private String relativesId;

    private ArrayList<String> url;

    private String note;

    public HealthRecordReqModel(String patientId, String relativesId, String tag, String content, String note, ArrayList<String> url) {
        this.content = content;
        this.patientId = patientId;
        this.tag = tag;
        this.relativesId = relativesId;
        this.url = url;
        this.note = note;
    }

    public HealthRecordReqModel(String patientId, String relativesId, String tag, String content) {
        this.patientId = patientId;
        this.relativesId = relativesId;
        this.tag = tag;
        this.content = content;
    }

    public HealthRecordReqModel(String patientId, String relativesId, String tag, String content, String note) {
        this.patientId = patientId;
        this.tag = tag;
        this.relativesId = relativesId;
        this.note = note;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getRelativesId() {
        return relativesId;
    }

    public void setRelativesId(String relativesId) {
        this.relativesId = relativesId;
    }

    public ArrayList<String> getUrl() {
        return url;
    }

    public void setUrl(ArrayList<String> url) {
        this.url = url;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ClassPojo [content = " + content + ", patientId = " + patientId + ", tag = " + tag + ", relativesId = " + relativesId + ", url = " + url + ", note = " + note + "]";
    }
}
