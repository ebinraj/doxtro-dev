package com.doxtro.doctor.receivers;

import android.content.Context;
import android.content.Intent;

import com.doxtro.doctor.activities.ChatActivity;
import com.doxtro.doctor.activities.DeepLinkingActivity;
import com.doxtro.doctor.activities.HomeActivity;
import com.doxtro.doctor.utils.Constants;

import java.util.Map;

/**
 * Created by yashaswi on 14/11/17.
 */

public class DeepLinkingHandler {
//    private static String compaignValue = "";

    public static void handleDeeplinks(Context context, Map<String, String> conversionData) {
        for (String attrName : conversionData.keySet()) {
//            Log.d(AppsFlyerLib.LOG_TAG, "attribute: " + attrName + " = " +
//                    conversionData.get(attrName));
//            if (attrName.equalsIgnoreCase("campaign")) {
//                String attribute = attrName;
//                compaignValue = conversionData.get(attribute);
//            }
//            if (attrName.equalsIgnoreCase("media_source")) {
////                if(!SharePref.isUserLogin(context).equals("NA")) {
//                switch (conversionData.get(attrName)) {
//                    case Constants.LINK_CHAT:
//                        if (!compaignValue.contains("None")) {
//                            DeepLinkingActivity.deepLinkingActivity.finish();
//                            DeepLinkingActivity.deepLinkingActivity = null;
//                            Intent intent = new Intent(context, ChatActivity.class);
//                            intent.putExtra(Constants.CONSULTATION_ID, compaignValue);
//                            context.startActivity(intent);
//                        }
//                        break;
//                    case Constants.LINK_DOCUMENTS:
//                    case Constants.LINK_AVAILABILITY:
//                    case Constants.LINK_BANKDETAILS:
//                        Intent intent1 = new Intent(context, HomeActivity.class);
//                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent1.putExtra(Constants.TAB_SWITCH, 2);
//                        context.startActivity(intent1);
//                        break;
//                    default:
//                        Intent intent4 = new Intent(context, HomeActivity.class);
//                        context.startActivity(intent4);
//                        break;
//
//                }
////                }else
////                {
////                    Intent intent=new Intent(context, LoginActivity.class);
////                    context.startActivity(intent);
////                }
//            }

            if (attrName.equalsIgnoreCase("path")) {
                String pathData = conversionData.get(attrName);
                if (pathData.contains(Constants.LINK_CHAT)) {
                    String consultationID = pathData.substring(pathData.lastIndexOf("/") + 1);
                    DeepLinkingActivity.deepLinkingActivity.finish();
                    DeepLinkingActivity.deepLinkingActivity = null;
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra(Constants.CONSULTATION_ID, consultationID);
                    intent.setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    context.startActivity(intent);
                    break;
                } else if (pathData.contains(Constants.LINK_DOCUMENTS)
                        || pathData.contains(Constants.LINK_AVAILABILITY)
                        || pathData.contains(Constants.LINK_BANKDETAILS)) {
                    DeepLinkingActivity.deepLinkingActivity.finish();
                    DeepLinkingActivity.deepLinkingActivity = null;
                    Intent intent1 = new Intent(context, HomeActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent1.putExtra(Constants.TAB_SWITCH, 2);
                    intent1.setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    context.startActivity(intent1);
                } else {
                    DeepLinkingActivity.deepLinkingActivity.finish();
                    DeepLinkingActivity.deepLinkingActivity = null;
                    Intent intent4 = new Intent(context, HomeActivity.class);
                    intent4.setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    context.startActivity(intent4);
                    break;
                }
            }

        }

    }


}

