let jwt = require('jsonwebtoken');
module.exports =

    function (cookie_key, session_secret) {
        return function (req, res, next) {
            // just to add session object and method to add session
            req.session = {};
            req.setSession = function (key, val) {
                if (req.session[key])
                    if (req.session[key] == val) return;
                req.session[key] = val;
                res.setHeader('Set-Cookie', cookie_key + '=' + jwt.sign(req.session, session_secret));
                console.log('Cookie set');
            }
            if (req.cookies[cookie_key]) {
                // parse it and set data to session obj
                try {
                    req.session = jwt.verify(req.cookies[cookie_key], session_secret);
                } catch (ex) {

                }
            }
            next();
        }
    }
