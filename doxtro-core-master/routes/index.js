let jwt = require('../generics/jwt');
global.jwtWebToken = new jwt();
const path = require('path');

module.exports = function(app) {
    let method, str;
    let noAuthRequiredList = ['device/sendPushNotification', 'payment/save', 'payment/cancel', 'doctor/signUp', 'doctor/signIn', 'doctor/verify', 'patient/signUp', 'patient/signIn', 'patient/verify', 'admin/signIn'];
    let adminTags = ['admin', 'superAdmin', 'bot', 'teleCaller', 'sales'];
    app.use((req, res, next) => {
        req.temp = {};
        let urlArr = (req.url).split('/');
        let intermediateStr = urlArr[urlArr.length - 1].split('?');
        //console.log(intermediateStr)
        if(urlArr[3] =='payment' && (intermediateStr[0] == 'save' || intermediateStr[0] == 'cancel'))
            str = urlArr[3] + '/' + intermediateStr[0];
         else
            str = urlArr[3] + '/' + urlArr[urlArr.length - 1];
        //console.log("str", str);
        if (noAuthRequiredList.indexOf(str) == -1 && req.host != "localhost"){
            //console.log("here", str);
            isAuthenticated(req, res, next);
        } 
            
        else
            next();
    });

    function isAuthenticated(req, res, next) {
        // check header or url parameters or post parameters for token
        let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization'];
        if (token)
            token = token.replace(/['"]+/g, '');
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwtWebToken.verify(token, function(err, decoded) {
                if (err) {
                    return res.status(400).json({ message: 'Unauthorized access.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    //req.decoded = decoded;
                    if (adminTags.indexOf(decoded.tag) != -1) {
                        decoded.tag = 'admin';
                        req.adminId = decoded._id
                    }
                    database.models[decoded.tag].findOne({ _id: decoded._id }).then(result => {
                        if (result)
                            next();
                        else
                            return res.status(400).json({ message: 'No user found.' });
                    }).catch(error => {
                        return res.status(400).json({ message: 'No user found.' });
                    })


                }
            });
        } else {
            return res.status(400).json({ message: 'Token not provided.' });
        }
    };


    var router = function(req, res, next) {
        req.params.controller += "Controller";
        if (!controllers[req.params.controller]) next();
        else if (!controllers[req.params.controller][req.params.method]) next();
        else if (req.params.method.startsWith('_')) next();
        else {
            new Promise((resolve, reject) => {
                try {
                    if (req.params.middlewear) {
                        method = req.method;
                        return controllers[req.params.controller][req.params.middlewear](req, res, next)
                            .then(result => {
                                req.method = method;
                                resolve(controllers[req.params.controller][req.params.method](req))
                            }).catch(error => {
                                reject(error);
                            })
                    } else
                        resolve(controllers[req.params.controller][req.params.method](req))
                } catch (ex) {
                    reject(ex);
                }
            }).then(result => {
                res.status(result.status ? result.status : 200).json({
                    //meta: {
                    //code: result.code,
                    //currentDate: new Date().toISOString()
                    //},
                    message: result.message,
                    profilePercentage: result.profilePercentage,
                    data: result.data,
                    additionalDetails : result.additionalDetails,
                    patientFee: result.patientFee,
                    pagination: result.pagination,
                    totalCount: result.totalCount,
                    count: result.count
                });
            }).catch(error => {
                res.status(error.status ? error.status : 400).json({
                    //meta: {
                    //code: error.errorCode,
                    message: error.message,
                    //currentDate: new Date().toISOString()
                    //}
                });
            });
        }
    }


    app.all('/api/v1/:controller/:method', router);

    app.all('/api/v1/:controller/:_id/:method', router);

    app.all('/api/v1/:controller/:method/:_id', router);

    app.all('/api/v1/:controller/:middlewear/redirect/:method', router);

    app.all('/*', function(req, res) {
        console.log(app.get('appPath'));
        res.sendFile(path.join(__dirname, '../dist/', 'index.html'));
    });

    app.use((req, res, next) => {
        res.status(404).send('Not found!');
    });
}