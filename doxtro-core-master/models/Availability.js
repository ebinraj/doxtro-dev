module.exports = {
    name: 'availability',
    schema: {
        //required fields
        userId: {
            type: ObjectId,
            ref: 'doctor'
        },

        dayOfWeek: {
            type: String,
            enum: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        },

        validFrom: {
            type: Date,
            default: new Date()
        },

        validTill: {
            type: Date,
            default: new Date()
        },

        startTime: {
            type: Number,
            required: true
        },

        endTime: {
            type: Number,
            required: true
        },

        count: {
            type: Number,
            default: 0
        },

        status: {
            type: 'string',
            enum: ['available', 'busy', 'booked']
        },

        isHoliday : {
            type : Boolean,
            default : false
        }

    }
}








