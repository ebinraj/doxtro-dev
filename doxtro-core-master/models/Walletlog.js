var mongoose = require("mongoose");
module.exports = {
    name: 'walletlog',
    schema: {
        //required fields
        patientId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'patient'
        },
        currentBalance: {
            type: Number
        },
        previousBalance: {
            type: Number
        },
        transactionType: {
            type: String,
        },
        transactionRemarks: {
            type: String
        },
        logicalDelete: {
            type: 'boolean',
            defaultsTo: false,
            boolean: true
        }
    }
}