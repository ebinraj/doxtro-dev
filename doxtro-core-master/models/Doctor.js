const mongoose = require("mongoose");
const uuidv1 = require('uuid/v1');

module.exports = {
    name: 'doctor',
    schema: {
        //required fields
        mobile: {
            type: String,
            required: [true, '`mobile` field is missing']
        },

        tag: {
            type: String,
            default: 'doctor'
        },

        //profile details
        emailid: {
            type: String
        },

        firstName: {
            type: String
        },

        lastName: {
            type: String
        },

        age: {
            type: String
        },

        gender: {
            type: String,
            enum: ['Male', 'Female']
        },

        birthDate: {
            type: Date
        },

        profilePic: {
            type: String
        },

        specialization: {
            type: Array
        },

        category: {
            type: Array
        },

        qualification: [{
            type: { type: String, enum: ['UG', 'PG'] },
            degree : String,
            college : String,
            additionalData: String,
            yearTo : String,
            yearFrom : String
        }],

        experience: {
            type: String
        },

        duns: {
            type: String
        },

        expertise: {
            type: String
        },

        memberships: {
            type: String
        },

        awards: {
            type: String
        },

        WorksFor:[{
            organization : String,
            specialization : String,
            experience : String,
        }],

        bio: {
            type: String
        },

        language: {
            type: Array
        },

        //account Details
        accountStatus: {
            type: String,
            enum: ['active', 'disabled', 'inactive', 'blocked'],
            default: 'inactive'
        },

        status: {
            type: String,
            enum: ['pending', 'ready', 'approved', 'rejected'],
            default: 'pending'
        },

        availabilityStatus: {
            type: String,
            enum: ['available', 'busy', 'online', 'offline'],
            default: 'available'
        },

        //social login
        provider: {
            type: String
        },

        accountActivationTime: {
            type: Date,
            default: new Date()
        },

        otp: {
            type: String
        },

        registrationToken: {
            type: String
        },

        otpExpirationTime: {
            type: Date
        },

        isExpired: {
            type: Boolean,
            default: false
        },

        documents: [{
            tag: { type: String, enum: ['idProof', 'certificates'] },
            status: { type: String, enum: ['pending', 'accepted', 'rejected'], default: "pending" },
            reason: String,
            url: String
        }],


        bankDetails: [{
            IFSC: String,
            accountNumber: String,
            accountHolderName: String,
            bank : String
        }],

        adminId: {
            type: ObjectId,
            ref: 'Admin'
        },

        statusChangedAt: {
            type: Date,
            date: true
        },


        modifiedAt: {
            type: Date,
            date: true
        },

        reason : {
            type : String
        },

        userId : {
            type : String
        },

         statusChangedBy : {
             type : ObjectId,
             ref : 'Admin'
        },

        doctorFee : {
            type : Number
        }

    }
}