var mongoose = require('mongoose');
module.exports = {
    name: 'payment',
    schema: {
         //RazorPay response parameters
        id: {
            type: String
        },//"pay_7IZD7aJ2kkmOjk",

        entity: {
            type: String
        },//"payment"

        amount: {
            type: Number
        },//50000,

        currency: {
            type: String
        },//"INR",

        status: {
            type: String
        },//"captured",

        order_id: {
            type: String
        },//null,
        invoice_id: {
            type: String
        },//null,

        international: {
            type: Boolean
        },//false,

        method: {
            type: String
        },//"wallet",

        amount_refunded: {
            type: Number
        },//0,

        refund_status: {
            type: String
        },//null,

        captured: {
            type: Boolean
        },//true,

        description: {
            type: String
        },//"Purchase Description",

        card_id: {
            type: String
        },//null,

        bank: {
            type: String
        },//null,

        wallet: {
            type: String
        },//"freecharge",

        vpa: {
            type: String
        },//null,

        email: {
            type: String
        },//"a@b.com",

        contact: {
            type: String
        },//"91xxxxxxxx",

        notes: {
            type: Object
        },//"merchant_order_id": "order id"

        fee: {
            type: Number
        },//1438,

        tax: {
            type: Number
        },//188,

        error_code: {
            type: String
        },//null,

        error_description: {
            type: String
        },//null,

        created_at: {
            type: Date,
            timestamp: true
        },//1400826750

        //merchat server parameters
        consultationId : {
            type : ObjectId,
            ref : 'consultation'
        },

         //merchat server parameters
        patientId : {
            type : ObjectId,
            ref : 'patient'
        },

         //merchat server parameters
        doctorId : {
            type : ObjectId,
            ref : 'doctor'
        },

        date : {
            type : Date,
            default : new Date()
        },

        service_tax : {
            type : Number
        }
    }
}