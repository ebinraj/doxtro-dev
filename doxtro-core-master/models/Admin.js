module.exports = {
    name: 'admin',
    schema: {

        emailid : {
            type: String,
            required: [true, '`emailId` field is missing']
        },

        password : {
            type : String
        },

        mobile: {
            type: String
        },

        tag: {
            type: String,
            enum: ['admin', 'superAdmin', 'bot', 'teleCaller', 'sales'],
            required: [true, '`tag` field is missing']
        },

        firstName : {
             type: String
        },

        lastName : {
             type: String
        },

        accountStatus: {
            type: String,
            enum: ['active', 'pending', 'locked', 'expired', 'disabled', 'inactive', 'invalid', 'blocked'],
            default : 'active'
        },

        numberOfWrongAttempt: {
            type: Number,
            default: 0
        },

        accountActivationTime: {
            type: Date,
            default : new Date()
        },

        passwordExpirationTime: {
            type: Date,
        },

        //social login
        provider: {
            type: String
        },

        userId : {
            type : String
        },

        createdBy : {
            type : ObjectId
        },

        statusChangedBy : {
             type : ObjectId
        }

    }
}








