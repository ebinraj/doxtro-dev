module.exports = {
    name: 'notification',
    schema: {

        sourceObject: {
            type: ObjectId // UUID of Source Object
        },

        targetObject: {
            type: ObjectId // UUID of Target Object
        },

        consultationId : {
            type : ObjectId,
            ref: 'consultation'
        },

        notificationType: {
            type: String,
            enum: ['consultation', 'assignRequest'] // Type Of Notification
        },

        notificationText: {
            type: String // Actual Notification Text
        },

        date: {
            type: Date,
            default: new Date() // Creation Date Time
        },

        status: {
            type: String,
            enum: ['new', 'dismissed'],// Status of the Notification - New and Dismissed
            default : 'new' 
        },

       responseText: {
            type: String,
            enum: ['accept', 'reject', 'invalid']
       },

       respondedAt : {
           type: Date
       },

       adminId : {
           type : ObjectId,
           ref : 'admin'
       }

    }
}








