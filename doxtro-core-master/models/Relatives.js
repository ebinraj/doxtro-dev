var mongoose = require('mongoose');
module.exports = {
    name: 'relatives',
    schema: {
        patientId: {
            type: mongoose.Schema.Types.ObjectId, //Id of patient
            ref: 'patient'
        },

        relation: {
            type: 'string' // relation of patient with user
        },

        name: {
            type: 'string' // name of patient
        },

        age: {
            type: 'string' // age of patient
        },

        gender: {
            type: 'string',
            enum: ['Female', 'Male']
        },

        logicalDelete: {
            type: 'boolean',
            defaultsTo: false,
            boolean: true
        }
    }
}