var mongoose = require("mongoose");
module.exports = {
    name: 'preappliedcoupon',
    schema: {
        couponTag: {
            type: String
        },
        amount: {
            type: Number
        },
        amountType: {
            type: String,
            enum: ["cashback", "flat", "percentage"]
        },

        countFrom: {
            type: Number
        },

        countTo: {
            type: Number,
        },

        discountType: {
            type: String,
            enum: ["discount", "surge"]
        }
    }
}