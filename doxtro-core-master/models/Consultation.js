var mongoose = require('mongoose');
module.exports = {
    name: 'consultation',
    schema: {
        patientId: {
            type: mongoose.Schema.Types.ObjectId, //Id of patient
            ref: 'patient'
        },

        relativesId: {
            type: mongoose.Schema.Types.ObjectId, //Id of patient
            ref: 'relatives'
        },

        consultingFor: {
            type: 'string'
        },

        consultationType: {
            type: 'string'
        },

        consultationLanguage: {
            type: 'string'
        },

        specializationCategory: {
            type: 'string' // speciality
        },

        note: {
            type: 'string' //additional note given by patient
        },

        attachedDocuments: {
            type: 'array' // documents attached from the patient side
        },

        doctorId: {
            type: mongoose.Schema.Types.ObjectId, //Id of the consulted doctor
            ref: 'doctor'
        },

        rejectedBy: {
            type: 'array'
        },

        prescriptionId: {
            type: 'array' //prescriptionId
        },

        externalReferenceId: {
            type: 'string' // payment Id
        },

        isFollowUp: {
            type: 'boolean',
            default: false,
            boolean: true
        },

        parentId : {
            type : ObjectId,
            ref : 'consultation'
        },

        status: {
            type: 'string',
            enum: ['New', 'Free', 'Paid', 'Waiting', 'Cancelled', 'Ongoing', 'Completed', 'Closed', 'Followup', 'Failed'],
            default: 'New'
        },

        paymentStatus: {
            type: 'string',
            enum: ['free', 'paid', 'pending'],
            default : 'pending'

        },

        validTill: {
            type: 'string' // to check valid date of followup
        },

        validity: {
            type: 'string'
        },

        assignRequestStatus: {
            type: 'string',
            enum: ['accepted', 'rejected', 'pending'],
            default: 'pending'
        },

        assignedBy: {
            type: ObjectId, // Admin Id who has assigned doctor to this consultation or Bot Id
            ref : 'admin'
        },

        failureReason: {
            type: 'string'
        },

        isValid: {
            type: 'boolean',
            boolean: true
        },

        isLive: {
            type: 'boolean',
            boolean: true
        },

        date: {
            type: mongoose.Schema.Types.Date
                //defaultsTo: new Date() //date of consultation
        },

        createdDate: {
            type: mongoose.Schema.Types.Date,
            date: true
        },

        assignedAt: {
            type: mongoose.Schema.Types.Date
        },

        messagedAt: {
            type: mongoose.Schema.Types.Date
        },

        closedAt: {
            type: mongoose.Schema.Types.Date
        },

        feedback: {
            rating: Number,
            ratingValue : String,
            behaviour: String,
            text: String,
            date: Date
        },

        userName : {
            type : String
        },

        notificationText : {
            type : String
        },

        couponUsed: {
            type: 'string'
        },

        couponId : {
            type : ObjectId,
            ref : 'coupon'
        },

        consultationFee : { //actual consultationFee
            type : Number
        },

        patientFee : { //amount payble by patient
            type : String
        },

        preAppliedCouponId : {
            type : ObjectId,
            ref : 'preappliedcoupon'
        },

        feedbackSubmitted : {
            type : Boolean,
            default : false
        }

    }
}