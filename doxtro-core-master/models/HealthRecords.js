module.exports = {
    name: 'healthRecords',
    schema: {

        patientId : {
            type: ObjectId,
            ref: 'patient'
        },

        relativesId : {
            type: ObjectId,
            ref: 'relatives'
        },

        type : {
            type : String,
            enum : ['history', 'medication', 'reports', 'intolerance']
        },

        title : {
            type : String
        },

        content : {
            type : String
        },

        url : {
            type : String
        },

        note : {
            type : String
        },

        date : {
            type : Date,
            default : new Date()
        }

    }
}