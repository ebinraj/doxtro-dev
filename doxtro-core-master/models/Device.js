module.exports = {
    name: 'device',
    schema: {

        userId : {
            type: ObjectId,
            required : true
        },

        deviceId: {
            type: String,
            required: true,
            //unique : true
        },

        typeOfPhone: {
            type: String,
            enum: ['iphone', 'android', 'web']
        },

        typeOfApp: {
            type: String,
            enum: ['doctor', 'patient', 'admin']
        },

        failureReason: {
            type: String
        },

        isEnabled : {
            type : Boolean,
            default : true
        }

    }
}








