/*module.exports = {
    name: 'settings',
    schema: {

        languages: {
            type: 'array'
        },
        color: {
            type: 'string'
        }
    }
}*/

module.exports = {
    name: 'settings',
    schema: {

        author: {
            type: ObjectId, // createdBy
            ref: 'admin'
        },

        tag: {
            type: String
        },

        category: {
            type: String,
            unique : true
        },

        subCategory: {
            type: String
        },

        entity: {
            type: String
        },

        entityArray : {
            type : Array
        },

        charges : {
            consultationFee : Number,
            serviceCharge : Number,
            doctorFee : Number
        },

        updatedBy: {
            type: ObjectId,
            ref: 'admin'
        },
    }
}