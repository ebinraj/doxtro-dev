var mongoose = require("mongoose");
module.exports = {
    name: 'coupon',
    schema: {
        couponTag: {
            type: String
        },

        amount: {
            type: Number
        },

        type : {
            type : String,
            enum : ["cashback", "flat", "percentage"]
        },

        validFrom: {
            type: Date
        },

        validTo: {
            type: Date,
        },

        isForFirstConsultation: {
            type: Boolean,
            defaultsTo: false
        },

        iterationAllowed: {
            type: Number
        },

        usageCount : {
           type : Number
        },

        logicalDelete: {
            type: 'boolean',
            defaultsTo: false,
            boolean: true
        },

        couponGenderSpecific : {
            type : String,
            enum : ["Male", "Female", "All"]
        }
    }
}