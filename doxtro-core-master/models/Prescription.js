module.exports = {
    name: 'prescription',
    schema: {

        consultationId: {
            type: ObjectId,
            ref: 'consultation'
        },

        date: {
            type: Date,
            timestamp : true,
            default : new Date()
        },

        updatedDate : {
            type : Date
        },

        type : {
            type : String,
            enum : ['prescription', 'diagnostic']
        },

       medication: [{
                name : String,
                doseValue: String,
                doseUnit: String,
                duration: String,
                doseTime : {type : Array, enum : ['Morning', 'Afternoon', 'Night']},
                foodWarning : { type: String, enum: ['Before meal', 'After meal']},
                instruction : String
            }],
            
       /* medication : {
            type : Array
        }, *///drug list

         diagnostic : [{name : String,instruction : String}],
        /*diagnostic : {
            type : Array
        },*/
     
        duration: {
            type: Number
        }, // days to follow particular prescription

        advice: {
            type: Array
        },

        prescriptionNumber : {
            type : String
        },

        tag : {
            type : String,
            enum : ['latest', 'older']
        }
    },
    
}
