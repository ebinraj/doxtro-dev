module.exports = {
    name: 'specialization',
    schema: {
        //required fields
        specialization: {
            type: String
        },
        description: {
            type: String
        },
        category: {
            type: String
        },
        isVisible: {
            type: Boolean
        },
        order : {
            type : Number
        },

        logicalDelete: {
            type: 'boolean',
            defaultsTo: false,
            boolean: true
        }
    }
}