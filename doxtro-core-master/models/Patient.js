module.exports = {
    name: 'patient',
    schema: {
        //required fields
        mobile: {
            type: String,
            required: [true, '`mobile` field is missing']
        },

        tag: {
            type: String,
            default: 'patient'
            //required: [true, '`tag` field is missing']
        },


        //profile details
        emailid: {
            type: String
        },

        firstName: {
            type: String
        },

        lastName: {
            type: String
        },

        age: {
            type: String
        },

        gender: {
            type: String,
            enum: ['Male', 'Female']
        },

        birthDate: {
            type: Date
        },

        profilePic: {
            type: String
        },


        //account Details
        accountStatus: {
            type: String,
            enum: ['active', 'disabled', 'inactive', 'blocked'],
            default: 'inactive'
        },

        //social login
        provider: {
            type: String
        },

        accountActivationTime: {
            type: Date,
            default: new Date()
        },

        otp: {
            type: String
        },

        registrationToken: {
            type: String
        },

        otpExpirationTime: {
            type: Date
        },

        isExpired: {
            type: Boolean,
            default: false
        },

        height: {
            type: String
        },

        weight: {
            type: String
        },

        userId: {
            type: String
        },

        statusChangedBy: {
            type: ObjectId,
            ref: 'Admin'
        }

    }
}









