var digitsClient = require('digits-server-client');

var digits = new digitsClient({
    digitsConsumerKey: 'Ga0dQWy6PPKKW0LyieXjbiL0V',
    digitsHost: "https://fabric.io" //MUST BE HTTPS 
});

var otp = class OTP {
    constructor() {}

    sendOTP(mobile) {
        return digits.sendVerificationCode({ phoneNumber: mobile, countryCode: 'IN' }).then(function(registrationToken) {
            return registrationToken;
        }).catch(function(error) {
            console.log("error", error);
            return error;
        });
    }

    verifyOTP(options){
     return digits.verifyCode(options).then(function(result) {
        if (result.success == true) 
            return true;
        else 
            return false;
       }).catch(function(error) {
            return false;
        });
    }
}

module.exports = otp;