var schedule = require('node-schedule');

module.exports = {
    run: function () {
        //update availability status at midnight
        schedule.scheduleJob('0 0 * * *', function () {
            console.log("cronjob started for offline doctors at ", new Date())
            let query = {
                availabilityStatus: 'offline',
                modifiedAt: {
                    '$gte': moment().subtract(1, 'days').startOf('day'),
                    '$lte': moment().subtract(1, 'days').endOf('day')
                }
            }

            database.models.doctor.update(query, { availabilityStatus: 'available', modifiedAt: new Date() }, { multi: true }).then(result => {
                console.log('records updated : ', result.nModified)
            }).catch(error => {
                console.log("error", error)
            })
        });
    },

    //close consultation after 48 hours
    closeConsultations: function () {
        schedule.scheduleJob('0 0 */2 * * *', function () {
            console.log("cronjob started for closing consultation at ", new Date())
            let query = {
                status : {$in : ["Completed", "Ongoing"]},
                assignedAt : {'$lte': moment().subtract(2, 'days').endOf('day')}
            }
            database.models.consultation.find(query).populate('doctorId', 'firstName').exec(function(err, consultations){
                database.models.consultation.update(query, {status : 'Closed', closedAt : new Date()}, { multi: true }).then(result => {
                sendMessage(consultations);
                console.log('records updated : ', result.nModified)
            })
        });
    }

    //send message to firebase
    function sendMessage(results){
        let chatobj, update, chatdata;
       results.forEach(function(result){
            chatobj = fbadmin.database().ref('/Consultations/' + result.consultationId);
            chatdata = {
                    _id: botId,
                    from: "Bot",
                    status: "Sent",
                    msgType: "info",
                    data: {
                        text: "Your chat session with " + result.doctorId.firstName ? result.doctorId.firstName : 'Doctor' + " ended on " + moment().format('DD-MM-YYYY') + " at " + moment().format('hh:mm a') + ' make the payment for a follow up consultation.'
                     }};
              chatobj.once('value').then(function (snapshot) {
              update = {};
              chatdata.timeStamp = new Date();
              update[chatdata.from + "_" + new Date().getTime()] = chatdata;
             chatobj.update(update);
          })
       })
    }
}