/*module.exports = class fcm {

    constructor() { }

    sendMessage(deviceId, pushData) {
        if (!pushData.message) 
            pushData.message = 'Testing Push Notification.';

        let payload = {
            "notification": {
                "title": "Doxtro",
                "body": pushData.message
            },

            "data": {
                "message": pushData.message,
                "type": pushData.type,
                "data" : JSON.stringify(pushData.data)
            }
        }

        let options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
        };
        fbadmin.messaging().sendToDevice(deviceId, payload, options)

            .then(function (response) {
                if(response.successCount > 0)
                    console.log("Successfully sent message to deviceId " + deviceId);
                else
                    console.log("Failed to send message to deviceId " + deviceId);
                return true;
            })
            .catch(function (error) {
                console.log(error.message + " for deviceId " + deviceId);
                return false;
            });

    }
}*/
module.exports = class fcm {

    constructor() { }

    sendMessage(pushData) {

        if(!pushData.title)
            pushData.title = 'Doxtro';
        if(!pushData.message)
            pushData.message = 'New notification from Doxtro';

        let payload = {
            notification: {
                title: pushData.title,
                body: pushData.message
            },
            data: {
                type : pushData.type,
                data : JSON.stringify(pushData.data)
            }
        }

        let options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
        };
    
        fbadmin.messaging().sendToDevice(pushData.deviceId, payload, options)

            .then(function (response) {
                if(response.successCount > 0)
                    console.log("Successfully sent message to deviceId " + pushData.deviceId);
                else
                    console.log("Failed to send message to deviceId " + pushData.deviceId);
                return true;
            })
            .catch(function (error) {
                console.log(error.message + " for deviceId " + pushData.deviceId);
                return false;
            });

    }
}