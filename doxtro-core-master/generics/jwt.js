var jwtToken = require('jsonwebtoken');
let secret = "doxtro";

module.exports = class jwt {
     constructor(){}

    issue(payload) {
        return jwtToken.sign(payload, secret)
    }

    verify(token, callback) {
        return jwtToken.verify(token, secret, callback);
    }
}
