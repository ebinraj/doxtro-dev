let generatePassword = require('password-generator');
let bcrypt = require('bcryptjs');
let allowedTags = ['patient', 'doctor', 'admin'];
module.exports = class Admin extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Admin";
    }

    signIn(req) {
        let method = req.method;
        return new Promise((resolve, reject) => {
            return this._validate(req).then(result => {
                req.temp.userData = result;
                if (result.status == 404)
                    reject(new Error("Not registered."));
                else {
                    return this._decrypt(req.body.password, result.data.password).then(result => {
                        let userData = {
                            _id: req.temp.userData.data._id,
                            emailid: req.temp.userData.data.emailid,
                            firstName: req.temp.userData.data.firstName,
                            tag: req.temp.userData.data.tag,
                            token: jwtWebToken.issue({ _id: req.temp.userData.data._id, tag: req.temp.userData.data.tag })
                        }
                        resolve({ data: userData, message: 'signIn successful' });
                    }).catch(error => {
                        reject(new Error("invalid password."));
                    })
                }
            }).catch(error => {
                reject(error);
            })

        })
    }




    createTeam(req) {
        let method = req.method;
        let allowedTags = ['admin', 'superAdmin', 'bot', 'teleCaller', 'sales'];
        return new Promise((resolve, reject) => {
            if (!req.body.tag || allowedTags.indexOf(req.body.tag) == -1)
                reject(new Error("Invalid tag."))
            return this._validate(req).then(result => {
                if (result.status == 200)
                    reject(new Error("emailid already exists."));
                else {
                    //create team member
                    req.method = method;
                    req.body.numberOfWrongAttempt = 0;
                    req.body.passwordExpirationTime = new Date(new Date().getTime() + (30 * 24 * 60 * 60 * 1000));
                    //req.temp.password = generatePassword(8, false);
                    req.temp.password = "doxtro";
                    req.body.createdBy = req.adminId;
                    return this._encrypt(req.temp.password).then(result => {
                        req.body.password = result;
                        return controllers.patientController._getNextSequence({ collectionName: "team", field: "userId" }).then(nextSequence => {
                            if (nextSequence)
                                req.body.userId = nextSequence;
                            return super.insert(req).then(result => {
                                //send mail
                                req.temp.userData = result.data;
                                mailer.sendMail({
                                    to: result.data.emailid,
                                    subject: 'Doxtro Account Activation',
                                    templateName: 'templates/adminActivation.html',
                                    templateData: {
                                        emailId: result.data.emailid, password: req.temp.password, userName: result.data.firstName,
                                        link: "login"
                                    }
                                }).then(result => {
                                    resolve({ message: "created successfully.", data: req.temp.userData })
                                });
                            }).catch(error => { reject(error); })
                        })
                    }).catch(erorr => { reject(error); })
                }
            }).catch(error => { reject(error); })
        })
    }





    getUsers(req) {
        let query = req.query != undefined ? req.query : req;
        if (!query.tag || allowedTags.indexOf(query.tag) == -1)
            throw new Error('Invalid tag')
        let tag = query.tag; delete query.tag;
        var count;
        let fields = 'firstName mobile status accountStatus specialization accountActivationTime userId emailid tag';
        return new Promise((resolve, reject) => {
            let pagination = {}, searchStr = {};
            if (query.limit) {
                pagination.limit = parseInt(query.limit);
                delete query.limit;
            }
            if (query.skip) {
                pagination.skip = parseInt(query.skip);
                delete query.skip;
            }
            if (query.sortField && query.sortOrder) {
                pagination.sort = {};
                pagination.sort[query.sortField] = query.sortOrder;
                delete query.sortField;
                delete query.sortOrder;
            } else
                pagination.sort = { createdAt: -1 };
            searchStr = query;
            if (searchStr.searchField != '""' && searchStr.searchField != undefined && searchStr.searchField != null) {
                searchStr["$or"] = [
                    { firstName: { "$regex": query.searchField, "$options": "i" } },
                    { mobile: { "$regex": query.searchField, "$options": "i" } },
                ]
            }
            delete query.searchField;

            return database.models[tag].count(searchStr).then(totalCount => {
                if (totalCount == 0)
                    return { message: 'No record found', data: [] };
                else {
                    count = totalCount;
                    return database.models[tag].find(searchStr, fields, pagination);
                }
            }).catch(error => {
                reject(error);
            }).then(records => {
                if (tag == 'doctor') {
                    return this._getDoctorRecords(records).then(records => {
                        resolve({
                            message: 'records found',
                            data: records,
                            totalCount: count,
                            count: records.length,
                            pagination: {
                                from: pagination.skip + 1,
                                to: pagination.skip + records.length
                            }
                        })
                    })

                } else {
                    resolve({
                        message: 'records found',
                        data: records,
                        totalCount: count,
                        count: records.length,
                        pagination: {
                            from: pagination.skip + 1,
                            to: pagination.skip + records.length
                        }
                    })
                }
            })

        })

    }

    //change password
    changePassword(req) {
        //_id, oldpassword, newpassword
        let query = req.body != undefined ? req.body : req;
        if (!query.oldPassword)
            throw new Error('old password not provided');
        if (!query.newPassword)
            throw new Error('new password not provided');
        return database.models.admin.findOne({ _id: query._id }).then(result => {
            if (result) {
                return this._decrypt(query.oldPassword, result.password);
            }
        }).then(result => {
            if (!result)
                throw new Error('Invalid credentials');
            else
                return this.resetPassword({ _id: query._id, password: query.newPassword });
        }).then(result => {
            return result;
        }).catch(error => {
            throw error;
        })
    }



    //forgot password
    forgotPassword(req) {
        //get emailId
        return this._validate(req).then(result => {
            if (result.status == 404)
                throw new Error("invalid user.");
            else {
                //user exists send password reset link
                let payload = {
                    _id: result.data._id,
                    tag: result.data.tag,
                    iat: Math.floor(Date.now() / 1000),
                    exp: Math.floor(Date.now() / 1000) + (15 * 60)

                }
                let token = jwtWebToken.issue(payload);
                return mailer.sendMail({
                    to: result.data.emailid,
                    subject: 'Doxtro Reset Password',
                    templateName: 'templates/resetPassword.html',
                    templateData: {
                        emailId: result.data.emailid, password: req.temp.password, userName: result.data.firstName,
                        link: "resetPassword?token=" + token
                    }
                }).then(res => {
                    return { message: "reset password link sent successfully", data: { _id: result.data._id, token: token } };
                }).catch(error => {
                    throw error;
                })
            }
        }).catch(error => {
            throw error;
        })
    }

    update(req) {
        let fields = '-__v -createdAt -updatedAt -numberOfWrongAttempt -passwordExpirationTime -password';
        let query = req.body != undefined ? req.body : req;
        if (!query._id)
            throw new Error('id not provided');
        if (query.password)
            throw new Error('Cannot update password field');
        return database.models.admin.findByIdAndUpdate(query._id, query, { new: true, fields }).then(result => {
            if (result)
                return { data: result, message: 'updated successfully.' };
            else
                throw new Error('No record found')
        }).catch(error => {
            throw error;
        })
    }

    //remove user
    remove(req) {
        let query = req.body != undefined ? req.body : req;
        if (!query._id)
            throw new Error('_id not provided');
        if (!query.tag || allowedTags.indexOf(query.tag) == -1)
            throw new Error('Invalid tag')
        return database.models[query.tag].delete(query).then(result => {
            if (result.nModified > 0)
                return { message: 'removed successfully' }
            else
                throw new Error('User Not Found')
        }).catch(error => {
            throw error;
        })

    }

    //block/unblock user
    changeAccountStatus(req) {
        let accountStatus = ['blocked', 'inactive']
        let query = req.body != undefined ? req.body : req;
        if (!query._id)
            throw new Error('_id not provided');
        if (!query.tag || allowedTags.indexOf(query.tag) == -1)
            throw new Error('Invalid tag')
        if (!query.accountStatus || accountStatus.indexOf(query.accountStatus) == -1)
            throw new Error('Invalid account status')
        return database.models[query.tag].findByIdAndUpdate(query._id, { isExpired: false, accountStatus: query.accountStatus, statusChangedBy: req.adminId }).then(result => {
            if (result)
                return { message: 'updated successfully' }
            else
                throw new Error('User Not Found');
        }).catch(error => {
            throw error;
        })
    }

    getDoctors(req) {
        if (!req.query.specializationCategory)
            throw new Error('No specializationCategory provided');
        return controllers.doctorController._getDoctorSuggestions(req.query.specializationCategory, req.query.language);
    }

    updateDeviceId(req) {
        if(!req.adminId)
            throw new Error('_id not found');
        req.temp.deviceData = {
            userId: req.adminId,
            deviceId: req.body.deviceId,
            typeOfPhone: 'web',
            typeOfApp: 'admin'
        }
        return controllers.deviceController._create(req.temp.deviceData).then(result => {
            return {data: { deviceId: result, _id: req.adminId }, message: 'updated successfully.' };
        }).catch(error => {
            throw new Error("Failed to update.");
        })
    }


    /*-------------------Internal methods----------------------------------*/
    _validate(req) {
        return new Promise((resolve, reject) => {
            if (!req.body.emailid)
                reject({ status: 400, message: "emailid not found." });
            req.method = "GET";
            req.query = { emailid: req.body.emailid };
            return super.findOne(req).then(result => {
                resolve({ status: 200, data: result.data });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    reject(error);
            })
        });
    }



    _encrypt(password) {
        return new Promise((resolve, reject) => {
            if (password) {
                bcrypt.genSalt(10, function (error, salt) {
                    if (error)
                        reject(error);
                    bcrypt.hash(password, salt, function (error, hash) {
                        if (error)
                            reject(error);
                        resolve(hash);
                    });
                });
            }
        })
    }

    _decrypt(password, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, hash, function (err, res) {
                if (err)
                    reject(err);
                resolve(res);
            });

        })
    }

    resetPassword(req) {
        let query = req.body != undefined ? req.body : req;
        return this._encrypt(query.password).then(result => {
            return database.models.admin.findByIdAndUpdate(query._id, { password: result })
        }).then(result => {
            return { data: { _id: result._id }, message: 'updated successfully.' };
        }).catch(error => {
            throw error;
        })
    }

    _getDoctorRecords(records) {
        let results = [];
        function getSyncData() {
            return records.reduce((promise, record) => {
                return promise.then(res => {
                    return controllers.availabilityController._getOnline({ userId: record._id }).then(result => {
                        let data = {
                            _id: record._id,
                            firstName: record.firstName,
                            mobile: record.mobile,
                            specialization: record.specialization,
                            status: record.status,
                            accountStatus: record.accountStatus,
                            accountActivationTime: record.accountActivationTime,
                            userId: record.userId
                        }
                        data.isOnline = (result == 0) ? false : true;
                        results.push(data);
                    })
                }).catch(console.error);
            }, Promise.resolve());
        }

        return getSyncData().then(() => {
            return results;
        });
    }

}
