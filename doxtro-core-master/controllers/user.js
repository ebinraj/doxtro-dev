let otp = require('../generics/otp');
let otpInstance = new otp();
let fixedArray = ["+917411019222", "+918123632112", "+919740324448", "+917353865923", "+919538177176", "+919940008482", "+918277471006", "+917975928149", "+919487266427", "+919902711211", "+917975982957", "+918978311468"];
module.exports = class User extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    signUp(req) {
        return new Promise((resolve, reject) => {
            return this._validate(req).then(result => {
                if (result.status == 200)
                    throw new Error('Already registered. Please sign In.');
                else {
                    return this._getNextSequence({ collectionName: this.schema, field: "userId" }).then(result => {
                        if (result)
                            req.body.userId = result;
                        super.insert(req).then(result => {
                            req.query = { _id: result.data._id };
                            resolve(this._sendOTP(req));
                        }).catch(error => {
                            throw error;
                        })
                    })
                }
            }).catch(error => {
                reject(error);
            })

        })
    }

    signIn(req) {
        return new Promise((resolve, reject) => {
            return this._validate(req).then(result => {
                if (result.status == 404)
                    throw new Error('Not registered. Please sign Up.');
                else {
                    req.query = { _id: result._id };
                    resolve(this._sendOTP(req));
                }

            }).catch(error => {
                reject(error);
            })

        })
    }



    verify(req) {

        return new Promise((resolve, reject) => {
            if (!req.body._id || !req.body.otp)
                throw new Error('Invalid data');
            return this.validate(req).then(result => {
                if (result.isExpired)
                    throw new Error('OTP Expired.Please try again.');
                if (fixedArray.indexOf(result.mobile) != -1) {
                    if (req.body.otp != '123456') {
                        throw new Error("Invalid OTP.");
                    } else {
                        return this._activateUser(req.body._id, req.body.deviceId, req.body.typeOfPhone).then(userData => {
                            resolve({ message: "Account verified successfully", data: userData })
                        }).catch(error => { reject(error); });
                    }
                } else {
                    req.temp.otpData = {
                        registrationToken: result.registrationToken,
                        code: req.body.otp
                    }
                    return otpInstance.verifyOTP(req.temp.otpData).then(result => {
                        if (result == false) {
                            throw new Error("Invalid OTP.");
                        } else {
                            return this._activateUser(req.body._id, req.body.deviceId, req.body.typeOfPhone).then(userData => {
                                resolve({ message: "Account verified successfully", data: userData })
                            }).catch(error => { reject(error); });
                        }
                    }).catch(error => {
                        throw new Error("Failed to verify OTP, Please try again.");
                    })
                }

            }).catch(error => { reject(error); })
        })
    }


    updateNumber(req) {
        //_id, old mobile number, new mobile number
        let query = req.body == undefined ? req : req.body;
        if(!query.mobile || !query.oldMobile || !query._id)
            throw new Error('Invalid data');
        return this.model.findOne({ _id: query._id, mobile: query.oldMobile }).then(result => {
            if (result)//user exists
                return this._validate(req);
            else
                throw new Error('Mobile number not found');
        }).then(result => {
            if (result.status == 200)
                throw new Error('Number is already registered');
            else
                return this._update(query._id, { mobile: query.mobile, accountStatus : 'inactive' });

        }).then(result => {
            req.query = { _id: result._id };
            return this._sendOTP(req);
        })
    }

    profile(req) {
        return new Promise((resolve, reject) => {
            let fields = '-__v -createdAt -updatedAt -registrationToken -isExpired -deleted';
            return this.model.findByIdAndUpdate(req.body._id, req.body, { new: true }).select(fields).exec().then(result => {
                resolve({ data: result, message: 'Profile updated successfully' })
            }).catch(error => {
                reject(error);
            })
        })
    }


    updateDeviceId(req) {
        return new Promise((resolve, reject) => {
            req.temp.deviceData = {
                userId: req.body._id,
                deviceId: req.body.deviceId,
                typeOfPhone: req.body.typeOfPhone,
                tyepOfApp: req.body.tyepOfApp,
            }
            return controllers.deviceController._create(req.temp.deviceData).then(result => {
                resolve({ data: { deviceId: result, _id: req.body._id }, message: 'updated successfully.' })
            }).catch(error => {
                reject(new Error("Failed to update."))
            })
        })
    }

    /*------------------------------Middleware---------------------------------------------------*/
    validate(req) {
        return new Promise((resolve, reject) => {
            req.temp.findQuery = {};
            if (req.params._id) {
                req.temp.findQuery = { query: { _id: req.params._id } };
            } else if (req.query._id) {
                req.temp.findQuery = { query: { _id: req.query._id } };
            } else if (req.body._id) {
                req.temp.findQuery = { query: { _id: req.body._id } };
            } else {
                reject(new Error("_id not provided."));
            }
            req.temp.findQuery.method = "GET";
            return super.findOne(req.temp.findQuery).then(result => {
                resolve(result.data);
            }).catch(error => {
                reject(error);
            })
        })

    }


    /*--------------------------Internal methods--------------------------------------------------------*/
    _validate(req) {
        return new Promise((resolve, reject) => {

            if (!req.body.mobile)
                throw new Error("Mobile number not found.");
            req.temp.countryCode = "+91";

            if ((req.body.mobile).indexOf(req.temp.countryCode) == -1)
                req.body.mobile = req.temp.countryCode + req.body.mobile;

            if (req.body.mobile.length != 13)
                throw new Error("Invalid mobile number");

            let query = {
                method: "GET",
                query: { mobile: req.body.mobile }
            }

            return super.findOne(query).then(result => {
                resolve({ status: 200, _id: result.data._id });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    reject(error);
            })
        });
    }

    _sendOTP(req) {
        return new Promise((resolve, reject) => {
            //get fixed OTP
            //return database.models.settings.findOne({category : 'fixedOTP'}, 'entityArray').then(result => {
                //let fixedArray = result.entityArray ? result.entityArray : [];
                 if (fixedArray.indexOf(req.body.mobile) != -1) {
                    return this.model.findByIdAndUpdate(req.query._id, { registrationToken: '123456', isExpired: false }).then(result => {
                        resolve({ code: 200, message: 'OTP is : 123456', data: { _id: req.query._id } });
                    }).catch(error => {
                        resolve({ code: 200, message: 'OTP is : 123456', data: { _id: req.query._id } });
                    })
                } else 
                     resolve(this._send(req.body.mobile, req.query._id));
            //}).catch(error => {
               //resolve(this._send(req.body.mobile, req.query._id));
            //})
       })
    }

    _send(mobile, _id){
        return otpInstance.sendOTP(mobile).then(result => {
            return this.model.findByIdAndUpdate(_id, { registrationToken: result, isExpired: false }).then(result => {
                return { code: 200, message: 'OTP has sent to registerd mobile number', data: { _id: _id }};
            }).catch(error => {
                return { code: 200, message: 'OTP has sent to registerd mobile number', data: { _id: _id }};
            })
       }).catch(error => {
           throw new Error('Failed to send OTP to registered number.Please try again.');
        })
    }

    _update(_id, params) {
        let fields = '-__v -createdAt -updatedAt -registrationToken -isExpired -deleted';
        return this.model.findByIdAndUpdate(_id, params, { new: true }).select(fields).exec().then(result => {
            return result
        }).catch(error => {
            throw error;
        })

    }

    _activateUser(_id, deviceId, typeOfPhone) {
        return this._update(_id, { accountStatus: "active", isExpired: true }).then(result => {
            let userData;
            if (result.tag == 'patient') {
                userData = {
                    _id: result._id,
                    mobile: result.mobile,
                    tag: result.tag,
                    emailid: result.emailid,
                    age: result.age,
                    gender: result.gender,
                    profilePic: result.profilePic,
                    height: result.height,
                    weight: result.weight,
                    accountStatus: result.accountStatus,
                    firstName: result.firstName,
                    accountActivationTime: result.accountActivationTime
                }
            } else {
                userData = {
                    _id: result._id,
                    mobile: result.mobile,
                    tag: result.tag,
                    emailid: result.emailid,
                    age: result.age,
                    gender: result.gender,
                    profilePic: result.profilePic,
                    height: result.height,
                    weight: result.weight,
                    accountStatus: result.accountStatus,
                    firstName: result.firstName,
                    accountActivationTime: result.accountActivationTime,
                    specialization: result.specialization,
                    qualification: result.qualification,
                    experience: result.experience,
                    duns: result.duns,
                    expertise: result.expertise,
                    awards: result.awards,
                    WorksFor: result.WorksFor,
                    bio: result.bio,
                    language: result.language,
                    status: result.status,
                    availabilityStatus: result.availabilityStatus,
                    documents: result.documents,
                    bankDetails: result.bankDetails

                }
            }

            //issue token
            userData.token = jwtWebToken.issue({ _id: result._id, tag: result.tag });
            //create device details
            let deviceData = {
                typeOfApp: result.tag,
                userId: result._id,
                deviceId: deviceId,
                typeOfPhone: typeOfPhone
            }
            return controllers.deviceController._create(deviceData).then(result => {
                userData.deviceId = result;
                return userData;
                //resolve({ message: "Account verified successfully", data: userData })
            }).catch(error => {
                return userData;
                //resolve({ message: "Account verified successfully", data: userData })
            })
        }).catch(error => { return error; });
    }


    _createSequence(params) {
        params.nextSequence = 1;
        return counters.insert(params);
    }

    _getNextSequence(query) {
        let self = this;
        return counters.findOne(query).then(result => {
            if (result) {
                return counters.findByIdAndUpdate(result._id, { $inc: { nextSequence: 1 } }, { new: true });
            } else {
                return this._createSequence(query);
            }
        }).then(data => {
            return (data.collectionName + '#' + data.nextSequence);
        }).catch(error => {
            return false;
        })
    }
}