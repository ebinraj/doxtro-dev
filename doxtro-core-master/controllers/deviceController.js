let fcm = require('../generics/fcm');
let fcmInstance = new fcm();
module.exports = class Device extends AbstractController{

    constructor(schema) {
        super(schema)
    }

    static get name() {
        return "Device";
    }


    _create(params) {
        return new Promise((resolve, reject) => {
            let query = {
                deviceId: params.deviceId,
                userId: params.userId,
                typeOfApp: params.typeOfApp
            }
            return database.models.device.findOne(query).then(result => {
                if (!result) {
                    //create new record
                    return database.models.device.insert(params).then(result => {
                        this._sendToFirebase(query);
                        resolve(result.deviceId);
                    }).catch(error => {
                        throw error;
                    })
                } else {
                   resolve(result.deviceId);
                }
            }).catch(error => { reject(error);})
        })
    }

      _pushNotification(data) {
        data.deviceId = [];
        return database.models.device.find(data.query, 'deviceId -_id', { sort: { createdAt: -1 }, limit: 3 }).then(result => {
            if (result.length != 0) {
                for (let i = 0; i < result.length; i++)
                    data.deviceId.push(result[i].deviceId);
                fcmInstance.sendMessage(data);
                return {message: 'sending notification' };
            } else return { message: 'No deviceId found' }
        }).catch(error => {
            return { message: 'No deviceId found' }
        })
    }

     testNotifcation(req) {
        fcmInstance.sendMessage(req.body.data);
        return { message: 'sending notification' };
    }


    sendPushNotification(req){
        return this._pushNotification(req.body);
    }

    //to save max 3 deviceIds to firebase
    _sendToFirebase(data){
        //userId and deviceId
        let userRef = fbadmin.database().ref('/Users/' + data.userId), devices = {};
        userRef.once("value", function(snapshot){
            if(!snapshot.val()){
                userRef.update( {
                     _id : data.userId,
                     status : 'online',
                     timeStamp : fbadmin.database.ServerValue.TIMESTAMP,
                     deviceId : [data.deviceId]
                 });
            }else{
                devices = snapshot.val().deviceId ? snapshot.val().deviceId : [];
                devices.push(data.deviceId);
                if(devices.length > 3) 
                    userData.devices.shift();
                userRef.update({deviceId : devices, timeStamp : fbadmin.database.ServerValue.TIMESTAMP})
            }
            return true;
        })
    }
}
