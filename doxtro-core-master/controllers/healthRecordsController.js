module.exports = class HealthRecords extends AbstractController {
    
    
    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "HealthRecords";
    }

    //create
    create(req){
       return database.models.healthRecords.insert(req.body).then(result => {
          return {data : result , messsage : 'Created successfully'}
       }).catch(error => {
           throw error;
       })
    }

    //update(req)
    update(req){
        //_id, params
       return database.models.healthRecords.findByIdAndUpdate(_id, req.body, {new : true}).then(result => {
          return {data : result , messsage : 'Updated successfully'}
       }).catch(error => {
           throw error;
       })
    }
    
    //remove
      delete(req) {
        //_id
        let query = req.body == undefined ? req : req.body;
        return database.models.healthRecords.findOne({ _id: query._id }).then(result => {
            if (result) {
                req.temp.dataToDelete = result;
                return database.models.healthRecords.delete({ _id: query._id });
            } else
                throw new Error('No healthRecords found');
        }).then(result => {
           return { message: 'Deleted successfully' };
        }).catch(error => { throw error;})
    }

    //find
    find(req) {
        //params
        let query = req.query == undefined ? req : req.query;
        let fields = '-__v -createdAt -updatedAt -deleted';
        return database.models.healthRecords.find(query, fields).then(result => {
            if(result.length != 0)
                return { data: result, message: 'record found' }
            else
                return { data: [], message: 'no record found' }
        }).catch(error => { throw error; })
    }
}
