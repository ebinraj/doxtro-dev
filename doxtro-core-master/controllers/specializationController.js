module.exports = class Specialization extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Specialization";
    }

    //for paiients
    getCategories(req) {
        return new Promise((resolve, reject) => {
            req.method = "GET";
            return database.models.specialization.aggregate([
                { $match: { logicalDelete: false } },
                { $group: { _id: { category: "$category", description: "$description", imageUrl: '$imageUrl', order: "$order" } } },
                { $sort: { "_id.order": 1 } },
                { $project: { _id: 0, category: "$_id.category", description: "$_id.description", imageUrl: "$_id.imageUrl" } }

            ]).then((result) => {
                resolve({ message: "Distinct Categories", status: 200, data: result });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    reject(error);
            });
        });
    }

    //for doctors
    getAllSpecialization(req) {
        return new Promise((resolve, reject) => {
            req.method = "GET";
            return database.models.specialization.aggregate([
                { $match: { logicalDelete: false } },
                { $group: { _id: { category: "$category", order: "$order" }, specializations: { $push: "$specialization" } } },
                { $sort: { "_id.order": 1 } },
                { $project: { _id: "$_id.category", specializations: "$specializations" } }
            ]).then((result) => {
                resolve({ message: "List of Specialization by Category", status: 200, data: result });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    reject(error);
            });
        });
    }

    updateOrder(req) {
        //array of categories with order
        let list = req.body.categories, result = [];

        return new Promise((resolve, reject) => {
            let promises = [], results = [];
            list.forEach(function (category, index) {
                promises.push(database.models.specialization.update({ category: category }, { order: index }, { multi: true }).then(result => {
                    results.push({category : category, order : index});
                })
                )
            })
            Promise.all(promises).then(() => {
                resolve({ data: results, message: "updated successfully." });
            }).catch(error => {
                reject(error);
            });
        })

    }

}