module.exports = class Availability extends AbstractController {
    constructor(schema) {

        super(schema);
    }

    static get name() {
        return "Availability";
    }

    create(req) {
        //_id, operationalHours
        return new Promise((resolve, reject) => {
            if (!req.body.operationalHours)
                throw new Error("availability not provided.");
            return this._checkForUpdate(req.body._id, req.body.operationalHours).then(operationalHours => {
                //got finalList
                if (operationalHours.length != 0) {
                    let promises = [], results = [];
                    operationalHours.forEach(function (operationalHour) {
                        if (operationalHour._id) {
                            //already exists..delete
                            database.models.availability.remove({ _id: operationalHour._id }).then(result => {
                            })
                        } else {
                            //create new
                            operationalHour.status = "available";
                            operationalHour.userId = req.body._id;
                            promises.push(
                                database.models.availability.insert(operationalHour).then(result => {
                                    results.push(result);
                                }).catch((error) => {
                                    //results.push({ error: error });
                                })

                            );
                        }
                    })

                    Promise.all(promises).then(() => {
                        resolve({ data: results, message: "Created successfully." });
                    }).catch(error => {
                        reject(error);
                    });
                } else {
                    //throw new Error("No data found.")
                    throw new Error("Please select different availability time to update.");
                }
            }).catch(error => {
                reject(error);
            })
        })

    }

    //To get availability
    getAvailability(req){
        return this._find(req.query._id).then(result => {
            if(result.length != 0)
                return {data : result, message : 'Availability found.'};
            else
            throw new Error('Availability not found.');
        }).catch(error => {
            throw error;
        })
    }
    
    




    /*------------------------------Middleware---------------------------------------------------*/
    validate(req) {
        return new Promise((resolve, reject) => {
            req.temp.findQuery = {};
            if (req.params._id) {
                req.temp.findQuery = { _id: req.params._id, accountStatus: 'active' };
            } else if (req.query._id) {
                req.temp.findQuery = { _id: req.query._id, accountStatus: 'active' };
            } else if (req.body._id) {
                req.temp.findQuery = { _id: req.body._id, accountStatus: 'active' };
            } else {
                reject(new Error("_id not provided."));
            }
            return database.models.doctor.findOne(req.temp.findQuery).then(result => {
                if (result)
                    resolve(result);
                else
                    throw new Error("No doctor record found.");
            }).catch(error => {
                reject(error);
            })
        })

    }


    /*---------------------------------Internal Methods--------------------------------------------------------*/

    _checkForUpdate(_id, operationalHours) {
        return new Promise((resolve, reject) => {
            return database.models.availability.find({ userId: _id }).then(result => {
                resolve(this._compare(result, operationalHours))
            }).catch(error => {
                if (error.status == 404)
                    resolve(operationalHours);
                else
                    reject(error);
            })
        })
    }


    _compare(oldArr, newArr) {
        function comparer(otherArray) {
            return function (current) {
                return otherArray.filter(function (other) {
                    return other.dayOfWeek == current.dayOfWeek && other.startTime == current.startTime && other.endTime == current.endTime
                }).length == 0;
            }
        }
        let onlyInOld = oldArr.filter(comparer(newArr));
        let onlyInNew = newArr.filter(comparer(oldArr));
        return onlyInOld.concat(onlyInNew);
    }

    _getOnline(options) {
        let currentDate = new Date();
        let convertedDay = moment.tz(currentDate, zone).format('dddd');
        let convertedTime = parseInt(moment.tz(currentDate, zone).format('HHmm'));
        options.dayOfWeek = convertedDay;
        options.startTime = { '$lte': convertedTime };
        options.endTime = { '$gt': convertedTime };
        options.deleted = false;
        return database.models.availability.count(options);
    }

    _find(userId) {
        return new Promise((resolve, reject) => {
            return database.models.availability.find({ userId: userId }, 'dayOfWeek startTime endTime status').then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            })
        })
    }



}