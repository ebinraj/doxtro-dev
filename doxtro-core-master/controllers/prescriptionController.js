module.exports = class Prescription extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Prescription";
    }

    //create or update prescription
    create(req) {
        //consultationId, medication, prescriptionId, type
        let query = req.body ? req.body : req;
        return database.models.consultation.findOne({ _id: query.consultationId }).then(result => {
            if (!result)
                throw new Error('No consultation found');
            else
                //check if prescription is already exists
                return database.models.prescription.findOne({ consultationId: query.consultationId, type: query.type });

        }).then(result => {
            if (!result)
                //insert new prescription
                return this._insert(query);
            else {
                //update prescription and keep track of older one
                return this._update(result.toObject(), query)
            }

        }).catch(error => { throw error; })

    }

    delete(req) {
        //_id
        let query = req.body == undefined ? req : req.body;
        return database.models.prescription.findOne({ _id: query._id }).then(result => {
            if (result) {
                req.temp.dataToDelete = result;
                return database.models.prescription.delete({ _id: query._id });
            } else
                throw new Error('No prescription found')
        }).then(result => {
            return database.models.consultation.findByIdAndUpdate(req.temp.dataToDelete.consultationId, { $pull: { prescriptionId: req.temp.dataToDelete._id } }).then(result => {
                return { message: 'Deleted successfully' }
            }).catch(error => { return { message: 'Deleted successfully' } })
        }).catch(error => { throw error; })
    }

    //find prescription and diagnostic
    find(req) {
        let query = req.query ? req.query : req;
        query.tag = 'latest';
        return database.models.prescription.find(query, '-__v -createdAt -updatedAt -deleted').then(result => {
            if (result.length != 0)
                return { data: result, message: 'record found' }
            else
                return { data: [], message: 'no record found' }
        })
    }


    /*---------------------------------------Internal Methods-------------------------------------*/

    _getRelativesDetails(consultationId) {
        return new Promise((resolve, reject) => {
            return database.models.consultation.findOne({ _id: consultationId }, 'relativesId').populate('relativesId', 'name age gender -_id').exec(function (err, res) {
                if (!err && res) 
                    resolve(res.relativesId);
                else
                    resolve({})
            })
        })
    }

    //create new prescription record
    _insert(data) {
        let prescription;
        data.tag = 'latest';
        data.date = new Date();
        return database.models.prescription.insert(data).then(result => {
            prescription = result;
            return database.models.consultation.update({ _id: result.consultationId }, { $push: { prescriptionId: result._id } });
        }).then(result => {
            return this._getRelativesDetails(data.consultationId).then(res => {
                return { data: prescription, additionalDetails: res, message: 'created successfully' };
            }).catch(error => { return { data: prescription, additionalDetails: {}, message: 'created successfully' }; })
        }).catch(error => {
            throw error;
        })
    }

    //update prescription record 
    _update(oldData, newData) {
        let _id = oldData._id;
        delete oldData._id;
        oldData.tag = 'older';
        return database.models.prescription.insert(oldData).then(result => {
            return database.models.prescription.findByIdAndUpdate(_id, newData, { new: true });
        }).then(result => {
            return this._getRelativesDetails(oldData.consultationId).then(res => {
                return { data: result, additionalDetails: res, message: 'updated successfully' };
            }).catch(error => { return { data: result, additionalDetails: {}, message: 'updated successfully' }; })
        }).catch(error => {
            throw error;
        })
    }
}