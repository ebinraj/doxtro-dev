const mongoose = require('mongoose');
module.exports = class Doctor extends User {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Doctor";
    }




    //approve/reject doctor
    updateDocumentStatus(req) {
        if (!req.body.status || !req.body.docId)
            throw new Error('Invalid data');
        let allowedStatus = ['accepted', 'rejected'];
        if (allowedStatus.indexOf(req.body.status) == -1)
            throw new Error('Invalid status');
        else {
            let params = { "documents.$.status": req.body.status };
            if (req.body.reason)
                params["documents.$.reason"] = req.body.reason;
            let query = {
                _id: req.body._id,
                "documents._id": req.body.docId
            }
            return database.models.doctor.update(query, params, { new: true }).then(result => {
                return { data: req.body, message: 'updated successfully' }
            }).catch(error => {
                throw new Error("Failed to update.");
            })
        }
    }

    //update doctor status
    updateStatus(req) {
        let allowedStatus = ['approved', 'rejected'];
        if (!req.body.status || allowedStatus.indexOf(req.body.status) == -1)
            throw new Error('Invalid status');
        else {
            req.body.statusChangedAt = new Date();
            if (req.body.status == 'approved') {
                //check for document approval
                let query = {
                    _id: req.body._id,
                    $or: [{ 'documents.status': { $in: ['rejected', 'pending'] } },
                    { 'documents.0': { $exists: false } }

                    ]

                }
                return database.models.doctor.findOne(query).then(result => {
                    if (result)
                        throw new Error('pending/rejected document.');
                    else {
                        req.body.adminId = req.adminId;
                        return super._update(req.body._id, req.body).then(result => {
                            return { data: req.body, message: 'updated successfully' }
                        }).catch(error => {
                            //throw error;
                            throw new Error("Failed to update.");
                        })
                    }
                });
            } else {
                return super._update(req.body._id, req.body).then(result => {
                    return { data: req.body, message: 'updated successfully' }
                }).catch(error => {
                    //throw error;
                    throw new Error("Failed to update.");
                })
            }
        }

    }

    //to update status of doctor
    updateAvailabilityStatus(req) {
        let allowedStatus = ['available', 'busy', 'offline'];
        if (allowedStatus.indexOf(req.body.availabilityStatus) == -1)
            throw new Error('Invalid status');
        req.body.modifiedAt = new Date();
        return super._update(req.body._id, req.body).then(result => {
            return { data: { availabilityStatus: result.availabilityStatus, _id: result._id }, message: 'Availability status changed to ' + result.availabilityStatus }
        }).catch(error => {
            //throw error;
            throw new Error("Failed to update.");
        })
    }


    //get doctor suggestions - top 3 records
    /*_getDoctorSuggestions(specializationCategory, language) {
        let query = specializationCategory != "General Physician" ? [specializationCategory, "General Physician"] : [specializationCategory];
        let currentDate = new Date();
        let convertedDay = moment.tz(currentDate, zone).format('dddd');
        let convertedTime = parseInt(moment.tz(currentDate, zone).format('HHmm'));
        return new Promise((resolve, reject) => {

            database.models.doctor.aggregate([{
                $match: {
                    "category": { $in: query },
                    "availabilityStatus": "available",
                    //"status" : "approved"
                }
            },
            {
                $lookup: {
                    from: "availability",
                    localField: "_id",
                    foreignField: "userId",
                    as: "availability"
                }

            },
            {
                $unwind: "$availability"
            },
            {
                $match: {
                    "availability.dayOfWeek": convertedDay,
                    "availability.startTime": { '$lte': convertedTime },
                    "availability.endTime": { '$gt': convertedTime }
                }
            },
            {
                $sort: { experience: -1 }
            },
            {
                $lookup: {
                    from: "consultation",
                    localField: "_id",
                    foreignField: "doctorId",
                    as: "consultations"
                }
            },
            {
                $project: {
                    "firstName": 1,
                    "qualification": 1,
                    "specialization": 1,
                    "category": 1,
                    "experience": 1,
                    "bio": 1,
                    "profilePic": 1,
                    "availabilityId": "$availability._id",
                    "language": 1,
                    "totalConsultations": { $size: "$consultations" }
                }
            },

            ]).then(result => {
                if (result.length != 0) {
                    if (language)
                        result = this._sort(result, 'language', language);
                    let finalList = this._sort(result, 'category', specializationCategory);
                    resolve({ data: finalList });
                    //resolve({ data: finalList.splice(0, 3) });
                } else {
                    resolve({ status: 404, message: "No online doctor found." });
                }
                //resolve({ data: result });
            }).catch(error => {
                reject(error);
            })
        }) //promise ends here
    }*/

    //get doctor suggestions - top 3 records
   _getDoctorSuggestions(specializationCategory, language) {
        let query = specializationCategory != "General Physician" ? [specializationCategory, "General Physician"] : [specializationCategory];
        let currentDate = new Date();
        let convertedDay = moment.tz(currentDate, zone).format('dddd');
        let convertedTime = parseInt(moment.tz(currentDate, zone).format('HHmm'));
        return new Promise((resolve, reject) => {

            database.models.doctor.aggregate([{
                $match: {
                    "category": { $in: query },
                    "availabilityStatus": "available",
                    //"status" : "approved"
                }
            },
            {
                $lookup: {
                    from: "availability",
                    localField: "_id",
                    foreignField: "userId",
                    as: "availability"
                }

            },
            {
                $unwind: "$availability"
            },
            {
                $match: {
                    "availability.dayOfWeek": convertedDay,
                    "availability.startTime": { '$lte': convertedTime },
                    "availability.endTime": { '$gt': convertedTime }
                }
            },
            {
                $sort: { experience: -1 }
            },
            {
                $project: {
                    "firstName": 1,
                    "qualification": 1,
                    "specialization": 1,
                    "category": 1,
                    "experience": 1,
                    "bio": 1,
                    "profilePic": 1,
                    "availabilityId": "$availability._id",
                    "language": 1
                }
            },

            ]).then(result => {

                if (result.length != 0) {
                    let promises = [], results = [];
                    result.forEach(function (item) {
                        item.specializationCategory = item.category.indexOf(specializationCategory) != -1 ? specializationCategory : 'General Physician';
                        //let sortOrder = specializationCategory - 'General Physician' > 0 ? 
                        let queryList = [{ type: 'overall', query: { doctorId: item._id } }, { type: 'ongoing', query: { doctorId: item._id, status: 'Ongoing' } }];
                        promises.push(
                            controllers.consultationController._count(queryList).then(res => {
                                item.totalConsultations = res[0].count;
                                item.ongoingConsultations = res[1].count;
                                results.push(item);
                            }).catch((error) => {
                                item.totalConsultations = 0;
                                item.ongoingConsultations = 0;
                                results.push(item);
                            })
                        );
                    })

                    Promise.all(promises).then(() => {
                        results = this._multiSort(results, specializationCategory);
                        resolve({ data: results });
                    }).catch(error => {
                        results = this._multiSort(results, specializationCategory);
                        resolve({ data: results });
                    });

                } else {
                    resolve({ status: 404, message: "No online doctor found." });
                }
                //resolve({ data: result });
            }).catch(error => {
                reject(error);
            })
        }) //promise ends here
    }


    profile(req) {
        return new Promise((resolve, reject) => {
            return Promise.all([super.profile(req), controllers.availabilityController._find(req.body._id)]).then(result => {
                if (result[0]) {
                    result[0].profilePercentage = this._profileCalculator(result[0].data, result[1]);
                    resolve(result[0]);
                } else
                    throw new Error("failed to update profile");
            }).catch(error => {
                reject(error);
            })
        })
    }

    getProfile(req) {
        let id, data;
        if (req.body._id)
            id = req.body._id;
        else if (req.params._id)
            id = req.params._id
        else if (req.query._id)
            id = req.query._id
        else
            throw new Error("_id not found.");
        return new Promise((resolve, reject) => {
            let fields = { createdAt: 0, updatedAt: 0, __v: 0, registrationToken: 0, deleted: 0, isExpired: 0 };
            return Promise.all([database.models.doctor.findOne({ _id: id }, fields), controllers.availabilityController._find(id)]).then((result) => {
                data = {
                    profile: result[0],
                    availability: result[1]
                }
                let profilePercentage = this._profileCalculator(data.profile, data.availability);
                resolve({ message: "doctor record found successfully", data: data, profilePercentage: profilePercentage });
            }).catch(error => {
                reject(new Error("Failed to fetch doctor details"));
            })

        })
    }




    //sortByGivenValue
    _sort(list, key, value) {
        let trueList = [],
            falseList = [];
        for (let i = 0; i < list.length; i++) {
            if (list[i][key] && list[i][key].indexOf(value) != -1)
                trueList.push(list[i]);
            else
                falseList.push(list[i]);
        }
        return trueList.concat(falseList);
    }

    _multiSort(list, specializationCategory) {
        let order = specializationCategory > 'General Physician' ? -1 : 1;
        return list.sort(function (a, b) {
            if (a.specializationCategory === b.specializationCategory) {
                  return (a.ongoingConsultations - b.ongoingConsultations);
            } else if (a.specializationCategory > b.specializationCategory) {
                return order;
            } else if (a.specializationCategory < b.specializationCategory) {
                return -(order);
            }
        });
    }




    //get top feedback of doctor


    _getTopFeedback(doctorId) {
        let query = {
            doctorId: mongoose.Types.ObjectId("5975bf57ced3555eaada44e3"),
            feedback: { "$exists": true },
            "feedback.rating": { "$gte": 3 }
        }
        return database.models.consultation.aggregate([
            { $match: query },
            {
                $lookup: {
                    from: "patient",
                    localField: "patientId",
                    foreignField: "_id",
                    as: "patient"
                }
            },
            {
                $unwind: "$patient"
            },
            { $sort: { 'feedback.rating': -1 } },
            { $limit: 3 },
            { $skip: 0 },
            {
                $project: {
                    feedback: "$feedback",
                    assignedAt: "$assignedAt",
                    userName: '$patient.firstName',
                    profilePic: '$patient.profilePic',
                    _id: 0
                }
            }
        ]).then(result => {
            return result;
        }).catch(error => {
            return [];
        })
    }


    //to calculate profile percentage 
    /*duns, gender, bio, profilePic, language -  2
     WorksFor,experience, expertise - 5
     specialization - 10
     qualification - 10
     bankdetails - 10
     availability - 20
     documents - 25*/
    _profileCalculator(profile, availability) {

        let count = 0;

        if (profile.duns)
            count += 2;
        if (profile.gender)
            count += 2;
        if (profile.bio)
            count += 2;
        if (profile.language && profile.language.length != 0)
            count += 2;
        if (profile.profilePic)
            count += 2;
        if (profile.WorksFor)
            count += 5;
        if (profile.experience)
            count += 5;
        if (profile.expertise)
            count += 5;
        if (profile.specialization && profile.specialization.length != 0)
            count += 10;
        if (profile.qualification)
            count += 10;
        if (profile.bankdetails && profile.bankdetails.length != 0)
            count += 10;
        if (profile.documents && profile.documents.length != 0)
            count += 25;
        if (availability && availability.length != 0)
            count += 20;
        return count;
    }

    getSuggestion(req) {
        return this._getDoctorSuggestions(req.body.specializationCategory, "English")
    }

}