const consultation = require('./consultationController');

module.exports = class Coupon extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Coupon";
    }

    create(req) {
        return new Promise((resolve, reject) => {
            req.method = "POST";
            let coupondata = req.body;
            return this._validateCreate(coupondata).then(result => {
                if (result.status == 200) {
                    reject(new Error("coupon already exists."));
                } else {
                    return super.insert(req).then((result) => {
                        resolve({ status: 200, data: result.data });
                    }).catch(error => {
                        if (error.status == 404)
                            resolve({ status: 404 })
                        else
                            reject({ error: error, message: error.message });
                    });
                }

            }).catch(error => {
                reject(error);
            });
        });
    }

    //for paiients
    getCouponByTag(req) {
        let coupanobj = {};
        if (req.body)
            coupanobj = req.body;
        else
            coupanobj = req;

        return new Promise((resolve, reject) => {
            return database.models.coupon.findOne({ "couponTag": coupanobj.couponTag, deleted: false }).then(result => {
                if (result)
                    resolve({ status: 200, data: result._doc });
                else
                    resolve({ status: 400, message: "No coupon code exists with this name" });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    resolve({ status: 400, message: error.message });
            });
        });

    }

    //apply coupon
    applyCoupon(req) {
        //patientFee
        let coupondata = req.body != undefined ? req.body : req;
        let patientFee = parseInt(coupondata.patientFee);
        if (isNaN(patientFee))
            throw new Error('Invalid patientFee');
        return new Promise((resolve, reject) => {
            return this.getCouponByTag(coupondata).then(result => {
                if (result.status === 200) {
                    return this._validateCoupan(result.data, coupondata.patientId, coupondata.gender).then(result => {
                        let totalPayable = controllers.preappliedcouponController._calculate(result.data.type, coupondata.patientFee, result.data.amount);
                        resolve({
                                data: {
                                    consultingFee: parseInt(coupondata.patientFee),
                                    doxtroCash: 0,
                                    couponDiscount: result.data.amount + (result.data.type == "percentage" ? "%" : ""),
                                    totalPayable: totalPayable,
                                    couponId : result.data._id,
                                    couponTag : result.data.couponTag
                                },
                                message: 'coupon discount'

                            })
                    }).catch(error => {
                        reject(error);
                    })
                } else {
                    reject(new Error("Invalid Coupon"));
                }
                debugger;
            }).catch(error => {
                reject(error);
            });
        });
    }

    //for Admin
    getCouponList(req) {
        return new Promise((resolve, reject) => {

            return database.models.coupon.find({ deleted: false }).then(result => {
                if (result)
                    resolve({ status: 200, data: result });
                else
                    resolve({ status: 400, message: "No coupon code exists" });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    resolve({ status: 400, message: error.message });
            });
        });
    }

    update(req) {
        if (!req.params._id)
            throw new Error('Invalid data');
        return new Promise((resolve, reject) => {
            let coupondata = req.body;
            coupondata._id = req.params._id;
            return this._validateUpdate(coupondata).then(result => {
                if (result.status == 200 && result.data == undefined) {
                    database.models.coupon.update({ _id: coupondata._id }, coupondata, { upsert: true }).then(result => {
                        resolve({ status: 200, data: result.ok, message: "object modified" });
                    }).catch(error => {
                        reject(error);
                    });
                } else {
                    reject(new Error("couponname already exists."));
                }
            }).catch(error => {
                reject(error);
            })

        });

    }

    deletecoupon(req) {
        if (!req.params._id)
            throw new Error('Invalid data');

        return new Promise(function(resolve, reject) {
            database.models.coupon.remove({ _id: req.params._id }).then((result) => {
                resolve({ status: 200, msg: result });
            }, (rej) => {
                reject({ status: 400, msg: rej });
            }, error => {
                throw new Error(error);
            });
        });
    }


    /*-------------------Internal methods----------------------------------*/
    _validateCreate(coupondata) {
        return new Promise((resolve, reject) => {
            if (!coupondata.couponTag)
                reject({ status: 400, message: "couponTag not found." });
            else if (!coupondata.amount)
                reject({ status: 400, message: "amount not found." });
            else if (!coupondata.validFrom)
                reject({ status: 400, message: "validFrom not found." });
            else if (!coupondata.validTo)
                reject({ status: 400, message: "validTo not found." });
            else if (!coupondata.iterationAllowed)
                reject({ status: 400, message: "iterationAllowed not found." });
            else if (isNaN(Date.parse(coupondata.validFrom)))
                reject({ status: 500, message: "validFrom is not a date" });
            else if (isNaN(Date.parse(coupondata.validTo)))
                reject({ status: 500, message: "validTo is not a date" });
            else if (isNaN(coupondata.amount))
                reject({ status: 500, message: "amount is not a valid number" });
            else if (isNaN(coupondata.iterationAllowed))
                reject({ status: 500, message: "iterationAllowed is not a valid number" });
            else if (coupondata.amount < 0)
                reject({ status: 400, message: "amount should be greater than 0(ZERO)" });
            else if (coupondata.iterationAllowed < 0)
                reject({ status: 400, message: "iterationAllowed should be greater than 0(ZERO)" });

            return this.getCouponByTag(coupondata).then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
        });
    }

    _validateUpdate(coupondata) {
        return new Promise((resolve, reject) => {
            let isNameChanged = false;
            if (coupondata.couponTag != undefined) {
                if (!coupondata.couponTag)
                    reject({ status: 400, message: "couponTag can not be null or empty" });

                isNameChanged = true;
            }
            if (coupondata.validFrom != undefined) {
                if (isNaN(Date.parse(coupondata.validFrom)))
                    reject({ status: 500, message: "validFrom is not a date" });
            }
            if (coupondata.validTo != undefined) {
                if (isNaN(Date.parse(coupondata.validTo)))
                    reject({ status: 500, message: "validTo is not a date" });
            }
            if (coupondata.amount != undefined) {
                if (isNaN(coupondata.amount))
                    reject({ status: 500, message: "amount is not a valid number" });
                else if (coupondata.amount < 0)
                    reject({ status: 400, message: "amount should be greater than 0(ZERO)" });
            }
            if (coupondata.iterationAllowed != undefined) {
                if (isNaN(coupondata.iterationAllowed))
                    reject({ status: 500, message: "iterationAllowed is not a valid number" });
                else if (coupondata.iterationAllowed < 0)
                    reject({ status: 400, message: "iterationAllowed should be greater than 0(ZERO)" });
            }

            if (isNameChanged) {
                return this.getCouponByTag(coupondata).then(result => {
                    if (result.status != 400)
                        resolve({ status: 200, data: result });
                    else
                        resolve({ status: 200, data: undefined })
                }).catch(error => {
                    reject(error);
                });
            } else {
                resolve({ status: 200 })
            }

        });
    }

    _validateCoupan(coupondata, patientId, gender) {
        return new Promise((resolve, reject) => {
            let today = new Date();
            if (today < coupondata.validFrom || today > coupondata.validTo)
                reject({ status: 400, message: "Coupan is not active" });

            consultation.prototype.getPaidConsultationByPatientId(patientId).then(result => {
                if (result.status == 200) {

                    if (coupondata.isForFirstConsultation == true && result.data.length > 0)
                        throw (new Error("Coupon code is only available for the new users"));

                    let couponcount = result.data.filter(function(ct) {
                        return ct.couponUsed === coupondata.couponTag;
                    });

                    if (couponcount.length > coupondata.iterationAllowed)
                        throw (new Error("You have already used this coupon for " + coupondata.iterationAllowed + " times. You are not allowed to use it further."));

                    if (coupondata.couponGenderSpecific != "All") {
                        if (coupondata.couponGenderSpecific != gender)
                            throw (new Error("This coupon is not allowed for " + gender));
                        else
                            resolve({ status: 200, data: coupondata });
                    } else {
                        resolve({ status: 200, data: coupondata });
                    }

                } else {
                    throw (new Error(result));
                }
            }).catch(error => {
                reject(error);
            });

        });
    }

}