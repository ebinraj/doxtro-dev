module.exports = class Patient extends User {
    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Patient";
    }

    getProfile(req) {
        return new Promise((resolve, reject) => {
            let fields = {
                createdAt: 0,
                updatedAt: 0,
                __v: 0,
                registrationToken: 0,
                deleted: 0,
                isExpired: 0

            }
            return database.models.patient.findOne({ _id: req.body._id }, fields).then(result => {
                resolve({ data: result, message: 'patient record found successfully' })
            }).catch(error => {
                reject(new Error("Failed to fetch patient details"));
            })

        })
    }

}

