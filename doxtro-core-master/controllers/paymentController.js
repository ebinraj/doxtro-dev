//const request = require('request');
const request = require('request-promise');
const key_id = 'rzp_test_AHmF8XvSfeTdOc';
const key_secret = 'tM75jVslKDghdYCU7N2kgF7D';
module.exports = class Payment extends AbstractController {

    //create
    create(req) {
        let query = req.body ? req.body : req;
        return database.models.payment.insert(query).then(result => {
            return { data: result, message: 'Payment created successfully' }
        }).catch(error => {
            throw error;
        })
    }

    //find
    find(req) {
        let query = req.query ? req.query : req;
        return database.models.payment.find(query).then(result => {
            if (result.length > 0)
                return { data: result, message: 'record found' };
            else
                return { data: [], message: 'no record found' };
        }).catch(error => {
            throw new Error('Failed to fetch payment details. Please try again');
        })
    }

    capture(req){
          if(!req.body.consultationId)
            throw new Error('consultationId not provided');
         let query = req.body ? req.body : req, paymentDetails;
         return this._validateConsultation(query.consultationId).then(result => {
             query.patientId = result.patientId;
            return request('https://' + key_id + ':' + key_secret + '@api.razorpay.com/v1/payments/' + query.payment_id);
         }).catch(error => {
             throw error;
         }).then(result => {
             result = JSON.parse(result);
              if(result && result.status){
                switch(result.status){
                    case 'authorized' : return request({url : 'https://' + key_id + ':' + key_secret + '@api.razorpay.com/v1/payments/' + req.body.payment_id + '/capture',
                                             method : 'POST',
                                            form: {amount :result.amount}
                                        });
                                    break;
                    case 'captured' : throw new Error('Payment has already been captured');
                                    break;
                    case 'failed' : throw new Error('Payment is failed');
                                    break;
                    default : throw new Error('Payment is not authorized');
                        break;
                }
            }else throw new Error("Failed to fetch payment details");
         }).then(result => {
            //capture result
            result = JSON.parse(result);
            result.consultationId = req.body.consultationId;
            result.patientId = req.body.patientId;
            return database.models.payment.insert(result);
        }).then(result => {
            //update consultation
            paymentDetails = result;
            return controllers.consultationController._updatePaymentStatus(req.body);
        }).then(result => {
            //send response to user
            return {data : result.data, additionalDetails : paymentDetails, message: result.message}
        }).catch(error => {
            throw error;
        })
     }

     _validateConsultation(consultationId){
        return database.models.consultation.findOne({_id :consultationId, status : 'New', paymentStatus : "pending"}).then(result => {
            if(result)
                return result.patientId;
            else
             throw new Error('No valid consultation found');
        })
     }
}