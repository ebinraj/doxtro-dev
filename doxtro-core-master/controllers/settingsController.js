module.exports = class Settings extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Settings";
    }
   
   /*-------------------------Configuration settings------------------------------------------*/

    //To get language list
    getLanguages(req) {
        return this._findEntityArray({category : 'language'}, 'entityArray -_id');
    }

    //To get doctor degree list
    getDegree(req) {
        return this._findEntityArray({category : 'degree'}, 'entityArray -_id');
    }

    //To create settings record
    create(req){
        let query = req.body != undefined ? req.body : req;
        return database.models.settings.create(query).then(result =>{
            return {data : result, message : 'created successfully'}
        }).catch(error => {
            throw error;
        })
    }

    //To update record
    update(req){
        let query = req.body != undefined ? req.body : req;
        return database.models.settings.findByIdAndUpdate(query._id, query, {new : true}).then(result =>{
            return {data : result, message : 'updated successfully'};
        }).catch(error => {
            throw error;
        })
    }
    
    //find
    find(req){
        let query = req.query != undefined ? req.query : req;
        return database.models.settings.find(query).then(result =>{
            if(result.length > 0)
                return {data : result, message : 'record found'};
            else
                return {data  :[], message : 'No record found'};
        }).catch(error => {
            throw error;
        })
    }

    /*---------------------Internal methods--------------------------------------------*/
    
    _findEntityArray(query, fields){
        return database.models.settings.findOne(query, fields).then(result => {
            if(result && result.entityArray)
                return({data:result.entityArray, message : 'record found', status : 200});
            else
                return({ data : [], message : 'No record found', status: 404});
        }).catch(error => {
            return({ data : [], message : 'No record found', status: 404});
        })
    }
    
}