module.exports = class PreAppliedCoupon extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "PreAppliedCoupon";
    }

    save(req) {
        return new Promise((resolve, reject) => {
            req.method = "POST";
            return super.insert(req).then((result) => {
                resolve({ status: 200, data: result.data });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    reject({ error: error, message: error.message });
            });
        });
    }

    delete(req) {
        if (!req.params._id)
            throw new Error('Invalid data');

        return new Promise(function(resolve, reject) {
            database.models.preappliedcoupon.remove({ _id: req.params._id }).then((result) => {
                resolve({ status: 200, msg: result });
            }, (rej) => {
                reject({ status: 400, msg: rej });
            }, error => {
                throw new Error(error);
            });
        });
    }

    list(req) {
        return new Promise(function(resolve, reject) {
            database.models.preappliedcoupon.find({}).then((result) => {
                resolve({ status: 200, data: result });
            }, (rej) => {
                reject({ status: 400, msg: rej });
            }, error => {
                throw new Error(error);
            });
        });
    }

    /*------------------------Internal methods--------------------------------*/

    //check pre applied coupon
    _checkpreAppliedCoupon(query){
        let amountPayble = query.patientFee;
        return database.models.consultation.count({patientId : query.patientId}).then(count => {
            return this._findOne({countFrom : {'$lte' : count}, countTo : {'$gte' : count}});
        }).then(result => {
            if(result){
                amountPayble = this._calculate(result.amountType, query.patientFee, result.amount);
                return {
                        consultationFee : parseInt(query.patientFee),
                        couponUsed : result.couponTag,
                        preAppliedCouponId : result._id,
                        couponDiscount: result.amount + (result.type == "percentage" ? "%" : ""),
                        patientFee : amountPayble}
            } else
                    return {consultationFee: parseInt(query.patientFee), patientFee: amountPayble}
        }).catch(error => {
              return {consultationFee: parseInt(query.patientFee), patientFee: amountPayble}
        })
    }

     _findOne(query){
        return database.models.preappliedcoupon.findOne(query).then(result => {
            return result;
        }).catch(error => {
            return false;
        })
    }

    //calculate the fee
    _calculate(type, fee, discount){
        let discountValue, amountPayble;
        switch(type){
            case 'flat' :   discountValue = parseInt(fee) - parseInt(discount);
                            amountPayble = discountValue > 0 ? discountValue : 0;
                        break;
            case 'percentage' : discountValue = (parseInt(fee) * parseInt(discount)) / 100;
                                discountValue = parseInt(fee) - discountValue;
                                amountPayble = discountValue > 0 ? discountValue : 0;
                    break;
            default : amountPayble = fee;
        }
        return amountPayble;
    }

}