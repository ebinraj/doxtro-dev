module.exports = class Relatives extends AbstractController {

    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Relatives";
    }

    create(req) {
        return new Promise((resolve, reject) => {
            req.method = "POST";
            return this._validate(req).then(result => {

                return super.insert(req).then((result) => {
                    resolve({ status: 200, data: result.data });
                }).catch(error => {
                    if (error.status == 404)
                        resolve({ status: 404 })
                    else
                        reject({ error: error, message: error.message });
                });

            });
        });
    }

    getRelatives(req) {
        return new Promise((resolve, reject) => {
            if (!req.query.patientId)
                reject({ status: 400, message: "patientId not found" });
            return database.models.relatives.find(req.query).sort({createdAt : -1}).then(result => {
                if(result.length > 0)
                    resolve({data :result, message : 'relatives record found successfully'});
                else
                    reject({message: "No relatives record found"});
            }).catch(error => {
                reject(error);
            })
        })
    }

    //update relatives
    update(req) {
        let query = req.body == undefined ? req : req.body;
        if (query.relation)
            throw new Error('relation cannot be updated');
        let fields = 'relation name age gender _id';
        return database.models.relatives.findByIdAndUpdate(query._id, query).then(result => {
            if (result)
                return { data: result, message: 'Updated successfully' };
            else
                throw new Error('No record found to update');
        }).catch(error => {
            throw error;
        })
    }


    /*-------------------Internal methods----------------------------------*/
    _validate(req) {
        return new Promise((resolve, reject) => {
            if (!req.body.patientId)
                reject({ status: 400, message: "patientId not found." });
            else if (!req.body.relation)
                reject({ status: 400, message: "relation not found." });
            else if (!req.body.name)
                reject({ status: 400, message: "name not found." });
            else if (!req.body.age)
                reject({ status: 400, message: "age not found." });
            else if (!req.body.gender)
                reject({ status: 400, message: "gender not found." });


            database.models.patient.findOne({ "_id": req.body.patientId }).then(result => {
                if (result)
                    resolve({ status: 200, data: result.data });
                else
                    reject({ status: 400, message: "patientId not found." });
            }).catch(error => {
                if (error.status == 404)
                    resolve({ status: 404 })
                else
                    resolve({ status: 400, message: error.message });
            });
        });
    }
}