module.exports = class Consultation extends AbstractController {

    constructor(schema) {
        super(schema);
        this.timeout = "";
    }

    static get name() {
        return "Consultation";
    }

    /*----------------------consultation initiate/assign-----------------------------*/

    start(req){
        return this._validateBeforeInitiate(req).then(relative => {
            req.temp.relatives = relative;
            return this._findCharge({patientId : req.body.patientId, specializationCategory : req.body.specializationCategory});
        }).then(result => {
            //found charges
            return this._start(req.body, req.temp.relatives, result);
        }).catch(error => {
            throw error;
        })
    }
    //Initiate consultation
    _start(query, relatives, charges) {
        //patientId, relativesId, consultationType,consultingFor, consultationLanguage, specializationCategory, note, attachedDocuments
                let firstName = relatives.patientId.firstName != undefined ? relatives.patientId.firstName : 'Patient';
                query.createdDate = new Date();
                query.userName = firstName;
                query.notificationText = relatives.name + "," + relatives.gender + "," + relatives.age;
                query.patientFee = charges.patientFee;
                query.consultationFee = charges.consultationFee;
                if(charges.preAppliedCouponId)
                    query.preAppliedCouponId = charges.preAppliedCouponId;
                return Promise.all([database.models.consultation.insert(query), controllers.doctorController._getDoctorSuggestions(query.specializationCategory, query.consultationLanguage)]).then((result) => {
                    let consultation = result[0];
                    let doctorslist = result[1];
                    //delete attached documents befor sending it to the unserintro
                    delete query.attachedDocuments;

                    if (relatives.relation == "Myself")
                        relatives.relation = relatives.gender == 'Female' ? 'Herself' : 'Himself';

                    let requiredDetails = {
                        note: query.note,
                        relativesName: relatives.name,
                        relativesAge: relatives.age,
                        relativesGender: relatives.gender,
                        firstName: firstName,
                        relation: relatives.relation,
                        profilePic: relatives.patientId.profilePic ? relatives.patientId.profilePic : ''
                    }

                    let userintro = {
                        _id: query.patientId,
                        from: "Patient",
                        status: "Sent",
                        msgType: "userIntro",
                        data: requiredDetails

                    };

                    let alert = {
                        _id: botId,
                        from: "Bot",
                        sender: "Doxtro Assistant",
                        //receiver : consultation.patientId,
                        status: "Sent",
                        msgType: "alert",
                        data: "Hello " + firstName + ", the following doctors are available to take up your consultation"
                    };

                    let suggestions = {
                        _id: botId,
                        from: "Bot",
                        sender: "Doxtro Assistant",
                        receiver : consultation.patientId,
                        status: "Sent",
                        msgType: "suggestion",
                        data: doctorslist.status === 404 ? [] : doctorslist.data
                    }

                    let payment = {
                        _id: botId,
                        from: "Bot",
                        sender: "Doxtro Assistant",
                        receiver : consultation.patientId,
                        status: "Sent",
                        msgType: "payment",
                        data: "99"
                    }
                    
                    return this.sendToFBChat([userintro, alert, suggestions, payment], consultation._id).then((result) => {
                         let excludedFields = ['__v', 'updatedAt', 'preAppliedCouponId', 'rejectedBy', 'notificationText', 'prescriptionId', 'deleted'];
                        return { status: 200, data: this._toObject(consultation, excludedFields), patientFee: 99 };
                    }).catch(err => {
                        throw error;
                    });
                }).catch(error => {
                    if (error.status == 404) return{ status: 404 }
                    else throw error;
                });
    }

    //Initiate followup
    startFollowup(req) {
        //parentId
        let query = req.body ? req.body : req;
        let fields = 'patientId relativesId consultingFor consultationType consultationLanguage specializationCategory note userName notificationText doctorId';
        return database.models.consultation.findOne({ _id: query.parentId, status: 'Closed' }, fields).then(result => {
            if (!result)
                throw new Error('No consultation found');
            else {
                //create new record
                result = result.toObject();
                result.isFollowUp = true;
                result.createdDate = new Date();
                result.parentId = result._id;
                result.patientFee = "99";
                delete result._id;
                return database.models.consultation.insert(result);
            }
        }).then(result => {
            //followup created
            return { data: result, patientFee: 99, message: 'followup created successfully' };
        }).catch(error => {
            throw error;
        })
    }

    //after successful payment
    makePayment(req) {
        //consultationId
        let query = req.body ? req.body : req;
        query.status = 'Waiting',
        query.paymentStatus = 'paid';
        return database.models.consultation.findByIdAndUpdate(query.consultationId, query, { new: true }).then(result => {
            if (result.isFollowUp) {
                return this._sendToDoctor({ doctorId: result.doctorId, consultationId: result._id });
            } else {
                query.specializationCategory = result.specializationCategory;
                query.consultationLanguage = result.consultationLanguage;
                query.consultationObject = result;
                let alert = {
                    _id: botId,
                    from: "Bot",
                    sender: "Doxtro Assistant",
                    //receiver : result.patientId,
                    status: "Sent",
                    msgType: "alert",
                    data: "Hello " + result.userName + ", Please wait while we assign a " + query.specializationCategory + " to you."

                };

                let alert2 = {
                    _id: botId,
                    from: "Bot",
                    sender: "Doxtro Assistant",
                    //receiver : result.patientId,
                    status: "Sent",
                    msgType: "waiting",
                    data: { min: 2, sec: 30 }
                };
                this.sendToFBChat([alert, alert2], query.consultationId);
                return this._autoAssign(query);
            }

        }).then(result => {return result;})
        .catch(error => { throw error; })
    }


    //send consultation request by admin
    sendRequest(req) {
        //doctorId, consultationId
        let query = req.body != undefined ? req.body : req, data;
        if (!query.consultationId)
            throw new Error('consultationId not provided');
        if (!query.doctorId)
            throw new Error('doctorId not provided');
        return this._checkAlreadyAccepted(query).then(result => {
            if (result.status == 409)
                throw new Error('This consultation request is already accepted')
            else {
                query.sourceObject = req.adminId ? req.adminId : botId;
                query.doctor = {}; query.doctor._id = query.doctorId;
                return this._sendRequest(query);
            }
        }).catch(error => {
            throw error;
        })

    }


    //doctor send response
    sendResponse(req) {
        //responseText, consultationId, doctorId
        let allowedType = ['accept', 'reject'], assignedBy, type;
        if (!req.body.consultationId)
            throw new Error("consultationId not provided");
        if (!req.body.responseText || allowedType.indexOf(req.body.responseText) == -1)
            throw new Error('Invalid responseText');
        return this._checkAlreadyAccepted(req.body).then(result => {
            if (result.status == 409)
                throw new Error('This consultation request is already accepted')
            else {
                let params = {
                    status: 'dismissed',
                    responseText: req.body.responseText,
                    respondedAt: new Date()
                }
                type = result.isFollowUp ? 'Followup' : 'Consultation';
                return controllers.notificationController._findOneAndUpdate({ targetObject: req.body.doctorId, consultationId: req.body.consultationId }, params).then(result => {
                    if (result && result.sourceObject)
                        assignedBy = result.sourceObject;
                    else assignedBy = botId;
                    if (req.body.responseText == 'accept') {
                        let dataToSend = {
                            doctorId: req.body.doctorId,
                            consultationId: req.body.consultationId,
                            assignedBy: assignedBy
                        }
                        return this._assignDoctor(dataToSend).then(result => {
                            let data = {
                                message: type + ' request accepted',
                                type: 'consultationResponse',
                                data: {
                                    consultationId: req.body.consultationId,
                                    isAccepted: true
                                },
                                query: { typeOfApp: 'admin' }
                            }
                            controllers.deviceController._pushNotification(data);
                            return { message: 'assigned successfully', data: { consultationId: req.body.consultationId } };
                        }).catch(error => { throw error; })
                    } else {
                        let data = {
                            message: type + ' request rejected',
                            type: 'consultationResponse',
                            data: {
                                consultationId: req.body.consultationId,
                                isAccepted: false
                            },
                            query: { typeOfApp: 'admin' }
                        }
                        controllers.deviceController._pushNotification(data);
                        return { message: type + ' request is rejected', data: { consultationId: req.body.consultationId } };
                    }
                }).catch(error => { throw error; })
            }

        }).catch(error => { throw error; })
    }

    /*----------------------------------autoAssign internal methods--------------------------------------------------------------*/
    
    //after successful payment
    _updatePaymentStatus(query) {
        //consultationId
        query.status = 'Waiting',
        query.paymentStatus = 'paid';
        return database.models.consultation.findByIdAndUpdate(query.consultationId, query, { new: true }).then(result => {
            if (result.isFollowUp) {
                return this._sendToDoctor({ doctorId: result.doctorId, consultationId: result._id });
            } else {
                query.specializationCategory = result.specializationCategory;
                query.consultationLanguage = result.consultationLanguage;
                query.consultationObject = result;
                let alert = {
                    _id: botId,
                    from: "Bot",
                    sender: "Doxtro Assistant",
                    //receiver : result.patientId,
                    status: "Sent",
                    msgType: "alert",
                    data: "Hello " + result.userName + ", Please wait while we assign a " + query.specializationCategory + " to you."

                };

                let alert2 = {
                    _id: botId,
                    from: "Bot",
                    sender: "Doxtro Assistant",
                    //receiver : result.patientId,
                    status: "Sent",
                    msgType: "waiting",
                    data: { min: 2, sec: 30 }
                };
                this.sendToFBChat([alert, alert2], query.consultationId);
                return this._autoAssign(query);
            }

        }).then(result => {return result;})
        .catch(error => { throw error; })
    }
     
     //autoAssign for consultation
    _autoAssign(query) {
        //specializationCategory, consultationId, consultationLanguage,consultationObject
        return new Promise((resolve, reject) => {
            return this._checkAlreadyAccepted(query).then(result => {
                if (result.status == 409)
                    throw new Error('This consultation request is already accepted.');
                return controllers.doctorController._getDoctorSuggestions(query.specializationCategory, query.consultationLanguage).then(result => {
                    if (result.status != 404) {
                        let consultationObject = query.consultationObject;
                        delete query.consultationObject;
                        query.notificationText = consultationObject.notificationText ? consultationObject.notificationText : 'Patient';
                        this._sendonebyone(query, result.data);
                        resolve({ status: 200, data: consultationObject, message: "We have received your request, we will notify you as soon as the doctor accepts the request." });
                    } else throw new Error('No doctor found online');
                }).catch(error => { reject(error); })
            }).catch(error => {reject(error);})
        })
    }

    _sendonebyone(query, data) {
        var c = 1;
        var self = this;
        query.doctor = data[0];
        query.sourceObject = botId;
        self._sendRequest(query, 'instantConsultation');

        this.timeout = setInterval(function () {

            //before going further - lets check whether the doctor has accepted the invitation or not 
            self._checkAlreadyAccepted(query).then(result => {

                if (result.status == 200) {

                    if (c > 1) {
                        clearInterval(this);
                        //update consultation with the doctorid - as rejected
                        self._updateConsultationWithRejectedIds(query);
                        //send notification to the admin
                        self._adminNotification(query.consultationId, 'consultation');

                    } else {
                        //update consultation with the doctorid - as rejected
                        self._updateConsultationWithRejectedIds(query);
                        query.doctor = data[c];
                        self._sendRequest(query, 'instantConsultation');
                    }

                } else {
                    console.log("doctor assigned : " + query.doctor._id);
                    clearInterval(this);
                }

                c++;

            }).catch(error => {
                self._adminNotification(query.consultationId, 'consultation');
                clearInterval(this);
            });
        }, 60000);

    }

    //assign for followup
    _sendToDoctor(data) {
        //find doctors availability
        return controllers.availabilityController._getOnline({ userId: data.doctorId }).then(result => {
            if (result > 0) {
                //doctor is available, send notification to doctor
                data.doctor = {};
                data.doctor._id = data.doctorId;
                data.sourceObject = botId;
                return this._sendRequest(data, 'followup');
            } else
                //doctor not available, send notification to admin
                return this._adminNotification(data.consultationId, 'followup');
        }).catch(error => {
            //erro in finding doc availability, send notification to admin
            return this._adminNotification(data.consultationId, 'followup');
        })
    }


    _checkAlreadyAccepted(data) {
        return new Promise((resolve, reject) => {
            if (!data.consultationId)
                throw new Error("ConsultationId not provided.");
            return database.models.consultation.findOne({
                _id: data.consultationId,
                assignRequestStatus: "accepted"
            }).then(result => {
                if (result) {
                    resolve({ status: 409, message: "Already Accepted" });
                } else
                    resolve({ status: 200 });
            }).catch(error => {
                reject(error);
            })
        })
    }


    _assignDoctor(query) {
        //doctorId, consultationId
        let tempArr = [], dataToUpdate;
        return database.models.doctor.findOne({ _id: query.doctorId }, 'firstName gender qualification experience profilePic').then(doctorDetails => {
            if (doctorDetails) {
                dataToUpdate = {
                    doctorId: query.doctorId,
                    status: 'Ongoing',
                    assignRequestStatus: 'accepted',
                    assignedAt: new Date(),
                    assignedBy: query.assignedBy
                }
                return database.models.consultation.findByIdAndUpdate(query.consultationId, dataToUpdate, { new: true }).then(result => {
                    //dismiss all notifications related to this consultation
                    controllers.notificationController._updateMultple({ consultationId: query.consultationId }, { status: 'dismissed' }, { multi: true });
                    let docName = doctorDetails.firstName ? doctorDetails.firstName : 'Doctor';
                    if (result.isFollowUp) {
                        let alert1 = {
                            _id: botId,
                            from: "Bot",
                            sender: "Doxtro Assistant",
                            //receiver : result.patientId,
                            status: "Sent",
                            msgType: "info",
                            data: "This is followup consultation with " + docName + ". You have previously consulted with this doctor on " + moment(result.assignedAt).format("DD-MM-YYYY")
                        };
                        tempArr = [alert1];
                    } else {
                        let docGender = doctorDetails.gender == 'Female' ? 'her' : 'him';
                        let alert1 = {
                            _id: botId,
                            from: "Bot",
                            sender: "Doxtro Assistant",
                            //receiver : result.patientId,
                            status: "Sent",
                            msgType: "alert",
                            data: "Hello " + result.userName + ", " + docName + " has taken up your case. Ping " + docGender + " to begin your consultation :)"
                        };

                        let alert2 = {
                            _id: botId,
                            from: "Bot",
                            sender: "Doxtro Assistant",
                            //receiver : result.patientId,
                            status: "Sent",
                            msgType: "profile",
                            data: doctorDetails.toObject()
                        }
                        tempArr = [alert1, alert2];
                    }

                    this.sendToFBChat(tempArr, query.consultationId);
                    return { status: 200 };
                }).catch(error => { throw error; })
            } else {
                //no doctor data found
                return controllers.notificationController._update({ targetObject: req.body.doctorId, consultationId: req.body.consultationId }, { status: 'invalid' }).then(result => {
                    throw new Error('failed to find doctor.');
                }).catch(error => { throw new Error('failed to find doctor.'); })
            }
        }).catch(error => { throw error; })
    }

    //send request notification
    _sendRequest(query, type) {
        //doctorId, consultationId
        console.log("sending request to : " + query.doctor._id);
        let data = {
            sourceObject: query.sourceObject,
            targetObject: query.doctor._id,
            consultationId: query.consultationId,
            notificationType: 'assignRequest'
        };

        return controllers.notificationController._create(data).then(result => {

            if (result.status != 409) {

                data = {
                     title : 'New consultation request',
                     type: type,
                     data: {
                        doctorId: query.doctor._id,
                        consultationId: query.consultationId
                    },
                    query: { userId: query.doctor._id }
                }
                  switch(type){
                        case 'followup' :  data.message = 'You have got a followup request from ' + query.notificationText;
                        break;
                        default : data.message = 'You have got an instant chat request from ' + query.notificationText,
                                  data.data.note = 'Request will be assigned to another doctor in 1 minute.'
                        break;
                }
                controllers.deviceController._pushNotification(data);
                return { data: result, message: "notification sent successfully." };
            } else
                return { data: result, message: "Consultation request already sent." };

        }).catch(error => { throw error; })
    }

    //method to update the consultation with reject doctor's ids
    _updateConsultationWithRejectedIds(data) {
        database.models.consultation.update({ _id: data.consultationId }, {
            $push: { rejectedBy: data.doctor._id }
        }, { upsert: true }).then((result) => { }).catch(err => {
            console.log(err)
        });
    }

    //method to notify admin that autoassign has failed
    _adminNotification(consultationId, type) {
        let data = {};
        console.log("sending request to admin : " + botId);
        data = {
            sourceObject: BotId,
            targetObject: BotId,
            consultationId: consultationId,
            notificationType: 'consultation'
        }
        return controllers.notificationController._create(data).then(result => {
            data = {
                message: 'New ' + type + ' request',
                type: 'consultation',
                data: {
                    consultationId: consultationId
                },
                query: { typeOfApp: 'admin' }
            }
            controllers.deviceController._pushNotification(data);
            return { message: "notification sent successfully." };
        }).catch(error => {
            return { message: 'failed to notify admin' }
        })
        //send sms, email and push notification
    }

    sendToFBChat(req, consultationId) {

        let chatdata = req.body != undefined ? req.body : req;
        consultationId = consultationId != undefined ? consultationId : req.params._id;

        return new Promise((resolve, reject) => {
            let chatobj = fbadmin.database().ref('/Consultations/' + consultationId);
            chatobj.once('value').then(function (snapshot) {

                if (Array.isArray(chatdata)) {
                    chatdata.forEach(function (element, i) {
                        setTimeout(function () {
                            let update = {};
                            element.timeStamp = fbadmin.database.ServerValue.TIMESTAMP;
                            update[element.from + "_" + new Date().getTime()] = element;
                            chatobj.update(update);
                        }, i * 100)

                    }, this);
                } else {
                    let update = {};
                    chatdata.timeStamp = fbadmin.database.ServerValue.TIMESTAMP;
                    update[chatdata.from + "_" + new Date().getTime()] = chatdata;
                    chatobj.update(update);
                }

                resolve({ status: 200, message: "Message Sent Successfully" });
            });
        }).catch(error => {
            reject(error);
        });

    }

    //find payment 
    _findCharge(query) {
         //specializationCategory, patientId
         query.patientFee = patientFee;
        return database.models.settings.findOne({category: query.specializationCategory}, 'charges').then(result => {
           if(result)
                query.patientFee = result.charges.consultationFee;
           //check for pre applied coupon
           return controllers.preappliedcouponController._checkpreAppliedCoupon(query);
        }).catch(error => {
            //use default payment value
            return controllers.preappliedcouponController._checkpreAppliedCoupon(query);
        })
    }
    /*-------------------------------------list, feedback, update-------------------------*/

    //get consultation list
    getConsultations(req) {
        let query = req.query != undefined ? req.query : req;
        if (req.adminId)
            query.subFields = 'emailid, firstName name age gender mobile relation';
        else {
            if (!query.patientId && !query.doctorId)
                throw new Error('Data not provied');
            query.subFields = 'emailid, firstName name age gender relation';
        }
        return this._find(query).then(result => {
            if (result.data && result.data.length != 0)
                return result;
            else
                //throw new Error('No consultation record found')
                return { data: [], message: 'No consultation record found' }
        }).catch(error => {
            throw error;
        })
    }

    //get today's requests
    getRequests(req) {
        //_id
        return new Promise((resolve, reject) => {
            let self = this;
            let query = req.query != undefined ? req.query : req;
            let start = moment().startOf('day'); // set to 12:00 am today
            let end = moment().endOf('day'); // set to 23:59 pm today
            if (!query.doctorId)
                throw new Error('DoctorId not provided');
            return database.models.notification.find({ targetObject: query.doctorId, status: "new", createdAt: { $gte: start, $lt: end } }).then(consultations => {
                let promises = [],
                    results = [];
                if (consultations.length != 0) {
                    consultations.forEach(function (consultation) {
                        promises.push(
                            self._findById({ _id: consultation.consultationId }).then(result => {
                                if (result)
                                    results.push(result);
                            }).catch((error) => { })

                        );
                    })
                    Promise.all(promises).then(() => {
                        if (results.length != 0)
                            resolve({ data: results, message: "requests found." })
                        else
                            resolve({ data: [], message: 'No consultation request found' });
                        //throw new Error('No consultation request found');
                    }).catch(error => {
                        reject(error);
                    });
                } else {
                    resolve({ data: [], message: 'No consultation request found' });
                    //throw new Error('No consultation request found');
                }

            }).catch(error => {
                reject(error);
            })
        })

    }

    //To submit feedback
    submitFeedback(req) {
        //consultationId, feedback
        let query = req.body != undefined ? req.body : req;
        if (!query.consultationId)
            throw new Error('consultationId not provided');
        if (!query.feedback)
            throw new Error('Feedback not provided');
        return database.models.consultation.findOne({ _id: query.consultationId, feedback: { $exists: true } }).then(result => {
            if (result)
                throw new Error("Feedback already submitted");
            query.feedback.date = new Date();
            query.feedbackSubmitted = true;
            return database.models.consultation.update({ _id: query.consultationId }, query).then(result => {
                return { message: 'feedback submitted successfully', data: { consultationId: query.consultationId } }
            }).catch(error => {
                throw error;
            })
        }).catch(error => {
            throw error;
        })
    }

    //search
    search(req) {
        let query = req.query != undefined ? req.query : req;
        let pagination = {},
            searchQuery = {};
        let aggregateArray = [];
        if (query.sortField && query.sortOrder) {
            pagination.sort = {};
            pagination.sort[query.sortField] = parseInt(query.sortOrder);
            delete query.sortField;
            delete query.sortOrder;
        } else {
            pagination.sort = { createdAt: -1 };
        }
        //limit
        if (query.limit) {
            pagination.limit = parseInt(query.limit);
            delete query.limit;
        } else
            pagination.limit = 10;
        //skip
        pagination.skip = 0;

        aggregateArray = [{
            $lookup: {
                from: "patient",
                localField: "patientId",
                foreignField: "_id",
                as: 'patientId'
            }
        },
        {
            $lookup: {
                from: "doctor",
                localField: "doctorId",
                foreignField: "_id",
                as: 'doctorId'
            }
        },
        {
            $lookup: {
                from: "relatives",
                localField: "relativesId",
                foreignField: "_id",
                as: 'relativesId'
            }
        },
        { $unwind: { path: '$patientId', preserveNullAndEmptyArrays: true } },
        { $unwind: { path: '$doctorId', preserveNullAndEmptyArrays: true } },
        { $unwind: { path: '$relativesId', preserveNullAndEmptyArrays: true } },
        {
            $match: {
                $or: [
                    { 'patientId.firstName': { "$regex": query.searchField, "$options": "i" } },
                    { 'patientId.mobile': { "$regex": query.searchField, "$options": "i" } },
                    { 'doctorId.firstName': { "$regex": query.searchField, "$options": "i" } },
                    { 'doctorId.mobile': { "$regex": query.searchField, "$options": "i" } },
                ]
            }
        },
        { $sort: pagination.sort },
        { $limit: pagination.limit },
        { $skip: pagination.skip }


        ];
        return database.models.consultation.aggregate(aggregateArray).then(result => {
            if (result.length > 0) {
                return {
                    data: result,
                    totalCount: result.length,
                    count: result.length,
                    pagination: {
                        from: pagination.skip + 1,
                        to: pagination.skip + result.length
                    },
                    message: 'records found.'

                }
            } else return { data: [], message: 'No consultations found' };
        }).catch(error => {
            return { data: [], message: 'No consultations found' };
        })
    }


    getDoctorProfile(req) {
        //doctorId
        if (!req.query.doctorId)
            throw new Error('doctorId not provided');
        let finalDetails = {};
        let fields = 'firstName specialization experience WorksFor qualification profilePic bio category'
        //profile
        return database.models.doctor.findOne({ _id: req.query.doctorId }, fields).then(result => {
            if (!result)
                throw new Error('No doctor record found');
            else {
                finalDetails.profile = result;
                return controllers.doctorController._getTopFeedback(req.query.doctorId);
            }
            //feedback    
        }).then(result => {
            finalDetails.feedback = result;
            return this._count([{ type: 'overall', query: { doctorId: req.query.doctorId } }]);
            //consultation count
        }).then(result => {
            finalDetails.consultationCount = result[0].count;
            finalDetails.fee = 99;
            return { data: finalDetails, message: 'record found' };
        }).catch(error => {
            throw new Error('Failed to fetch doctor details');
        })
    }

    //close consultation
    updateStatus(req) {
        //consultationId, status, firstName
        let allowedstatus = ["Closed", "Completed"];
        let query = req.body != undefined ? req.body : req;
        if (!query.status || allowedstatus.indexOf(query.status) == -1)
            throw new Error('Invalid status');
        if (!query.consultationId)
            throw new Error('No consultationId provided');
        return database.models.consultation.update({ _id: query.consultationId }, query).then(result => {
            if (result.nModified > 0) {
                /* let firstName = query.firstName != undefined ? query.firstName : 'Doctor'
                 let alert = {
                     _id: botId,
                     from: "Bot",
                     status: "Sent",
                     msgType: "info",
                     data: "Your chat session with " + firstName + " ended on " + moment().format('DD-MM-YYYY') + " at " + moment().format('hh:mm a') + ' chat window will be open for 48 hours.'
                 };
                 return this.sendToFBChat([alert], query.consultationId).then((result) => {
                     return { message: 'Updated successfully', data: { consultationId: query.consultationId } };
                 }).catch(err => {
                     throw error;
                 });*/
                return { message: 'Updated successfully', data: { consultationId: query.consultationId } };
            } else throw new Error('no consultation found');
        }).catch(error => {
            throw error;
        })

    }


    /*-------------------Internal methods----------------------------------*/
   

    //find
    _find(query) {
        let pagination = {},
            requiredFields, subFields;
        if (query.limit) {
            pagination.limit = parseInt(query.limit);
            delete query.limit;
        }
        if (query.skip != undefined) {
            pagination.skip = parseInt(query.skip);
            delete query.skip;
        }
        if (query.sortField && query.sortOrder) {
            pagination.sort = {};
            pagination.sort[query.sortField] = query.sortOrder;
            delete query.sortField;
            delete query.sortOrder;
        } else
            pagination.sort = { createdAt: -1 };
        if (query.requiredFields) {
            requiredFields = query.requiredFields;
            delete query.requiredFields;
        }

        if (query.subFields) {
            subFields = query.subFields;
            delete query.subFields;
        }
        return new Promise((resolve, reject) => {

            return database.models.consultation.count(query).then(count => {
                if (count > 0) {

                    return database.models.consultation.find(query, requiredFields, pagination).populate('relativesId patientId doctorId assignedBy', subFields).exec(function (error, result) {
                        if (error)
                            throw error;
                        resolve({
                            data: result,
                            totalCount: count,
                            count: result.length,
                            pagination: {
                                from: pagination.skip + 1,
                                to: pagination.skip + result.length
                            },
                            message: 'records found.'

                        })
                    })
                } else {
                    resolve({ data: [] })
                }

            }).catch(error => {
                reject(error);
            })

        })
    }

    _validateBeforeInitiate(req) {
        return new Promise((resolve, reject) => {
            if (!req.body.patientId)
                reject({ status: 400, message: "patientId not found." });
            else if (!req.body.relativesId)
                reject({ status: 400, message: "relativesId not found" });
            else if (!req.body.consultationType)
                reject({ status: 400, message: "consultationType not found" });
            else if (!req.body.consultationLanguage)
                reject({ status: 400, message: "consultationLanguage not found" });
            else if (!req.body.specializationCategory)
                reject({ status: 400, message: "specializationCategory not found" });
            return database.models.relatives.findOne({ _id: req.body.relativesId }).populate({ path: 'patientId', model: 'patient' }).exec(function (error, result) {
                if (error)
                    reject(error);
                else {
                    if (result && result.patientId)
                        resolve(result);
                    else
                        reject({ status: 400, message: "Invalid data" });
                }
            })
        });
    }

   

    _findById(_id) {
        return new Promise((resolve, reject) => {
            let fields = 'emailid, firstName name age gender';
            return database.models.consultation.findById(_id, '-__v -deleted -updatedAt -createdAt -feedback -logicalDelete -rejectedBy').populate('relativesId patientId doctorId', fields).exec(function (error, result) {
                if (error)
                    throw error;
                resolve(result);
            })
        })
    }

    //To get consultation count

    getConsultationCounts(req) {
        let query = req.query != undefined ? req.query : req;
        let start = moment().startOf('day'); // set to 12:00 am today
        let end = moment().endOf('day'); // set to 23:59 pm today
        let startFortNight = moment().subtract(14, 'days').startOf('day') // start of day before 14 days
        //today
        let queries = [{ type: 'today', query: { doctorId: query.doctorId, assignedAt: { '$gte': start, '$lte': end } } },
        { type: 'fortnight', query: { doctorId: query.doctorId, assignedAt: { '$gte': startFortNight, '$lte': end } } },
        { type: 'overall', query: { doctorId: query.doctorId } }
        ]
        return this._count(queries).then(result => {
            return { data: result };
        }).catch(error => {
            throw error;
        })

    }

    //to get count
    _count(queries) {
        //array of queries
        return new Promise((resolve, reject) => {
            let promises = [],
                results = [];
            queries.forEach(function (item) {
                promises.push(
                    database.models.consultation.count(item.query).then(result => {
                        results.push({ type: item.type, count: result });
                    }).catch((error) => {
                        results.push({ type: item.type, count: 0 });
                    })
                );

            })

            Promise.all(promises).then(() => {
                resolve(results);
            }).catch(error => {
                resolve(results);
            });
        })

    }

    getByPatientId(req) {
        let patientId = req.body != undefined ? req.body.patientId : req;

        return new Promise((resolve, reject) => {
            return database.models.consultation.find({ "patientId": patientId }).then(result => {
                if (result)
                    resolve({ status: 200, data: result });
                else
                    resolve({ status: 200, message: "No data exists" });
            }).catch(error => {
                if (error.status == 404)
                    reject({ status: 404 })
                else
                    reject({ status: 400, message: error.message });
            });

        });
    }

    getPaidConsultationByPatientId(req) {
        let patientId = req.body != undefined ? req.body.patientId : req;

        return new Promise((resolve, reject) => {
            return database.models.consultation.find({ "paymentStatus": "Paid", "patientId": patientId }).then(result => {
                if (result)
                    resolve({ status: 200, data: result });
                else
                    resolve({ status: 200, message: "No data exists" });
            }).catch(error => {
                if (error.status == 404)
                    reject({ status: 404 })
                else
                    reject({ status: 400, message: error.message });
            });

        });
    }


    _toObject(obj, fields){
        try{
             obj = obj.toObject();
             for(let i=0; i<fields.length; i++)
                delete obj[fields[i]];
            return obj;
        }catch(e){
            return obj;
        }
    }

}