module.exports = class Notification extends AbstractController {
    constructor(schema) {
        super(schema);
    }

    static get name() {
        return "Notification";
    }

    _create(data) {
        return new Promise((resolve, reject) => {
            return database.models.notification.findOne({
                //sourceObject: data.sourceObject,
                targetObject: data.targetObject,
                consultationId: data.consultationId,
                status: 'new'
            }).then(result => {
                if (result)
                    resolve({ status: 409, message: 'already exists', data: result });
                else {
                    return database.models.notification.create(data).then(result => {
                        resolve({ message: 'created successfully.', data: result });
                    }).catch(error => {
                        throw error;
                    })
                }

            }).catch(error => {
                reject(error);
            })
        })
    }


    _update(query, params) {
        return new Promise((resolve, reject) => {
            return database.models.notification.update(query, params, { new: true }).then(result => {
                resolve({ message: 'updated successfully.', data: result.ok });
            }).catch(error => {
                reject(error);
            })
        })
    }

    _updateMultple(query, params) {
        return new Promise((resolve, reject) => {
            return database.models.notification.update(query, params, {multi: true}).then(result => {
                resolve({ message: 'updated successfully.', data: result.ok });
            }).catch(error => {
                reject(error);
            })
        })
    }

    _findOneAndUpdate(query, params){
        return database.models.notification.findOneAndUpdate(query, params, { new: true }).then(result => {
            return result;
        }).catch(error => {
            throw error;
        })
    }

}


