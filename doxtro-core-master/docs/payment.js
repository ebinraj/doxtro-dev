/**
   * @api {post} payment/capture capture payment
   * @apiHeader {String} Authorization token
   * @apiGroup Payment
   * @apiDescription capture
   * @apiName capture
   * @apiVersion 0.0.1
   * @apiParam {String} consultationId consultationId
   * @apiParam {String} payment_id paymentId received from payment gateway
   * @apiParam {String} [couponId] id of coupon if applied
   * @apiParam {String} [couponTag] name of coupon if applied
   * @apiSampleRequest payment/capture
   * @apiParamExample {json} Request-Example
   *  {
	 *    "payment_id" : "pay_8kFtZcAGtOMpcN",
	 *    "consultationId" : "59d24dcf0a00b514895a21fc",
	 *    "couponId" : "59bf94530b2c9d0f7aade0b5",
	 *    "couponUsed" : "SEP100"
   *    }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "Payment has already been captured"
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Payment is failed"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Payment is not authorized"
   * } 
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to fetch payment details"
   * } 
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No valid consultation found"
   * } 
   */