 /**
   * @api {post} doctor/signUp signUp
   * @apiGroup Doctor
   * @apiDescription signUp
   * @apiName signUp
   * @apiVersion 0.0.1
   * @apiParam {String} mobile mobile number
   * @apiParam {String} [firstName] name
   * @apiParam {String} [emailid] emailid
   * @apiSampleRequest doctor/signUp
   * @apiParamExample {json} Request-Example
   *   {
   *     "mobile": "+911111111111",
   *     "firstName" : "John",
   *     "emailid" : "john@gmail.com"
   *   }
   * @apiSuccess (Success 200) {string} message message related to response
   * @apiSuccess (Success 200) {string} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "OTP has sent to registerd mobile number."
   *  "data": {
   *      "_id": "59686e62f580992130125e9f"
   *    }
   * }
   * @apiError (Error 200) {string} message message related to response
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Mobile number not found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Already registered. Please sign In."
   *}
   * @apiErrorExample {json} Error-Response
   * {
   * "message": "Failed to send OTP to registered number.Please try again."
   * }
   */


  /**
   * @api {post} doctor/signIn signIn
   * @apiGroup Doctor
   * @apiDescription signIn
   * @apiName signIn
   * @apiVersion 0.0.1
   * @apiParam {String} mobile mobile number
   * @apiSampleRequest doctor/signIn
   * @apiParamExample {json} Request-Example
   *   {
   *     "mobile": "+911111111111"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "OTP has sent to registerd mobile number."
   *  "data": {
   *      "_id": "59686e62f580992130125e9f"
   *    }
   * }
   * @apiError (Error 200) {string} message message related to response
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Mobile number not found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Not registered. Please sign Up."
   *}
   * @apiErrorExample {json} Error-Response
   * {
   * "message": "Failed to send OTP to registered number.Please try again."
   * }
   */


  /**
   * @api {post} doctor/verify verify 
   * @apiGroup Doctor
   * @apiDescription verify 
   * @apiName verify
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiParam {String} otp otp
   * @apiParam {String="iphone", "android"} typeOfPhone 
   * @apiParam {String} deviceId device token
   * @apiSampleRequest doctor/verify
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "otp" : "136992",
   *    "typeOfPhone" : "android",
   *    "deviceId" : "sample"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   * {
   * "message": "Account verified successfully",
   * "data": {
   *     "_id": "592d1c7afd590a237760bb0f",
   *     "mobile": "+919740324448",
   *     "accountActivationTime": "2017-07-14T07:19:27.753Z",
   *     "accountStatus": "active",
   *     "tag": "doctor",
   *     "status" : "pending",
   *     "deviceId" : "sample",
   *     "token" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTc1YmY1N2NlZDM1NTVlYWFkYTQ0ZTMiLCJ0YWciOiJkb2N0b3IiLCJpYXQiOjE1MDA4ODkwMzF9.TF5hdqnLBVh2ZF-PDcZftTKkYb1XyyPb1qBG-QIoKYU"
   *  }
   * }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "Invalid data"
   *   }
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "OTP Expired.Please try again."
   *   }
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "Invalid OTP."
   *   }
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "Failed to verify OTP, Please try again."
   *   }
   */


  /**
   * @api {put} doctor/profile profile
   * @apiHeader {String} Authorization token
   * @apiGroup Doctor
   * @apiDescription profile
   * @apiName profile
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiParam {String} [firstName] firstName
   * @apiParam {String} [lastName] lastName
   * @apiParam {String} [emailid]emailid
   * @apiParam {String} [duns] MCI number
   * @apiParam {String="Male", "Female"} [gender] gender
   * @apiParam {String[]} [specialization] specialization
   * @apiParam {String[]} [category] category of specialization
   * @apiParam {Object[]} [documents] documents
   * @apiParam {Object[]} [bankDetails] bankDetails
   * @apiParam {Object[]} [language] Language known
   * @apiParam {String} [experience] years of experience
   * @apiParam {String} [bio] bio
   * @apiParam {String} [profilePic] profilePic url
   * @apiParam {String} [qualification] qualification
   * @apiParam {String} [expertise] expertise
   * @apiParam {String} [memberships] memberships
   * @apiParam {String} [WorksFor] WorksFor
   * @apiSampleRequest doctor/validate/redirect/profile
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "firstName" : "Dr.John",
   *    "gender" : "Male",
   *    "age" : "32",
   *    "specialization" : ["General Physician", "Biological Psychiatrist"],
   *    "category" : ["General Physician", "Psychiatrist / Mental Health"],
   *    "language" : ["English", "Hindi"],
   *    "experience" : "5",
   *    "bio" : "I am General Physician",
   *    "profilePic" : "gs://doxtrodevelopment.appspot.com/profilepic.jpg",
   *    "documents" :[{
	 *	      "tag" : "idProof",
	 *	      "url" : "http://myid.jpeg"
	 *      },
	 *      {
	 *	      "tag" : "certificates",
	 *	      "url" : "http://mycer.jpeg"
	 *      }],
   * 	  "bankDetails" : [
	 *		  {
	 *	      "IFSC" : "ABCD00000",
   * 	      "accountHolderName" : "Dr.John",
   * 	      "accountNumber" : "424242424242",
   *        "bank" : "HDFC"
	 *    	}]
   *    }
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   * "message": "doctor record updated successfully",
   * "profilePercentage": 64,
   * "data": {
   *     "_id": "592d1c7afd590a237760bb0f",
   *     "mobile": "+919740324448",
   *     "firstName": "John",
   *     "gender": "Male",
   *     "age": "32",
   *     "accountActivationTime": "2017-07-14T07:19:27.753Z",
   *     "accountStatus": "active",
   *     "tag": "doctor",
   *     "status" : "pending",
   *    "specialization" : ["General Physician", "Biological Psychiatrist"],
   *    "category" : ["General Physician", "Psychiatrist / Mental Health"],
   *    "language" : ["English", "Hindi"],
   *    "experience" : "5",
   *    "bio" : "I am General Physician",
   *    "profilePic" : "gs://doxtrodevelopment.appspot.com/profilepic.jpg"
   *    "documents": [
   *         {
   *             "url": "http://myid.jpeg",
   *             "tag": "idProof",
   *             "_id": "597194d7fc04991b46ca4ed0",
   *            "status": "pending"
   *         },
   *         {
   *             "url": "http://mycer.jpeg",
   *             "tag": "certificates",
   *             "_id": "597194d7fc04991b46ca4ecf",
   *             "status": "pending"
   *         }],
   *      "bankDetails": [
   *         {
   *             "accountNumber": "424242424242",
   *             "accountHolderName": "Dr.John",
   *             "IFSC": "ABCD00000",
   *             "_id": "597194d7fc04991b46ca4ece"
   *         }]
   *      }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found"
   * }
   */


   /**
   * @api {post} doctor/getProfile getProfile
   * @apiHeader {String} Authorization token
   * @apiGroup Doctor
   * @apiDescription getProfile
   * @apiName getProfile
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiSampleRequest doctor/validate/redirect/getProfile
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Number} profile percentage count
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   * "message": "doctor record found successfully",
   * "profilePercentage": 64,
   * "data": {
   *     "profile": {
   *         "_id": "5975a4a4540798368b5d11f3",
   *         "mobile": "+912222222222",
   *         "emailid": "priyanka.tendulkar@above-inc.com",
   *         "firstName": "abcde",
   *         "experience": "4",
   *         "bankDetails": [],
   *         "documents": [],
   *         "accountActivationTime": "2017-07-24T07:40:53.984Z",
   *         "availabilityStatus": "available",
   *         "status": "pending",
   *         "accountStatus": "active",
   *         "language": [],
   *         "category": [
   *             "General Physician"
   *         ],
   *         "specialization": [
   *             "General Physician"
   *         ],
   *         "tag": "doctor"
   *     },
   *     "availability": [
   *         {
   *             "_id": "5975a4e6540798368b5d11f5",
   *             "dayOfWeek": "Friday",
   *             "startTime": 930,
   *             "endTime": 1300
   *         },
   *         {
   *             "_id": "597716d40992a152f6b3f68a",
   *             "dayOfWeek": "Tuesday",
   *             "startTime": 900,
   *             "endTime": 2300
   *         }
   *     ]}
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to fetch doctor details"
   * }
   */




   /**
   * @api {post} availability/create availability create/update
   * @apiHeader {String} Authorization token
   * @apiGroup Doctor
   * @apiDescription create
   * @apiName create
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiParam {Object[]} operationalHours availability
   * @apiSampleRequest availability/validate/redirect/create
   * @apiParamExample {json} Request-Example
   *{
	 *  "_id" : "596dd6e3298f3e44ce6cf8f1",
   * "operationalHours" : [
   *   {
	 *	"dayOfWeek": "Wednesday",
   * 	"startTime": 1100,
   * 	"endTime": 1500
	 *	},
	 *{
   * 	"dayOfWeek": "Friday",
   * 	 "startTime": 930,
   * 	"endTime": 1300
   *  }]
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   * "message": "Created successfully.",
   * "data": [
   *     {
   *         "dayOfWeek": "Wednesday",
   *         "startTime": 1100,
   *         "endTime": 1500,
   *         "status": "available",
   *         "userId": "596dd6e3298f3e44ce6cf8f1"
   *     }
   *     {
   *         "dayOfWeek": "Friday",
   *         "startTime": 930,
   *         "endTime": 1300,
   *         "status": "available",
   *         "userId": "596dd6e3298f3e44ce6cf8f1"
   *     }
   *   ]
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Availability not provided."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No data found."
   * }
   */

   /**
   * @api {post} doctor/updateDeviceId update DeviceId
   * @apiHeader {String} Authorization token
   * @apiGroup Doctor
   * @apiDescription updateDeviceId
   * @apiName updateDeviceId
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiParam {String} deviceId deviceId
   * @apiParam {String} typeOfApp="doctor" type Of App
   * @apiParam {String="iphone", "android"} typeOfPhone type Of Phone
   * @apiSampleRequest doctor/validate/redirect/updateDeviceId
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "typeOfApp" : "doctor",
   *    "typeOfPhone" : "android",
   *    "deviceId" : "sample"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "updated successfully.",
   *  "data": {
   *      "_id" : "592d1c7afd590a237760bb0f",
   *      "deviceId": "sample"
   *    }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to update."
   * } 
   */

  /**
  * @api {GET} consultation/getConsultations get Consultations
  * @apiHeader {String} Authorization token
  * @apiGroup Doctor
  * @apiDescription Get consultation list 
  * @apiName Consultation
  * @apiVersion 0.0.1
  * @apiParam {String} doctorId A valid _id of the doctor
  * @apiParam {Number} [skip] skip value for pagination
  * @apiParam {Number} [limit] value for pagination
  * @apiParam {string} [sortField] sorting field
  * @apiParam {Number = 1 -1} [sortOrder] sorting order
  * @apiSampleRequest consultation/getConsultations
  * @apiParamExample {json} Request-Example
  *   {
  *     "doctorId":"5975a4a4540798368b5d11f3",
  *     "skip" : 0,
  *     "limit" : 2
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object} pagination pagination
  * @apiSuccess (Success 200) {Number} count count 
  * @apiSuccess (Success 200) {Number} totalCount total records count
  * @apiSuccess (Success 200) {Object[]} data data
  * @apiSuccessExample {json} Success-Response
  *  {
  *     "message": "records found.",
  *     "pagination": {
  *       "from": 1,
  *       "to": 2
  *     },
  *     "totalCount": 3,
  *     "count": 2,
  *     "data": [
  *      {
  *          "_id": "597716860992a152f6b3f689",
  *          "patientId": "5971b907d0e72b3945ee78aa",
  *          "relativesId": {
  *              "_id": "597715cc0992a152f6b3f688",
  *              "name": "Rajesh Sharma",
  *              "age": "56",
  *              "gender": "Male"
  *          },
  *          "doctorId": {
  *              "_id": "5975a4a4540798368b5d11f3",
  *              "firstName": "abcde"
  *          },
  *          "consultingFor": "Father",
  *          "consultationType": "Chat",
  *          "consultationLanguage": "English",
  *          "specializationCategory": "General Physician",
  *          "note": "This is a test consultation",
  *          "status" : "New",
  *          "prescriptionId": [],
  *          "attachedDocuments": [
  *              "http://google.com",
  *              "http://google.co.in"
  *          ]
  *      },
  *      {
  *          "_id": "59788089137dfe32e7ef4ddd",
  *         "patientId": {
  *              "_id": "5971b907d0e72b3945ee78aa",
  *              "firstName": "John",
  *              "gender": "Male",
  *              "age": "25"
  *          },
  *          "relativesId": {
  *              "_id": "597715cc0992a152f6b3f688",
  *              "name": "Rajesh Sharma",
  *              "age": "56",
  *              "gender": "Male"
  *          },
  *          "consultingFor": "Father",
  *          "consultationType": "Chat",
  *          "consultationLanguage": "English",
  *          "specializationCategory": "General Physician",
  *          "note": "This is a note test",
  *          "status" : "New",
  *          "prescriptionId": ["59788089137dfe32e7ef4ddf"],
  *          "attachedDocuments": [
  *              "http://google.com",
  *              "http://google.co.in"
  *          ]
  *      }]
  *   }
  * @apiError (Error 400) {String} message message related to response
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "No consultation record found"
  * }
  */

 /**
   * @api {post} doctor/updateAvailabilityStatus update Availability Status
   * @apiHeader {String} Authorization token
   * @apiGroup Doctor
   * @apiDescription updateAvailabilityStatus
   * @apiName updateAvailabilityStatus
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiParam {String="offline", "available"} availabilityStatus availabilityStatus
   * @apiSampleRequest doctor/validate/redirect/updateAvailabilityStatus
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "availabilityStatus" : "offline"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *  {
   *    "message": "Availability status changed to offline",
   *    "data": {
   *      "availabilityStatus": "offline",
   *      "_id": "5975a2cc498bf2335adb4ca7"
   *    }
   * }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to update."
   * } 
   */

   /**
  * @api {GET} consultation/getRequests get today's consultation Requests
  * @apiHeader {String} Authorization token
  * @apiGroup Doctor
  * @apiDescription get today's consultation Requests
  * @apiName getRequests
  * @apiVersion 0.0.1
  * @apiParam {String} doctorId A valid _id of the doctor
  * @apiSampleRequest consultation/getRequests
  * @apiParamExample {json} Request-Example
  *   {
  *     "doctorId":"5975a4a4540798368b5d11f3"
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object[]} data data
  * @apiSuccessExample {json} Success-Response
  *  {
  *     "message": "requests found.",
  *     "data": [
  *      {
  *          "_id": "597716860992a152f6b3f689",
  *          "patientId": "5971b907d0e72b3945ee78aa",
  *          "relativesId": {
  *              "_id": "597715cc0992a152f6b3f688",
  *              "name": "Rajesh Sharma",
  *              "age": "56",
  *              "gender": "Male"
  *          },
  *          "consultingFor": "Father",
  *          "consultationType": "Chat",
  *          "consultationLanguage": "English",
  *          "specializationCategory": "General Physician",
  *          "note": "This is a test consultation",
  *          "status" : "New",
  *          "prescriptionId": [],
  *          "attachedDocuments": [
  *              "http://google.com",
  *              "http://google.co.in"
  *          ]
  *      }]
  *   }
  * @apiError (Error 400) {String} message message related to response
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "No consultation request found"
  * }
  */


   /**
  * @api {POST} consultation/sendResponse send response to consultation request
  * @apiHeader {String} Authorization token
  * @apiGroup Doctor
  * @apiDescription send response to consultation request
  * @apiName sendResponse
  * @apiVersion 0.0.1
  * @apiParam {String} doctorId A valid _id of the doctor
  * @apiParam {String} consultationId consultationId
  * @apiParam {String="accept", "reject"} responseText respose from doctor
  * @apiSampleRequest consultation/sendResponse
  * @apiParamExample {json} Request-Example
  *   {
  *     "doctorId":"5975a4a4540798368b5d11f3",
  *     "consultationId" : "597716860992a152f6b3f689",
  *     "responseText" : "accept"
  *   }
  * @apiParamExample {json} Request-Example
  *   {
  *     "doctorId":"5975a4a4540798368b5d11f3",
  *     "consultationId" : "597716860992a152f6b3f689",
  *     "responseText" : "reject"
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object} data data
  * @apiSuccessExample {json} Success-Response
  *  {
  *     "message": "assigned successfully",
  *     "data": {
  *          "consultationId" : "597716860992a152f6b3f689"
  *        }
  *   }
  * @apiSuccessExample {json} Success-Response
  *  {
  *     "message": "consultation request is rejected",
  *     "data": {
  *          "consultationId" : "597716860992a152f6b3f689"
  *        }
  *   }
  * @apiError (Error 400) {String} message message related to error
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "consultationId not provided"
  * }
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "Invalid responseText"
  * }
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "This consultation request is already accepted"
  * }
  */

    /**
   * @api {post} doctor/updateNumber update mobile Number
   * @apiGroup Doctor
   * @apiDescription update mobile Number
   * @apiName updateNumber
   * @apiVersion 0.0.1
   * @apiParam {String} _id valid id of user
   * @apiParam {String} oldMobile old mobile number
   * @apiParam {String} mobile new mobile number
   * @apiSampleRequest doctor/updateNumber
   * @apiParamExample {json} Request-Example
   *{
	 *   "_id" : "5975bf57ced3555eaada44e3",
	 *   "oldMobile" : "+919740324448",
	 *   "mobile" : "+918123632112"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "OTP has sent to registerd mobile number."
   *  "data": {
   *      "_id": "5975bf57ced3555eaada44e3"
   *    }
   * }
   * @apiError (Error 200) {String} message message related to response
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Mobile number not found"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Invalid mobile number"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Number is already registered"
   *}
   * @apiErrorExample {json} Error-Response
   * {
   * "message": "Failed to send OTP to registered number.Please try again."
   * }
   */