 /**
  * @api {GET} settings/getLanguages get Languages(List)
  * @apiHeader {String} Authorization token
  * @apiGroup Settings
  * @apiDescription get Languages(List)
  * @apiName get Languages(List)
  * @apiVersion 0.0.1
  * @apiSampleRequest settings/getLanguages
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {String[]} data data
  * @apiSuccessExample {json} Success-Response
  * {
  *   "data": [
  *       "English",
  *       "Hindi",
  *       "Tamil",
  *       "Telugu",
  *       "Kannada"
  *   ]
  * }
  */


  /**
  * @api {GET} settings/getDegree get Degree(List)
  * @apiHeader {String} Authorization token
  * @apiGroup Settings
  * @apiDescription get Degree(List)
  * @apiName getDegree(List)
  * @apiVersion 0.0.1
  * @apiSampleRequest settings/getDegree
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {String[]} data data
  * @apiSuccessExample {json} Success-Response
  * {
  *   "message" : "record found",
  *   "data": [
  *      "MBBS",
  *      "BSc",
  *      "MD",
  *      "BAMS",
  *      "BUMS",
  *      "BHMS",
  *      "BSMS",
  *      "BMS",
  *      "BA",
  *      "B com",
  *      "BS",
  *      "BDS",
  *      "MDS",
  *      "MSc",
  *      "MS",
  *      "DDVL",
  *      "DNB",
  *      "DGO",
  *      "DVD",
  *      "DLO",
  *      "D Ortho",
  *      "DO/DOMS",
  *      "DCH",
  *      "DPM"
  *   ]
  * }
  */

  /**
   * @api {post} settings/create create charges setting
   * @apiHeader {String} Authorization token
   * @apiGroup Settings
   * @apiDescription create
   * @apiName create
   * @apiVersion 0.0.1
   * @apiParam {String} category category
   * @apiParam {Object} charges object of charges with following values
   * @apiParam {Number} charges.consultationFee consultationFee
   * @apiParam {Number} charges.serviceCharge serviceCharge
   * @apiParam {Number} charges.doctorFee doctorFee
   * @apiSampleRequest settings/create
   * @apiParamExample {json} Request-Example
   *   {
   *     "category": "General Physician",
   *     "charges" : {
   *        "consultationFee"" : 200,
   *        "serviceCharge" : 2,
   *        "doctorFee"" : 100
   *       }
   *   }
   * @apiSuccess (Success 200) {string} message message related to response
   * @apiSuccess (Success 200) {string} data data
   * @apiSuccessExample {json} Success-Response
   *   {
   *     "message": "created successfully",
   *     "data": {
   *         "__v": 0,
   *         "createdAt": "2017-10-03T07:34:34.559Z",
   *         "updatedAt": "2017-10-03T07:34:34.559Z",
   *         "category": "General Physician",
   *         "deleted": false,
   *         "_id": "59d33d8a84c14d3887ee5d5d",
   *         "charges": {
   *             "consultationFee": 200,
   *             "serviceCharge": 2,
   *             "doctorFee": 100
   *         },
   *         "entityArray": []
   *     }
   * }
   */

   /**
   * @api {post} settings/find find
   * @apiHeader {String} Authorization token
   * @apiGroup Settings
   * @apiDescription find
   * @apiName find
   * @apiVersion 0.0.1
   * @apiSampleRequest settings/find
   * @apiSuccess (Success 200) {string} message message related to response
   * @apiSuccess (Success 200) {string} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *    "message": "record found",
   *    "data": [{
   *            "_id": "59d33d8a84c14d3887ee5d5d",
   *            "createdAt": "2017-10-03T07:34:34.559Z",
   *            "updatedAt": "2017-10-03T07:34:34.559Z",
   *            "category": "General Physician",
   *            "__v": 0,
   *            "deleted": false,
   *            "charges": {
   *                "consultationFee": 200,
   *                "serviceCharge": 2,
   *                "doctorFee": 100
   *            },
   *            "entityArray": []
   *        }]
   *}
   */


  /**
   * @api {post} settings/update update charges setting
   * @apiHeader {String} Authorization token
   * @apiGroup Settings
   * @apiDescription update
   * @apiName update
   * @apiVersion 0.0.1
   * @apiParam {String} _id _id of record
   * @apiParam {Object} charges object of charges with following values
   * @apiParam {Number} charges.consultationFee consultationFee
   * @apiParam {Number} charges.serviceCharge serviceCharge
   * @apiParam {Number} charges.doctorFee doctorFee
   * @apiSampleRequest settings/update
   * @apiParamExample {json} Request-Example
   *   {
   *     "_id": "59d33d8a84c14d3887ee5d5d",
   *     "charges" : {
   *        "consultationFee"" : 100,
   *        "serviceCharge" : 2,
   *        "doctorFee"" : 100
   *       }
   *   }
   * @apiSuccess (Success 200) {string} message message related to response
   * @apiSuccess (Success 200) {string} data data
   * @apiSuccessExample {json} Success-Response
   *   {
   *     "message": "updated successfully",
   *     "data": {
   *         "__v": 0,
   *         "createdAt": "2017-10-03T07:34:34.559Z",
   *         "updatedAt": "2017-10-03T07:34:34.559Z",
   *         "category": "General Physician",
   *         "deleted": false,
   *         "_id": "59d33d8a84c14d3887ee5d5d",
   *         "charges": {
   *             "consultationFee": 100,
   *             "serviceCharge": 2,
   *             "doctorFee": 100
   *         },
   *         "entityArray": []
   *     }
   * }
   */
