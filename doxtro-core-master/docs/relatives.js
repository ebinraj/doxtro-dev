 /**
  * @api {POST} relatives/create Create Members
  * @apiHeader {String} Authorization token
  * @apiGroup Patient
  * @apiDescription Create Relative Members
  * @apiName Create Members
  * @apiVersion 0.0.1
  * @apiParam {ObjectId} patientId
  * @apiParam {String} relation relation with patient
  * @apiParam {String} name name of relative
  * @apiParam {Number} age age of relative
  * @apiParam {String} gender gender of relative
  * @apiSampleRequest relatives/create
  * @apiParamExample {json} Request-Example
  *   {
  *     "patientId":"596f095febee38d36c343198",
  *     "relation":"Father",
  *     "name":"Rajesh Sharma",
  *     "age":56,
  *     "gender":"Male"
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object} data data
  * @apiSuccessExample {json} Success-Response
  *{
  *  "message": "New Member Added"
  *   "data": {
  *          "_id": "597715cc0992a152f6b3f688",
  *          "createdAt": "2017-07-25T09:56:28.164Z",
  *          "updatedAt": "2017-07-25T09:56:28.164Z",
  *          "patientId": "5971b907d0e72b3945ee78aa",
  *          "relation": "Father",
  *          "name": "Rajesh Sharma",
  *          "age": "56",
  *          "gender": "Male",
  *          "__v": 0,
  *          "deleted": false
  *      }
  * }
  * @apiError (Error 400) {String} message message related to response
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "patientId does not exists"
  * }
  * @apiErrorExample {json} Error-Repsonse(Invalid PatientId)
  * {
  *     "message": "Cast to ObjectID failed for value \"5968d36c343198\" at path \"patientId\""
  * }
  */



   /**
  * @api {GET} relatives/getRelatives get Relatives List
  * @apiHeader {String} Authorization token
  * @apiGroup Patient
  * @apiDescription get Relatives
  * @apiName get Relatives
  * @apiVersion 0.0.1
  * @apiParam {String} patientId _id of patient
  * @apiSampleRequest relatives/getRelatives
  * @apiParamExample {json} Request-Example
  *   {
  *     "patientId":"5971b907d0e72b3945ee78aa"
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object} pagination pagination
  * @apiSuccess (Success 200) {Number} count count 
  * @apiSuccess (Success 200) {Number} totalCount total records count
  * @apiSuccess (Success 200) {Object} data data
  * @apiSuccessExample {json} Success-Response
  *{
  *  "message": "relatives's record found successfully",
  *  "pagination": {
  *      "from": 1,
  *      "to": 1
  *  },
  *  "totalCount": 1,
  *  "count": 1,
  *  "data": [
  *      {
  *          "_id": "597715cc0992a152f6b3f688",
  *          "createdAt": "2017-07-25T09:56:28.164Z",
  *          "updatedAt": "2017-07-25T09:56:28.164Z",
  *          "patientId": "5971b907d0e72b3945ee78aa",
  *          "relation": "Father",
  *          "name": "Rajesh Sharma",
  *          "age": "56",
  *          "gender": "Male",
  *          "__v": 0,
  *          "deleted": false
  *      }
  * ]
  *  }
  * @apiError (Error 400) {String} message message related to error
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "patientId not found"
  * }
  * @apiErrorExample {json} Error-Repsonse(Invalid PatientId)
  * {
  *     "message": "No relatives record found"
  * }
  */

    /**
   * @api {put} relatives/update update members
   * @apiHeader {String} Authorization token
   * @apiGroup Patient
   * @apiDescription update
   * @apiName update
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of relative member
   * @apiParam {String} [name] name
   * @apiParam {String="Male", "Female"}[gender] gender
   * @apiParam {String} [age] age
   * @apiSampleRequest relatives/update
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "name" : "John",
   *    "gender" : "Male",
   *    "age" : "32"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   * "message": "Updated successfully",
   * "data": {
   *     "_id": "592d1c7afd590a237760bb0f",
   *    "name" : "John",
   *     "gender": "Male",
   *     "age": "32"
   *    }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "No record found to update"
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "relation cannot be updated"
   * }
   */
