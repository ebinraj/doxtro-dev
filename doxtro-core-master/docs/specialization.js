 /**
  * @api {get} specialization/getCategories List of Specialization / Category
  * @apiHeader {String} Authorization token
  * @apiGroup Patient
  * @apiDescription List of Specialization/Categories
  * @apiName Specialization
  * @apiVersion 0.0.1
  * @apiSampleRequest specialization/getCategories
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object[]} data data
  * @apiSuccessExample {json} Success-Response
  *{
  *   "message": "Distinct Categories",
  *   "data": [
  *             {
  *               "category": "test cat",
  *               "description": "Lorem ipsum dolor sit amet, nam dicant deterruisset ex. Ex sea minim expetenda, solum posidonium in eam, vim eros erant dignissim ut. Ei vim nullam mentitum praesent, pro causae fuisset tincidunt no. Et sint definiebas quo, per stet deseruisse at."
  *            }
  *    ]
  * }
  */

 /**
  * @api {get} specialization/getAllSpecialization List of Specialization
  * @apiHeader {String} Authorization token
  * @apiGroup Doctor
  * @apiDescription List of Specialization/Categories
  * @apiName Specialization
  * @apiVersion 0.0.1
  * @apiSampleRequest specialization/getAllSpecialization
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object[]} data data
  * @apiSuccessExample {json} Success-Response
  *{
  *   "message": "List of Specialization by Category",
  *   "data": [
  *       {
  *           "_id": "test cat",
  *           "specializations": [
  *               "test spec"
  *           ]
  *        }
  *    ]
  * }
  */