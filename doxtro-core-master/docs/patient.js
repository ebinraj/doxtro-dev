 /**
   * @api {post} patient/signUp signUp
   * @apiGroup Patient
   * @apiDescription signUp
   * @apiName signUp
   * @apiVersion 0.0.1
   * @apiParam {string} mobile mobile number
   * @apiParam {String} [firstName] name
   * @apiParam {String} [emailid] emailid
   * @apiSampleRequest patient/signUp
   * @apiParamExample {json} Request-Example
   *   {
   *     "mobile": "+911111111111"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "OTP has sent to registerd mobile number."
   *  "data": {
   *      "_id": "59686e62f580992130125e9f"
   *    }
   * }
   * @apiError (Error 200) {String} message message related to response
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Mobile number not found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Already registered. Please sign In."
   *}
   * @apiErrorExample {json} Error-Response
   * {
   * "message": "Failed to send OTP to registered number.Please try again."
   * }
   */


  /**
   * @api {post} patient/signIn signIn
   * @apiGroup Patient
   * @apiDescription signIn
   * @apiName signIn
   * @apiVersion 0.0.1
   * @apiParam {String} mobile mobile number
   * @apiSampleRequest patient/signIn
   * @apiParamExample {json} Request-Example
   *   {
   *     "mobile": "+911111111111"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "OTP has sent to registerd mobile number."
   *  "data": {
   *      "_id": "59686e62f580992130125e9f"
   *    }
   * }
   * @apiError (Error 200) {String} message message related to response
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Mobile number not found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Not registered. Please sign Up."
   *}
   * @apiErrorExample {json} Error-Response
   * {
   * "message": "Failed to send OTP to registered number.Please try again."
   * }
   */


  /**
   * @api {post} patient/verify verify
   * @apiGroup Patient
   * @apiDescription verify patient
   * @apiName verify
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of patient
   * @apiParam {String} otp otp
   * @apiParam {String="iphone", "android"} typeOfPhone 
   * @apiParam {String} deviceId device token
   * @apiSampleRequest patient/verify
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "otp" : "136992",
   *    "typeOfPhone" : "android",
   *    "deviceId" : "sample"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   * {
   * "message": "Account verified successfully",
   * "data": {
   *     "_id": "5968711fb90f222569d46f20",
   *     "mobile": "+919740324448",
   *     "accountActivationTime": "2017-07-14T07:19:27.753Z",
   *     "accountStatus": "active",
   *     "tag": "patient",
   *     "deviceId" : "sample",
   *     "token" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTc1YmY1N2NlZDM1NTVlYWFkYTQ0ZTMiLCJ0YWciOiJkb2N0b3IiLCJpYXQiOjE1MDA4ODkwMzF9.TF5hdqnLBVh2ZF-PDcZftTKkYb1XyyPb1qBG-QIoKYU"
   *  }
   * }
   * @apiError (Error 400) {string} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "Invalid data"
   *   }
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "OTP Expired.Please try again."
   *   }
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "Invalid OTP."
   *   }
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "Failed to verify OTP, Please try again."
   *   }
   */


  /**
   * @api {put} patient/profile profile
   * @apiHeader {String} Authorization token
   * @apiGroup Patient
   * @apiDescription profile
   * @apiName profile
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of patient
   * @apiParam {String} [firstName] firstName
   * @apiParam {String} [lastName] lastName
   * @apiParam {String} [emailid] emailid
   * @apiParam {String="Male", "Female"}[gender] gender
   * @apiParam {String} [age] age
   * @apiParam {String} [profilePic] profilePic url
   * @apiSampleRequest patient/validate/redirect/profile
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "firstName" : "John",
   *    "gender" : "Male",
   *    "age" : "32"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   * "message": "patient record updated successfully",
   * "data": {
   *     "_id": "592d1c7afd590a237760bb0f",
   *     "mobile": "+919740324448",
   *     "firstName": "John",
   *     "gender": "Male",
   *     "age": "32",
   *     "accountActivationTime": "2017-07-14T07:19:27.753Z",
   *     "accountStatus": "active",
   *     "tag": "patient"
   *    }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No patient record found"
   * }
   */


  /**
   * @api {post} patient/getProfile getProfile
   * @apiHeader {String} Authorization token
   * @apiGroup Patient
   * @apiDescription getProfile
   * @apiName getProfile
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of patient
   * @apiSampleRequest patient/validate/redirect/getProfile
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   * "message": "patient record found successfully",
   * "data": {
   *     "_id": "592d1c7afd590a237760bb0f",
   *     "mobile": "+919740324448",
   *     "firstName": "John",
   *     "gender": "Male",
   *     "age": "32",
   *     "accountActivationTime": "2017-07-14T07:19:27.753Z",
   *     "accountStatus": "active",
   *     "tag": "patient"
   *    }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No patient record found"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to fetch patient details"
   * }
   */

  /**
   * @api {post} patient/updateDeviceId update DeviceId
   * @apiHeader {String} Authorization token
   * @apiGroup Patient
   * @apiDescription updateDeviceId
   * @apiName updateDeviceId
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of patient
   * @apiParam {String} deviceId deviceId
   * @apiParam {String} typeOfApp="patient" type Of App
   * @apiParam {String="iphone", "android"} typeOfPhone type Of Phone
   * @apiSampleRequest patient/validate/redirect/updateDeviceId
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "592d1c7afd590a237760bb0f",
   *    "typeOfApp" : "patient",
   *    "typeOfPhone" : "android",
   *    "deviceId" : "sample"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "updated successfully.",
   *  "data": {
   *      "_id" : "592d1c7afd590a237760bb0f",
   *      "deviceId": "sample"
   *    }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No patient record found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to update."
   * } 
   */

  /**
  * @api {GET} consultation/getConsultations get Consultations
  * @apiHeader {String} Authorization token
  * @apiGroup Patient
  * @apiDescription Get consultation list 
  * @apiName Consultation
  * @apiVersion 0.0.1
  * @apiParam {String} patientId A valid _id of the Patient
  * @apiParam {Number} [skip] skip value for pagination
  * @apiParam {Number} [limit] value for pagination
  * @apiParam {string} [sortField] sorting field
  * @apiParam {Number = 1 -1} [sortOrder] sorting order
  * @apiSampleRequest consultation/getConsultations
  * @apiParamExample {json} Request-Example
  *   {
  *     "patientId":"5971b907d0e72b3945ee78aa",
  *     "skip" : 0,
  *     "limit" : 2
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object} pagination pagination
  * @apiSuccess (Success 200) {Number} count count 
  * @apiSuccess (Success 200) {Number} totalCount total records count
  * @apiSuccess (Success 200) {Object[]} data data
  * @apiSuccessExample {json} Success-Response
  *  {
  *     "message": "records found.",
  *     "pagination": {
  *       "from": 1,
  *       "to": 2
  *     },
  *     "totalCount": 3,
  *     "count": 2,
  *     "data": [
  *      {
  *          "_id": "597716860992a152f6b3f689",
  *          "patientId": "5971b907d0e72b3945ee78aa",
  *          "relativesId": {
  *              "_id": "597715cc0992a152f6b3f688",
  *              "name": "Rajesh Sharma",
  *              "age": "56",
  *              "gender": "Male"
  *          },
  *          "doctorId": {
  *              "_id": "5975a4a4540798368b5d11f3",
  *              "firstName": "abcde"
  *          },
  *          "consultingFor": "Father",
  *          "consultationType": "Chat",
  *          "consultationLanguage": "English",
  *          "specializationCategory": "General Physician",
  *          "note": "This is a test consultation",
  *          "status" : "New",
  *          "prescriptionId": [],
  *          "attachedDocuments": [
  *              "http://google.com",
  *              "http://google.co.in"
  *          ]
  *      },
  *      {
  *          "_id": "59788089137dfe32e7ef4ddd",
  *         "patientId": {
  *              "_id": "5971b907d0e72b3945ee78aa",
  *              "firstName": "John",
  *              "gender": "Male",
  *              "age": "25"
  *          },
  *          "relativesId": {
  *              "_id": "597715cc0992a152f6b3f688",
  *              "name": "Rajesh Sharma",
  *              "age": "56",
  *              "gender": "Male"
  *          },
  *          "consultingFor": "Father",
  *          "consultationType": "Chat",
  *          "consultationLanguage": "English",
  *          "specializationCategory": "General Physician",
  *          "note": "This is a note test",
  *          "status" : "New",
  *          "prescriptionId": ["59788089137dfe32e7ef4ddf"],
  *          "attachedDocuments": [
  *              "http://google.com",
  *              "http://google.co.in"
  *          ]
  *      }]
  *   }
  * @apiError (Error 400) {String} message message related to error
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "No consultation record found"
  * }
  */


 /**
   * @api {GET} consultation/getDoctorProfile get Doctor Profile
   * @apiHeader {String} Authorization token
   * @apiGroup Patient
   * @apiDescription getDoctorProfile
   * @apiName getDoctorProfile
   * @apiVersion 0.0.1
   * @apiParam {String} doctorId id of doctor
   * @apiSampleRequest consultation/getDoctorProfile
   * @apiParamExample {json} Request-Example
   *  {
   *    "doctorId" : "5971bdf253554f1dfcfcbca7"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   * "message": "record found",
   * "data": {
   *     "profile": {
   *         "_id": "5971bdf253554f1dfcfcbca7",
   *         "profilePic": "https://firebasestorage.googleapis.com/v0/b/doxtrodevelopment.appspot.com/o/Profile_Images%2F6DABC83A-3A57-4B6B-BE42-46ECE7E23EA5.png?alt=media&token=fb1d2ea3-ffa9-4350-bdfe-dca8b8d01f80",
   *         "firstName": "Dr. Nair",
   *         "experience": "4",
   *         "WorksFor": [
   *             {
   *                "experience": "36",
   *                 "organization": "Apollo Hospital",
   *                 "specialization": "Dermatologist"
   *             },
   *             {
   *                 "experience": "21",
   *                 "organization": "Lotus Hospital",
   *                 "specialization": "Dermatologist"
   *             }
   *         ],
   *         "qualification": [
   *             {
   *                 "college": "ABCD",
   *                  "degree": "BDS",
   *                 "type": "UG"
   *             },
   *             {
   *                 "college": "FAMT",
   *                 "degree": "MD",
   *                 "type": "PG"
   *             }
   *         ],
   *         "specialization": [
   *             "Dermatologist"
   *         ]
   *     },
   *     "feedback": [
   *         {
   *             "assignedAt": "2017-08-01T06:59:45.992Z",
   *             "userName" : "rahul",
   *         "profilePic": "https://firebasestorage.googleapis.com/v0/b/doxtrodevelopment.appspot.com/o/Profile_Images%2F6DABC83A-3A57-4B6B-BE42-46ECE7E23EA5.png?alt=media&token=fb1d2ea3-ffa9-4350-bdfe-dca8b8d01f80",
   *             "feedback": {
   *                 "text": "good doctor",
   *                 "behaviour": "good",
   *                 "rating": 4
   *             }
   *         },
   *        {
   *             "assignedAt": "2017-08-24T06:59:45.992Z",
   *             "userName" : "rahul",
   *             "profilePic": "https://firebasestorage.googleapis.com/v0/b/doxtrodevelopment.appspot.com/o/Profile_Images%2F6DABC83A-3A57-4B6B-BE42-46ECE7E23EA5.png?alt=media&token=fb1d2ea3-ffa9-4350-bdfe-dca8b8d01f80",
   *             "feedback": {
   *                "text": "good doctor",
   *                 "behaviour": "good",
   *                 "rating": 4
   *             }
   *        },
   *         {
   *             "assignedAt": "2017-08-24T06:59:45.992Z",
   *             "userName" : "rahul",
   *             "profilePic": "https://firebasestorage.googleapis.com/v0/b/doxtrodevelopment.appspot.com/o/Profile_Images%2F6DABC83A-3A57-4B6B-BE42-46ECE7E23EA5.png?alt=media&token=fb1d2ea3-ffa9-4350-bdfe-dca8b8d01f80",
   *             "feedback": {
   *                 "rating": 4,
   *                 "text": "doctor is very good",
   *                 "ratedAt": "2017-08-01T07:58:45.768Z"
   *             }
   *         }
   *     ],
   *     "consultationCount": 65,
   *     "fee": 99
   *     }
   *   }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "doctorId not provided"
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to fetch doctor details"
   * }
   */

  /**
   * @api {post} patient/updateNumber update mobile Number
   * @apiGroup Patient
   * @apiDescription update mobile Number
   * @apiName updateNumber
   * @apiVersion 0.0.1
   * @apiParam {String} _id valid id of user
   * @apiParam {String} oldMobile old mobile number
   * @apiParam {String} mobile new mobile number
   * @apiSampleRequest patient/updateNumber
   * @apiParamExample {json} Request-Example
   *{
	 *   "_id" : "5975bf57ced3555eaada44e3",
	 *   "oldMobile" : "+919740324448",
	 *   "mobile" : "+918123632112"
   *   }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "OTP has sent to registerd mobile number."
   *  "data": {
   *      "_id": "5975bf57ced3555eaada44e3"
   *    }
   * }
   * @apiError (Error 200) {String} message message related to response
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Mobile number not found"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Invalid mobile number"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Number is already registered"
   *}
   * @apiErrorExample {json} Error-Response
   * {
   * "message": "Failed to send OTP to registered number.Please try again."
   * }
   */
