/**
  * @api {GET} admin/getUsers get Users
  * @apiHeader {String} Authorization token
  * @apiGroup Admin
  * @apiDescription get Users
  * @apiName getUsers
  * @apiVersion 0.0.1
  * @apiParam {String = "patient", "doctor", "admin"} tag
  * @apiParam {Number} [skip] skip value for pagination
  * @apiParam {Number} [limit] value for pagination
  * @apiParam {String} [sortField] sorting field
  * @apiParam {Number = 1 -1} [sortOrder] sorting order
  * @apiParam {String} [searchField] search text
  * @apiSampleRequest admin/getUsers
  * @apiParamExample {json} Request-Example
  *   {
  *     "tag" : "doctor",
  *     "skip" :  0,
  *      "limit" : 2
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccess (Success 200) {Object} data data
  * @apiSuccessExample {json} Success-Response
  *{
  *  "message": "records found",
  *  "data": [
  *      {
  *          "_id": "59897a278639233ccdcca4c7",
  *          "mobile": "+918123632112",
  *          "specialization": ["Dermatologist"],
  *          "status": "pending",
  *          "accountStatus": "inactive",
  *          "accountActivationTime": "2017-08-08T08:39:57.810Z",
  *          "isOnline": false
  *      },
  *      {
  *          "_id": "59897c89c3c4b541b66504ae",
  *          "mobile": "+919740324448",
  *          "specialization": ["Dermatologist"],
  *          "status": "pending",
  *          "accountStatus": "active",
  *          "accountActivationTime": "2017-08-08T08:55:06.160Z",
  *          "isOnline": false
  *      }
  *  ],
  *  "pagination": {
  *      "from": 1,
  *      "to": 2
  *  },
  *  "totalCount": 5,
  *  "count": 2
  *  }
  * @apiError (Error 400) {String} message message related to response
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "No record found"
  * }
  */

  /**
   * @api {post} doctor/updateDocumentStatus update Document Status
   * @apiHeader {String} Authorization token
   * @apiGroup Admin
   * @apiDescription updateDocumentStatus
   * @apiName updateDocumentStatus
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiParam {String} docId id of document
   * @apiParam {String="accepted", "rejected"} status status
   * @apiParam {String} [reason] reason if rejected
   * @apiSampleRequest doctor/validate/redirect/updateDocumentStatus
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "5975a2cc498bf2335adb4ca7",
   *    "status" : "accepted",
   *    "docId" : "5975a32d498bf2335adb4cac"
   *  }
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "5975a2cc498bf2335adb4ca7",
   *    "status" : "rejected",
   *    "reason"  : "not good",
   *    "docId" : "5975a32d498bf2335adb4cac"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   * {
   *    "message": "updated successfully",
   *    "data": {
   *        "_id": "5975a2cc498bf2335adb4ca7",
   *        "status": "accepted",
   *        "docId": "5975a32d498bf2335adb4cac"
   *     }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to update."
   * } 
   */


  /**
   * @api {post} doctor/updateStatus update doctor Status
   * @apiHeader {String} Authorization token
   * @apiGroup Admin
   * @apiDescription updateStatus
   * @apiName updateStatus
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of doctor
   * @apiParam {String="approved", "rejected"} status status
   * @apiParam {String} [reason] reason if rejected
   * @apiSampleRequest doctor/validate/redirect/updateStatus
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "5975a2cc498bf2335adb4ca7",
   *    "status" : "approved"
   *  }
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "5975a2cc498bf2335adb4ca7",
   *    "status" : "rejected",
   *    "reason"  : "not good"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   * {
   *    "message": "updated successfully",
   *    "data": {
   *        "_id": "5975a2cc498bf2335adb4ca7",
   *        "status": "approved",
   *        "adminId": "5975a32d498bf2335adb4cac",
   *        "statusChangedAt": "2017-08-09T12:17:21.539Z"
   *      }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided."
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "No doctor record found."
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "pending/rejected document."
   * } 
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to update."
   * } 
   */

  /**
   * @api {post} admin/remove remove user
   * @apiHeader {String} Authorization token
   * @apiGroup Admin
   * @apiDescription remove
   * @apiName remove
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of user
   * @apiParam {String="admin", "patient", "doctor"} tag tag
   * @apiSampleRequest admin/remove
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "5975a2cc498bf2335adb4ca7",
   *    "tag" : "patient"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   * {
   *    "message": "removed successfully",
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided"
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Invalid tag"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "User Not Found"
   * }
   */

  /**
   * @api {post} admin/changeAccountStatus block/unblock user
   * @apiHeader {String} Authorization token
   * @apiGroup Admin
   * @apiDescription block/unblock user
   * @apiName changeAccountStatus
   * @apiVersion 0.0.1
   * @apiParam {String} _id id of user
   * @apiParam {string="admin", "patient", "doctor"} tag tag
   * @apiParam {string="blocked", "inactive"} accountStatus accountStatus
   * @apiSampleRequest admin/block
   * @apiParamExample {json} Request-Example
   *  {
   *    "_id" : "5975a2cc498bf2335adb4ca7",
   *    "tag" : "patient",
   *    "accountStatus" : "blocked"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccessExample {json} Success-Response
   * {
   *    "message": "updated successfully",
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "_id not provided"
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Invalid tag"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Invalid account status"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "User Not Found"
   * }
   */

  /**
   * @api {post} admin/updateDeviceId update DeviceId
   * @apiHeader {String} Authorization token
   * @apiGroup Admin
   * @apiDescription updateDeviceId
   * @apiName updateDeviceId
   * @apiVersion 0.0.1
   * @apiParam {String} deviceId deviceId
   * @apiSampleRequest admin/updateDeviceId
   * @apiParamExample {json} Request-Example
   *  {
   *    "deviceId" : "sample"
   *  }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "updated successfully.",
   *  "data": {
   *      "_id" : "592d1c7afd590a237760bb0f",
   *      "deviceId": "sample"
   *    }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Failed to update."
   * } 
   */