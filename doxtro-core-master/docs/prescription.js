/**
 * @api {POST} prescription/create Create/Update prescription
 * @apiHeader {String} Authorization token
 * @apiGroup Prescription
 * @apiDescription Create prescription
 * @apiName Create prescription
 * @apiVersion 0.0.1
 * @apiParam {String} consultationId consultationId
 * @apiParam {String} type="prescription" type
 * @apiParam {Object[]} medication Array of object which contains following values
    * @apiParam {String} name name of medicine
    * @apiParam {String} [doseValue] dose
    * @apiParam {String} [doseUnit] unit
    * @apiParam {String} duration duration
    * @apiParam {String[]} doseTime time["Morning", "Afternoon", "Night"]
    * @apiParam {String= "Before meal", "After meal"} [foodWarning] foodWarning
    * @apiParam {String} instruction instruction
    * @apiSampleRequest prescription/create
 * @apiParamExample {json} Request-Example
 *{
 *   "medication" : [ 
 *       {
 *           "name" : "amoxykind CV",
 *           "doseValue" : "375",
 *           "doseUnit" : "mg",
 *           "duration" : "5 days ",
 *           "instruction" : "note test",
 *           "doseTime" : ["Morning","Night"],
 *           "foodWarning" : "After meal"
 *       }],
 *   "consultationId" : "59788089137dfe32e7ef4ddd",
 *   "type" : "prescription"
 *  }
 *@apiSuccess (Success 200) {String} message message related to response
 * @apiSuccess (Success 200) {Object} data data
 * @apiSuccessExample {json} Success-Response
 *{
 *   "message": "created successfully",
 *   "data": {
 *      "consultationId": "59788089137dfe32e7ef4ddd",
 *       "type": "prescription",
 *       "_id": "59a500ee2395a51980e20eb0",
 *       "medication": [
 *           {
 *                "foodWarning": "After meal",
 *                "doseTime" : ["Morning", "Night"],
 *                "instruction": "note test",
 *                "duration": "5 days ",
 *                "doseUnit": "mg",
 *                "doseValue": "375",
 *                "name": "amoxykind CV"
 *            }
 *        ],
 *        "date": "2017-08-29T05:51:33.833Z"
 *    }
 *   }
 * @apiError (Error 400) {string} message message related to response
 * @apiErrorExample {json} Validation Error
 * {
 *     "message": "No consultation found"
 * }
 */

/**
 * @api {POST} prescription/create Create/Update Diagnostic Test
 * @apiHeader {String} Authorization token
 * @apiGroup Prescription
 * @apiDescription Create Diagnostic Test
 * @apiName Create Diagnostic Test
 * @apiVersion 0.0.1
 * @apiParam {String} consultationId consultationId
 * @apiParam {String} type="diagnostic" type
 * @apiParam {Object[]} diagnostic Array of diagnostic tests to be submitted which contains following values
    * @apiParam {String} name name of test
    * @apiParam {String} instruction instruction
    * @apiSampleRequest prescription/create
 * @apiParamExample {json} Request-Example
 *{
 *   "diagnostic" : [ 
 *       {
 *           "name" : "X-Ray",
 *           "instruction" : "test instruction"
 *       }],
 *   "consultationId" : "59788089137dfe32e7ef4ddd",
 *   "type" : "diagnostic"
 *  }
 * @apiSuccess (Success 200) {String} message message related to response
 * @apiSuccess (Success 200) {Object} data data
 * @apiSuccessExample {json} Success-Response
 *{
 *   "message": "created successfully",
 *   "data": {
 *      "consultationId": "59788089137dfe32e7ef4ddd",
 *       "type": "diagnostic",
 *       "_id": "59a500ee2395a51980e20eb0",
 *       "diagnostic": [{
 *           "name" : "X-Ray",
 *           "instruction" : "test instruction"
 *            }],
 *        "date": "2017-08-29T05:51:33.833Z"
 *    }
 *   }
 * @apiError (Error 400) {String} message message related to response
 * @apiErrorExample {json} Validation Error
 * {
 *     "message": "No consultation found"
 * }
 */

/**
 * @api {GET} prescription/find find prescription and diagnostic test
 * @apiHeader {String} Authorization token
 * @apiGroup Prescription
 * @apiDescription find prescription
 * @apiName find
 * @apiVersion 0.0.1
 * @apiParam {String} consultationId consultationId
 * @apiSampleRequest prescription/find
 * @apiParamExample {json} Request-Example
 *{
 *   "consultationId": "59788089137dfe32e7ef4ddd"
 * }
 * @apiSuccess (Success 200) {String} message message related to response
 * @apiSuccess (Success 200) {Object} data data
 * @apiSuccessExample {json} Success-Response
 *{
 *   "message": "record found",
 *   "data": {
 *      "consultationId": "59788089137dfe32e7ef4ddd",
 *       "type": "prescription",
 *       "_id": "59a500ee2395a51980e20eb0",
 *       "medication": [
 *           {
 *                "foodWarning": "After meal",
 *                "doseTime" : ["Morning", "Night"],
 *                "instruction": "note test",
 *                "duration": "5 days ",
 *                "doseUnit": "mg",
 *                "doseValue": "375",
 *                "name": "amoxykind CV"
 *            }
 *        ],
 *        "date": "2017-08-29T05:51:33.833Z"
 *    }
 *   }
 */

/**
 * @api {DELETE} prescription/delete delete prescription
 * @apiHeader {String} Authorization token
 * @apiGroup Prescription
 * @apiDescription delete prescription
 * @apiName delete
 * @apiVersion 0.0.1
 * @apiParam {String} _id _id of prescription
 * @apiSampleRequest prescription/delete
 * @apiParamExample {json} Request-Example
 *{
 *   "_id": "59a500ee2395a51980e20eb0"
 * }
 * @apiSuccess (Success 200) {String} message message related to response
 * @apiSuccessExample {json} Success-Response
 *{
 *   "message": "Deleted successfully"
 * @apiError (Error 400) {String} message message related to response
 * @apiErrorExample {json} Validation Error
 * {
 *     "message": "No prescription found"
 * }
 */



