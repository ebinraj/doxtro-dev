/**
 * @api {POST} coupon/create Create Coupon
 * @apiHeader {String} Authorization token
 * @apiGroup Admin
 * @apiDescription Create Coupon
 * @apiName Create Coupon
 * @apiVersion 0.0.1
 * @apiParam {String} couponTag A unique coupon tag ex. "NOV50"
 * @apiParam {decimal} amount Discount/Amount is a valid number. Should be > 0.
 * @apiParam {DateTime} validFrom DateTime should be in yyyy-mm-dd hh:mm:ss format.
 * @apiParam {DateTime} validTo DateTime should be in yyyy-mm-dd hh:mm:ss format
 * @apiParam {boolean} isForFirstConsultation true if the coupon is for only first consultation
 * @apiParam {integer} iterationAllowed A valid number. Should be > 0. How many times a coupon can be used for a single user.
 * @apiSampleRequest coupon/create
 * @apiParamExample {json} Request-Example
 *   {
 *     "couponTag":"TestCoupan3",
 *     "amount":100,
 *     "validFrom":"2017-07-22",
 *     "validTo":"2017-07-29",
 *     "isForFirstConsultation":false,
 *     "iterationAllowed":3
 *   }
 * @apiSuccess (Success 200) {string} data data
 * @apiSuccessExample {json} Success-Response
 *"data": {
 *     "__v": 0,
 *     "createdAt": "2017-07-24T09:27:31.542Z",
 *     "updatedAt": "2017-07-20T06:31:32.648Z",
 *     "couponTag": "TestCoupan3",
 *      "amount": 100,
 *      "validFrom": "2017-07-22T00:00:00.000Z",
 *      "validTo": "2017-07-29T00:00:00.000Z",
 *      "isForFirstConsultation": false,
 *      "iterationAllowed": 3,
 *      "deleted": false,
 *      "_id": "5975bd83dc027b44127949c4"
 *   }
 * @apiError (Error 400) {string} message message related to response
 * @apiErrorExample {json} CoupanTag Conflict
 * {
 *     "message": "coupon already exists."
 * }
 * @apiErrorExample {json} Validation Error
 * {
 *     "message": "amount is not a valid number"
 * }
 */



/**
 * @api {GET} coupon/getCouponList Get Coupon List
 * @apiHeader {String} Authorization token
 * @apiGroup Admin
 * @apiDescription Get Coupon List
 * @apiName Get Coupon List
 * @apiVersion 0.0.1
 * @apiSampleRequest coupon/getCouponList
 * @apiSuccess (Success 200) {Object[]} data data
 * @apiSuccessExample {json} Success-Response
 *{
 *   "data": [
 *       {
 *          "_id": "5971c6c10ffd63123b97cfe6",
 *           "createdAt": "2017-07-21T09:17:53.858Z",
 *           "updatedAt": "2017-07-21T09:17:53.858Z",
 *           "couponTag": "TestCoupan",
 *           "amount": 100,
 *           "validFrom": "2017-07-22T00:00:00.000Z",
 *           "validTo": "2017-07-29T00:00:00.000Z",
 *           "isForFirstConsultation": false,
 *           "iterationAllowed": 3,
 *           "__v": 0,
 *           "deleted": false
 *       },
 *       {
 *           "_id": "5971c9cd7957f51344c2610a",
 *           "createdAt": "2017-07-21T09:30:53.654Z",
 *           "updatedAt": "2017-07-21T09:30:53.654Z",
 *           "couponTag": "TestCoupan1",
 *           "amount": 100,
 *           "validFrom": "2017-07-22T00:00:00.000Z",
 *           "validTo": "2017-07-29T00:00:00.000Z",
 *           "isForFirstConsultation": false,
 *           "iterationAllowed": 3,
 *           "__v": 0,
 *           "deleted": false
 *       }
 *   ] 
 *}
 */

/**
 * @api {POST} coupon/getCouponByTag Get Coupon by Tag
 * @apiHeader {String} Authorization token
 * @apiGroup Admin
 * @apiDescription Get single Coupon details by Tag
 * @apiName Create Coupon
 * @apiVersion 0.0.1
 * @apiParam {String} couponTag A unique coupon tag ex. "NOV50"
 * @apiSampleRequest coupon/getCouponByTag
 * @apiParamExample {json} Request-Example
 *   {
 *     "couponTag":"TestCoupan3"
 *   }
 * @apiSuccess (Success 200) {Object} data data
 * @apiSuccessExample {json} Success-Response
 *{
 *   "data": {
 *     "__v": 0,
 *     "createdAt": "2017-07-24T09:27:31.542Z",
 *     "updatedAt": "2017-07-20T06:31:32.648Z",
 *     "couponTag": "TestCoupan3",
 *      "amount": 100,
 *      "validFrom": "2017-07-22T00:00:00.000Z",
 *      "validTo": "2017-07-29T00:00:00.000Z",
 *      "isForFirstConsultation": false,
 *      "iterationAllowed": 3,
 *      "deleted": false,
 *      "_id": "5975bd83dc027b44127949c4"
 *   }
 * }
 * @apiError (Error 400) {String} message message related to response
 * @apiErrorExample {json} Not Found
 * {
 *     "message": "No coupon code exists with this name"
 * }
 */

/**
 * @api {PUT} coupon/_id/update Update Coupon
 * @apiHeader {String} Authorization token
 * @apiGroup Admin
 * @apiDescription Update Coupon
 * @apiName Update Coupon
 * @apiVersion 0.0.1
 * @apiParam {String} _id _id of coupon
 * @apiParam {String} couponTag A unique coupon tag ex. "NOV50"
 * @apiParam {decimal} amount Discount/Amount is a valid number. Should be > 0.
 * @apiParam {DateTime} validFrom DateTime should be in yyyy-mm-dd hh:mm:ss format.
 * @apiParam {DateTime} validTo DateTime should be in yyyy-mm-dd hh:mm:ss format
 * @apiParam {boolean} isForFirstConsultation true if the coupon is for only first consultation
 * @apiParam {integer} iterationAllowed A valid number. Should be > 0. How many times a coupon can be used for a single user.
 * @apiSampleRequest coupon/:_id/update
 * @apiParamExample {json} Request-Example
 *   {
 *     "couponTag":"TestCoupan3",
 *     "amount":100,
 *     "validFrom":"2017-07-22",
 *     "validTo":"2017-07-29",
 *     "isForFirstConsultation":false,
 *     "iterationAllowed":3
 *   }
 * @apiSuccess (Success 200) {String} message message related to response
 * @apiSuccess (Success 200) {Number} data data
 * @apiSuccessExample {json} Success-Response
 * {
 *   "message": "object modified",
 *   "data": 1
 * }
 * @apiError (Error 400) {string} message message related to response
 * @apiErrorExample {json} CoupanTag Conflict
 * {
 *     "message": "couponname already exists."
 * }
 * @apiErrorExample {json} Validation Error
 * {
 *     "message": "amount is not a valid number"
 * }
 */

/**
 * @api {POST} coupon/applyCoupon Apply Coupon
 * @apiHeader {String} Authorization token
 * @apiGroup Patient
 * @apiDescription Apply coupon for the discount
 * @apiName Apply coupon for the discount
 * @apiVersion 0.0.1
 * @apiParam {String} couponTag A unique coupon tag ex. "NOV50"
 * @apiParam {String} patientId PatientId (_id)
 * @apiParam {String} patientFee patient fee
 * @apiSampleRequest coupon/applyCoupon
 * @apiParamExample {json} Request-Example
 *   {
 *     "couponTag":"TestCoupon",
 *     "patientId":"597054d28a4183ef91c1f882",
 *     "patientFee" : 99,
 *     "gender" : "Male"
 *   }
 * @apiSuccess (Success 200) {Object} data data
 * @apiSuccessExample {json} Success-Response
 *{
 *   "message": "coupon discount",
 *   "data": {
 *       "consultingFee": 99,
 *       "doxtroCash": 0,
 *       "couponDiscount": 50,
 *       "totalPayable": 49
 *     }
 *   }
 * @apiError (Error 400) {string} message message related to response
 * @apiErrorExample {json} Not Found
 * {
 *     "message": "Invalid Coupon"
 * }
 * @apiErrorExample {json} Not Found
 * {
 *     "message": "Invalid patientFee"
 * }
 * @apiErrorExample {json} Expired Coupon
 * {
 *     "message": "Coupan is not active"
 * }
 * @apiErrorExample {json} Not new user OR have atleast one paid order in the past
 * {
 *     "message": "Coupon code is only available for the new users"
 * }
 */