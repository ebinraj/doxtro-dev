 /**
  * @api {POST} consultation/start Initiate Consultation
  * @apiHeader {String} Authorization token
  * @apiGroup Patient
  * @apiDescription Initiate Consultation
  * @apiName Initiate Consultation
  * @apiVersion 0.0.1
  * @apiParam {String} patientId A valid _id of the Patient
  * @apiParam {String} relativesId A valid _id of the Relatives.
  * @apiParam {String} consultingFor  relation 
  * @apiParam {String = "Chat", "Audio"} consultationType type of consultation
  * @apiParam {String} consultationLanguage language preferred for consultation
  * @apiParam {String} specializationCategory specialization looking for
  * @apiParam {String} note health concern
  * @apiParam {String[]} attachedDocuments array of the URLs (firebase document storage)
  * @apiSampleRequest consultation/start
  * @apiParamExample {json} Request-Example
  *   {
  *     "patientId":"596f095febee38d36c343198",
  *     "relativesId":"596f095febee38d36c343198",
  *     "consultingFor":"MySelf",
  *     "consultationType":"Chat",
  *     "consultationLanguage":"English",
  *     "specializationCategory":"General Physician",
  *     "note":"This is a note test",
  *     "attachedDocuments":["http://google.com","http://google.co.in"]
  *   }
  * @apiSuccess (Success 200) {Object} data data
  * @apiSuccessExample {json} Success-Response
  *"data": {
  *     "__v": 0,
  *     "createdAt": "2017-07-20T06:31:32.648Z",
  *       "updatedAt": "2017-07-20T06:31:32.648Z",
  *       "patientId": "596f095febee38d36c343198",
  *       "relativesId": "596f095febee38d36c343198",
  *       "consultingFor": "MySelf",
  *       "consultationType": "Chat",
  *       "consultationLanguage":"English",
  *       "specializationCategory": "General Physician",
  *       "note": "This is a note test",
  *       "deleted": false,
  *       "_id": "59704e442d0f35ed65279909",
  *       "prescriptionId": [],
  *       "attachedDocuments": [
  *           "http://google.com",
  *           "http://google.co.in"
  *       ]
  *   },
  * "patientFee" : 99
  * @apiError (Error 400) {String} message message related to response
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "Please enter a valid PatientId"
  * }
  * @apiErrorExample {json} Error-Repsonse(Invalid PatientId(ObjectId))
  * {
  *     "message": "Cast to ObjectId failed for value \"dsds\" at path \"_id\" for model \"patient\""
  * }
  */

 /**
  * @api {POST} consultation/getByPatientId Consultation list By PatientId
  * @apiHeader {String} Authorization token
  * @apiGroup Patient
  * @apiDescription Get consultation list by PatientId
  * @apiName Consultation By PatientId
  * @apiVersion 0.0.1
  * @apiParam {String} patientId A valid _id of the Patient
  * @apiSampleRequest consultation/getByPatientId
  * @apiParamExample {json} Request-Example
  *   {
  *     "patientId":"596f095febee38d36c343198",
  *   }
  * @apiSuccess (Success 200) {Object[]} data consultation list
  * @apiSuccessExample {json} Success-Response
  *{
  *   "data": [
  *       {
  *           "_id": "596f0a62a157d8d39af61d7f",
  *           "createdAt": "2017-07-19T07:29:38.922Z",
  *           "updatedAt": "2017-07-19T07:29:38.922Z",
  *           "patientId": "596f095febee38d36c343198",
  *           "consultingFor": "MySelf",
  *           "consultationType": "Chat",
  *           "specializationCategory": "General Physician",
  *           "note": "This is a note test",
  *           "__v": 0,
  *           "deleted": false,
  *           "prescriptionId": [],
  *           "attachedDocuments": [
  *               "http://google.com",
  *               "http://google.co.in"
  *           ]
  *       },
  *       {
  *           "_id": "59704e442d0f35ed65279909",
  *           "createdAt": "2017-07-20T06:31:32.648Z",
  *           "updatedAt": "2017-07-20T06:31:32.648Z",
  *           "patientId": "596f095febee38d36c343198",
  *           "consultingFor": "MySelf",
  *           "consultationType": "Chat",
  *           "specializationCategory": "General Physician",
  *           "note": "This is a note test",
  *           "__v": 0,
  *           "deleted": false,
  *           "prescriptionId": [],
  *           "attachedDocuments": [
  *               "http://google.com",
  *               "http://google.co.in"
  *           ]
  *       }
  *   ] 
  *}
  * @apiError (Error 400) {String} message message related to response
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "Please enter a valid PatientId"
  * }
  * @apiErrorExample {json} Error-Repsonse(Invalid PatientId(ObjectId))
  * {
  *     "message": "Cast to ObjectId failed for value \"dsds\" at path \"_id\" for model \"patient\""
  * }
  */

 /**
  * @api {POST} consultation/{_id}/sendToFBChat Send Message to FireBase chat
  * @apiHeader {String} Authorization token
  * @apiGroup Admin
  * @apiDescription Send Message to FireBase chat
  * @apiName Send Message to FireBase chat
  * @apiVersion 0.0.1
  * @apiParam {String} _id A valid ObjectId of the consultation in the API URL
  * @apiParam {String} _id A valid ObjectId of the Patient/Doctor/Bot
  * @apiParam {String} from Patient/Doctor/Bot
  * @apiParam {String} status Read/Delivered/Sent
  * @apiParam {String} msgType userIntro/alert/suggestion/prescription/userchat
  * @apiParam {Object} data A valid object containing body/header or direct objectlist in case of suggestion
  * @apiSampleRequest consultation/596f0a62a157d8d39af61d7g/sendToFBChat
  * @apiParamExample {json} Request-Example
  *   {
  *     "_id":"596f095febee38d36c343198",
  *     "from": "AA",
  *     "timeStamp": "2017-07-29 04:20",
  *     "status": "Sent",
  *     "msgType": "userchat",
  *     "data": {
  *              "text": "Hello Arun, Any one of the following doctros would be contacting you shortly"
  *              }
  *   }
  * @apiSuccess (Success 200) {String} message message
  * @apiSuccessExample {json} Success-Response
  *{
  *   "message": "Message Sent Successfully"
  *}
  */


  /**
  * @api {POST} consultation/submitFeedback submit feedback
  * @apiHeader {String} Authorization token
  * @apiGroup Patient
  * @apiDescription submit feedback
  * @apiName submitFeedback
  * @apiVersion 0.0.1
  * @apiParam {String} consultationId consultation Id
  * @apiParam {Object} feedback feedback object which contains following data
  * @apiParam {Number} feedback.rating rating
  * @apiParam {String} feedback.ratingValue rating value
  * @apiParam {String} feedback.behaviour behaviour
  * @apiParam {String} feedback.text text feedback
  * @apiSampleRequest consultation/submitFeedback
  * @apiParamExample {json} Request-Example
  *{
	*"consultationId" : "59b54fafe52f8a601746dfd4",
	*"feedback" : {
	*	       "rating" : 3,
	*	       "ratingValue" : "good",
	*	       "behaviour" : "good",
	*	       "text" : "good doctor"
	*     }
  *  }
  * @apiSuccess (Success 200) {String} message message
  * @apiSuccess (Success 200) {Object} data data
  * @apiSuccessExample {json} Success-Response
  *{
  * "message": "feedback submitted successfully",
  *  "data": {
  *      "consultationId": "59b54fafe52f8a601746dfd4"
  *    }
  *  }
  * @apiError (Error 400) {String} message message related to response
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "consultationId not provided"
  * }
  * @apiErrorExample {json} Error-Repsonse
  * {
  *     "message": "Feedback not provided"
  * }
  * @apiErrorExample {json} Error-Repsonse
  * {
  *     "message": "Feedback already submitted"
  * }
  */

  /**
   * @api {post} consultation/updateStatus close consultation
   * @apiHeader {String} Authorization token
   * @apiGroup Doctor
   * @apiDescription updateStatus
   * @apiName updateStatus
   * @apiVersion 0.0.1
   * @apiParam {String} consultationId consultation Id
   * @apiParam {String} firstName name of doctor
   * @apiParam {String} status="Completed" status of consultation
   * @apiSampleRequest consultation/updateStatus
   * @apiParamExample {json} Request-Example
   *{
	 *     "consultationId" : "59b54fafe52f8a601746dfd4",
	 *     "status" : "Completed",
	 *     "firstName" : "Dr. Ayush"
   * }
   * @apiSuccess (Success 200) {String} message message related to response
   * @apiSuccess (Success 200) {Object} data data
   * @apiSuccessExample {json} Success-Response
   *{
   *  "message": "updated successfully",
   *  "data": {
   *      "consultationId" : "59b54fafe52f8a601746dfd4"
   *    }
   *  }
   * @apiError (Error 400) {String} message message related to error
   * @apiErrorExample {json} Error-Response
   *   {
   *     "message": "No consultationId provided"
   *   }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "Invalid status"
   * }
   * @apiErrorExample {json} Error-Response
   *{
   * "message": "no consultation found"
   * } 
   */

    /**
  * @api {POST} consultation/sendRequest send request to doctor
  * @apiHeader {String} Authorization token
  * @apiGroup Admin
  * @apiDescription send request to doctor
  * @apiName sendResponse
  * @apiVersion 0.0.1
  * @apiParam {String} doctorId A valid _id of the doctor
  * @apiParam {String} consultationId consultationId
  * @apiSampleRequest consultation/sendResponse
  * @apiParamExample {json} Request-Example
  *   {
  *     "doctorId":"5975a4a4540798368b5d11f3",
  *     "consultationId" : "597716860992a152f6b3f689"
  *   }
  * @apiSuccess (Success 200) {String} message message related to response
  * @apiSuccessExample {json} Success-Response
  *  {
  *     "message": "notification sent successfully"
  *   }
  * @apiError (Error 400) {String} message message related to error
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "consultationId not provided"
  * }
  * @apiErrorExample {json} Error-Response
  * {
  *     "message": "This consultation request is already accepted"
  * }
  */
  