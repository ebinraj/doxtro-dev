/**
 * Project          : Doxtro
 * Module           : Configuration
 * Source filename  : index.js
 * Description      : Environment related configuration variables
 * Author           : Lloyd Presly Saldanha <lloyd.presly@above-inc.com>
 * Copyright        : Copyright © 2017
 *                    Written under contract by Above Solutions Pvt. Ltd.
 */

global.neev = require('neev');

let installNeev = function(configData) {
    global.database = neev.init('database', configData.DB_Config);
    global.AbstractController = neev.init('abstract', { 'database': 'mongo' }, database).abstract;
    global.mailer = neev.init('mail', '/config/mail.js', database);
    global.ObjectId = database.ObjectId;
    global.BotId = ObjectId("598c05caba42bb2f04e4ba63");
    global.botId = "598c05caba42bb2f04e4ba63";
    global.patientFee = 99;
    global.counters = database.createModel({
        name: 'counters',
        schema: { collectionName: String, field: String, nextSequence: Number }
    });
}

const configuration = {
    development: {
        root: require('path').normalize(__dirname + '/..'),
        app: {
            name: 'doxtro-api'
        },
        host: process.env.HOST || 'http://localhost',
        port: process.env.PORT || 3000,
        DB_Config: {
            connection: {
                mongodb: {
                    host: process.env.MONGODB_URL || process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || "mongodb://localhost:27017",
                    user: "",
                    pass: "",
                    database: process.env.DB || 'Doxtro-Development'
                }
            },
            plugins: {}
        },
        version: '0.0.1',
        URLPrefix: '/api/v1',
        security: {
            tokenLife: 3600
        },
        email: {
            senderEmail: '',
            password: ''
        },
        LoginByPass: false
    },
    staging: {
        root: require('path').normalize(__dirname + '/..'),
        app: {
            name: 'doxtro-api'
        },
        host: process.env.HOST || 'http://localhost',
        port: process.env.PORT || 8001,
        DB_Config: {
            connection: {
                mongodb: {
                    host: process.env.MONGODB_URL || process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || "mongodb://localhost:27017",
                    user: "",
                    pass: "",
                    database: process.env.DB || 'Doxtro-Staging'
                }
            }
        },
        version: '0.0.1',
        URLPrefix: '/api/v1',
        security: {
            tokenLife: 3600
        },
        facebook: {
            clientID: '',
            clientSecret: ''
        },
        google: {
            clientID: '',
            clientSecret: ''
        },
        email: {
            senderEmail: '',
            password: ''
        },
        LoginByPass: false
    },
    qa: {
        root: require('path').normalize(__dirname + '/..'),
        app: {
            name: 'doxtro-api'
        },
        host: process.env.HOST || 'http://localhost',
        port: process.env.PORT || 8000,
        DB_Config: {
            connection: {
                mongodb: {
                    host: process.env.MONGODB_URL || process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || "mongodb://localhost:27017",
                    user: "",
                    pass: "",
                    database: process.env.DB || 'Doxtro-Testing'
                }
            },
            plugins: {
                timestamps: true,
                elasticSearch: false,
                softDelete: true,
                autoPopulate: false,
                timestamps_fields: {
                    createdAt: 'createdAt',
                    updatedAt: 'updatedAt'
                }
            }
        },
        version: '0.0.1',
        URLPrefix: '/api/v1',
        security: {
            tokenLife: 3600
        },
        facebook: {
            clientID: '',
            clientSecret: ''
        },
        google: {
            clientID: '',
            clientSecret: ''
        },
        email: {
            senderEmail: '',
            password: ''
        },
        LoginByPass: false
    }
};

let env = process.env.NODE_ENV || 'development';
installNeev(configuration[env]);

module.exports = configuration[env];