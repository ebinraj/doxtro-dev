let config = require('./config');
let router = require('./routes');
global.User = require('./controllers/user');

const requireAll = require('require-all');
var compression = require('compression')
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morgan = require('morgan');
var cors = require('cors');
const express = require('express');
let app = express();

app.use(express.static('dist'));

global.fbadmin = require("firebase-admin");

//moment
global.moment = require('moment-timezone');
global.zone = "Asia/Kolkata";

//cronJob
//require('./generics/cronjob').run();

let serviceAccount = require("./config/serviceAccountKey.json");

fbadmin.initializeApp({
    credential: fbadmin.credential.cert(serviceAccount),
    databaseURL: "https://doxtrodevelopment.firebaseio.com"
});

app.use(cors());

app.get('/ping', (req, res) => {
    res.send("pong!");
});

// boostrap all models
global.models = requireAll({
    dirname: __dirname + '/models',
    filter: /(.+)\.js$/,
    resolve: function(Model) {
        return Model;
    }
});

// boostrap all controllers
global.controllers = requireAll({
    dirname: __dirname + '/controllers',
    filter: /(.+Controller)\.js$/,
    resolve: function(Controller) {
        return new Controller(models[Controller.name]);
    }
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(morgan('dev'));
app.all('*', (req, res, next) => {
    //console.log('---------------------------------------------------------------------------');
    //console.log('%s %s on %s from ', req.method, req.url, new Date(), req.headers['user-agent']);
    //console.log('Request Headers: ', req.headers);
    //console.log('Request Body: ', req.body);
    //console.log('Request Files: ', req.files);
    next();
});

// Add headers
app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

router(app);

app.listen(config.port, () => {
    console.log("Application is running on the port:" + config.port);
});